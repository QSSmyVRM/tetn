//ZD 100147 Start
/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace NS_LOGGER
{
	#region References
	
	#endregion 

	class Log
	{
		private string logFilePath = "C:\\VRMSchemas_v18\\ReportLog.log";
		private bool fileTraceEnabled = true;
		
		public Log ()
		{
		}

		public void Exception (int errorCode,string message)
		{
			message = ReplaceInvalidChars(message);			
			int severity = FetchSeverityLevel(errorCode);			

			StackFrame CallStack = new StackFrame(1, true);
			string file = CallStack.GetFileName();
			string method = CallStack.GetMethod().Name;
			int line = CallStack.GetFileLineNumber();

			string logRecord = "<EXCEPTION>";
			logRecord += "<MESSAGE>" + message + "</MESSAGE>";
			logRecord += "<SEVERITY>" + severity.ToString() + "</SEVERITY>";
			logRecord += "<FILE>" + file + "</FILE>";
			logRecord += "<METHOD>" + method + "</METHOD>";
			logRecord += "<LINE>" + line.ToString() + "</LINE>";
			logRecord += "<TIMESTAMP>" + DateTime.Now.ToString() + "</TIMESTAMP>";
			logRecord += "</EXCEPTION>";
			
			WriteToLogFile(logRecord);	
		}
		
		public  void Trace (string message)
		{
			if (fileTraceEnabled)
			{
				StackFrame CallStack = new StackFrame(1, true);
				string file = CallStack.GetFileName();
				string method = CallStack.GetMethod().Name;
				int line = CallStack.GetFileLineNumber();

				WriteToLogFile(message);
			}			
		}
		
		public  string FetchComMsg(int errorCode,string level,string message)
		{
			string errorMsg = "<error>";
			errorMsg += "<errorCode>" + errorCode.ToString() + "</errorCode>";
			errorMsg += "<message>" + message + "</message>";
			errorMsg += "<level>" + level + "</level>";
			errorMsg += "</error>";

			return (errorMsg);
		}

		private int FetchSeverityLevel(int errorCode)
		{
			if (errorCode >=0 && errorCode <=99)
				return (0);
			if (errorCode >=100 && errorCode <=199)
				return (1);
			if (errorCode >=200 && errorCode <=299)
				return (2);
			if (errorCode >=300 && errorCode <=399)
				return (3);

			return (0);
		}

		private  void WriteToLogFile(string logRecord)
		{

			Console.WriteLine (logRecord);
			StreamWriter sw ;

			try
			{
				if (!File.Exists(logFilePath))
				{
					// file doesnot exist . hence, create a new log file.
					sw = File.CreateText(logFilePath); 
					sw.Flush();
					sw.Close();
				}
				else
				{
					// check if exisiting log file size is greater than 50 MB
					FileInfo fi = new FileInfo(logFilePath);
					if (fi.Length > 50000000)
					{
						// delete the log file						
						File.Delete(logFilePath);
						
						// create a new log file 
						sw = File.CreateText(logFilePath);
						sw.Flush();
						sw.Close();
					}
				}
			
				// write the log record.
				sw = File.AppendText(logFilePath);	
				sw.WriteLine (logRecord);
				sw.Flush();
				sw.Close();
			}
			catch (Exception) 
			{
				// do nothing
			}
		}

		private  string ReplaceInvalidChars(string input)
		{
			input = input.Replace("<","&lt;");
			input = input.Replace(">","&gt;");
			input = input.Replace ("&","&amp;");
			return(input);
		}

	}
	
	class EvtLog
	{
		public string errMsg = null;
		
	
		public bool Log(string sSource,string sLog,string sEvent)
		{
			try
			{
				if (!EventLog.SourceExists(sSource))
					EventLog.CreateEventSource(sSource,sLog);
				EventLog.WriteEntry(sSource,sEvent);
				EventLog.WriteEntry(sSource,sEvent,EventLogEntryType.Error,100);
				
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				return false;
			}
			
		}
	}

}
