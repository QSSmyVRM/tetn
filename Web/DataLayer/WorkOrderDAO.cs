/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Work Orders.
    /// Implement IDispose to close session
    /// </summary>
    public class WorkOrderDAO
    {
        private static Hashtable woDAOFactories = null;
        private log4net.ILog m_log;
        private string m_configPath;

        conferenceDAO m_confDao;
        private IConferenceDAO m_IConferenceDAO;
        public InvWorkOrderDAO m_IWorkOrderDAO;
        private IAVItemDAO m_IAVItemDAO;
        private ICAItemDAO m_ICAItemDAO;
        private IHKItemDAO m_IHKItemDAO;

        public WorkOrderDAO(string config, log4net.ILog log)
        {
            try
            {
                m_log = log;
                m_configPath = config;

                if (woDAOFactories != null)
                {

                    if (woDAOFactories["m_confDao"] != null)
                        m_confDao = (conferenceDAO)woDAOFactories["m_confDao"];
                   
                }

                if (woDAOFactories == null)
                {

                    woDAOFactories = new Hashtable();
                    m_confDao = new conferenceDAO(m_configPath, m_log);
                    woDAOFactories.Add("m_confDao", m_confDao);
                    
                }
                m_IConferenceDAO = m_confDao.GetConferenceDao();
                m_IWorkOrderDAO = GetWorkOrderDAO();

                m_IAVItemDAO = GetAVItemDAO();
                m_ICAItemDAO = GetCAItemDAO();
                m_IHKItemDAO = GetHKItemDAO();

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        ~WorkOrderDAO() { }
        #region Public Properties

        
        #endregion

        public IReportScheduleDao GetReportScheduleDao()
        {
            return new ReportScheduleDao(m_configPath);
        }

        public InvCategoryDAO GetCategoryDAO() { return new CategoryDAO(m_configPath); }
        public IItemListDAO GetItemListDAO() { return new ItemListDAO(m_configPath); }
        public IAVItemDAO GetAVItemDAO() { return new AVItemDAO(m_configPath); }
        public ICAItemDAO GetCAItemDAO() { return new CAItemDAO(m_configPath); }
        public IHKItemDAO GetHKItemDAO() { return new HKItemDAO(m_configPath); }
        public InvListDAO GetInvListDAO() { return new invListDAO(m_configPath); }
        public InvRoomDAO GetInvRoomDAO() { return new invRoomDAO(m_configPath); }
        public InvWorkOrderDAO GetWorkOrderDAO() { return new invWorkOrderDAO(m_configPath); }
        public InvWorkItemDAO GetWorkItemDAO() { return new invWorkItemDAO(m_configPath); }
        public InvDeliveryTypeDAO GetDeliveryTypeDAO() { return new invDeliveryTypeDAO(m_configPath); }
        public InvServiceDAO GetInvServiceDAO() { return new invServiceDAO(m_configPath); }
        public InvWorkChargeDAO GetWorkChargeDAO() { return new invWorkChargeDAO(m_configPath); }
        public InvMenuDAO GetMenuDAO() { return new invMenuDAO(m_configPath); }
        public InvMenuServiceDAO GetMenuServiceDAO() { return new invMenuServiceDAO(m_configPath); }

        public class CategoryDAO :
             AbstractPersistenceDao<InventoryCategory, int>, InvCategoryDAO
        { 
            public CategoryDAO(string ConfigPath) : base(ConfigPath) { }
        }

        public class ItemListDAO :
             AbstractPersistenceDao<AVInventoryItemList, int>, IItemListDAO
        { public ItemListDAO(string ConfigPath) : base(ConfigPath) { } }

        public class AVItemDAO :
                     AbstractPersistenceDao<AVInventoryItemList, int>, IAVItemDAO
        { public AVItemDAO(string ConfigPath) : base(ConfigPath) { } }

        public class CAItemDAO :
                          AbstractPersistenceDao<CAInventoryItemList, int>, ICAItemDAO
        { public CAItemDAO(string ConfigPath) : base(ConfigPath) { } }

        public class HKItemDAO :
                          AbstractPersistenceDao<HKInventoryItemList, int>, IHKItemDAO
        { public HKItemDAO(string ConfigPath) : base(ConfigPath) { } }

        public class invListDAO :
                            AbstractPersistenceDao<InventoryList, int>, InvListDAO
        { public invListDAO(string ConfigPath) : base(ConfigPath) { } }

        public class invRoomDAO :
                                AbstractPersistenceDao<InventoryRoom, int>, InvRoomDAO
        { public invRoomDAO(string ConfigPath) : base(ConfigPath) { } }

        public class invWorkOrderDAO :
                                AbstractPersistenceDao<WorkOrder, int>, InvWorkOrderDAO
        { public invWorkOrderDAO(string ConfigPath) : base(ConfigPath) { } }

        public class invWorkItemDAO :
                                AbstractPersistenceDao<WorkItem, int>, InvWorkItemDAO
        { public invWorkItemDAO(string ConfigPath) : base(ConfigPath) { } }

        public class invDeliveryTypeDAO :
                                    AbstractPersistenceDao<DeliveryType, int>, InvDeliveryTypeDAO
        { public invDeliveryTypeDAO(string ConfigPath) : base(ConfigPath) { } }

        public class invServiceDAO :
                                    AbstractPersistenceDao<InventoryService, int>, InvServiceDAO
        { public invServiceDAO(string ConfigPath) : base(ConfigPath) { } }
        
        public class invWorkChargeDAO :
                                AbstractPersistenceDao<WorkItemCharge, int>, InvWorkChargeDAO
        { public invWorkChargeDAO(string ConfigPath) : base(ConfigPath) { } }
        public class invMenuDAO :
                            AbstractPersistenceDao<Menu, int>, InvMenuDAO
        { public invMenuDAO(string ConfigPath) : base(ConfigPath) { } }
        public class invMenuServiceDAO :
                            AbstractPersistenceDao<MenuService, int>, InvMenuServiceDAO
        { public invMenuServiceDAO(string ConfigPath) : base(ConfigPath) { } }

        public bool GetWODateByConfTime(int confid, int instanceid, ref DateTime theDate)
        {
            try
            {

                if (instanceid == 0)
                    instanceid = 1;

                vrmConference conf = m_IConferenceDAO.GetByConfId(confid, instanceid);

                if (!timeZone.userPreferedTime(conf.timezone, ref theDate))
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return true;
        }

        public bool SaveWorkOrderEmail(vrmEmail email)
        {
            bool bRet = true;
            try
            {
                userDAO user = new userDAO(m_configPath, m_log);
                user.SaveUserEmail(email);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return bRet;
        }

        public bool DeleteInventory(int ID, int cType)
        {
            bool bRet = true;
            try
            {
                InvCategoryDAO catDAO = GetCategoryDAO();
                InventoryCategory cat = catDAO.GetById(ID);
                cat.deleted = 1;
                catDAO.SaveOrUpdate(cat);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return bRet;
        }       
    }
}