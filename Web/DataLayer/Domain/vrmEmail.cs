/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Globalization;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for Email (Email_Queue_S).
	/// </summary>
	public class vrmEmail
	{
		#region Private Internal Members

		private int m_ID, m_RetryCount,m_isCalendar;//ICAL Fixes
		private string m_from, m_to, m_cc, m_subject, m_message, m_BCC, m_Attachment;
		private DateTime m_DateTimeCreated, m_LastRetryDateTime;
        private int m_orgID; //Added for FB 1710
        private int m_release;// Release email
		#endregion

		#region Public Properties

		public int UUID
		{
			get {return m_ID;}
			set {m_ID = value;}
		}
		public int RetryCount
		{
			get {return m_RetryCount;}
			set {m_RetryCount = value;}
		}
		public DateTime DateTimeCreated
		{
			get {return m_DateTimeCreated;}
			set {m_DateTimeCreated = value;}
		}			
		public DateTime LastRetryDateTime
		{
			get {return m_LastRetryDateTime;}
			set {m_LastRetryDateTime = value;}
		}			
		public string emailFrom
		{
			get {return m_from;}
			set {m_from = value;}
		}	
		public string emailTo
		{
			get {return m_to;}
			set {m_to = value;}
		}	
		public string cc
		{
			get {return m_cc;}
			set {m_cc = value;}
		}	
		public string BCC
		{
			get {return m_BCC;}
			set {m_BCC = value;}
		}	
		public string Subject
		{
			get {return m_subject;}
			set {m_subject = value;}
		}	
		public string Message
		{
			get {return m_message;}
			set {m_message = value;}
		}	
		public string Attachment
		{
			get {return m_Attachment;}
			set {m_Attachment = value;}
		}

        public int Iscalendar//ICAL Fixes
        {
            get { return m_isCalendar; }
            set { m_isCalendar = value; }
        }

        public int orgID //Added for FB 1710
        {
            get { return m_orgID; }
            set { m_orgID = value; }
        }

        public int Release //Added for release Email
        {
            get { return m_release; }
            set { m_release = value; }
        }

		#endregion

		public vrmEmail()
		{
			m_ID	= 0;
			m_cc	= string.Empty; 
			m_BCC	= string.Empty;
			m_RetryCount = 0;
			m_Attachment = string.Empty;
			m_DateTimeCreated = DateTime.Today;
			LastRetryDateTime = DateTime.Today;				
		}

	}

    public class vrmEmailFeedback
    {
        #region Private Internal Members

        private int m_userID;
        private string m_name, m_email, m_comment, m_subject, m_parentPage, m_browserAgent, m_browserLanguage;
        private string m_ipAddress, m_hostAddress;
       
        
        #endregion

        #region Public Properties

        public int userID
        {
            get { return m_userID; }
            set { m_userID = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string email
        {
            get { return m_email; }
            set { m_email = value; }
        }
        public string comment
        {
            get { return m_comment; }
            set { m_comment = value; }
        }
        public string subject
        {
            get { return m_subject; }
            set { m_subject = value; }
        }
        public string parentPage
        {
            get { return m_parentPage; }
            set { m_parentPage = value; }
        }

        public string browserAgent
        {
            get { return m_browserAgent; }
            set { m_browserAgent = value; }
        }
        public string browserLanguage
        {
            get { return m_browserLanguage; }
            set { m_browserLanguage = value; }
        }
        public string ipAddress
        {
            get { return m_ipAddress; }
            set { m_ipAddress = value; }
        }
        public string hostAddress
        {
            get { return m_hostAddress; }
            set { m_hostAddress = value; }
        }
 
        #endregion
 
    }

    //New classes added for FB 1830 start
    public class vrmEmailType
    {
        #region Private Internal Members

        private int m_emailid, m_emailcategory;
        private string m_emailtype, m_emailsubject;
        #endregion

        #region Public Properties

        public int emailid
        {
            get { return m_emailid; }
            set { m_emailid = value; }
        }
        public string emailtype
        {
            get { return m_emailtype; }
            set { m_emailtype = value; }
        }
        public string emailsubject
        {
            get { return m_emailsubject; }
            set { m_emailsubject = value; }
        }
        public int emailcategory
        {
            get { return m_emailcategory; }
            set { m_emailcategory = value; }
        }
        #endregion

        public vrmEmailType()
        {
            m_emailtype = "";
            m_emailsubject = "";
        }
    }

    public class vrmEmailContent
    {
        #region Private Internal Members

        private int m_EmailContentId, m_EmailLangId, m_EmailMode, m_Emailtypeid, m_ConfType; //ZD 104556
        private string m_EmailSubject, m_EmailBody, m_Placeholders, m_EmailLanguage;
        #endregion

        #region Public Properties

        public int EmailContentId
        {
            get { return m_EmailContentId; }
            set { m_EmailContentId = value; }
        }
        public int EmailLangId
        {
            get { return m_EmailLangId; }
            set { m_EmailLangId = value; }
        }
        public string Placeholders
        {
            get { return m_Placeholders; }
            set { m_Placeholders = value; }
        }
        public int EmailMode
        {
            get { return m_EmailMode; }
            set { m_EmailMode = value; }
        }
        public int Emailtypeid
        {
            get { return m_Emailtypeid; }
            set { m_Emailtypeid = value; }
        }
        public int ConfType //ZD 104556
        {
            get { return m_ConfType; }
            set { m_ConfType = value; }
        }

        public string EmailSubject
        {
            get { return m_EmailSubject; }
            set { m_EmailSubject = value; }
        }
        public string EmailBody
        {
            get { return m_EmailBody; }
            set { m_EmailBody = value; }
        }
        public string EmailLanguage
        {
            get { return m_EmailLanguage; }
            set { m_EmailLanguage = value; }
        }
        #endregion

    }

    public class vrmEmailLanguage
    {
        #region Private Internal Members

        private int m_EmailLangId, m_LanguageId, m_OrgId;
        private string m_EmailLanguage;
        #endregion

        #region Public Properties

        public int EmailLangId
        {
            get { return m_EmailLangId; }
            set { m_EmailLangId = value; }
        }
        public int LanguageId
        {
            get { return m_LanguageId; }
            set { m_LanguageId = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public string EmailLanguage
        {
            get { return m_EmailLanguage; }
            set { m_EmailLanguage = value; }
        }
        #endregion
      
    }

    public class vrmEPlaceHolders
    {
        #region Private Internal Members

        private int m_PlaceHolderId;
        private string m_Description;
        #endregion

        #region Public Properties

        public int PlaceHolderId
        {
            get { return m_PlaceHolderId; }
            set { m_PlaceHolderId = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        #endregion

    }
    //New classes added for FB 1830 end
}