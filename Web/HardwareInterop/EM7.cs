﻿//ALLDEV-842 Start - new class added for EM7

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;
using System.Collections;
using System.IO;
using System.Net;
using System.Xml;

namespace NS_EM7
{
    class EM7
    {
        #region public properties
        string EM7DeviceOutXML = "";        
        private NS_MESSENGER.ConfigParams configParams;
        private NS_MESSENGER.EM7Details EM7Details;
        private NS_DATABASE.Database db = null;
        private NS_LOGGER.Log logger;
        internal string errMsg = null;

        internal EM7(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
        }

        public class EM7DeviceStatus
        {
            public string DeviceIP { get; set; }
            public string DeviceStatus { get; set; }
            public string DeviceCurrentStatus { get; set; }
        }
        #endregion
          
        #region EM7EndPointPollStart
        internal bool EM7EndPointPollStart(ref String InXML)
        {
            XmlDocument xmldoc = null;
            String orgID = "";
            string EM7URI = "";
            if (db == null)
                db = new NS_DATABASE.Database(configParams);
            try
            {
                logger.Trace("******** EM7EndPointPoll Service Started ***************************");
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(InXML);
                if (xmldoc.SelectSingleNode("//RefreshEM7Endpoints/organizationID") != null && xmldoc.SelectSingleNode("//RefreshEM7Endpoints/organizationID").InnerText.Trim() != "")
                    orgID = xmldoc.SelectSingleNode("//RefreshEM7Endpoints/organizationID").InnerText.Trim();

                db.FetchEM7Details(ref EM7Details, orgID);

                EM7URI = "device?limit=100&extended_fetch=1&filter.organizations.in=[" + EM7Details.EM7OrgID + "]&filter.ip.not=";
                EM7DeviceOutXML = WebServiceCall(EM7URI);

                UpdatemyVRMEM7EnpointStatus();
                logger.Trace("******** EM7EndPointPoll Service Completed ***************************");
            }
            catch (Exception ex)
            {
                logger.Trace(ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        
        #region WebServiceCall
        private string WebServiceCall(string EM7URI)
        {
            string outXML = "";
            try
            {
                logger.Trace("**************Getting into Web Request Call*************");
                string url = EM7Details.EM7URL + "/api/" + EM7URI;
                logger.Trace("EM7 URL:" + url);
                string authInfo = EM7Details.EM7Username + ":" + EM7Details.EM7Password;
                logger.Trace("EM7 Authentication info:" + authInfo);
                String credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Accept = "application/xml, */*.";
                request.MediaType = "HTTP/1.1";
                request.KeepAlive = true;
                request.Headers["Authorization"] = "Basic " + credentials;
                request.Method = "GET";

                WebResponse response = request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    outXML = reader.ReadToEnd();
                }
                if (response.Headers["http_code"] != null)
                {
                    outXML += "Error :" + response.Headers["http_code"];
                }
                logger.Trace("*******************************Received XML:" + outXML);
                return outXML;
            }
            catch (WebException wex)
            {
                String error = "";
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = " Error from response : " + reader.ReadToEnd();
                        }
                    }
                }
                error = " message from wex" + wex.Message;
                outXML = "Error from web exception: " + error;
            }

            catch (Exception ex)
            {
                outXML = "Error Error Pls help Message:" + ex.Message + " Stactarce:" + ex.StackTrace;
            }
            logger.Trace("*******************************Received XML:" + outXML);
            return outXML;
        }
        #endregion

        #region UpdatemyVRMEM7EnpointStatus
        private void UpdatemyVRMEM7EnpointStatus()
        {
            XDocument xmlDoc = new XDocument();
            Hashtable DeviceHashTable = new Hashtable();
            bool ret = false;
            if (db == null)
                db = new NS_DATABASE.Database(configParams);
            try
            {
                logger.Trace("************************Update EM7 EndPoint in Myvrm***************");
                xmlDoc = XDocument.Parse(EM7DeviceOutXML);
                logger.Trace(xmlDoc.ToString());
                if (EM7DeviceOutXML.IndexOf("<error>") < 0)
                {
                    var EM7DeviceState = (from data in xmlDoc.Descendants("device")
                                          select new EM7DeviceStatus
                                          {
                                              DeviceIP = data.Element("ip").Value,
                                              DeviceStatus = data.Element("state").Value,
                                              DeviceCurrentStatus = data.Element("state").Value
                                          }).ToList();
                    
                    for (int i = 0; i < EM7DeviceState.Count; i++)
                    {
                        DeviceHashTable.Add(EM7DeviceState[i].DeviceIP, EM7DeviceState[i].DeviceStatus);
                    }
                    ret = db.UpdateEndpointStatus(DeviceHashTable);
                    if (!ret)
                        logger.Trace("************************Update EM7 EndPoint in Myvrm Failed***************");
                    else
                        logger.Trace("************************Update EM7 EndPoint in Myvrm Succeed***************");
                }
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
            }
        }
        #endregion
    }
}
