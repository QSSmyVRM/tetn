﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using log4net;
using NHibernate.Criterion;
using myVRM.DataLayer;

namespace myVRM.BusinessLayer
{
    #region sRecurrDates
    internal struct sRecurrDates
    {
        #region Private Members

        private DateTime s_RecDatetime;
        private int s_Duration;
        private int s_SetDuration;
        private int s_TearDuration;
        private int s_Timezone;
        private string s_StartHour;
        private string s_StartMin;
        private string s_StartSet;

        #endregion

        #region Public Property Definitions

        public DateTime RecDatetime { get { return s_RecDatetime; } set { s_RecDatetime = value; } }
        public int Duration { get { return s_Duration; } set { s_Duration = value; } }
        public int SetDuration { get { return s_SetDuration; } set { s_SetDuration = value; } }
        public int TearDuration { get { return s_TearDuration; } set { s_TearDuration = value; } }
        public int Timezone { get { return s_Timezone; } set { s_Timezone = value; } }
        public string StartHour { get { return s_StartHour; } set { s_StartHour = value; } }
        public string StartMin { get { return s_StartMin; } set { s_StartMin = value; } }
        public string StartSet { get { return s_StartSet; } set { s_StartSet = value; } }

        #endregion

    }
    #endregion

    #region RecurrenceFactory
    /// <summary>
    /// RecurrenceFactory
    /// </summary>
    /// /***************** Recurring Pattern Information ********************************** */
    /*
    //common
    recurInfo.recurType;  //recur type: 1= Daily; 2= Weekly; 3= Monthly; 4= Yearly; 5= Customly.
    recurInfo.endType; //end type : 1= no end date ,2=end after <occurrence> times ,3=end by <endDate>
    recurInfo.startTime; //start
    recurInfo.duration;
    recurInfo.occurrence; //repeat

    //Daily
    recurInfo.subType; //daily type - 1 - Every N Days or 2 - Every Weekdays
    recurInfo.gap; //gap 

    //Weekly
    recurInfo.gap; //gap 
    recurInfo.days; //selected days... delimited with comma 1 to 7 (for Sunday to Saturday respectively).

    //Monthly
    recurInfo.subType = monthlyType; //1 - Day of every month or 2 - The -- of every month
                
    //For Monthly type 1
    recurInfo.gap = monthGap; //gap 
    recurInfo.dayno = monthDayNo; //day

    //For Monthly type 2  //startDate, gap, repeat, weekday, nthday
    recurInfo.gap = monthGap; //gap 
    recurInfo.dayno = monthWeekDayNo; //1= First; 2= Second; 3= Third; 4= Fourth; 5= Last.
    recurInfo.days = monthWeekDay; //1 = Day; 2 = Weekday; 3 = Weekend; 4 = Sunday; 5 = Monday; 6 = Tuesday; 7 = Wednesday; 8 = Thursday; 9 = Friday; 10 = Saturday.

    //Yearly
    recurInfo.subType = yearlyType; //1-Every.. or 2 - The
    recurInfo.yearMonth = yearMonth; //month 1 to 12 for respective months. 1-Jan, 2-FEB..

    //Yearly type - 1 //startDate, dayOfMonth, month, repeat
    recurInfo.days = yearMonthDay; //dayOfMonth

    //Yearly type - 2
    recurInfo.dayno = yearMonthWeekDayNo; //nthday ==> 1= First; 2= Second; 3= Third; 4= Fourth; 5= Last.
    recurInfo.days = yearMonthWeekDay; //weekday ==>1= Day; 2= Weekday; 3= Weekend;4= Sunday; 5= Monday; 6= Tuesday; 7= Wednesday; 8= Thursday;9= Friday; 10= Saturday.
    */
    /***************** Recurring Pattern Information ********************************** */

    internal class RecurrenceFactory
    {
        #region Members

        private int starthour;
        private int startmin;
        private string conftime = "";
        private string startdate = "";
        private DateTime startDatetime;

        internal vrmRecurInfo recurInfo;
        internal string startHour;
        internal string startMin;
        internal string startSet;
        internal int setDuration;
        internal int tearDuration;
        internal string recurringText; //ZD 103500
                
        internal int maxRecurInstance = sysSettings.ConfRecurLimit;//ZD 101837
        List<sRecurrDates> recDates = new List<sRecurrDates>();
        sRecurrDates sRecDt = new sRecurrDates();
        //FB 2231 start
        internal string RecurPatternTxt = "";
        String endType1Pattern = "", endType2Pattern = "", endType3Pattern = "";
        String[] recurDayNo = { "first ", "second ", "thrid ", "fourth ", "last " };

        String[] recurDays = {"day", "Weekday","Weekend","Sunday","Monday","Tuesday",
                               "Wednesday","Thursday","Friday","Saturday" };
        //FB 2231 end
        internal string RecurPattern = "", recurParams = ""; //ZD 103500
        #endregion

        #region SetTime
        private void SetTime()
        {
            try
            {
                starthour = 0;
                startmin = 0;
                startdate = "";
                startDatetime = DateTime.MinValue;

                int.TryParse(startHour, out starthour);
                int.TryParse(startMin, out startmin);
                
                conftime = startHour + ":" + startMin + ":45 " + startSet;
                //FB 2231 start
                RecurPatternTxt = RecurPatternTxt.Replace("{2}", recurInfo.startTime.ToString("MM/dd/yyyy"));
                RecurPatternTxt = RecurPatternTxt.Replace("{4}", recurInfo.startTime.ToString("hh:mm tt"));
                RecurPatternTxt = RecurPatternTxt.Replace("{5}", (recurInfo.duration / 60).ToString());

                //ZD 103500
                DateTime stDate = recurInfo.startTime;
                timeZone.changeToGMTTime(recurInfo.timezoneid, ref stDate);
                recurParams = stDate.ToString();
                RecurPattern = RecurPattern.Replace("{5}", (recurInfo.duration / 60).ToString()); 
                string strDuration = "";
                if ((recurInfo.duration / 60) < 1)
                    strDuration = recurInfo.duration + " mins";
                    //RecurPatternTxt = RecurPatternTxt.Trim() + " " + recurInfo.duration + " mins";
                else if((recurInfo.duration % 60) > 0 )
                    strDuration = (recurInfo.duration % 60) + " mins";
                    //RecurPatternTxt = RecurPatternTxt.Trim() + " " + (recurInfo.duration % 60) + " mins";
                //FB 2231 end
                RecurPatternTxt = RecurPatternTxt.Trim() + " " + strDuration;
                RecurPattern = RecurPattern.Trim() + " " + strDuration;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region GetDailyRecurDates
        internal List<sRecurrDates> GetDailyRecurDates()
        {
            try
            {
                //FB 2231 start
                Int32 intNoOfRecurrence = Int32.MinValue;
                
                //ZD 101837 //ZD 102783
                if (recurInfo.Conf != null && recurInfo.Conf.WebExConf == 1)
                {
                    if (maxRecurInstance > 50)
                        maxRecurInstance = 50;
                }                

                if (recurInfo.subType == 1)
                {
                    endType1Pattern = "Occurs every {1} day(s) effective {2} from {4} for {5} hrs"; //ZD 100528
                    endType2Pattern = "Occurs every {1} day(s) effective {2} occurs {3} time(s) from {4} for {5} hrs"; //ZD 100528
                    endType3Pattern = "Occurs every {1} day(s) effective {2} until {3} from {4} for {5} hrs"; //ZD 100528
                }
                else if (recurInfo.subType == 2)
                {
                    endType1Pattern = "Occurs every weekday effective {2} from {4} for {5} hrs"; //ZD 100528
                    endType2Pattern = "Occurs every weekday effective {2} occurs {3} time(s) from {4} for {5} hrs"; //ZD 100528
                    endType3Pattern = "Occurs every weekday effective {2} until {3} from {4} for {5} hrs"; //ZD 100528
                }

                if (recurInfo.endType == 1)
                    RecurPatternTxt = endType1Pattern;
                else if (recurInfo.endType == 2)
                    RecurPatternTxt = endType2Pattern;
                else if (recurInfo.endType == 3)
                    RecurPatternTxt = endType3Pattern;
                //FB 2231 end
                RecurPattern = RecurPatternTxt; //ZD 103500
                SetTime();
                if (recurInfo.subType == 1)
                {
                    DateTime createdDate = DateTime.Today.Date;
                    DateTime CalCulateddate = DateTime.MinValue;
                    Int32 intdateGap = Int32.MinValue;

                    createdDate = recurInfo.startTime; // INPUT START DATE          Convert.ToDateTime(row["StartDate"]);
                    intdateGap = recurInfo.gap; // INPUT GAP                        Convert.ToInt32(row["DayGap"].ToString());

                    if (recurInfo.endType != 2)
                        intNoOfRecurrence = maxRecurInstance;
                    else
                    {
                        if (recurInfo.occurrence > maxRecurInstance)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                            intNoOfRecurrence = recurInfo.occurrence;
                    }

                    if (intdateGap != Int32.MinValue)
                    {
                        CalCulateddate = createdDate;
                        for (int i = 0; i < intNoOfRecurrence; i++)
                        {
                            startdate = CalCulateddate.ToShortDateString() + " " + conftime;
                            DateTime.TryParse(startdate, out startDatetime);

                            if (i == 0)
                            {
                                DateTime dtGMT = startDatetime;
                                timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                                recurParams = dtGMT.ToString(); //ZD 103500
                            }
                            sRecDt.RecDatetime = startDatetime;

                            //sRecDt.RecDatetime = new DateTime(CalCulateddate.Year, CalCulateddate.Month, CalCulateddate.Day, -, startMin, 0);
                            sRecDt.Duration = recurInfo.duration;
                            sRecDt.SetDuration = setDuration;
                            sRecDt.TearDuration = tearDuration;
                            sRecDt.Timezone  = recurInfo.timezoneid;
                            sRecDt.StartHour = startHour;
                            sRecDt.StartMin = startMin;
                            sRecDt.StartSet = startSet;
                            recDates.Add(sRecDt);

                            if (recurInfo.endType == 3)
                            {
                                if (i < intNoOfRecurrence && DateTime.Compare(recurInfo.endTime, CalCulateddate) < 0)
                                    break;
                                else if (i + 1 == intNoOfRecurrence && DateTime.Compare(CalCulateddate, recurInfo.endTime) < 0)
                                    break;
                            }
                            CalCulateddate = CalCulateddate.AddDays(intdateGap);

                            DateTime calDate = DateTime.MinValue;//FB 2164
                            DateTime endDate = DateTime.MinValue;
                            DateTime.TryParse(CalCulateddate.ToShortDateString(), out calDate);
                            DateTime.TryParse(recurInfo.endTime.ToShortDateString(), out endDate);
                            if (recurInfo.endType == 3 && calDate > endDate)
                                break;
                        }
                    }

                    RecurPatternTxt = RecurPatternTxt.Replace("{1}", recurInfo.gap.ToString()); //FB 2231
                    RecurPattern = RecurPattern.Replace("{1}", recurInfo.gap.ToString()); //ZD 103500

                }
                else if (recurInfo.subType == 2)
                {
                    DateTime createdDate = DateTime.Today.Date;
                    DateTime CalCulateddate = DateTime.MinValue;
                    intNoOfRecurrence = Int32.MinValue;

                    if (recurInfo.endType != 2)
                        intNoOfRecurrence = maxRecurInstance;
                    else
                    {
                        if (recurInfo.occurrence > maxRecurInstance)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                            intNoOfRecurrence = recurInfo.occurrence;
                    }

                    int recNo = System.Convert.ToInt32(intNoOfRecurrence); //recursive number
                    DateTime[] actDate = new DateTime[5 * recNo]; // Actural Number of Dates Required
                    DateTime today = DateTime.Today.Date;
                    DateTime today1 = DateTime.Today.Date;
                    today1 = recurInfo.startTime.Date; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    //today = today1.AddDays(1);
                    today = today1;
                    int glbCount = 0;
                    for (int k = 0; k < recNo; k++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            string day = today.DayOfWeek.ToString();

                            if (day == "Saturday")
                            {
                                today = today.AddDays(2);
                                j -= 1;

                            }
                            else if (day == "Sunday")
                            {
                                today = today.AddDays(1);
                                j -= 1;
                            }
                            else
                            {
                                if (recurInfo.endType == 3)
                                {
                                    if (k < recNo && DateTime.Compare(recurInfo.endTime, today) < 0)
                                    {
                                        k = recNo; // TO EXIT OUTER LOOP
                                        recNo = glbCount; // COUNT OF GENERATED DATEs
                                        break;
                                    }
                                    else if (k + 1 == recNo && DateTime.Compare(today, recurInfo.endTime) < 0)
                                    {
                                        k = recNo; // TO EXIT OUTER LOOP
                                        recNo = glbCount - 1; // COUNT OF GENERATED DATEs
                                        break;
                                    }
                                }
                                actDate[glbCount++] = today;
                                today = today.AddDays(1);
                            }
                        }
                    }
                    int recNo2 = recNo; // 5 * recNo;

                    //strDailyweekday = actDate[0].Month.ToString() + "/" + actDate[0].Day.ToString() + "/" + actDate[0].Year.ToString();
                    for (int cnt = 0; cnt < recNo2; cnt++)
                    {
                        startdate = actDate[cnt].ToShortDateString() + " " + conftime;
                        DateTime.TryParse(startdate, out startDatetime);

                        if (cnt == 0)
                        {
                            DateTime dtGMT = startDatetime;
                            timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                            recurParams = dtGMT.ToString(); //ZD 103500
                        }
                        sRecDt.RecDatetime = startDatetime;
                        //sRecDt.RecDatetime = new DateTime(actDate[cnt].Year, actDate[cnt].Month, actDate[cnt].Day, startHour, startMin, 0);
                        sRecDt.Duration = recurInfo.duration;
                        sRecDt.SetDuration = setDuration;
                        sRecDt.TearDuration = tearDuration;
                        sRecDt.Timezone = recurInfo.timezoneid;
                        sRecDt.StartHour = startHour;
                        sRecDt.StartMin = startMin;
                        sRecDt.StartSet = startSet;

                        recDates.Add(sRecDt);
                    }
                }

                if (recurInfo.endType == 2) //FB 2231
                {
                    RecurPatternTxt = RecurPatternTxt.Replace("{3}", intNoOfRecurrence.ToString());
                    RecurPattern = RecurPattern.Replace("{3}", intNoOfRecurrence.ToString()); //ZD 103500
                }
                else if (recurInfo.endType == 3)
                {
                    RecurPatternTxt = RecurPatternTxt.Replace("{3}", recurInfo.endTime.ToString("MM/dd/yyyy"));
                    DateTime dtEnd = recurInfo.endTime;
                    timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtEnd);

                    recurParams = recurParams + "||" + dtEnd.ToString(); //ZD 103500
                }

                recurringText = RecurPattern + "||" + recurParams; //ZD 103500

            }
            catch (Exception e)
            {
                throw new Exception("GetDailyRecurDates: ", e);
            }
            return recDates;
        }
        #endregion

        #region GetWeeklyRecurDates
        internal List<sRecurrDates> GetWeeklyRecurDates()
        {
            try
            {
                //ZD 101837 //ZD 102783
                if (recurInfo.Conf != null && recurInfo.Conf.WebExConf == 1)
                {
                    if (maxRecurInstance > 50)
                        maxRecurInstance = 50;
                }      

                //FB 2231 start
                endType1Pattern = "Occurs every {1} week(s) on {6} effective {2} from {4} for {5} hrs"; //ZD 100528
                endType2Pattern = "Occurs every {1} week(s) on {6} effective {2} occurs {3} time(s) from {4} for {5} hrs"; //ZD 100528
                endType3Pattern = "Occurs every {1} week(s) on {6} effective {2} until {3} from {4} for {5} hrs";//ZD 100528

                if (recurInfo.endType == 1)
                    RecurPatternTxt = endType1Pattern;
                else if (recurInfo.endType == 2)
                    RecurPatternTxt = endType2Pattern;
                else if (recurInfo.endType == 3)
                    RecurPatternTxt = endType3Pattern;
                //FB 2231 end
                RecurPattern = RecurPatternTxt; //ZD 103500
                SetTime();
                if (recurInfo != null)
                {

                    if (recurInfo.subType == 0)
                    {
                        Int32 intNoOfRecurrence = Int32.MinValue;
                        DateTime createdDate = DateTime.Now;
                        DateTime CalCulateddate = DateTime.MinValue;
                        Int32 intweekgap = Int32.MinValue;
                        intweekgap = recurInfo.gap; // INPUT GAP                	Convert.ToInt32(row["WeekGap"].ToString());

                        if (recurInfo.endType != 2)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                        {
                            if (recurInfo.occurrence > maxRecurInstance)
                                intNoOfRecurrence = maxRecurInstance;
                            else
                                intNoOfRecurrence = recurInfo.occurrence;
                        }

                        string[] wkdays = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

                        String wk = "";

                        string selWeekDays = recurInfo.days; // INPUT SELECTED WEEK DAYS IN NUMBER  row["WeekDays"].ToString();//Selected Week Days

                        for (int i = 1; i < 8; i++)
                        {
                            if (selWeekDays.IndexOf(i.ToString()) >= 0)
                            {
                                wk += ", " + wkdays[i - 1];
                            }

                        }

                        int stp = intweekgap - 1; //stp x 7
                        int recNo = System.Convert.ToInt32(intNoOfRecurrence); //recursive number
                        DateTime today = DateTime.Today.Date;
                        DateTime today1 = DateTime.Today.Date;
                        today1 = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                        int glbCount = 0;

                        today = today1.Date; //today1.AddDays((stp * 7) + 1);

                        string[] wkdayslist = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                        string curday = today.DayOfWeek.ToString();

                        if (curday != "Sunday")
                        {
                            for (int i = 0; i < 7; i++)
                            {
                                if (curday == wkdayslist[i])
                                {
                                    for (int j = 0; j < 7; j++)
                                    {
                                        wkdays[j] = wkdayslist[i];
                                        i++;
                                        if (i > 6)
                                        {
                                            i = 0;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        int nilCount = 0;
                        for (int ind = 0; ind < wkdays.Length; ind++)
                        {
                            if (wk.IndexOf(wkdays[ind]) < 0)
                            {
                                wkdays[ind] = "Nil";
                                nilCount += 1;
                            }
                        }
                        DateTime[] actDate = new DateTime[(7 - nilCount) * recNo]; // Actural Number of Dates Required
                        for (int k = 0; k < recNo; k++)
                        {

                            for (int j = 0; j < 7; j++)
                            {
                                string day = today.DayOfWeek.ToString();
                                if (wkdays[j] == day)
                                {
                                    actDate[glbCount++] = today;

                                    if (recurInfo.endType == 3)
                                    {
                                        if (k < recNo && DateTime.Compare(recurInfo.endTime, today) < 0)
                                        {
                                            k = recNo; // TO EXIT OUTER LOOP
                                            recNo = glbCount - 1; // COUNT OF GENERATED DATEs
                                            break;

                                        }
                                        else if (k + 1 == recNo && DateTime.Compare(today, recurInfo.endTime) < 0)
                                        {
                                            k = recNo; // TO EXIT OUTER LOOP
                                            recNo = glbCount - 1; // COUNT OF GENERATED DATEs
                                            break;
                                        }
                                    }
                                }
                                today = today.AddDays(1);
                            }

                            today = today.AddDays(stp * 7);
                        }
                        int recNo2 = (7 - nilCount) * recNo;

                        for (int cnt = 0; cnt < recNo; cnt++)
                        {
                            startdate = actDate[cnt].ToShortDateString() + " " + conftime;
                            DateTime.TryParse(startdate, out startDatetime);
                            if (cnt == 0)
                            {
                                DateTime dtGMT = startDatetime;
                                timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                                recurParams = dtGMT.ToString(); //ZD 103500
                            }
                            sRecDt.RecDatetime = startDatetime;
                            //sRecDt.RecDatetime = new DateTime(actDate[cnt].Year, actDate[cnt].Month, actDate[cnt].Day, startHour, startMin, 0);
                            sRecDt.Duration = recurInfo.duration;
                            sRecDt.SetDuration = setDuration;
                            sRecDt.TearDuration = tearDuration;
                            sRecDt.Timezone = recurInfo.timezoneid;
                            sRecDt.StartHour = startHour;
                            sRecDt.StartMin = startMin;
                            sRecDt.StartSet = startSet;

                            recDates.Add(sRecDt);
                            // strWeekly += ", " + actDate[cnt].Month.ToString() + "/" + actDate[cnt].Day.ToString() + "/" + actDate[cnt].Year.ToString();
                        }

                        //FB 2231 start
                        String days = "";
                        if (recurInfo.days != null)
                        {
                            int DayWeek = 0;
                            String[] daysArray = recurInfo.days.Split(',');
                            for (int d = 0; d < daysArray.Length; d++)
                            {
                                Int32.TryParse(daysArray[d].ToString(), out DayWeek);

                                if (daysArray.Length > 1 && d == (daysArray.Length - 1))
                                    days += " and ";

                                if (wkdayslist[DayWeek - 1] != null)
                                    days += wkdayslist[DayWeek - 1].ToString();

                                if (daysArray.Length > 1 && d < (daysArray.Length - 2))
                                    days += ",";
                            }
                        }

                        RecurPatternTxt = RecurPatternTxt.Replace("{1}", recurInfo.gap.ToString());
                        RecurPatternTxt = RecurPatternTxt.Replace("{6}", days);
                        //ZD 103500
                        RecurPattern = RecurPattern.Replace("{1}", recurInfo.gap.ToString());
                        RecurPattern = RecurPattern.Replace("{6}", days);

                        if (recurInfo.endType == 2) //FB 2231
                        {
                            RecurPatternTxt = RecurPatternTxt.Replace("{3}", intNoOfRecurrence.ToString());
                            RecurPattern = RecurPattern.Replace("{3}", intNoOfRecurrence.ToString()); //ZD 103500
                        }
                        else if (recurInfo.endType == 3)
                        {
                            RecurPatternTxt = RecurPatternTxt.Replace("{3}", recurInfo.endTime.ToString("MM/dd/yyyy"));
                            DateTime dtEnd = recurInfo.endTime;
                            timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtEnd);

                            recurParams = recurParams + "||" + dtEnd.ToString(); //ZD 103500
                        }

                        recurringText = RecurPattern + "||" + recurParams; //ZD 103500
                        //FB 2231 end
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("GetWeeklyRecurDates: ", e);
            }
            return recDates;

        }
        #endregion

        #region GetMonthlyRecurDates
        internal List<sRecurrDates> GetMonthlyRecurDates()
        {
            try
            {
                //FB 2231 start
                Int32 intNoOfRecurrence = Int32.MinValue;

                //ZD 101837 //ZD 102783
                if (recurInfo.Conf != null && recurInfo.Conf.WebExConf == 1)
                {
                    if (maxRecurInstance > 50)
                        maxRecurInstance = 50;
                }      

                if (recurInfo.subType == 1)
                {
                    endType1Pattern = "Occurs day {6} of every {1} month(s) effective {2} from {4} for {5} hrs"; //ZD 100528
                    endType2Pattern = "Occurs day {6} of every {1} month(s) effective {2} occurs {3} time(s) from {4} for {5} hrs";//ZD 100528
                    endType3Pattern = "Occurs day {6} of every {1} month(s) effective {2} until {3} from {4} for {5} hrs"; //ZD 100528
                }
                else if (recurInfo.subType == 2)
                {
                    endType1Pattern = "Occurs the {6} of every {1} month(s) effective {2} from {4} for {5} hrs"; //ZD 100528
                    endType2Pattern = "Occurs the {6} of every {1} month(s) effective {2} occurs {3} time(s) from {4} for {5} hrs "; //ZD 100528
                    endType3Pattern = "Occurs the {6} of every {1} month(s) effective {2} until {3} from {4} for {5} hrs ";//ZD 100528
                }

                if (recurInfo.endType == 1)
                    RecurPatternTxt = endType1Pattern;
                else if (recurInfo.endType == 2)
                    RecurPatternTxt = endType2Pattern;
                else if (recurInfo.endType == 3)
                    RecurPatternTxt = endType3Pattern;
                //FB 2231 end
                RecurPattern = RecurPatternTxt; //ZD 103500

                SetTime();
                if (recurInfo.subType == 1)
                {
                    DateTime createdDate = DateTime.Today.Date;
                    int curDay = createdDate.Day; // Current Day
                    DateTime CalCulateddsteMonth = DateTime.MinValue;
                    DateTime CalCulateddate = DateTime.MinValue;
                    Int32 intMonthGap = Int32.MinValue;
                    Int32 intMonthDayno = Int32.MinValue;
                    createdDate = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    intMonthDayno = recurInfo.dayno; // INPUT START DATE    Convert.ToInt32(row["MonthDayno"].ToString()); //Day Passed Here
                    intMonthGap = recurInfo.gap; // INPUT GAP                	Convert.ToInt32(row["MonthGap"].ToString());

                    if (recurInfo.endType != 2)
                        intNoOfRecurrence = maxRecurInstance;
                    else
                    {
                        if (recurInfo.occurrence > maxRecurInstance)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                            intNoOfRecurrence = recurInfo.occurrence;
                    }

                    if (intMonthDayno < createdDate.Day)
                        createdDate = createdDate.AddMonths(intMonthGap);

                    if (intMonthDayno > 28)
                    {
                        int days = DateTime.DaysInMonth(createdDate.Year, createdDate.Month);
                        if (intMonthDayno > days)
                            createdDate = createdDate.AddMonths(intMonthGap);
                    }


                    if (intMonthGap != Int32.MinValue)
                    {

                        CalCulateddate = Convert.ToDateTime(createdDate.Month + "/" + intMonthDayno + "/" + createdDate.Year);

                        for (int i = 0; i < intNoOfRecurrence; i++)
                        {
                            if (intMonthDayno > 28)
                            {
                                int Days = DateTime.DaysInMonth(CalCulateddate.Year, CalCulateddate.Month);
                                if (intMonthDayno > Days)
                                    CalCulateddate = CalCulateddate.AddMonths(1);
                            }

                            if (recurInfo.endType == 3)
                            {
                                if (i < intNoOfRecurrence && DateTime.Compare(recurInfo.endTime, CalCulateddate) < 0)
                                    break;
                                else if (i + 1 == intNoOfRecurrence && DateTime.Compare(CalCulateddate, recurInfo.endTime) < 0)
                                    break;
                            }

                            //strMonthlyDay += Convert.ToString(CalCulateddate.Month.ToString() + "/" + intMonthDayno + "/" + CalCulateddate.Year.ToString());

                            startDatetime = new DateTime(CalCulateddate.Year, CalCulateddate.Month, intMonthDayno, starthour, startmin, 0);
                            startdate = startDatetime.ToShortDateString() + " " + conftime;
                            DateTime.TryParse(startdate, out startDatetime);
                            if (i == 0)
                            {
                                DateTime dtGMT = startDatetime;
                                timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                                recurParams = dtGMT.ToString(); //ZD 103500
                            }
                            sRecDt.RecDatetime = startDatetime;
                            sRecDt.Duration = recurInfo.duration;
                            sRecDt.SetDuration = setDuration;
                            sRecDt.TearDuration = tearDuration;
                            sRecDt.Timezone = recurInfo.timezoneid;
                            sRecDt.StartHour = startHour;
                            sRecDt.StartMin = startMin;
                            sRecDt.StartSet = startSet;

                            recDates.Add(sRecDt);

                            CalCulateddate = CalCulateddate.AddMonths(intMonthGap);
                        }
                    }
                    RecurPatternTxt = RecurPatternTxt.Replace("{6}", recurInfo.dayno.ToString()); //FB 2231
                    RecurPattern = RecurPattern.Replace("{6}", recurInfo.dayno.ToString()); //ZD 103500

                }
                else if (recurInfo.subType == 2)
                {
                    String intMonthWeekDay = "", subRecurPatt = ""; //FB 2231
                    DateTime createdDate = DateTime.Today.Date;
                    Int32 intMonthGap = Int32.MinValue;
                    int currentYear = createdDate.Year;
                    int currentMonth = createdDate.Month;
                    int currentDay = createdDate.Day;
                    int intCurrentDay = Convert.ToInt32(currentDay);

                    if (recurInfo.endType != 2)
                        intNoOfRecurrence = maxRecurInstance;
                    else
                    {
                        if (recurInfo.occurrence > maxRecurInstance)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                            intNoOfRecurrence = recurInfo.occurrence;
                    }

                    intMonthGap = recurInfo.gap; // INPUT GAP                	Convert.ToInt32(row["MonthGap"].ToString());
                    intMonthWeekDay = recurInfo.days; // INPUT DAY OF WEEK   row["MonthWeekDay"].ToString();
                    int MonthWeekDayNoflag = recurInfo.dayno;   // INPUT Nth DAY
                    string[] steps = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                    int mon = intMonthGap; //4
                    string wk = "";

                    if (Int32.Parse(intMonthWeekDay) > 3)
                        wk = steps[Int32.Parse(intMonthWeekDay) - 4]; //Monday

                    int stp = System.Convert.ToInt32(MonthWeekDayNoflag) - 1; //2 
                    int recNo = intNoOfRecurrence; //recursive number
                    DateTime[] actDate = new DateTime[recNo];
                    DateTime today = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    DateTime today2 = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    int curMon = today.Month;
                    int curYear = today.Year;

                    for (int k = 0; k < recNo; k++)
                    {

                        int j = 0;
                        DateTime[] dayCount = new DateTime[5];
                        int days = DateTime.DaysInMonth(curYear, curMon);

                        for (int i = 1; i <= days; i++)
                        {
                            if (Int32.Parse(intMonthWeekDay) == 1)
                            {
                                DateTime loop = new DateTime(curYear, curMon, i);

                                if (i < 5)
                                    dayCount[j++] = Convert.ToDateTime(curMon + "/" + i + "/" + curYear);

                                if (i == 5)
                                {
                                    dayCount[j++] = Convert.ToDateTime(curMon + "/" + days + "/" + curYear);
                                    break;
                                }
                            }
                            if (Int32.Parse(intMonthWeekDay) == 2)
                            {
                                if (j < 4)
                                {
                                    DateTime loop = new DateTime(curYear, curMon, i);
                                    string day = loop.DayOfWeek.ToString();
                                    if (day != "Sunday" && day != "Saturday")
                                    {
                                        dayCount[j++] = Convert.ToDateTime(curMon + "/" + i + "/" + curYear);
                                    }
                                }
                                if (j == 4)
                                {
                                    today2 = Convert.ToDateTime(curMon + "/" + days + "/" + curYear);
                                    while (true)
                                    {
                                        string day = today2.DayOfWeek.ToString();
                                        if (day != "Sunday" && day != "Saturday")
                                        {
                                            dayCount[4] = today2;
                                            i = days + 1;
                                            break;
                                        }
                                        today2 = today2.AddDays(-1);
                                    }
                                }

                            }
                            if (Int32.Parse(intMonthWeekDay) == 3)
                            {
                                if (j < 4)
                                {
                                    DateTime loop = new DateTime(curYear, curMon, i);
                                    string day = loop.DayOfWeek.ToString();
                                    if (day == "Sunday" || day == "Saturday")
                                    {
                                        dayCount[j++] = Convert.ToDateTime(curMon + "/" + i + "/" + curYear);
                                    }
                                }
                                if (j == 4)
                                {
                                    today2 = Convert.ToDateTime(curMon + "/" + days + "/" + curYear);
                                    while (true)
                                    {
                                        string day = today2.DayOfWeek.ToString();
                                        if (day == "Sunday" || day == "Saturday")
                                        {
                                            dayCount[4] = today2;
                                            i = days + 1;
                                            break;
                                        }
                                        today2 = today2.AddDays(-1);
                                    }
                                }
                            }
                            if (Int32.Parse(intMonthWeekDay) > 3)
                            {
                                DateTime loop = new DateTime(curYear, curMon, i);
                                string day = loop.DayOfWeek.ToString();
                                if (wk == day)
                                {
                                    dayCount[j++] = Convert.ToDateTime(curMon + "/" + i + "/" + curYear);
                                }
                            }

                        }

                        if (stp == 4 && dayCount[stp].ToString() != "1/1/0001 12:00:00 AM")
                        {
                            actDate[k] = dayCount[stp];
                        }
                        else if (stp == 4 && dayCount[stp - 1].ToString() != "")
                        {
                            actDate[k] = dayCount[stp - 1];
                        }
                        else if (dayCount[stp].ToString() != "")
                        {
                            actDate[k] = dayCount[stp];
                        }

                        if (recurInfo.endType == 3)
                        {
                            if (k < recNo && DateTime.Compare(recurInfo.endTime, actDate[k]) < 0)
                            {
                                //k = recNo; // TO EXIT OUTER LOOP
                                recNo = k; // COUNT OF GENERATED DATEs
                                break;

                            }
                            else if (k + 1 == recNo && DateTime.Compare(actDate[k], recurInfo.endTime) < 0)
                            {
                                //k = recNo; // TO EXIT OUTER LOOP
                                recNo = k; // COUNT OF GENERATED DATEs
                                break;
                            }
                        }

                        int intResult = DateTime.Compare(actDate[k], today.Date);  //FB 2366

                        if (intResult < 0)
                        {
                            k -= 1;
                            curMon += mon;
                        }
                        else
                            curMon += mon;

                        if (curMon > 12)
                        {
                            curMon -= 12;
                            curYear += 1;
                        }
                    }

                    for (int cnt = 0; cnt < recNo; cnt++)
                    {
                        startdate = actDate[cnt].ToShortDateString() + " " + conftime;
                        DateTime.TryParse(startdate, out startDatetime);
                        if (cnt == 0)
                        {
                            DateTime dtGMT = startDatetime;
                            timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                            recurParams = dtGMT.ToString(); //ZD 103500
                        }
                        sRecDt.RecDatetime = startDatetime;
                        sRecDt.Duration = recurInfo.duration;
                        sRecDt.SetDuration = setDuration;
                        sRecDt.TearDuration = tearDuration;
                        sRecDt.Timezone = recurInfo.timezoneid;
                        sRecDt.StartHour = startHour;
                        sRecDt.StartMin = startMin;
                        sRecDt.StartSet = startSet;

                        recDates.Add(sRecDt);

                        //StrMonthlyThe += ", " + actDate[cnt].Month.ToString() + "/" + actDate[cnt].Day.ToString() + "/" + actDate[cnt].Year.ToString();
                    }

                    //FB 2231 start
                    int intDays = 0;
                    subRecurPatt = "";

                    if (recurDayNo[recurInfo.dayno - 1] != null)
                        subRecurPatt = recurDayNo[recurInfo.dayno - 1].ToString();

                    Int32.TryParse(recurInfo.days, out intDays);
                    if (recurDays[intDays - 1] != null)
                        subRecurPatt += recurDays[intDays - 1].ToString();

                    RecurPatternTxt = RecurPatternTxt.Replace("{6}", subRecurPatt);
                    RecurPattern = RecurPattern.Replace("{6}", subRecurPatt); //ZD 103500
                }

                RecurPatternTxt = RecurPatternTxt.Replace("{1}", recurInfo.gap.ToString());
                RecurPattern = RecurPattern.Replace("{1}", recurInfo.gap.ToString()); //ZD 103500

                if (recurInfo.endType == 2) //FB 2231
                {
                    RecurPatternTxt = RecurPatternTxt.Replace("{3}", intNoOfRecurrence.ToString());
                    RecurPattern = RecurPattern.Replace("{3}", intNoOfRecurrence.ToString()); //ZD 103500
                }
                else if (recurInfo.endType == 3)
                {
                    RecurPatternTxt = RecurPatternTxt.Replace("{3}", recurInfo.endTime.ToString("MM/dd/yyyy"));
                    DateTime dtEnd = recurInfo.endTime;
                    timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtEnd);

                    recurParams = recurParams + "||" + dtEnd.ToString(); //ZD 103500
                }

                recurringText = RecurPattern + "||" + recurParams; //ZD 103500

                //FB 2231 end

            }
            catch (Exception e)
            {
                throw new Exception("GetMonthlyRecurDates: ", e);
            }
            return recDates;
        }
        #endregion

        #region GetYearlyRecurDates
        internal List<sRecurrDates> GetYearlyRecurDates()
        {
            try
            {
                //FB 2231 start
                Int32 intNoOfRecurrence = Int32.MinValue;
                //ZD 101837 //ZD 102783
                if (recurInfo.Conf != null && recurInfo.Conf.WebExConf == 1)
                {
                    if (maxRecurInstance > 50)
                        maxRecurInstance = 50;
                }      
                String subRecurPatt = "";
                String[] MonthName = { "January ", "February ", "March ", "April ", "May ", "June ", "July ",
                                       "August ", "September ", "October ", "November ", "December " }; //FB 2231

                if (recurInfo.subType == 1)
                {
                    endType1Pattern = "Occurs every {1} effective {2} from {4} for {5} hrs "; //ZD 100528
                    endType2Pattern = "Occurs every {1} effective {2} occurs {3} time(s) from {4} for {5} hrs"; //ZD 100528
                    endType3Pattern = "Occurs every {1} effective {2} until {3} from {4} for{5} hrs "; //ZD 100528
                }
                else if (recurInfo.subType == 2)
                {
                    endType1Pattern = "Occurs the {6} of {1} effective {2} from {4} for {5} hrs"; //ZD 100528
                    endType2Pattern = "Occurs the {6} of {1} effective {2} occurs {3} time(s) from {4} for {5} hrs"; //ZD 100528
                    endType3Pattern = "Occurs the {6} of {1} effective {2} until {3} from {4} for {5} hrs"; //ZD 100528
                }

                if (recurInfo.endType == 1)
                    RecurPatternTxt = endType1Pattern;
                else if (recurInfo.endType == 2)
                    RecurPatternTxt = endType2Pattern;
                else if (recurInfo.endType == 3)
                    RecurPatternTxt = endType3Pattern;

                //FB 2231 end
                RecurPattern = RecurPatternTxt; //ZD 103500
                SetTime();
                if (recurInfo.subType == 1)
                {
                    DateTime createdDate = DateTime.Today.Date;
                    DateTime CalCulateddsteMonth = DateTime.MinValue;
                    DateTime CalCulateddate = DateTime.MinValue;
                    Int32 intYearMonthDay = Int32.MinValue;
                    intYearMonthDay = Int32.Parse(recurInfo.days); // INPUT DAY OF MONTH        Convert.ToInt32(row["YearMonthDay"].ToString());

                    if (recurInfo.endType != 2)
                        intNoOfRecurrence = maxRecurInstance;
                    else
                    {
                        if (recurInfo.occurrence > maxRecurInstance)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                            intNoOfRecurrence = recurInfo.occurrence;
                    }

                    int MonthWeekDayNoflag = recurInfo.yearMonth; // INPUT SELECTED MONTH
                    createdDate = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    CalCulateddate = Convert.ToDateTime(MonthWeekDayNoflag + "/" + intYearMonthDay + "/" + createdDate.Year);
                    int S = 0;

                    for (int i = 0; i < intNoOfRecurrence; i++)
                    {
                        S++;
                        if (createdDate > CalCulateddate)
                        {

                            if (CalCulateddsteMonth == DateTime.MinValue)
                                CalCulateddsteMonth = CalCulateddate.AddYears(1);
                            else
                                CalCulateddsteMonth = CalCulateddsteMonth.AddYears(1);
                        }
                        else
                        {
                            if (CalCulateddsteMonth == DateTime.MinValue)
                                CalCulateddsteMonth = CalCulateddate;
                            else
                                CalCulateddsteMonth = CalCulateddsteMonth.AddYears(1);

                        }
                        if (recurInfo.endType == 3)
                        {
                            if (i < intNoOfRecurrence && DateTime.Compare(recurInfo.endTime, CalCulateddsteMonth) < 0)
                                break;
                            else if (i + 1 == intNoOfRecurrence && DateTime.Compare(CalCulateddsteMonth, recurInfo.endTime) < 0)
                                break;
                        }
                        //strYearEvery += ", " + Convert.ToString(CalCulateddsteMonth.Month.ToString() + "/" + CalCulateddsteMonth.Day.ToString() + "/" + CalCulateddsteMonth.Year.ToString());

                        startdate = CalCulateddsteMonth.ToShortDateString() + " " + conftime;
                        DateTime.TryParse(startdate, out startDatetime);
                        if (i == 0)
                        {
                            DateTime dtGMT = startDatetime;
                            timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                            recurParams = dtGMT.ToString(); //ZD 103500
                        }
                        sRecDt.RecDatetime = startDatetime;
                        sRecDt.Duration = recurInfo.duration;
                        sRecDt.SetDuration = setDuration;
                        sRecDt.TearDuration = tearDuration;
                        sRecDt.Timezone = recurInfo.timezoneid;
                        sRecDt.StartHour = startHour;
                        sRecDt.StartMin = startMin;
                        sRecDt.StartSet = startSet;

                        recDates.Add(sRecDt);
                    }

                    if (MonthName[recurInfo.yearMonth - 1] != null)//FB 2231
                    {
                        RecurPatternTxt = RecurPatternTxt.Replace("{1}", (MonthName[recurInfo.yearMonth - 1].ToString() + recurInfo.days).ToString());
                        RecurPattern = RecurPattern.Replace("{1}", (MonthName[recurInfo.yearMonth - 1].ToString() + recurInfo.days).ToString()); //ZD 103500

                    }
                }
                else if (recurInfo.subType == 2)
                {
                    String intYearMonthWeeekDay = "";
                    intYearMonthWeeekDay = recurInfo.days;  // INPUT DAY OF WEEK row["YearMonthWeekDay"].ToString();

                    if (recurInfo.endType != 2)
                        intNoOfRecurrence = maxRecurInstance;
                    else
                    {
                        if (recurInfo.occurrence > maxRecurInstance)
                            intNoOfRecurrence = maxRecurInstance;
                        else
                            intNoOfRecurrence = recurInfo.occurrence;
                    }

                    int YearMonthWeekDayNoflag = 0;
                    string[] steps = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                    YearMonthWeekDayNoflag = recurInfo.dayno; // INPUT Nth DAY
                    int mon = recurInfo.yearMonth;  // INPUT MONTH OF YEAR //4 
                    string wk = "";   // steps[Int32.Parse(intYearMonthWeeekDay) - 4]; //Monday

                    if (Int32.Parse(intYearMonthWeeekDay) > 3)
                        wk = steps[Int32.Parse(intYearMonthWeeekDay) - 4]; //Monday

                    int stp = System.Convert.ToInt32(YearMonthWeekDayNoflag) - 1; //2 
                    int recNo = System.Convert.ToInt32(intNoOfRecurrence); //recursive number
                    DateTime[] actDate = new DateTime[recNo];
                    DateTime today = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    DateTime today2 = recurInfo.startTime; // INPUT START DATE    Convert.ToDateTime(row["StartDate"]);
                    int curMon = today.Month;
                    int curYear = today.Year;

                    if (mon < curMon)
                        curYear += 1;

                    for (int k = 0; k < recNo; k++)
                    {
                        int j = 0;
                        DateTime[] dayCount = new DateTime[5];
                        int days = DateTime.DaysInMonth(curYear, mon);

                        for (int i = 1; i <= days; i++)
                        {
                            if (Int32.Parse(intYearMonthWeeekDay) == 1)
                            {
                                DateTime loop = new DateTime(curYear, mon, i);

                                if (i < 5)
                                    dayCount[j++] = Convert.ToDateTime(mon + "/" + i + "/" + curYear);

                                if (i == 5)
                                {
                                    dayCount[j++] = Convert.ToDateTime(mon + "/" + days + "/" + curYear);
                                    break;

                                }
                            }
                            if (Int32.Parse(intYearMonthWeeekDay) == 2)
                            {
                                if (j < 4)
                                {
                                    DateTime loop = new DateTime(curYear, mon, i);
                                    string day = loop.DayOfWeek.ToString();
                                    if (day != "Sunday" && day != "Saturday")
                                    {
                                        dayCount[j++] = Convert.ToDateTime(mon + "/" + i + "/" + curYear);
                                    }
                                }
                                if (j == 4)
                                {
                                    today2 = Convert.ToDateTime(mon + "/" + days + "/" + curYear);
                                    while (true)
                                    {
                                        string day = today2.DayOfWeek.ToString();
                                        if (day != "Sunday" && day != "Saturday")
                                        {
                                            dayCount[4] = today2;
                                            i = days + 1;
                                            break;
                                        }
                                        today2 = today2.AddDays(-1);
                                    }

                                }

                            }
                            if (Int32.Parse(intYearMonthWeeekDay) == 3)
                            {
                                if (j < 4)
                                {
                                    DateTime loop = new DateTime(curYear, mon, i);
                                    string day = loop.DayOfWeek.ToString();
                                    if (day == "Sunday" || day == "Saturday")
                                    {
                                        dayCount[j++] = Convert.ToDateTime(mon + "/" + i + "/" + curYear);
                                    }
                                }
                                if (j == 4)
                                {
                                    today2 = Convert.ToDateTime(mon + "/" + days + "/" + curYear);
                                    while (true)
                                    {
                                        string day = today2.DayOfWeek.ToString();
                                        if (day == "Sunday" || day == "Saturday")
                                        {
                                            dayCount[4] = today2;
                                            i = days + 1;
                                            break;
                                        }
                                        today2 = today2.AddDays(-1);
                                    }
                                }
                            }
                            if (Int32.Parse(intYearMonthWeeekDay) > 3)
                            {
                                DateTime loop = new DateTime(curYear, mon, i);
                                string day = loop.DayOfWeek.ToString();
                                if (wk == day)
                                {
                                    dayCount[j++] = Convert.ToDateTime(mon + "/" + i + "/" + curYear);
                                }
                            }
                        }
                        if (stp == 4 && dayCount[stp].ToString() != "1/1/0001 12:00:00 AM")
                        {
                            actDate[k] = dayCount[stp];
                        }
                        else if (stp == 4 && dayCount[stp - 1].ToString() != "")
                        {
                            actDate[k] = dayCount[stp - 1];
                        }
                        else if (dayCount[stp].ToString() != "")
                        {
                            actDate[k] = dayCount[stp];
                        }

                        if (recurInfo.endType == 3)
                        {
                            if (k < recNo && DateTime.Compare(recurInfo.endTime, actDate[k]) < 0)
                            {
                                //k = recNo; // TO EXIT OUTER LOOP
                                recNo = k; // COUNT OF GENERATED DATEs
                                break;

                            }
                            else if (k + 1 == recNo && DateTime.Compare(actDate[k], recurInfo.endTime) < 0)
                            {
                                //recNo = recNo * 2; if need to extend the occuance when enddate > default occurance 
                                //k = recNo; // TO EXIT OUTER LOOP
                                recNo = k; // COUNT OF GENERATED DATEs
                                break;
                            }
                        }
                        int intResult = DateTime.Compare(actDate[k], DateTime.Today);

                        if (intResult < 0)
                            k -= 1;

                        curYear += 1;
                    }

                    for (int cnt = 0; cnt < recNo; cnt++)
                    {
                        startdate = actDate[cnt].ToShortDateString() + " " + conftime;
                        DateTime.TryParse(startdate, out startDatetime);
                        if (cnt == 0)
                        {
                            DateTime dtGMT = startDatetime;
                            timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtGMT);
                            recurParams = dtGMT.ToString(); //ZD 103500
                        }
                        sRecDt.RecDatetime = startDatetime;
                        //sRecDt.RecDatetime = new DateTime(actDate[cnt].Year, actDate[cnt].Month, actDate[cnt].Day, startHour, startMin, 0);
                        
                        sRecDt.Duration = recurInfo.duration;
                        sRecDt.SetDuration = setDuration;
                        sRecDt.TearDuration = tearDuration;
                        sRecDt.Timezone = recurInfo.timezoneid;
                        sRecDt.StartHour = startHour;
                        sRecDt.StartMin = startMin;
                        sRecDt.StartSet = startSet;

                        recDates.Add(sRecDt);
                        //strYearEveryThe += ", " + actDate[cnt].Month.ToString() + "/" + actDate[cnt].Day.ToString() + "/" + actDate[cnt].Year.ToString();
                    }

                    //FB 2231 start
                    int intDays = 0;

                    if(recurDayNo[recurInfo.dayno - 1] != null)
                        subRecurPatt = recurDayNo[recurInfo.dayno - 1].ToString();
                    
                    Int32.TryParse(recurInfo.days, out intDays);
                    if (recurDays[intDays - 1] != null)
                        subRecurPatt += recurDays[intDays - 1].ToString();

                    RecurPatternTxt = RecurPatternTxt.Replace("{6}", subRecurPatt);
                    RecurPattern = RecurPattern.Replace("{6}", subRecurPatt); //ZD 103500

                    if (MonthName[recurInfo.yearMonth - 1] != null)
                    {
                        RecurPatternTxt = RecurPatternTxt.Replace("{1}", MonthName[recurInfo.yearMonth - 1].ToString());
                        RecurPattern = RecurPattern.Replace("{1}", MonthName[recurInfo.yearMonth - 1].ToString()); //ZD 103500
                    }
                }

                if (recurInfo.endType == 2) //FB 2231
                {
                    RecurPatternTxt = RecurPatternTxt.Replace("{3}", intNoOfRecurrence.ToString());
                    RecurPattern = RecurPattern.Replace("{3}", intNoOfRecurrence.ToString()); //ZD 103500
                }
                else if (recurInfo.endType == 3)
                {
                    RecurPatternTxt = RecurPatternTxt.Replace("{3}", recurInfo.endTime.ToString("MM/dd/yyyy"));
                    DateTime dtEnd = recurInfo.endTime;
                    timeZone.changeToGMTTime(recurInfo.timezoneid, ref dtEnd);

                    recurParams = recurParams + "||" + dtEnd.ToString(); //ZD 103500
                }

                recurringText = RecurPattern + "||" + recurParams; //ZD 103500

                //FB 2231 end
            }
            catch (Exception e)
            {
                throw new Exception("GetYearlyRecurDates: ", e);
            }
            return recDates;
        }
        #endregion
    }
    #endregion
}
