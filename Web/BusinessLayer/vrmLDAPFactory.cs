//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using NHibernate;
using NHibernate.Criterion;
using log4net;
using System.DirectoryServices;
using myVRM.DataLayer;
using System.Threading;
using System.Linq;
using System.Xml.Linq;

namespace myVRM.BusinessLayer
{
    public class vrmLDAPFactory
    {
        private SystemDAO m_systemDAO;

        private static log4net.ILog m_log;
        private string m_configPath;
        private vrmDataObject m_obj;
        private userDAO m_userDAO;
        private ILDAPConfigDAO m_ILDAPConfigDAO;
        private DirectorySearcher dirSearcher;
        private SearchResultCollection m_objSearchResults;
        private DirectoryEntry dirEntry;
        private IUserDao m_IuserDao;
        private IInactiveUserDao m_IinactiveUserDao;
        private IVRMLdapUserDao m_IUsrldapDao;
        private orgDAO m_orgDAO;
        private IOrgDAO m_IOrgDAO;
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private IUserRolesDao m_IUserRolesDao;//ZD 102156
        private ILDAPGroupsDAO m_ILDAPGroupsDAO; //ZD 102156
        private IUserTemplateDao m_ItempDao;//ZD 102156

        private DirectoryEntry DE;
        private myVRMLDAP LDAP;
        //private myVRMDirectoryEntryCollection myVRMde;
        //private int _itemCnt = 0;

        private Queue m_objLldapUserList = null;
        //private int m_lpdatSyncState = -1;
        //System.Threading.Thread m_objLDAPSyncThreadFromLDAP = null;
        //System.Threading.Thread m_objLDAPSyncThreadTOmyVRMDB = null;
        int m_iLDAPTotalUserCount = 0;
        int m_iTotalUserNotImported = 0;
        string m_strLDAPErrorInfo = null;
        private myVRMException myvrmEx; //FB 1881

        /*enum LDAP_SYNC_STATE
        {
            LDAP_SYNC_INITIALIZE = 0,
            LDAP_SYNC_STARTED = 1,
            LDAP_SYNC_RUNNING = 2,
            LDAP_SYNC_FINISHED = 3,
            LDAP_SYNC_ERROR = 4,
            LDAP_SYNC_NOTSTARTED = 5
        }*/

        public vrmLDAPFactory(ref vrmDataObject obj)
        {
            try
            {
                m_obj = obj;
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_systemDAO = new SystemDAO(m_configPath, m_log);
                m_orgDAO = new orgDAO(m_configPath, m_log); //organization module
                m_IOrgDAO = m_orgDAO.GetOrgDao();
                m_IOrgSettingsDAO = m_orgDAO.GetOrgSettingsDao();
                m_userDAO = new userDAO(obj.ConfigPath, obj.log);
                m_ILDAPConfigDAO = m_orgDAO.GetLDAPConfigDao();
                dirEntry = new DirectoryEntry();
                dirSearcher = new DirectorySearcher(dirEntry);

                m_IuserDao = m_userDAO.GetUserDao();
                m_IinactiveUserDao = m_userDAO.GetInactiveUserDao();
                m_IUsrldapDao = m_userDAO.GetLDAPUserDao();
                m_IUserRolesDao = m_userDAO.GetUserRolesDao(); //ZD 102156
                m_ILDAPGroupsDAO = m_orgDAO.GetLDAPGroupDao(); //ZD 102156
                m_ItempDao = m_userDAO.GetUserTemplateDao();//ZD 102156
                

                m_objLldapUserList = new Queue();
                //m_objLDAPSyncThreadFromLDAP = new Thread(SyncFroLDAPThreadProc);
                //m_objLDAPSyncThreadTOmyVRMDB = new Thread(SyncTomyVRMDBThreadProc);
                //m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_NOTSTARTED;
                m_strLDAPErrorInfo = "";
                this.LDAP = new ActiveDirectory();
            }
            catch (Exception e)
            {
                //FB 1881 start
                m_log.Error("sytemException in LDAP Factory " + e.Message);
                //obj.outXml = myVRMException.toXml("sytemException in LDAP Factory " + e.Message);
                obj.outXml = "";
                //FB 1881 end
                return;
            }
        }

        public bool BuildLDAPFormyVRM(ref vrmDataObject obj)
        {
            string outXML = "";
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                // parse the inxml 
                // get the user name and password
                string _domain = xd.SelectSingleNode("//login/domain").InnerXml.Trim();
                string _userName = xd.SelectSingleNode("//login/userName").InnerXml.Trim();
                string _password = xd.SelectSingleNode("//login/userPassword").InnerXml.Trim();


                LDAP.UserName = _userName;
                LDAP.Password = _password;
                LDAP.HostName = _domain;

                LDAP.SetNamingContext();

                DE = LDAP.GetRootObject();
                outXML += "<LDAPResult>";
                outXML += GetChildrens(DE);
                outXML += "</LDAPResult>";
                obj.outXml = outXML;
                return true;
            }
            catch (Exception ex)
            {
                //FB 1881 start
                m_log.Error(ex.Message);
                //obj.outXml = myVRMException.toXml(ex.Message);
                obj.outXml = "";
                //FB 1881 end
                return false;
            }

        }

        public string GetChildrens(DirectoryEntry de)
        {
            string outXML = "";
            myVRMDirectoryEntryCollection children;
            try
            {

                ArrayList arrProperty = new ArrayList();
                arrProperty.Add("givenname");
                arrProperty.Add("sn");
                arrProperty.Add("name");
                arrProperty.Add("mail");
                arrProperty.Add("cn");
                arrProperty.Add("countrycode");
                arrProperty.Add("samaccountname");
                arrProperty.Add("whenchanged");
                arrProperty.Add("whencreated");
                arrProperty.Add("telephonenumber");
                arrProperty.Add("distinguishedName");
                this.LDAP.AddChildrens(de);
                if (LDAP.Entrycoll.Count > 0)
                    children = LDAP.Entrycoll;
                else
                    children = new myVRMDirectoryEntryCollection();
                outXML += "<childrens>";
                for (int i = 0; i < children.Count; i++)
                {
                    outXML += "<children>";
                    try
                    {
                        myVRMDirectoryEntry entry = (myVRMDirectoryEntry)children[i];
                        outXML += "<tagid>" + entry.DE.NativeGuid.ToString() + "</tagid>";
                        outXML += "<parent>" + entry.DE.Parent.NativeGuid.ToString() + "</parent>";
                        outXML += "<objectClass>" + entry.ObjectClass + "</objectClass>";
                        outXML += "<objectName>" + entry.ObjectName + "</objectName>";
                        if (entry.DE.Properties.Count > 0)
                        {
                            //outXML += "<properties>";
                            foreach (string proprty in arrProperty)
                            {
                                //outXML += "<property>";
                                //outXML += "<name>" + proprty + "</name>";
                                outXML += "<" + proprty + ">" + LDAP.getProperties(entry.DE, proprty) + "</" + proprty + ">";
                                //outXML += "<value>" + LDAP.getProperties(entry.DE,proprty) + "</value>";
                                //outXML += "</property>";
                            }
                            //outXML += "</properties>";
                        }
                    }
                    catch (Exception)
                    {
                    }
                    outXML += "</children>";
                }
                outXML += "</childrens>";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outXML;
        }

        public bool AuthenticateUser(vrmLDAPConfig ldapSettings)//(string userName,string userPassword)
        {
            string uName = "";//LDAP SYNC
            try
            {
                uName = ldapSettings.serverLogin;
                if (ldapSettings.domainPrefix != "")
                    uName = ldapSettings.domainPrefix + uName;//LDAP SYNC

                // specify the ldap server info
                string ldapPath = "LDAP://" + ldapSettings.serveraddress + ":" + ldapSettings.connPort;
                this.dirEntry.Path = ldapPath;

                //check to see if domain prefix exists
                if (ldapSettings.domainPrefix != null)
                    ldapSettings.serverLogin = ldapSettings.domainPrefix + ldapSettings.serverLogin;// prefix the domain before the username 

                // specify ldap settings
                this.dirEntry.Username = ldapSettings.serverLogin.Trim();
                this.dirEntry.Password = ldapSettings.serverPassword.Trim();
                // set auth type to "None"
                this.dirEntry.AuthenticationType = AuthenticationTypes.None;

                if (!string.IsNullOrWhiteSpace(ldapSettings.SearchFilter))
                    this.dirSearcher.Filter = ("(&" + ldapSettings.SearchFilter + "(" + ldapSettings.LoginKey + "=" + uName.Trim() + "))");//("(" + ldapSettings.LoginKey + "=" + ldapSettings.login.Trim() + ")");//LDAP SYNC
                //this.dirSearcher.Filter = ldapSettings.SearchFilter;								
                dirSearcher.PropertiesToLoad.Clear();
                this.dirSearcher.PropertiesToLoad.Add(ldapSettings.LoginKey);

                // run the search. check the result count to see there is only one record.
                // if multiple records are returned then username entered should not be considered as valid.
                //SearchResultCollection objSearchResults = this.dirSearcher.FindAll();
                SearchResult objSearchResult = this.dirSearcher.FindOne();
                if (objSearchResult == null)
                {
                    //FB 1881 start
                    //m_obj.outXml = myVRMException.toXml("Connection successful. However, no matching ldap records were found.");
                    myvrmEx = new myVRMException(505);
                    m_obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return true;
                }
                else
                {
                    m_obj.outXml = "<success>Connection Successful.</success>";
                    return true;
                }
            }
            catch (Exception e)
            {
                //FB 1881 start
                m_log.Error(e.Message);
                //m_obj.outXml = myVRMException.toXml(e.Message);
                m_obj.outXml = "";
                //FB 1881 end
                return false;
            }
            finally
            {
                dirSearcher.Dispose();
            }
        }

        public bool AuthLdapUser(ref vrmDataObject obj)
        {
            try
            {

                // Load the string into xml parser and retrieve the tag values.
                int organizationID = 0;
                XmlDocument xd = new XmlDocument();
                XmlNode node = null;
                xd.LoadXml(obj.inXml);

                // parse the inxml 
                // get the user name and password
                string userName = xd.SelectSingleNode("//login/userName").InnerXml.Trim();
                string userPassword = xd.SelectSingleNode("//login/userPassword").InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (userName != "admin")
                {
                    /*vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                    ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                    if (ldapSettings.serveraddress.Trim().Length < 1)
                    {
                        //FB 1881 start
                        m_log.Error("Error in fetching ldap settings from database.");
                        obj.outXml = "";
                        //FB 1881 end
                        return false;
                    }*/

                    //ZD 101525 Starts
                    vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                    //ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                    if (organizationID <= 11)
                        organizationID = 11;

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", organizationID));
                    List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                        ldapSettings = ldapSettingsList[0];

                    if (ldapSettings == null || ldapSettings.serveraddress.Trim().Length < 1)
                        return false;
                    //ZD 101525 End

                    // Replace the server login/pwd with the info of the user who is trying to authenticate
                    ldapSettings.login = userName;
                    ldapSettings.serverLogin = userName;
                    ldapSettings.password = userPassword;
                    ldapSettings.serverPassword = ldapSettings.password;
                    bool ret = AuthenticateUser(ldapSettings);
                    if (!ret)
                    {
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml("Authentication Failed! No matching ldap records were found.");
                        myvrmEx = new myVRMException(506);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }
                }
                obj.outXml = "<success>1</success>";
                return true;

            }
            catch (Exception e)
            {
                m_log.Error(e.Message); //FB 1881
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
        }

        private void SyncFroLDAPThreadProc(object objLdapSettings)
        {
            try
            {
                vrmLDAPConfig ldapSettings = (vrmLDAPConfig)objLdapSettings;
                if (ldapSettings.serveraddress.Trim().Length < 1)
                {
                    myvrmEx = new myVRMException(502);
                    m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                }

                ldapSettings.serverLogin = ldapSettings.login;
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapSettings.serverPassword = crypto.decrypt(ldapSettings.password);

                bool ret = AuthenticateUser(ldapSettings);
                if (!ret)
                {
                    myvrmEx = new myVRMException(503);
                    m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                }
                else
                {
                    dirSearcher = new DirectorySearcher(dirEntry);
                    dirSearcher.PropertiesToLoad.Clear();
                    this.dirSearcher.PropertiesToLoad.Add("givenname");
                    this.dirSearcher.PropertiesToLoad.Add("sn");
                    this.dirSearcher.PropertiesToLoad.Add("name");
                    this.dirSearcher.PropertiesToLoad.Add("mail");
                    this.dirSearcher.PropertiesToLoad.Add("cn");
                    this.dirSearcher.PropertiesToLoad.Add("countrycode");
                    this.dirSearcher.PropertiesToLoad.Add("samaccountname");
                    this.dirSearcher.PropertiesToLoad.Add("whenchanged");
                    this.dirSearcher.PropertiesToLoad.Add("whencreated");
                    this.dirSearcher.PropertiesToLoad.Add("telephonenumber");
                    this.dirSearcher.PropertiesToLoad.Add("IsDeleted");
                    //// over-ride the search filter as we need to fetch all users and not only one user.
                    this.dirSearcher.Filter = ldapSettings.SearchFilter; //("(objectClass=person)");
                    this.dirSearcher.PageSize = 250; // 1k is the max in AD server.				
                    //m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_RUNNING;
                    //// run the search and get the results in 
                    m_objSearchResults = this.dirSearcher.FindAll();
                    m_iLDAPTotalUserCount = m_objSearchResults.Count;
                    ResultPropertyCollection propCollection;

                    int iLoopCount = 0;

                    // cycle thru the search records
                    while (iLoopCount < m_iLDAPTotalUserCount) //FB 1040
                    {
                        vrmLDAPUser ldapUser = new vrmLDAPUser();
                        propCollection = m_objSearchResults[iLoopCount].Properties;
                        try
                        {
                            foreach (string key in propCollection.PropertyNames)
                            {
                                foreach (Object collection in propCollection[key])
                                {
                                    if (key == "givenname")
                                    {
                                        ldapUser.FirstName = collection.ToString();
                                        ldapUser.FirstName = ReplaceInvalidChars(ldapUser.FirstName);
                                        break;
                                    }
                                    else if (key == "sn")
                                    {
                                        ldapUser.LastName = collection.ToString();
                                        ldapUser.LastName = ReplaceInvalidChars(ldapUser.LastName);
                                        break;
                                    }
                                    else if (key == "mail")
                                    {
                                        ldapUser.Email = collection.ToString();
                                        ldapUser.Email = ReplaceInvalidChars(ldapUser.Email);
                                        break;
                                    }
                                    else if (key == "telephonenumber")
                                    {
                                        ldapUser.Telephone = collection.ToString();
                                        ldapUser.Telephone = ReplaceInvalidChars(ldapUser.Telephone);
                                        break;
                                    }
                                    else if (key == "whencreated")
                                    {
                                        ldapUser.whencreated = DateTime.Parse(collection.ToString());
                                        ldapUser.whencreated = ldapUser.whencreated;
                                        break;
                                    }
                                    //else if (key == "cn") //ZD 101525
                                    else if (key == "samaccountname")
                                    {
                                        ldapUser.Login = collection.ToString();
                                        ldapUser.Login = ReplaceInvalidChars(ldapUser.Login);
                                        break;
                                    }
                                }
                            }
                            //if (ldapUser.Email != null)
                            m_objLldapUserList.Enqueue(ldapUser);
                        }
                        catch (Exception e)
                        {
                            m_log.Error("Error fetching user record. Message = " + e.Message); //FB 1881
                            myvrmEx = new myVRMException(200);
                            m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                            // keep continuing fetching other users
                        }
                        iLoopCount++;//FB 1040
                    }
                }
                return;
            }
            catch (Exception e)
            {
                m_strLDAPErrorInfo = e.Message;
                return;
            }
            finally
            {
                this.dirSearcher.Dispose();
                m_objSearchResults.Dispose();
            }
        }

        //Commented - Removed thread concept.
        /*private void SyncTomyVRMDBThreadProc()
        {
            try
            {
                // check if the LDAP sync is running, If running then wait till the Sync Finishes
                while ((m_objLDAPSyncThreadFromLDAP.IsAlive == true)
                    && ((m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_ERROR)
                    || (m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED))
                    )
                {
                    Thread.Sleep(5000);
                    m_iTotalUserNotImported = 0;
                    if (!SyncUniqueLDAPUsersTomyVRMDB())
                    {
                        //FB 1881 start
                        //m_strLDAPErrorInfo = " Error in comparing and loading users in ldap holding area.";
                        myvrmEx = new myVRMException(504);
                        m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                        //FB 1881 end
                        continue;
                    }
                }
                if (m_lpdatSyncState != (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED)
                {
                    m_strLDAPErrorInfo = "ERROR";
                }

                m_iLDAPTotalUserCount = 0;

                // Update the LDAP sync Time even if the sync was not successful.
                // Update the sync time if the Sync was successful. FB 649
                if (m_lpdatSyncState == (int)LDAP_SYNC_STATE.LDAP_SYNC_FINISHED)
                {
                    UpdateSyncTime();
                }
                m_objLldapUserList.Clear();
                // Reset the LDAP Sync State.
                m_lpdatSyncState = (int)LDAP_SYNC_STATE.LDAP_SYNC_NOTSTARTED;
            }
            catch (Exception ex)
            {
                m_log.Error("Error occured in SyncTomyVRMDBThreadProc", ex);
                m_strLDAPErrorInfo = ex.Message;
                return;
            }
        }*/

        private bool SyncUniqueLDAPUsersTomyVRMDB(vrmLDAPConfig ldapSettings)//Ldap Synchronisation
        {
            try
            {
                // check to see if the users are already there in the database.
                // if user records already exists then skip adding the user. 				
                // get all the user email list from the active,inactive,ldap tables				
                //Queue uniqueNewUserList = new Queue();
                if (m_objLldapUserList.Count < 1)
                    return true; // when there are no records in the queue, it is not an error, so returing true.

                vrmLDAPUser newUser = new vrmLDAPUser();

                List<string> strLoginList = new List<string>();

                bool bIsValidUser = false;
                while (m_objLldapUserList.Count > 0)
                {
                    bIsValidUser = false;

                    // Release the CPU for sometime.
                    //Thread.Sleep(100);
                    newUser = (vrmLDAPUser)m_objLldapUserList.Dequeue();

                    // check if Login or email are not blank
                    if ((newUser.Login != null)
                        && (newUser.Email != null)
                        && (newUser.Login.Length > 0)
                        && (newUser.Email.Length > 0)
                        && (newUser.Email.IndexOf("@") > 0))
                        bIsValidUser = true;
                    if ((bIsValidUser))
                    {
                        //ZD 101525
                        if (!strLoginList.Contains(newUser.Login))
                            strLoginList.Add(newUser.Login);

                        if (!UpdateLdapUser(newUser))//check if any user exists with given login and email, if yes then update the user
                        {
                            m_iTotalUserNotImported++;
                            continue;
                        }
                    }
                    else
                        m_iTotalUserNotImported++;
                }
                //ZD 101525
                if (strLoginList.Count > 0) //check if any user exists with given login and email, if yes then block other users.
                    BlockNonLdapUsers(strLoginList, ldapSettings.OrgId);

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Exception Occured in SyncUniqueLDAPUsersTomyVRMDB method", e);
                return false;
            }

        }

        /// <summary>
        /// Synchronize with ldap server
        /// </summary>
        /// <returns></returns>
        public bool SyncWithLdap(ref vrmDataObject obj)//FB 2462
        {
            vrmLDAPConfig ldapSettings = null;
            DateTime syncTime = DateTime.Now;
            //ZD 102156 Starts
            string lastName = "", firstName = "", prntNode = "", emailid = "";
            string samaccountname = "";
            List<string> groupArray = null;
            List<vrmLDAPGroups> ldapGroupList = null;
            List<vrmUserRoles> roleList = null;
            List<vrmUser> userUpdateList = null;
            vrmUser usrDetails = null;
            List<ICriterion> criUsrList = null;
            List<vrmUser> usrList = null;
            SearchResult objSearchResult = null;
            List<vrmUserTemplate> templateList = null;
            int roleid = 0;
            bool isUserValid = false;

            //ZD 102156 End
            try
            {
                ldapSettings = new vrmLDAPConfig();

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmLDAPConfig> LDAPList = m_ILDAPConfigDAO.GetByCriteria(criterionList);

                if (LDAPList != null && LDAPList.Count > 0)
                {
                    for (int i = 0; i < LDAPList.Count; i++)
                    {
                        userUpdateList = new List<vrmUser>();//ZD 102156
                        ldapSettings = LDAPList[i];
                        DateTime.TryParse(syncTime.ToShortDateString() + " " + ldapSettings.SyncTime.ToShortTimeString(), out syncTime);

                        if (ldapSettings.serveraddress.Trim().Length < 1)
                            return false;

						//ZD 102156 Starts
                        if (DateTime.Now.Subtract(syncTime).TotalMinutes <= 10)
                        {
                            userUpdateList = new List<vrmUser>();
                            usrDetails = new vrmUser();
                            dirEntry = new DirectoryEntry();
                            criUsrList = new List<ICriterion>();

                            dirEntry.Username = ldapSettings.domainPrefix + ldapSettings.login;//"myvrm\\jvideo";
                            cryptography.Crypto crypto = new cryptography.Crypto();
                            dirEntry.Password = crypto.decrypt(ldapSettings.password);
                            dirEntry.Path = "LDAP://" + ldapSettings.serveraddress + ":" + ldapSettings.port;
                            if (ldapSettings.TenantOption == 4)//102732
                                dirEntry.Path = "GC://" + ldapSettings.serveraddress + ":" + ldapSettings.connPort;
                            dirEntry.AuthenticationType = AuthenticationTypes.None;

                            criUsrList.Add(Expression.Eq("companyId", ldapSettings.OrgId));
                            criUsrList.Add(Expression.Eq("Deleted", 0));
                            criUsrList.Add(Expression.Eq("Audioaddon", "0"));
                            criUsrList.Add(Restrictions.Not(Restrictions.Eq("Login", string.Empty)));
                            usrList = m_IuserDao.GetByCriteria(criUsrList);

                            if (usrList == null || usrList.Count <= 0)
                                continue;

                            for (int u = 0; u < usrList.Count; u++)
                            {
                                try
                                {
                                    firstName = ""; lastName = ""; emailid = "";
                                    samaccountname = ""; groupArray = new List<string>();
                                    objSearchResult = null;
                                    isUserValid = false;

                                    usrDetails = usrList[u];

                                    dirSearcher = new DirectorySearcher(dirEntry);
                                    dirSearcher.Filter = ("(samaccountname=" + usrDetails.Login + ")");
                                    objSearchResult = this.dirSearcher.FindOne();
                                    prntNode = ldapSettings.domainPrefix.ToLower().Replace("\\", "");
                                    if (objSearchResult != null)
                                    {
                                        firstName = ""; lastName = ""; samaccountname = ""; emailid = "";
                                        roleList = new List<vrmUserRoles>();
                                        ldapGroupList = new List<vrmLDAPGroups>();

                                        try
                                        {
                                            if (objSearchResult.Properties["samaccountname"][0] != null)
                                                samaccountname = objSearchResult.Properties["samaccountname"][0].ToString();
                                        }
                                        catch (Exception)
                                        { }

                                        try
                                        {
                                            if (objSearchResult.Properties["givenname"][0] != null)
                                                firstName = objSearchResult.Properties["givenname"][0].ToString();
                                        }
                                        catch (Exception)
                                        { }

                                        try
                                        {
                                            if (objSearchResult.Properties["sn"][0] != null)
                                                lastName = objSearchResult.Properties["sn"][0].ToString();
                                        }
                                        catch (Exception)
                                        { }

                                        try
                                        {
                                            if (objSearchResult.Properties["mail"][0] != null)
                                                emailid = objSearchResult.Properties["mail"][0].ToString();
                                        }
                                        catch (Exception)
                                        { }

                                        try
                                        {
                                            if (objSearchResult.Properties["memberOf"] != null)
                                                groupArray = objSearchResult.Properties["memberOf"].Cast<string>().ToArray().Select(grp => grp.Split(',')[0].Remove(0, 3)).ToList();
                                        }
                                        catch (Exception)
                                        { }

                                        if (!string.IsNullOrEmpty(firstName))
                                            usrDetails.FirstName = firstName;
                                        if (!string.IsNullOrEmpty(lastName))
                                            usrDetails.LastName = lastName;
                                        if (!string.IsNullOrEmpty(samaccountname))
                                            usrDetails.Login = samaccountname;
                                        if (!string.IsNullOrEmpty(emailid))
                                            usrDetails.Email = emailid;

                                        if (groupArray != null && groupArray.Count > 0)
                                        {
                                            criterionList = new List<ICriterion>();
                                            criterionList.Add(Expression.In("GroupName", groupArray));
                                            criterionList.Add(Expression.Eq("OrgId", usrDetails.companyId));
                                            criterionList.Add(Expression.Gt("RoleOrTemplateId", 0));
                                            ldapGroupList = m_ILDAPGroupsDAO.GetByCriteria(criterionList, true);

                                            if (ldapGroupList != null && ldapGroupList.Count > 0) //Get the total no.of groups using above criterion list
                                            {
                                                List<vrmLDAPGroups> vrmLDAPGroups = null;
                                                List<int> TemplateIds = null;
                                                List<int> RoleIds = null;
                                                templateList = new List<vrmUserTemplate>();
                                                criterionList = new List<ICriterion>();
                                                vrmLDAPGroups = ldapGroupList.Where(g => g.Origin == 1).ToList();//Origin - 0 Roles,1-Templates
                                                if (vrmLDAPGroups != null && vrmLDAPGroups.Count > 0)
                                                    TemplateIds = vrmLDAPGroups.Select(ldgrp => ldgrp.RoleOrTemplateId).ToList();//Get the list of Group which is assigned by template
                                                List<vrmUserRoles> userRolesort = null;
                                                if (TemplateIds != null && TemplateIds.Count > 0)// Get the one or more - template assign group
                                                {
                                                    criterionList.Add(Expression.In("id", TemplateIds));
                                                    criterionList.Add(Expression.Eq("deleted", 0));
                                                    criterionList.Add(Expression.Eq("companyId", ldapSettings.OrgId));
                                                    templateList = m_ItempDao.GetByCriteria(criterionList);
                                                    if (templateList != null && templateList.Count > 0)
                                                        RoleIds = templateList.Select(t => t.roleId).ToList();//Get the role id of from the user template table for the temaplate assign group

                                                    criterionList = new List<ICriterion>();
                                                    criterionList.Add(Expression.In("roleID", RoleIds));
                                                    roleList = m_IUserRolesDao.GetByCriteria(criterionList, true);
                                                }
                                                else
                                                {
                                                    RoleIds = ldapGroupList.Select(l => l.RoleOrTemplateId).ToList();
                                                    criterionList.Add(Expression.In("roleID", RoleIds));
                                                    roleList = m_IUserRolesDao.GetByCriteria(criterionList, true);
                                                }

                                                if (roleList != null && roleList.Count > 0)
                                                {
                                                    userRolesort = new List<vrmUserRoles>();
                                                    if (roleList.Where(r => r.crossaccess == 1).Any())// Get prefered RoleID using this criteria
                                                        userRolesort = roleList.OrderByDescending(r => r.level).OrderByDescending(r => r.crossaccess).ThenBy(r => r.createType).ThenBy(r => r.roleName).ToList();
                                                    else
                                                        userRolesort = roleList.OrderByDescending(r => r.level).ThenBy(r => r.createType).ThenBy(r => r.roleName).ToList();
                                                    roleid = userRolesort[0].roleID;

                                                    usrDetails.Admin = userRolesort[0].level;
                                                    usrDetails.MenuMask = userRolesort[0].roleMenuMask;
                                                    if (usrDetails.roleID != userRolesort[0].roleID)
                                                    {
                                                        usrDetails.LandingPageLink = "";
                                                        usrDetails.LandingPageTitle = "";
                                                    }
                                                    usrDetails.roleID = userRolesort[0].roleID;

                                                    isUserValid = true;
                                                }

                                                if (userRolesort == null || userRolesort.Count <= 0)
                                                    isUserValid = false;
                                            }
                                            else if (usrDetails.userid != 11)
                                                isUserValid = false;
                                        }
                                        else if (usrDetails.userid != 11)
                                            isUserValid = false;
                                    }
                                    else if (usrDetails.userid != 11)
                                        isUserValid = false;

                                    if (!userUpdateList.Contains(usrDetails))
                                    {
                                        if (!isUserValid && usrDetails.userid != 11)
                                        {
                                            usrDetails.LockStatus = 3;
                                            usrDetails.lockCntTrns = 1;
                                        }
                                        userUpdateList.Add(usrDetails);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    m_log.Error("Exception Occured in SyncWithLdap method", ex);
                                    continue;
                                }
                            }
                            if (userUpdateList != null && userUpdateList.Count > 0)
                            {
                                m_IuserDao.SaveOrUpdateList(userUpdateList);
                                UpdateSyncTime(ldapSettings);
                            }
                        }
						//ZD 102156 End
						//ZD 102156 Commented Starts
                        /*
                        if (DateTime.Now.Subtract(syncTime).TotalMinutes <= 10)
                        {
                            SyncFroLDAPThreadProc(ldapSettings);
                            if (!SyncUniqueLDAPUsersTomyVRMDB(ldapSettings))
                            {
                                myvrmEx = new myVRMException(504);
                                m_strLDAPErrorInfo = myvrmEx.FetchErrorMsg();
                            }
                            m_iLDAPTotalUserCount = 0;
                            UpdateSyncTime(ldapSettings);
                            m_objLldapUserList.Clear();

                            if (!ret)
                            {
                                obj.outXml = "<success>Operation succesful.</success>";
                                //return false;//ZD101525
                            }
                        }
                         */
						//ZD 102156 Commented End
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Exception Occured in SyncWithLdap method", e);
                return false;
            }
        }

        public bool UpdateSyncTime(vrmLDAPConfig updLDAPCnfg)
        {
            try
            {
                //vrmLDAPConfig updLDAPCnfg = new vrmLDAPConfig();
                //updLDAPCnfg = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);
                updLDAPCnfg.SyncTime = DateTime.UtcNow;
                m_ILDAPConfigDAO.clearFetch();
                m_ILDAPConfigDAO.SaveOrUpdate(updLDAPCnfg);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in SyncUniqueLDAPUsersTomyVRMDB method", ex);
                return false;
            }

        }

        public bool InsertLdapUser(vrmLDAPUser ldapUser)
        {
            int userId = 0;
            try
            {
                if (ldapUser != null)
                {
                    m_IUsrldapDao.addProjection(Projections.Max("userid"));
                    IList maxId = m_IUsrldapDao.GetObjectByCriteria(new List<ICriterion>());
                    if (maxId[0] != null)
                        userId = ((int)maxId[0]) + 1;
                    else
                        userId = 1;
                    m_ILDAPConfigDAO.clearProjection();

                    ldapUser.userid = userId;
                    if (ldapUser.whencreated == null)
                        ldapUser.whencreated = DateTime.UtcNow;
                    ldapUser.newUser = 1;
                    m_IUsrldapDao.Save(ldapUser);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in InsertLdapUser method", ex);
                return false;
            }

        }

        public bool UpdateLdapUser(vrmLDAPUser ldapUser)// For LDAP SYnchronisation
        {
            int errCount = 0;
            List<vrmUser> results = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            ICriterion criterium = null;

            try
            {
                if (ldapUser != null)
                {

                    m_IuserDao = m_userDAO.GetUserDao();

                    criterium = Expression.Eq("Login", ldapUser.Login.ToUpper()).IgnoreCase();
                    if (ldapUser.Email.Trim().Length > 0)
                        criterium = Expression.Or(criterium, Expression.Like("Email", ldapUser.Email.Trim().ToUpper()).IgnoreCase());

                    criterionList.Add(criterium);
                    results = m_IuserDao.GetByCriteria(criterionList);
                    errCount = results.Count;

                    if (errCount == 1)
                    {
                        if (results[0] != null)
                        {
                            if (!string.IsNullOrEmpty(ldapUser.FirstName))
                                results[0].FirstName = ldapUser.FirstName;
                            if (!string.IsNullOrEmpty(ldapUser.LastName))
                                results[0].LastName = ldapUser.LastName;
                            //results[0].Login = ldapUser.Login;
                            results[0].Email = ldapUser.Email;
                            if (!string.IsNullOrEmpty(ldapUser.Telephone))
                                results[0].Telephone = ldapUser.Telephone;

                            m_IuserDao.SaveOrUpdateList(results);
                            return true;
                        }

                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in UpdateLdapUser method", ex);
                return false;
            }

        }

        private bool BlockNonLdapUsers(List<string> strLoginList, int orgId)// For Block the Non-LDAP UserList
        {
            List<vrmUser> results = null;
            List<vrmUser> BlockedList = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                m_IuserDao = m_userDAO.GetUserDao();//Why do we need this?
                criterionList.Add(Expression.Eq("companyId", orgId));
                criterionList.Add(Expression.Eq("Deleted", 0));
                criterionList.Add(Expression.Eq("Audioaddon", "0"));
                criterionList.Add(Expression.Not(Expression.In("Login", strLoginList)));
                criterionList.Add(Restrictions.Not(Restrictions.Eq("Login", string.Empty)));
                //criterionList.Add(Expression.IsNotEmpty("Login"));
                //criterionList.Add(Expression.IsNotNull("Login"));
                results = m_IuserDao.GetByCriteria(criterionList, true);
                if (results != null && results.Count > 0)
                {
                    BlockedList = results.Select((data, i) => { data.LockStatus = 3; data.lockCntTrns = 1; return data; }).ToList();
                    m_IuserDao.SaveOrUpdateList(BlockedList);
                }
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in BlockNonLdapUsers method", ex);
                return false;
            }

        }

        public bool DoesUserExists(string strLogin, string strEmailId)
        {

            try
            {
                // check the active users list
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium;
                m_IuserDao = m_userDAO.GetUserDao();
                m_IinactiveUserDao = m_userDAO.GetInactiveUserDao();
                m_IUsrldapDao = m_userDAO.GetLDAPUserDao();

                criterium = Expression.Eq("Login", strLogin.ToUpper()).IgnoreCase();
                if (strEmailId.Trim().Length > 0)
                    criterium = Expression.Or(criterium, Expression.Like("Email", strEmailId.ToUpper()).IgnoreCase());

                criterionList.Add(criterium);
                List<vrmUser> results = m_IuserDao.GetByCriteria(criterionList);
                int errCount = results.Count;
                if (errCount > 0) // If any user found with given email id or login then return true.
                    return true;

                List<vrmInactiveUser> inactiveResults = m_IinactiveUserDao.GetByCriteria(criterionList);
                errCount = inactiveResults.Count;
                if (errCount > 0) // If any user found with given email id or login then return true.
                    return true;

                List<vrmLDAPUser> LDAPResults = m_IUsrldapDao.GetByCriteria(criterionList);
                errCount = LDAPResults.Count;
                if (errCount > 0) // If any user found with given email id or login then return true.
                    return true;

                return false;

            }
            catch (Exception ex)
            {
                m_log.Error("Exception Occured in DoesUserExists method", ex);
                return false;
            }
        }

        /// <summary>
        /// Test the LDAP server connectivity.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool TestLDAPConnection(ref vrmDataObject obj)
        {
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();

                // extract the LDAP server info from the InXML
                ldapSettings.login = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
                ldapSettings.serverLogin = ldapSettings.login;
                ldapSettings.password = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                ldapSettings.serverPassword = ldapSettings.password;
                ldapSettings.serveraddress = xd.SelectSingleNode("//login/ServerAddress").InnerXml.Trim();
                ldapSettings.connPort = int.Parse(xd.SelectSingleNode("//login/Port").InnerXml.Trim());
                ldapSettings.sessionTimeout = int.Parse(xd.SelectSingleNode("//login/ConnectionTimeout").InnerXml.Trim());
                ldapSettings.LoginKey = xd.SelectSingleNode("//login/LoginKey").InnerXml.Trim();
                ldapSettings.SearchFilter = xd.SelectSingleNode("//login/SearchFilter").InnerXml.Trim();
                ldapSettings.domainPrefix = xd.SelectSingleNode("//login/LDAPPrefix").InnerXml.Trim();

                // Replace "&amp;" with "&" as ldap search filters need that. 
                ldapSettings.SearchFilter = RevertBackTheInvalidChars(ldapSettings.SearchFilter);

                // Test LDAP connection
                bool ret = AuthenticateUser(ldapSettings);
                if (!ret)
                {
                    //this.errMsg = ldap.errMsg;
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                //FB 1881 start
                m_log.Error("LDAP connection failed.");
                //obj.outXml = myVRMException.toXml("LDAP connection failed.");
                obj.outXml = "";
                //FB 1881 end
                return false;
            }
        }

        /// <summary>
        /// Test the Exchange server connectivity.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        public bool TestExchangeConnection(ref vrmDataObject obj)
        {
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                ldapSettings.login = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
                ldapSettings.password = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                ldapSettings.serveraddress = xd.SelectSingleNode("//login/ServerURL").InnerXml.Trim();
                //ldapSettings.connPort = int.Parse(xd.SelectSingleNode("//login/Port").InnerXml.Trim());

                // test ldap connection
                bool ret = AuthenticateUser(ldapSettings);
                if (!ret)
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("LDAP connection failed.");
                    myvrmEx = new myVRMException(501);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Exception Occured in TestExchangeConnection method", e);
                return false;
            }
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        public string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\'", "&apos;;");
            return (input);
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        public string RevertBackTheInvalidChars(string input)
        {
            input = input.Replace("&lt;", "<");
            input = input.Replace("&gt;", ">");
            input = input.Replace("&amp;", "&");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&apos;", "\'");
            return (input);
        }


        public Boolean GetLDAPUsers(ref vrmDataObject obj)
        {
            string outXML = "";
            //SearchResultCollection objSearchResults = null;
            string SearchFilter = "";
            int organizationID = 0;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                // Database settings
                //ZD 101525 Starts
                vrmLDAPConfig ldapSettings = new vrmLDAPConfig();
                //ldapSettings = m_ILDAPConfigDAO.GetLDAPConfigDetails(m_configPath);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapSettings = ldapSettingsList[0];

                if (ldapSettings == null || ldapSettings.serveraddress.Trim().Length < 1)
                    return false;
                //ZD 101525 End


                ldapSettings.serverLogin = ldapSettings.login;
                //decrypt pwd
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapSettings.serverPassword = crypto.decrypt(ldapSettings.password);
                node = xd.SelectSingleNode("//login/searchFor");
                if (node != null)
                    SearchFilter = node.InnerText.Trim();

                if (!SearchFilter.Equals(""))
                    ldapSettings.SearchFilter = SearchFilter;

                outXML = "<GetLDAPUsers>";
                outXML += "<ServerDetails>";
                outXML += "<ServerAddress>" + ldapSettings.serveraddress.ToString() + "</ServerAddress>";
                outXML += "<Username>" + ldapSettings.login.ToString() + "</Username>";
                outXML += "<Password>" + ldapSettings.serverPassword.ToString() + "</Password>";
                outXML += "<Domain>" + ldapSettings.domainPrefix.ToString() + "</Domain>";
                outXML += "<SearchFilter>" + ldapSettings.SearchFilter.Trim() + "</SearchFilter>"; //FB 2096
                outXML += "<LoginKey>" + ldapSettings.LoginKey.Trim() + "</LoginKey>"; //FB 2096
                outXML += "<AuthenticationType>" + ldapSettings.AuthType + "</AuthenticationType>"; //FB 2993 LDAP 
                outXML += "</ServerDetails>";

                // Authenticate user 
                //bool ret = AuthenticateUser(ldapSettings);
                //if (!ret)
                //{
                //    outXML += "<error>";
                //    outXML += "<message>User Authentication failed.</message>";
                //    outXML += "<errorCode>200</errorCode>";
                //    outXML += "</error>";
                //}
                //else
                //{

                //    //// specify the search string
                //    dirSearcher = new DirectorySearcher(dirEntry);
                //    dirSearcher.PropertiesToLoad.Clear();
                //    this.dirSearcher.PropertiesToLoad.Add("givenname");
                //    this.dirSearcher.PropertiesToLoad.Add("sn");
                //    this.dirSearcher.PropertiesToLoad.Add("name");
                //    this.dirSearcher.PropertiesToLoad.Add("mail");
                //    this.dirSearcher.PropertiesToLoad.Add("cn");
                //    this.dirSearcher.PropertiesToLoad.Add("countrycode");
                //    this.dirSearcher.PropertiesToLoad.Add("samaccountname");
                //    this.dirSearcher.PropertiesToLoad.Add("whenchanged");
                //    this.dirSearcher.PropertiesToLoad.Add("whencreated");
                //    this.dirSearcher.PropertiesToLoad.Add("telephonenumber");

                //    //if (ldapSettings.SearchFilter != "")
                //    //    ldapSettings.SearchFilter = "(&" + ldapSettings.SearchFilter + "(UserAccountControl:1.2.840.113556.1.4.803:=2))";
                //    //else
                //    //    ldapSettings.SearchFilter = "(UserAccountControl:1.2.840.113556.1.4.803:=2)";

                //    //// over-ride the search filter as we need to fetch all users and not only one user.
                //    this.dirSearcher.Filter = ldapSettings.SearchFilter; //("(objectClass=person)");
                //    this.dirSearcher.PageSize = 500; // 1k is the max in AD server.				
                //    //// run the search and get the results in 
                //    objSearchResults = this.dirSearcher.FindAll();
                //    m_iLDAPTotalUserCount = objSearchResults.Count;
                //    ResultPropertyCollection propCollection;
                //    int iLoopCount = 0;

                //    outXML += "<SearchResults>";
                //    outXML += "<LDAPUsers>";

                //    // cycle thru the search records
                //    while (iLoopCount < m_iLDAPTotalUserCount) //FB 1040
                //    {
                //        outXML += "<LDAPUserInfo>";
                //        //Release the CPU
                //        Thread.Sleep(10);
                //        propCollection = objSearchResults[iLoopCount].Properties;
                //        try
                //        {
                //            foreach (string key in propCollection.PropertyNames)
                //            {
                //                foreach (Object collection in propCollection[key])
                //                {
                //                    switch (key.Trim())
                //                    {
                //                        case "cn":
                //                        case "givenname":
                //                        case "sn":
                //                        case "name":
                //                        case "mail":
                //                        case "telephonenumber":
                //                        case "whencreated":
                //                        case "whenchanged":
                //                            outXML += "<" + key.ToString().Trim() + ">" + ReplaceInvalidChars(collection.ToString().Trim()) + "</" + key.ToString().Trim() + ">";
                //                            break;
                //                    }
                //                }
                //            }
                //        }
                //        catch (Exception e)
                //        {
                //            // keep continuing fetching other users
                //        }
                //        outXML += "</LDAPUserInfo>";
                //        iLoopCount++;
                //    }
                //    outXML += "</LDAPUsers>";
                //    outXML += "<SearchFilter>" + dirSearcher.Filter.ToString() + "</SearchFilter>";
                //    outXML += "<ResultCount>" + (iLoopCount-1).ToString() + "</ResultCount>";
                //    outXML += "</SearchResults>";
                //}
                outXML += "</GetLDAPUsers>";
                obj.outXml = outXML;
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);//FB 1881
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
            finally
            {
                // free all the resources used by the objects
                //this.dirSearcher.Dispose();
                //objSearchResults.Dispose();
            }
        }

        //ZD 101525 Start
        public bool GetOrgLDAP(ref vrmDataObject obj)
        {
            string outXML = "";
            int OrgID = 0;
            List<ICriterion> criterionList = null;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/OrgID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out OrgID);


                vrmLDAPConfig ldapSettings = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", OrgID));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapSettings = ldapSettingsList[0];

                //decrypt pwd
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapSettings.serverPassword = crypto.decrypt(ldapSettings.password);

                if (ldapSettings != null)
                {

                    outXML = "<GetOrgLDAP>";
                    outXML += "<ServerDetails>";
                    outXML += "<ServerAddress>" + ldapSettings.serveraddress.ToString() + "</ServerAddress>";
                    outXML += "<Username>" + ldapSettings.login.ToString() + "</Username>";
                    outXML += "<Password>" + ldapSettings.serverPassword.ToString() + "</Password>";
                    outXML += "<Domain>" + ldapSettings.domainPrefix.ToString() + "</Domain>";
                    outXML += "<SearchFilter>" + ldapSettings.SearchFilter.Trim() + "</SearchFilter>";
                    outXML += "<LoginKey>" + ldapSettings.LoginKey.Trim() + "</LoginKey>";
                    outXML += "<AuthenticationType>" + ldapSettings.AuthType + "</AuthenticationType>";
                    outXML += "<portNo>" + ldapSettings.port + "</portNo>";
                    outXML += "<connectionTimeout>" + ldapSettings.timeout + "</connectionTimeout>";
                    outXML += "<synctime>" + ldapSettings.SyncTime.ToString("hh:mm tt") + "</synctime>";
                    outXML += "<scheduler>";
                    outXML += "<Time>" + ldapSettings.scheduleTime.ToString("hh:mm tt") + "</Time>";
                    outXML += "<Days>" + ldapSettings.scheduleDays + "</Days>";
                    outXML += "</scheduler>";
                    outXML += "<TenantOption>" + ldapSettings.TenantOption + "</TenantOption>";// AD groups change
                    outXML += "</ServerDetails>";

                    outXML += "</GetOrgLDAP>";
                }
                obj.outXml = outXML;
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            finally
            {
                // free all the resources used by the objects
                //this.dirSearcher.Dispose();
                //objSearchResults.Dispose();
            }
        }
        //ZD 101525 End
    }

    public class myVRMLDAP
    {
        // Fields
        private string _defaultNamingContext = "";
        private string _hostname = "";
        private string _password = "";
        private string _username = "";
        private DirectoryEntry de;
        private myVRMDirectoryEntryCollection _entrycoll;
        public myVRMDirectoryEntryCollection Entrycoll
        {
            get { return _entrycoll; }
            set { _entrycoll = value; }
        }

        // Methods
        [Description("Returns a collection of Directory Entry Objects, that are the children of the passed object")]
        public virtual void AddChildrens(DirectoryEntry de)
        {
            try
            {
                foreach (DirectoryEntry entry in de.Children)
                {
                    try
                    {
                        myVRMDirectoryEntry myVRMDE = this.newDEitem(entry);
                        if (_entrycoll == null)
                            _entrycoll = new myVRMDirectoryEntryCollection();
                        _entrycoll.Add(myVRMDE);
                        AddChildrens(myVRMDE.DE);
                        continue;
                    }
                    catch
                    {
                        throw new ApplicationException("AddChildrens: Unable to add directory item to collection: " + entry.Name);
                    }
                }
            }
            catch
            {
                throw new ApplicationException("AddChildrens: Too many items in this collection");
            }
            int cnt = _entrycoll.Count;
        }

        [Description("Returns the property of an Directory Object")]
        public virtual string getProperties(DirectoryEntry objADObject, string objectType)
        {
            string str = "";
            int prptyCnt = 0;
            try
            {
                prptyCnt = objADObject.Properties[objectType].Count;
                if (prptyCnt > 0)
                {
                    for (int i = 0; i < prptyCnt; i++)
                    {
                        if (str == "")
                            str = objADObject.Properties[objectType][i].ToString();
                        else
                            str += "||" + objADObject.Properties[objectType][i].ToString();
                    }
                }
                return str;
                //str = "";
            }
            catch
            {
                str = "";
                throw new ApplicationException("getProperties : Error getting property for : " + objectType);
            }
            //return str;
        }

        [Description("Returns the root LDAP Object")]
        public virtual DirectoryEntry GetRootObject()
        {
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            return this.de;
        }

        [Description("Returns the root LDAP Object")]
        public virtual myVRMDirectoryEntryCollection GetDirectoryEntryCollection()
        {
            myVRMDirectoryEntryCollection deColl = new myVRMDirectoryEntryCollection();
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            DirectorySearcher dsr = new DirectorySearcher(this.de);
            dsr.Filter = ("(cn=User)");
            SearchResultCollection objSrchResults = dsr.FindAll();
            foreach (SearchResult srchRslt in objSrchResults)
            {
                deColl.Add(srchRslt.GetDirectoryEntry());
            }
            return deColl;
        }

        public virtual string n()
        {
            _entrycoll = new myVRMDirectoryEntryCollection();
            return "oLDAP";
        }

        private myVRMDirectoryEntry newDEitem(DirectoryEntry de)
        {
            string[] strArray = null;
            char[] separator = "=".ToCharArray();
            try
            {
                strArray = de.Name.Split(separator, 2);
            }
            catch
            {
                strArray[0] = "";
            }
            myVRMDirectoryEntry entry = new myVRMDirectoryEntry();
            entry.DE = de;
            entry.ObjectClass = strArray[0].ToLower();
            entry.ObjectName = de.Name;
            return entry;
        }

        [Description("Sets the default naming context")]
        public virtual void SetNamingContext()
        {
            this._defaultNamingContext = "";
        }

        // Properties
        [Description("Set the hostname")]
        public string HostName
        {
            set
            {
                this._hostname = value;
            }
        }

        [Description("Set the password to logon with Optional")]
        public string Password
        {
            set
            {
                this._password = value;
            }
        }

        [Description("Set the username to logon with Optional")]
        public string UserName
        {
            set
            {
                this._username = value;
            }
        }
    }

    public class myVRMDirectoryEntryCollection : ArrayList
    {
        // Methods
        public myVRMDirectoryEntryCollection()
        { }
    }

    public class myVRMDirectoryEntry
    {
        // Fields
        private DirectoryEntry _de;
        private string _objclass;
        private string _objname;

        // Properties
        public DirectoryEntry DE
        {
            get
            {
                return this._de;
            }
            set
            {
                this._de = value;
            }
        }

        public string ObjectClass
        {
            get
            {
                return this._objclass;
            }
            set
            {
                this._objclass = value;
            }
        }

        public string ObjectName
        {
            get
            {
                return this._objname;
            }
            set
            {
                this._objname = value;
            }
        }
    }

    public class NDS : myVRMLDAP
    {
        // Methods
        public override string n()
        {
            return "NDS";
        }
    }

    public class ActiveDirectory : myVRMLDAP
    {
        // Fields
        //private string _defaultNamingContext;

        // Methods
        public override string n()
        {
            return "AD";
        }

        public override void SetNamingContext()
        {
            base.SetNamingContext();
        }
    }
}
