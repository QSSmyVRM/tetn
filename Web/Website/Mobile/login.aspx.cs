﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100866 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Microsoft.Win32;
using System.Management;
using System.Management.Instrumentation;
using System.Text;//ZD 101911


public partial class Droid_login : System.Web.UI.Page
{
        /* Login management - Codes changed for FB 1820 */
        /*SetApplicationVariables - method removed as it is not used anywhere */

        #region Private Data Members
        /*

        /// <summary>
        /// Declaring Data Members
        /// </summary>
        const String loginPage = "../en/genlogin.aspx";
        const String loginPageTitle = "VRM - Video Conferencing Made Simple !";
        const String company_logo = "../en/image/company-logo/SiteLogo.jpg";
        const String mainTopImg = "../en/image/vrmtopleft.GIF";
        const String bgColor = "#FFEBCE";
        const String popUpbgColor = "#FFEBCE";

        protected Boolean wizard_enable = false;
        protected String ind = "";
        protected String WHO_USE = "";
        protected Boolean blockNSFF = false;
        protected Boolean corBroswer = true;
        protected System.Web.UI.HtmlControls.HtmlGenericControl CompanyTagLine;
        */

        //protected System.Web.UI.HtmlControls.HtmlInputText username;
        //protected System.Web.UI.HtmlControls.HtmlInputPassword pwdid;
        //protected System.Web.UI.WebControls.Label errLabel;

        MyVRMNet.LoginManagement loginMgmt = null;
        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log = null;

        /*
        int versionNo = 0;
        public String companyInfo = "";
        String language = "en"; //FB 1830
        protected System.Web.UI.WebControls.CheckBox RememberMe;//FB 2009
        */

        bool isloginFailed = false; //FB 2027 GetHome
        string macID = "";
        #endregion

        //public string mailextn = "";//FB 1943

        # region Constructor
        /// <summary>
        /// Public Constructor 
        /// </summary>
        public Droid_login()
        {
            obj = new myVRMNet.NETFunctions();
            loginMgmt = new MyVRMNet.LoginManagement();
            log = new ns_Logger.Logger();

        }
        #endregion

        #region Page Load Event handler
        /// <summary>
        /// Page Load Event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //FB 1633 start
                //loginMgmt.SetSitePaths(); 
                //loginMgmt.SetSiteThemes();
                //FB 1633 end

                //mailextn = loginMgmt.mailextn;//FB 1943
                BindProcessorDetails();//ZD 101031
                if (!IsPostBack)
                {                    
                    errLabel.Text = "";

                    if (Request.QueryString["m"] == "1")
                    {
                        //ZD 101031 - Start
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();                         
                                             
                        if (Request.Cookies["VRMuser"]["mmacid"] != null)
                        {                            
                            Request.Cookies["VRMuser"]["mmacid"] = null;
                            Request.Cookies["VRMuser"]["mact"] = "";
                            Request.Cookies["VRMuser"]["mpwd"] = "";
                        }

                        //ZD 101031 - End
                        
                        errLabel.Text = "You have successfully logged out of myVRM.";
                        errLabel.Visible = true;
                    }

                    if (loginMgmt.errMessage.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(loginMgmt.errMessage);
                        errLabel.Visible = true;
                    }
                    //CompanyTagLine.InnerText = loginMgmt.companyTagline;
                }
                
                RemembermeLogin();

                //loginMgmt.HeaderASP();
                //loginMgmt.readaspconfigASP();
                //loginMgmt.cookiedectASP();  Remember me
                
                /*
                if (File.Exists(Application["SchemaPath"].ToString() + "\\company.ifo"))
                {
                    companyInfo = File.ReadAllText(Application["SchemaPath"].ToString() + "\\company.ifo");
                }
                if (Session["versionNo"] != null)
                {
                    Int32.TryParse(Session["versionNo"].ToString().Trim(), out versionNo);
                }

                if (Session["browser"] != null)
                {
                    if (Session["browser"].ToString().Trim().ToLower() == "internet explorer")
                    {
                        if (versionNo > 5.0)
                        {
                            corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "netscape")
                    {
                        if (versionNo > 5)
                        {
                            if (blockNSFF)
                                corBroswer = false;
                            else
                                corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "firefox")
                    {
                        if (versionNo >= 1)
                        {
                            if (blockNSFF)
                                corBroswer = false;
                            else
                                corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "opera")
                    {
                        corBroswer = true;
                    }
                }

                if (Session["wizard_enable"] != null)
                    if (Session["wizard_enable"].ToString().Equals("1"))
                        wizard_enable = true;

                if (Session["WHO_USE"] != null)
                    WHO_USE = Session["WHO_USE"].ToString();
                
                //FB 1628 START
                if (Request.QueryString["id"] != null && Request.QueryString["req"] != null && Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["id"].ToString() != "" && Request.QueryString["req"].ToString() != "" && Request.QueryString["tp"].ToString() != "")
                    {
                        try
                        {
                            String idStr = Request.QueryString["id"].ToString();
                            String reqStr = Request.QueryString["req"].ToString();
                            String tpStr = Request.QueryString["tp"].ToString();

                            if (Session["req"] == null && Session["id"] == null)
                            {
                                Session.Add("req", Request.QueryString["req"]);
                                Session.Add("id", Request.QueryString["id"]);
                            }
                            else
                            {
                                Session["req"] = Request.QueryString["req"].ToString();
                                Session["id"] = Request.QueryString["id"].ToString();
                            }

                            Session["userID"] = "11";//Hard Corded to fetch the System Time Zone
                            Session["organizationID"] = "11";//default organization ID

                            Response.Redirect("ResponseConference.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "&t=hf");
                        }
                        catch (System.Threading.ThreadAbortException) { }

                    }
                }

                //FB 1628 END

                RemembermeLogin();

                GetQueryStrings();
                */
            }
            catch (Exception ex)
            {
                log.Trace("Page Load: "+ ex.Message);
            }
        }

        #endregion
        
        /*
        #region BrowswerDetect
        /// <summary>
        /// BrowswerDetect
        /// </summary>
        private void BrowserDetect()
        {
            try
            {
                Session["ip"] = Request.ServerVariables["REMOTE_ADDR"];
                Session["referrer"] = Request.ServerVariables["HTTP_REFERER"];
                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("WINDOWS"))
                    Session["os"] = "Windows";
                else if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MAC"))
                    Session["os"] = "Mac";
                else if (Request.ServerVariables["HTTP_USER_AGENT"].ToUpper().ToUpper().Contains("LINUX"))
                    Session["os"] = "Linux";
                else
                    Session["os"] = "Other";

                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MOZILLA"))
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session["browser"] = "Internet Explorer";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("NETSCAPE"))
                    {
                        Session["browser"] = "Netscape";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }

                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("FIREFOX"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("SAFARI"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("CHROME"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Session["browser"] == null)
                        Session["browser"] = "Undetectable";
                    if (Session["versionNo"] == null)
                        Session["versionNo"] = "Undetectable";
                    if (Session["version"] == null)
                        Session["version"] = "Undetectable";
                }
                else
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("OPERA"))
                    {
                        Session["browser"] = "Opera";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    else
                    {
                        Session["browser"] = "Not Mozilla";
                        Session["version"] = "Undetectable";
                    }
                }
            }
            catch (Exception ex)
            {
                //log.trace
                log.Trace(ex.StackTrace + ex.Message);

            }
        }
        #endregion
        */

        /*
        #region GetQueryStrings
        /// <summary>
        /// GetQueryStrings
        /// </summary>
        private void GetQueryStrings()
        {
            Int32 domain = 0;
            String user = "";
            try
            {
                Session.Add("c", "0");
                //Session.Add("guestlogin", ""); - This variable is not used in the system
                
                if (Request.QueryString["sct"] != null)
                    if (Request.QueryString["sct"] != "")
                    {
                        Session["c"] = Request.QueryString["sct"];
                    }
                ind = Session["c"].ToString();

                if (Application["CosignEnable"].ToString() == "1") //Check for cosign environment
                {
                    if (Request.ServerVariables["HTTP_REMOTE_USER"] != null) //FB 1820
                    {
                        loginMgmt.queyStraVal = Request.ServerVariables["HTTP_REMOTE_USER"].ToString();
                        
                        log.Trace("HTTP_REMOTE_USER: " + loginMgmt.queyStraVal);

                        loginMgmt.qStrPVal = "pwd";
                        loginMgmt.windwsAuth = true;
                        login();
                    }
                }
                else if (Application["ssoMode"].ToString().ToUpper() == "YES")    
                {
                    if (Request.ServerVariables["AUTH_USER"] != null)
                    {
                        domain = (Request.ServerVariables["AUTH_USER"]).ToLower().IndexOf("\\", 0) + 1;
                        user = ((Request.ServerVariables["AUTH_USER"]).ToLower()).Substring(domain);
                        
                        log.Trace("AUTH_USER: "+user); //FB 1820
                        loginMgmt.queyStraVal = user;
                        loginMgmt.qStrPVal = "pwd";
                        loginMgmt.windwsAuth = true;
                        login();
                    }
                }
                else
                {  
                    String orgId = "";
                    if (Request.QueryString["OrgID"] != null)
                    {
                        if (Request.QueryString["OrgID"].ToString() != "")
                        {
                            orgId = Request.QueryString["OrgID"].ToString();
                        }
                    }
                    //RSS Fix -- End

                    //FB 1830
                    if (Session["language"] != null)
                    {
                        if (Session["language"].ToString() != "")
                            language = Session["language"].ToString();
                    }

                    if (Request.QueryString["rss"] != null)//Rss Feed
                    {
                        if (Request.QueryString["rss"] != "")
                        {
                            if (Session["userID"].ToString() != "")
                            {
                                Response.Redirect("../" + language + "/ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgId);
                            }
                        }
                    }
                }

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("GetQueryStrings: " + ex.Message); //FB 1820
            }
        }

        #endregion
        */

        #region Submit Event Handlers

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                if (username.Value.ToString().Trim().IndexOf("@") <= 0)
                {
                    errLabel.Text = "Please enter Email"; // FB 2310
                    return;
                }
                else if (isloginFailed)//FB 2027 GetHome
                    return;

                loginMgmt = new MyVRMNet.LoginManagement();//ZD 101031 - Start
                loginMgmt.queyStraVal = username.Value.ToString().Trim();

                Session["LoginUserName"] = username.Value.ToString().Trim(); // FB 1725
                loginMgmt.qStrPVal = pwdid.Value.ToString().Trim();
                loginMgmt.qStrIpVal = macID;
                loginMgmt.windwsAuth = false;
                login();
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                log.Trace("btnSubmit_Click: "+ ex.Message);
            }
        }

        #endregion

        #region Login
        //MEthod modified for FB 1820
        private void login()
        {
            String outXML = "";
            XmlDocument errDoc = null; //ZD 100152
            try
            {
                errLabel.Text = "";
                errLabel.Visible = false;

                outXML = loginMgmt.GetHomeCommand();

                if (outXML != "")
                {
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errDoc = new XmlDocument();//ZD 100152
                        errDoc.LoadXml(outXML);
                        if (errDoc.SelectSingleNode("error/message") != null)
                        { 
                            errLabel.Text = errDoc.SelectSingleNode("error/message").InnerText.Trim();
                            errLabel.Visible = true;
                            isloginFailed = true;
                            return;
                        }
                    }
                }

                //ZD 101031 - Start
                if (Session["outXML"] != null) 
                    outXML = Session["outXML"].ToString();
                //ZD 101911 start
                if (!CheckMobileEnableforUser())
                {
                    return;
                }
                //ZD 101911 End
                if (RememberMe.Checked)
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(outXML);
                    if (xdoc.SelectNodes("enEmailID") != null)
                        Response.Cookies["VRMuser"]["mact"] = xdoc.SelectSingleNode("user/globalInfo/enEmailID").InnerText.Trim();
                    if (xdoc.SelectNodes("enUserPWD") != null)
                        Response.Cookies["VRMuser"]["mpwd"] = xdoc.SelectSingleNode("user/globalInfo/enUserPWD").InnerText.Trim();
                    if (xdoc.SelectNodes("enIPAddress") != null)
                        Response.Cookies["VRMuser"]["mmacid"] = xdoc.SelectSingleNode("user/globalInfo/enMacID").InnerText.Trim();
                    Response.Cookies["VRMuser"].Expires = DateTime.Now.AddDays(365);
                    Response.Cookies["VRMuser"].HttpOnly = true; // ZD 100263
                }

                outXML = HttpContext.Current.Session["outXML"].ToString();
                errDoc = new XmlDocument();

                errDoc.LoadXml(outXML);

                //ZD 101031 - End
                

                /*
				//FB 1830
                if(Session["language"] != null)
                {
                    if (Session["language"].ToString() != "")
                        language = Session["language"].ToString();
                }

                //RSS Fix-Single Sign On -- Start
                String orgID = "";
                if (Request.QueryString["OrgID"] != null)
                {
                    if (Request.QueryString["OrgID"].ToString() != "")
                    {
                        orgID = Request.QueryString["OrgID"].ToString();
                    }
                }
                //RSS Fix -- End

                if (Request.QueryString["rss"] != null)//Rss Feed
                {
                    if (Request.QueryString["rss"] != "")
                    {
                        if (Session["userID"].ToString() != "")
                        {
                            Response.Redirect("ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgID);
                        }
                    }
                }
                //Single Sign On -- End

                if (Session["NavLobby"] == null) // To avoid the alert message on refreshing lobby page
                    Session["NavLobby"] = "1";

                // //FB 2009
                if (RememberMe.Checked)
                {
                    Response.Cookies["VRMuser"]["act"] = UserName.Value.ToString().Trim(); ;
                    Response.Cookies["VRMuser"]["pwd"] = UserPassword.Value.ToString().Trim();
                    Response.Cookies["VRMuser"].Expires = DateTime.Now.AddDays(365);
                }
                //FB 2009

                //FB 1779 start
                if (Session["isExpressUser"] != null)
                {
                    if (Session["isExpressUser"].ToString() == "1")
                        Response.Redirect("~/" + language + "/ExpressConference.aspx?t=n"); //FB 1830
                }
                //FB 1779 end
				//FB 1830
                */

                Response.Redirect("~/Mobile/calendar.aspx");
                //Response.Redirect("~/" + language + "/lobby.aspx"); //LOBBY
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace("login: "+ex.Message);
            }
        }
        #endregion

        //ZD 101031 - Start

        #region Remember me
        /// <summary>
        /// ZD 101031
        /// </summary>
        private void RemembermeLogin()
        {
            try
            {
                if (Request.Cookies["VRMuser"] != null)
                {
                    if (Request.Cookies["VRMuser"].HasKeys)
                    {
                        if (Request.Cookies["VRMuser"]["mmacid"] == null)
                            return;

                        if (Request.Cookies["VRMuser"]["mact"] != null)
                            if (Request.Cookies["VRMuser"]["mact"] != "")
                                loginMgmt.qStrEnUnVal = Request.Cookies["VRMuser"]["mact"];

                        if (Request.Cookies["VRMuser"]["mpwd"] != null)
                            if (Request.Cookies["VRMuser"]["mpwd"] != "")
                                loginMgmt.qStrEnPwVal = Request.Cookies["VRMuser"]["mpwd"];

                        if (Request.Cookies["VRMuser"]["mmacid"] != null)
                            if (Request.Cookies["VRMuser"]["mmacid"] != "")
                                loginMgmt.qStrEnIpVal = Request.Cookies["VRMuser"]["mmacid"];
                        
                        loginMgmt.queyStraVal = username.Value.ToString().Trim();

                        Session["LoginUserName"] = loginMgmt.qStrEnUnVal;                         

                        loginMgmt.qStrIpVal = macID;
                        loginMgmt.windwsAuth = false;
                        login();                        
                    }
                }

            }
            catch (Exception ex)
            {

                log.Trace("Rememberme: " + ex.Message);
            }
        }

        #endregion

        #region Bind Processor

        public void BindProcessorDetails()
        {
            ManagementObjectSearcher objCS;
            try
            {
                objCS = null;
                objCS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter");

                foreach (ManagementObject objmgmt in objCS.Get())
                {
                    if (objmgmt.Properties["AdapterTypeID"].Value != null)//Ethernet 802.3
                        if (objmgmt.Properties["AdapterTypeID"].Value.ToString() == "0")
                            macID += "," + objmgmt.Properties["MACAddress"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        //ZD 101031 - End

    //ZD 101911 start

    #region CheckMobileEnableforUser
    /// <summary>
        /// CheckMobileEnableforUser
    /// </summary>
    /// <returns></returns>
    public bool CheckMobileEnableforUser()
    {
        String outXML = "";
        XmlDocument errDoc = null;
        try 
        {
            StringBuilder inXML = new StringBuilder();
            inXML.Append("<myVRM>");
            inXML.Append("<login>" + username.Value + "</login>");
            inXML.Append("<password>" + pwdid.Value + "</password>");
            inXML.Append("<IsAuthenticated>No</IsAuthenticated>");
            inXML.Append("<client>6</client>");//ZD 103346 //Based on userfactory files we need to send client tag value 6 and removed the validation for mobile license
            inXML.Append("</myVRM>");
            outXML = obj.CallMyVRMServer("CheckUserCredentials", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML != "")
            {
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errDoc = new XmlDocument();//ZD 100152
                    errDoc.LoadXml(outXML);
                    if (errDoc.SelectSingleNode("error/message") != null)
                    {
                        errLabel.Text = errDoc.SelectSingleNode("error/message").InnerText.Trim();
                        errLabel.Visible = true;
                        isloginFailed = true;
                        return false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
        return true;
    }
    #endregion
    //ZD 101911 End


}