/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxMenu;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts;
                

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

public partial class MasterChildReport : System.Web.UI.Page
{
    #region protected Members

    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDepartmentList;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMainIPValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRightMenuValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnOkValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSubmitValue;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSave;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnReportNames;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAllReports;
    
    protected System.Web.UI.HtmlControls.HtmlTableRow rptImgRow;
    protected System.Web.UI.HtmlControls.HtmlTableRow trDetails;
    
    protected System.Web.UI.HtmlControls.HtmlGenericControl MainDiv;

    protected System.Web.UI.WebControls.Label lblHeading;
    protected System.Web.UI.WebControls.Label errLabel;
    //protected System.Web.UI.WebControls.TextBox txtStartDate;
    //protected System.Web.UI.WebControls.TextBox txtEndDate;

    protected DevExpress.Web.ASPxPopupControl.ASPxPopupControl ASPxPopupControl1;
    protected DevExpress.Web.ASPxEditors.ASPxListBox OrgList;
    protected DevExpress.Web.ASPxEditors.ASPxListBox lstDepartment;
    protected DevExpress.Web.ASPxGridView.ASPxGridView MainGrid;
    protected DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter gridExport;
    protected DevExpress.Web.ASPxEditors.ASPxDateEdit startDateedit;
    protected DevExpress.Web.ASPxEditors.ASPxDateEdit endDateedit;
    protected DevExpress.Web.ASPxEditors.ASPxTextBox txtSaveReport;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnReportSelection; //FB 2808
    protected DevExpress.Web.ASPxEditors.ASPxListBox lstReportList; //FB 2808
	//FB 2593
    protected DevExpress.Web.ASPxEditors.ASPxListBox lstendPt;
    protected DevExpress.Web.ASPxEditors.ASPxListBox lstMCU; 
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAdmin; //FB 2593
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUList; //FB 2593
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEptList; //FB 2593
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomList; //FB 2593
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCDROption; //FB 2593
    protected DevExpress.Web.ASPxEditors.ASPxRadioButton rbConferenceCDR;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRptSave; //FB 2593
    protected DevExpress.Web.ASPxMenu.ASPxMenu MenuCDR; //FB 2593
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGroup; //FB 2965    
    protected DevExpress.Web.ASPxEditors.ASPxRadioButton rbVMRCDR;//ZD 101348
    protected DevExpress.Web.ASPxMenu.ASPxMenu MenuWorkOrder; // ZD 102835

    protected System.Web.UI.HtmlControls.HtmlTableCell tdCustomOptions; //ZD 104686
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCustomOptions; //ZD 104686
    
    #endregion

    #region protected enums

    protected enum MainMenuSelection
    {
        Conference = 1, Host, Participant, SheduledConference, ConfType, ConfBehavior, ConfTime, Duration
    }

    #endregion

    #region protected data members 

    private myVRMNet.NETFunctions obj;
    private ns_Logger.Logger log;

    protected Int32 orgId = 11;
    protected XmlDocument xmlDoc = null;
    protected DataSet ds = null;
    protected DataTable rptTable = new DataTable();
    protected DataTable filterRptTable = new DataTable(); //ZD 104686
    protected DataTable participantTable = new DataTable();
    protected DataTable subDetailTable = new DataTable(); //FB 2593
    protected DataTable childDetailTable = new DataTable(); //FB 2593
    protected DataTable workOrderTable; // ZD 102835
    protected String format = "dd/MM/yyyy";
    protected String dtformat = ""; //ZD 100958
    protected String tformat = "hh:mm tt";
    protected String tmzone = "";
    protected String organizationID = "";
    protected String xmlPath = ""; //FB 2808
    protected String strvalue = "";
    protected DataTable rmHostTable; // ZD 103216
    string oldConfID = ""; //ZD 104686

    #endregion

    #region Constructor 
    public MasterChildReport()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
        //imageUtilObj = new myVRMNet.ImageUtil(); 
    }

    #endregion

    #region Page_init
    /// <summary>
    /// Page_init
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_init(object sender, EventArgs e)
    {
        try
        {
            //FB 3049
            if (Session["organizationID"] == null)
                DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("genlogin.aspx"); 

            
            //ASPxGridView.RegisterBaseScript(Page);
            //FB 2808
            xmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en\\" +
                Session["RoomXmlPath"].ToString().Replace("Rooms/Room.xml", "Reports.xml");

            if (!IsPostBack)
                Session["ReportXML"] = null;

            if (Session["organizationID"] != null)
                Int32.TryParse(Session["organizationID"].ToString(), out orgId);

            //String strList = "11:1:Sales;11:2:Engineering;11:3:larry;12:4:New Dept";
            //hdnDepartmentList.Value = strList;

            if (Session["timezoneID"] != null)
                tmzone = Session["timezoneID"].ToString();
            else
                tmzone = "26";
            //FB 2808
            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            //ZD 100958
            dtformat = format;
            if (Session["EmailDateFormat"] != null && Session["EmailDateFormat"].ToString() == "1")
                dtformat = "dd MMM yyyy";

            if (Session["timeFormat"] != null)
            {
                if (Session["timeFormat"].ToString() != "")
                    if (Session["timeFormat"].ToString() == "0")
                        tformat = "HH:mm";
                    else if (Session["timeFormat"].ToString() == "2")//FB 2588
                        tformat = "HHmmZ";
            }

            startDateedit.DisplayFormatString = format;
            startDateedit.EditFormatString = format;

            endDateedit.DisplayFormatString = format;
            endDateedit.EditFormatString = format;

            if (Session["hdnhdnSubmitValue"] != null && Session["hdnhdnSubmitValue"].ToString() != "" && hdnSubmitValue.Value == "")
                hdnSubmitValue.Value = Session["hdnhdnSubmitValue"].ToString();

            //FB 2593 - Starts
            if (Session["EnableCDR"] != null)
            {
                if (Session["EnableCDR"].ToString() == "1")
                    MenuCDR.Visible = true;
                else
                {
                    MenuCDR.Visible = false;
                    MenuWorkOrder.Style.Add("margin-top", "-2px"); // ZD 102835
                }
            }
            //FB 2593 - End

            BindCustomOptions(); //ZD 104686

            if (!IsPostBack)
            {
                TimeSpan sevenDays = new TimeSpan(7, 0, 0, 0);
                //txtStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now.Subtract(sevenDays));
                //txtEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now);
                //startDateedit.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now.Subtract(sevenDays));
                //endDateedit.Text = myVRMNet.NETFunctions.GetFormattedDate(DateTime.Now);
                //MainDiv.Attributes.Add("style", "display:none;");
                trDetails.Attributes.Add("style", "display:none;");
                Session["ReportXML"] = null;
                BindData();
                RetrieveReport();

            }
            String rptSave = "";

            if (Request["hdnRptSave"] != null)
            {
                rptSave = Request["hdnRptSave"].ToString();
            }

            //FB 2965
            String grpValue = "";
            if (Request["hdnGroup"] != null)
                grpValue = Request["hdnGroup"].ToString();

            if (hdnReportSelection.Value == "" && rptSave == "" && grpValue == "") //FB 2808 //FB 2965
            {
                btnOk_Click(null, null);
            }

        }
        catch (System.Threading.ThreadAbortException) { }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("Page_Init" + ex.Message);//ZD 100263
        }
    }
    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Methods Executed on Page Load

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {

            // ZD 100263 Starts
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("masterchildreport.aspx",Request.Url.AbsoluteUri.ToLower());

            if (!Session["EnableAdvancedReport"].ToString().Equals("1"))
                Response.Redirect("ShowError.aspx");
            // ZD 100263 Ends
            

            hdnGroup.Value = ""; //FB 2965

            if (hdnSubmitValue.Value != "")
                Session["hdnhdnSubmitValue"] = hdnSubmitValue.Value;

        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            log.Trace("Page_load" + ex.Message);//ZD 100263
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
        }
    }

    #endregion

    #region BindData 

    private void BindData()
    {
        try
        {
            DropDownList tempDropDown = new DropDownList();
            tempDropDown.DataTextField = "OrganizationName";
            tempDropDown.DataValueField = "OrgId";
            XmlDocument xmldoc = null; //FB 2593
            StringBuilder InXML = null;
            string OutXML = "";
            obj.BindOrganizationNames(tempDropDown);
			//FB 2593 Starts
            OrgList.Items.Clear();
            for (int i = 0; i < tempDropDown.Items.Count; i++)
            {
                //FB 2593
                if ((Session["admin"].ToString() == "2" || Session["admin"].ToString() == "3") && Session["UsrCrossAccess"].ToString() == "1"
                    && Session["organizationID"].ToString() == Session["loginUserSiloID"].ToString()) //FB 2882
                    OrgList.Items.Add(tempDropDown.Items[i].Text, tempDropDown.Items[i].Value);
                else
                {
                    if (tempDropDown.Items[i].Value == Session["organizationID"].ToString())
                    {
                        OrgList.Items.Add(tempDropDown.Items[i].Text, tempDropDown.Items[i].Value);
                        OrgList.Items[0].Selected = true; //FB 2882
                        hdnAdmin.Value = "U";
                        break;
                    }
                }
            }

			//FB 2593
            tempDropDown = new DropDownList();
            tempDropDown.DataTextField = "BridgeName";
            tempDropDown.DataValueField = "BridgeID";
            string bridgeXML = "";
            obj.GetAllBridges(ref bridgeXML,"A");

            if (bridgeXML.IndexOf("<error>") < 0)
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(bridgeXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
                
                String mcuOrgID = "";
                foreach (XmlNode node in nodes)
                {

                    mcuOrgID = node.SelectSingleNode("OrgId").InnerText;
                    if (node.SelectSingleNode("Public").InnerText == "1")
                        mcuOrgID = "0";

                    string strList = mcuOrgID + "!" + node.SelectSingleNode("BridgeID").InnerText + "!"
                        + node.SelectSingleNode("BridgeName").InnerText.Replace("(*)","");

                    if (hdnMCUList.Value == "")
                        hdnMCUList.Value = strList;
                    else
                        hdnMCUList.Value = hdnMCUList.Value + "``" + strList;
                }
            }

            InXML = new StringBuilder();
            InXML.Append("<EndpointDetails>");
            InXML.Append(obj.OrgXMLElement());
            InXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
            InXML.Append("<PublicEndpoint>0</PublicEndpoint>");
            InXML.Append("</EndpointDetails>");

            OutXML = obj.CallMyVRMServer("GetAllEndpoints", InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

            if (OutXML.IndexOf("<error>") < 0)
            {
                xmldoc.LoadXml(OutXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint");

                foreach (XmlNode node in nodes)
                {
                    String strList = node.SelectSingleNode("Bridge").InnerText + "!" + node.SelectSingleNode("EndpointID").InnerText + "!"
                        + node.SelectSingleNode("EndpointName").InnerText + "!" + node.SelectSingleNode("OrgId").InnerText;

                    if (hdnEptList.Value == "")
                        hdnEptList.Value = strList;
                    else
                        hdnEptList.Value = hdnEptList.Value + "``" + strList;
                }
            }

            obj.GetAllRoomsList(ref OutXML);
            
            if (OutXML.IndexOf("<error>") < 0)
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(OutXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetAllRoomsList/Rooms/Room");

                foreach (XmlNode node in nodes)
                {
                    string strList = node.SelectSingleNode("orgId").InnerText + "!" + node.SelectSingleNode("id").InnerText + "!"
                        + node.SelectSingleNode("name").InnerText;

                    if (hdnRoomList.Value == "")
                        hdnRoomList.Value = strList;
                    else
                        hdnRoomList.Value = hdnRoomList.Value + "``" + strList;
                }
            }

			//FB 2593 End
            string outXml = "";
            obj.GetAllDepartments(ref outXml);

            if (outXml.IndexOf("<error>") < 0)
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXml);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetAllDepartments/departments/department");

                foreach (XmlNode node in nodes)
                {
                    String strList = node.SelectSingleNode("orgId").InnerText + "!" + node.SelectSingleNode("id").InnerText + "!"
                        + node.SelectSingleNode("name").InnerText;

                    if (hdnDepartmentList.Value == "")
                        hdnDepartmentList.Value = strList;
                    else
                        hdnDepartmentList.Value = hdnDepartmentList.Value + "``" + strList;
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("BindData" + ex.Message);//ZD 100263
        }
    }

    #endregion

    //ZD 104686 - Start
    #region BindCustomOptions
    private void BindCustomOptions()
    {
        try
        {
            string customOptOutXML = "";
            obj.GetAllCustomOptions(ref customOptOutXML);
            Int32 c = 1;
            if (customOptOutXML.IndexOf("<error>") < 0)
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(customOptOutXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//CustomAttributesList/CustomAttribute");
                ASPxCheckBox chkCustom = new ASPxCheckBox();

                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Status").InnerText == "0")
                    {
                        if (node.SelectSingleNode("Status").InnerText == "0")
                        {
                            chkCustom = new ASPxCheckBox();
                            chkCustom.Text = obj.GetTranslatedText(node.SelectSingleNode("Title").InnerText);
                            chkCustom.Value = node.SelectSingleNode("CustomAttributeID").InnerText;
                            if (hdnCustomOptions.Value.IndexOf(node.SelectSingleNode("CustomAttributeID").InnerText + ",") >= 0)
                                chkCustom.Checked = true;
                           
                            chkCustom.ID = "chkCustom" + node.SelectSingleNode("CustomAttributeID").InnerText;
                            chkCustom.ClientInstanceName = "chkCustom" + node.SelectSingleNode("CustomAttributeID").InnerText;
                            chkCustom.ClientSideEvents.CheckedChanged = "function(s, e){if(!s.GetChecked()) chkCustom0.SetChecked(false);}";
                            tdCustomOptions.Controls.Add(chkCustom);
                            c++;
                        }
                    }
                }

                chkCustom = new ASPxCheckBox();
                chkCustom.Text = obj.GetTranslatedText("All");
                chkCustom.Value = "0";
                //chkCustom.Checked = false;
                chkCustom.ID = "chkCustom0";
                chkCustom.ClientInstanceName = "chkCustom0";
                chkCustom.ClientSideEvents.CheckedChanged = "function(s, e) {fnCustomAll();}";
                tdCustomOptions.Controls.Add(chkCustom);
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace("BindCustomOptions" + ex.Message);
        }
    }
    #endregion
    //ZD 104686 - End

    protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        int newPageSize;
        if (e != null)
        {
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            grid.SettingsPager.PageSize = newPageSize;
        }
    }
	//FB 2593
    protected void masterGrid_DetailRowGetButtonVisibility(object sender, ASPxGridViewDetailRowButtonEventArgs e)
    {
        if(e.KeyValue.ToString() == "")
            e.ButtonState = GridViewDetailRowButtonState.Hidden;
    }

    protected void detailsGrid_DetailRowGetButtonVisibility(object sender, ASPxGridViewDetailRowButtonEventArgs e)
    {
        if(e.KeyValue.ToString() == "" || subDetailTable == null || subDetailTable.Rows.Count == 0)
            e.ButtonState = GridViewDetailRowButtonState.Hidden;
    }

    #region detailGrid_DataSelect 

    protected void detailGrid_DataSelect(object sender, EventArgs e)
    {
        String confid = (sender as ASPxGridView).GetMasterRowKeyValue().ToString();

        ASPxGridView detailsgrid = sender as ASPxGridView;

        if (hdnSubmitValue.Value != "6")
            MainGrid.DetailRows.CollapseAllRows();

        if(hdnSubmitValue.Value != "")
            BindDetailsGrid(confid, detailsgrid);

        HtmlGenericControl DetailsDiv = (HtmlGenericControl)MainGrid.FindControl("DetailsDiv");

        if (DetailsDiv != null)
        {
            if (hdnSubmitValue.Value == "1")
                DetailsDiv.Attributes.Add("style", "height:50px");
            else
                DetailsDiv.Attributes.Add("style", "height:250px");
        }
    }
   
    #endregion

    #region subDetailGrid_DataSelect

    protected void subDetailGrid_DataSelect(object sender, EventArgs e)
    {
        String confid = (sender as ASPxGridView).GetMasterRowKeyValue().ToString();

        ASPxGridView subDetailsgrid = sender as ASPxGridView;

        subDetailsgrid.DetailRows.CollapseAllRows();

        if (hdnSubmitValue.Value != "")
            BindSubDetailsGrid(confid, subDetailsgrid);

        //HtmlGenericControl DetailsDiv = (HtmlGenericControl)MainGrid.FindControl("DetailsDiv");

        //if (DetailsDiv != null)
        //{
        //    if (hdnSubmitValue.Value == "1")
        //        DetailsDiv.Attributes.Add("style", "height:50px");
        //    else
        //        DetailsDiv.Attributes.Add("style", "height:250px");
        //}
    }
    #endregion

    #region ChildDetailGrid_DataSelect

    protected void ChildDetailGrid_DataSelect(object sender, EventArgs e)
    {
        String confid = (sender as ASPxGridView).GetMasterRowKeyValue().ToString();

        ASPxGridView childDetailsgrid = sender as ASPxGridView;

        childDetailsgrid.DetailRows.CollapseAllRows();

        if (hdnSubmitValue.Value != "")
            BindChildDetailsGrid(confid, childDetailsgrid);

    }
    #endregion

    protected void MainDetailsGrid_DataBound(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView gridView = sender as ASPxGridView;

            if (hdnSubmitValue.Value == "6")
            {
                if (rptTable.Columns.Contains("PartyGUID"))
                    gridView.Columns["PartyGUID"].Visible = false;
            }
            //FB 3066
            if (rptTable.Columns.Contains("ConfID"))
                gridView.Columns["ConfID"].Visible = false;

            if (rptTable.Columns.Contains("userid"))
                gridView.Columns["userid"].Visible = false;

            if (rptTable.Columns.Contains("roomid"))
                gridView.Columns["roomid"].Visible = false;

            if (rptTable.Columns.Contains("bridgeid"))
                gridView.Columns["bridgeid"].Visible = false;

            if (rptTable.Columns.Contains("endpointid"))
                gridView.Columns["endpointid"].Visible = false;
            //ZD 103216
           

            for (Int32 c = 0; c < gridView.Columns.Count; c++)
            {
                gridView.Columns[c].CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("MainDetailsGrid_DataBound" + ex.Message);//ZD 100263
        }
    }

    protected void DetailsGrid_DataBound(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView gridView = sender as ASPxGridView;

            if (hdnSubmitValue.Value == "1")
            {
                if (participantTable.Columns.Contains("ConfID"))
                    gridView.Columns["ConfID"].Visible = false;
            }
            else if (hdnSubmitValue.Value == "2")
            {
                if (participantTable.Columns.Contains("userid"))
                    gridView.Columns["userid"].Visible = false;
            }
            else if (hdnSubmitValue.Value == "3")
            {
                if (participantTable.Columns.Contains("roomid"))
                    gridView.Columns["roomid"].Visible = false;

                if (participantTable.Columns.Contains("bridgeid"))
                    gridView.Columns["bridgeid"].Visible = false;
            }
            else if (hdnSubmitValue.Value == "4" || hdnSubmitValue.Value == "5")
            {

                if (participantTable.Columns.Contains("endpointid"))
                    gridView.Columns["endpointid"].Visible = false;

                if (participantTable.Columns.Contains("roomid"))
                    gridView.Columns["roomid"].Visible = false;

                if (participantTable.Columns.Contains("bridgeid"))
                    gridView.Columns["bridgeid"].Visible = false;
            }
            else if (hdnSubmitValue.Value == "6")
            {
                if (participantTable.Columns.Contains("bridgeid"))
                    gridView.Columns["bridgeid"].Visible = false;

                if (participantTable.Columns.Contains("confid"))
                    gridView.Columns["ConfID"].Visible = false;

                if (participantTable.Columns.Contains("PartyGUID"))
                    gridView.Columns["PartyGUID"].Visible = false;
                //FB 3066
                if (rptTable.Columns.Contains("userid"))
                    gridView.Columns["userid"].Visible = false;

                if (rptTable.Columns.Contains("roomid"))
                    gridView.Columns["roomid"].Visible = false;

                if (rptTable.Columns.Contains("endpointid"))
                    gridView.Columns["endpointid"].Visible = false;
            }
            else if (hdnSubmitValue.Value == "7") // ZD 102835
            {

            }

            for (Int32 c = 0; c < gridView.Columns.Count; c++)
            {
                gridView.Columns[c].CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("DetailsGrid_DataBound" + ex.Message);//ZD 100263
            
        }
    }

    protected void subDetailGrid_DataBound(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView gridView = sender as ASPxGridView;

            if (hdnSubmitValue.Value == "6")
            {
                if (rbConferenceCDR.Checked || rbVMRCDR.Checked) //ZD 101348
                {
                    if (subDetailTable.Columns.Contains("bridgeid"))
                        gridView.Columns["bridgeid"].Visible = false;

                    if (subDetailTable.Columns.Contains("confid"))
                        gridView.Columns["ConfID"].Visible = false;

                    if (subDetailTable.Columns.Contains("PartyGUID"))
                        gridView.Columns["PartyGUID"].Visible = false;
                }
            }

            for (Int32 c = 0; c < gridView.Columns.Count; c++)
            {
                gridView.Columns[c].CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("subDetailGrid_DataSelect" + ex.Message);//ZD 100263

        }
    }

    protected void childDetailGrid_DataBound(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView gridView = sender as ASPxGridView;

            if (hdnSubmitValue.Value == "6")
            {
                if (rbConferenceCDR.Checked || rbVMRCDR.Checked) //ZD 101348
                {
                    if (childDetailTable.Columns.Contains("room"))
                        gridView.Columns["Room"].Visible = false;

                    if (childDetailTable.Columns.Contains("PartyGUID"))
                        gridView.Columns["PartyGUID"].Visible = false;

                    if (childDetailTable.Columns.Contains("bridgeid"))
                        gridView.Columns["bridgeid"].Visible = false;

                    if (childDetailTable.Columns.Contains("confid"))
                        gridView.Columns["ConfID"].Visible = false;
                }
            }

            for (Int32 c = 0; c < gridView.Columns.Count; c++)
            {
                gridView.Columns[c].CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("childDetailGrid_DataBound" + ex.Message);//ZD 100263
        }
    }

    #region BindDetailsGrid

    private void BindDetailsGrid(String confid, ASPxGridView detailsgrid)
    {
        String outXML = "";
        try
        {
            //confid = "12";
            DataTable detailsTable = new DataTable();

            if (Session["ReportXML"] != null)
            {
                outXML = Session["ReportXML"].ToString();

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);
                ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmlDoc));

                if (ds.Tables.Count > 0 && participantTable != null && participantTable.Rows.Count == 0) //FB 2808
                    CreateTable();

                DataRow[] rows = null;

                if (hdnSubmitValue.Value == "1")
                    rows = participantTable.Select("Confid ='" + confid + "'");
                else if (hdnSubmitValue.Value == "2")
                    rows = participantTable.Select("userid ='" + confid + "'");
                else if (hdnSubmitValue.Value == "3")
                    rows = participantTable.Select("roomid ='" + confid + "'");
                else if (hdnSubmitValue.Value == "4")
                    rows = participantTable.Select("endpointid ='" + confid + "'");
                else if (hdnSubmitValue.Value == "5" || hdnSubmitValue.Value == "6") //FB 2593
                    rows = participantTable.Select("bridgeid ='" + confid + "'");
                else if (hdnSubmitValue.Value == "7") // ZD 102835
                {
                }
                detailsTable = participantTable.Clone();
                
                //ZD 100288
                foreach (DataRow row in rows)
                {
                    if (detailsTable.Columns.Contains("Conference Type"))
                        row["Conference Type"] = obj.GetTranslatedText(row["Conference Type"].ToString());

                    if (detailsTable.Columns.Contains("Connection Type"))
                        row["Connection Type"] = obj.GetTranslatedText(row["Connection Type"].ToString());

                    if (detailsTable.Columns.Contains("Single/Recuring"))
                        row["Single/Recuring"] = obj.GetTranslatedText(row["Single/Recuring"].ToString());
                    
                }
                foreach(DataColumn col in detailsTable.Columns)
                    col.Caption = obj.GetTranslatedText(col.Caption);

                foreach (DataRow thisRow in rows)
                    detailsTable.Rows.Add(thisRow.ItemArray);
                
                if (hdnSubmitValue.Value != "")
                {
                    if (hdnSubmitValue.Value == "6")
                    {
                        //ZD 101348
                        //if (rbConferenceCDR.Checked)
                        //    detailsgrid.KeyFieldName = "ConfID";
                        //else
                        detailsgrid.KeyFieldName = "ConfID";
                    }
                    else
                        detailsgrid.KeyFieldName = MainGrid.KeyFieldName;
                }

                detailsgrid.DataSource = detailsTable;

                //detailsgrid.Columns[0].Visible = false;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("BindDetailsGrid" + ex.Message);//ZD 100263
        }
    }

    #endregion

    #region BindSubDetailsGrid

    private void BindSubDetailsGrid(String confid, ASPxGridView subDetailsgrid)
    {
        String outXML = "";
        try
        {
            DataTable detailsTable = new DataTable();

            if (Session["ReportXML"] != null)
            {
                outXML = Session["ReportXML"].ToString();

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);
                ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmlDoc));

                if (ds.Tables.Count > 0 && subDetailTable != null && subDetailTable.Rows.Count == 0)
                    CreateTable();

                DataRow[] rows = null;
                if (subDetailTable.Rows.Count > 0)
                {
                    if (hdnSubmitValue.Value == "6")
                        rows = subDetailTable.Select("Confid ='" + confid + "'");

                    detailsTable = subDetailTable.Clone();

                    //ZD 100288
                    foreach (DataRow row in rows)
                    {
                        if (detailsTable.Columns.Contains("Connection Type"))
                            row["Connection Type"] = obj.GetTranslatedText(row["Connection Type"].ToString());
                    }

                    foreach (DataColumn col in detailsTable.Columns)
                        col.Caption = obj.GetTranslatedText(col.Caption);

                    foreach (DataRow thisRow in rows)
                        detailsTable.Rows.Add(thisRow.ItemArray);

                    if (hdnSubmitValue.Value == "6")
                    {
                        //detailsTable.Columns.Remove("confid");
                        //detailsTable.Columns.Remove("bridgeid");
                        subDetailsgrid.KeyFieldName = "PartyGUID";
                    }

                    subDetailsgrid.DataSource = detailsTable;
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("BindSubDetailsGrid" + ex.Message);//ZD 100263
        }
    }

    #endregion

    #region BindChildDetailsGrid

    private void BindChildDetailsGrid(String confid, ASPxGridView childDetailsgrid)
    {
        String outXML = "";
        try
        {
            DataTable detailsTable = new DataTable();

            if (Session["ReportXML"] != null)
            {
                outXML = Session["ReportXML"].ToString();

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);
                ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmlDoc));

                if (ds.Tables.Count > 0 && childDetailTable != null && childDetailTable.Rows.Count == 0)
                    CreateTable();

                DataRow[] rows = null;
                if (childDetailTable.Rows.Count > 0)
                {
                    if (hdnSubmitValue.Value == "6")
                        rows = childDetailTable.Select("PartyGUID ='" + confid + "'");

                    detailsTable = childDetailTable.Clone();

                    //ZD 100288
                    //ZD 100288
                    foreach (DataRow row in rows)
                    {
                        if (detailsTable.Columns.Contains("Connection Type"))
                            row["Connection Type"] = obj.GetTranslatedText(row["Connection Type"].ToString());
                    }

                    foreach (DataColumn col in detailsTable.Columns)
                        col.Caption = obj.GetTranslatedText(col.Caption);

                    foreach (DataRow thisRow in rows)
                        detailsTable.Rows.Add(thisRow.ItemArray);

                    if (hdnSubmitValue.Value == "6")
                    {
                        //detailsTable.Columns.Remove("confid");
                        childDetailsgrid.KeyFieldName = "PartyGUID";
                    }

                    childDetailsgrid.DataSource = detailsTable;
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("BindChildDetailsGrid" + ex.Message);//ZD 100263
        }
    }

    #endregion

    // ZD 102835 Start
    public string ConvertDataTableToHTML(DataTable dt)
    {
        if (dt.Columns["workorder_conferenceid"] != null)
            dt.Columns.Remove("workorder_conferenceid");

        if (dt.Columns["type"] != null)
            dt.Columns.Remove("type");

        string html = "<table border=\"1\" style=\"border-collapse: collapse; white-space: nowrap; border:1px solid gray\">";
        //add header row
        html += "<tr style=\"background-color: lightgray;\">";
        for (int i = 0; i < dt.Columns.Count; i++)
            html += "<td>" + dt.Columns[i].Caption + "</td>";
        html += "</tr>";
        //add rows
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            html += "<tr>";
            for (int j = 0; j < dt.Columns.Count; j++)
                html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
            html += "</tr>";
        }
        html += "</table>";
        return html;
    }
    // ZD 102835 End

    #region MainGrid_HtmlRowCreated

    protected void MainGrid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
            
            Int32 index = 0;
            
            string confid = MainGrid.GetRowValues(e.VisibleIndex, "ConfID").ToString();
            //ZD 104686
            if(confid == "")
            {
                e.Row.Visible = false;
            }
            else
            {
                GridViewDataColumn colR = (GridViewDataColumn)MainGrid.Columns["Rooms In Conf."];
                //GridViewDataColumn colR1 = (GridViewDataColumn)MainGrid.Columns["Rooms In Conf."];
                if (colR != null && colR.GroupIndex < 0) // ZD 102835
                {
                    index = colR.VisibleIndex; //FB 3066
                    if (participantTable.Rows.Count > 0)
                        index = colR.VisibleIndex + 1;

                    String rCellText = MainGrid.GetRowValues(e.VisibleIndex, "Rooms In Conf").ToString(); // e.Row.Cells[colR.Index].Text;
                    if (Session["EnableTravelAvoidTrack"].ToString() == "0")
                        rCellText = MainGrid.GetRowValues(e.VisibleIndex, "Rooms In Conf.").ToString();
                    //ZD 103216
                    rmHostTable = new DataTable();
                    rmHostTable.Columns.Add(obj.GetTranslatedText("Room Name"));
                    rmHostTable.Columns.Add("HostHeading " + obj.GetTranslatedText("Host"));
                    rmHostTable.Columns.Add(obj.GetTranslatedText("# of Attendees"));
                    string[] strArray = rCellText.Split(',');
                    int totTravalAvoid = 0;
                    if (rCellText != "-" && Session["EnableTravelAvoidTrack"].ToString() == "1")
                    {
                        for (int i = 0; i < strArray.Length; i++)
                        {
                            string[] strRm = strArray[i].Split('|');
                            string strHst = strRm[1];
                            if (strHst == "1")
                                strHst = "ImgRoomHost";
                            else
                                strHst = "";
                            rmHostTable.Rows.Add(strRm[0], strHst, "AttAlign" + strRm[2]);
                            if (strRm[1] == "0")
                                totTravalAvoid += Convert.ToInt32(strRm[2]);
                        }

                        rmHostTable.Rows.Add("", obj.GetTranslatedText("Total Travel Avoided"), "AttAlign" + totTravalAvoid);

                        rCellText = ConvertDataTableToHTML(rmHostTable);
                    }

                    e.Row.Cells[index].Text = obj.GetTranslatedText("Rooms");
                    ImageButton imgRoom = new ImageButton();
                    imgRoom.Height = Unit.Pixel(11);
                    imgRoom.ID = "imgRoom" + e.VisibleIndex + "_" + index;// colR.Index;
                    imgRoom.ImageUrl = "../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png";
                    imgRoom.Attributes.Add("onclick", "javascript:return fnAssignValue(this,'R','" + rCellText + "')");
                    imgRoom.Style.Add("Cursor", "Hand");

                    e.Row.Cells[index].Controls.Add(new LiteralControl(obj.GetTranslatedText("Rooms")));//ZD 100288
                    e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                    e.Row.Cells[index].Controls.Add(imgRoom);
                    // e.Row.Cells[colR.Index].Controls.Add(new LiteralControl("<a href='javascript:void(0);' onclick=\"fnAssignValue(this, '" + rCellText + "')\">" + "More Info...</a>"));
                }

                GridViewDataColumn colE = (GridViewDataColumn)MainGrid.Columns["Endpoints in Conf"];
                index = 0;
                if (colE != null && colE.GroupIndex < 0) // ZD 102835
                {
                    index = colE.VisibleIndex; //FB 3066
                    if (participantTable.Rows.Count > 0)
                        index = colE.VisibleIndex + 1;

                    String eCellText = MainGrid.GetRowValues(e.VisibleIndex, "Endpoints in Conf").ToString(); //e.Row.Cells[colE.Index].Text;
                    //e.Row.Attributes.Add("EptVal", e.Row.Cells[colE.Index].Text);
                    e.Row.Cells[index].Text = obj.GetTranslatedText("Endpoints");
                    ImageButton imgEpt = new ImageButton();
                    imgEpt.Height = Unit.Pixel(11);
                    imgEpt.ID = "imgEpt" + e.VisibleIndex + "_" + colE.Index;
                    imgEpt.ImageUrl = "../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png";
                    imgEpt.Attributes.Add("onclick", "javascript:return fnAssignValue(this,'E','" + eCellText + "')");
                    imgEpt.Style.Add("Cursor", "Hand");
                    e.Row.Cells[index].Controls.Add(new LiteralControl(obj.GetTranslatedText("Endpoints"))); //ZD 100288
                    e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                    e.Row.Cells[index].Controls.Add(imgEpt);
                }

                GridViewDataColumn colD = (GridViewDataColumn)MainGrid.Columns["Department Name"];
                index = 0;
                if (colD != null && colD.GroupIndex < 0) // ZD 102835
                {
                    index = colD.VisibleIndex; //FB 3066
                    if (participantTable.Rows.Count > 0)
                        index = colD.VisibleIndex + 1;

                    String eCellText = MainGrid.GetRowValues(e.VisibleIndex, "Department Name").ToString();
                    e.Row.Cells[index].Text = obj.GetTranslatedText("Departments");
                    ImageButton imgDpt = new ImageButton();
                    imgDpt.ID = "imgDpt" + e.VisibleIndex + "_" + colD.Index;
                    imgDpt.ImageUrl = "../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png";
                    imgDpt.Height = Unit.Pixel(11);
                    imgDpt.Attributes.Add("onclick", "javascript:return fnAssignValue(this,'D','" + eCellText + "')");
                    imgDpt.Style.Add("Cursor", "Hand");
                    e.Row.Cells[index].Controls.Add(new LiteralControl(obj.GetTranslatedText("Departments")));//ZD 100288
                    e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                    e.Row.Cells[index].Controls.Add(imgDpt);
                }

                // ZD 102835 Start
                GridViewDataColumn colID = (GridViewDataColumn)MainGrid.Columns["ConfID"];
                GridViewDataColumn colAV = (GridViewDataColumn)MainGrid.Columns["AV WO"];
                GridViewDataColumn colCAT = (GridViewDataColumn)MainGrid.Columns["CAT WO"];
                GridViewDataColumn colHK = (GridViewDataColumn)MainGrid.Columns["HK WO"];
                index = 0;
                if (colID != null && (colAV != null || colCAT != null || colHK != null))
                {

                    if (colAV != null && MainGrid.GetRowValues(e.VisibleIndex, "AV WO").ToString() != "-" && colAV.GroupIndex < 0)
                    {
                        index = colAV.VisibleIndex;
                        //if (workOrderTable.Rows.Count > 0)
                        //    index = colAV.VisibleIndex + 1;

                        confid = MainGrid.GetRowValues(e.VisibleIndex, "ConfUID").ToString();
                        string criteria = "workorder_conferenceid = " + confid + " and type = 1";
                        DataRow[] dr = workOrderTable.Select(criteria);

                        DataTable dt = workOrderTable.Clone();
                        foreach (DataRow row in dr)
                            dt.ImportRow(row);

                        String eCellText = ConvertDataTableToHTML(dt);
                        //String eCellText = MainGrid.GetRowValues(e.VisibleIndex, "Department Name").ToString();
                        //e.Row.Cells[index].Text = obj.GetTranslatedText("Departments");
                        ImageButton imgAV = new ImageButton();
                        imgAV.ID = "imgAV" + e.VisibleIndex + "_" + colID.Index;
                        imgAV.ImageUrl = "../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png";
                        imgAV.Height = Unit.Pixel(11);
                        imgAV.Attributes.Add("onclick", "javascript:return fnAssignValue(this,'W1','" + eCellText + "')");
                        imgAV.Style.Add("Cursor", "Hand");
                        //e.Row.Cells[index].Controls.Add(new LiteralControl(obj.GetTranslatedText("Audiovisual Work Order")));
                        e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                        e.Row.Cells[index].Controls.Add(imgAV);
                    }

                    if (colCAT != null && MainGrid.GetRowValues(e.VisibleIndex, "CAT WO").ToString() != "-" && colCAT.GroupIndex < 0)
                    {
                        index = colCAT.VisibleIndex;
                        //if (workOrderTable.Rows.Count > 0)
                        //    index = colCAT.VisibleIndex + 1;

                        confid = MainGrid.GetRowValues(e.VisibleIndex, "ConfUID").ToString();
                        string criteria = "workorder_conferenceid = " + confid + " and type = 2";
                        DataRow[] dr = workOrderTable.Select(criteria);

                        DataTable dt = workOrderTable.Clone();
                        foreach (DataRow row in dr)
                            dt.ImportRow(row);

                        String eCellText = ConvertDataTableToHTML(dt);
                        //String eCellText = MainGrid.GetRowValues(e.VisibleIndex, "Department Name").ToString();
                        //e.Row.Cells[index].Text = obj.GetTranslatedText("Departments");
                        ImageButton imgCAT = new ImageButton();
                        imgCAT.ID = "imgCAT" + e.VisibleIndex + "_" + colID.Index;
                        imgCAT.ImageUrl = "../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png";
                        imgCAT.Height = Unit.Pixel(11);
                        imgCAT.Attributes.Add("onclick", "javascript:return fnAssignValue(this,'W2','" + eCellText + "')");
                        imgCAT.Style.Add("Cursor", "Hand");
                        //e.Row.Cells[index].Controls.Add(new LiteralControl(obj.GetTranslatedText("Catering Work Order")));
                        e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                        e.Row.Cells[index].Controls.Add(imgCAT);
                    }

                    if (colHK != null && MainGrid.GetRowValues(e.VisibleIndex, "HK WO").ToString() != "-" && colHK.GroupIndex < 0)
                    {
                        index = colHK.VisibleIndex;
                        //if (workOrderTable.Rows.Count > 0)
                        //    index = colHK.VisibleIndex + 1;

                        confid = MainGrid.GetRowValues(e.VisibleIndex, "ConfUID").ToString();
                        string criteria = "workorder_conferenceid = " + confid + " and type = 3";
                        DataRow[] dr = workOrderTable.Select(criteria);

                        DataTable dt = workOrderTable.Clone();
                        foreach (DataRow row in dr)
                            dt.ImportRow(row);

                        String eCellText = ConvertDataTableToHTML(dt);
                        //String eCellText = MainGrid.GetRowValues(e.VisibleIndex, "Department Name").ToString();
                        //e.Row.Cells[index].Text = obj.GetTranslatedText("Departments");
                        ImageButton imgHK = new ImageButton();
                        imgHK.ID = "imgHK" + e.VisibleIndex + "_" + colID.Index;
                        imgHK.ImageUrl = "../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png";
                        imgHK.Height = Unit.Pixel(11);
                        imgHK.Attributes.Add("onclick", "javascript:return fnAssignValue(this,'W3','" + eCellText + "')");
                        imgHK.Style.Add("Cursor", "Hand");
                        //e.Row.Cells[index].Controls.Add(new LiteralControl(obj.GetTranslatedText("Housekeeping Work Order")));
                        e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                        e.Row.Cells[index].Controls.Add(imgHK);
                    }
                }
                // ZD 102835 End

                //for (Int32 c = 0; c < e.Row.Cells.Count; c++)
                for (Int32 c = 0; c < MainGrid.Columns.Count; c++) //FB 2965
                {
                    var colname = MainGrid.Columns[c].Caption.ToLower();
                    var cname = MainGrid.Columns[c].Caption;

                    //if (cname == "Duration(Min)")
                    //    cname = "Duration";
                    if (cname != "")
                    {
                        //MainGrid.GetRowValues(e.VisibleIndex, cname).ToString() == "-"
                        if (MainGrid.Columns[c].Visible == true && c < e.Row.Cells.Count
                            && (colname != "rooms in conf" && colname != "endpoints in conf" && colname != "department name"))
                            e.Row.Cells[c].HorizontalAlign = HorizontalAlign.Center;
                    }
                }
            }
            oldConfID = confid;
            //GridViewDataColumn dur = (GridViewDataColumn)MainGrid.Columns["Duration"];
            //if (dur != null)
            //    dur.Caption = "Duration(Min)";
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(ex.StackTrace + " MainGrid_HtmlRowCreated: " + ex.Message);
        }
    }

    #endregion

    #region btnOk_Click 
    protected void btnOk_Click(object sender, EventArgs e)
    {
        StringBuilder inXml = new StringBuilder();
        String outXML = "";
        try
        {
            errLabel.Text = ""; //FB 2654
            hdnRptSave.Value = "";
            hdnReportSelection.Value = ""; //FB 2886
            if(hdnOkValue.Value != "")
                GenerateInXML(ref inXml);
            if ((Session["ReportXML"] == null || hdnOkValue.Value == "1") && inXml.ToString() != "")
            {
                hdnOkValue.Value = "0";
                outXML = obj.CallMyVRMServer("GetMCCReport", inXml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                Session["ReportXML"] = outXML;
            }
            else if (Session["ReportXML"] != null && Session["ReportXML"].ToString() != "")
                outXML = Session["ReportXML"].ToString();
            //FB 2808
            if (outXML.ToString().IndexOf("<error>") >= 0)
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(outXML);
                errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                errLabel.Visible = true;
            }
            else if (outXML != "")
            {
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);
                ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmlDoc));
            }
            //MainGrid.DataSource = null;
            //MainGrid.DataBind();
            MainGrid.Columns.Clear();
            MainGrid.KeyFieldName = "ConfID"; //FB 3066

            if (ds != null && ds.Tables.Count > 0) //FB 2808
            {
                rptImgRow.Attributes.Add("style", "display:none;");
                //MainDiv.Attributes.Add("style", "display:block;");
                trDetails.Attributes.Add("style", "display:block;");
                
                CreateTable();
                CreateGridColumns();

                if (hdnSubmitValue.Value == "1")
                {
                    filterRptTable.Merge(rptTable);

                    List<DataRow> rowsToDelete = new List<DataRow>();
                    foreach (DataRow row in rptTable.Rows)
                    {
                        string confid = row["ConfID"].ToString();
                        if (confid == "")
                            rowsToDelete.Add(row);
                    }

                    foreach (DataRow row in rowsToDelete)
                    {
                        row.Delete();
                    }

                    rptTable.AcceptChanges();
                    
                }

                if (participantTable != null && participantTable.Rows.Count > 0 && hdnSubmitValue.Value!= "")
                {
                    if (hdnSubmitValue.Value == "1")
                        MainGrid.KeyFieldName = "ConfID";
                    else if (hdnSubmitValue.Value == "2")
                        MainGrid.KeyFieldName = "userid";
                    else if (hdnSubmitValue.Value == "3")
                        MainGrid.KeyFieldName = "roomid";
                    else if (hdnSubmitValue.Value == "4")
                        MainGrid.KeyFieldName = "endpointid";
                    else if (hdnSubmitValue.Value == "5")
                        MainGrid.KeyFieldName = "bridgeid";
                    else if (hdnSubmitValue.Value == "6") //FB 2593
                        MainGrid.KeyFieldName = "bridgeid";
                    else if (hdnSubmitValue.Value == "7") //ZD 102835
                        MainGrid.KeyFieldName = "Conf ID";

                    MainGrid.SettingsDetail.ShowDetailRow = true;
                    MainGrid.SettingsDetail.ExportMode = GridViewDetailExportMode.All;
                }
                else
                    MainGrid.SettingsDetail.ShowDetailRow = false;

                if (hdnSubmitValue.Value == "1")
                    MainGrid.Caption = obj.GetTranslatedText("Conference Report");//FB 2272
                else if (hdnSubmitValue.Value == "2")
                    MainGrid.Caption = obj.GetTranslatedText("User Selection");
                else if (hdnSubmitValue.Value == "3")
                    MainGrid.Caption = obj.GetTranslatedText("Room Selection");
                else if (hdnSubmitValue.Value == "4")
                    MainGrid.Caption = obj.GetTranslatedText("Endpoint Selection");
                else if (hdnSubmitValue.Value == "5")
                    MainGrid.Caption = obj.GetTranslatedText("MCU Selection");
                else if (hdnSubmitValue.Value == "6")//FB 2593
                    MainGrid.Caption = obj.GetTranslatedText("CDR Scheduled Report");
                else if (hdnSubmitValue.Value == "7") // ZD 102835
                    MainGrid.Caption = obj.GetTranslatedText("Work Orders Report"); 

                try
                {
                    if (Session["languageID"].ToString() != "1")
                    {
                        foreach (DataRow row in rptTable.Rows)
                        {
                            if (rptTable.Columns.Contains("Status"))
                                row["Status"] = obj.GetTranslatedText(row["Status"].ToString());
                            if (rptTable.Columns.Contains("Conference Type"))
                                row["Conference Type"] = obj.GetTranslatedText(row["Conference Type"].ToString());
                            if (rptTable.Columns.Contains("Host Role"))
                                row["Host Role"] = obj.GetTranslatedText(row["Host Role"].ToString());
                            if (rptTable.Columns.Contains("Occurrence"))
                                row["Occurrence"] = obj.GetTranslatedText(row["Occurrence"].ToString());
                            if (rptTable.Columns.Contains("Role Name"))
                                row["Role Name"] = obj.GetTranslatedText(row["Role Name"].ToString());
                            //if (rptTable.Columns.Contains("Company Relationship"))//ZD 102131 start
                            //    row["Company Relationship"] = obj.GetTranslatedText(row["Company Relationship"].ToString());
                            if (rptTable.Columns.Contains("Attended Conference"))
                                row["Attended Conference"] = obj.GetTranslatedText(row["Attended Conference"].ToString());
                            if (rptTable.Columns.Contains("Date/Time Signed-In")) 
                                row["Date/Time Signed-In"] = obj.GetTranslatedText(row["Date/Time Signed-In"].ToString()); //ZD 102131 End
                            if (rptTable.Columns.Contains("Address Type"))
                                row["Address Type"] = obj.GetTranslatedText(row["Address Type"].ToString());
                            if (rptTable.Columns.Contains("Pre. Dialing Option"))
                                row["Pre. Dialing Option"] = obj.GetTranslatedText(row["Pre. Dialing Option"].ToString());
                            if (rptTable.Columns.Contains("VMR Type")) //ZD 101950
                                row["VMR Type"] = obj.GetTranslatedText(row["VMR Type"].ToString());
                            
                        }
                    }

                    MainGrid.DataSource = rptTable;
                    MainGrid.DataBind();

                    //MainGrid.Styles.Cell.Font.Size = FontUnit.Smaller;
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                }                
            }
            else
            {
                //FB 2808
                if (IsPostBack)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("No Records");
                }
                rptImgRow.Attributes.Add("style", "display:block;");
                //MainDiv.Attributes.Add("style", "display:none;");
                trDetails.Attributes.Add("style", "display:none;");
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region GenerateInXML 

    private void GenerateInXML(ref StringBuilder inXml)
    {
        try
        {
            String strInputValue = "";

            String[] arrMain = null;

            strInputValue = hdnMainIPValue.Value;
            arrMain = strInputValue.Split('|');

            String fromDate = myVRMNet.NETFunctions.GetDefaultDate(startDateedit.Text);
            String toDate = myVRMNet.NETFunctions.GetDefaultDate(endDateedit.Text);

            inXml.Append("<MCCReport>");
            inXml.Append(obj.OrgXMLElement());
            inXml.Append("<Type>" + hdnSubmitValue.Value + "</Type>");
            inXml.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
            inXml.Append("<DateFormat>" + format + "</DateFormat>");
            inXml.Append("<timezone>" + tmzone + "</timezone>");
            inXml.Append("<StartDate>" + fromDate + "</StartDate>");
            inXml.Append("<EndDate>" + toDate + "</EndDate>");
            inXml.Append("<TimeFormat>" + tformat + "</TimeFormat>");//FB 2588


            inXml.Append("<Params>");

            for (Int32 i = 0; i < arrMain.Length; i++)
            {
                String[] arrSubIP = arrMain[i].Split(':');

                switch (arrSubIP[0])
                {
                    //Conference Menu Start
                    case "1":
                        inXml.Append("<Conference>");
                        GetFilterParams(ref inXml, arrSubIP, "B", "");
                        inXml.Append("</Conference>");
                        break;
                    case "2":
                        inXml.Append("<Host>");
                        GetFilterParams(ref inXml, arrSubIP, "B", "");
                        inXml.Append("</Host>");
                        break;
                    case "3":
                        inXml.Append("<Participant>");
                        GetFilterParams(ref inXml, arrSubIP, "B", "");
                        inXml.Append("</Participant>");
                        break;
                    case "4":
                        inXml.Append("<Scheduled>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "Status");
                        inXml.Append("</Scheduled>");
                        break;
                    case "5":
                        inXml.Append("<ConfType>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "ConfType");
                        inXml.Append("</ConfType>");
                        break;
                    case "6":
                        inXml.Append("<ConfBehavior>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "recuring");
                        inXml.Append("</ConfBehavior>");
                        break;
                    case "7":
                        inXml.Append("<ConfTime>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</ConfTime>");
                        break;
                    case "8":
                        inXml.Append("<ConfDuration>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</ConfDuration>");
                        break;                    
                    case "27": //ZD 101950
                        inXml.Append("<ConfVMR>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "");
                        inXml.Append("</ConfVMR>");
                        break;
                    case "30": //ZD 104686
                        inXml.Append("<CustomOptions>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "");
                        inXml.Append("</CustomOptions>");
                        break;  
                    //Conference Menu End
                    //User Menu Start
                    case "9":
                        inXml.Append("<UserCommon>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</UserCommon>");
                        break;
                    case "10":
                        inXml.Append("<Exchange>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Exchange>");
                        break;
                    case "11":
                        inXml.Append("<Domino>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Domino>");
                        break;
                    //User Menu End
                    //Room Menu Start
                    case "12":
                        inXml.Append("<RoomType>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "VideoAvailable");
                        inXml.Append("</RoomType>");
                        break;
                     case "13":
                        inXml.Append("<Immediate>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "Immediate");
                        inXml.Append("</Immediate>");
                        break;
                     case "14":
                        inXml.Append("<SubConfSelect>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</SubConfSelect>");
                        break;
                    case "15":
                        inXml.Append("<EptDetails>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</EptDetails>");
                        break;
                    case "16":
                        inXml.Append("<MCUDetails>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</MCUDetails>");
                        break;
                    case "17":
                        inXml.Append("<WODetails>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</WODetails>");
                        break;
                    case "18":
                        inXml.Append("<RmAsset>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</RmAsset>");
                        break;
                    case "19":
                        inXml.Append("<Endpoint>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Endpoint>");
                        break;
                    case "20":
                        inXml.Append("<Tier>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Tier>");
                        break;
                    //Room Menu End
                    case "21": //Conference Menu FB 2654
                        inXml.Append("<Confcierge>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Confcierge>");
                        break;
                    case "22": //FB 2593 Starts
                        inXml.Append("<CDR>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</CDR>");
                        break;
                    case "23":
                        inXml.Append("<SecondTable>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</SecondTable>");
                        break;
                    case "24":
                        inXml.Append("<MCU>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</MCU>");
                        break;
                    case "25":
                        inXml.Append("<Room>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Room>");
                        break;
                    case "26":
                        inXml.Append("<EndPoints>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</EndPoints>");
                        break;
                    case "28": // ZD 102835
                        inXml.Append("<WorkOrders>");
                        GetFilterParams(ref inXml, arrSubIP, "F", "");
                        inXml.Append("</WorkOrders>");
                        break;
                    case "29": // ZD 102835
                        inXml.Append("<Personnel>");
                        GetFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Personnel>");
                        break;
                    //FB 2593 End
                }
            }

            inXml.Append("</Params>");
            inXml.Append("</MCCReport>");

        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region GetFilterParams 

    private void GetFilterParams(ref StringBuilder inXml, String[] arrSubIP, String strType, String fieldName)
    {
        String query1 = "";
        String query2 = "";
        try
        {
            inXml.Append("<Query>");

            String[] arrSub1IP = arrSubIP[1].Split(',');

            for (Int32 j = 0; j < arrSub1IP.Length; j++)
            {
                if (arrSub1IP[j].IndexOf("right") >= 0)
                {
                    GetRightFilter(ref query1, ref query2, ref inXml);

                    query2 = "";
                }
                else if (arrSub1IP[j].IndexOf("Person") >= 0)
                {
                    query2 = "";
                }
                else
                {
                    if (query1 == "")
                        query1 = arrSub1IP[j].ToString();
                    else
                        query1 = query1 + "," + arrSub1IP[j].ToString();
                }
            }

            if (query1 != "")
            {
                if (strType != "F")
                {
                    inXml.Append("<Select>");
                    inXml.Append(query1);
                    inXml.Append("</Select>");
                }
                else
                    query2 = query1;
            }

            if (query2 != "")
            {
                inXml.Append("<Filter>");
                if (fieldName != "")
                    inXml.Append(fieldName + " in (");

                inXml.Append(query2);

                if (fieldName != "")
                    inXml.Append(")");
                inXml.Append("</Filter>");
            }

            inXml.Append("</Query>");
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("GetFilterParams: " + ex.Message);
        }
    }

    #endregion

    #region GetRightFilter

    private void GetRightFilter(ref String query1, ref String query2, ref StringBuilder inXml)
    {
        String strRightInputValue = "";
        //1:orgid;2:Departmentid;3(Rooms):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
        //;4(MCU):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
        //;5(WO):LastName [In Charge],
        //6:[Company Relationship];7:(invitee) 1 - External / 2 - Internal
        //8:(EP)0|1 End Point refer "GetConferenceEndpoint"
        //9:(MCU)0|1 can get from End Point
        //10:(Conf Speed)0|1 can get from End Point
        //11:(Conn Protocol)0|1 can get from End Point
        //12:(WO) 0|1 Name check with type is it A/V | CAT | HK
        try
        {
            strRightInputValue = hdnRightMenuValue.Value;
            String[] arrMain = strRightInputValue.Split('|');

            inXml.Append("<OrgFilter>");
            for (Int32 i = 0; i < arrMain.Length; i++)
            {
                String[] arrSubIP = arrMain[i].Split(':');

                switch (arrSubIP[0])
                {
                    case "1":
                        inXml.Append("<Org>");
                        GetRightFilterParams(ref inXml, arrSubIP, "F", "OrgID");
                        inXml.Append("</Org>");
                        break;
                    case "2":
                        inXml.Append("<Dept>");
                        GetRightFilterParams(ref inXml, arrSubIP, "F", "");
                        inXml.Append("</Dept>");
                        break;
                    case "3":
                        inXml.Append("<RmIncharge>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</RmIncharge>");
                        break;
                    case "4":
                        inXml.Append("<MCUIncharge>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</MCUIncharge>");
                        break;
                    case "5":
                        inXml.Append("<WOIncharge>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</WOIncharge>");
                        break;
                    case "6":
                        inXml.Append("<Attendees>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</Attendees>");
                        break;
                    case "7":
                        inXml.Append("<RoomsParty>");
                        GetRightFilterParams(ref inXml, arrSubIP, "F", "invitee");
                        inXml.Append("</RoomsParty>");
                        break;
                    case "8":
                        inXml.Append("<EndPoints>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</EndPoints>");
                        break;
                    case "9":
                        inXml.Append("<MCU>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</MCU>");
                        break;
                    case "10":
                        inXml.Append("<ConfSpeed>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</ConfSpeed>");
                        break;
                    case "11":
                        inXml.Append("<ConProtocol>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</ConProtocol>");
                        break;
                    case "12":
                        inXml.Append("<ConfWO>");
                        GetRightFilterParams(ref inXml, arrSubIP, "S", "");
                        inXml.Append("</ConfWO>");
                        break;
                }
            }
            inXml.Append("</OrgFilter>");
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("GetRightFilterParams: " + ex.Message);
        }
    }

    #endregion

    #region GetRightFilterParams

    private void GetRightFilterParams(ref StringBuilder inXml, String[] arrSubIP, String strType, String fieldName)
    {
        String query1 = "";
        String query2 = "";
        try
        {
            inXml.Append("<Query>");

            String[] arrSub1IP = arrSubIP[1].Split(',');

            for (Int32 j = 0; j < arrSub1IP.Length; j++)
            {
                if (query1 == "")
                    query1 = arrSub1IP[j].ToString();
                else
                    query1 = query1 + "," + arrSub1IP[j].ToString();

                if (strType == "B")
                    query2 = arrSub1IP[0];
            }

            if (query1 != "")
            {
                if (strType != "F")
                {
                    inXml.Append("<Select>");
                    inXml.Append(query1);
                    inXml.Append("</Select>");
                }
                else
                    query2 = query1;
            }

            if (query2 != "")
            {
                inXml.Append("<Filter>");
                if (fieldName != "")
                    inXml.Append(fieldName + " in (");

                inXml.Append(query2);

                if (fieldName != "")
                    inXml.Append(")");
                inXml.Append("</Filter>");
            }

            inXml.Append("</Query>");
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("GetFilterParams: " + ex.Message);
        }
    }

    #endregion

    #region CreateTable 

    private void CreateTable()
    {
        try
        {
            String colName = "";
            rptTable = new DataTable();
            participantTable = new DataTable();
            subDetailTable = new DataTable(); //FB 2593
            childDetailTable = new DataTable();
            workOrderTable = new DataTable(); // ZD 102835

            for (Int32 t1 = 0; t1 < ds.Tables.Count; t1++)
            {
                DataTable colTable = ds.Tables[t1];

                if (colTable.Columns[0].ToString().ToLower() == "workorder_conferenceid") // ZD 102835
                {
                    workOrderTable = ds.Tables[t1];
                    continue;
                }

                for (Int32 i = 0; i < colTable.Columns.Count; i++)
                {
                    colName = "";
                    colName = colTable.Columns[i].ToString().ToLower();

                    if (colName == "endpointconnectstatus") //FB 2593
                        continue;

                    if (t1 == 0 && (colName == "column1" || colName == "confdate" || colName == "tzone" || colName == "orgname" || colName == "mcuname")) //FB 2593
                        continue;

                    if (colName.IndexOf("range") >= 0)
                    {
                        if (t1 == 0)
                            rptTable.Columns.Add(colTable.Columns[i].ToString(), typeof(Int32));
                        else if (t1 == 1) //Participant Table
                            participantTable.Columns.Add(colTable.Columns[i].ToString(), typeof(Int32));
                    }
                    else
                    {
                        if (t1 == 0)
                            rptTable.Columns.Add(colTable.Columns[i].ToString(), typeof(String));
                        else if (t1 == 1) //Participant Table
                            participantTable.Columns.Add(colTable.Columns[i].ToString(), typeof(String));
                        else if (t1 == 2)
                            subDetailTable.Columns.Add(colTable.Columns[i].ToString(), typeof(String)); //FB 2593
                        else if (t1 == 3)
                            childDetailTable.Columns.Add(colTable.Columns[i].ToString(), typeof(String)); //FB 2593
                    }
                }
            }
            for (Int32 t = 0; t < ds.Tables.Count; t++)
            {
                DataTable detailsTable = ds.Tables[t];

                if (detailsTable.Columns[0].ToString().ToLower() == "workorder_conferenceid") // ZD 102835
                {
                    //workOrderTable = ds.Tables[t];
                    continue;
                }

                if (t == 3 && hdnSubmitValue.Value == "6")
                {
                    BindChildTable(detailsTable);
                    continue;
                }

                string tmpConfUIID = "";
                for (Int32 i = 0; i < detailsTable.Rows.Count; i++)
                {
                    if (detailsTable.Columns.Contains("MCU Assignment") && detailsTable.Rows[i]["MCU Assignment"].ToString() == "") //FB 2593
                        continue;
                    //ZD 104686
                    if (detailsTable.Columns.Contains("ConfUID") && detailsTable.Columns.Contains("ConfID") && t == 0)
                    {
                        if (i + 1 < detailsTable.Rows.Count)
                        {
                            if (detailsTable.Rows[i]["ConfUID"].ToString() == detailsTable.Rows[i + 1]["ConfUID"].ToString())
                                detailsTable.Rows[i]["ConfID"] = "";
                        }
                    }
                    DataRow dataRow = null;

                    if (t == 0)
                        dataRow = rptTable.NewRow();
                    else if (t == 1)
                        dataRow = participantTable.NewRow();
                    else if (t == 2) //FB 2593
                        dataRow = subDetailTable.NewRow();

                    if (hdnSubmitValue.Value != "6")
                    {
                        if (detailsTable.Rows[0][0].ToString() == "")
                            break;
                    }

                    Int32 rCnt = 0;
                    if (t == 0)
                        rCnt = rptTable.Columns.Count;
                    else if (t == 1)
                        rCnt = participantTable.Columns.Count;
                    else if (t == 2) //FB 2593
                        rCnt = subDetailTable.Columns.Count;

                    for (int j = 0; j < rCnt; j++)
                    {
                        colName = "";
                        if (t == 0)
                            colName = rptTable.Columns[j].ColumnName.ToLower();
                        else if (t == 1)
                            colName = participantTable.Columns[j].ColumnName.ToLower();
                        else if (t == 2) //FB 2593
                            colName = subDetailTable.Columns[j].ColumnName.ToLower();

                        if (t == 0 && (colName == "category1" || colName == "d1" || colName == "tzone" || colName == "orgname" || colName == "mcuname")) //FB 2593
                            continue;

                        if (colName == "start" || colName == "end" || colName == "start time")
                        {
                            dataRow[j] = Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);

                            if (colName == "start time" && tformat != "HHmmZ" && hdnSubmitValue.Value != "7") //FB 2588 ZD 102835
                            {
                                if (dataRow[j].ToString() != "")
                                    dataRow[j] = Convert.ToDateTime(dataRow[j].ToString()).ToString("h:mmtt").ToLower() + ' ' + detailsTable.Rows[i]["tzone"].ToString();
                            }
                        }
                        else if (colName == "enddate" || colName == "start date" || colName == "date" || colName == "date of conf." || colName == "account expiration")
                        {
                            //ZD 100958
                            //dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate(detailsTable.Rows[i][colName].ToString());
                            DateTime fieldDate = Convert.ToDateTime(detailsTable.Rows[i][colName].ToString());
                            dataRow[j] = fieldDate.ToString(dtformat);
                        }
                        else if (colName == "connect time" || colName == "disconnect time" || colName == "scheduled start" || colName == "scheduled end") //FB 2593
                        {
                            if (detailsTable.Rows[i][colName].ToString() != "") //FB 2593
                            {
                                if (detailsTable.Rows[i][colName].ToString() == "N/A")
                                {
                                    dataRow[j] = obj.GetTranslatedText(detailsTable.Rows[i][colName].ToString()); //ZD 100288
                                }
                                else
                                {
                                    //dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate(detailsTable.Rows[i][colName].ToString())
                                    //ZD 100958
                                    //dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate((Convert.ToDateTime(detailsTable.Rows[i][colName].ToString())).ToString())
                                    //    + ' ' + Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);

                                    DateTime fieldDate = Convert.ToDateTime(detailsTable.Rows[i][colName].ToString());
                                    dataRow[j] = fieldDate.ToString(dtformat) + ' ' + Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);
                                }
                            }
                        }
                        else if (colName == "next poll in mins" && detailsTable.Rows[i][colName].ToString() != "" && detailsTable.Rows[i][colName].ToString() != "N/A") //FB 2944
                        {
                            //FB 2881
                            TimeSpan minu = (DateTime.Now).Subtract(Convert.ToDateTime(detailsTable.Rows[i][colName].ToString()));

                            if (Math.Abs(minu.TotalMinutes) >= 16) //FB 2944
                                dataRow[j] = obj.GetTranslatedText("N/A");
                            else
                                dataRow[j] = (15 - Convert.ToInt16(Math.Truncate(minu.TotalMinutes))).ToString(); //FB 2944
                        }
                        else if (colName == "secondary email")
                            dataRow[j] = (detailsTable.Rows[i][colName].ToString() == "") ? "-" : detailsTable.Rows[i][colName].ToString();
                        else if (colName == "duration")
                        {
                            int dur = 0; //ZD 101348
                            if (detailsTable.Rows[i][colName].ToString() == "N/A")
                                dataRow[j] = detailsTable.Rows[i][colName].ToString();
                            else
                            {
                                dur = Convert.ToInt32(detailsTable.Rows[i][colName].ToString());
                                //FB 2654
                                dataRow[j] = dur > 0 ? dur : 0;//obj.GetProperValue((dur / 60) + " hour(s) " + (dur % 60) + " min(s)");
                            }
                        }
                        else if (colName == "date/time signed-in")//ZD 102131 Start
                        {
                            if (!string.IsNullOrEmpty(detailsTable.Rows[i][colName].ToString()))
                            {
                                DateTime fieldDate = Convert.ToDateTime(detailsTable.Rows[i][colName].ToString());
                                dataRow[j] = Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);

                                if (tformat != "HHmmZ" && hdnSubmitValue.Value != "7")
                                {
                                    if (dataRow[j].ToString() != "")
                                        dataRow[j] = fieldDate.ToString(dtformat) + ' ' + Convert.ToDateTime(dataRow[j].ToString()).ToString("h:mmtt").ToLower();
                                }
                            }
                        } //ZD 102131 End
                        else
                            dataRow[j] = detailsTable.Rows[i][colName].ToString();

                        //ZD 102131 Start
                        if (colName == "attended conference" && detailsTable.Rows[i][colName].ToString() != "")
                        {
                            if (detailsTable.Rows[i][colName].ToString() == "1")
                            {
                                dataRow[j] = obj.GetTranslatedText("Yes");
                            }
                            else
                            {
                                dataRow[j] = obj.GetTranslatedText("No");
                            }
                        }

                        if (colName == "date/time signed-in")
                        {
                            if (detailsTable.Rows[i]["attended conference"].ToString() == "0")
                            {
                                dataRow[j] = "--------------";
                            }
                        }
                        //ZD 102131 End

                        //ZD 100288
                        if (dataRow[j].ToString() == "N/A")
                            dataRow[j] = obj.GetTranslatedText(dataRow[j].ToString());
                    }

                    if (t == 0)
                        rptTable.Rows.Add(dataRow);
                    else if (t == 1) //Participant Table
                        participantTable.Rows.Add(dataRow);
                    else if (t == 2) //FB 2593
                        subDetailTable.Rows.Add(dataRow);
					//ZD 104686
                    if (detailsTable.Columns.Contains("ConfUID") && detailsTable.Columns.Contains("ConfID") && t == 0)
                        tmpConfUIID = detailsTable.Rows[i]["ConfUID"].ToString();
                }
            }

            //foreach (DataColumn col in workOrderTable.Columns) // ZD 102835
            //    col.Caption = obj.GetTranslatedText(col.Caption);

            // ZD 102835 Start
            if (workOrderTable.Rows.Count > 0)
            {
                string colVal = "";
                foreach (DataColumn col in workOrderTable.Columns)
                {
                    colVal = col.ColumnName.ToString().ToLower();
                    if (!(colVal == "workorder_conferenceid" || colVal == "type" || colVal == "wo person-in-charge"))
                        rptTable.Columns.Add(col.ColumnName, typeof(String));
                    col.Caption = obj.GetTranslatedText(col.Caption);
                }


                DataTable tempWO = workOrderTable.Clone();
                string WO_Name = "", Inventory_Item = "", Quantity = "", Delivery_Cost = "", Service_Charge = "", Comment = "", Room = "", WO_Person_In_Charge = "";
                string WO_ConfID = "";
                DataRow drTempWO;
                for (int i = 0; i < workOrderTable.Rows.Count; i++)
                {
                    if (WO_ConfID != "" && WO_ConfID != workOrderTable.Rows[i]["workorder_conferenceid"].ToString())
                    {
                        drTempWO = tempWO.NewRow();
                        drTempWO["workorder_conferenceid"] = WO_ConfID;
                        drTempWO["type"] = "0";
                        drTempWO["WO Name"] = WO_Name;
                        drTempWO["Inventory Item"] = Inventory_Item;
                        drTempWO["Quantity"] = Quantity;
                        drTempWO["Delivery Cost"] = Delivery_Cost;
                        drTempWO["Service Charge"] = Service_Charge;
                        drTempWO["Comment"] = Comment;
                        drTempWO["Room"] = Room;
                        drTempWO["WO Person-in-Charge"] = WO_Person_In_Charge;
                        tempWO.Rows.Add(drTempWO);

                        WO_Name = ""; Inventory_Item = ""; Quantity = ""; Delivery_Cost = ""; Service_Charge = ""; Comment = ""; Room = ""; WO_Person_In_Charge = "";
                    }

                    WO_ConfID = workOrderTable.Rows[i]["workorder_conferenceid"].ToString();

                    if(WO_Name == "")
                        WO_Name = workOrderTable.Rows[i]["WO Name"].ToString();
                    else
                        WO_Name += "," + workOrderTable.Rows[i]["WO Name"].ToString();

                    if (Inventory_Item == "")
                        Inventory_Item = workOrderTable.Rows[i]["Inventory Item"].ToString();
                    else
                        Inventory_Item += "," + workOrderTable.Rows[i]["Inventory Item"].ToString();

                    if (Quantity == "")
                        Quantity = workOrderTable.Rows[i]["Quantity"].ToString();
                    else
                        Quantity += "," + workOrderTable.Rows[i]["Quantity"].ToString();

                    if (Delivery_Cost == "")
                        Delivery_Cost = workOrderTable.Rows[i]["Delivery Cost"].ToString();
                    else
                        Delivery_Cost += "," + workOrderTable.Rows[i]["Delivery Cost"].ToString();

                    if (Service_Charge == "")
                        Service_Charge = workOrderTable.Rows[i]["Service Charge"].ToString();
                    else
                        Service_Charge += "," + workOrderTable.Rows[i]["Service Charge"].ToString();

                    if (Comment == "")
                        Comment = workOrderTable.Rows[i]["Comment"].ToString();
                    else
                        Comment += "," + workOrderTable.Rows[i]["Comment"].ToString();

                    if (Room == "")
                        Room = workOrderTable.Rows[i]["Room"].ToString();
                    else
                        Room += "," + workOrderTable.Rows[i]["Room"].ToString();

                    if (WO_Person_In_Charge == "")
                        WO_Person_In_Charge = workOrderTable.Rows[i]["WO Person-in-Charge"].ToString();
                    else
                        WO_Person_In_Charge += "," + workOrderTable.Rows[i]["WO Person-in-Charge"].ToString();

                }

                drTempWO = tempWO.NewRow();
                drTempWO["workorder_conferenceid"] = WO_ConfID;
                drTempWO["type"] = "0";
                drTempWO["WO Name"] = WO_Name;
                drTempWO["Inventory Item"] = Inventory_Item;
                drTempWO["Quantity"] = Quantity;
                drTempWO["Delivery Cost"] = Delivery_Cost;
                drTempWO["Service Charge"] = Service_Charge;
                drTempWO["Comment"] = Comment;
                drTempWO["Room"] = Room;
                drTempWO["WO Person-in-Charge"] = WO_Person_In_Charge;
                tempWO.Rows.Add(drTempWO);


                string criteria = "";
                DataRow[] woResult;
                foreach (DataRow row in rptTable.Rows)
                {
                    criteria = "workorder_conferenceid = " + row["ConfUID"].ToString();
                    woResult = workOrderTable.Select(criteria);
                    if (woResult.Length > 0)
                    {
                        row["WO Name"] = woResult[0]["WO Name"].ToString();
                        row["Inventory Item"] = woResult[0]["Inventory Item"].ToString();
                        row["Quantity"] = woResult[0]["Quantity"].ToString();
                        row["Delivery Cost"] = woResult[0]["Delivery Cost"].ToString();
                        row["Service Charge"] = woResult[0]["Service Charge"].ToString();
                        row["Comment"] = woResult[0]["Comment"].ToString();
                        row["Room"] = woResult[0]["Room"].ToString();
                    }
                }
                                
            }
            // ZD 102835 End
        }
        catch (Exception ex)
        {
            log.Trace("CreateTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    //FB 2593

    #region BindChildTable
    private void BindChildTable(DataTable detailsTable)
    {
        try
        {
            Boolean isDisconnect = false;
            if (detailsTable != null && detailsTable.Rows.Count > 0)
            {
                for (Int32 i = 0; i < detailsTable.Rows.Count; i++)
                {
                    DataRow dataRow = null;
                    dataRow = childDetailTable.NewRow();
                    Int32 rCnt = 0;
                    String colName = "";
                    rCnt = childDetailTable.Columns.Count;
                    isDisconnect = false;
                    Int32 n = 0;
                    Boolean isLast = false;
                    if (i == detailsTable.Rows.Count - 1)
                    {
                        n = i;
                        isLast = true;
                    }
                    else
                        n = i + 1;
                    for (int j = 0; j < rCnt; j++)
                    {
                        colName = childDetailTable.Columns[j].ColumnName.ToLower();

                        if (colName == "disconnect time" || colName == "disconnect reason")
                        {
                            if (detailsTable.Rows[n]["Endpointconnectstatus"].ToString() == "2")
                            {
                                if (colName == "disconnect time")
                                {
                                    if (detailsTable.Rows[n][colName].ToString() == "" || detailsTable.Rows[n][colName].ToString() == "N/A") //FB 2593
                                        dataRow[j] = obj.GetTranslatedText("N/A");
                                    else
                                    {
                                        dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate((Convert.ToDateTime(detailsTable.Rows[n][colName].ToString())).ToString())
                                            + ' ' + Convert.ToDateTime(detailsTable.Rows[n][colName]).ToString(tformat);
                                    }
                                }
                                else
                                {
                                    dataRow[j] = detailsTable.Rows[n][colName].ToString();
                                }
                                isDisconnect = true;

                                if (isLast == false && n + 1 <= detailsTable.Rows.Count - 1)
                                {
                                    if (detailsTable.Rows[n + 1]["Endpointconnectstatus"].ToString() == "2")
                                        isDisconnect = false;
                                }
                            }
                            else
                                dataRow[j] = "";
                        }
                        else if (colName == "connect time")
                        {
                            if (detailsTable.Rows[i][colName].ToString() == "" || detailsTable.Rows[i][colName].ToString() == "N/A") //FB 2593
                                dataRow[j] = obj.GetTranslatedText("N/A");
                            else
                            {
                                //dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate(detailsTable.Rows[i][colName].ToString())
                                dataRow[j] = myVRMNet.NETFunctions.GetFormattedDate((Convert.ToDateTime(detailsTable.Rows[i][colName].ToString())).ToString())
                                    + ' ' + Convert.ToDateTime(detailsTable.Rows[i][colName]).ToString(tformat);
                            }

                        }
                        else
                            dataRow[j] = detailsTable.Rows[i][colName].ToString();

                        //ZD 100288
                        if (dataRow[j].ToString() == "N/A")
                            dataRow[j] = obj.GetTranslatedText(dataRow[j].ToString());

                    }

                    childDetailTable.Rows.Add(dataRow);

                    if (isDisconnect)
                        i = i + 1;
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("BindChildTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }
    #endregion

    #region CreateGridColumns 

    private void CreateGridColumns()
    {
        try
        {
            GridViewDataColumn dataColumn = new GridViewDataColumn();
            //MainGrid.Columns.Clear();
            //MainGrid.DataBind();
            for (Int32 i = 0; i < rptTable.Columns.Count; i++)
            {
                String colName = "";
                dataColumn = new GridViewDataColumn();

                colName = rptTable.Columns[i].ColumnName;
                dataColumn.Caption = obj.GetTranslatedText(colName);  //ZD 100288
                dataColumn.FieldName = colName;

                dataColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
                //dataColumn.HeaderStyle.CssClass = "templateCaption";

                if (colName.ToLower() == "department name")
                    dataColumn.Width = Unit.Pixel(110);
                else if (colName.ToLower() == "conf.id")
                    dataColumn.Width = Unit.Pixel(60);

                ////else if (colName.ToLower() == "duration (minutes)")
                ////    dataColumn.Width = Unit.Percentage(5);
                //else if (colName.ToLower() == "company relationship")
                //    dataColumn.Width = Unit.Percentage(10);
                //else if (colName.ToLower() == "conf.title")
                //    dataColumn.Width = Unit.Percentage(10);
                //else if (colName.ToLower() == " date of conf.")
                //    dataColumn.Width = Unit.Percentage(10);
                //else
                //    dataColumn.Width = Unit.Percentage(5);

                if (colName.ToLower() == "confid")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "confuid") // ZD 102835
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "roomid")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "endpointid")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "bridgeid")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "userid")
                    dataColumn.Visible = false;
 				// ZD 102835 Start
                else if (colName.ToLower() == "wo name")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "inventory item")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "quantity")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "delivery cost")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "service charge")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "comment")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "room")
                    dataColumn.Visible = false;
				// ZD 102835 End
                else if (colName.ToLower() == "rooms in conf")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "# of attendees" || colName.ToLower() == "# of attendees.") //ZD 104686
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "host room")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "total travel avoided")
                    dataColumn.Visible = false;
                else if (colName.ToLower() == "room name")//ZD 104686
                    dataColumn.Visible = false;
                
                dataColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;

                //if (ConfScheRptDivList.Value.ToString() != "1")
                //    dataColumn.CellStyle.CssClass = "";
                //else
                //    dataColumn.CellStyle.CssClass = "templateTable";

                MainGrid.Columns.Add(dataColumn);
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateGridColumns" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region menuExport_OnItemClick
    /// <summary>
    /// menuExport_OnItemClick
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void menuExport_OnItemClick(object sender, DevExpress.Web.ASPxMenu.MenuItemEventArgs e)
    {
        try
        {
            errLabel.Text = "";
            //ZD 104686
            MainGrid.DataSource = filterRptTable;
            MainGrid.DataBind();

            // ZD 102835 Start
            if (MainGrid.Columns["WO Name"] != null && Session["hdnhdnSubmitValue"] != null && Session["hdnhdnSubmitValue"].ToString() == "1")
            {
                MainGrid.Columns["WO Name"].Visible = true;
                MainGrid.Columns["Inventory Item"].Visible = true;
                MainGrid.Columns["Quantity"].Visible = true;
                MainGrid.Columns["Delivery Cost"].Visible = true;
                MainGrid.Columns["Service Charge"].Visible = true;
                MainGrid.Columns["Comment"].Visible = true;
                MainGrid.Columns["Room"].Visible = true;
            }
            // ZD 102835 End
            //ZD 103216
          
            if (MainGrid.Columns["Rooms In Conf"] != null)
                MainGrid.Columns["Rooms In Conf"].Visible = false;

            if (Session["EnableTravelAvoidTrack"].ToString() == "1")
            {
                //ZD 104686
                if (MainGrid.Columns["Rooms In Conf."] != null)
                    MainGrid.Columns["Rooms In Conf."].Visible = false;
                if (MainGrid.Columns["Host Room"] != null)
                    MainGrid.Columns["Host Room"].Visible = true;
                if (MainGrid.Columns["Total Travel Avoided"] != null)
                    MainGrid.Columns["Total Travel Avoided"].Visible = true;
                //if (MainGrid.Columns["Rooms In Conf - # of Attendees"] != null)
                //    MainGrid.Columns["Rooms In Conf - # of Attendees"].Visible = true;
                if (MainGrid.Columns["# of Attendees"] != null) //ZD 104686
                    MainGrid.Columns["# of Attendees"].Visible = false;
                if (MainGrid.Columns["# of Attendees."] != null) //ZD 104686                    
                    MainGrid.Columns["# of Attendees."].Visible = true;
                if (MainGrid.Columns["Room Name"] != null) //ZD 104686                    
                    MainGrid.Columns["Room Name"].Visible = true;
            }
            else
            {
				//ZD 104686
                if (MainGrid.Columns["Rooms In Conf."] != null)
                    MainGrid.Columns["Rooms In Conf."].Visible = false;

                if (MainGrid.Columns["Room Name"] != null) //ZD 104686                    
                    MainGrid.Columns["Room Name"].Visible = true;
            }

            MainGrid.DataBind();
            
            if (e.Item.Name == "mXls")
                ExportExcel(null, null);
            else if (e.Item.Name == "mPdf")
                ExportPDF(null, null);
            else if (e.Item.Name == "mDoc")
                gridExport.WriteRtfToResponse();
        }
        catch (System.Threading.ThreadAbortException thex)
        {
            log.Trace("menuExport_OnItemClick" + thex.StackTrace);
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(" menuExport_OnItemClick" + ex.Message);//ZD 100263

        }
    }
    #endregion

    #region ExportExcel

    protected void ExportExcel(object sender, EventArgs e)
    {
        try
        {
            gridExport.WriteXlsToResponse();

            //string fileName = @"F:\Test.xls";
            //FileStream fileStream = new FileStream(fileName, FileMode.Create);
            //gridExport.WriteXls(fileStream);
            //fileStream.Close();
            //MainGrid.DataBind();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region ExportPDF

    protected void ExportPDF(object sender, EventArgs e)
    {
        try
        {
            //gridExport.WritePdfToResponse();

            /*
            if (ReportsList.Value.ToString() == "4" && (DrpChrtType.Value.ToString() == "1" || DrpChrtType.Value.ToString() == "2"))
            {
                PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());

                pcl.PageHeaderFooter = new PageHeaderFooter(new PageHeaderArea(new string[] { "A", "Header" }, SystemFonts.DialogFont, BrickAlignment.Center), new PageFooterArea(new string[] { "B" }, SystemFonts.DialogFont, BrickAlignment.Center));

                pcl.Component = ((IChartContainer)webChartCtrl).Chart;
                pcl.Landscape = true;
                ((Chart)pcl.Component).OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
                pcl.CreateDocument();

                MemoryStream stream = new MemoryStream();
                pcl.PrintingSystem.ExportToPdf(stream);

                bool inline = false;

                Response.ContentType = "application/pdf";
                Response.AddHeader("Accept-Header", stream.Length.ToString());
                Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + lblHeading.Text.Trim() + ".pdf");
                Response.AddHeader("Content-Length", stream.Length.ToString());
                Response.BinaryWrite(stream.ToArray());
                Response.End();
                pcl.Dispose();

            }
            else
            {
             * */
            DevExpress.Web.ASPxGridView.Export.Helper.GridViewLink link = new DevExpress.Web.ASPxGridView.Export.Helper.GridViewLink(gridExport);
            PrintingSystem ps = link.CreatePS();
            ps.PageSettings.Landscape = true;
            ps.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            ps.ExportToPdf(stream);
            WriteToResponse("filename", true, "pdf", stream);
            //}
        }
        catch (System.Threading.ThreadAbortException)
        { }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region WriteToResponse
    protected void WriteToResponse(string fileName, bool saveAsFile, string fileFormat, MemoryStream stream)
    {
        if (Page == null || Page.Response == null) return;
        string disposition = saveAsFile ? "attachment" : "inline";
        Page.Response.Clear();
        Page.Response.Buffer = false;
        Page.Response.AppendHeader("Content-Type", string.Format("application/{0}", fileFormat));
        Page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
        Page.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename={1}.{2}", disposition, HttpUtility.UrlEncode(fileName).Replace("+", "%20"), fileFormat));
        if (stream.Length > 0)
            Page.Response.BinaryWrite(stream.ToArray());
    }

    #endregion

    #region PrintReport

    protected void PrintReport(object sender, EventArgs e)
    {
        try
        {
            String pageURL = "";
            DataGrid grid = new DataGrid();
            Type cstype = this.GetType();

            grid.DataSource = MainGrid.DataSource;
            grid.DataBind();

            if (Session["PrintGrid"] == null)
                Session.Add("PrintGrid", grid);
            else
                Session["PrintGrid"] = grid;

            if (Session["PrintTable"] == null)
                Session.Add("PrintTable", MainGrid.DataSource);
            else
                Session["PrintTable"] = MainGrid.DataSource;

            if (Session["reportType"] == null)
                Session.Add("reportType", "RptDtal");
            else
                Session["reportType"] = "RptDtal";

            if (Session["titleString"] == null)
                Session.Add("titleString", lblHeading.Text);
            else
                Session["titleString"] = lblHeading.Text;

            pageURL = "<script>window.open('PrintInterface.aspx')</script>";
            ClientScript.RegisterStartupScript(cstype, "Print", pageURL);

        }
        catch (System.Threading.ThreadAbortException)
        { }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

	//FB 2808
    #region SaveReport

    protected void SaveReport(object sender, EventArgs e)
    {
        XmlDocument loadReports = null;
        XmlNodeList rptList = null;
        Boolean newrpt = true;
        String hdnvalue = "";
        try
        {
            hdnSave.Value = "";

            if (File.Exists(xmlPath))
            {
                loadReports = new XmlDocument();
                loadReports.Load(xmlPath);
            }

            if (loadReports == null)
            {
                loadReports = new XmlDocument();
                String rptXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Report><Conference></Conference><User></User><Rooms></Rooms><Endpoints></Endpoints><MCU></MCU><CDR></CDR></Report>";
                loadReports.LoadXml(rptXML);
            }

            hdnvalue = hdnSubmitValue.Value;

            if (hdnvalue == "1")
                rptList = loadReports.SelectNodes("Report/Conference/ReportSring");
            else if (hdnvalue == "2")
                rptList = loadReports.SelectNodes("Report/User/ReportSring");
            else if (hdnvalue == "3")
                rptList = loadReports.SelectNodes("Report/Rooms/ReportSring");
            else if (hdnvalue == "4")
                rptList = loadReports.SelectNodes("Report/Endpoints/ReportSring");
            else if (hdnvalue == "5")
                rptList = loadReports.SelectNodes("Report/MCU/ReportSring");
            else if (hdnvalue == "6")//FB 2593
                rptList = loadReports.SelectNodes("Report/CDR/ReportSring");
            else if (hdnvalue == "7") // ZD 102835
                rptList = loadReports.SelectNodes("Report/WorkOrder/ReportSring");

            if (rptList != null)
            {
                XmlDocument xmlDocument = new XmlDocument();
                if (hdnReportSelection.Value != "")
                {
                    XmlNode confNode = null;

                    if (hdnvalue == "1")
                        confNode = loadReports.SelectSingleNode("Report/Conference");
                    else if (hdnvalue == "2")
                        confNode = loadReports.SelectSingleNode("Report/User");
                    else if (hdnvalue == "3")
                        confNode = loadReports.SelectSingleNode("Report/Rooms");
                    else if (hdnvalue == "4")
                        confNode = loadReports.SelectSingleNode("Report/Endpoints");
                    else if (hdnvalue == "5")
                        confNode = loadReports.SelectSingleNode("Report/MCU");
                    else if (hdnvalue == "6") //FB 2593
                        confNode = loadReports.SelectSingleNode("Report/CDR");
                    else if (hdnvalue == "7") // ZD 102835
                        confNode = loadReports.SelectSingleNode("Report/WorkOrder");

                    String[] delrptary = hdnReportSelection.Value.Split(',');
                    XmlNode smallNode = null;

                    //for (Int32 i = 0; i < delrptary.Length; i++)
                    for (Int32 i = delrptary.Length - 1; i >= 0; i = i - 1)
                    {
                        smallNode = confNode.ChildNodes[Int32.Parse(delrptary[i])];
                        confNode.RemoveChild(smallNode);
                    }

                    //confNode.InnerXml += xmlDocument.InnerXml;
                    loadReports.SelectSingleNode("Report").AppendChild(confNode);

                    if (File.Exists(xmlPath))
                    {
                        if (obj.WaitForFile(xmlPath))
                        {
                            File.Delete(xmlPath);
                            loadReports.Save(xmlPath);
                        }
                    }

                    RetrieveReport();
                    lstReportList.Items.Clear(); //FB 2808
                    ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>fnShow(" + hdnvalue + ");</script>");
                    hdnReportSelection.Value = "";

                    errLabel.Text = obj.GetTranslatedText("Deleted Successfully."); //FB 2808
                    hdnRptSave.Value = "";
                }
                else
                {
                    Session["rptSave"] = "";

                    if (rptList.Count > 0)
                    {
                        for (int a = 0; a < rptList.Count; a++)
                        {
                            if (rptList[a].SelectSingleNode("Name").InnerText.Trim() == txtSaveReport.Text.Trim())
                                newrpt = false;
                        }
                    }
                    else
                        newrpt = true;

                    if (newrpt)
                    {
                        xmlDocument = new XmlDocument();

                        XmlNode rptNode = xmlDocument.CreateElement("ReportSring");
                        xmlDocument.AppendChild(rptNode);

                        XmlElement elem = null;
                        elem = xmlDocument.CreateElement("Name");
                        elem.InnerText = txtSaveReport.Text.Trim();
                        rptNode.AppendChild(elem);

                        elem = xmlDocument.CreateElement("MainMenuValue");
                        elem.InnerText = hdnMainIPValue.Value;
                        rptNode.AppendChild(elem);

                        elem = xmlDocument.CreateElement("RightMenuValue");
                        elem.InnerText = hdnRightMenuValue.Value;
                        rptNode.AppendChild(elem);

                        xmlDocument.AppendChild(rptNode);

                        XmlNode confNode = null;
                        String nodeName = "";
                        if (hdnvalue == "1")
                        {
                            confNode = loadReports.SelectSingleNode("Report/Conference");
                            nodeName = "Conference";
                        }
                        else if (hdnvalue == "2")
                        {
                            confNode = loadReports.SelectSingleNode("Report/User");
                            nodeName = "User";
                        }
                        else if (hdnvalue == "3")
                        {
                            confNode = loadReports.SelectSingleNode("Report/Rooms");
                            nodeName = "Rooms";
                        }
                        else if (hdnvalue == "4")
                        {
                            confNode = loadReports.SelectSingleNode("Report/Endpoints");
                            nodeName = "Endpoints";
                        }
                        else if (hdnvalue == "5")
                        {
                            confNode = loadReports.SelectSingleNode("Report/MCU");
                            nodeName = "MCU";
                        }
                        else if (hdnvalue == "6") //FB 2593
                        {
                            confNode = loadReports.SelectSingleNode("Report/CDR");
                            nodeName = "CDR";
                        }
                        else if (hdnvalue == "7") // ZD 102835
                        {
                            confNode = loadReports.SelectSingleNode("Report/WorkOrder");
                            nodeName = "WorkOrder";
                        }

                        if (confNode == null)
                            confNode = loadReports.CreateNode(XmlNodeType.Element, nodeName, "");

                        confNode.InnerXml += xmlDocument.InnerXml;
                        loadReports.SelectSingleNode("Report").AppendChild(confNode);

                        if (File.Exists(xmlPath))
                        {
                            if (obj.WaitForFile(xmlPath))
                            {
                                File.Delete(xmlPath);
                                loadReports.Save(xmlPath);
                            }
                        }
                        else
                            loadReports.Save(xmlPath);

                        RetrieveReport();

                        errLabel.Text = obj.GetTranslatedText("Saved Successfully.");
                        hdnRptSave.Value = "";
                        return;
                    }
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("Report name is already exists.");
                        return;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("SaveReport" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region RetrieveReport

    private void RetrieveReport()
    {
        String xmlPath = "";
        XmlDocument loadReports = null;
        XmlNodeList rptList = null;
        Boolean newrpt = false;
        String name = "";
        String mainValue = "";
        String rightvalue = "";
        try
        {
            xmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en\\" +
                Session["RoomXmlPath"].ToString().Replace("Rooms/Room.xml", "Reports.xml");

            if (File.Exists(xmlPath))
            {
                loadReports = new XmlDocument();
                loadReports.Load(xmlPath);
            }

            hdnReportNames.Value = "";
            hdnAllReports.Value = "";

            if (loadReports != null)
            {
                for (Int32 r = 1; r <= 7; r++) // ZD 102835
                {
                    if (r == 1)
                        rptList = loadReports.SelectNodes("Report/Conference/ReportSring");
                    else if (r == 2)
                        rptList = loadReports.SelectNodes("Report/User/ReportSring");
                    else if (r == 3)
                        rptList = loadReports.SelectNodes("Report/Rooms/ReportSring");
                    else if (r == 4)
                        rptList = loadReports.SelectNodes("Report/Endpoints/ReportSring");
                    else if (r == 5)
                        rptList = loadReports.SelectNodes("Report/MCU/ReportSring");
                    else if (r == 6)//FB 2593
                        rptList = loadReports.SelectNodes("Report/CDR/ReportSring");
                    else if (r == 7) // ZD 102835
                        rptList = loadReports.SelectNodes("Report/WorkOrder/ReportSring");

                    if (rptList != null)
                    {
                        if (rptList.Count > 0)
                        {
                            for (int a = 0; a < rptList.Count; a++)
                            {
                                name = rptList[a].SelectSingleNode("Name").InnerText;
                                mainValue = rptList[a].SelectSingleNode("MainMenuValue").InnerText;
                                rightvalue = rptList[a].SelectSingleNode("RightMenuValue").InnerText;

                                //1�confname�1:5,5�3:5.6�confname�1:5,5�3:5.6� �
                                //2�confname�1:5,5�3:5.6�confname�1:5,5�3:5.6�                           
                                // 1. � / 2. � / 3. � / 4. �

                                if (a == 0)
                                {
                                    if (hdnAllReports.Value == "")
                                        hdnAllReports.Value = r.ToString() + "�";
                                    else
                                        hdnAllReports.Value += "�" + r.ToString() + "�"; // � - Alt + 0213, � - Alt + 0212 
                                }

                                hdnAllReports.Value += name + "�" + mainValue + "??" + rightvalue + "�"; //� + Alt + 0214, � + Alt + 0196                            

                                if (hdnReportNames.Value == "")
                                    hdnReportNames.Value = name;
                                else
                                    hdnReportNames.Value += "�" + name;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("RetrieveReport" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region PollReport
    /// <summary>
    /// PollReport
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PollReport(object sender, EventArgs e)
    {
        try
        {
            String InXML = "<Login><UserID>11</UserID></Login>";
            String OutXML = obj.CallCommand("CallDetailRecords", InXML);
            if (OutXML.IndexOf("<error>") >= 0) 
            {
                errLabel.Text = obj.ShowErrorMessage(OutXML);
                errLabel.Visible = true;
                return;
            }
            else
            {
                errLabel.Text = obj.ShowSuccessMessage();
                errLabel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            log.Trace("PollReport" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion
}