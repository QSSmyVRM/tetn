/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Configuration;


public partial class en_Transcoding1 : System.Web.UI.Page
{
    #region Private Data Members
    /// <summary>
    /// Private Data members
    /// </summary>

    private String sessionXML = "";
	//FB 2450
    //public msxml4_Net.DOMDocument40Class XmlDoc = null;
    //public msxml4_Net.IXMLDOMNode node = null;
    //public msxml4_Net.IXMLDOMNodeList nodes = null;

    public XmlDocument XmlDoc = null;
    public XmlNode node = null;
    public XmlNodeList nodes = null;

    private object CheckFileExists = null;
    private String errorMessage = "";
    private Int32 i = 0;

   

    private object subnode = null;
    private object subnotes = null;
    private Int32 length = 0;
    object sublength = null;
    private String[] vsIDs = null;
    private String[] vsNames = null;
    private String[] aaIDs = null;
    private String[] aaNames = null;
    private String[] lrIDs = null;
    private String[] lrNames = null;
    private String[] vpIDs = null;
    private String[] vpNames = null;
    private String[] audioIDs = null;
    private String[] audioNames = null;
    private ns_Logger.Logger log = null;

    protected String imageFiles = "";
    protected String imageFilesBT = "";
    protected String fileName = "";
    protected String suffix = "";
    //protected Int32 rowsize = 4;
    protected String DISPLAY_LAYOUT_IMAGE_PATH = "image/displaylayout/";
    myVRMNet.NETFunctions obj;//FB 1881
    #endregion

    # region Protected Members
    protected System.Web.UI.WebControls.DropDownList VideoProtocol;
    protected System.Web.UI.WebControls.DropDownList restrictUsage;
    protected System.Web.UI.WebControls.DropDownList VideoSession;
    protected System.Web.UI.WebControls.DropDownList AudioAlgorithm;
    protected System.Web.UI.WebControls.DropDownList LineRate;
    protected System.Web.UI.HtmlControls.HtmlInputHidden Hidden1;
    protected System.Web.UI.HtmlControls.HtmlInputHidden confID;
    protected System.Web.UI.HtmlControls.HtmlInputHidden Hidden2;
    protected System.Web.UI.HtmlControls.HtmlInputHidden rowsize;
    protected System.Web.UI.HtmlControls.HtmlInputHidden selvsName;
    protected System.Web.UI.HtmlControls.HtmlInputCheckBox dualStreamMode1;
    protected System.Web.UI.HtmlControls.HtmlInputCheckBox dualStreamMode;
    protected System.Web.UI.HtmlControls.HtmlInputText maxVideo;
    protected System.Web.UI.HtmlControls.HtmlInputText maxAudio;
    //ZD 101931 start
    protected System.Web.UI.HtmlControls.HtmlTableCell tdVideoDisplay;
    protected System.Web.UI.HtmlControls.HtmlTableCell tdimgVideoDisplay;
    //ZD 101931 End
    # endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page Load
    /// <summary>
    /// Page Load Event handler
    /// </summary>

    protected void Page_Load(object sender, EventArgs e)
    {
        log = new ns_Logger.Logger();
        obj = new myVRMNet.NETFunctions(); //FB 1881
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("Transcoding1.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            GetImgForClient();
            QueryStrings();
            BuildXML();
            TranscodingIncFile();

            //ZD 101931 start
            if (Session["ShowVideoLayout"] != null && !string.IsNullOrEmpty(Session["ShowVideoLayout"].ToString()) && (Session["ShowVideoLayout"].ToString() == "1" || Session["ShowVideoLayout"].ToString() == "0"))
            {
                tdVideoDisplay.Attributes.Add("style", "Display:;");
                tdimgVideoDisplay.Attributes.Add("style", "Display:;");
            }
            else
            {
                tdVideoDisplay.Attributes.Add("style", "Display:None;");
                tdimgVideoDisplay.Attributes.Add("style", "Display:None;");
            }
            //ZD 101931 End
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
            log = null;
        }
    }
    #endregion

    #region BuildXML
    /// <summary>
    /// Build InputXML -Get the Session XML or build XML,from mangetemplate2.asp to bind values 
    /// </summary>
    private void BuildXML()
    {      
        if (Session["transcodingXML"] != null)
        {
            if (Session["transcodingXML"].ToString() != "")
                sessionXML = Session["transcodingXML"].ToString();

        }

    }
    #endregion

    #region Transcoding Included File
    /// <summary>
    /// The Included File 'en/inc/transcodinginc.asp' is defined as method
    /// </summary>
    private void TranscodingIncFile()
    {
		//FB 2450
        //XmlDoc = new msxml4_Net.DOMDocument40Class();
        XmlDoc = new XmlDocument();
        try
        {
            if (sessionXML != "")
            {
              //FB 2450 - Start
               // XmlDoc.async = false;
                XmlDoc.LoadXml(Convert.ToString(sessionXML));

                if (XmlDoc.InnerText == "" || XmlDoc.InnerText.IndexOf("<error>") >= 0) // if (XmlDoc.parseError.errorCode != 0)
                {
                    if (Convert.ToString(Session["who_use"]) == "VRM")
                    {
                        //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";//FB 1881
                        //errorMessage = errorMessage + Convert.ToString(Session["outXML"]);
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXML"]));
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        //CatchErrors(errorMessage);
                        Response.Redirect("underconstruction.aspx");
                    }
                    else
                    {
                        //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                        ////Response.Write("<br><br><p align='left'><font size=2><b>" + Convert.ToString(Transfer[Session["outXML"]]) + "<b></font></p>");
                        //Response.Write("<br><br><p align='left'><font size=2><b>" + sessionXML + "<b></font></p>");
                        log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + sessionXML);
                        errorMessage  = obj.ShowSystemMessage();//FB 1881
                        Response.Write(errorMessage);
                    }
                    XmlDoc = null;
                    Response.End();
                }
                if (Request.QueryString["from"].ToString() != null)
                {
                    if ((Request.QueryString["from"].ToString() == "te"))
                    {
                        nodes = XmlDoc.SelectNodes("/template/confInfo/videoSession/session");
                    }
                    else
                    {
                        nodes = XmlDoc.SelectNodes("/conference/confInfo/videoSession/session");
                    }
                }
                length = nodes.Count;
                vsIDs = new string[length - 1 + 1];
                vsNames = new string[length - 1 + 1];

                for (i = 0; i <= length - 1; i += 1)
                {
                    node = nodes[i]; //.nextNode();
                    vsIDs[i] = node.SelectSingleNode("videoSessionID").InnerText;
                    vsNames[i] = node.SelectSingleNode("videoSessionID").InnerText + "|" + node.SelectSingleNode("videoSessionName").InnerText;
                }
                BindDropdownList(VideoSession, vsNames);
                if ((Request.QueryString["from"].ToString() == "te"))
                {
                    nodes = XmlDoc.SelectNodes("/template/confInfo/audioAlgorithm/audio");
                }
                else
                {
                    nodes = XmlDoc.SelectNodes("/conference/confInfo/audioAlgorithm/audio");
                }
                length = nodes.Count;
                aaIDs = new string[length - 1 + 1];
                aaNames = new string[length - 1 + 1];
                for (i = 0; i <= length - 1; i += 1)
                {
                    node = nodes[i]; //.nextNode();
                    aaIDs[i] = node.SelectSingleNode("audioAlgorithmID").InnerText;
                    aaNames[i] = node.SelectSingleNode("audioAlgorithmID").InnerText + "|" + node.SelectSingleNode("audioAlgorithmName").InnerText;
                }
                
                BindDropdownList(AudioAlgorithm, aaNames);
                if ((Request.QueryString["from"].ToString() == "te"))
                {
                    nodes = XmlDoc.SelectNodes("/template/confInfo/lineRate/rate");
                }
                else
                {
                    nodes = XmlDoc.SelectNodes("/conference/confInfo/lineRate/rate");
                }
                length = nodes.Count;
                lrIDs = new string[length - 1 + 1];
                lrNames = new string[length - 1 + 1];
                for (i = 0; i <= length - 1; i += 1)
                {
                    node = nodes[i]; //.nextNode();
                    lrIDs[i] = node.SelectSingleNode("lineRateID").InnerText;
                    lrNames[i] = node.SelectSingleNode("lineRateID").InnerText + "|" + node.SelectSingleNode("lineRateName").InnerText;
                }
                if (((Convert.ToString(Application["Client"])).ToUpper() == "SCTECH"))
                {
                    if (((Convert.ToString(lrNames[i]).IndexOf("512") > 0) || (Convert.ToString(lrNames[i]).IndexOf("768") > 0)))
                    {
                        BindDropdownList(LineRate, lrNames);

                    }
                }
                else
                {
                    BindDropdownList(LineRate, lrNames);
                }

                if ((Request.QueryString["from"] == "te"))
                {
                    nodes = XmlDoc.SelectNodes("/template/confInfo/videoProtocol/video");
                }
                else
                {
                    nodes = XmlDoc.SelectNodes("/conference/confInfo/videoProtocol/video");
                }
                length = nodes.Count;
                vpIDs = new string[length - 1 + 1];
                vpNames = new string[length - 1 + 1];
                for (i = 0; i <= length - 1; i += 1)
                {
                    node = nodes[i]; //.nextNode();
                    vpIDs[i] = node.SelectSingleNode("videoProtocolID").InnerText;
                    vpNames[i] = node.SelectSingleNode("videoProtocolID").InnerText + "|" + node.SelectSingleNode("videoProtocolName").InnerText;
                }
                //BindDropdownList(VideoProtocol, vpNames); FB 1721
                if ((Request.QueryString["from"] == "te"))
                {
                    nodes = XmlDoc.SelectNodes("/template/confInfo/audioUsage/audio");
                }
                else
                {
                    nodes = XmlDoc.SelectNodes("/conference/confInfo/audioUsage/audio");
                }
                length = nodes.Count;
                audioIDs = new string[length - 1 + 1];
                audioNames = new string[length - 1 + 1];
                for (i = 0; i <= length - 1; i += 1)
                {
                    node = nodes[i];//.nextNode();
                    audioIDs[i] = node.SelectSingleNode("audioUsageID").InnerText;
                    audioNames[i] = node.SelectSingleNode("audioUsageID").InnerText + "|" + node.SelectSingleNode("audioUsageName").InnerText;
                }
				//FB 2450 - ENd
                BindDropdownList(restrictUsage, audioNames);
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    #region Check File
    /*
    /// <summary>
    /// Checking File Whether Exist or Not
    /// </summary>
    /// <param name="ffullname"></param>
    /// <returns></returns>
    object chkfile(object ffullname)
    {
        return CheckFileExists[Strings.Replace(Server.MapPath(Convert.ToString(ffullname)), "\\", "\\\\", 1, -1, CompareMethod.Binary)];
    }
     */


    #endregion

    #region Client Validating
    private void ClientValidation()
    {
        try
        {
            //object CheckFileExists = null;
            //int rowsize = 0;
            //string imageFiles = "";
            //string imageFilesBT = "";
            //object DISPLAY_LAYOUT_IMAGE_PATH = null;
            //string fileName = "";
            //string suffix = "";
            //FileSystemObject fs = null;
            //Folder folder = null;
            //Files files = null;
            //vbsFile file = null;
            //object imageNum = null;
            //object rowNum = null;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    #endregion

    #region Query Strings
    /// <summary>
    /// Query String validations
    /// </summary>

    private void QueryStrings()
    {
        if (Request.QueryString["from"].ToString() != null)
        {
            if (Request.QueryString["from"].ToString() == "rm")
            {
                Response.Write("<tr>");
                Response.Write("  <td align='right'><SPAN class=blackblodtext>Password</SPAN></td>");
                Response.Write("  <td align='left'><input type='password' name='ConferencePassword' maxlength='50' size='15' value=''></td>");
                Response.Write("</tr>");
            }
        }
        
    }
    #endregion

    #region Bind Drop down Values
    /// <summary>
    /// All Drop Downlist values will bind using this method
    /// </summary>
   
    protected void BindDropdownList(DropDownList lstTemp, String[] tempValues)
    {
        
        try
        {
            if (tempValues != null)
            {
                ListItem lst = null;
                foreach (String str in tempValues)
                {
                    if (str.Split('|').Length <= 1)
                    {
                        lst = new ListItem(obj.GetTranslatedText(str), str); // ZD 100288
                        lstTemp.Items.Add(lst);
                        lst = null;


                    }
                    else
                    {
                        lst = new ListItem(obj.GetTranslatedText(str.Split('|')[1]), str.Split('|')[0]); // ZD 100288
                        lstTemp.Items.Add(lst);
                        lst = null;

                        
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }
    #endregion

    #region Get Image For Client
    /// <summary>
    /// Getting Image Display Layout,will bind based on the client
    /// </summary>
    protected void GetImgForClient()
    {
        rowsize.Value = "4";
        
        try
        {
            if ((((Convert.ToString(Application["Client"])).ToUpper() == "BTBOCES") || ((Convert.ToString(Application["Client"])).ToUpper() == "WHOUSE")))
            {

                DirectoryInfo fs = new DirectoryInfo(Server.MapPath(Convert.ToString(DISPLAY_LAYOUT_IMAGE_PATH)) + "\\BTBoces");
                FileInfo[] files = fs.GetFiles();
                foreach (FileSystemInfo file in files)
                {
                    fileName = file.FullName;
                    suffix = (fileName.Substring(fileName.Length - 3 - 1)).ToLower();
                    if (suffix == ".gif")
                    {
                        //if (imageFilesBT == "")
                        //    imageFilesBT = imageFilesBT + fileName.Substring(0, fileName.Length - 4) + ":";
                        //else
                            imageFilesBT = imageFilesBT + file.Name.Substring(0, file.Name.Length - 4) + ":";
                    }
                }
            }

            DirectoryInfo fs1 = new DirectoryInfo(Server.MapPath(Convert.ToString(DISPLAY_LAYOUT_IMAGE_PATH)));
            FileInfo[] files1 = fs1.GetFiles();
            foreach (FileSystemInfo file1 in files1)
            {
                fileName = file1.Name;
                suffix = (fileName.Substring(fileName.Length - 3 - 1)).ToLower();
                if (suffix == ".gif")
                {
                    if (((Convert.ToString(Application["Client"])).ToUpper() != "PSU"))
                    {
                        imageFiles = imageFiles + file1.Name.Substring(0, file1.Name.Length - 4) + ":";

                    }
                }
                
            }
            if (((Convert.ToString(Application["Client"])).ToUpper() == "PSU"))
            {
                imageFiles = "01:02:03:05:16:";
            }
            //layoutimg.Src = fileName;
            


        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion
}
