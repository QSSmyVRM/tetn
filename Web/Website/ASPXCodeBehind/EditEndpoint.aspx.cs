/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using System.Collections;//ZD 100664
using System.Collections.Generic; //ZD 100619 
using System.Text;
/// <summary>
/// Summary description for Edit Endpoints
/// </summary>
/// 
namespace ns_EditEndpoint
{
    public partial class EditEndpoint : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblNoEndpoints;
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstSystemLocation;//ZD 104821
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;//ZD 100619
        protected System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstManufacturer; //ZD 100736
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;

        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtEndpointID;
        protected System.Web.UI.WebControls.DataGrid dgEndpointList;
        protected System.Web.UI.WebControls.DataGrid dgProfiles;
        protected System.Web.UI.WebControls.Table tblPage;

        protected System.Web.UI.WebControls.TextBox txtUserName; //ZD 100814        

        protected CustomValidator cvSubmit;
		//ZD 100420
        //protected System.Web.UI.WebControls.Button btnAddNewProfile;
        protected System.Web.UI.HtmlControls.HtmlButton btnAddNewProfile;
        protected System.Web.UI.WebControls.Button btnSubmit; //FB 2670
        //protected System.Web.UI.HtmlControls.HtmlButton btnSubmit; //FB 2670
		//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlInputHidden isMarkedDeleted;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTmpPass; // ZD 100736
        bool hasReports = false;//ZD 100664
        protected System.Web.UI.WebControls.DataGrid dgChangedHistory;//ZD 100664
        protected System.Web.UI.HtmlControls.HtmlTableRow trHistory; //ZD 100664
        //ZD 100815  start
        protected System.Web.UI.WebControls.DropDownList lstProfileType; 
        protected System.Web.UI.WebControls.TextBox txtProfilePassword2;
        protected System.Web.UI.WebControls.TextBox txtProfilePassword;
        protected System.Web.UI.WebControls.Label lblConfirmPassword;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.Label lblUserName;
        protected System.Web.UI.WebControls.Label lblAPIPort;
        protected System.Web.UI.WebControls.Label lblTelnetAPIEnabled;
        protected System.Web.UI.WebControls.Label lblSSH; //ZD 101363

        protected System.Web.UI.WebControls.Label lblTelePresence;
        protected System.Web.UI.WebControls.Label lblAddressType;
        protected System.Web.UI.WebControls.Label lblAssignedtoMCU;
        protected System.Web.UI.WebControls.Label lblPreferredDialOption;
        protected System.Web.UI.WebControls.Label lblDefaultProtocol;
        protected System.Web.UI.WebControls.Label lblAssociateMCUAddress;
        protected System.Web.UI.WebControls.Label lblMCUAddressType;
        protected System.Web.UI.WebControls.Label lblWebAccessURL;
        protected System.Web.UI.WebControls.Label lbliCalInvite;
        protected System.Web.UI.WebControls.Label lblEncryptionPreferred;
        protected System.Web.UI.WebControls.Label lblEmailID;
        protected System.Web.UI.WebControls.Label lblRearSecurityCameraAddress;
        //ZD 100815  End
        string dtFormatType = "MM/dd/yyyy";
        string tformat = "hh:mm tt";
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        private String DrpValue = "";  //Endpoint Search Fix
        protected List<int> PolycomMCU; //ZD 100619

        protected System.Web.UI.WebControls.CheckBox ChkLCR;//ZD 100040
        protected System.Web.UI.WebControls.DropDownList lstEptResolution; //ZD 100040
        public EditEndpoint()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("editendpoint.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                Session["profCnt"] = "";

                //ZD 100664 Startx
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() == "")
                        Session["FormatDateType"] = "MM/dd/yyyy";
                    else
                        dtFormatType = Session["FormatDateType"].ToString();
                }

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //ZD 100664 End

                //code added for endpoint search -- Start
                if (Request.QueryString["EpID"] != null)
                    if (Request.QueryString["EpID"].ToString() != "")
                    {
                        String enpointID = Request.QueryString["EpID"].ToString();
                        Session.Remove("EndpointID");
                        Session.Add("EndpointID", enpointID);
                    }

                if (Request.QueryString["DrpValue"] != null)
                    if (Request.QueryString["DrpValue"].ToString().Trim() != "")
                    {
                        DrpValue = Request.QueryString["DrpValue"].ToString().Trim();
                    }
                //code added for endpoint search -- End
                //ZD 100619 Starts
                PolycomMCU = new List<int>();
                PolycomMCU.Add(ns_MyVRMNet.MCUType.PolycomMGC25);
                PolycomMCU.Add(ns_MyVRMNet.MCUType.PolycomMGC50);
                PolycomMCU.Add(ns_MyVRMNet.MCUType.PolycomMGC100);
                PolycomMCU.Add(ns_MyVRMNet.MCUType.PolycomRMX2000); 
                PolycomMCU.Add(ns_MyVRMNet.MCUType.PolycomRPRM); 

                //ZD 100619 Ends

                //errLabel.Visible = false; //FB 2400
                //Response.Write("here");
                //Response.Write(Session["userEmail"].ToString());

                //ZD 100664 start
                string[] mary = Session["sMenuMask"].ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]); // Convert.ToInt32(mmary[1].Split('*')[1]); ZD 101233 103095

                if (Convert.ToBoolean(topMenu & 16))
                {
                    hasReports = Convert.ToBoolean(adminMenu & 4);
                }

                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString().Equals("2") && (hasReports == true))
                    {
                        trHistory.Attributes.Add("Style", "visibility:visible;");
                    }
                    else
                    {
                        trHistory.Attributes.Add("Style", "visibility:hidden;");
                    }
                }
                //ZD 100664 End

                if (!IsPostBack)
                {
                    BindData();
                }
                BindprofileAddresseslist(); //FB 2400

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnAddNewProfile.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnAddNewProfile.Attributes.Add("Class", "btndisable");// FB 2796
                    lblHeader.Text = obj.GetTranslatedText("View Endpoint Details");
                    //ZD 100263
                    btnSubmit.Visible = false;
                    btnAddNewProfile.Visible = false;
                }
                // FB 2796 Start
                else
                {
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    btnAddNewProfile.Attributes.Add("Class", "altLongBlueButtonFormat");
                }
                // FB 2796 End
                //ZD 101026
                if (Session["EndpointID"].ToString().ToLower().Equals("new"))
                    trHistory.Attributes.Add("Style", "visibility:hidden;");

            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
            }

        }

        protected void InitializeLists(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))// || (e.Item.ItemType.Equals(ListItemType.Footer)))
                {
                    ((DropDownList)e.Item.FindControl("lstAddressType")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    ((DropDownList)e.Item.FindControl("lstMCUAddressType")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    //((DropDownList)e.Item.FindControl("lstConnectionType")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    ((DropDownList)e.Item.FindControl("lstVideoProtocol")).SelectedIndexChanged += new EventHandler(ValidateTypes);
                    //                    Response.Write("1");
                    obj.BindBridges((DropDownList)e.Item.FindControl("lstBridges"));
                    obj.BindBridgesType((DropDownList)e.Item.FindControl("lstBridgeType"));	//ZD 100619 
                    obj.BindVideoEquipment((DropDownList)e.Item.FindControl("lstVideoEquipment"));
                    obj.BindManufacturer((DropDownList)e.Item.FindControl("lstManufacturer")); //ZD 100736
                    obj.BindLineRate((DropDownList)e.Item.FindControl("lstLineRate"));
                    obj.BindAddressType((DropDownList)e.Item.FindControl("lstAddressType"));
                    obj.BindAddressType((DropDownList)e.Item.FindControl("lstMCUAddressType"));
                    obj.BindVideoProtocols((DropDownList)e.Item.FindControl("lstVideoProtocol"));
                    obj.BindDialingOptions((DropDownList)e.Item.FindControl("lstConnectionType"));
                    obj.BindEptResolution((DropDownList)e.Item.FindControl("lstEptResolution"));//ZD 100040
                    ((Label)e.Item.FindControl("lblProfileCount")).Text = (e.Item.ItemIndex + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace("InitializeLists: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        //ZD 100736 START
        #region UpdateEndpointModel
        protected void UpdateEndpointModel(Object sender, EventArgs e)
        {
            try
            {
                DataGridItem clickedRow = ((DropDownList)sender).NamingContainer as DataGridItem;
                DropDownList lstVE = (DropDownList)clickedRow.FindControl("lstVideoEquipment");
                DropDownList lstMan = (DropDownList)clickedRow.FindControl("lstManufacturer");
                lstVE.Items.Clear();

                if (!lstMan.SelectedValue.Equals("-1"))
                    obj.GetManufacturerModel(lstVE, lstMan.SelectedValue);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateEndpointModel: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100736 END
        //ZD 100815 Start
        #region UpdateProfileType
        protected void UpdateProfileType(Object sender, EventArgs e)
        {
            try
            {
                int BrdgeID = 0, BridgeType = 0; //ZD 104821

                DataGridItem clickedRow = ((DropDownList)sender).NamingContainer as DataGridItem;
                DropDownList lstProfileType = (DropDownList)clickedRow.FindControl("lstProfileType");
                DropDownList lstAddressType = (DropDownList)clickedRow.FindControl("lstAddressType");


                TextBox txtUserName = (TextBox)clickedRow.FindControl("txtUserName");
                TextBox txtProfilePassword = (TextBox)clickedRow.FindControl("txtProfilePassword");
                Label lblPassword = (Label)clickedRow.FindControl("lblPassword");
                TextBox txtProfilePassword2 = (TextBox)clickedRow.FindControl("txtProfilePassword2");
                Label lblConfirmPassword = (Label)clickedRow.FindControl("lblConfirmPassword");
                Label lblAPIPort = (Label)clickedRow.FindControl("lblAPIPort");
                Label lblSSH = (Label)clickedRow.FindControl("lblSSH"); //ZD 101363
                TextBox txtApiport = (TextBox)clickedRow.FindControl("txtApiport");
                Label lblTelnetAPIEnabled = (Label)clickedRow.FindControl("lblTelnetAPIEnabled");
                CheckBox chkP2PSupport = (CheckBox)clickedRow.FindControl("chkP2PSupport");
                CheckBox chkSSHSupport = (CheckBox)clickedRow.FindControl("chkSSHSupport");//ZD 101363
                Label lblUserName = (Label)clickedRow.FindControl("lblUserName");

                Label lblTelePresence = (Label)clickedRow.FindControl("lblTelePresence");
                CheckBox chkTelepresence = (CheckBox)clickedRow.FindControl("chkTelepresence");
                ListBox lstProfileAddress = (ListBox)clickedRow.FindControl("lstProfileAddress");
                
                Label lblAddressType = (Label)clickedRow.FindControl("lblAddressType");
                Label lblAssignedtoMCU = (Label)clickedRow.FindControl("lblAssignedtoMCU");
                DropDownList lstBridges = (DropDownList)clickedRow.FindControl("lstBridges");
                Label lblPreferredDialOption = (Label)clickedRow.FindControl("lblPreferredDialOption");
                DropDownList lstConnectionType = (DropDownList)clickedRow.FindControl("lstConnectionType");
                Label lblDefaultProtocol = (Label)clickedRow.FindControl("lblDefaultProtocol");
                DropDownList lstVideoProtocol = (DropDownList)clickedRow.FindControl("lstVideoProtocol");
                Label lblAssociateMCUAddress = (Label)clickedRow.FindControl("lblAssociateMCUAddress");
                TextBox txtMCUAddress = (TextBox)clickedRow.FindControl("txtMCUAddress");
                Label lblMCUAddressType = (Label)clickedRow.FindControl("lblMCUAddressType");
                DropDownList lstMCUAddressType = (DropDownList)clickedRow.FindControl("lstMCUAddressType");
                Label lblWebAccessURL = (Label)clickedRow.FindControl("lblWebAccessURL");
                TextBox txtURL = (TextBox)clickedRow.FindControl("txtURL");
                Label lbliCalInvite = (Label)clickedRow.FindControl("lbliCalInvite");
                CheckBox chkIsCalderInvite = (CheckBox)clickedRow.FindControl("chkIsCalderInvite");
                Label lblEncryptionPreferred = (Label)clickedRow.FindControl("lblEncryptionPreferred");
                CheckBox chkEncryptionPreferred = (CheckBox)clickedRow.FindControl("chkEncryptionPreferred");
                Label lblEmailID = (Label)clickedRow.FindControl("lblEmailID");
                TextBox txtExchangeID = (TextBox)clickedRow.FindControl("txtExchangeID");
                Label lblRearSecurityCameraAddress = (Label)clickedRow.FindControl("lblRearSecurityCameraAddress");
                TextBox txtRearSecCamAdd = (TextBox)clickedRow.FindControl("txtRearSecCamAdd");

                DropDownList lstEptResolution = (DropDownList)clickedRow.FindControl("lstEptResolution"); //ZD 100040
                CheckBox ChkLCR = (CheckBox)clickedRow.FindControl("ChkLCR"); //ZD 100040
                //ZD 104821 Starts
                DropDownList lstSystemLocation = (DropDownList)clickedRow.FindControl("lstSystemLocation"); 
                Label SysLocation = (Label)clickedRow.FindControl("SysLocation"); 
                TextBox txtSysLoc = (TextBox)clickedRow.FindControl("txtSysLoc");
                //ZD 104821 Ends

                if (lstProfileType.SelectedValue.Equals("0"))
                {
                    if (Session["isAssignedMCU"] != null)
                        if (Session["isAssignedMCU"].ToString() == "1")
                        {
                            if (((RequiredFieldValidator)clickedRow.FindControl("reqBridges")) != null)
                                ((RequiredFieldValidator)clickedRow.FindControl("reqBridges")).Enabled = true;
                        }
                    lstAddressType.SelectedValue =  "1";
                    //lstAddressType.Enabled = false;
                    txtUserName.Enabled = true;
                    lblUserName.ForeColor = System.Drawing.Color.Black;
                    txtProfilePassword.Enabled = true;
                    lblPassword.ForeColor = System.Drawing.Color.Black;
                    lblConfirmPassword.ForeColor = System.Drawing.Color.Black;
                    txtProfilePassword2.Enabled = true;
                    lblAPIPort.ForeColor = System.Drawing.Color.Black;
                    txtApiport.Enabled = true;
                    lblTelnetAPIEnabled.ForeColor = System.Drawing.Color.Black;
                    chkP2PSupport.Enabled = true;
                    lblSSH.ForeColor = System.Drawing.Color.Black;//ZD 101363
                    chkSSHSupport.Enabled = true;//ZD 101363
                    lblTelePresence.ForeColor = System.Drawing.Color.Black;
                    chkTelepresence.Enabled = true;
                    lblAddressType.ForeColor = System.Drawing.Color.Black;
                    lstAddressType.Enabled = true;
                    lblAssignedtoMCU.ForeColor = System.Drawing.Color.Black; //ZD 100815
                    lstBridges.Enabled = true;
                   
                    lblPreferredDialOption.ForeColor = System.Drawing.Color.Black;
                    lstConnectionType.Enabled = true;
                    lblDefaultProtocol.ForeColor = System.Drawing.Color.Black;
                    lstVideoProtocol.Enabled = true;
                    lblAssociateMCUAddress.ForeColor = System.Drawing.Color.Black;
                    lblAssociateMCUAddress.Enabled = true;
                    lblMCUAddressType.ForeColor = System.Drawing.Color.Black;
                    lstMCUAddressType.Enabled = true;
                    lstMCUAddressType.SelectedValue = "1";
                    lblWebAccessURL.ForeColor = System.Drawing.Color.Black;
                    txtURL.Enabled = true;
                    lbliCalInvite.ForeColor = System.Drawing.Color.Black;
                    chkIsCalderInvite.Enabled = true;
                    lblEncryptionPreferred.ForeColor = System.Drawing.Color.Black;
                    chkEncryptionPreferred.Enabled = true;
                    lblEmailID.ForeColor = System.Drawing.Color.Black;
                    txtExchangeID.Enabled = true;
                    lblRearSecurityCameraAddress.ForeColor = System.Drawing.Color.Black;
                    txtRearSecCamAdd.Enabled = true;
                    txtMCUAddress.Enabled = true;
                    ChkLCR.Enabled = true; //ZD 100040

                    //ZD 104821 Starts
                    if (!lstBridges.SelectedValue.Equals("-1"))
                    {
                        int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                        int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                        if (BridgeType == 16)
                        {
                            SysLocation.Visible = true;
                            lstSystemLocation.Visible = true;
                            BindSystemLocation(BrdgeID, BridgeType, lstSystemLocation);
                            lstSystemLocation.Items.FindByValue(txtSysLoc.Text).Selected = true;
                            lstSystemLocation.Enabled = true;
                            SysLocation.Enabled = true;
                        }
                    }
                    else
                    {
                        SysLocation.Visible = false;
                        lstSystemLocation.Visible = false;
                        lstSystemLocation.ClearSelection();
                    }
                    //ZD 104821 Ends
                }
                else if (lstProfileType.SelectedValue.Equals("1"))
                {
                    if (Session["isAssignedMCU"] != null)
                        if (Session["isAssignedMCU"].ToString() == "1")
                        {
                            if (((RequiredFieldValidator)clickedRow.FindControl("reqBridges")) != null)
                                ((RequiredFieldValidator)clickedRow.FindControl("reqBridges")).Enabled = true;
                        }
                    lblAssignedtoMCU.ForeColor = System.Drawing.Color.Black; 
                    lstBridges.Enabled = true;
                    txtUserName.Enabled = false;
                    lblUserName.ForeColor = System.Drawing.Color.Gray;
                    txtProfilePassword.Enabled = false;
                    lblPassword.ForeColor = System.Drawing.Color.Gray;
                    lblConfirmPassword.ForeColor = System.Drawing.Color.Gray;
                    txtProfilePassword2.Enabled = false;
                    lblAPIPort.ForeColor = System.Drawing.Color.Gray;
                    txtApiport.Enabled = false;
                    lblTelnetAPIEnabled.ForeColor = System.Drawing.Color.Gray;
                    chkP2PSupport.Enabled = false;
                    lblSSH.ForeColor = System.Drawing.Color.Gray;//ZD 101363
                    chkSSHSupport.Enabled = false;//ZD 101363
                    chkSSHSupport.Checked = false;
                    lblTelePresence.ForeColor = System.Drawing.Color.Black;
                    chkTelepresence.Enabled = true;
                    chkTelepresence.Checked = false;
                    lblAddressType.ForeColor = System.Drawing.Color.Black;
                    lstAddressType.Enabled = true;
                   
                    lblPreferredDialOption.ForeColor = System.Drawing.Color.Black;
                    lstConnectionType.Enabled = true;
                    lblDefaultProtocol.ForeColor = System.Drawing.Color.Black;
                    lstVideoProtocol.Enabled = true;
                    lblAssociateMCUAddress.ForeColor = System.Drawing.Color.Black;
                    lblAssociateMCUAddress.Enabled = true;
                    lblMCUAddressType.ForeColor = System.Drawing.Color.Black;
                    lstMCUAddressType.Enabled = true;
                    lblWebAccessURL.ForeColor = System.Drawing.Color.Black;
                    txtURL.Enabled = true;
                    lbliCalInvite.ForeColor = System.Drawing.Color.Black;
                    chkIsCalderInvite.Enabled = true;
                    lblEncryptionPreferred.ForeColor = System.Drawing.Color.Black;
                    chkEncryptionPreferred.Enabled = true;
                    lblEmailID.ForeColor = System.Drawing.Color.Black;
                    txtExchangeID.Enabled = true;
                    lblRearSecurityCameraAddress.ForeColor = System.Drawing.Color.Black;
                    txtRearSecCamAdd.Enabled = true;
                    txtMCUAddress.Enabled = true;

                    //ZD 104821 Starts
                    if (!lstBridges.SelectedValue.Equals("-1"))
                    {
                        int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                        int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                        if (BridgeType == 16)
                        {
                            SysLocation.Visible = true;
                            lstSystemLocation.Visible = true;
                            BindSystemLocation(BrdgeID, BridgeType, lstSystemLocation);
                            lstSystemLocation.Items.FindByValue(txtSysLoc.Text).Selected = true;
                            lstSystemLocation.Enabled = true;
                            SysLocation.Enabled = true;
                        }
                    }
                    else
                    {
                        SysLocation.Visible = false;
                        lstSystemLocation.Visible = false;
                        lstSystemLocation.ClearSelection();
                    }
                    //ZD 104821 Ends
                }
                else
                { 
                    lblTelePresence.ForeColor = System.Drawing.Color.Gray;
                    chkTelepresence.Enabled = false;
                    chkTelepresence.Checked = false;
                    lstProfileAddress.Items.Clear();//ZDLatest
                    lblAddressType.ForeColor = System.Drawing.Color.Gray;
                    lstAddressType.Enabled = false;
                    lstAddressType.SelectedValue = "1";
                  
                    lblPreferredDialOption.ForeColor = System.Drawing.Color.Gray;
                    lstConnectionType.Enabled = false;
                    lstConnectionType.SelectedValue = "1";
                    lblDefaultProtocol.ForeColor = System.Drawing.Color.Gray;
                    lstVideoProtocol.Enabled = false;
                    lstVideoProtocol.SelectedValue = "1";
                    lblAssociateMCUAddress.ForeColor = System.Drawing.Color.Gray;
                    lblAssociateMCUAddress.Enabled = false;
                    txtMCUAddress.Enabled = false;
                    lblMCUAddressType.ForeColor = System.Drawing.Color.Gray;
                    lstMCUAddressType.Enabled = false;
                    lblWebAccessURL.ForeColor = System.Drawing.Color.Gray;
                    txtURL.Enabled = false;
                    txtURL.Text = "";
                    lbliCalInvite.ForeColor = System.Drawing.Color.Gray;
                    chkIsCalderInvite.Enabled = false;
                    chkIsCalderInvite.Checked = false;
                    lblEncryptionPreferred.ForeColor = System.Drawing.Color.Gray;
                    chkEncryptionPreferred.Enabled = false;
                    chkEncryptionPreferred.Checked = false;
                    lblTelnetAPIEnabled.ForeColor = System.Drawing.Color.Gray;
                    chkP2PSupport.Enabled = false;
                    chkP2PSupport.Checked = true;
                    lblSSH.ForeColor = System.Drawing.Color.Black;//ZD 101363
                    chkSSHSupport.Enabled = true;//ZD 101363
                    lblEmailID.ForeColor = System.Drawing.Color.Gray;
                    txtExchangeID.Enabled = false;
                    txtExchangeID.Text = "";
                    lblRearSecurityCameraAddress.ForeColor = System.Drawing.Color.Gray;
                    txtRearSecCamAdd.Enabled = false;
                    txtRearSecCamAdd.Text = "";

                    if (((RequiredFieldValidator)clickedRow.FindControl("reqBridges")) != null)
                        ((RequiredFieldValidator)clickedRow.FindControl("reqBridges")).Enabled = false;
                    lblAssignedtoMCU.ForeColor = System.Drawing.Color.Gray; 
                    lstBridges.Enabled = false;
                    lstBridges.SelectedValue = "-1";
                    //ZD 104821 Starts
                    lstSystemLocation.SelectedValue = "0"; 
                    lstSystemLocation.Visible = false; 
                    SysLocation.Visible = false; 
                    //ZD 104821 Ends
                    txtUserName.Enabled = true;
                    lblUserName.ForeColor = System.Drawing.Color.Black;
                    txtProfilePassword.Enabled = true;
                    lblPassword.ForeColor = System.Drawing.Color.Black;
                    lblConfirmPassword.ForeColor = System.Drawing.Color.Black;
                    txtProfilePassword2.Enabled = true;
                    lblAPIPort.ForeColor = System.Drawing.Color.Black;
                    txtApiport.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateProfileType: " + ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion
        //ZD 100815 End
        protected void BindData()
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                if (Session["EndpointID"].ToString().ToLower().Equals("new"))
                {
                    lblHeader.Text = obj.GetTranslatedText("Create New Endpoint"); //FB 1830 - Translation
                    txtEndpointID.Text = Session["EndpointID"].ToString();
                    txtEndpointName.Text = "";
                    //String DetailsXml = "<Profile><ProfileID>new</ProfileID><ProfileName>New Profile</ProfileName><EncryptionPreferred>0</EncryptionPreferred><AddressType>1</AddressType><Password></Password><Address></Address><URL></URL><IsOutside>0</IsOutside><VideoEquipment>1</VideoEquipment><LineRate>384</LineRate><Bridge>-1</Bridge><DefaultProtocol>-1</DefaultProtocol><ConnectionType>1</ConnectionType><MCUAddress></MCUAddress><MCUAddressType>-1</MCUAddressType></Profile>";
                    xmldoc = new XmlDocument();
                    XmlNodeList nodes = xmldoc.SelectNodes("//Profile");
                    LoadProfiles(nodes, "new");
                }
                else
                {
                    String inXML = "";
                    inXML += "<EndpointDetails>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <EndpointID>" + Session["EndpointID"].ToString() + "</EndpointID>";
                    inXML += "  <EntityType></EntityType>";
                    inXML += "</EndpointDetails>";
                    String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    outXML = outXML.Replace(" % ", "&lt;br/&gt;"); //FB 1886
                    //Response.Write(obj.Transfer(outXML));
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        if (Session["ProfileID"] != null)
                            if (Session["ProfileID"].ToString().ToLower().Equals("new"))
                            {
                                String newProfileXml = "<Profile><ProfileID>new</ProfileID><ProfileName>New Profile</ProfileName><EncryptionPreferred>0</EncryptionPreferred><ProfileType>0</ProfileType><AddressType>1</AddressType><Password></Password><Address></Address><URL></URL><IsOutside>0</IsOutside><VideoEquipment>3</VideoEquipment><Manufacturer>3</Manufacturer><LineRate>768</LineRate><Bridge>-1</Bridge><DefaultProtocol>1</DefaultProtocol><ConnectionType>2</ConnectionType><MCUAddress></MCUAddress><MCUAddressType>-1</MCUAddressType></Profile>";//FB 2272 //FB 2565 //ZD 100736//ZD 100815
                                outXML = outXML.Substring(0, outXML.IndexOf("</Profiles></Endpoint></EndpointDetails>"));
                                outXML += newProfileXml;
                                outXML += "</Profiles></Endpoint></EndpointDetails>";
                            }

                        //Response.Write(obj.Transfer(outXML));
                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                        txtEndpointID.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/ID").InnerText;
                        txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;
                        LoadProfiles(nodes, xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText);
                        //Response.Write(nodes.Count);
                        //ZD 100664 Starts
                        XmlNodeList changedHistoryNodes = xmldoc.SelectNodes("//EndpointDetails/LastModifiedDetails/LastModified");
                        LoadHistory(changedHistoryNodes);
                        //ZD 100664 End
                    }
                    //ZD 100263_Nov11 Start
                    else if (outXML.IndexOf("<error>-1</error>") == 0)
                        Response.Redirect("ShowError.aspx");
                    //ZD 100263_Nov11 End
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
                
                
                if (xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile").Count >= 5)
                    btnAddNewProfile.Visible = false;
            }
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void LoadProfiles(XmlNodeList nodes, String defaultProfile)
        {
            string videoEquipment = "";//ZD 100736
            int isP2P = 0, BridgeType = 0, BrdgeID = -1; //ZD 100815 //ZD 104821
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                String adds = ""; //FB 2400 start
                XmlNodeList multicode = null;

                foreach (XmlNode node in nodes)
                {
                    adds = "";//FB 2602
                    if (node.SelectNodes("//MultiCodec/Address") != null)
                    {
                        multicode = node.SelectNodes("MultiCodec/Address");//FB 2602
                        for (int i = 0; i < multicode.Count; i++)
                        {
                            if (multicode[i] != null)
                                if (multicode[i].InnerText.Trim() != "")
                                {
                                    if (i > 0)
                                        adds += "~";
                                    
                                    adds += multicode[i].InnerText;
                                }
                        }
                        
                        if (multicode.Count > 0)
                            node.SelectSingleNode("MultiCodec").InnerText = adds;
                    }

                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                //FB 2400 end
                
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dt = ds.Tables[0];
                    if (!dt.Columns.Contains("DefaultProfile"))
                        dt.Columns.Add("DefaultProfile");
                    if (!dt.Columns.Contains("isTelePresence"))//FB 2400 start
                        dt.Columns.Add("isTelePresence");
                    if (!dt.Columns.Contains("RearSecCameraAddress"))
                        dt.Columns.Add("RearSecCameraAddress");
                    if (!dt.Columns.Contains("MultiCodec"))//FB 2400 end
                        dt.Columns.Add("MultiCodec");
                    if (!dt.Columns.Contains("GateKeeeperAddress"))//ZD 100132
                        dt.Columns.Add("GateKeeeperAddress");
                    if (!dt.Columns.Contains("Manufacturer"))//ZD 100736
                        dt.Columns.Add("Manufacturer");
                    if (!dt.Columns.Contains("ProfileType"))//ZD 100815
                        dt.Columns.Add("ProfileType");
                    if (!dt.Columns.Contains("IsP2PDefault"))//ZD 100815
                        dt.Columns.Add("IsP2PDefault");
                    
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["ProfileID"].ToString().Equals(defaultProfile) || dr["IsP2PDefault"].ToString().Equals("1")) //ZD 100815
                            dr["DefaultProfile"] = "1";
                        else
                            dr["DefaultProfile"] = "0";

                        if (dr["MCUAddressType"].ToString().Equals("0"))
                            dr["MCUAddressType"] = "-1";
                        if (dr["DefaultProtocol"].ToString().Equals("0"))
                            dr["DefaultProtocol"] = "-1";
                        if (dr["ConnectionType"].ToString().Equals("0"))
                            dr["ConnectionType"] = "2";
                        if (dr["isTelePresence"].ToString().Equals("1")) //FB 2400
                            dr["Address"] = "";
                        //dr["Password"] = ns_MyVRMNet.vrmPassword.MCUProfile; //FB 3054
                        //ZD 100040
                        if (dr["EptResolution"].ToString().Equals("") || dr["EptResolution"].ToString().Equals("0"))
                            dr["EptResolution"] = "-1";
                    }
                }
                else
                {
                    dt.Columns.Add("ProfileID");
                    dt.Columns.Add("ProfileName");
                    dt.Columns.Add("EncryptionPreferred");
                    dt.Columns.Add("AddressType");
                    dt.Columns.Add("Password");
                    dt.Columns.Add("Address");
                    dt.Columns.Add("URL");
                    dt.Columns.Add("IsOutside");
                    dt.Columns.Add("ConnectionType");
                    dt.Columns.Add("VideoEquipment");
                    dt.Columns.Add("Manufacturer");//ZD 100736
                    dt.Columns.Add("LineRate");
                    dt.Columns.Add("Bridge");
                    dt.Columns.Add("BridgeType"); //ZD 100619
                    dt.Columns.Add("DefaultProfile");
                    dt.Columns.Add("DefaultProtocol");
                    dt.Columns.Add("MCUAddress");
                    dt.Columns.Add("MCUAddressType");
                    dt.Columns.Add("ExchangeID"); //Cisco Telepresence fix
                    dt.Columns.Add("IsCalendarInvite"); //Cisco ICAL FB 1602
                    //Code Added For FB1422 - Start
                    dt.Columns.Add("TelnetAPI");
                    //Code Added For FB1422 - End
                   
                    dt.Columns.Add("ApiPortno");//Api Port...
                    dt.Columns.Add("isTelePresence"); //FB 2400 start
                    dt.Columns.Add("RearSecCameraAddress");
                    dt.Columns.Add("MultiCodec");  //FB 2400 end
                    dt.Columns.Add("Secured");//FB 2595
                    dt.Columns.Add("NetworkURL");//FB 2595
                    dt.Columns.Add("Securedport");//FB 2595
                    dt.Columns.Add("GateKeeeperAddress");//ZD 100132
                    dt.Columns.Add("UserName");//ZD 100814
                    dt.Columns.Add("ProfileType");//ZD 100815
                    dt.Columns.Add("SSHSupport");//ZD 101363
                    dt.Columns.Add("EptResolution"); //ZD 100040
                    dt.Columns.Add("LCR"); //ZD 100040
                    dt.Columns.Add("SysLocation"); //ZD 104821

                    DataRow dr = dt.NewRow();
                    AddNewRow(dr);
                    dr["DefaultProfile"] = "1";
                    dt.Rows.Add(dr);
                }
                dgProfiles.DataSource = dt;
                dgProfiles.DataBind();
                //ZD 103595 START
                //foreach (DataGridItem dgi in dgProfiles.Items)
                //{
                //    if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                //        AllowCreatingNew(dgi, false);
                //}
                //ZD103595 END
                //ZD 100736 START
                foreach (DataGridItem dgi in dgProfiles.Items)
                {

                    DropDownList lstVideoEquipment = (DropDownList)dgi.FindControl("lstVideoEquipment");
                    DropDownList lstManufacturer = (DropDownList)dgi.FindControl("lstManufacturer");
                    try
                    {
                        videoEquipment = "";
                        if (Session["EndpointID"].ToString().ToLower().Equals("new"))
                        {
                            lstVideoEquipment.ClearSelection();
                            obj.GetManufacturerModel(lstVideoEquipment, "3");
                            lstManufacturer.ClearSelection();
                            lstManufacturer.Items.FindByValue("3").Selected = true; // Polycom
                            
                        }
                        else
                        {
                            videoEquipment = lstVideoEquipment.SelectedValue;
                            obj.GetManufacturerModel(lstVideoEquipment, lstManufacturer.SelectedValue);
                            if (lstVideoEquipment.Items.FindByValue(videoEquipment) != null)
                                lstVideoEquipment.Items.FindByValue(videoEquipment).Selected = true;

                        }
						//ZD 100815 start

                        DropDownList lstProfileType = (DropDownList)dgi.FindControl("lstProfileType");
                        DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                        

                        TextBox txtUserName = (TextBox)dgi.FindControl("txtUserName");
                        TextBox txtProfilePassword = (TextBox)dgi.FindControl("txtProfilePassword");
                        Label lblPassword = (Label)dgi.FindControl("lblPassword");
                        TextBox txtProfilePassword2 = (TextBox)dgi.FindControl("txtProfilePassword2");
                        Label lblConfirmPassword = (Label)dgi.FindControl("lblConfirmPassword");
                        Label lblAPIPort = (Label)dgi.FindControl("lblAPIPort");
                        TextBox txtApiport = (TextBox)dgi.FindControl("txtApiport");
                        Label lblTelnetAPIEnabled = (Label)dgi.FindControl("lblTelnetAPIEnabled");
                        CheckBox chkP2PSupport = (CheckBox)dgi.FindControl("chkP2PSupport");
                        Label lblSSH = (Label)dgi.FindControl("lblSSH");//ZD 101363                 
                        CheckBox chkSSHSupport = (CheckBox)dgi.FindControl("chkSSHSupport");//ZD 101363                        
                        Label lblUserName = (Label)dgi.FindControl("lblUserName");

                        Label lblTelePresence = (Label)dgi.FindControl("lblTelePresence");
                        CheckBox chkTelepresence = (CheckBox)dgi.FindControl("chkTelepresence");
                        Label lblAddressType = (Label)dgi.FindControl("lblAddressType");
                        Label lblAssignedtoMCU = (Label)dgi.FindControl("lblAssignedtoMCU");
                        DropDownList lstBridges = (DropDownList)dgi.FindControl("lstBridges");
                        Label lblPreferredDialOption = (Label)dgi.FindControl("lblPreferredDialOption");
                        DropDownList lstConnectionType = (DropDownList)dgi.FindControl("lstConnectionType");
                        Label lblDefaultProtocol = (Label)dgi.FindControl("lblDefaultProtocol");
                        DropDownList lstVideoProtocol = (DropDownList)dgi.FindControl("lstVideoProtocol");
                        Label lblAssociateMCUAddress = (Label)dgi.FindControl("lblAssociateMCUAddress");
                        TextBox txtMCUAddress = (TextBox)dgi.FindControl("txtMCUAddress");
                        Label lblMCUAddressType = (Label)dgi.FindControl("lblMCUAddressType");
                        DropDownList lstMCUAddressType = (DropDownList)dgi.FindControl("lstMCUAddressType");
                        Label lblWebAccessURL = (Label)dgi.FindControl("lblWebAccessURL");
                        TextBox txtURL = (TextBox)dgi.FindControl("txtURL");
                        Label lbliCalInvite = (Label)dgi.FindControl("lbliCalInvite");
                        CheckBox chkIsCalderInvite = (CheckBox)dgi.FindControl("chkIsCalderInvite");
                        Label lblEncryptionPreferred = (Label)dgi.FindControl("lblEncryptionPreferred");
                        CheckBox chkEncryptionPreferred = (CheckBox)dgi.FindControl("chkEncryptionPreferred");
                        Label lblEmailID = (Label)dgi.FindControl("lblEmailID");
                        TextBox txtExchangeID = (TextBox)dgi.FindControl("txtExchangeID");
                        Label lblRearSecurityCameraAddress = (Label)dgi.FindControl("lblRearSecurityCameraAddress");
                        TextBox txtRearSecCamAdd = (TextBox)dgi.FindControl("txtRearSecCamAdd");
                        //ZD 104821 Starts
                        Label syslocs = (Label)dgi.FindControl("SysLocation"); 
                        DropDownList drpSysLocation = (DropDownList)dgi.FindControl("lstSystemLocation");
                        DropDownList lstBridgeType = (DropDownList)dgi.FindControl("lstBridgeType");
                        TextBox txtSysLoc = (TextBox)dgi.FindControl("txtSysLoc");

                        //ZD 104821 Ends
                        if (lstProfileType.SelectedValue.Equals("0"))
                        {
                            lstAddressType.SelectedValue = "1";
                            //lstAddressType.Enabled = false;
                            txtUserName.Enabled = true;
                            lblUserName.ForeColor = System.Drawing.Color.Black;
                            txtProfilePassword.Enabled = true;
                            lblPassword.ForeColor = System.Drawing.Color.Black;
                            lblConfirmPassword.ForeColor = System.Drawing.Color.Black;
                            txtProfilePassword2.Enabled = true;
                            lblAPIPort.ForeColor = System.Drawing.Color.Black;
                            txtApiport.Enabled = true;
                            lblTelnetAPIEnabled.ForeColor = System.Drawing.Color.Black;
                            chkP2PSupport.Enabled = true;
                            lblSSH.ForeColor = System.Drawing.Color.Black;//ZD 101363
                            chkSSHSupport.Enabled = true;//ZD 101363

                            lblTelePresence.ForeColor = System.Drawing.Color.Black;
                            chkTelepresence.Enabled = true;
                            lblAddressType.ForeColor = System.Drawing.Color.Black;
                            lstAddressType.Enabled = true;
                            //lblAssignedtoMCU.ForeColor = System.Drawing.Color.Black; //ZD 100815
                            //lstBridges.Enabled = true;
                            lblPreferredDialOption.ForeColor = System.Drawing.Color.Black;
                            lstConnectionType.Enabled = true;
                            lblDefaultProtocol.ForeColor = System.Drawing.Color.Black;
                            lstVideoProtocol.Enabled = true;
                            lblAssociateMCUAddress.ForeColor = System.Drawing.Color.Black;
                            lblAssociateMCUAddress.Enabled = true;
                            lblMCUAddressType.ForeColor = System.Drawing.Color.Black;
                            lstMCUAddressType.Enabled = true;
                            lblWebAccessURL.ForeColor = System.Drawing.Color.Black;
                            txtURL.Enabled = true;
                            lbliCalInvite.ForeColor = System.Drawing.Color.Black;
                            chkIsCalderInvite.Enabled = true;
                            lblEncryptionPreferred.ForeColor = System.Drawing.Color.Black;
                            chkEncryptionPreferred.Enabled = true;
                            lblEmailID.ForeColor = System.Drawing.Color.Black;
                            txtExchangeID.Enabled = true;
                            lblRearSecurityCameraAddress.ForeColor = System.Drawing.Color.Black;
                            txtRearSecCamAdd.Enabled = true;
                            txtMCUAddress.Enabled = true;
                            //ZD 104821 Starts
                            syslocs.Visible = false;
                            drpSysLocation.Visible = false;
                            drpSysLocation.ClearSelection();
                            if (!lstBridges.SelectedValue.Equals("-1"))
                            {
                                int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                                int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                                if (BridgeType == 16)
                                {
                                    syslocs.Visible = true;
                                    drpSysLocation.Visible = true;
                                    BindSystemLocation(BrdgeID, BridgeType, drpSysLocation);
                                    drpSysLocation.Items.FindByValue(txtSysLoc.Text).Selected = true;
                                    drpSysLocation.Enabled = true;
                                    syslocs.Enabled = true;
                                }
                            }
                            //ZD 104821 Ends
                        }
                        else if (lstProfileType.SelectedValue.Equals("1"))
                        {

                            txtUserName.Enabled = false;
                            lblUserName.ForeColor = System.Drawing.Color.Gray;
                            txtProfilePassword.Enabled = false;
                            lblPassword.ForeColor = System.Drawing.Color.Gray;
                            lblConfirmPassword.ForeColor = System.Drawing.Color.Gray;
                            txtProfilePassword2.Enabled = false;
                            lblAPIPort.ForeColor = System.Drawing.Color.Gray;
                            txtApiport.Enabled = false;
                            lblTelnetAPIEnabled.ForeColor = System.Drawing.Color.Gray;
                            chkP2PSupport.Enabled = false;
                            lblSSH.ForeColor = System.Drawing.Color.Gray;//ZD 101363
                            chkSSHSupport.Enabled = false;//ZD 101363
                            chkSSHSupport.Checked = false;

                            lblTelePresence.ForeColor = System.Drawing.Color.Black;
                            chkTelepresence.Enabled = true;
                            lblAddressType.ForeColor = System.Drawing.Color.Black;
                            lstAddressType.Enabled = true;
                            //lblAssignedtoMCU.ForeColor = System.Drawing.Color.Black; //ZD 100815
                            //lstBridges.Enabled = true;
                            lblPreferredDialOption.ForeColor = System.Drawing.Color.Black;
                            lstConnectionType.Enabled = true;
                            lblDefaultProtocol.ForeColor = System.Drawing.Color.Black;
                            lstVideoProtocol.Enabled = true;
                            lblAssociateMCUAddress.ForeColor = System.Drawing.Color.Black;
                            lblAssociateMCUAddress.Enabled = true;
                            lblMCUAddressType.ForeColor = System.Drawing.Color.Black;
                            lstMCUAddressType.Enabled = true;
                            lblWebAccessURL.ForeColor = System.Drawing.Color.Black;
                            txtURL.Enabled = true;
                            lbliCalInvite.ForeColor = System.Drawing.Color.Black;
                            chkIsCalderInvite.Enabled = true;
                            lblEncryptionPreferred.ForeColor = System.Drawing.Color.Black;
                            chkEncryptionPreferred.Enabled = true;
                            lblEmailID.ForeColor = System.Drawing.Color.Black;
                            txtExchangeID.Enabled = true;
                            lblRearSecurityCameraAddress.ForeColor = System.Drawing.Color.Black;
                            txtRearSecCamAdd.Enabled = true;
                            txtMCUAddress.Enabled = true;

                            //ZD 104821 Starts
                            syslocs.Visible = false;
                            drpSysLocation.Visible = false;
                            drpSysLocation.ClearSelection();
                            if (!lstBridges.SelectedValue.Equals("-1"))
                            {
                                int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                                int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                                if (BridgeType == 16)
                                {
                                    syslocs.Visible = true;
                                    drpSysLocation.Visible = true;
                                    BindSystemLocation(BrdgeID, BridgeType, drpSysLocation);
                                    drpSysLocation.Items.FindByValue(txtSysLoc.Text).Selected = true;
                                    drpSysLocation.Enabled = true;
                                    syslocs.Enabled = true;
                                }
                            }
                            //ZD 104821 Ends
                        }
                        else
                        {
                            lblTelePresence.ForeColor = System.Drawing.Color.Gray;
                            chkTelepresence.Enabled = false;
                            lblAddressType.ForeColor = System.Drawing.Color.Gray;
                            lstAddressType.Enabled = false;
                            lblAssignedtoMCU.ForeColor = System.Drawing.Color.Gray; 
                            lstBridges.Enabled = false;
                            txtMCUAddress.Enabled = false;
                            lblPreferredDialOption.ForeColor = System.Drawing.Color.Gray;
                            lstConnectionType.Enabled = false;
                            lblDefaultProtocol.ForeColor = System.Drawing.Color.Gray;
                            lstVideoProtocol.Enabled = false;
                            lblAssociateMCUAddress.ForeColor = System.Drawing.Color.Gray;
                            lblAssociateMCUAddress.Enabled = false;
                            lblMCUAddressType.ForeColor = System.Drawing.Color.Gray;
                            lstMCUAddressType.Enabled = false;
                            lblWebAccessURL.ForeColor = System.Drawing.Color.Gray;
                            txtURL.Enabled = false;
                            lbliCalInvite.ForeColor = System.Drawing.Color.Gray;
                            chkIsCalderInvite.Enabled = false;
                            lblEncryptionPreferred.ForeColor = System.Drawing.Color.Gray;
                            chkEncryptionPreferred.Enabled = false;
                            lblEmailID.ForeColor = System.Drawing.Color.Gray;
                            txtExchangeID.Enabled = false;
                            lblRearSecurityCameraAddress.ForeColor = System.Drawing.Color.Gray;
                            txtRearSecCamAdd.Enabled = false;

                            txtUserName.Enabled = true;
                            lblUserName.ForeColor = System.Drawing.Color.Black;
                            txtProfilePassword.Enabled = true;
                            lblPassword.ForeColor = System.Drawing.Color.Black;
                            lblConfirmPassword.ForeColor = System.Drawing.Color.Black;
                            txtProfilePassword2.Enabled = true;
                            lblAPIPort.ForeColor = System.Drawing.Color.Black;
                            txtApiport.Enabled = true;
                            lblSSH.ForeColor = System.Drawing.Color.Black;//ZD 101363
                            chkSSHSupport.Enabled = true;//ZD 101363
                            //ZD 104821 Starts
                            syslocs.Visible = false;
                            drpSysLocation.Visible = false;
                            drpSysLocation.ClearSelection();
                            if (!lstBridges.SelectedValue.Equals("-1"))
                            {
                                int.TryParse(lstBridges.SelectedValue, out BrdgeID);
                                int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                               
                                if (BridgeType == 16)
                                {
                                    syslocs.Visible = true;
                                    drpSysLocation.Visible = true;
                                    BindSystemLocation(BrdgeID, BridgeType, drpSysLocation);
                                    drpSysLocation.Items.FindByValue(txtSysLoc.Text).Selected = true;
                                    drpSysLocation.Enabled = true;
                                    syslocs.Enabled = true;
                                }
                            }
                            //ZD 104821 Ends
                        } //ZD 100815 end
                    }
                    catch (Exception ex1)
                    {

                        log.Trace("BindData: " + ex1.StackTrace + " : " + ex1.Message);
                    }
                }
                //ZD 100736 END
                
                //FB 1901
                foreach (DataGridItem dgi in dgProfiles.Items)
                {
                    if (Session["isAssignedMCU"] != null)
                        if (Session["isAssignedMCU"].ToString() == "1")
                        {
                            if (((DropDownList)dgi.FindControl("lstProfileType")) != null) //ZD 100815
                                int.TryParse(((DropDownList)dgi.FindControl("lstProfileType")).SelectedValue, out isP2P);
                            if (isP2P != 2)
                            {
                                if (((RequiredFieldValidator)dgi.FindControl("reqBridges")) != null)
                                    ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = true;

                                //FB 2093
                                if (((RequiredFieldValidator)dgi.FindControl("reqPrefDial")) != null)
                                    ((RequiredFieldValidator)dgi.FindControl("reqPrefDial")).Enabled = true;
                            }
                        }

                    //FB 2595 - Starts
                    CheckBox chkTempSecured = (CheckBox)(dgi.FindControl("chkSecured"));
                    if (Session["SecureSwitch"] != null)
                    {
                        if (Session["SecureSwitch"].ToString() == "0")
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:hidden");
                            chkTempSecured.Visible = false;
                            chkTempSecured.Checked = false;
                        }
                        else
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:visible");
                            chkTempSecured.Visible = true;
                        }
                    }
                    
                    if (chkTempSecured.Checked)
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:visible");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style","visibility:visible");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:hidden");
                    }
                    //FB 2595 - End
                    //FB 2993 Start
                    if (Session["EnableNetworkFeatures"].ToString() != null)
                    {
                        if (Session["EnableNetworkFeatures"].ToString().Equals("1"))
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:visible");
                            chkTempSecured.Visible = true;
                        }
                        else
                        {
                            ((Label)dgi.FindControl("lblSecured")).Attributes.Add("style", "visibility:hidden");
                            chkTempSecured.Visible = false;
                            chkTempSecured.Checked = false;
                            ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:hidden");
                            ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:hidden");
                        }
                    }
                    //FB 2993 End

                    //ZD 100132 START
                    CheckBox chkTemp = (CheckBox)(dgi.FindControl("chkIsOutside"));
                    if (chkTemp.Checked)
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");
                        
                    }
                    //ZD 100132 - End
                }

                ViewState.Add("dgDataSource", dt);
            }
            catch (Exception ex)
            {
                log.Trace("LoadProfiles: " + ex.StackTrace + " : " + ex.Message);
            }

        }
        protected void AddNewRow(DataRow dr)
        {
            try
            {
                dr["ProfileID"] = "new";
                dr["ProfileName"] = "New Profile";
                dr["EncryptionPreferred"] = "";
                dr["AddressType"] = "1";
                dr["Password"] = "";
                dr["Address"] = "";
                dr["URL"] = "";
                dr["IsOutside"] = "0";
                dr["ConnectionType"] = "2"; //FB 2565
                dr["VideoEquipment"] = "3";
                dr["Manufacturer"] = "3";//ZD 100736
                dr["DefaultProtocol"] = "1";
                dr["LineRate"] = "768"; //FB 2565
                dr["Bridge"] = "-1";
                dr["BridgeType"] = "0"; //ZD 100619
                dr["MCUAddress"] = "";
                dr["MCUAddressType"] = "-1";
                //Code Added For FB1422
                dr["TelnetAPI"] = "";         
                dr["IsCalendarInvite"] = "";  //Cisco ICAL FB 1602
                dr["Secured"] = "";  //FB 2595
                dr["NetworkURL"]="";//FB 2595
                dr["Securedport"]="";//FB 2595
                dr["UserName"] = ""; //ZD 100814
                dr["ProfileType"] = "0"; //ZD 100815;
                dr["EptResolution"] = "-1";//ZD 100040
            }
            catch (Exception ex)
            {
                log.Trace("AddNewRow: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void SubmitEndpoint(Object sender, EventArgs e)
        {
            string outXML = "";
            try
            {
                if (Page.IsValid)
                {
                    String inXML = GenerateInxml();
                    //Response.Write(obj.Transfer(inXML));
                    //Response.End();
                    if (inXML == "error") //Cisco ICAL FB 1602//FB 2601
                        return;

                    //ZD 101363 Start
                    outXML =  obj.CallCommand("TestEndpointConnection", inXML); 
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        outXML = obj.CallMyVRMServer("SetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //String outXML = "<success>1</success>";
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            GetAllEndPoints(); //FB 2361
                            Response.Redirect("EndpointList.aspx?t=&m=1&Drpvalue=" + DrpValue);
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Text = errLabel.Text.Replace("SSHS Connection failed for Endpoint", obj.GetTranslatedText("SSHS Connection failed for Endpoint"));
                            errLabel.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    //ZD 101363 End
                }
            }
            catch (Exception ex)
            {
                log.Trace("SubmitEndpoint: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected String GenerateInxml()
        {
            try
            {
                int isTelePresence = 0; //FB 2400
                TextBox txtapiTemp = null;
				int Telepresencecount = 0;//FB 2602
                TextBox txtSecuredTemp = null;//FB 2595
                System.Web.UI.HtmlControls.HtmlInputHidden lblPWChange = null; //FB 3054
                System.Web.UI.HtmlControls.HtmlInputHidden lblPass = null; //ZD 100965
                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "      <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "      <EndpointName>" + txtEndpointName.Text + "</EndpointName>";
                inXML += "      <EntityType></EntityType>";
                inXML += "      <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "      <Profiles>";
                foreach (DataGridItem dgi in dgProfiles.Items)
                {
                    TextBox txtTemp = (TextBox)(dgi.FindControl("txtProfileID"));
                    inXML += "        <Profile>";
                    inXML += "          <ProfileID>" + txtTemp.Text + "</ProfileID>";
                    txtTemp = (TextBox)(dgi.FindControl("txtProfileName"));
                    inXML += "          <ProfileName>" + txtTemp.Text + "</ProfileName>";
                    CheckBox chkTemp = (CheckBox)dgi.FindControl("chkDelete");
                    if (chkTemp.Checked)
                        inXML += "          <Deleted>1</Deleted>";
                    else
                        inXML += "          <Deleted>0</Deleted>";

                    //ZD 100815 Start

                    DropDownList lstprof = (DropDownList)dgi.FindControl("lstProfileType");
                    CheckBox rdTemp = (CheckBox)(dgi.FindControl("rdDefault")); //ZD 100815

                    inXML += " <ProfileType>" + lstprof.SelectedValue + "</ProfileType>";

                    if (lstprof.SelectedValue.Equals("0") && rdTemp.Checked)
                    {
                        inXML += "<Default>1</Default>";
                        inXML += "<IsP2PDefault>1</IsP2PDefault>";
                    }
                    else if (lstprof.SelectedValue.Equals("1") && rdTemp.Checked)
                    {
                        inXML += "<Default>1</Default>";
                        inXML += "<IsP2PDefault>0</IsP2PDefault>";
                    }
                    else if (lstprof.SelectedValue.Equals("2") && rdTemp.Checked)
                    {
                        inXML += "<Default>0</Default>";
                        inXML += "<IsP2PDefault>1</IsP2PDefault>";
                    }
                    else
                    {
                        inXML += "<Default>0</Default>";
                        inXML += "<IsP2PDefault>0</IsP2PDefault>";
                    }
                    //ZD 100815 End

                    chkTemp = (CheckBox)(dgi.FindControl("chkEncryptionPreferred"));
                    if (chkTemp.Checked)
                        inXML += "          <EncryptionPreferred>1</EncryptionPreferred>";
                    else
                        inXML += "          <EncryptionPreferred>0</EncryptionPreferred>";

                    DropDownList lstTemp = (DropDownList)(dgi.FindControl("lstAddressType"));
                    inXML += "          <AddressType>" + lstTemp.SelectedValue + "</AddressType>";

                    //ZD 100814
                    txtTemp = (TextBox)dgi.FindControl("txtUserName");
                    inXML += "          <UserName>" + txtTemp.Text + "</UserName>";

                    lblPass = (System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnPass"); //ZD 100965
                    lblPWChange = (System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("lbltxtCompare"); //FB 3054
                    if (lblPWChange.Value == "true")
                        inXML += "          <Password>" + Password(lblPass.Value) + "</Password>"; //FB 3054
                    
                    txtTemp = (TextBox)dgi.FindControl("txtURL");
                    inXML += "          <URL>" + txtTemp.Text + "</URL>";
                    chkTemp = (CheckBox)(dgi.FindControl("chkIsOutside"));
                    if (chkTemp.Checked)
                        inXML += "          <IsOutside>1</IsOutside>";
                    else
                        inXML += "          <IsOutside>0</IsOutside>";
                    
                    //ZD 100132 START
                    if (chkTemp.Checked)
                    {
                        inXML += "<GateKeeeperAddress>" + ((TextBox)(dgi.FindControl("txtGateKeeeperAddress"))).Text + "</GateKeeeperAddress>";
                    }
                    else
                    {
                        inXML += "<GateKeeeperAddress></GateKeeeperAddress>";
                    }
                    //ZD 100132 END
                    
                    lstTemp = (DropDownList)dgi.FindControl("lstConnectionType");
                    inXML += "          <ConnectionType>" + lstTemp.SelectedValue + "</ConnectionType>";
                    lstTemp = (DropDownList)dgi.FindControl("lstVideoEquipment");
                    inXML += "          <VideoEquipment>" + lstTemp.SelectedValue + "</VideoEquipment>";
                    //ZD 100736
                    lstTemp = (DropDownList)dgi.FindControl("lstManufacturer");
                    inXML += "          <Manufacturer>" + lstTemp.SelectedValue + "</Manufacturer>"; 
                   
                    lstTemp = (DropDownList)dgi.FindControl("lstLineRate");
                    inXML += "          <LineRate>" + lstTemp.SelectedValue + "</LineRate>";
                    lstTemp = (DropDownList)dgi.FindControl("lstVideoProtocol");
                    inXML += "          <DefaultProtocol>" + lstTemp.SelectedValue + "</DefaultProtocol>";
                    lstTemp = (DropDownList)dgi.FindControl("lstBridges");
                    inXML += "          <Bridge>" + lstTemp.SelectedValue + "</Bridge>";
                    txtTemp = (TextBox)dgi.FindControl("txtMCUAddress");
                    inXML += "          <MCUAddress>" + txtTemp.Text + "</MCUAddress>";
                    lstTemp = (DropDownList)dgi.FindControl("lstMCUAddressType");
                    inXML += "          <MCUAddressType>" + lstTemp.SelectedValue + "</MCUAddressType>";
                    //Code Added For FB1422-Start
                    chkTemp = (CheckBox)(dgi.FindControl("chkP2PSupport"));
                    if (chkTemp.Checked)
                        inXML += "          <TelnetAPI>1</TelnetAPI>";
                    else
                        inXML += "          <TelnetAPI>0</TelnetAPI>";
                    //Code Added For FB1422-Start
                    CheckBox chkSSHSupport = (CheckBox)(dgi.FindControl("chkSSHSupport"));//ZD 101363
                    if (chkSSHSupport.Checked)
                        inXML += "<SSHSupport>1</SSHSupport>";
                    else
                        inXML += "<SSHSupport>0</SSHSupport>";                    

                    txtTemp = (TextBox)dgi.FindControl("txtExchangeID"); //Cisco Telepresence fix
                    inXML += "          <ExchangeID>" + txtTemp.Text + "</ExchangeID>";

                    //Cisco ICAL FB 1602 -- Start
                    CheckBox chkCalendarInvite = (CheckBox)(dgi.FindControl("chkIsCalderInvite"));   
                    if (chkCalendarInvite.Checked)
                        inXML += "          <IsCalendarInvite>1</IsCalendarInvite>"; 
                    else
                        inXML += "          <IsCalendarInvite>0</IsCalendarInvite>";

                    TextBox txtTemp1 = (TextBox)(dgi.FindControl("txtProfileName"));
                    //API Port starts...
                   
                    if(dgi.FindControl("txtApiport") != null)
                    txtapiTemp = (TextBox)(dgi.FindControl("txtApiport"));
                    
                    if(txtapiTemp.Text!= "")
                        inXML += "              <ApiPortno>" + txtapiTemp.Text + "</ApiPortno>";
                    else
                        inXML += "              <ApiPortno>" + "23" + "</ApiPortno>";
                    //API Port ends...
                    if (chkCalendarInvite.Checked)
                    {
                        if (txtTemp.Text.Trim() == "")
                        {
                            String bret = "error";
                            errLabel.Text = obj.GetTranslatedText("Please enter the ExchangeID for the profile name ") + "'" + txtTemp1.Text + "'";//FB 1830 - Translation //FB 2601
                            errLabel.Visible = true;
                            return bret;
                        }
                    }
                    //Cisco ICAL FB 1602 -- End

                    //FB 2400 start
                    txtTemp = (TextBox)dgi.FindControl("txtAddress");
                    isTelePresence = 0;
                    if (((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked)
                        isTelePresence = 1;

                    if (isTelePresence == 1)//FB 2602
                        Telepresencecount++;
                        
                    String[] multiCodec = ((System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnprofileAddresses")).Value.Split('~');//ZD 100263
                    inXML += "<MultiCodec>";
                    if (isTelePresence == 1)
                    {
                        for (int i = 0; i < multiCodec.Length; i++)
                        {
                            if (multiCodec[i].Trim() != "")
                                inXML += "<Address>" + multiCodec[i].Trim().Replace('~', ' ') + "</Address>";

                            if (i == 0)
                                txtTemp.Text = multiCodec[i].Trim().Replace('~', ' ');
                        }
                    }
                    inXML += "</MultiCodec>";

                    inXML += "<Address>" + txtTemp.Text + "</Address>";
                    inXML += "<RearSecCameraAddress>" + ((TextBox)(dgi.FindControl("txtRearSecCamAdd"))).Text + "</RearSecCameraAddress>";
                    inXML += "<isTelePresence>" + isTelePresence + "</isTelePresence>"; //FB 2400 end
                    //FB 2595 - Starts
                    CheckBox chkTempSecured = (CheckBox)(dgi.FindControl("chkSecured"));
                    if (chkTempSecured.Checked)
                        inXML += "<Secured>1</Secured>";
                    else
                        inXML += "<Secured>0</Secured>";

                    if (dgi.FindControl("txtSecuredport") != null)
                    {
                        txtSecuredTemp = (TextBox)(dgi.FindControl("txtSecuredport"));
                        inXML += "<Securedport>" + txtSecuredTemp.Text + "</Securedport>";
                    }

                    txtTemp = (TextBox)dgi.FindControl("txtNetworkURL");
                    inXML += "<NetworkURL>" + txtTemp.Text + "</NetworkURL>";
                    //FB 2595 - End

                    //ZD 100040 start
                    lstTemp = (DropDownList)dgi.FindControl("lstEptResolution");
                    inXML += "          <EptResolution>" + lstTemp.SelectedValue + "</EptResolution>";
                    chkTemp = (CheckBox)(dgi.FindControl("ChkLCR"));
                    if (chkTemp.Checked)
                        inXML += "          <LCR>1</LCR>";
                    else
                        inXML += "          <LCR>0</LCR>";
                    //ZD 100040

                    //ZD 104821 Starts
                    lstTemp = (DropDownList)dgi.FindControl("lstSystemLocation");
                    if (lstTemp != null)
                        inXML += "<SysLocation>" + lstTemp.SelectedValue + "</SysLocation>";
                    else
                        inXML += "<SysLocation>0</SysLocation>";
                    //ZD 104821 Ends
                    inXML += "</Profile>";
                }
                if (Telepresencecount > 0)//FB 2602
                {
                    if (dgProfiles.Items.Count != Telepresencecount)
                    {
                        errLabel.Text = obj.GetTranslatedText("Please check the telepresence status of all profiles");
                        errLabel.Visible = true;
                        return "error";
                    }
                }                
                inXML += "      </Profiles>";
                inXML += "<EM7EndpointStatus>2</EM7EndpointStatus>"; // FB 2501 EM7 1- Active 0-inActive 2- Not Monitored//ZD 100629 //ZD 100825
                inXML += "<Eptcurrentstatus>0</Eptcurrentstatus>"; // FB 2616 EM7 0- Healthy 
                inXML += "    </SetEndpoint>";
                //Response.Write(obj.Transfer(inXML));
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("GenerateInxml: " + ex.StackTrace + " : " + ex.Message);
                return "<error></error>";
            }
        }
        protected void AddNewProfile(Object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                    if (dgProfiles.Items.Count < 5)
                    {
                        hdnTmpPass.Value = ""; // ZD 100736
                        Session["profCnt"] = dgProfiles.Items.Count + 2; // ZD 100263
                        String outXML = ""; //<success>1</success>";
                        String inXML = GenerateInxml();
                        if (inXML == "error")
                        {
                            errLabel.Visible = true;
                            return;
                        }
						else //FB 2601
                        {
                            errLabel.Visible = false;
                        }
                        outXML = obj.CallMyVRMServer("SetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            GetAllEndPoints(); //FB 2361

                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);

                            Session["EndpointID"] = xmldoc.SelectSingleNode("//SetEndpoint/EndpointID").InnerText;
                            if (Session["ProfileID"] == null)
                                Session.Add("ProfileID", "new");
                            else
                                Session["ProfileID"] = "new";
                            BindData();
                            BindprofileAddresseslist(); //FB 2400
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("At most 5 profiles can be created.");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                log.Trace("AddNewProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void CancelEndpoint(Object sender, EventArgs e)
        {
            Response.Redirect("EndpointList.aspx?t=&Drpvalue="+DrpValue);
        }
        //Fogbugz case 427
        protected void ValidateTypes(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                DataGridItem dgi = (DataGridItem)lstTemp.Parent.Parent;
                ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = false;
                //((RegularExpressionValidator)dgi.FindControl("regAddress")).Enabled = true;
                ((RegularExpressionValidator)dgi.FindControl("regMPI")).Enabled = false;
                if (lstTemp.ID.IndexOf("AddressType") > 0)
                {
                    switch (lstTemp.SelectedValue)
                    {
                            /*commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                        case ns_MyVRMNet.vrmAddressType.MPI:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = true;
                            ((RegularExpressionValidator)dgi.FindControl("regAddress")).Enabled = false;
                            ((RegularExpressionValidator)dgi.FindControl("regMPI")).Enabled = true;
                            isMarkedDeleted.Value = "1";
                            AllowCreatingNew(dgi, false);
                            break;
                             */
                        //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage END
                        case ns_MyVRMNet.vrmAddressType.ISDN:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            if (((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                            {
                                ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                            }
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.ISDN).Selected = true;
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            AllowCreatingNew(dgi, true);
                            isMarkedDeleted.Value = "0";
                            break;
                        case ns_MyVRMNet.vrmAddressType.IP:
                            //Removed MPI ZD 103595- Operations - Remove MPI Functionality and Verbiage
                            if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                            {
                                ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.IP).Selected = true;
                            }
                            if (((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                            {
                                ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                            }
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.IP).Selected = true;
                            //Remove MPI for ZD 103595- Operations - Remove MPI Functionality and Verbiage
                            if (((DropDownList)dgi.FindControl("lstMCUAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                            {
                                ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.IP).Selected = true;
                            }
                            AllowCreatingNew(dgi, true);
                            isMarkedDeleted.Value = "0";
                            break;
                    }
                }
                if (lstTemp.ID.IndexOf("Protocol") > 0)
                {
                    switch (lstTemp.SelectedValue)
                    {
                            /*commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                        case ns_MyVRMNet.vrmVideoProtocol.MPI:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                            ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = true;
                            AllowCreatingNew(dgi, false);
                            isMarkedDeleted.Value = "1";
                            break;
                             */
                        //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage END
                        case ns_MyVRMNet.vrmVideoProtocol.ISDN:
                            ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            if (((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                            {
                                ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                                ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                            }
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                            ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                            AllowCreatingNew(dgi, true);
                            isMarkedDeleted.Value = "0";
                            break;
                    }
                }

                //if (lstTemp.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && lstTemp.ID.IndexOf("AddressType") > 0)
                //{
                //    ((DropDownList)dgi.FindControl("lstAddressType")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                //    ((DropDownList)dgi.FindControl("lstConnectionType")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstConnectionType")).Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                //    ((DropDownList)dgi.FindControl("lstVideoProtocol")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstVideoProtocol")).Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                //    ((DropDownList)dgi.FindControl("lstMCUAddressType")).ClearSelection();
                //    ((DropDownList)dgi.FindControl("lstMCUAddressType")).Items.FindByValue(ns_MyVRMNet.vrmAddressType.MPI).Selected = true;
                //}
            }
            catch (Exception ex)
            {
                log.Trace("Validatetypes: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void AllowCreatingNew(DataGridItem dgiCurrent, bool isEnabled)
        {
            try
            {
                btnAddNewProfile.Visible = isEnabled;
                isMarkedDeleted.Value = "0";
                int BridgeType = 0, connectionType = 0; //ZD 100619 Starts
                
                if (dgProfiles.Items.Count > 1)
                    foreach (DataGridItem dgi in dgProfiles.Items)
                    {
                        if (!dgi.Equals(dgiCurrent))
                        {
                            ((CheckBox)dgi.FindControl("chkDelete")).Enabled = isEnabled;
                            ((CheckBox)dgi.FindControl("chkDelete")).Checked = !isEnabled;
                            ((CheckBox)dgi.FindControl("rdDefault")).Enabled = isEnabled;//ZD 100815
                            //FB Case 734 changes start here: Saima
                            //Removed MPI check for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
								//ZD 100619 Starts
                                int.TryParse(((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.ToString(), out connectionType);
                                int.TryParse(((DropDownList)dgi.FindControl("lstBridgeType")).SelectedItem.Text, out BridgeType);
								//ZD 100619 Ends
                                //((RadioButton)dgi.FindControl("rdDefault")).Checked = isEnabled; //Commented for FB 1845
                                ((RequiredFieldValidator)dgi.FindControl("reqAddressType")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqName")).Enabled = isEnabled;
                                //ZD 100619 Starts
								if (connectionType == 1)
                                {
                                    if (BridgeType == 8)
                                        ((RequiredFieldValidator)dgi.FindControl("reqProfAddress")).Enabled = isEnabled;
                                    else
                                        ((RequiredFieldValidator)dgi.FindControl("reqProfAddress")).Enabled = !isEnabled;
                                }
                                else
                                    ((RequiredFieldValidator)dgi.FindControl("reqVideoEquipment")).Enabled = isEnabled;
								//ZD 100619 Ends
                                ((RequiredFieldValidator)dgi.FindControl("reqLineRate")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = isEnabled;
                                ((RequiredFieldValidator)dgi.FindControl("reqManufacturerType")).Enabled = isEnabled;//ZD 100736
                                isMarkedDeleted.Value = "1";
                            //Removed MPI check ZD 103595- Operations - Remove MPI Functionality and Verbiage END
                            //FB Case 734 changes end here: Saima
                        }
                        else
                        {
                            //if (!isEnabled) //Commented for FB 1845
                              //  ((RadioButton)dgi.FindControl("rdDefault")).Checked = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqAddressType")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqName")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqAddress")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqVideoEquipment")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqManufacturerType")).Enabled = !isEnabled;//ZD 100736
                            ((RequiredFieldValidator)dgi.FindControl("reqLineRate")).Enabled = !isEnabled;
                            ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = !isEnabled;
                        }
                    }
            }
            catch (Exception ex)
            {
                log.Trace("AllowCreatingNew: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void ValidateInput(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                int P2Pprofile = 0, MCUProfile = 0, BothProfile = 0; //ZD 100815
                cvSubmit.Text = "";
				//FB 3012 Starts
                bool valid = true, isDefaultProfileset = false; //ZD 100815
                e.IsValid = true;
                string patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}\:?(\d{4})?$";
                Regex checkIP = new Regex(patternIP);
                String patternISDN = @"^[0-9]+$";
                Regex checkISDN = new Regex(patternISDN);
                int BridgeType = 0, connectionType = 0; //ZD 100619 
                foreach (DataGridItem dgi in dgProfiles.Items) //FB 2400
                {
                    int.TryParse(((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue.ToString(), out connectionType); //ZD 100619 
                    int.TryParse(((DropDownList)dgi.FindControl("lstBridgeType")).SelectedItem.Text, out BridgeType); //ZD 100619 
                    DropDownList lstVideoProtocol = (DropDownList)dgi.FindControl("lstVideoProtocol");
                    DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                    RequiredFieldValidator reqAdd = ((RequiredFieldValidator)dgi.FindControl("reqAddress")); 
                    RequiredFieldValidator reqProAdd = ((RequiredFieldValidator)dgi.FindControl("reqProfAddress")); //ZD 100619
                    TextBox txtAdd = ((TextBox)dgi.FindControl("txtAddress"));
                    String[] IPToValidate = ((System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnprofileAddresses")).Value.Split('~');//ZD 100263

                    reqAdd.Enabled = false; reqProAdd.Enabled = false;
                    reqAdd.ControlToValidate = "lstProfileAddress";
                    if ((dgProfiles.Items.Count > 1) || (dgProfiles.Items.Count == 1 && !((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked))
                        reqAdd.ControlToValidate = "txtAddress";
                    
                    if(!((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked)
                        IPToValidate = new String[] { txtAdd.Text };

                    if (IPToValidate.Length == 1 && IPToValidate[0].Trim() == "") 
                    {
                        reqAdd.Enabled = true;
                        valid = false;
                    }
                    //ZD 100815 start
                    CheckBox rdTemp = (CheckBox)(dgi.FindControl("rdDefault")); //ZD 100815
                    CheckBox DeleteChck =(CheckBox)(dgi.FindControl("chkDelete"));
                    if (rdTemp.Checked && !DeleteChck.Checked)
                        isDefaultProfileset = true;

                    DropDownList lstProfileType = (DropDownList)dgi.FindControl("lstProfileType");
                    if (lstProfileType.SelectedValue.Equals("0") && rdTemp.Checked)
                        P2Pprofile = P2Pprofile + 1;
                    else if (lstProfileType.SelectedValue.Equals("1") && rdTemp.Checked)
                        MCUProfile = MCUProfile + 1;
                    else if (lstProfileType.SelectedValue.Equals("2") && rdTemp.Checked)
                        BothProfile = BothProfile + 1;

                    if (lstProfileType.SelectedValue.Equals("0") && !((CheckBox)(dgi.FindControl("chkTelepresence"))).Checked)
                    {
                        if (lstAddressType.SelectedValue.Equals("1"))
                        {
                            valid = checkIP.IsMatch(txtAdd.Text, 0);
                            if (!valid)
                            {
                                ((TextBox)dgi.FindControl("txtAddress")).Focus();
                                cvSubmit.Text = obj.GetTranslatedText("Invalid IP Address:") + txtAdd.Text;
                            }
                        }
                        else
                        {
                            valid = false;
                            cvSubmit.Text = obj.GetTranslatedText("Only an IP address is permitted for this profile type.");
                        }

                        e.IsValid = valid;
                        if (valid.Equals(false))
                            break;

                    }
                    //ZD 100815 End
                    else if (!((CheckBox)(dgi.FindControl("chkDelete"))).Checked) //FB 2044
                    {
                        for (int i = 0; i < IPToValidate.Length; i++)
                        {                            
                            switch (lstAddressType.SelectedValue)
                            {
                                case ns_MyVRMNet.vrmAddressType.IP:
                                    {
                                        if (lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP))
                                        {
                                            
                                            if (IPToValidate[i] == "")
                                                valid = false;
                                            else
                                                valid = checkIP.IsMatch(IPToValidate[i], 0);
                                            if (!valid)
                                            {
                                                ((TextBox)dgi.FindControl("txtAddress")).Focus();
                                                cvSubmit.Text = obj.GetTranslatedText("Invalid IP Address:") + IPToValidate[i];//FB 1830 - Translation
                                            }
                                        }
                                        else
                                        {
                                            valid = false;
                                            cvSubmit.Text = obj.GetTranslatedText("Default Protocol does not match with selected Address Type.");
                                        }
                                        break;
                                    }

                                case ns_MyVRMNet.vrmAddressType.ISDN:
                                    {
                                        if (lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN))
                                        {                                           
                                            if (IPToValidate[i] == "")
                                                valid = false;
                                            else
                                                valid = checkISDN.IsMatch(IPToValidate[i], 0);
                                            if (!valid)
                                            {
                                                ((TextBox)dgi.FindControl("txtAddress")).Focus();
                                                cvSubmit.Text = obj.GetTranslatedText("Invalid ISDN Address") + ":" + IPToValidate[i];//FB 1830 - Translation
                                            }
                                        }
                                        else
                                        {
                                            valid = false;
                                            cvSubmit.Text = obj.GetTranslatedText("Default Protocol does not match with selected Address Type.");
                                        }
                                        break;
                                    }
                            }                              
                                
                            String errMsg = "";
                            //ZD 103595 START
                            //if (!AreMPITypesValid(dgi, ref errMsg))  //Fogbugz case 427
                            //{
                            //    valid = false;
                            //    cvSubmit.Text = errMsg;
                            //}
                            //ZD 103595 END
                            e.IsValid = valid;
                            if (valid.Equals(false))
                                break;

                            //FB 3012 Ends          
                        }
                    }
                    //FB 2703 - Start
                    CheckBox chkTempSecured = (CheckBox)(dgi.FindControl("chkSecured"));
                    if (chkTempSecured.Checked)
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:visible");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("lblNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((Label)dgi.FindControl("lblSecuredPort")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtNetworkURL")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtSecuredport")).Attributes.Add("style", "visibility:hidden");
                    }
                    //FB 2703 - End

                    //ZD 100132 START
                    CheckBox chkTemp = (CheckBox)(dgi.FindControl("chkIsOutside"));
                    if (chkTemp.Checked)
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:visible");
                    }
                    else
                    {
                        ((Label)dgi.FindControl("LblGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");
                        ((TextBox)dgi.FindControl("txtGateKeeeperAddress")).Attributes.Add("style", "visibility:hidden");

                    }
                    //ZD 100132 - End
                    
                    //FB 2044 - Starts
                    if (((CheckBox)(dgi.FindControl("chkDelete"))).Checked)
                    {
                        reqAdd.Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqBridges")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqName")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regProfileName")).Enabled = false;

                        ((RegularExpressionValidator)dgi.FindControl("regnumPassword1")).Enabled = false;
                        ((CompareValidator)dgi.FindControl("cmpPass1")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regnumPassword2")).Enabled = false;
                        ((CompareValidator)dgi.FindControl("cmpPass2")).Enabled = false;

                        ((RegularExpressionValidator)dgi.FindControl("regAddress")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regMPI")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqAddressType")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqVideoEquipment")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqManufacturerType")).Enabled = false;//ZD 100736
                        ((RequiredFieldValidator)dgi.FindControl("reqLineRate")).Enabled = false;
                        ((RequiredFieldValidator)dgi.FindControl("reqPrefDial")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegMCUAddress")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegURL")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegExchangeID")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("RegApiport")).Enabled = false;
                        ((RegularExpressionValidator)dgi.FindControl("regRearSecCamAdd")).Enabled = false;
                        valid = true;
                    }
                    //FB 2044 - End
                    
                    if (valid.Equals(false))
                        break;
                    //Response.End();
                }
                //ZD 100815 Starts
                if (P2Pprofile > 0)
                    P2Pprofile = MCUProfile + BothProfile + P2Pprofile;

                if (!isDefaultProfileset) 
                {
                    valid = false;
                    cvSubmit.Text = obj.GetTranslatedText("A Default Profile should be selected");
                }
                if (P2Pprofile > 1 || MCUProfile > 1 || BothProfile > 1)
                {
                    valid = false;
                    cvSubmit.Text = obj.GetTranslatedText("Profile type cannot be the same");
                }
                //ZD 100815 Ends
                e.IsValid = valid;

            }
            catch (Exception ex)
            {
                log.Trace("ValidateInput: " + ex.StackTrace + " : " + ex.Message);
                e.IsValid = false;
            }
        }
        //Fogbugz case 427
        /* Code lines commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
        protected bool AreMPITypesValid(DataGridItem dgi, ref String errMsg)
        {
            try
            {
                DropDownList lstVideoProtocol = (DropDownList)dgi.FindControl("lstVideoProtocol");
                DropDownList lstConnectionType = (DropDownList)dgi.FindControl("lstConnectionType");
                DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                DropDownList lstMCUAddressType = (DropDownList)dgi.FindControl("lstMCUAddressType");
                DropDownList lstMCU = (DropDownList)dgi.FindControl("lstBridges");

                TextBox txtAddress = (TextBox)dgi.FindControl("txtAddress");

                if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    if (!lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    {
                        errMsg = obj.GetTranslatedText("Invalid MCU Address Type, Connection Type or Protocol Selected for profile #:") + " " +(dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    if (!lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    {
                        errMsg = obj.GetTranslatedText("Invalid Address Type, Connection Type or Protocol Selected for profile #:") + " " + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                    if (!lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    {
                        errMsg = obj.GetTranslatedText("Invalid MCU Address Type, Address Type or Protocol Selected for profile #:") + " " + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstVideoProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI))
                    if (!lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) || !lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    {
                        errMsg = obj.GetTranslatedText("Invalid Address Types, Connection Type or MCU Address Type Selected for profile #:") + (dgi.ItemIndex + 1).ToString();
                        return false;
                    }
                if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && !obj.IsValidMCUForMPI(lstMCU.SelectedValue, ref errMsg))
                {
                    errMsg = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidMPIBridge);//ZD 100288
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("TypesAreValid: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }
        */
        //Code lines commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage END
        //FB 2361
        #region GetAllEndPoints

        private void GetAllEndPoints()
        {
            try
            {
                String eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();//FB 1830
                //FB 2594 Starts
                string isPublicEP = "0";
                if (Session["EnablePublicRooms"].ToString() == "1")
                    isPublicEP = "1";
                //FB 2594 Ends
                String inXML = "";
                inXML += "<EndpointDetails>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <PublicEndpoint>" + isPublicEP + "</PublicEndpoint>"; //FB 2594
                inXML += "</EndpointDetails>";

                if (File.Exists(eptxmlPath))
                    File.Delete(eptxmlPath);

                String outXML1 = obj.CallMyVRMServer("GetAllEndpoints", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML1.IndexOf("<error>") < 0)
                {
                    XmlDocument endPoints = new XmlDocument();
                    endPoints.LoadXml(outXML1);
                    endPoints.Save(eptxmlPath);
                    endPoints = null;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "GetAllEndPoints error: " + ex.Message;
                log.Trace(ex.StackTrace + " GetAllEndPoints error : " + ex.Message);
            }
        }

        #endregion

        //FB 2400
        #region BindprofileAddresseslist
        /// <summary>
        /// BindprofileAddresseslist
        /// </summary>
        private void BindprofileAddresseslist()
        {
            String[] ProfileAdds = null;

            if (dgProfiles.Items.Count == 1)
            {
                //FB 2602
                //if (((CheckBox)(dgProfiles.Items[0].FindControl("chkTelepresence"))).Checked)
                //    btnAddNewProfile.Enabled = false;
                //else
                btnAddNewProfile.Disabled = false; //ZD 100420
            }

            DataGridItem dgi = null;
            for (int p = 0; p < dgProfiles.Items.Count; p++)
            {
                dgi = dgProfiles.Items[p];
                System.Web.UI.HtmlControls.HtmlInputHidden hdnprofileAddresses
                       = (System.Web.UI.HtmlControls.HtmlInputHidden)(dgi.FindControl("hdnprofileAddresses"));
                ListBox lstProfileAddress = (ListBox)(dgi.FindControl("lstProfileAddress"));
                lstProfileAddress.Items.Clear();


               if (!((CheckBox)(dgProfiles.Items[p].FindControl("chkTelepresence"))).Checked) //Telepresence will be available if endpoint has only one profile
               {
                    //FB 2602
                    //if (dgProfiles.Items.Count > 1)
                        //((CheckBox)(dgProfiles.Items[p].FindControl("chkTelepresence"))).Enabled = false;
                    
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Enabled = false;
                    ((ListBox)(dgProfiles.Items[p].FindControl("lstProfileAddress"))).Enabled = false;
                    ((System.Web.UI.HtmlControls.HtmlInputHidden)dgi.FindControl("hdnprofileAddresses")).Value = "";
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Attributes.Add("onfocus", "this.style.borderColor = 'black'");//ZD 100420
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Attributes.Add("onblur", "this.style.borderColor = ''");
               }
                else
                {
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Enabled = true;
                    ((ListBox)(dgProfiles.Items[p].FindControl("lstProfileAddress"))).Enabled = true;
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Attributes.Add("onfocus", "this.style.borderColor = 'black'");//ZD 100420
                    ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Attributes.Add("onblur", "this.style.borderColor = ''");
                }
               ((Button)(dgProfiles.Items[p].FindControl("btnProfileAddr"))).Attributes.Add("OnClick", "javascript:return AddRemoveList('add',this)");                     

                if (hdnprofileAddresses != null && hdnprofileAddresses.Value.Trim() != "")
                {
                    ProfileAdds = hdnprofileAddresses.Value.Split('~');//ZD 100263
                    for (int i = 0; i < ProfileAdds.Length; i++)
                    {
                        if (ProfileAdds[i].Trim() != "")
                        {
                            ListItem item = new ListItem();
                            item.Text = ProfileAdds[i];
                            item.Attributes.Add("title", ProfileAdds[i]);
                            lstProfileAddress.Items.Add(item);
                        }
                    }
                }

            }
        }
        #endregion

        protected string Password(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }

        //ZD 101026 Starts
        #region LoadHistory
        /// <summary>
        /// LoadHistory
        /// </summary>
        /// <param name="changedHistoryNodes"></param>
        /// <returns></returns>
        private bool LoadHistory(XmlNodeList changedHistoryNodes)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            try
            {
                DataTable dtable = new DataTable();
                if (!dtable.Columns.Contains("ModifiedUserId"))
                    dtable.Columns.Add("ModifiedUserId");
                if (!dtable.Columns.Contains("ModifiedDateTime"))
                    dtable.Columns.Add("ModifiedDateTime");
                if (!dtable.Columns.Contains("ModifiedUserName"))
                    dtable.Columns.Add("ModifiedUserName");
                if (!dtable.Columns.Contains("Description"))
                    dtable.Columns.Add("Description");

                if (changedHistoryNodes != null && changedHistoryNodes.Count > 0)
                {
                    dtable = obj.LoadDataTable(changedHistoryNodes, null);
                    DateTime ModifiedDateTime = DateTime.Now;

                    for (Int32 i = 0; i < dtable.Rows.Count; i++)
                    {
                        DateTime.TryParse(dtable.Rows[i]["ModifiedDateTime"].ToString(), out ModifiedDateTime);
                        dtable.Rows[i]["ModifiedDateTime"] = myVRMNet.NETFunctions.GetFormattedDate(ModifiedDateTime.Date.ToString()) + " " + ModifiedDateTime.ToString(tformat);

                        dtable.Rows[i]["Description"] = obj.GetTranslatedText(dtable.Rows[i]["Description"].ToString());
                    }
                }
                dgChangedHistory.DataSource = dtable;
                dgChangedHistory.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace("LoadHistory" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 101026 End

        //ZD 100619 Starts
        #region ChangeMCU
        protected void ChangeMCU(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                int BridgeType = 0, ConnectionType = 0, BrdgeID = -1;
                DataGridItem clickedRow = ((DropDownList)sender).NamingContainer as DataGridItem;
                DropDownList lstBridge = (DropDownList)clickedRow.FindControl("lstBridges");
                DropDownList lstBridgeType = (DropDownList)clickedRow.FindControl("lstBridgeType");
                DropDownList lstConnectionType = (DropDownList)clickedRow.FindControl("lstConnectionType");
                Label syslocs = (Label)clickedRow.FindControl("SysLocation"); //ZD 104821
                DropDownList drpSysLocation = (DropDownList)clickedRow.FindControl("lstSystemLocation"); //ZD 104821

                syslocs.Visible = false;
                drpSysLocation.Visible = false;
                drpSysLocation.ClearSelection();
                if (!lstBridge.SelectedValue.Equals("-1"))
                {
                    lstBridgeType.SelectedValue = lstBridge.SelectedValue;
                    int.TryParse(lstBridge.SelectedValue, out BrdgeID);
                    int.TryParse(lstBridgeType.SelectedItem.Text,out BridgeType);
                    int.TryParse(lstConnectionType.SelectedValue.ToString(), out ConnectionType);
                    //ZD 104821 Starts
                    drpSysLocation.Visible = false;
                    if (BridgeType == 16)
                    {
                        syslocs.Visible = true;
                        drpSysLocation.Visible = true;
                        BindSystemLocation(BrdgeID, BridgeType, drpSysLocation);
                        drpSysLocation.Enabled = true;
                        syslocs.Enabled = true;
                        //ZD 104821 Ends
                    }
                }
                ((RequiredFieldValidator)clickedRow.FindControl("reqProfAddress")).Enabled = false;
                if (ConnectionType == 1)
                {
                    if (BridgeType == 8)
                        ((RequiredFieldValidator)clickedRow.FindControl("reqProfAddress")).Enabled = true;
                }
                else
                    ((RequiredFieldValidator)clickedRow.FindControl("reqProfAddress")).Enabled = true;

            }
            catch (Exception ex)
            {
                log.Trace("UpdateEndpointModel: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100619 Ends
        

        //ZD 104821 Starts
        #region BindSystemLocation
        /// <summary>
        /// BindSystemLocation
        /// </summary>
        protected void BindSystemLocation(int BridgeID, int BridgeType, DropDownList drpdwnSysLocation)
        {
            string outXML = "";
            StringBuilder inXML = new StringBuilder();
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML.Append("<GetSystemLocation>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>");
                inXML.Append("<BridgeId>" + BridgeID + "</BridgeId>");
                inXML.Append("<BridgeTypeId>" + BridgeType + "</BridgeTypeId>");
                inXML.Append("</GetSystemLocation>");

                outXML = obj.CallMyVRMServer("GetSystemLocation", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));
                    XmlNodeList nodes = xmlDoc.SelectNodes("//GetSystemLocations/GetSystemLocation");
                    drpdwnSysLocation.Items.Clear();
                    if (nodes.Count > 0)
                        LoadList(drpdwnSysLocation, nodes);
                    drpdwnSysLocation.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0"));
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindSystemLocation" + ex.Message);
            }
        }
        #endregion

        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                if (dt.Columns.Contains("name"))//FB 2272
                    foreach (DataRow dr in dt.Rows)
                        dr["name"] = obj.GetTranslatedText(dr["name"].ToString());
                if (dt.Columns.Contains("name"))
                    dt.DefaultView.Sort = "name ASC";//ZD 103787
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
		//ZD 104821 End
    }
}
