﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace ns_MyVRM
{

    public partial class ManageAudioAddOnBridges : System.Web.UI.Page
    {
        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #endregion

        #region ProtectDataMember
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLicencesRemaining;
        
        protected System.Web.UI.WebControls.Label lblTotalUsers;
        protected System.Web.UI.WebControls.Label lblLicencesRemaining;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.DataGrid dgAudioBridges;
        protected System.Web.UI.WebControls.Button btnNewAudioBridge;
        protected System.Web.UI.WebControls.Table tblNoAudioBridges;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNewBridge;
        protected System.Web.UI.WebControls.Table tblPage;//FB 2023
        protected int isLDAP = 0; //ZD 101443

        #endregion

        #region ManageAudioAddOnBridges
        /// <summary>
        /// ManageAudioAddOnBridges
        /// </summary>
        public ManageAudioAddOnBridges()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("manageaudioaddonbridges.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //FB 2670
                if (Session["admin"].ToString().Equals("2") || Session["admin"].ToString().Equals("3"))
                    trNewBridge.Visible = true;
                else
                    trNewBridge.Visible = false;

                //ZD 101443 START
                if (Session["IsLDAP"] != null && Session["IsLDAP"].ToString() != "")
                    int.TryParse(Session["IsLDAP"].ToString(), out isLDAP);
                //ZD 101443 End

                BindAudioBridges();

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                   // btnNewAudioBridge.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnNewAudioBridge.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263
                    btnNewAudioBridge.Visible = false;
                }
                else
                    btnNewAudioBridge.Attributes.Add("Class", "altMedium0BlueButtonFormat");// FB 2796

            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region BindAudioBridges
        /// <summary>
        /// BindAudioBridges
        /// </summary>
        private void BindAudioBridges()
        {
            try
            {
				//FB 2023 - Starts
                string inXML = "", outXML="", pageNo="1",Alpha="ALL",sortBy="1";
                int totalPages = 1;

                if (Request.QueryString["pageNo"] != null)
                    pageNo = Request.QueryString["pageNo"].ToString();

                inXML = "<login>"
                      + obj.OrgXMLElement()
                      + "  <userID>" + Session["userID"].ToString() + "</userID>"
                      + "  <sortBy>" + sortBy + "</sortBy>" //FB 2023 - End
                      + "  <alphabet>" + Alpha + "</alphabet>"
                      + "  <pageNo>" + pageNo + "</pageNo>"
                      + " <audioaddon>1</audioaddon>"
                      + "</login>";

                outXML = obj.CallMyVRMServer("GetManageUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    DataTable dt = new DataTable();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                    totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//users/totalPages").InnerText);
                    
                    lblTotalUsers.Text = xmldoc.SelectSingleNode("//users/totalNumber").InnerText;

                    //ZD 101443 Starts
                    if (isLDAP == 1)
                        lblLicencesRemaining.Text = obj.GetTranslatedText("N/A"); 
                    else
                        lblLicencesRemaining.Text = xmldoc.SelectSingleNode("//users/licensesRemain").InnerText;
                    //ZD 101443 End
                    
                    if (nodes.Count > 0)
                    {
                        dgAudioBridges.Visible = true;
                        tblNoAudioBridges.Visible = false;
                        dt = obj.LoadDataTable(nodes, null);
                        if (!dt.Columns.Contains("Status")) dt.Columns.Add("Status");
                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["Status"] = "Active";
                        }

                        dgAudioBridges.DataSource = dt;
                        dgAudioBridges.DataBind();

                        //FB 2670
                        if (Session["admin"].ToString() == "3")
                        {
                            foreach (DataGridItem dgi in dgAudioBridges.Items)
                            {
                                ((LinkButton)dgi.FindControl("btnEdit")).Text = obj.GetTranslatedText("View");
                                ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;//ZD 100263
                            }
                        }

                        if (totalPages > 1)//FB 2023
                        {
                            //  Request.QueryString.Remove("pageNo");
                            obj.DisplayPaging(totalPages, Int32.Parse(pageNo), tblPage, "ManageAudioAddOnBridges.aspx?");
                        }

                    }
                    else
                    {
                        dgAudioBridges.Visible = false;
                        tblNoAudioBridges.Visible = true;
                    }
                }
                else
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                    lblError.Visible = true;
                }

            }
            catch (Exception ex)
            {
                log.Trace("BindAudioBridges" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region CreateNewAudioAddonBridge
        /// <summary>
        /// CreateNewAudioAddonBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateNewAudioAddonBridge(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("AudioAddOnBridge.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("CreateNewAudioAddonBridge" + ex.Message);
            }
        }
        #endregion

        #region EditAudionAddonBridge
        /// <summary>
        /// EditAudionAddonBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditAudionAddonBridge(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                log.Trace("AudioBridgeToEdit :"+ e.Item.Cells[0].Text);
                Response.Redirect("AudioAddOnBridge.aspx?t=" + e.Item.Cells[0].Text);
            }
            catch (Exception ex)
            {
                lblError.Text = obj.ShowSystemMessage(); //ZD 100263
            }
        }
        #endregion

        #region DeleteAudionAddonBridge
        /// <summary>
        /// DeleteAudionAddonBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteAudionAddonBridge(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <user>";
                inXML += "      <userID>" + e.Item.Cells[0].Text + "</userID>";
                inXML += "      <action>-1</action>";
                inXML += "  </user>";
                inXML += "</login>";
                
                String outXML = obj.CallMyVRMServer("ChangeUserStatus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    lblError.Text = obj.GetTranslatedText("Operation Successful!");
                    BindAudioBridges();
                }
                else
                    lblError.Text = obj.ShowErrorMessage(outXML);
            }
            catch (Exception ex)
            {
                lblError.Text = obj.ShowSystemMessage(); //ZD 100263
                log.Trace("EditAudionAddonBridge" + ex.StackTrace);
            }
        }
        #endregion

    }
}
