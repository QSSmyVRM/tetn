/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using ns_SqlHelper;
using System.Text;
using System.Collections.Generic;


namespace myVRMNet
{
    public partial class UtilizationReport:System.Web.UI.Page
    {
        #region Private Data Members

        private ArrayList entityList = null;
        private static Hashtable roomsList = null;
        private String utilRptKey = "";
        private String utilURLKey = "";

        private String DestDirectory = "";
        private String redURL = "";
        private String dateTimeString = "";
        private Int32 export = 0;
        
        private string dFile = "";
        DataSet ds = null;
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
       
        #endregion

        #region protected Data Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtStartDate;
        protected System.Web.UI.WebControls.TextBox txtEndDate;
        protected System.Web.UI.WebControls.Button btnExcel;
        protected System.Web.UI.WebControls.DropDownList drpTimeZone;
        protected System.Web.UI.WebControls.DropDownList drpEntitiyCodeRange;
        //protected System.Web.UI.WebControls.DropDownList drpEntity;//ZD 102909
        protected System.Web.UI.WebControls.TextBox txtRooms;
        protected System.Web.UI.WebControls.CheckBox chkSelectall;
        protected System.Web.UI.WebControls.CheckBoxList cblRoom;
        protected System.Web.UI.WebControls.DataGrid dgRptDt;
        protected System.Web.UI.WebControls.DataGrid dgSummary;
        protected System.Web.UI.WebControls.Table tblNoDt;
        protected System.Web.UI.WebControls.Label lblDetails;
        protected System.Web.UI.WebControls.Label lblSummary;
        protected String language = "";//FB 1830
        protected System.Web.UI.WebControls.Button btnviewreport;//FB 2292
        protected String format = "MM/dd/yyyy"; //FB 2404 - Default for this Report.
        protected String tformat = "hh:mm tt";//FB 2588
        protected String dtformat = ""; //ZD 100958
        protected System.Web.UI.WebControls.TextBox txtEntity;//ZD 102909


        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Constructor
        public UtilizationReport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion
        
        #region Page Load Event Handler

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("UtilizationReport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                utilRptKey = "UtilRpt" + Session.SessionID;
                utilURLKey = "UtilURL" + Session.SessionID;
                //FB 1830- Starts
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830- End

                //FB 2588
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                //ZD 100958
                dtformat = format;
                if (Session["EmailDateFormat"] != null && Session["EmailDateFormat"].ToString() == "1")
                    dtformat = "dd MMM yyyy";

                if (Session["timeFormat"] != null)
                {
                    if (Session["timeFormat"].ToString() != "")
                        if (Session["timeFormat"].ToString() == "0")
                            tformat = "HH:mm";
                        else if (Session["timeFormat"].ToString() == "2")//FB 2588
                            tformat = "HHmmZ";
                }

                if (!IsPostBack)
                {
                    BindData();
                    ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>fnAssignDefault()</script>");
                    FetchEntityReports();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        #endregion

        #region IntializeUIResources

        private void IntializeUIResources()
        {
            this.Page.Init += new EventHandler(Page_Init);

            btnExcel.Attributes.Add("onclick", "javascript:return fnValidate();");
            btnviewreport.Attributes.Add("onclick", "javascript:return fnValidate();");//FB 2292
            chkSelectall.Attributes.Add("onclick", "javascript:fnSelectAll(this,'" + cblRoom.ClientID + "');");

        }


        #endregion

        #region Page Init Event Handler

        void Page_Init(object sender, EventArgs e)
        {
            try
            {
                IntializeUIResources();
            }
            catch
            {
                Response.Write("The method or operation is not implemented.");
            }
        }

        #endregion

        #region BindData

        private void BindData()
        {
            try
            {
                //PopulateEntity();//ZD 102909
                PopulateTimeZone();
                PopulateEntityCodeRange();
                PopulateRooms();

                if (!IsPostBack)
                {
                    //ZD 102497
                    //drpTimeZone.SelectedValue = "19";
                    drpTimeZone.SelectedValue = "26";
                    drpEntitiyCodeRange.SelectedValue = ((Int32)enumEntityCodeRange.ALL).ToString();
                    //FB 2404
                    //txtStartDate.Text = DateTime.Today.ToShortDateString() + " 00:00";
                    //txtEndDate.Text = DateTime.Today.ToShortDateString() + " 23:59";
                    txtStartDate.Text = DateTime.Today.ToString(format);
                    txtEndDate.Text = DateTime.Today.AddDays(1).ToString(format);
                }

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }

        }

        #endregion

        //ZD 102909 - Start
        /* #region PopulateEntity
        
        private DataTable GetEntitities()
        {
            String sqlSelect = "";
            String inXML = "";
            DataSet dsEntity = null;
            SqlHelper sqlHelp = null;
            String outXML = "";
            XmlDocument xmlDstDoc = new XmlDocument();
            try
            {

                inXML = "<report>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<configpath>" + Application["MyVRMServer_ConfigPath"].ToString() + "</configpath>";
                inXML += "<languageid>" + HttpContext.Current.Session["languageID"].ToString() + "</languageid>"; //ZD 100288
                inXML += "<starttime></starttime>";
                inXML += "<endtime></endtime>";
                inXML += "<timezone></timezone>";
                inXML += "<roomslist></roomslist>";
                inXML += "<reportType>EC</reportType>";
                inXML += "</report>";
                
                outXML = obj.CallMyVRMServer("ConferenceReports", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlDstDoc.LoadXml(outXML);
                    dsEntity = new DataSet();
                    dsEntity.ReadXml(new XmlNodeReader(xmlDstDoc));
                }

                if (dsEntity != null)
                {
                    if (dsEntity.Tables.Count > 0)
                    {
                        entityList = null;

                        foreach (DataRow row in dsEntity.Tables[0].Rows)
                        {
                            if (entityList == null)
                                entityList = new ArrayList();

                            entityList.Add(row["EntityCode"].ToString());
                        }
                        if (entityList != null)
                            Cache["Entity"] = entityList;
                    }

                    DataTable tblEntity = new DataTable();
                    if (dsEntity.Tables.Count > 0)
                        tblEntity = dsEntity.Tables[0];

                    return tblEntity;
                }

               /*
                sqlHelp = new SqlHelper(Application["MyVRMServer_ConfigPath"].ToString());
                sqlSelect = "GetEntityCodes";
                dsEntity = sqlHelp.ExecuteDataSet(sqlSelect, CommandType.StoredProcedures);
                if (dsEntity != null)
                {
                    if (dsEntity.Tables.Count > 0)
                    {
                        entityList = null;

                        foreach (DataRow row in dsEntity.Tables[0].Rows)
                        {
                            if (entityList == null)
                                entityList = new ArrayList();

                            entityList.Add(row["EntityCode"].ToString());
                        }
                        if (entityList != null)
                            Cache["Entity"] = entityList;
                    }

                    return dsEntity.Tables[0];
                }
               
                return null; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        private void PopulateEntity()
        {
            try
            {
                DataTable dt = GetEntitities();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        drpEntity.DataTextField = "EntityCode";
                        drpEntity.DataValueField = "OptionID";
                        drpEntity.DataSource = dt;
                        drpEntity.DataBind();
                        drpEntity.Items.Insert(0, new ListItem("<"+obj.GetTranslatedText("Select")+">", ""));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        */ 
		//ZD 102909 - End
        #region PopulateTimeZone

        private void PopulateTimeZone()
        {
            String sqlSelect = "";
            DataSet dsTimeZone = null;
            SqlHelper sqlHelp = null;
            try
            {
                sqlHelp = new SqlHelper(Application["MyVRMServer_ConfigPath"].ToString());
                sqlSelect = "GetTimeZoneDisplay";
                dsTimeZone = sqlHelp.ExecuteDataSet(sqlSelect, CommandType.StoredProcedure);
                if (dsTimeZone != null)
                {
                    if (dsTimeZone.Tables.Count > 0)
                    {
                        drpTimeZone.DataTextField = "timezonedisplay";
                        drpTimeZone.DataValueField = "timezoneid";
                        drpTimeZone.DataSource = dsTimeZone.Tables[0];
                        drpTimeZone.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Populate Entity Code Range Methods

        private void PopulateEntityCodeRange()
        {
            Hashtable hashTable = null;
            try
            {
                hashTable = HashEntityCodeRange();

                foreach (String key in hashTable.Keys)
                {
                    String temp = hashTable[key].ToString();
                    drpEntitiyCodeRange.Items.Add(new ListItem(obj.GetTranslatedText(key), hashTable[key].ToString())); // ZD 100288
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region PopulateRooms

        private void PopulateRooms()
        {
            String sqlSelect = "";
            DataSet dsRoom = null;
            SqlHelper sqlHelp = null;

            try
            {
                sqlHelp = new SqlHelper(Application["MyVRMServer_ConfigPath"].ToString());
                sqlSelect = "GetRooms";
                dsRoom = sqlHelp.ExecuteDataSet(sqlSelect, CommandType.StoredProcedure);
                if (dsRoom != null)
                {
                    if (dsRoom.Tables.Count > 0)
                    {
                        foreach (DataRow row in dsRoom.Tables[0].Rows)
                        {
                            if (roomsList == null)
                                roomsList = new Hashtable();

                            roomsList[row["name"].ToString().Trim()] = row["roomid"].ToString();
                            ListItem lstObj = new ListItem(row["name"].ToString().Trim(), row["roomid"].ToString());
                            lstObj.Attributes.Add("onclick", "javascript:fnDeselectAll(this,'" + chkSelectall.ClientID + "');");
                            cblRoom.Items.Add(lstObj);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Button Event Handlers

        #region Button To Export Excel

        protected void ExportExcel(object sender, EventArgs e)
        {
            try
            {
                export = 1;
                if (FetchEntityReports())
                {
                    redURL = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3) + "/UtilityReports/"; //FB 1830 //ZD 101714
                    
                    redURL = redURL + dFile;

                    String respStr = "<script> var swidth=screen.availWidth-20; var sHeight=screen.availHeight-100; window.open('" + redURL + "','Report','menubar=1,resizable=1,toolbar=1,width=' + swidth + ',height='+ sHeight + ',top=0,left=0');</script>";
                    ClientScript.RegisterStartupScript(GetType(), "ReportScript", respStr);
                    //Response.Redirect(redURL);
                }
            }
            //catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                //errlab
                log.Trace("ExportExcel :" + ex.Message);
            }
            finally
            {}
        }

        private bool FetchEntityReports()
        {
            SqlHelper sqlHelper = null;
            String sqlSelect = "";
            String tableName = "";
            DataTable roomTable = null;

            String inXML = "";
            String outXML = "";
            XmlDocument xmlDstDoc = new XmlDocument();
             MemoryStream ms = null;
            StreamReader sr = null;
            DataTable dtEntityCode1 = new DataTable();//ZD 102909
            DataTable dtgetEntity = new DataTable();
            string OptEntityVal = "";
            string prefixtxt = "";
            try
            {
                dateTimeString = DateTime.Now.ToString("yyyyMMddssmmhh");

                //To get the selected roomids in the datatable
                PopulateRoomTable(ref roomTable);
                
                tableName = "T" + dateTimeString;

                inXML = "<report>";
                inXML += "<configpath>" + Application["MyVRMServer_ConfigPath"].ToString() + "</configpath>";
                inXML += "<tableName>" + tableName + "</tableName>";
                inXML += "<reportType>CR</reportType>";
                inXML += "<rooms>";
                
                    DataSet ds1 = new DataSet();
                    roomTable.TableName = "Rooms";
                    ds1.Tables.Add(roomTable);

                    ms = new MemoryStream();
                    ds1.WriteXml(ms);// extract string from MemoryStream
                    ms.Position = 0;
                    sr = new StreamReader(ms, System.Text.Encoding.UTF8);
                    inXML += sr.ReadToEnd();
                    sr.Close();
                    ms.Close();

                inXML += "</rooms>";
                inXML += "</report>";

                outXML = obj.CallMyVRMServer("ConferenceReports", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                
				//ZD 102909 - Start
                if (drpEntitiyCodeRange.SelectedValue == ((Int32)enumEntityCodeRange.ALL).ToString())
                {
                    if (entityList == null)
                    {
                        if (Cache["Entity"] != null)
                            entityList = (ArrayList)Cache["Entity"];
                        else
                        {
                            dtgetEntity = obj.GetEntityCodes1(prefixtxt);

                            if (dtgetEntity != null)
                            {
                                entityList = null;

                                foreach (DataRow row in dtgetEntity.Rows)
                                {
                                    if (entityList == null)
                                        entityList = new ArrayList();

                                    entityList.Add(row[1].ToString());
                                }
                                if (entityList != null)
                                    Cache["Entity"] = entityList;


                                //GetEntitities();
                            }
                        }
						//ZD 102909 - End
                        if (!entityList.Contains("NA"))
                            entityList.Add(obj.GetTranslatedText("NA"));

                        if (!entityList.Contains("ISDN"))
                            entityList.Add(obj.GetTranslatedText("ISDN"));

                        if (!entityList.Contains("ALL"))
                            entityList.Add(obj.GetTranslatedText("ALL"));
                    }
                }
                else if (drpEntitiyCodeRange.SelectedValue == ((Int32)enumEntityCodeRange.Specific).ToString())
                {
                    entityList = null;
                    entityList = new ArrayList();
                    if (txtEntity.Text != "")//ZD 102909
                        entityList.Add(txtEntity.Text);
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("Please enter specific entity code");//FB 1830 - Translation
                        errLabel.Visible = true;
                        return false;
                    }
                }
                else if (drpEntitiyCodeRange.SelectedValue == ((Int32)enumEntityCodeRange.None).ToString())
                {
                    entityList = null;
                    entityList = new ArrayList();
                    entityList.Add(obj.GetTranslatedText("NA"));
                }
                if (entityList != null)
                {
                    DestDirectory = Server.MapPath(".") + "\\UtilityReports";                    
                    dFile = "Savedon" + dateTimeString + ".xls";

                    inXML = "<report>";
                    inXML += obj.OrgXMLElement();
                    inXML += "<languageid>" + HttpContext.Current.Session["languageID"].ToString() + "</languageid>"; //ZD 100288
                    inXML += "<configpath>" + Application["MyVRMServer_ConfigPath"].ToString() + "</configpath>";
					//ZD 100995 start
                    //myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text)
                    inXML += "<starttime>" + myVRMNet.NETFunctions.GetDefaultDate(txtStartDate.Text) + "</starttime>";
                    inXML += "<endtime>" + myVRMNet.NETFunctions.GetDefaultDate(txtEndDate.Text + " 23:59") + "</endtime>"; //FB 2404

                    //inXML += "<starttime>" + Convert.ToDateTime(txtStartDate.Text) + "</starttime>";
                    //inXML += "<endtime>" + Convert.ToDateTime(txtEndDate.Text + " 23:59") + "</endtime>"; //FB 2404
					//ZD 100995 End
                    inXML += "<timezone>" + drpTimeZone.SelectedValue.ToString() + "</timezone>";
                    inXML += "<roomslist></roomslist>";
                    inXML += "<reportType>CD</reportType>";
                    inXML += "<codeType>" + drpEntitiyCodeRange.SelectedValue + "</codeType>";

                    //ZD 102909 - Start                               
                    if (HttpContext.Current.Session["EntityCodesVal"] != null)
                    {
                        if (HttpContext.Current.Session["EntityCodesVal"].ToString() != "")
                        {
                            dtEntityCode1 = (DataTable)HttpContext.Current.Session["EntityCodesVal"];

                            DataRow[] datrow = dtEntityCode1.Select("Caption like '" + txtEntity.Text + "'");

                            if (datrow.Count() > 0)
                                OptEntityVal = datrow[0].ItemArray[0].ToString();
                        }
                    }

                    inXML += "<entityCode>" + OptEntityVal + "</entityCode>";
					//ZD 102909 - End
                    inXML += "<tableName>" + tableName + "</tableName>";
                    inXML += "<export>" + export + "</export>";
                    inXML += "<Destination>" + DestDirectory + "</Destination>";
                    inXML += "<fileName>" + dFile + "</fileName>";
                    inXML += "<DateFormat>" + dtformat + "</DateFormat>";//FB 2588 //ZD 100958
                    inXML += "<TimeFormat>" + tformat + "</TimeFormat>"; 

                    String eList = "";
                    if (entityList != null)
                    {
                        if (entityList.Count > 0)
                        {
                            foreach(String el in entityList)
                            {
                                if (eList == "")
                                    eList = el;
                                else
                                    eList += "," + el;
                            }
                        }
                    }

                    inXML += "<entityList>" + eList + "</entityList>";
                    inXML += "</report>";

                    outXML = obj.CallMyVRMServer("ConferenceReports", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        xmlDstDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDstDoc));
                    }

                    if (ds == null)
                    {
                        dgRptDt.DataSource = null;
                        dgRptDt.DataBind();
                        dgSummary.DataSource = null;
                        dgSummary.DataBind();
                        dgRptDt.Visible = false;
                        dgSummary.Visible = false;
                        lblDetails.Visible = false;
                        lblSummary.Visible = false;
                        tblNoDt.Visible = true;
                        return false;
                    }

                    if (ds.Tables.Count < 2)
                    {
                        dgRptDt.DataSource = null;
                        dgRptDt.DataBind();
                        dgSummary.DataSource = null;
                        dgSummary.DataBind();
                        dgRptDt.Visible = false;
                        dgSummary.Visible = false;
                        lblDetails.Visible = false;
                        lblSummary.Visible = false;
                        tblNoDt.Visible = true;
                        return false;
                    }

                    if (ds.Tables[0].Rows.Count <= 0)
                    {
                        dgRptDt.DataSource = null;
                        dgRptDt.DataBind();
                        dgSummary.DataSource = null;
                        dgSummary.DataBind();
                        dgRptDt.Visible = false;
                        dgSummary.Visible = false;
                        lblDetails.Visible = false;
                        lblSummary.Visible = false;
                        tblNoDt.Visible = true;
                        return false;
                    }

                    if (Session["language"] != null && Session["language"].ToString() != "en") // ZD 100288 
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (ds.Tables[0].Columns["Entity Code"] != null)
                                dr["Entity Code"] = obj.GetTranslatedText(dr["Entity Code"].ToString());
                            if (ds.Tables[0].Columns["Conference Type"] != null)
                                dr["Conference Type"] = obj.GetTranslatedText(dr["Conference Type"].ToString());
                            if (ds.Tables[0].Columns["Protocol"] != null)
                                dr["Protocol"] = obj.GetTranslatedText(dr["Protocol"].ToString());
                            if (ds.Tables[0].Columns["Status"] != null)
                                dr["Status"] = obj.GetTranslatedText(dr["Status"].ToString());
                        }
                    }

                    dgRptDt.DataSource = ds.Tables[0];
                    dgRptDt.DataBind();

                    if (Session["language"] != null && Session["language"].ToString() != "en") // ZD 100288 
                    {
                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            if (ds.Tables[1].Columns["Entity_Code"] != null)
                            {
                                if (dr["Entity_Code"].ToString() == "NA")
                                    dr["Entity_Code"] = obj.GetTranslatedText(dr["Entity_Code"].ToString());
                            }
                        }
                        for (int i = 0; i < ds.Tables[1].Columns.Count; i++)
                        {
                            ds.Tables[1].Columns[i].ColumnName = obj.GetTranslatedText(ds.Tables[1].Columns[i].ColumnName);
                        }
                    }

                    dgSummary.DataSource = ds.Tables[1];
                    dgSummary.DataBind();

                    dgRptDt.Visible = true;
                    dgSummary.Visible = true;
                    lblDetails.Visible = true;
                    lblSummary.Visible = true;
                    tblNoDt.Visible = false;

                }
            }
            catch (Exception ex)
            {
                log.Trace("FetchEntity : " + ex.Message);
            }
            finally
            { }
            return true;
        }

        private string GetConnectionString()
        {
            string _connectionString = "";
            try
            {
                string xml = Application["MyVRMServer_ConfigPath"].ToString() + "app.config.xml";
                
                XmlTextReader textReader = new XmlTextReader(xml);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(textReader);

                XmlNodeList NodeList = xmlDoc.GetElementsByTagName("session-factory");

                foreach (XmlNode xnode in NodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        if (xnod.LocalName == "property")
                        {
                            if (xnod.Attributes["name"].Value == "connection.connection_string")
                            {
                                _connectionString = xnod.InnerText;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return _connectionString;
        }
        #endregion

        //FB 2292 Start
        #region Button To View Report

        protected void ViewReport(object sender, EventArgs e)
        {
            try
            {
                export = 0;
                FetchEntityReports();
            }
            catch (Exception ex)
            {
                //errlab
                log.Trace("ExportExcel :" + ex.Message);
            }
            finally
            { }
        }

       #endregion
        //FB 2292 End

        #endregion

        #region User Defined Methods

        #region Hashtable Methods

        public Hashtable HashEntityCodeRange()
        {
            Hashtable hashTable = null;
            try
            {
                String[] tempEntityCode = Enum.GetNames(typeof(enumEntityCodeRange));

                if (tempEntityCode.Length <= 0)
                    return null;

                hashTable = new Hashtable();

                for (int index = 0; index < tempEntityCode.Length; index++)
                {
                    hashTable.Add(tempEntityCode[index], Convert.ToInt32(Enum.Parse(typeof(enumEntityCodeRange), tempEntityCode[index])));
                }
                return hashTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region PopulateRoomTable

        private void PopulateRoomTable(ref DataTable roomTable)
        {
            DataRow dr = null;
            string rooms = "";
            this.txtRooms.Text = "";

            if (roomTable == null)
                roomTable = new DataTable();

            roomTable.Columns.Add("RoomID", typeof(System.Int32));

            foreach (ListItem item in cblRoom.Items)
            {
                if (item.Selected)
                {
                    dr = roomTable.NewRow();

                    dr["RoomID"] = Convert.ToInt32(item.Value);

                    if (rooms.Length > 0)
                        rooms += ",";

                    rooms += item.Text;

                    roomTable.Rows.Add(dr);

                    dr = null;
                }
            }

            if (rooms != "")
                txtRooms.Text = rooms;
        }

        #endregion

        #region InitializeComponent

        private void InitializeComponent()
        {

        }

        #endregion
        
        #region CreateRoomSqlTable

        private Boolean CreateRoomSqlTable(String tableName)
        {
            Boolean isTableCreated = true;
            String sqlInsert = "";
            SqlHelper sqlHelper = null;
            try
            {
                sqlInsert = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" + tableName + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)";
                sqlInsert += " drop Table [dbo].[" + tableName + "]";
                sqlInsert += " Create Table [dbo].[" + tableName + "]([RoomID] [int] NOT NULL)";

                if (sqlHelper == null)
                    sqlHelper = new SqlHelper(Application["MyVRMServer_ConfigPath"].ToString());

                sqlHelper.OpenConnection();

                sqlHelper.ExecuteNonQuery(sqlInsert);

            }
            catch
            {
                isTableCreated = false;

            }
            finally
            {
                if (sqlHelper != null)
                    sqlHelper.CloseConnection();
            }
            return isTableCreated;
        }

        #endregion

        #region Enum Definiton

        private enum enumEntityCodeRange
        {
            ALL = 0,
            None = 1,
            Specific = 2,
        }
        #endregion


        #endregion

        //ZD 102909 - Start
        #region GetEntityCodes
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string[] GetEntityCodes(string prefixText)
        {
            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
            ns_Logger.Logger log = new ns_Logger.Logger();
            List<string> EntityCodeList = new List<string>();
            DataTable dt = new DataTable();
            ArrayList entityList1 = new ArrayList();
            try
            {
                dt = obj.GetEntityCodes1(prefixText);
                // ZD 103265
                if (prefixText.Length >= 3)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        EntityCodeList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr["Caption"].ToString(), dr["OptionID"].ToString()));

                        entityList1.Add(dr[1].ToString());
                    }
                }

                if ((EntityCodeList == null) || (EntityCodeList != null && EntityCodeList.Count == 0))
                {
                    EntityCodeList.Add(obj.GetTranslatedText("No items found"));
                }
                else
                {
                    HttpContext.Current.Cache["Entity"] = entityList1;
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetEntityCodes" + ex.Message);
            }
            return EntityCodeList.ToArray();
        }
        #endregion

        #region page_unload
        protected void Page_UnLoad(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["EntityCodesVal"] != null)
                    {
                        Session.Remove("EntityCodesVal");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("RemoveEntityCodes" + ex.Message);
            }
        }
        #endregion
        //ZD 102909 - End
    }
}
