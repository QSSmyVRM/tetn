/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

namespace en_ifrmImageList
{
    public partial class ifrmImageList : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        #region Protected Data Members

        protected System.Web.UI.HtmlControls.HtmlInputHidden ROOM_IMAGE_PATH;
        protected System.Web.UI.HtmlControls.HtmlImage imgList;

        #endregion

        #region Private Data Members

        private String rmName = "";
        ns_Logger.Logger log = null;

        #endregion

        #region Page Load EVent Handler

        protected void Page_Load(object sender, EventArgs e)
        {
            log = new ns_Logger.Logger();

            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                BindFrame();
            }
            catch (Exception ex)
            {
                log.Trace("Error on Loading Image List" + ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        #region Bind Data

        private void BindFrame()
        {
            log = new ns_Logger.Logger();
            try
            {
                ROOM_IMAGE_PATH.Value = "image\\room\\";
                Session["appPath"] = Server.MapPath("." + "\\" + ROOM_IMAGE_PATH.Value);
                String appPath = Session["appPath"].ToString();
                DirectoryInfo dfs = new DirectoryInfo(appPath);
                FileInfo[] fIF = dfs.GetFiles();

                foreach (FileInfo fSysInfo in fIF)
                {
                    String str = fSysInfo.Name;
                    rmName = str;
                    //str = dfs.Name + "\\" + str;
                    
                    if (str.Contains(".jpg"))
                    {
                        str = str.Remove(str.LastIndexOf("."));
                        //String rName = rmName.ToString().Replace(".jpg", "");
                        Response.Write("<table border='0' width='145'>");
                        Response.Write("<td align='center'><img width='140' height='105' src='image\\room\\" + str + ".jpg' onclick=javascript:selImg('" + str + "') alt='ImageList' /></td></tr>");
                        Response.Write("<tr><td align='center' style='height:25' valign='top'><span class='srcstext'>" + str + "</span></td></tr>");
                        Response.Write("</table>");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error on Binding Images on Iframe" + ex.StackTrace + " : " + ex.Message);
                throw ex;
            }
        }
        #endregion
    }

}
