﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Google.Apis.Util;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Threading.Tasks;
using System.IO;
using DDay.iCal;
using DDay.iCal.DataTypes;
using Newtonsoft.Json.Linq;
//using Google.Apis.Util.Store;
//using Google.Apis;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Web;
using Google.Apis.Auth.OAuth2;
using System.Net;
using System.Threading;


public partial class GoogleReceiver : System.Web.UI.Page
{

    ns_Logger.Logger _log;
    VRMRTC.VRMRTC obj = null;
    private String RTCConfigPath = @"C:\VRMSchemas_v1.8.3\VRMRTCConfig.xml";
    private myVRMNet.NETFunctions _obj;
    string _inXML = "", _outXML = "", _GoogleClientID = "", _GoogleSecretID = "", _GoogleAPIKey = "", _RefreshToken = "", _AccessToken = "", _ChannelAddress = "", _orgID, _userID;
    int _Pushtogoogle = 0, _Usertzid = 33;

    CalendarService _service = new CalendarService();
    private IAuthorizationState _state;
    private OAuth2Authenticator<WebServerClient> _authenticator;
    static object x = new object();

    private IAuthorizationState AuthState
    {
        get
        {
            return _state ?? HttpContext.Current.Session["AUTH_STATE"] as IAuthorizationState;
        }
    }


    public class HeaderData
    {
        public string key { get; set; }
        public string Value { get; set; }
    }
    public GoogleReceiver()
    {
        _obj = new myVRMNet.NETFunctions();
        _log = new ns_Logger.Logger();
        obj = new VRMRTC.VRMRTC();
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        XmlDocument _Xmldoc = new XmlDocument();
        try
        {
            if (_obj == null)
                _obj = new myVRMNet.NETFunctions();

            List<HeaderData> _HeaderData = new List<HeaderData>();
            try
            {
                for (int _i = 0; _i < Request.Headers.Count; _i++)
                {
                    _HeaderData.Add(new HeaderData { key = Request.Headers.Keys[_i], Value = Request.Headers.GetValues(_i)[0] });
                    _log.Trace("Logging the headers: " + Request.Headers.Keys[_i] + ":" + Request.Headers.GetValues(_i)[0]);
                }

                _log.Trace("Checking google header" + _HeaderData.Where(val => val.key == "X-Goog-Channel-Token").Select(val1 => val1.Value).ToList()[0]);
                _inXML = "<CheckGooglePoll><userid>" + _HeaderData.Where(val => val.key == "X-Goog-Channel-Token").Select(val1 => val1.Value).ToList()[0] + "</userid></CheckGooglePoll>";
                _outXML = obj.Operations(RTCConfigPath, "CheckGooglePoll", _inXML);
                _log.Trace("Outxml " + _outXML);

            }
            catch (Exception exin)
            {

                _log.Trace("ERROR" + exin.Data + " Inner " + exin.InnerException + "Message " + exin.Message + " stack trace " + exin.StackTrace);
            }

            _Xmldoc.LoadXml(_outXML);

            int PollStatus = 0;
            if (_Xmldoc.SelectSingleNode("GetCheckGooglePoll/PollStatus") != null)
                int.TryParse(_Xmldoc.SelectSingleNode("GetCheckGooglePoll/PollStatus").InnerText, out PollStatus);

            if (PollStatus == 0)
            {
                try
                {
                    _userID = _HeaderData.Where(val => val.key == "X-Goog-Channel-Token").Select(val1 => val1.Value).ToList()[0];

                    if (!string.IsNullOrEmpty(_userID))
                    {

                        _inXML = "<GetGoogleChannelDetails><UserID>" + _userID + "</UserID></GetGoogleChannelDetails>";

                        _outXML = _obj.CallMyVRMServer("GetGoogleChannelDetails", _inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                        _log.Trace("OutXML: GetGoogleChannelDetails:" + _outXML);

                        if (_outXML.IndexOf("<error>") >= 0)
                            _log.Trace("Error in GetGoogleChannelExpiredList");

                        _Xmldoc.LoadXml(_outXML);

                        if (_Xmldoc.SelectSingleNode("GoogleInfo/PushtoGoogle") != null)
                            int.TryParse(_Xmldoc.SelectSingleNode("GoogleInfo/PushtoGoogle").InnerText, out _Pushtogoogle);

                        if (_Pushtogoogle == 1)
                        {
                            if (_Xmldoc.SelectSingleNode("GoogleInfo/GoogleClientID") != null)
                                _GoogleClientID = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), _Xmldoc.SelectSingleNode("GoogleInfo/GoogleClientID").InnerText);


                            if (_Xmldoc.SelectSingleNode("GoogleInfo/GoogleSecretID") != null)
                                _GoogleSecretID = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), _Xmldoc.SelectSingleNode("GoogleInfo/GoogleSecretID").InnerText);

                            if (_Xmldoc.SelectSingleNode("GoogleInfo/RefreshToken") != null)
                                _RefreshToken = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), _Xmldoc.SelectSingleNode("GoogleInfo/RefreshToken").InnerText);

                            if (_Xmldoc.SelectSingleNode("GoogleInfo/AccessToken") != null)
                                _AccessToken = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), _Xmldoc.SelectSingleNode("GoogleInfo/AccessToken").InnerText);

                            if (_Xmldoc.SelectSingleNode("GoogleInfo/ChannelAddress") != null)
                                _ChannelAddress = _Xmldoc.SelectSingleNode("GoogleInfo/ChannelAddress").InnerText;

                            if (_Xmldoc.SelectSingleNode("GoogleInfo/OrgID") != null)
                                _orgID = _Xmldoc.SelectSingleNode("GoogleInfo/OrgID").InnerText;

                            if (_Xmldoc.SelectSingleNode("GoogleInfo/usertimezone") != null)
                                int.TryParse(_Xmldoc.SelectSingleNode("GoogleInfo/usertimezone").InnerText, out _Usertzid);

                            var _Data = _HeaderData.Where(val => val.key == "X-Goog-Resource-URI").Select(val1 => val1.Value).ToList()[0];

                            string[] UserEmailarr = _Data.Split('/');

                            GetAuthentication(UserEmailarr[6].ToString());

                            FetchGoogleUserConfList(UserEmailarr[6].ToString(), _userID, _Usertzid);

                            _inXML = "<DeleteCheckGooglePoll><userid>" + _userID + "</userid></DeleteCheckGooglePoll>";
                            _outXML = obj.Operations(RTCConfigPath, "DeleteCheckGooglePoll", _inXML);
                            _log.Trace("outXML : DeleteCheckGooglePoll :" + _outXML);

                        }
                        else
                            _log.Trace("Google is disbled in myVRM");
                    }
                    else
                    {
                        _log.Trace("Push Notification doesnt contains the required details");
                    }
                }
                catch (Exception ex)
                {
                    _log.Trace("Error in create update delete conference in myVrm" + ex.Message);
                    _inXML = "<DeleteCheckGooglePoll><userid>" + _userID + "</userid></DeleteCheckGooglePoll>";
                    _outXML = obj.Operations(RTCConfigPath, "DeleteCheckGooglePoll", _inXML);
                    _log.Trace("outXML : DeleteCheckGooglePoll :" + _outXML);
                }
            }

            _inXML = "<DeleteCheckGooglePoll><userid>" + _userID + "</userid></DeleteCheckGooglePoll>";
            _outXML = obj.Operations(RTCConfigPath, "DeleteCheckGooglePoll", _inXML);
            _log.Trace("outXML : DeleteCheckGooglePoll :" + _outXML);


        }
        catch (Exception exout)
        {

            _log.Trace("ERROR" + exout.Data + " Inner " + exout.InnerException + "Message " + exout.Message + " stack trace " + exout.StackTrace);
        }

    }

    #region FetchGoogleUserConfList
    /// <summary>
    /// FetchGoogleUserConfList
    /// </summary>
    /// <param name="UserEmail"></param>
    private void FetchGoogleUserConfList(string UserEmail, string _userID, int _Usertzid)
    {
        DateTime _StartDateTime, _EndDateTime;
        TimeSpan _span;
        string _Confid = "new";
        string _InstanceID = "",_Recur="";
        XmlDocument _xdoc = null;
        string temp = "";
        int _Endtype = 0;
        DateTime _GoogleReceivedDateTime, _GoogleConfLastUpdated;
        bool _AllowEdit = true;
        string ConfRecur = "0";//ZD 101837

        try
        {
            if (Session["ConfRecurLimit"] != null)
                ConfRecur = Session["ConfRecurLimit"].ToString();

            List<Event> _evt = new List<Google.Apis.Calendar.v3.Data.Event>();
            EventsResource.ListRequest _calRequest = _service.Events.List(UserEmail);

            _calRequest.TimeZone = "UTC";
            string s = XmlConvert.ToString(DateTime.Now.AddMinutes(-2));
            _calRequest.UpdatedMin = XmlConvert.ToDateTime(s, XmlDateTimeSerializationMode.Utc); //To fetch the last 2 mins updated events from google user calendar  

            Events response = _calRequest.Execute();         
                        
            _log.Trace("After requesting for conferences.");
            for (int _i = 0; _i < response.Items.Count; _i++)
            {
                try
                {
                    _span = new TimeSpan();
                    _Confid = "new";
                    _InstanceID = "";
                    temp = "";
                    _Endtype = 0;
                    _xdoc = new XmlDocument();
                    _inXML = "<GetGoogleConference><userid>" + _userID + "</userid><GoogleGUID>" + response.Items[_i].Id + "</GoogleGUID></GetGoogleConference>";
                    _outXML = obj.Operations(RTCConfigPath, "GetGoogleConference", _inXML);
                    _log.Trace("GetGoogleConference OutXML:" + _outXML);

                    if (_outXML.IndexOf("<error>") >= 0)
                    {
                        _log.Trace("Error in Getting the GetGoogleConference");
                        continue;
                    }
                    else
                    {
                        _xdoc.LoadXml(_outXML);
                        if (_xdoc.SelectSingleNode("GetGoogleConference/confid") != null)
                            _Confid = _xdoc.SelectSingleNode("GetGoogleConference/confid").InnerText;
                        if (_xdoc.SelectSingleNode("GetGoogleConference/InstanceID") != null)
                            _InstanceID = _xdoc.SelectSingleNode("GetGoogleConference/InstanceID").InnerText;
                        if (_xdoc.SelectSingleNode("GetGoogleConference/Recurring") != null)
                            _Recur = _xdoc.SelectSingleNode("GetGoogleConference/Recurring").InnerText;
                        if (_xdoc.SelectSingleNode("GetGoogleConference/GoogleConfLastUpdated") != null)
                        {
                            DateTime.TryParse(_xdoc.SelectSingleNode("GetGoogleConference/GoogleConfLastUpdated").InnerText, out _GoogleConfLastUpdated);
                            //_GoogleConfLastUpdated = _GoogleConfLastUpdated.ToUniversalTime();
                            DateTime.TryParse(response.Items[_i].Updated.ToString(), out _GoogleReceivedDateTime);
                            _log.Trace("_GoogleReceivedDateTime : " + _GoogleReceivedDateTime.ToString("MM/dd/yyyy hh:mm"));
                            _log.Trace("_GoogleConfLastUpdated : " + _GoogleConfLastUpdated.ToString("MM/dd/yyyy hh:mm"));
                            if (_GoogleReceivedDateTime.ToString("MM/dd/yyyy hh:mm") == _GoogleConfLastUpdated.ToString("MM/dd/yyyy hh:mm"))
                                _AllowEdit = false;
                            else
                                _AllowEdit = true;
                        }
                    }

                    if (response.Items[_i].Status == "cancelled" && _Confid != "new")
                    {
                        _inXML = "<login>";
                        _inXML += "<organizationID>" + _orgID + "</organizationID>";
                        _inXML += "<multisiloOrganizationID>0</multisiloOrganizationID>";
                        _inXML += "<userID>" + _userID + "</userID>";
                        _inXML += "<delconference>";
                        _inXML += "<conference>";
                        if (!response.Items[_i].Id.Contains('_') && _Recur == "1")
                            _inXML += "<confID>" + _Confid + "</confID>";
                        else
                            _inXML += "<confID>" + _Confid + "," + _InstanceID + "</confID>";

                        _inXML += "<reason></reason>";
                        _inXML += "<deny>Delete</deny>";
                        _inXML += "<fromAPI>1</fromAPI>";
                        _inXML += "</conference>";
                        _inXML += "</delconference>";
                        _inXML += "</login>";

                        _outXML = _obj.CallMyVRMServer("DeleteConference", _inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                        _log.Trace("OutXML: DeleteConference:" + _outXML);

                    }
                    else
                        if (response.Items[_i].Status.ToLower() == "confirmed" && _AllowEdit == true)
                        {
                            if (UserEmail.Trim() == response.Items[_i].Organizer.Email.Trim())
                            {
                                if (response.Items[_i].Attendees != null)
                                {
                                    var _RoomList = response.Items[_i].Attendees.Where(EmailC => EmailC.Email.Contains("@resource.calendar.google.com")).Select(Email => Email.Email).ToList();

                                    if (_RoomList.Count > 0)
                                    {

                                        _inXML = "<conference>";
                                        _inXML += "<userEMail>" + UserEmail.Trim() + "</userEMail>";
                                        _inXML += "<organizationID>" + _orgID + "</organizationID>";
                                        _inXML += "<confInfo>";

                                        _log.Trace("Conf Start Time : " + response.Items[_i].Start.DateTime.ToString());

                                        if (string.IsNullOrEmpty(response.Items[_i].Start.DateTime.ToString()) && !(string.IsNullOrEmpty(response.Items[_i].Start.Date)))
                                            _StartDateTime = DateTime.Parse(response.Items[_i].Start.Date);
                                        else
                                            _StartDateTime = DateTime.Parse(response.Items[_i].Start.DateTime.ToString());// Changed for timezone issue

                                        _log.Trace("Conf Start Time Allday : " + _StartDateTime.ToString());
                                       
                                        _obj.changeToGMTTime(_Usertzid, ref _StartDateTime);
                                        _log.Trace("Conf UTC Start Time : " + _StartDateTime.ToString());

                                        if (response.Items[_i].Recurrence != null)
                                        {
                                            _inXML += "<confID>" + _Confid + "</confID>";
                                            _inXML += "<recurring>1</recurring>";
                                            _inXML += "<recurrencePattern>";


                                            #region Recurrence
                                            RecurrencePattern _RecurPattern = new RecurrencePattern(response.Items[_i].Recurrence[0]);
                                            switch (_RecurPattern.Frequency.ToString())
                                            {
                                                case "Daily":
                                                    if (_RecurPattern.Count > 0)
                                                    {
                                                        _Endtype = 2;
                                                        _inXML += "<recurType>1</recurType>";
                                                        _inXML += "<dailyType>1</dailyType>";
                                                        _inXML += "<dayGap>" + _RecurPattern.Interval + "</dayGap>";

                                                    }
                                                    else
                                                        if (_RecurPattern.Until.ToString() != null)
                                                        {
                                                            _Endtype = 3;
                                                            _inXML += "<recurType>1</recurType>";
                                                            _inXML += "<dailyType>1</dailyType>";
                                                            _inXML += "<dayGap>" + _RecurPattern.Interval + "</dayGap>";

                                                        }
                                                        else
                                                        {
                                                            _Endtype = 1;
                                                            _inXML += "<recurType>1</recurType>";
                                                            _inXML += "<dailyType>1</dailyType>";
                                                            _inXML += "<dayGap>" + _RecurPattern.Interval + "</dayGap>";

                                                        }
                                                    break;

                                                case "Weekly":

                                                    if (_RecurPattern.Count > 0)
                                                    {
                                                        _Endtype = 2;
                                                        _inXML += "<recurType>2</recurType>";
                                                        _inXML += "<weekGap>" + _RecurPattern.Interval + "</weekGap>";

                                                        temp = "";
                                                        for (int _j = 0; _j < _RecurPattern.ByDay.Count; _j++)
                                                        {
                                                            if (_j > 0)
                                                                temp = temp + ",";
                                                            temp = temp + GetDayName(_RecurPattern.ByDay[_j].DayOfWeek.ToString());
                                                        }

                                                        _inXML += "<weekDay>" + temp + "</weekDay>";

                                                    }
                                                    else
                                                        if (_RecurPattern.Until.ToString() != null)
                                                        {
                                                            _Endtype = 3;
                                                            _inXML += "<recurType>2</recurType>";
                                                            _inXML += "<weekGap>" + _RecurPattern.Interval + "</weekGap>";
                                                            temp = "";
                                                            for (int _j = 0; _j < _RecurPattern.ByDay.Count; _j++)
                                                            {
                                                                if (_j > 0)
                                                                    temp = temp + ",";
                                                                temp = temp + GetDayName(_RecurPattern.ByDay[_j].DayOfWeek.ToString());
                                                            }
                                                            _inXML += "<weekDay>" + temp + "</weekDay>";

                                                        }
                                                        else
                                                        {
                                                            _Endtype = 1;
                                                            _inXML += "<recurType>2</recurType>";
                                                            _inXML += "<weekGap>" + _RecurPattern.Interval + "</weekGap>";

                                                            temp = "";
                                                            for (int _j = 0; _j < _RecurPattern.ByDay.Count; _j++)
                                                            {
                                                                if (_j > 0)
                                                                    temp = temp + ",";
                                                                temp = temp + GetDayName(_RecurPattern.ByDay[_j].DayOfWeek.ToString());
                                                            }

                                                            _inXML += "<weekDay>" + temp + "</weekDay>";

                                                        }


                                                    break;
                                                case "Monthly":
                                                    if (_RecurPattern.ByDay.Count == 0)
                                                    {
                                                        if (_RecurPattern.Count > 0)
                                                        {
                                                            _Endtype = 2;
                                                            _inXML += "<recurType>3</recurType>";
                                                            _inXML += " <monthlyType>1</monthlyType>";
                                                            _inXML += "<monthDayNo>" + _StartDateTime.Day + "</monthDayNo>";
                                                            _inXML += "<monthGap>" + _RecurPattern.Interval + "</monthGap>";

                                                        }
                                                        else
                                                            if (_RecurPattern.Until.ToString() != null)
                                                            {
                                                                _Endtype = 3;
                                                                _inXML += "<recurType>3</recurType>";
                                                                _inXML += "<monthlyType>1</monthlyType>";
                                                                _inXML += "<monthDayNo>" + _StartDateTime.Day + "</monthDayNo>";
                                                                _inXML += "<monthGap>" + _RecurPattern.Interval + "</monthGap>";

                                                            }
                                                            else
                                                            {
                                                                _Endtype = 1;
                                                                _inXML += "<recurType>3</recurType>";
                                                                _inXML += " <monthlyType>1</monthlyType>";
                                                                _inXML += "<monthDayNo>" + _StartDateTime.Day + "</monthDayNo>";
                                                                _inXML += "<monthGap>" + _RecurPattern.Interval + "</monthGap>";

                                                            }
                                                    }
                                                    else
                                                    {
                                                        if (_RecurPattern.Count > 0)
                                                        {
                                                            _Endtype = 2;
                                                            _inXML += "<recurType>3</recurType>";
                                                            _inXML += " <monthlyType>2</monthlyType>";
                                                            if (_RecurPattern.ByDay[0].Num != -1)
                                                                _inXML += "<monthWeekDayNo>" + _RecurPattern.ByDay[0].Num + "</monthWeekDayNo>";
                                                            else
                                                                _inXML += "<monthWeekDayNo>5</monthWeekDayNo>";
                                                            _inXML += "<monthWeekDay>" + _RecurPattern.ByDay[0].Num + 3 + "</monthWeekDay>";
                                                            _inXML += "<monthGap>" + _RecurPattern.Interval + "</monthGap>";

                                                        }
                                                        else
                                                            if (_RecurPattern.Until.ToString() != null)
                                                            {
                                                                _Endtype = 3;
                                                                _inXML += "<recurType>3</recurType>";
                                                                _inXML += " <monthlyType>2</monthlyType>";
                                                                if (_RecurPattern.ByDay[0].Num != -1)
                                                                    _inXML += "<monthWeekDayNo>" + _RecurPattern.ByDay[0].Num + "</monthWeekDayNo>";
                                                                else
                                                                    _inXML += "<monthWeekDayNo>5</monthWeekDayNo>";
                                                                _inXML += "<monthWeekDay>" + _RecurPattern.ByDay[0].Num + 3 + "</monthWeekDay>";
                                                                _inXML += "<monthGap>" + _RecurPattern.Interval + "</monthGap>";

                                                            }
                                                            else
                                                            {
                                                                _Endtype = 2;
                                                                _inXML += "<recurType>3</recurType>";
                                                                _inXML += " <monthlyType>2</monthlyType>";
                                                                if (_RecurPattern.ByDay[0].Num != -1)
                                                                    _inXML += "<monthWeekDayNo>" + _RecurPattern.ByDay[0].Num + "</monthWeekDayNo>";
                                                                else
                                                                    _inXML += "<monthWeekDayNo>5</monthWeekDayNo>";
                                                                _inXML += "<monthWeekDay>" + _RecurPattern.ByDay[0].Num + 3 + "</monthWeekDay>";
                                                                _inXML += "<monthGap>" + _RecurPattern.Interval + "</monthGap>";

                                                            }
                                                    }
                                                    break;
                                                case "Yearly":
                                                    if (_RecurPattern.ByDay.Count == 0)
                                                    {
                                                        if (_RecurPattern.Count > 0)
                                                        {
                                                            _Endtype = 2;
                                                            _inXML += "<recurType>4</recurType>";
                                                            _inXML += "<yearlyType>1</yearlyType>";
                                                            _inXML += "<yearMonth>" + _StartDateTime.Month + "</yearMonth>";
                                                            _inXML += "<yearMonthDay>" + _StartDateTime.Day + "</yearMonthDay>";

                                                        }
                                                        else
                                                            if (_RecurPattern.Until.ToString() != null)
                                                            {
                                                                _Endtype = 3;
                                                                _inXML += "<recurType>4</recurType>";
                                                                _inXML += "<yearlyType>1</yearlyType>";
                                                                _inXML += "<yearMonth>" + _StartDateTime.Month + "</yearMonth>";
                                                                _inXML += "<yearMonthDay>" + _StartDateTime.Day + "</yearMonthDay>";

                                                            }
                                                            else
                                                            {
                                                                _Endtype = 1;
                                                                _inXML += "<recurType>4</recurType>";
                                                                _inXML += "<yearlyType>1</yearlyType>";
                                                                _inXML += "<yearMonth>" + _StartDateTime.Month + "</yearMonth>";
                                                                _inXML += "<yearMonthDay>" + _StartDateTime.Day + "</yearMonthDay>";
                                                            }
                                                    }

                                                    break;


                                            }
                                            switch (_Endtype)
                                            {
                                                case 1:
                                                    _inXML += "<recurrenceRange>";
                                                    _inXML += "<startDate>" + _StartDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                                                    _inXML += "<endType>2</endType>";
                                                    //_inXML += "<occurrence>" + HttpContext.Current.Application["confRecurrence"].ToString() + "</occurrence>";
                                                    _inXML += "<occurrence>" + ConfRecur + "</occurrence>";//ZD 101837
                                                    _inXML += "</recurrenceRange>";
                                                    break;
                                                case 2:
                                                    _inXML += "<recurrenceRange>";
                                                    _inXML += "<startDate>" + _StartDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                                                    _inXML += "<endType>2</endType>";
                                                    _inXML += "<occurrence>" + _RecurPattern.Count + "</occurrence>";
                                                    _inXML += "</recurrenceRange>";
                                                    break;
                                                case 3:
                                                    _inXML += "<recurrenceRange>";
                                                    _inXML += "<startDate>" + _StartDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                                                    _inXML += "<endType>3</endType>";
                                                    _inXML += "<endDate>" + _RecurPattern.Until.Date.ToShortDateString() + "</endDate>";
                                                    _inXML += "</recurrenceRange>";
                                                    break;
                                            }


                                            _inXML += "</recurrencePattern>";
                                            #endregion
                                        }
                                        else
                                            if (_Confid == "new")
                                                _inXML += "<confID>" + _Confid + "</confID>";
                                            else
                                                _inXML += "<confID>" + _Confid + "," + _InstanceID + "</confID>";
                                        _inXML += "<confName>" + response.Items[_i].Summary.Trim() + "</confName>";
                                        _inXML += "<confHostEmail>" + UserEmail.Trim() + "</confHostEmail>";
                                        _inXML += "<confOrigin>15</confOrigin>";

                                        if (response.Items[_i].Recurrence != null)
                                            _inXML += "<appointmentTime>";

                                        _inXML += "<startDate>" + _StartDateTime.ToString("MM/dd/yyyy") + "</startDate>";
                                        _inXML += "<startHour>" + _StartDateTime.ToString("hh") + "</startHour>";
                                        _inXML += "<startMin>" + _StartDateTime.ToString("mm") + "</startMin>";
                                        _inXML += "<startSet>" + _StartDateTime.ToString("tt") + "</startSet>";
                                        _inXML += "<timeZone>33</timeZone>";
                                        _inXML += "<setupDuration></setupDuration>";


                                        if (string.IsNullOrEmpty(response.Items[_i].Start.DateTime.ToString()) && !(string.IsNullOrEmpty(response.Items[_i].Start.Date)))
                                            _EndDateTime = DateTime.Parse(response.Items[_i].End.Date).AddMinutes(-1);
                                        else
                                            _EndDateTime = DateTime.Parse(response.Items[_i].End.DateTime.ToString());// Changed for timezone issue

                                        _log.Trace("Conf End Time Allday : " + _EndDateTime.ToString());
                                        
                                        _obj.changeToGMTTime(_Usertzid, ref _EndDateTime);

                                        _span = _EndDateTime - _StartDateTime;
                                        _inXML += "<durationMin>" + (int)Math.Round(_span.TotalMinutes) + "</durationMin>";

                                        if (response.Items[_i].Recurrence != null)
                                            _inXML += "</appointmentTime>";

                                        _inXML += "<createBy>7</createBy>";

                                        _inXML += "<description>" + response.Items[_i].Description + "</description>";
                                        if (response.Items[_i].Id.Contains('_'))
                                            _inXML += "<GoogleGUID>" + response.Items[_i].Id.Split('_')[0] + "</GoogleGUID>";
                                        else
                                            _inXML += "<GoogleGUID>" + response.Items[_i].Id + "</GoogleGUID>";
                                        _inXML += "<GoogleSequence>" + response.Items[_i].Sequence + "</GoogleSequence>";

                                        if (_Confid == "new")
                                            _inXML += "<GoogleConfLastUpdated>" + response.Items[_i].Created.ToString() + "</GoogleConfLastUpdated>";
                                        else
                                            _inXML += "<GoogleConfLastUpdated>" + response.Items[_i].Updated.ToString() + "</GoogleConfLastUpdated>";
                                        _inXML += "<locationList>";

                                        for (int _Roomcnt = 0; _Roomcnt < _RoomList.Count; _Roomcnt++)
                                        {
                                            _inXML += "<selected>";
                                            _inXML += "<roomQueue>" + _RoomList[_Roomcnt] + "</roomQueue>";
                                            _inXML += "</selected>";
                                        }
                                        _inXML += "</locationList>";


                                        _inXML += "<partys>";

                                        var _PartyList = response.Items[_i].Attendees.Where(EmailC => !EmailC.Email.Contains("@resource.calendar.google.com")).Select(Email => Email).ToList();
                                        for (int _PartyCnt = 0; _PartyCnt < _PartyList.Count; _PartyCnt++)
                                        {
                                            _inXML += "<party>";
                                            _inXML += "<partyID>new</partyID>";
                                            if (!string.IsNullOrEmpty(_PartyList[_PartyCnt].DisplayName))
                                            {
                                                _inXML += "<partyFirstName>" + _PartyList[_PartyCnt].DisplayName.Trim() + "</partyFirstName>";
                                                _inXML += "<partyLastName>" + _PartyList[_PartyCnt].DisplayName.Trim() + "</partyLastName>";
                                            }
                                            else
                                            {
                                                _inXML += "<partyFirstName>" + _PartyList[_PartyCnt].Email.Trim().Split('@')[0].ToString() + "</partyFirstName>";
                                                _inXML += "<partyLastName>Guest</partyLastName>";
                                            }


                                            _inXML += "<partyEmail>" + _PartyList[_PartyCnt].Email.Trim() + "</partyEmail>";
                                            _inXML += "<partyInvite>2</partyInvite>";
                                            _inXML += "<partyNotify>1</partyNotify>";
                                            _inXML += "<partyAudVid>1</partyAudVid>";
                                            _inXML += "<notifyOnEdit>1</notifyOnEdit>";
                                            _inXML += "</party>";
                                        }

                                        _inXML += "</partys>";
                                        _inXML += "</confInfo>";
                                        _inXML += "</conference>";

                                        _outXML = _obj.CallMyVRMServer("SetConferenceDetails", _inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                                        _log.Trace("OutXML: SetConferenceDetails:" + _outXML);

                                    }
                                }
                            }
                        }
                }
                catch (Exception ex)
                {

                    _log.Trace("Error in creating updating conference in myVRM from google" + ex.Message);
                    _log.Trace("Error in creating updating conference in myVRM from google Stack" + ex.StackTrace);
                    _log.Trace("Error in creating updating conference in myVRM from google inner" + ex.InnerException);
                    _log.Trace("Error in creating updating conference in myVRM from google Data" + ex.Data);
                    continue;
                }

            }

        }
        catch (Exception ex)
        {

            _log.Trace("Error in fetch google user conference List" + ex.Message);
            _log.Trace("Error in creating updating conference in myVRM from google Stack" + ex.StackTrace);
            _log.Trace("Error in creating updating conference in myVRM from google inner" + ex.InnerException);
            _log.Trace("Error in creating updating conference in myVRM from google Data" + ex.Data);
        }
    }
    #endregion

    #region GetAuthentication
    /// <summary>
    /// GetAuthentication
    /// </summary>
    private void GetAuthentication(string useremail)
    {
        try
        
        {
            ClientSecrets secrets = new ClientSecrets
            {
                ClientId = _GoogleClientID,
                ClientSecret = _GoogleSecretID
            };

            var token = new TokenResponse { RefreshToken = _RefreshToken }; 
            var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                new GoogleAuthorizationCodeFlow.Initializer 
                {
                    ClientSecrets = secrets
                }), useremail, token);
               credentials.RefreshTokenAsync(CancellationToken.None);
               _log.Trace("Refresh Token for user : " + useremail);
            _log.Trace("Refresh Token before is : " + _RefreshToken);
            _log.Trace("Access Token is : "+  credentials.Token.AccessToken);
            _log.Trace("Scope Token is : " + credentials.Token.Scope);
            _log.Trace("Refresh Token is : " + credentials.Token.RefreshToken);
            
            //var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description)
            //{
            //    ClientIdentifier = _GoogleClientID,
            //    ClientSecret = _GoogleSecretID
            //};
            //var auth = new OAuth2Authenticator<NativeApplicationClient>(secrets, GetAuthorization);

            //auth.LoadAccessToken();
            //_service = new CalendarService(new BaseClientService.Initializer()
            //{
            //    //Authenticator = auth,
            //    ApplicationName = "myVRM Google Calendar Intergration",

            //});

            _service  = new CalendarService(new BaseClientService.Initializer
                    {
                        ApplicationName = "myVRM Calendar Sync",
                        HttpClientInitializer = credentials
                        
                    });
        }
        catch (System.Threading.ThreadAbortException ex)
        {
            _log.Trace("Thread abort GetAuthentication Block:" + ex.Message);
        }
        catch (System.Exception exp)
        {
            _log.Trace("GetAuthentication Block:" + exp.Message);
        }
    }

    #endregion

    #region GetAuthorization
    /// <summary>
    /// GetAuthorization
    /// </summary>
    /// <param name="arg"></param>
    /// <returns></returns>
    private IAuthorizationState GetAuthorization(NativeApplicationClient arg)
    {
        try
        {
            // Get the auth URL:
            IAuthorizationState state = new AuthorizationState(new[] { "https://www.googleapis.com/auth/userinfo.email" });
            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
            Uri authUri = arg.RequestUserAuthorization(state);

            state.RefreshToken = _RefreshToken;
            var tokenRefreshed = arg.RefreshToken(state);
            if (tokenRefreshed)
                return state;
            else
                throw new ApplicationException("Unable to refresh the token.");

        }
        catch (WebException wex)
        {
            string error = "";
            if (wex.Response != null)
            {
                using (var errorResponse = (HttpWebResponse)wex.Response)
                {
                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                    {

                        _log.Trace(error = reader.ReadToEnd());
                    }
                }
            }


            return null;
        }
        catch (Exception ex)
        {
            _log.Trace("Unable to refresh the token:" + ex.Message);
            return null;

        }


    }
    #endregion

    #region Int for GetDayName
    private int GetDayName(String day)
    {
        int dayName = 0;
        switch (day)
        {
            case "Sunday":
                dayName = 1;
                break;
            case "Monday":
                dayName = 2;
                break;
            case "Tuesday":
                dayName = 3;
                break;
            case "Wednesday":
                dayName = 4;
                break;
            case "Thursday":
                dayName = 5;
                break;
            case "Friday":
                dayName = 6;
                break;
            case "Saturday":
                dayName = 7;
                break;
        }
        return dayName;
    }
    #endregion






}
