/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Xml;


/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_MeetingPlanner
{
    public partial class MeetingPlanner : System.Web.UI.Page
    {
       
        #region protected data members

        protected System.Web.UI.WebControls.Table MeetingPlannerTable;
        protected System.Web.UI.WebControls.RadioButton OfficeHours;
        protected System.Web.UI.WebControls.RadioButton ShowAll;
        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "";
        protected String timeZone = "0";
        String tformat = "hh:mm tt";
        //Code added by Offshore for FB Issue 1073 -- End
        #endregion  
      

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;


        #region Private Data Members

        private String roomID = "";
        private DateTime confDateTime = DateTime.MinValue;
        private DateTime confDate = DateTime.MinValue;
        private DateTime confTime = DateTime.MinValue;
        private Int32 confTimeZone = Int32.MinValue;
        private String confDate1 = "";
        private DataSet resultSet = null;
        private DataTable resultTable = null;
        private DataTable mainTable = null;
        private DataTable mainSearchTable = null;


        #endregion

        public MeetingPlanner()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();


        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            // InitializeComponent();
            ////base.OnInit(e);

        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {


        }

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("MeetingPlanner.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                //Code added by Offshore for FB Issue 1073 -- End
                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                if (Session["timeFormat"].ToString() == "2") //FB 2588
                    tformat = "HHmmZ";
                /* **** Code added for FB 1425 *** */
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }
                if (Session["userID"] == null)
                    Session.Add("userID", "11");

                if (Request.QueryString["ConferenceTimeZone"] != null)
                {
                    if (Request.QueryString["ConferenceTimeZone"] != "")
                    {
                        confTimeZone = Convert.ToInt32(Request.QueryString["ConferenceTimeZone"].ToString());
                    }

                }
                //else
                //{
                //    confTimeZone = 26;
                //}
                string Date, Time; //FB 2588
                if (Request.QueryString["ConferenceDateTime"] != null)
                {
                    if (Request.QueryString["ConferenceDateTime"].ToString() != "")
                    {
                        confDate1 = Request.QueryString["ConferenceDateTime"].ToString();

                        Date = confDate1.Split(' ')[0]; //FB 2588
                        Time = confDate1.Split(' ')[1];
                        //code added for FB Issue 1073
                        confDate1 = myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time);
                    }
                }
                //else
                //{
                //    confDate1 = "7-11-2008 11:00:00 PM";
                //}

                if (Request.QueryString["RoomID"] != null)
                {
                    if (Request.QueryString["RoomID"].ToString() != "")
                    {
                        roomID = Request.QueryString["RoomID"].ToString();
                    }
                }
                //else
                //{
                //    roomID = "75,76,";
                //}

                if (!IsPostBack)
                {
                    // ShowConfDiv.Attributes.Add("style", "display:none");
                    OfficeHours.Checked = true;
                    this.BindDataFromSearch(null, null);

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

        }

        protected void BindDataFromSearch(object sender, EventArgs e)
        {

            DataSet dset = null;
            DataSet dSearchSet = null;
            String roomName = "";
            String roomTimeZoneName = "";
            String confTimeZoneName = "";
            ArrayList roomlist = null;
            DateTime systemStartTime = DateTime.MinValue;
            DateTime systemEndTime = DateTime.MinValue;
            String conferenceDate = "";
            DateTime conferenceEndDate = DateTime.MinValue;
            Double timeDiff = Double.MinValue;
            Double minDiff = Double.MinValue;

            try
            {
                confDate = Convert.ToDateTime(confDate1);
                confTime = Convert.ToDateTime(confDate1);
                conferenceDate = confDate.ToShortDateString();
                conferenceEndDate = Convert.ToDateTime(conferenceDate);
                conferenceEndDate = conferenceEndDate.AddDays(1);

                // DateTime confdate1 = Convert.ToDateTime(confDate);

                DateTime conftime = confTime.AddHours(-12); //binding thevalues in table
                DateTime confStartTime = confDate.AddHours(-12); //for input xml
                DateTime conftime1 = confDate.AddHours(24);


                XmlDocument xmldocInXML = new XmlDocument();
                Int32 i = 0;


                String[] roomspilt = null;

                if (roomID.IndexOf(',') > 0)
                {
                    roomspilt = roomID.Split(',');
                }
                else
                {
                    roomID = roomID + "," + ",";
                    roomspilt = roomID.Split(',');
                }
                //Data table Main table for bind the values in colunm wise(dublicate of main asp table)
                mainTable = new DataTable();
                mainTable.Columns.Add("ConferenceDate", typeof(System.String));
                mainTable.Columns.Add("ConferenceTimeZone", typeof(System.String));
                mainTable.Columns.Add("Status", typeof(System.String));
                mainTable.Columns.Add("DivConfDetailsMain", typeof(System.String));

                for (Int32 j = 0; j < roomspilt.Length - 1; j++)
                {

                    mainTable.Columns.Add("Room" + j, typeof(System.String));
                }
                //This table is for get the values from outxml for all the rooms
                resultTable = new DataTable();
                resultTable.Columns.Add("Date", typeof(System.String));
                resultTable.Columns.Add("ConferenceDate", typeof(System.String));
                resultTable.Columns.Add("OfficeDate", typeof(System.String));
                resultTable.Columns.Add("OffDateTime", typeof(System.String));
                resultTable.Columns.Add("IsOfficeHour", typeof(System.Int32));
                resultTable.Columns.Add("RoomName", typeof(System.String));
                resultTable.Columns.Add("TableName", typeof(System.String));
                resultTable.Columns.Add("Status", typeof(System.String));
                resultTable.Columns.Add("ConferenceTime", typeof(System.String));
                resultTable.Columns.Add("OfficeStatus", typeof(System.String));
                resultTable.Columns.Add("DivConfDetailsResult", typeof(System.String));

                //Data Table for Get the values for the exisiting conferences
                mainSearchTable = new DataTable();
                mainSearchTable.Columns.Add("ConferenceDate", typeof(System.String));
                mainSearchTable.Columns.Add("ConferenceName", typeof(System.String));
                mainSearchTable.Columns.Add("ConfEndDate", typeof(System.String));
                mainSearchTable.Columns.Add("ConferenceDuration", typeof(System.String));
                mainSearchTable.Columns.Add("ConferenceDateTime", typeof(System.String));
                mainSearchTable.Columns.Add("ConferenceEndDateTime", typeof(System.String)); //ALLBUGS-55

                ArrayList BaseTimeZoneList = new ArrayList();
                ArrayList BaseDateTimeList = new ArrayList();
                ArrayList headingTable = new ArrayList();
                headingTable.Insert(0, obj.GetTranslatedText("Conference Date"));//FB 2272

                for (i = 0; i < roomspilt.Length - 1; i++)
                {
                    roomName = "";
                    timeDiff = Double.MinValue;
                    minDiff = Double.MinValue;
                    mainSearchTable.Rows.Clear(); //ALLBUGS-55

                    String inXML = "<GetRoomTimeZoneMapping>" + obj.OrgXMLElement() + "<RoomID>" + roomspilt[i] + "</RoomID><BaseTimeZoneID>" + confTimeZone + "</BaseTimeZoneID><StartDateTime>" + confStartTime + "</StartDateTime></GetRoomTimeZoneMapping>";//Organization Module Fixes
                    log.Trace("GetRoomTimeZoneMapping" + inXML);

                    String outXML = obj.CallMyVRMServer("GetRoomTimeZoneMapping", inXML, Application["MyVRMServer_ConfigPath"].ToString());


                    log.Trace("GetRoomTimeZoneMapping OutXML: " + outXML);
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        roomName = xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/RoomName").InnerText.ToString();
                        confTimeZoneName = xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/ConfTimeZoneName").InnerText.ToString();
                        roomTimeZoneName = xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/RoomTimeZoneName").InnerText.ToString();
                        systemStartTime = Convert.ToDateTime(xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/SystemStartTime").InnerText.ToString());
                        systemEndTime = Convert.ToDateTime(xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/SystemEndTime").InnerText.ToString());

                        timeDiff = Convert.ToDouble(xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/HourDifference").InnerText.ToString());
                        minDiff = Convert.ToDouble(xmldoc.SelectSingleNode("//GetRoomTimeZoneMapping/MinDifference").InnerText.ToString());


                        if (i == 0)
                            headingTable.Insert(1, obj.GetTranslatedText("Conference Time Zone") + "<br>[" + confTimeZoneName + "]"); //104066

                        headingTable.Insert(i + 2, roomName + "<br>[" + roomTimeZoneName + "]");


                        // if its office hours calculate the Office times 
                        if (i == 0)
                        {
                            if (OfficeHours.Checked == true)
                            {
                                for (int p = 0; systemStartTime <= systemEndTime; p++)
                                {
                                    BaseTimeZoneList.Insert(p, systemStartTime.ToShortTimeString());
                                    BaseDateTimeList.Insert(p, systemStartTime.ToShortDateString());
                                    systemStartTime = systemStartTime.AddHours(1);
                                }
                            }
                            else
                            {
                                for (int p = 0; p < 24; p++)
                                {
                                    BaseTimeZoneList.Insert(p, conftime.ToShortTimeString());
                                    BaseDateTimeList.Insert(p, conftime.ToShortDateString());
                                    conftime = conftime.AddHours(1);
                                }

                            }
                        }


                        dSearchSet = new DataSet();
                        ArrayList confEndDateList = new ArrayList();
                        //FB 2274 Starts
                        string ORGID = "11";
                        ORGID = Session["organizationID"].ToString();
                        if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
                        {
                            if (Session["multisiloOrganizationID"].ToString() != Session["organizationID"].ToString())
                            {
                                ORGID = Session["multisiloOrganizationID"].ToString();
                            }
                        }
                        //FB 2274 Ends

                        String inXML1 = "<SearchConference><organizationID>" + ORGID + "</organizationID><UserID>" + Session["userID"] + "</UserID><ConferenceID></ConferenceID><ConferenceUniqueID></ConferenceUniqueID>";//Organization Module Fixes //FB 2274
                        inXML1 += "<StatusFilter> <ConferenceStatus>1</ConferenceStatus>";
                        inXML1 += "<ConferenceStatus>1</ConferenceStatus>";
                        inXML1 += "<ConferenceStatus>0</ConferenceStatus>";
                        inXML1 += "<ConferenceStatus>5</ConferenceStatus>";
                        inXML1 += "<ConferenceStatus>6</ConferenceStatus>";
                        inXML1 += "<ConferenceStatus>7</ConferenceStatus></StatusFilter><ConferenceName></ConferenceName>";
                        inXML1 += "<ConferenceSearchType>5</ConferenceSearchType><DateFrom>" + conferenceDate + "</DateFrom><DateTo>" + conferenceEndDate + "</DateTo>";
                        inXML1 += "<ConferenceHost></ConferenceHost><ConferenceParticipant></ConferenceParticipant><Public></Public>";
                        inXML1 += "<RecurrenceStyle>0</RecurrenceStyle><Location><SelectionType>2</SelectionType><Selected>" + roomspilt[i] + "</Selected>";
                        inXML1 += "</Location><FilterType>" + ns_MyVRMNet.vrmSearchFilterType.custom + "</FilterType><PageNo>0</PageNo><SortBy>3</SortBy></SearchConference>"; //ZD 100933

                        String outXML1 = obj.CallMyVRMServer("SearchConference", inXML1, Application["MyVRMServer_ConfigPath"].ToString());
                        XmlDocument xmldoc1 = new XmlDocument();
                        xmldoc1.LoadXml(outXML1);
                        //  Response.Write(outXML1);
                        XmlNodeList searchNodes = xmldoc1.SelectNodes("//SearchConference/Conferences/Conference");


                        if (searchNodes.Count > 0)
                        {
                            XmlTextReader xtr;

                            foreach (XmlNode node in searchNodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dSearchSet.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            DataTable xmlSearchConfTable = dSearchSet.Tables[0];

                            if (xmlSearchConfTable.Rows.Count > 0) //search conference out xml
                            {
                                foreach (DataRow row in xmlSearchConfTable.Rows)
                                {
                                    DataRow mainrow = mainSearchTable.NewRow();
                                    mainrow["ConferenceDate"] = Convert.ToDateTime(row["ConferenceDateTime"]).ToString("MM/dd/yyyy hh:00 tt"); //ALLBUGS-55
                                    mainrow["ConferenceName"] = row["ConferenceName"].ToString().Replace(",","+").Replace("\"","+"); //FB 2321
                                    mainrow["ConferenceDuration"] = Convert.ToDouble(row["ConferenceDuration"].ToString());
                                    mainrow["ConferenceDateTime"] = Convert.ToDateTime(row["ConferenceDateTime"]).ToShortTimeString();

                                    //DateTime starttime = Convert.ToDateTime(mainrow["ConferenceDate"]);
                                    String startdate = Convert.ToDateTime(row["ConferenceDateTime"]).ToString("MM/dd/yyyy hh:00 tt"); //ALLBUGS-55
                                    DateTime endTime = Convert.ToDateTime(row["ConferenceDateTime"]);
                                    double confMin = Convert.ToDouble(row["ConferenceDuration"].ToString());

                                    endTime = endTime.AddMinutes(confMin);
                                    //ALLBUGS-55 Start
                                    mainrow["ConferenceEndDateTime"] = endTime.ToShortTimeString();
                                    int mints = endTime.Minute;
                                    if (mints > 0)
                                        endTime = endTime.AddHours(1);

                                    String enddate = endTime.ToString("MM/dd/yyyy hh:00 tt");
                                    mainrow["ConfEndDate"] = endTime.ToString("MM/dd/yyyy hh:00 tt"); 
                                    //ALLBUGS-55 End
                                    DateTime endDate = Convert.ToDateTime(enddate);
                                    DateTime stratDate = Convert.ToDateTime(startdate);

                                    if (confMin > 60)
                                    {
                                        for (int n = 0; stratDate < endDate; n++)
                                        {
                                            confEndDateList.Insert(n, stratDate.ToString("MM/dd/yyyy hh:mm tt"));
                                            stratDate = stratDate.AddHours(1);
                                        }
                                    }
                                  
                                    mainSearchTable.Rows.Add(mainrow);
                                }
                            }
                        }

                        XmlNodeList nodes = xmldoc.SelectNodes("//GetRoomTimeZoneMapping/Cells/Cell");
                        dset = new DataSet();
                        String confName = "";
                        String confOffName = "";
                        String offTime = "";
                        DateTime offTime1 = DateTime.MinValue;
                        DataRow[] filterSearchRow = null;
                        DataRow[] filterOfficeRow = null;
                        DataRow[] filterOfficeDiffMinRow = null;
                        if (nodes.Count > 0) //GetRoomTimeZoneMapping out xml
                        {
                            XmlTextReader xtr;

                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dset.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            DataTable xmlResultTable = dset.Tables[0];


                            foreach (DataRow row in xmlResultTable.Rows)
                            {
                                DataRow dr = resultTable.NewRow();

                                DateTime stime = Convert.ToDateTime(row["StartTime"]);
                                stime = stime.AddHours(timeDiff); //conference time difference
                                if (minDiff != 0)
                                    stime = stime.AddMinutes(minDiff); //conference Miniute difference

                                String showConfTime = stime.ToString("MM/dd/yyyy hh:mm tt");


                                if (mainSearchTable.Rows.Count > 0) //filter is for match the conference for the date
                                {
                                    filterSearchRow = mainSearchTable.Select("ConferenceDate <= #" + showConfTime + "# AND ConfEndDate > #" + showConfTime + "#"); //ALLBUGS-55
                                }
                                if (filterSearchRow != null && filterSearchRow.Length > 0)
                                {

                                    if (confName != filterSearchRow[0]["ConferenceName"].ToString().Trim())
                                    {
                                        dr["Date"] = filterSearchRow[0]["ConferenceName"].ToString();
                                        dr["DivConfDetailsResult"] = filterSearchRow[0]["ConferenceName"].ToString() + "," + filterSearchRow[0]["ConferenceDuration"].ToString()
                                                                + "," + filterSearchRow[0]["ConferenceDateTime"].ToString() + " - " + filterSearchRow[0]["ConferenceEndDateTime"].ToString() + "," + roomName; //ALLBUGS-55

                                    }
                                    else
                                    {
                                        dr["Date"] = "Conf";
                                    }

                                    dr["Status"] = 4;
                                    confName = filterSearchRow[0]["ConferenceName"].ToString().Trim();

                                }

                                else if (confEndDateList != null && confEndDateList.Count > 0)
                                {
                                    if (confEndDateList.Contains(showConfTime))
                                    {
                                        dr["Date"] = "Conf";
                                        dr["Status"] = 4;
                                    }
                                    else
                                    {
                                        dr["Date"] = Convert.ToDateTime(row["StartTime"]).ToString("ddd  "+ tformat);
                                        dr["Status"] = row["Status"].ToString();
                                    }
                                }
                                else
                                {
                                    dr["Date"] = Convert.ToDateTime(row["StartTime"]).ToString("ddd  " + tformat);
                                    dr["Status"] = row["Status"].ToString();
                                }
                                if (OfficeHours.Checked)
                                {
                                    if (row["OffStartTime"].ToString() != "")
                                    {
                                        DateTime offSTime = Convert.ToDateTime(row["OffStartTime"]);
                                       
                                        offSTime = offSTime.AddHours(timeDiff);
                                        if (minDiff != 0)
                                            offSTime = offSTime.AddMinutes(minDiff);
                                        offTime1 = offSTime;

                                        offTime = offSTime.ToString("MM/dd/yyyy "+ tformat);

                                        offTime1 = offTime1.AddMinutes(-30);
                                        String offTime2 = offTime1.ToString("MM/dd/yyyy " + tformat);


                                        if (mainSearchTable.Rows.Count > 0 && offTime != "")
                                        {
                                            filterOfficeRow = mainSearchTable.Select("ConferenceDate <= #" + offTime + "# AND ConfEndDate > #" + offTime + "#"); //ALLBUGS-55
                                        }

                                        if (filterOfficeRow != null && filterOfficeRow.Length > 0)
                                        {
                                            if (confOffName != filterOfficeRow[0]["ConferenceName"].ToString().Trim())
                                            {
                                                dr["OffDateTime"] = filterOfficeRow[0]["ConferenceName"].ToString();
                                                dr["DivConfDetailsResult"] = filterOfficeRow[0]["ConferenceName"].ToString() + "," + filterOfficeRow[0]["ConferenceDuration"].ToString()
                                                                 + "," + filterOfficeRow[0]["ConferenceDateTime"].ToString() + " - " + filterOfficeRow[0]["ConferenceEndDateTime"].ToString() + "," + roomName; //ALLBUGS-55

                                            }
                                            else
                                                dr["OffDateTime"] = "Conf";

                                            dr["OfficeStatus"] = 4;
                                            confOffName = filterOfficeRow[0]["ConferenceName"].ToString().Trim();

                                        }
                                        /* //ALLBUGS-55 Start
                                         else if (filterOfficeDiffMinRow != null && filterOfficeDiffMinRow.Length > 0)
                                         {
                                             if (confOffName != filterOfficeDiffMinRow[0]["ConferenceName"].ToString())
                                             {
                                                 dr["OffDateTime"] = filterOfficeDiffMinRow[0]["ConferenceName"].ToString();
                                                 dr["DivConfDetailsResult"] = filterOfficeDiffMinRow[0]["ConferenceName"].ToString() + "," + filterOfficeDiffMinRow[0]["ConferenceDuration"].ToString()
                                                                  + "," + filterOfficeDiffMinRow[0]["ConferenceDateTime"].ToString() + "," + roomName;

                                             }
                                             else
                                                 dr["OffDateTime"] = "Conf";

                                             dr["OfficeStatus"] = 4;
                                             confOffName = filterOfficeDiffMinRow[0]["ConferenceName"].ToString();
                                         }
                                         */ //ALLBUGS-55 End
                                        else if (confEndDateList != null && confEndDateList.Count > 0)
                                        {
                                            if (confEndDateList.Contains(offTime) || confEndDateList.Contains(offTime2))
                                            {
                                                dr["OffDateTime"] = "Conf";
                                                dr["OfficeStatus"] = 4;
                                            }
                                            else
                                            {
                                                dr["OffDateTime"] = Convert.ToDateTime(row["OffStartTime"]).ToString("ddd  " + tformat);
                                                dr["OfficeStatus"] = row["OffStatus"].ToString();
                                            }
                                        }

                                        else
                                        {
                                            dr["OffDateTime"] = Convert.ToDateTime(row["OffStartTime"]).ToString("ddd  " + tformat);
                                            dr["OfficeStatus"] = row["OffStatus"].ToString();
                                        }

                                    }
                                }


                                dr["ConferenceTime"] = Convert.ToDateTime(row["ConferenceTimeFrom"]).ToShortTimeString();
                                dr["IsOfficeHour"] = row["IsOfficeHour"].ToString();
                                dr["TableName"] = "Table" + i;

                                resultTable.Rows.Add(dr);
                            }
                        }

                    }
                }


                TableHeaderCell headerCell = null;
                TableRow tRow = null;
                TableCell tCell = null;
                tRow = new TableRow();

                //Bind the Datatable Main Table  from Output xml table[this method is for assign the values in colunm wise]
                for (Int32 c = 0; c < BaseTimeZoneList.Count; c++)
                {
                    DataRow dr = mainTable.NewRow();

                    for (Int32 n = 0; n < roomspilt.Length - 1; n++)
                    {
                        tRow.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#9999ff");
                        DataRow[] filterRow = null;

                        if (OfficeHours.Checked)
                        {
                            filterRow = resultTable.Select("IsOfficeHour=1 and TableName='Table" + n + "'");
                           
                            if (filterRow.Length > 0)
                            {
                                if (filterRow[c]["DivConfDetailsResult"] != "")
                                    dr["Room" + n] = filterRow[c]["OffDateTime"] + "," + filterRow[c]["OfficeStatus"] + "," + filterRow[c]["DivConfDetailsResult"].ToString();
                                else
                                    dr["Room" + n] = filterRow[c]["OffDateTime"] + "," + filterRow[c]["OfficeStatus"];
                            }
                        }
                        else
                        {
                            filterRow = resultTable.Select("TableName='Table" + n + "'");
                            if (filterRow.Length > 0)
                            {
                                if (filterRow[c]["DivConfDetailsResult"] != "")
                                    dr["Room" + n] = filterRow[c]["Date"] + "," + filterRow[c]["Status"] + "," + filterRow[c]["DivConfDetailsResult"].ToString();

                                else
                                    dr["Room" + n] = filterRow[c]["Date"] + "," + filterRow[c]["Status"];
                            }

                        }
                        dr["ConferenceTimeZone"] = Convert.ToDateTime(BaseTimeZoneList[c]).ToString(tformat);
                        //Code changed for FB Issue 1073
                        //dr["ConferenceDate"] = Convert.ToDateTime(BaseDateTimeList[c]).ToString("dddd,MM/dd/yyyy");
                        //ZD 100288 Start
                        string date = Convert.ToDateTime(BaseDateTimeList[c]).ToString("dddd");
                        date = obj.GetTranslatedText(date);
                        dr["ConferenceDate"] = date + "," + myVRMNet.NETFunctions.GetFormattedDate(Convert.ToDateTime(BaseDateTimeList[c]).ToString()); //ZD 100995
                        //ZD 100288 End
                    }
                    mainTable.Rows.Add(dr);
                }
                //Main Table  Meeting Planner Header Binding

                for (Int32 h = 0; h < headingTable.Count; h++)
                {

                    headerCell = new TableHeaderCell();

                    headerCell.Text = headingTable[h].ToString();
                    tRow.Cells.Add(headerCell);

                }
                MeetingPlannerTable.Rows.Add(tRow);



                String dateCol = "";
                String[] spiltConfDetails = null;
                String divConfName = "";
                String divConfTime = "";
                String divConfLoc = "";
                String divConfDur = "";


                //ASP Table Meeting Planner Rows Binding 
                foreach (DataRow row in mainTable.Rows)
                {

                    tRow = new TableRow();

                    headerCell = new TableHeaderCell();

                    foreach (DataColumn dc in mainTable.Columns)
                    {
                        tCell = new TableCell();

                        if (dc.ColumnName == "Status")
                            continue;
                        if (dc.ColumnName == "DivConfDetailsMain")
                            continue;


                        if (dc.ColumnName != "ConferenceTimeZone" && dc.ColumnName != "ConferenceDate")
                        {
                            String[] str = null;
                            if (row[dc.ColumnName].ToString().IndexOf(',') > 0)
                            {
                                str = row[dc.ColumnName].ToString().Split(',');

                                if (str[0] != "Conf")
                                    tCell.Text = str[0];
                                tCell.Text=tCell.Text.Replace("+", ",").Replace("+","\""); //FB 2321 + - alt985
                                tCell.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                                tCell.Style.Add(HtmlTextWriterStyle.Width, "3%");

                                switch (str[1])
                                {
                                    case "2"://more suitable
                                        tCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#7CCD7C");
                                        break;
                                    case "1": //not suitable
                                        tCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ccccff");
                                        break;
                                    case "3": //passable
                                        tCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#7CCD7C");
                                        break;
                                    case "4": //Conference
                                        tCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#3300ff");

                                        if (str.Length > 5)
                                        {
                                            divConfName = str[2];
                                            divConfDur = str[3];
                                            divConfTime = str[4];
                                            divConfLoc = str[5];
                                            tCell.Style.Add(HtmlTextWriterStyle.Cursor, "hand");
                                            //tCell.Attributes.Add("onMouseOver", "javascript:mouseoverdiv(\"" + divConfName + "\",\"" + divConfTime + "\",\"" + divConfDur + "\",\"" + divConfLoc + "\")");
                                            tCell.Attributes.Add("onMouseOver", "javascript:mouseoverdiv('" + divConfName + "','" + divConfTime + "','" + divConfDur + "','" + divConfLoc + "')"); //ZD 100933
                                            tCell.Attributes.Add("onmousemove", "javascript:mousemovediv()");
                                            tCell.Attributes.Add("onmouseout", "javascript:mouseoutdiv()");

                                        }
                                        break;
                                }
                            }
                        }
                        else
                        {
                            if (dc.ColumnName == "ConferenceDate")
                            {
                                if (dateCol != row[dc.ColumnName].ToString())
                                    tCell.Text = row[dc.ColumnName].ToString();

                                dateCol = row[dc.ColumnName].ToString();

                                tCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ccccff");
                                tCell.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                                tCell.Style.Add(HtmlTextWriterStyle.Width, "1%");
                            }
                            else
                            {
                                tCell.Text = row[dc.ColumnName].ToString();
                                tCell.Style.Add(HtmlTextWriterStyle.TextAlign, "Center");
                                tCell.Style.Add(HtmlTextWriterStyle.Width, "2%");
                                tCell.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ccccff");
                            }
                        }

                        tRow.Cells.Add(tCell);

                    }
                    MeetingPlannerTable.Rows.Add(tRow);
                }

            }
            catch (Exception ex)
            {
                throw ex;
                log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
            }
        }

    }
}
