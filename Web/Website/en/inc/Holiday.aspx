<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147  ZD 100886 End-->
<script type="text/javascript">

    var holidayString = '<%=Session["HolidaysString"]%>';
    var cssString = '<%=Session["HolidaysCSSString"]%>';
    
    // FB 2050 Starts
    function addCss(cssCode)
    {
    var styleElement = document.createElement("style");
    styleElement.type = "text/css";
        if (styleElement.styleSheet)
        {
        styleElement.styleSheet.cssText = cssCode;
        }
        else
        {
        styleElement.appendChild(document.createTextNode(cssCode));
        }
    document.getElementsByTagName("head")[0].appendChild(styleElement);
    }    
    // FB 2050 Ends
    
    if(cssString != "")
    {
        addCss(cssString); // FB 2050
        /* Commented for FB 2050
        var isCalBlue = 0;
        for(var h=0;h < document.styleSheets.length;h++)
        {
            if(document.styleSheets[h].href.indexOf('calendar-blue.css') > 0)
            {
                isCalBlue = 1;
                break;
            }
        }

        if(isCalBlue == 0)
            h = h - 1;
         
        if(h > 0)
        { 
          document.styleSheets[h].cssText = document.styleSheets[h].cssText +  cssString; 
        }
        */
    }
    
</script>