//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100866 End
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Text;
using myVRM;

namespace ns_HolidayImport
{
    public partial class HolidayImport : System.Web.UI.Page
    {
        String schemaPath = "C:\\VRMSchemas_V1.8.3\\";
        static DataTable dtMaster;
        DataView dvZone;
        DataTable dtZone;
        myVRMNet.NETFunctions obj;
        DataSet dsUsers = new DataSet();
        DataView dvUsers;
        DataTable dtUsers = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("HolidayImport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

        }       
        protected void GenerateDataTable(Object sender, EventArgs e)
        {
            try
            {
                dtMaster = CreateDataTableFromCSV();
                if (dtMaster != null)
                    errLabel.Text = "Data Table has been generated successfully!";

                ImportConferences(dtMaster);
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportConferences(DataTable dtMaster)
        {
            String outxml = "";
            StringBuilder orgDt = new StringBuilder();
            String errMsg = "";
            try
            {

                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                if (dtMaster != null)
                {

                    orgDt.Append("<SetOrgHolidays>");
                    orgDt.Append(obj.OrgXMLElement());
                    orgDt.Append("<SystemHolidays>");
                    if (dtMaster.Rows.Count > 0)
                    {
                        orgDt.Append("<Holidays>");

                        for (int dts = 0; dts < dtMaster.Rows.Count; dts++)
                        {
                            DataRow dr = dtMaster.Rows[dts];
                            orgDt.Append("<Holiday>");
                            orgDt.Append("<Date>" + dr[0].ToString() + "</Date>");
                            orgDt.Append("<HolidayType>" + dr[2].ToString() + "</HolidayType>");
                            orgDt.Append("</Holiday>");
                        }
                        orgDt.Append("</Holidays>");
                    }
                    orgDt.Append("</SystemHolidays>");
                    orgDt.Append("</SetOrgHolidays>");

                    outxml = obj.CallCommand("SetOrgHolidays", orgDt.ToString());

                    if (outxml.IndexOf("<error>") > 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outxml);
                        errLabel.Visible = true;
                    }
                }

                

               
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected DataTable CreateDataTableFromCSV()
        {
            StreamReader sr = null;
            try
            {
                
                DataTable dt = new DataTable();
                String ExternalFileName = fleMasterCSV.PostedFile.FileName;
                GenerateTableSchema(ref dt);
                sr = File.OpenText(ExternalFileName);
                string strRecord = sr.ReadLine();
                string[] strTemp = strRecord.Split(',');
                int len = strTemp.Length;
                while (sr.Peek() != -1)
                {
                    strRecord = sr.ReadLine();
                    DataRow dr = dt.NewRow();
                    strTemp = strRecord.Split(',');
                    for (int i = 0; i < len; i++)
                    {
                        dr[i] = strTemp[i].ToString().Trim();
                        dr[i] = dr[i].ToString().Replace("'", "");
                        dr[i] = dr[i].ToString().Replace("\"", "");
                        dr[i] = dr[i].ToString().Replace("&", "&amp;");
                        dr[i] = dr[i].ToString().Replace("(", "");
                        dr[i] = dr[i].ToString().Replace(")", "");
                        dr[i] = dr[i].ToString().Replace("<", "");
                        dr[i] = dr[i].ToString().Replace(">", "");
                        dr[i] = dr[i].ToString().Replace("%", "");
                    }
                    

                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;
            }
        }
        protected void GenerateTableSchema(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Date")) dt.Columns.Add("Date");
                if (!dt.Columns.Contains("HolidayType")) dt.Columns.Add("HolidayType");
                if (!dt.Columns.Contains("EntityType")) dt.Columns.Add("EntityType");
                if (!dt.Columns.Contains("EntityID")) dt.Columns.Add("EntityID");

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }

    }
    
}