<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_ConferenceSetup.ConferenceSetup" %><%--ZD 100170--%>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--fb 2779 Ends--%>

<style type="text/css">
    input[readonly]{
  color:black !important;
  cursor: default;  
}
#TopMenu div 
{
	width:100px; /* FB 2050 ZD 100393*/
	height:35px;
}

#divPCdetails div
{	
    position:relative;
    overflow:hidden;
}

a img { outline:none;
    text-decoration:none;
    border:0;
}
#ImgbtnAddress
{
	vertical-align:top
}
#imgAddressList
{
	vertical-align:middle
}

/* ZD 102909 - Start */
.completionList 
{
    border:solid 1px Gray;
    margin:0px;
    padding:3px;
    height: 120px;
    overflow:auto;
    background-color: #FFFFFF;     
} 
        
.listItem 
{
    color: #191919;
} 
        
.itemHighlighted
{
    background-color: #ADD6FF;       
}
        
.loadingImg
{
    background-image: url(image/wait1.gif);
    background-position: right;
    background-repeat: no-repeat;
}   
/* ZD 102909 - End */
</style>

<script type="text/javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
var isIE = false; //ZD 100420
    if (navigator.userAgent.indexOf('Trident') > -1)
        isIE = true;
  var servertoday = new Date();

  //ZD 102909 - Start

    function OnClientPopulating(sender, e) 
    {       
        //sender._element.className = "loadingImg"; //ZD 103265
    }

    function OnClientCompleted(sender, e) 
    {           
        sender._element.className = "";
    }   

    function validate(par) 
    {
        if(par != null)
            document.getElementById('hdnSelEntityCodeVal').value = par.value;

        //ZD 103265
        if(par.value == "")            
            document.getElementById("hdnRemoveEntityVal").value = "";                            
        else
            document.getElementById("hdnRemoveEntityVal").value = par.value;
    }
    //ZD 102909 - End
  
//  Merging Recurrence - start
  var dFormat;
    dFormat = "<%=format %>";
  
  var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10)-1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10)); 
 
  var  maxDuration = 24; //Buffer zone
  if( "<%=Application["MaxConferenceDurationInHours"] %>" != "")
     maxDuration = parseInt("<%=Application["MaxConferenceDurationInHours"] %>",10); 
//  Merging Recurrence - end

    //FB 2426 Start
    
    //FB 2486
    function toggle() 
    {
	    var ele = document.getElementById("toggleText");
	    var text = document.getElementById("displayText");
	    if(ele.style.display == "block")
	    {
    		ele.style.display = "none";
		    text.innerHTML = "<asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_displayText%>" runat="server"></asp:Literal>";//ZD 101344 
  	    }
	    else 
	    {
		    ele.style.display = "block";
		    text.innerHTML = "<asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_less%>" runat="server"></asp:Literal>"; //ZD 101344 
	    }
	    
	    document.getElementById('hdnSaveData').value = ""; //ZD 100875
    } 
    
    //FB 3055-Filter in Upload Files Starts
    
    function checkTipoFileInput(fileTypes) {

    var fileUpload1 = document.getElementById("FileUpload1");
    var fileUpload2 = document.getElementById("FileUpload2");
    var fileUpload3 = document.getElementById("FileUpload3");
    var retError = false;
    
    if ((fileUpload1 != null && ( fileUpload1.value == "" || fileUpload1.value.length < 3)) && (fileUpload2 != null && ( fileUpload2.value == "" || fileUpload2.value.length < 3)) && (fileUpload3 != null && (fileUpload3.value == "" || fileUpload3.value.length < 3)) )
        return false;

    if( fileUpload1 != null && fileUpload1.value != "")
    {
        dots = fileUpload1.value.split(".");
        fileType = "." + dots[dots.length - 1];

        if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1) 
            retError = true;
    }
    if( fileUpload2 != null && fileUpload2.value != "")
    {
        dots = fileUpload2.value.split(".");
        fileType = "." + dots[dots.length - 1];
        if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1) 
            retError = true;
    }
    if( fileUpload3 != null && fileUpload3.value != "")
    {
        dots = fileUpload3.value.split(".");
        fileType = "." + dots[dots.length - 1];
        if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1) 
            retError = true;
    }
    if(retError)
    {
        alert(RFileTypeValid);
        return false;
    }
  }
    //FB 3055-Filter in Upload Files - End
	
    function ExpandAll() {

        var bool = "0";
        if (document.getElementById("ImgbtnAddress").src.indexOf('nolines_minus.gif') > -1)
            bool = "0";
        else
            bool = "1";

        if (bool == "1") {
            document.getElementById('tblAddress').style.visibility = 'visible';
            document.getElementById("ImgbtnAddress").src = "../image/loc/nolines_minus.gif";
        }
        else if (bool == "0") {
            document.getElementById('tblAddress').style.visibility = 'hidden';
            document.getElementById("ImgbtnAddress").src = "../image/loc/nolines_plus.gif";
        }
        document.getElementById("expColStat").value = bool;
        return false;

    }

    function fnSetExpColStatus() {
        if (document.getElementById("expColStat") != null) {
            if (document.getElementById("expColStat")) {
                if (document.getElementById("expColStat").value == "1") {
                    document.getElementById("ImgbtnAddress").src = "../image/loc/nolines_plus.gif";
                    ExpandAll();
                }
            }
        }
    }

    
    function ValidateflyEndpoints()
	{
	  var ret = true; var ConnectionType =2; var MCU =8, AddressType =0, Address=0;
      var ErrorTxt="";
      var CheckIP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
      //var CheckISDN =/^[0-9]+$/;   /^[0-9]+$/;

      var CheckISDN = /^\d+/

	  ValidatorEnable(document.getElementById("reqRoomName"), true);
      ValidatorEnable(document.getElementById("regRoomName"), true);
      ValidatorEnable(document.getElementById("reqcontactName"), true);   
      ValidatorEnable(document.getElementById("reqcontactEmail"), true); 
      var dgProfileCount = document.getElementById('dgProfiles').rows.length;
      for(var i=2; i<= dgProfileCount; i++)
      {
        if(document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1"))
            ConnectionType = document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex;
       if(document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType"))
            MCU = document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType").selectedIndex;
             
        ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), false);
        if(ConnectionType == 1)
        {
            if(MCU ==8)
                ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true);
        }
        else
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true); 


            
        if(document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType"))
            AddressType = document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType").selectedIndex;
       if(document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress"))
            Address = document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress").value;
       if(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"))
            ErrorTxt = document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress").value;

           ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), true); 
           document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress").enabled = true;
            if(AddressType == 1)
            {
                if(CheckIP.test(Address))
                    ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false); 
            }else  if(AddressType == 4)
            {
                    CheckISDN = isFinite(Address);
                    if(CheckISDN)
                        ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false); 
            }
            else
                ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false); 

       }     
        if (!Page_ClientValidate())
               return Page_IsValid;
                  
	  return ret;
	}
	 function  fnClearGuestGrid()
	{
        document.getElementById("txtsiteName").value = "";
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
        document.getElementById("txtPhone").value = "";
        document.getElementById("txtContactPhone").value = "";

        document.getElementById("txtAddress").value = "";
        document.getElementById("txtState").value = "";
        document.getElementById("txtCity").value = "";
        document.getElementById("txtZipcode").value = "";
        document.getElementById("lstCountries").value = -1;//ZD 100484
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;
        for(var i=2; i<= dgProfileCount; i++)
        {
              document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress").value = "";
//            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex = 2; 
//            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType").selectedIndex = 1; 
//            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestBridges").selectedIndex = 0;  
        }
       
        document.getElementById("btnGuestLocationSubmit").value = "<asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGuestLocationSubmit%>" runat="server"></asp:Literal>"; //ZD 101344
        document.getElementById("hdnGuestRoom").value = "-1";
    }

      	
	function deleteAssistant()
    {
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
    }
    
    function GusetLocClose()
    {
    
        document.getElementById("txtsiteName").value = "";
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
        document.getElementById("txtPhone").value = "";
        document.getElementById("txtContactPhone").value = "";

        document.getElementById("txtAddress").value = "";
        document.getElementById("txtState").value = "";
        document.getElementById("txtCity").value = "";
        document.getElementById("txtZipcode").value = "";
        document.getElementById("lstCountries").value = -1;//ZD 100484

        
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;
        if(dgProfileCount > 2)
        {
            for(var i=2; i< dgProfileCount; i++)
            {
                //document.getElementById('dgProfiles').rows(i+1)="";
                document.getElementById('dgProfiles').deleteRow(2);
            }
        }
        dgProfileCount = document.getElementById('dgProfiles').rows.length;
        for(var i=2; i<= dgProfileCount; i++)
        {
            document.getElementById("dgProfiles_ctl0" + i + "_txtGuestAddress").value = "";
            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex = 2; 
            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestAddressType").selectedIndex = 1; 
            document.getElementById("dgProfiles_ctl0" + i + "_lstGuestBridges").selectedIndex = 0;  
        }
        document.getElementById("btnGuestLocationSubmit").value = "<asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGuestLocationSubmit%>" runat="server"></asp:Literal>"; //ZD 101344
        document.getElementById("hdnGuestRoom").value = "-1";
    }
    
    function fnValidator()
    {
        fnClearGuestGrid(); 
        document.getElementById("expColStat").value =0;
        var ConnectionType =2,MCU=8; 
        var VMRType= "0"; //ZD 100522
        document.getElementById("reqRoomName").enabled = "true";
        document.getElementById("regRoomName").enabled = "true";
        document.getElementById("reqcontactName").enabled = "true"; 
        document.getElementById("reqcontactEmail").enabled = "true"; 
        //ZD 100522 Starts
        if(document.getElementById("lstVMR")!= null)
        {
            var e =document.getElementById("lstVMR");
            VMRType =  e.options[e.selectedIndex].value;
        }
        //ZD 100522 Ends

        var dgProfileCount = document.getElementById('dgProfiles').rows.length;
        for(var i=2; i<= dgProfileCount; i++)
        {

             if(document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1"))
                ConnectionType = document.getElementById("dgProfiles_ctl0" + i + "_lstGuestConnectionType1").selectedIndex;
             if(document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType"))
                MCU = document.getElementById("dgProfiles_ctl0" + i + "_lstBridgeType").selectedIndex;
             
            
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestConnectionType").enabled = "true"; 
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddressType").enabled = "true"; 
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestLineRate").enabled = "true"; 
            document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress").enabled = "false";  

            if(ConnectionType == 1)
            {
                if(MCU ==8)
                    ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true);
            }
            //else
                //ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), true); 
            //ZD 100522 Starts
            document.getElementById("dgProfiles_ctl0" + i + "lstGuestBridges").enabled="false";
            if(VMRType =="2")
                document.getElementById("dgProfiles_ctl0" + i + "lstGuestBridges").enabled="true";
            //ZD 100522  Ends
        }

    }

    function fnDisableValidator()
    {
    
        
        ValidatorEnable(document.getElementById("reqRoomName"), false);
        ValidatorEnable(document.getElementById("regRoomName"), false);
        ValidatorEnable(document.getElementById("reqcontactName"), false); 
        ValidatorEnable(document.getElementById("reqcontactEmail"), false); 
        ValidatorEnable(document.getElementById("regTestemail"), false);
        ValidatorEnable(document.getElementById("regcontactEmail"), false);
        ValidatorEnable(document.getElementById("regcontactEmail"), false); 
        ValidatorEnable(document.getElementById("reqAddress2"), false);
        ValidatorEnable(document.getElementById("reqState"), false);
        ValidatorEnable(document.getElementById("reqCity"), false); 
        ValidatorEnable(document.getElementById("reqZipcode"), false); 
        ValidatorEnable(document.getElementById("reqPhone"), false); 
        var dgProfileCount = document.getElementById('dgProfiles').rows.length;

        for(var i=2; i<= dgProfileCount; i++)
        {
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), false);            
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_regIPAddress"), false);   
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestConnectionType"), false);   
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestLineRate"), false);   
            ValidatorEnable(document.getElementById("dgProfiles_ctl0" + i + "_reqGuestAddress"), false);   
            
        }    
        //GusetLocClose();    
//        __doPostBack('btnUpdtGrid',''); //ALLBUGS-111
        document.getElementById("btnGuestLocationSubmit").value = "<asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGuestLocationSubmit%>" runat="server"></asp:Literal>"; //ZD 101344
        document.getElementById("hdnGuestloc").value = "";
        document.getElementById("hdnGuestRoomID").value = "";
        document.getElementById("hdnSaveData").value = "1";

    }

    function CheckIPAddress(obj)
    {
        var Address = document.getElementById(obj.id.replace("txtGuestAddress", "lstGuestAddressType"))
        var e =document.getElementById(Address.id);

        var CheckIP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
        
        
        if(CheckIP.test(obj.value))
            e.options[1].selected = true;
         else
           e.options[0].selected = true;
            
    }
    //FB 2426 End
    function SelectDefault(obj)
    {
    
        if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
        {
            var elements = document.getElementsByTagName('input'); 
            var chkDefault = obj.id.substring(0,obj.id.indexOf("rdDefault")) + "chkDelete";
            var objDef = document.getElementById(chkDefault);
            if(objDef.checked)
            {
                alert(ConfDelProf);
                objDef.checked = false;
            }
            for (i=0;i<elements.length;i++)
            if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("Default") >= 0) ) 
            {
                if(elements.item(i).id!=obj.id) 
                {
                    elements.item(i).checked= false; 
                }
            } 
        }
    } 

// FB 2359 Start
   //Dan Disney Requirement for audio addon start
	function fnShowHideAVLink()
	{
	    var args = fnShowHideAVLink.arguments;
	    var obj = eval(document.getElementById("LnkAVExpand"));
	    
	    if(obj)
	    {
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	    }
	}
	
	function fnShowAVParams()
	{
	    var obj = eval(document.getElementById("trAVCommonSettings"));
	    
	    var linkState = eval(document.getElementById("hdnAVParamState"));
	    var expandlink = eval(document.getElementById("LnkAVExpand"));
	    
	    if(linkState)
	    {
	        if(obj)
	        {
	            obj.style.display = 'none';
	            if(linkState.value == '')
	            {
	                obj.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Collapse';
	                }
	            }
	            else
	            {
	                obj.style.display = 'none';
	                
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
	//Dan Disney Requirement for audio addon end
	//FB 2359 End
	
	function fnShowHideMeetLink()
	{
	    var args = fnShowHideMeetLink.arguments;
	    var obj = eval(document.getElementById("LnkMeetExpand"));
	    
	    if(obj)
	    {
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	    }
	}
	
	function fnShowMeetPlanner()
	{
	    var obj = eval(document.getElementById("tdMeetingPlanner"));
	    var linkState = eval(document.getElementById("hdnMeetLinkSt"));
	    var expandlink = eval(document.getElementById("LnkMeetExpand"));
	    
	    if(linkState)
	    {
	        if(obj)
	        {
	            obj.style.display = 'none';
	            if(linkState.value == '')
	            {
	                obj.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Collapse';
	                }
	            }
	            else
	            {
	                obj.style.display = 'none';
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerText = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
// FB 1985 End
    //FB 2501 Starts
    function deleteApprover(id)
    {
	    eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
	    eval("document.getElementById('txtApprover" + (id) + "')").value = "";
    }
    //FB 2501 Ends

    //FB 2659 - Starts
    function fnShowSeats()
    {
        document.getElementById("modalDivPopup").style.display = 'block';
        document.getElementById("modalDivContent").style.display = 'block';
    }

    function fnPopupSeatsClose()
    {
        document.getElementById("modalDivPopup").style.display = 'none';
        document.getElementById("modalDivContent").style.display = 'none';
        return false;
    }
    
    //ZD 100568 21-12-2013 Inncrewin
    function fnShowPopupConfAlert()
    {    
        document.getElementById("modalDivPopup").style.display = 'block';
        document.getElementById("modalDivContentforConf").style.display = 'block';
    }
    
    function fnPopupConfAlertClose()
    {
        document.getElementById("modalDivPopup").style.display = 'none';
        document.getElementById("modalDivContentforConf").style.display = 'none';
        return false;
    }
    //ZD 100568 21-12-2013 Inncrewin
    
    function SelectOneDefault(obj)
    {
        var elements = document.getElementById(obj).innerHTML; 
        var a = elements.toUpperCase().split("<BR>");//Split the dateTime and assign. Start Date/Time: 04/30/2013 08:00<br>End Date/Time: 04/30/2013 09:00<br>Available Seats: 4
        var c = a[0];
        var b = c.split(": "); 
        var strStartDate = b[1];
        
        c = a[1];
        b = c.split(": ");
        var strEndDate = b[1];
        //ZD 100995 start
        if("<%=Session["EmailDateFormat"].ToString()%>" != null &&  "<%=Session["EmailDateFormat"].ToString()%>" == '1')
         {   
            var monthNames =["JAN", "FEB", "MAR", "APR", "MAY", "JUN","JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
            var month=["01","02","03","04","05","06","07","08","09","10","11","12"]
            
            var confStartDate = strStartDate.split(" ")[0] + "/" + month[monthNames.indexOf(strStartDate.split(" ")[1])] + "/" +strStartDate.split(" ")[2];
            if('<%=Session["FormatDateType"]%>' != null && '<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
                document.getElementById("confStartDate").value = confStartDate;
            else
               document.getElementById("confStartDate").value = GetDefaultDate(confStartDate);
                            
            if(strStartDate.split(" ")[4] != null)
                document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[3] + " " + strStartDate.split(" ")[4];
            else    
                document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[3];
                
            var confEnDate = strEndDate.split(" ")[0] + "/" + month[monthNames.indexOf(strEndDate.split(" ")[1])] + "/" +strEndDate.split(" ")[2];
            if('<%=Session["FormatDateType"]%>' != null && '<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
                document.getElementById("confEndDate").value = confEnDate;
            else
               document.getElementById("confEndDate").value = GetDefaultDate(confEnDate);
            if(strEndDate.split(" ")[4] != null)
                document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[3] + " " + strEndDate.split(" ")[4];
            else    
                document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[3];    
         }
        else //ZD 100995 end
        {
        document.getElementById("confStartDate").value = strStartDate.split(" ")[0];
        if(strStartDate.split(" ")[2] != null)
            document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[1] + " " + strStartDate.split(" ")[2];
        else    
            document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[1];
            
        document.getElementById("confEndDate").value = strEndDate.split(" ")[0];
        if(strEndDate.split(" ")[2] != null)
            document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[1] + " " + strEndDate.split(" ")[2];
        else    
            document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[1];
        }
       fnPopupSeatsClose();     
    }  
    //FB 2659 - End
    //ZD 100221 Start
    function fnShowWebExConf()
    {
        //ZD 101837
        
        var ConfRecurLimit = 50;
        if(document.getElementById('spnRecurLimit') != null)
            ConfRecurLimit = document.getElementById('spnRecurLimit').innerHTML;
        if(document.getElementById("chkWebex") != null)
        {
            if(document.getElementById("chkWebex").checked) 
            {
				//ZD 100221 Starts
                document.getElementById("divWebPW").style.display = "";
                document.getElementById("divWebCPW").style.display = "";
                ValidatorEnable(document.getElementById('RequiredFieldValidator7'), true);
				//ZD 100221 Ends

                //ZD 101837
                if(ConfRecurLimit > 50)
                    document.getElementById('spnRecurLimit').innerHTML = 50;
            }
            else
            {
                if(document.getElementById('spnRecurLimit') != null)
                    document.getElementById('spnRecurLimit').innerHTML = <%=Session["ConfRecurLimit"]%>;
                document.getElementById("divWebPW").style.display = "none";
                document.getElementById("divWebCPW").style.display = "none";
                ValidatorEnable(document.getElementById('RequiredFieldValidator7'), false);//ZD 100221_17Feb 
            }
        }
    }
    
    function PasswordChange(par) 
    {  
        
        document.getElementById("hdnPasschange").value = true;
        
        if (par == 1)
        {
            document.getElementById("hdnPW1Visit").value = true;
            }
        else
            document.getElementById("hdnPW2Visit").value = true;
        document.getElementById("hdnWebExPwd").value = document.getElementById("txtPW1").value; // ZD PRABU
    }
    function fnTextFocus(xid,par) {
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
                document.getElementById("txtPW1").value = "";
                document.getElementById("txtPW2").value="";
            }else
            {
                document.getElementById("txtPW1").value = "";
            }
        }
        else
        {
             document.getElementById("txtPW1").value = "";
             document.getElementById("txtPW2").value="";
        }
    }
       else{
           if(document.getElementById("hdnPW1Visit") != null)
           {
            if(document.getElementById("hdnPW1Visit").value == "false")
            { 
                document.getElementById("txtPW1").value = "";
                document.getElementById("txtPW2").value="";
            }
            else{
                document.getElementById("txtPW2").value = "";
                }
           
           }
           else{
                document.getElementById("txtPW2").value = "";
                }
        }
        
        if(document.getElementById("RequiredFieldValidator7")!= null) //ZD 100152
        {
            ValidatorEnable(document.getElementById('RequiredFieldValidator7'), false);
            ValidatorEnable(document.getElementById('RequiredFieldValidator7'), true);
        }
//         if(document.getElementById("cmpValPassword1")!= null) //ZD 102692
//        {
//            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
//            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
//        }
         if(document.getElementById("reqPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('reqPassword2'), false);
            ValidatorEnable(document.getElementById('reqPassword2'), true);
        }
         if(document.getElementById("cmpValPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword2'), false);
            ValidatorEnable(document.getElementById('cmpValPassword2'), true);
        }
    }
    //ZD 100221 Ends
    //ZD 100522 Starts
    function ChangeRoomVMRPIN()
    {
        document.getElementById("hdnROOMPIN").value = "1";
    
        if(document.getElementById("trVMRPIN1") != null)
            document.getElementById("trVMRPIN1").style.display  = '';
        if(document.getElementById('trVMRPIN2') != null)
            document.getElementById('trVMRPIN2').style.display  = '';
    }
	//ZD 101371 Starts
    function ConfPWDLen()
    {
        var passlen = "4", obj1 ="",obj2="",obj3 = ""; //ZD 102692
    
        if(document.getElementById("hdnPINlen")!= null && document.getElementById("hdnPINlen").value != "")
            passlen = document.getElementById("hdnPINlen").value ;

        obj1 = document.getElementById("ConferencePassword");
        document.getElementById("txtVMRPIN1").value = obj1.value; // 101344 Password issue
        document.getElementById("txtVMRPIN2").value = obj1.value; //101344 Password issue     
        ValidatorEnable(document.getElementById("numPassword1"), false);

        var numbers = /^[0-9]+$/;
        if(!obj1.value.match(numbers)) 
        {
        ValidatorEnable(document.getElementById("numPassword1"), true);
        } 
 
        if( (obj1.value != "" && obj1.value.length < passlen))
        {
            //alert("Passwords should have a minimum of "+ passlen +" characters");
            ValidatorEnable(document.getElementById("numPassword1"), true);
        }

        obj3 = document.getElementById("ConferencePassword2"); //ZD 102692
        if(obj1.value != obj3.value)
        {
         if(document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "inline";
        }

        return  true;
    }
    //ZD 102692 - Start
    function ConfpwdValidation()
    {
        var confPwd1 = "", confPwd2 = "";
        confPwd1 = document.getElementById("ConferencePassword");
        confPwd2 = document.getElementById("ConferencePassword2"); 

        if(confPwd1.value != confPwd2.value)
        {
         if(document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "inline";
        }
    }
    //ZD 102692 - End

    function VMRPWDLen()
    {
        var passlen = "4", obj1 ="",obj2="";

        if(document.getElementById("hdnisBJNConf")!= null && document.getElementById("hdnisBJNConf").value == "1") //ZD 103263
            return true;
    
        if(document.getElementById("hdnPINlen")!= null && document.getElementById("hdnPINlen").value != "")
            passlen = document.getElementById("hdnPINlen").value ;

        obj1 = document.getElementById("txtVMRPIN1");
        document.getElementById("ConferencePassword").value = obj1.value; // 101344 Password issue
        document.getElementById("ConferencePassword2").value = obj1.value; //101344 Password issue       
        ValidatorEnable(document.getElementById("RegVMRPIN"), false);

        var numbers = /^[0-9]+$/;
        if(!obj1.value.match(numbers)) 
        {
        ValidatorEnable(document.getElementById("RegVMRPIN"), true);
        } 
 		//ZD 101371 Ends
        if( (obj1.value != "" && obj1.value.length < passlen))
        {
            //alert("Passwords should have a minimum of "+ passlen +" characters");
            ValidatorEnable(document.getElementById("RegVMRPIN"), true);
//                if(document.getElementById("numPassword1") != null)
//                {
//                    document.getElementById("numPassword1").style.display = "";
//                    document.getElementById("numPassword1") = "<span id=\"numPassword1\" style=\"color: red;\">Minimum Length is " + hdnPINlen.Value + ". First digit should be non-zero.</span>";
//                    }
//            return false;
        }
        return  true;
    }
    //ZD 100522 Ends
    //ALLDEV-826 Starts
    function HostPWDLen()
    {
        var minPwdLength = "4", hostPwd1 ="", hostPwd2="";
    
        if(document.getElementById("hdnHostGuestPwdlen")!= null && document.getElementById("hdnHostGuestPwdlen").value != "")
            minPwdLength = document.getElementById("hdnHostGuestPwdlen").value ;

        hostPwd1 = document.getElementById("txtHostPwd");
        hostPwd2 = document.getElementById("txtCfmHostPwd");
        ValidatorEnable(document.getElementById("regValHostPwd"), false);

        if(document.getElementById("ConferencePassword") != null)
        {
            if (document.getElementById("txtHostPwd") != null) 
            {
                var confPwd = "", reqValHostPwd = "";
                confPwd = document.getElementById("ConferencePassword");
                reqValHostPwd = document.getElementById("reqValHostPwd");

                if (confPwd.value != "" && hostPwd1.value == "") {
                    ValidatorEnable(reqValHostPwd, true);
                }
                else{
                    ValidatorEnable(reqValHostPwd, false);
                }
                if (document.getElementById('lstVMR') != null) {
                    if (document.getElementById('lstVMR').value == "2") {                    
                        ValidatorEnable(reqValHostPwd, false);
                    }
                }
            }
        }

        var numbers = /^[0-9]+$/;
        if(!hostPwd1.value.match(numbers)) 
        {
            ValidatorEnable(document.getElementById("regValHostPwd"), true);
        }

        if( (hostPwd1.value != "" && hostPwd1.value.length < minPwdLength))
        {
            ValidatorEnable(document.getElementById("regValHostPwd"), true);
        }
                
        if(hostPwd1.value != hostPwd2.value)
        {
            if(document.getElementById("cmpValHostPwd") != null)
                document.getElementById("cmpValHostPwd").style.display = "inline";
        }
        return  true;
    }

    function HostPwdValidation()
    {
        var hostPwd1 = document.getElementById("txtHostPwd").value;
        var hostPwd2 = document.getElementById("txtCfmHostPwd").value;
        if(hostPwd1 != hostPwd2)
        {
            if(document.getElementById("cmpValHostPwd") != null)
                document.getElementById("cmpValHostPwd").style.display = "inline";
        }
    }

    function chkRequiredHostPwd()
    {
        if(document.getElementById("ConferencePassword") != null)
        {
            if (document.getElementById("txtHostPwd") != null) 
            {
                var confPwd = "", hostPwd = "", reqValHostPwd = "";
                confPwd = document.getElementById("ConferencePassword");
                hostPwd = document.getElementById("txtHostPwd");
                reqValHostPwd = document.getElementById("reqValHostPwd");

                if (confPwd.value != "" && hostPwd.value == "") {
                    ValidatorEnable(reqValHostPwd, true);
                }
                else
                {
                    ValidatorEnable(reqValHostPwd, false);
                }
                if (document.getElementById('lstVMR') != null) {
                    if (document.getElementById('lstVMR').value == "2") {                    
                        ValidatorEnable(reqValHostPwd, false);
                    }
                }
            }
        }
    }

    function host_gen()
    {
        var num = randam();
        if (num.indexOf(0) == 0) 
            num = num.replace(0,1);
                  
        if(document.getElementById("cmpValHostPwd") != null)
            document.getElementById("cmpValHostPwd").style.display = "none";
        if(document.getElementById("regValHostPwd") != null)
            document.getElementById("regValHostPwd").style.display = "none";

        var hostPwd1 = document.getElementById("txtHostPwd");
        var hostPwd2 = document.getElementById("txtCfmHostPwd");
        if(hostPwd1 != null)
            hostPwd1.value = num;
        if(hostPwd2 != null)
            hostPwd2.value = num;

        SaveHostPassword(); 
    }
        
    function SaveHostPassword()
    {
        document.getElementById("<%=hdnHostPassword.ClientID %>").value = document.getElementById("<%=txtHostPwd.ClientID %>").value;
    }
    //ALLDEV-826 Ends
</script>
<script runat="server">
    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        int index = Int32.Parse(e.Item.Value);
        Page.Validate();
        //Response.Write(index);
        if (Page.IsValid)
        {
           
            TopMenu.Items[Int32.Parse(e.Item.Value)].Selected = true;
            Wizard1.ActiveViewIndex = index;
        }
        else
        {
            TopMenu.Items[Int32.Parse(e.Item.Value)].Selected = false;
            TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
        }

        //ZD 100875
        hdnConfName.Value = ConferenceName.Text;
        hdnSaveData.Value = "1";

        if (index < 8) //FB 2659
            hdnOverBookConf.Value = "0";
        
        if (Wizard1.ActiveViewIndex == 0)
            index = 0;

        //Merging Recurrence - start
        if (index > 0)
        {   
            if (hdnRecurValue.Value == "R" && Recur.Value == "")
            {   
                errLabel.Text = "Please select the Recurrence Pattern";
                Wizard1.ActiveViewIndex = 0;
                TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
                index = 0;
            }
        }

        hdnValue.Value = index.ToString();
        //Merging Recurrence - end

        /*if (!ValidateSetupTearDownTime())   //buffer zone
        {
            errLabel.Visible = true;
            TopMenu.Items[0].Selected = true;
            Wizard1.ActiveViewIndex = 0;
        }*/
       
        
        if (index.Equals(Wizard1.Views.Count - 1))
        {
            btnPrev.Visible = false;
            btnNext.Visible = false;
        }
        else
        {
            btnNext.Visible = true;
            btnPrev.Visible = true;
        }

        if (index == 0)
            btnPrev.Visible = false; //FB 2516

        //ZD 103216        
        Hashtable RmHostAttendee = new Hashtable();
        foreach (DataGridItem dgi in dgRoomHost.Items)
        {
            DropDownList dlNoofAttendees = ((DropDownList)dgi.FindControl("dlNoofAttendees"));
            RadioButton rdRoomHost = ((RadioButton)dgi.FindControl("rdRoomHost"));
            if (dlNoofAttendees != null)
            {
                string hstValue = (rdRoomHost.Checked) ? "1" : "0";
                RmHostAttendee[dlNoofAttendees.Attributes["RoomID"]] = hstValue + "|" + dlNoofAttendees.SelectedValue;
            }
        }

        if (RmHostAttendee.Count > 0)
            Session["RoomHostDetails"] = RmHostAttendee;
        
        //Response.Write(index + " : " + Wizard1.Views.Count);
        if (index.Equals(Wizard1.Views.Count - 1))
            LoadPreview();
        if (e.Item.Text.IndexOf("Cater") > 0)
        {
            LoadCateringWorkorders();
        }
        //FB 1985
        //if (e.Item.Text.IndexOf("Audio/Video") > 0) 
        if (e.Item.Text.IndexOf("Audio") > 0)
        {
            hdnUpdateAV.Value = "1"; //ZD 101490
            UpdateAdvAVSettings(new object(), new EventArgs());
        }
        // FB Case 718: Saima hiding the conflict datagrid when navigating away from preview screen after it is bbeen populated.
        if (Wizard1.ActiveViewIndex.Equals(Wizard1.Views.Count - 1) && (dgConflict.Items.Count > 0) && (dgConflict.Visible.Equals(true)))
        {
            dgConflict.DataSource = null;
            dgConflict.DataBind();
            dgConflict.Visible = false;
            tblConflict.Visible = false;
            btnConfSubmit.InnerText = "Submit Conference";
        }
        
        //FB 1865
        if (hdnParty.Value != "" && txtPartysInfo.Text.Trim() == "")
            txtPartysInfo.Text = hdnParty.Value;
    }
</script>

<%--Merging Recurrence start--%>
<script type="text/javascript" language="JavaScript" src="../<%=Session["language"] %>/script/errorList.js"></script>
<script type="text/javascript" language="javascript1.1" src="extract.js"></script>
<%--Merging Recurrence end--%>
<script language="VBScript" src="script/lotus.vbs"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/inc/functions.js"></script>
<%--Merging Recurrence--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/script/settings2.js"></script>
<script type="text/vbscript" src="script/settings2.vbs"></script>
<script type="text/javascript" src="script/mousepos.js"></script>

<%--Merging Recurrence start--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script language="VBScript" src="script/outlook.vbs"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/script/saveingroup.js"></script>
<%--<script type="text/javascript" src="script/group2.js"></script> - Code Commented For FB 1476 --%>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<%--Merging Recurrence end--%>
<script type="text/javascript" src="script/RoomSearch.js"></script><%--Room Search--%>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /><%--FB 2769--%>

<script language="javascript">
		
			function callalert(val)
			{
				alert(val);
				return false;
			}
</script>


<script language="javascript">
//Merging Recurrence
function fnEnableBuffer()
 {//FB 2634
 /*
 var chkenablebuffer = document.getElementById("chkEnableBuffer");
 
 //FB 1911
 if(document.getElementById("RecurSpec").value  != "")
    return;
 
 
    var chkrecurrence = document.getElementById("chkRecurrence");
 
 if(chkrecurrence && chkrecurrence.checked == true)
            {
    
             var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var apBfr = endTime.format("tt"); //FB 2108
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            
            timeMins = timeMins + parseInt(mins);
             
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
            //FB 2614
            var hh = endTime.getHours() ;// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
            if("<%=Session["timeFormat"]%>" == "1") //FB 2108
            {
                if(hh >= 12)
                    hh = hh - 12;
                    
                if(hh == 0)
                    hh = 12
            }
            if (hh < 10)
                hh = "0" + hh;
            //FB 2614
            var mm = endTime.getMinutes();// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var ap = endTime.format("tt"); //FB 2108
            
            //document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            var evntime = parseInt(hh,10);
            
            if(evntime < 12)
                evntime = evntime + 12
                       
            if("<%=Session["timeFormat"]%>" == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
//Commented for FB 1714 - Starts            
//            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//            
//             document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//         document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value; //Edited For FF
//Commented for FB 1714 - End       
            }
            
            
 
    if(chkenablebuffer && chkenablebuffer.checked)
    {
        document.getElementById("SDateText").innerHTML = "Pre Conference Start <span class='reqfldstarText'>*</span>";
        document.getElementById("EDateText").innerHTML  = "Post Conference End <span class='reqfldstarText'>*</span>";
        document.getElementById("ConfStartRow").style.display = ""; // Edited for FF
        document.getElementById("ConfEndRow").style.display = ""; // Edited for FF
        document.getElementById("SetupTime_Text").style.width = "75px"; //buffer zone
        document.getElementById("TeardownTime_Text").style.width = "75px"; //buffer zone
    }
    else
    {   
        document.getElementById("SDateText").innerHTML = "Start Date/Time <span class='reqfldstarText'>*</span>";     
        document.getElementById("EDateText").innerHTML = "End Date/Time <span class='reqfldstarText'>*</span>";            
        document.getElementById("ConfStartRow").style.display = "None";
        document.getElementById("ConfEndRow").style.display = "None";
        
         document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
         
         document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;//Edited for FF
        
        if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
        { 
            
                           
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;//Edited for FF
        } 
          
    }
    */
 }
 /***  SJV BufferZone Fix ****/
 function SetRecurBuffer()
 {//FB 2634
    /*var chkenablebuffer = document.getElementById("chkEnableBuffer");
    var chkrecurrence = document.getElementById("chkRecurrence");
    
            if(chkenablebuffer && chkenablebuffer.checked == false)
            {
    
            if(chkrecurrence && chkrecurrence.checked == true)
            {
    
             var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var apBfr = endTime.format("tt"); //FB 2108
            
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            
            timeMins = timeMins + parseInt(mins);
             
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
            //FB 2614
             var hh = endTime.getHours(); //parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);  
             if("<%=Session["timeFormat"]%>" == "1") //FB 2108
            {
                if(hh >= 12)
                    hh = hh - 12;
                    
                if(hh == 0)
                    hh = 12
            }          
            
            if (hh < 10)
                hh = "0" + hh;
            //FB 2614
            var mm = endTime.getMinutes();// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var ap = endTime.format("tt"); //FB 2108
            
            //document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            var evntime = parseInt(hh,10);
            
            if(evntime < 12)
                evntime = evntime + 12
                       
            if('<%=Session["timeFormat"]%>' == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
            //FB 2634
//            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//            
//             document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//         document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
            
            }
            }*/
 }
 /***  SJV BufferZone Fix ****/
     
function randam() { 
//ZD 100522 Starts
   var pwdCharLength =  "<%= PINlength%>"; //ZD 100522
   if(pwdCharLength <= 0)
      pwdCharLength = 4;
   var totalDigits = pwdCharLength;
//ZD 100522 End
   var n = Math.random()*10000;
   n = Math.round(n);
   n = n.toString(); 
      var pd = ''; 
      if (totalDigits > n.length) { 
         for (i=0; i < (totalDigits-n.length); i++) { 
            pd += '0'; 
         } 
      } 
   return pd + n.toString();
}

function num_gen() 
{
   var num = randam()
  //alert(num);
   if (num.indexOf(0) == 0) 
      num = num.replace(0,1)
  
    //FB 2244 - Starts
    if(document.getElementById("cmpValPassword") != null)
        document.getElementById("cmpValPassword").style.display = "none";
    //if(document.getElementById("cmpValPassword1") != null) //ZD 102692
        //document.getElementById("cmpValPassword1").style.display = "none";
    if(document.getElementById("numPassword1") != null)
        document.getElementById("numPassword1").style.display = "none";

    var pass = document.getElementById("ConferencePassword")
    var pass1 = document.getElementById("ConferencePassword2")
    //if(pass != "")
    if(pass != null)
        pass.value = num;
    //if(pass1 != "")
    if(pass1 != null)
        pass1.value = num;
    //FB 2244 - End
     
   SavePassword(); 
} //fb 676 end 



    function UpdateCheckbox(obj)
    {
        if (obj.value == "")
        {
           document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = false;
           UpdatePrice(obj);
        }
        else
        {
            if (!isNaN(obj.value) && obj.value.indexOf(".") < 0)
            {
                document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = true;
                UpdatePrice(obj);
            }
            else
            {
                alert(RInvalidValue);
                obj.value = "";
            }
        }
    }
    
    function UpdatePrice(obj)
    {
		//ZD 104792
        var idValue = obj.id.split("_")[1].replace("ctl","");
        var lblPrice = document.getElementById(obj.id.substring(0, obj.id.indexOf("_dgCateringMenus")) + "_lblPrice");
        var price = parseFloat("0.00"); //FB 1686
        var i = idValue.replace("0","");
        var n = "0" + i;
        var qtyId  = "CATMainGrid_ctl"+ idValue +"_dgCateringMenus_ctl"+ idValue +"_txtQuantity"; //obj.id.substring(0, (obj.id.indexOf("Menus_ctl0")) + i + "_txtQuantity")

        while(document.getElementById(qtyId))
        {

            price += document.getElementById("CATMainGrid_ctl"+ idValue +"_dgCateringMenus_ctl"+ n + "_txtPrice").value * document.getElementById(qtyId).value;
//            if (i<10)
//            {
//                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity").value;
//            }
//            else
//            {
//                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtQuantity").value;
//            }
            i++;
            n = i;
            if(i < 10)
                n = "0" + i;

            qtyId  = "CATMainGrid_ctl"+ idValue +"_dgCateringMenus_ctl"+ n +"_txtQuantity";
        }
        // WO Bug Fix
        lblPrice.innerHTML = ((price * 100.00) / 100.00).toFixed(2);
        //FB 1830
        if("<%=cFormat%>" == "�")
            lblPrice.innerHTML = (((price * 100.00) / 100.00).toFixed(2)).replace(","," ").replace(".",",");
    }

    function ShowItems(obj)
    {
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.borderWidth = 1;
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
    }

    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }


function CheckFiles()
{
    //ALLDEV-826 Starts
    if(document.getElementById("ConferencePassword") != null)
    {
        if(document.getElementById("txtHostPwd") != null)
        {
            var confPwd = "", hostPwd = "", reqValHostPwd="";
            confPwd = document.getElementById("ConferencePassword");
            hostPwd = document.getElementById("txtHostPwd");
            reqValHostPwd = document.getElementById("reqValHostPwd");

            if (confPwd.value != "" && hostPwd.value == "") {
                ValidatorEnable(reqValHostPwd, true);
            }
            else{
                ValidatorEnable(reqValHostPwd, false);
            }
            if (document.getElementById('lstVMR') != null) {
                if (document.getElementById('lstVMR').value == "2") {                    
                    ValidatorEnable(reqValHostPwd, false);
                }
            }            
        }
    }        
    //ALLDEV-826 Ends

    //ZD 102692
    if(document.getElementById("cmpValPassword") != null)
    {
        if(document.getElementById("cmpValPassword").style.display == "inline")
            return false;
    }

    //ZD 101052
    if(document.getElementById("hdnFileUpload") != null)
        document.getElementById("hdnFileUpload").value = "0";
    //ZD 100875
    if(document.getElementById("hdnSaveData") != null)
        document.getElementById("hdnSaveData").value = "1";
    
        
    //alert("in checkfiles");
    var objMain = document.getElementById("dgUsers");
    //alert(objMain);
    
    //ZD 103624
    if(document.getElementById("confStartTime_Text"))
    {
        var sDate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value); 
        var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value); 
        if(!validateStartEndDate(sDate,eDate,1,1))
            return false;
    }
    DataLoading(1);
//    alert(typeof(Page_ClientValidate));
    if (typeof(Page_ClientValidate) == 'function') 
        if (!Page_ClientValidate())
        {
            DataLoading(0);
            return false;
        }
        
    if (document.getElementById("ConferencePassword")) //FB Case 763: Saima
        SavePassword();
    if (document.getElementById("lstEndpoints"))
    {
        if ( (document.getElementById("chkLectureMode").checked) && (document.getElementById("lstEndpoints").value == "-1") && (document.getElementById("lstEndpoints").options.length > 1))
            {
                DataLoading(0);
                alert(ConfLect);
                return false;
            }
        else
            document.getElementById("txtLecturer").value = document.getElementById("lstEndpoints").value;
       
    }
    if (document.getElementById("lblTab"))
    {
		//ZD 100716 - Start
        if (document.getElementById("lblTab").value == "1")
        {
            if(document.getElementById("btnSubmit") && document.getElementById("AVTab").value == "1")
            {
                DataLoading(0);
                alert(ConfClickCreate);  //FB 1246
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                return false;
            }
            if(document.getElementById("btnSubmit") && document.getElementById("AVTab").value == "2")
            {
                DataLoading(0);
                alert(ConfClickEdit);  //FB 1246
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                return false;
            }
         }
           
        if (document.getElementById("lblTab").value == "2")
        {
            if(!document.getElementById("btnAddNewCAT") && document.getElementById("CatTab").value == "1" && document.getElementById("CATMainGrid"))
            {
                    DataLoading(0);
                    alert(ConfClickCreate);//ZD 100716
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                    return false;
            }
            if (!document.getElementById("btnAddNewCAT") && document.getElementById("CatTab").value == "2" && document.getElementById("CATMainGrid"))
            {   
                    DataLoading(0);
                    alert(ConfClickUpdate);  //FB 1246
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                    return false;
            } 
            if (!document.getElementById("btnAddNewCAT") && document.getElementById("CatTab").value == "3" && document.getElementById("CATMainGrid"))
            {   
                    DataLoading(0);
                    alert(ConfClickCancel);  
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                    return false;
            } 
        }
        if (document.getElementById("lblTab").value == "3")
        {
            if(document.getElementById("btnHKSubmit") && document.getElementById("HKTab").value == "1")
            {
                DataLoading(0);
                alert(ConfClickCreate);  //FB 1246
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                return false;
            }
            if(document.getElementById("btnHKSubmit") && document.getElementById("HKTab").value == "2")
            {
                DataLoading(0);
                document.getElementById("hdnSaveData").value = ""; //ZD 100875
                alert(ConfClickEdit);  //FB 1246
                return false;
            }
         }
		//ZD 100716 - End
    }
    if (document.getElementById("ifrmPartylist"))
    {
        if (!ifrmPartylist.bfrRefresh())
        {
            DataLoading(0);
            return false;
        }
    }
    if (document.getElementById("FileUpload1"))
    {
        if(document.getElementById("FileUpload1").value)
        {
            if (confirm(UploadFile))
            {
                //ZD 101052 Starts
                document.getElementById("hdnFileUpload").value = "1";
                //document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                //WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                //ZD 101052 End
            }
            else
            {
                DataLoading(0);            
            }
        }
    }
    if (document.getElementById("FileUpload2"))
    {
        if(document.getElementById("FileUpload2").value)
        {
            if (confirm("Do you want to upload the browsed file?"))
            {
                //ZD 101052 Starts
                document.getElementById("hdnFileUpload").value = "1";
                //document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                //WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                //ZD 101052 End
            }
            else
                DataLoading(0);            
        }
    }
    if (document.getElementById("FileUpload3"))
    {
        if(document.getElementById("FileUpload3").value)
        {
            if (confirm(UploadFile))
            {
                //ZD 101052 Starts
                document.getElementById("hdnFileUpload").value = "1";
                //document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                //WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                //ZD 101052 End
            }
            else
                DataLoading(0);            
        }
    }
   
    //Merging Recurrence
    if(CheckFiles.arguments.length > 0 && CheckFiles.arguments[0] == 'N')
    {     
        var chkrecurrence = document.getElementById("chkRecurrence");
    
        if(chkrecurrence != null && chkrecurrence.checked == true)
        {
            if(!SubmitRecurrence())
            {
                DataLoading(0);            
                return false;
            }
        }
    }
    
    if (document.getElementById("confStartDate") != null)
    {
        if (document.getElementById("<%=ConferenceName.ClientID %>") != null)//FB 2694
        {
            if (document.getElementById("<%=ConferenceName.ClientID %>").value == "")
            {
                DataLoading(0);            
                return false;
            }
        }
        //return ChangeEndDate(2); //MOJ //FB 2634
        return ChangeStartDate(0);
    }
    // FB Case 712 Saima
    if (document.getElementById("dgUsers"))
    {
       
       //alert("dgusers");
        var objMain = document.getElementById("dgUsers");
        var tVars = objMain.getElementsByTagName("INPUT").length + objMain.getElementsByTagName("SPAN").length + objMain.getElementsByTagName("SELECT").length;        

        var inputs = objMain.getElementsByTagName("INPUT");
        var selects = objMain.getElementsByTagName("SELECT");
        var objResult = document.getElementById("txtdgUsers");
        
        objResult.value = "";
        /*
        for(var i=0;i<inputs.length-1;i++)
        {
            if (inputs[i].id.indexOf("chkIsOutside") > 0)
                if (inputs[i].checked)
                    objResult.value = objResult.value + "1:";
                else
                    objResult.value = objResult.value + "0:";
            if (inputs[i].id.indexOf("txtAddress") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
            if (inputs[i].id.indexOf("lblUserID") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
            if (inputs[i].id.indexOf("txtEndpointURL") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
            if (inputs[i].id.indexOf("txtExchangeID") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
               //API Port Starts...
                if (inputs[i].id.indexOf("txtApiportno") > 0)
                objResult.value = objResult.value + inputs[i].value + ";";
                //API Port Ends...
        }   
        for (var i=0;i<selects.length;i++)
        {
            if (selects[i].id.indexOf("lstBridges") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            else if (selects[i].id.indexOf("lstTelnet") > 0)    //FB 1468
                objResult.value = objResult.value + selects[i].value + ":";
                
            if (selects[i].id.indexOf("lstLineRate") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstConnectionType") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstProtocol") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstAddressType") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstVideoEquipment") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstConnection") > 0 && selects[i].id.indexOf("lstConnectionType") <= 0)
                objResult.value = objResult.value + selects[i].value + ";";
        }
        */
        /* Code Modified during API Ports */
        var hdnextusrs = document.getElementById("hdnextusrcnt");
        var extusrcnt = 0;
        var controlName = 'dgUsers_ctl';
        var ctlid = '';
        
        if(hdnextusrs)
        {
            if(hdnextusrs.value != '')
                extusrcnt = parseInt(hdnextusrs.value, 10); // ZD 101722
        }
        var inputval;
        
        if(extusrcnt > 0)
		{
		    if(objResult)
                objResult.value = '';
                
            var conftype = document.getElementById("hdnconftype");
            
            var cnftype = "";
			  
		    if(conftype)
		        cnftype = conftype.value;
			        
            for(var i=1; i<=extusrcnt; i++)
			{
			    ctlid = ""; 
                if(i < 10)
                    ctlid = '0' + (i+1); 
                else 
                    ctlid = i+1;

			    //All input controls
			    var opt1 = document.getElementById(controlName + ctlid + "_" + "chkIsOutside");
			    var txtAdd = document.getElementById((controlName + ctlid + "_" + 'txtAddress'));
				var lblusr = document.getElementById((controlName + ctlid + "_" + 'lblUserID'));
                var txtepturl = document.getElementById((controlName + ctlid + "_" + 'txtEndpointURL'));
                var txtexchangeid = document.getElementById((controlName + ctlid + "_" + 'txtExchangeID'));
                var txtAPIPort = document.getElementById((controlName + ctlid + "_" + 'txtApiportno'));
                var txtconfcode = document.getElementById((controlName + ctlid + "_" + 'txtConfCode'));//Audio Addon
                var txtleader = document.getElementById((controlName + ctlid + "_" + 'txtleaderPin'));//Audio Addon
                var txtpartyCode = document.getElementById((controlName + ctlid + "_" + 'txtPartyCode'));//ZD 101446
                var lblProfileID = document.getElementById((controlName + ctlid + "_" + 'lblProfileID'));//ALLDEV-814
                var txtisUserAudio = document.getElementById((controlName + ctlid + "_" + 'txtisUserAudio'));//ALLDEV-814
                var txtAudioaddon = document.getElementById((controlName + ctlid + "_" + 'txtAudioaddon'));//ALLDEV-814
                var txtEmail = document.getElementById((controlName + ctlid + "_" + 'txtEmail'));//ALLDEV-814
                
                //All select(drp) controls
                var lstBridge = document.getElementById((controlName + ctlid + "_" + 'lstBridges'));
                var lstTelnet = document.getElementById((controlName + ctlid + "_" + 'lstTelnetUsers'));
                var lstLineRate = document.getElementById((controlName + ctlid + "_" + 'lstLineRate'));
                var lstConType = document.getElementById((controlName + ctlid + "_" + 'lstConnectionType'));
                var lstProtocol = document.getElementById((controlName + ctlid + "_" + 'lstProtocol'));
                var lstAddType = document.getElementById((controlName + ctlid + "_" + 'lstAddressType'));
                var lstVideoEquip = document.getElementById((controlName + ctlid + "_" + 'lstVideoEquipment'));
                var lstConn = document.getElementById((controlName + ctlid + "_" + 'lstConnection'));
                var lstMCUProf = document.getElementById((controlName + ctlid + "_" + 'lstMCUProfile')); //FB 2839                
                var lstPoolOrder = document.getElementById((controlName + ctlid + "_" + 'drpPoolOrder')); //ZD 104256             
                                
                inputval = '';
			    if(opt1) //0
				{
					if(opt1.checked)
					{
                        inputval = inputval + "1:";
                    }
                    else
                    {
                        inputval = inputval + "0:";
					}
				}
				if (txtAdd) //1
				{
				    inputval = inputval + txtAdd.value +':';
				}
				else
				    inputval = inputval + ':';
				
				if(lblusr) //2
				{
					inputval = inputval + lblusr.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtepturl) //3
				{
					inputval = inputval + txtepturl.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtexchangeid)//4
				{
					inputval = inputval + txtexchangeid.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtAPIPort) //5
				{
					inputval = inputval + txtAPIPort.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				
				    
				if(lstBridge) // Selects
				{
				    inputval = inputval + lstBridge.value +':';
				}
				else if (lstTelnet)
				{
				    inputval = inputval + lstTelnet.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
			    
				if(lstLineRate)
				{
					inputval = inputval + lstLineRate.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstConType)
				{
					inputval = inputval + lstConType.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstProtocol)
				{
					inputval = inputval + lstProtocol.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstAddType)
				{
					inputval = inputval + lstAddType.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstVideoEquip)
				{
					inputval = inputval + lstVideoEquip.value +':';
				}
				if(lstConn)
				{
					inputval = inputval + lstConn.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				/****** Code addedd for audio addon ***** */
				    
				if(txtconfcode)//13 
				{
					inputval = inputval + txtconfcode.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtleader)//14
				{
					inputval = inputval + txtleader.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(lstMCUProf)//15
				{
					inputval = inputval + lstMCUProf.value +':';
				}
				else
				    inputval = inputval + ':';

                if(txtpartyCode)//16 //ZD 101446 
				{
					inputval = inputval + txtpartyCode.value +':';
				}
				else
				    inputval = inputval + ':';

                if(lstPoolOrder)//17  //ZD 104256
					inputval = inputval + lstPoolOrder.value +':';
				else
				    inputval = inputval + ':';

                if(lblProfileID)//18  //ALLDEV-814
					inputval = inputval + lblProfileID.value +':';
				else
				    inputval = inputval + ':';    

                 if(txtisUserAudio)//19  //ALLDEV-814
					inputval = inputval + txtisUserAudio.value +':';
				else
				    inputval = inputval + ':';    
                    
                if(txtAudioaddon)//20  //ALLDEV-814
					inputval = inputval + txtAudioaddon.value +':';
				else
				    inputval = inputval + ':';    

                if(txtEmail)//21  //ALLDEV-814
					inputval = inputval + txtEmail.value +';';
				else
				    inputval = inputval + ';';    
                    
				/****** Code addedd for audio addon ***** */
				objResult.value = objResult.value + inputval;
			}
	    }
        /* **** */
        //alert(objResult.value);
    }
    //false::127.0.0.1::;;false::1.2.3.4::;;1::768::1::1;;1::1::1::3;;1::384::2::2;;1::1::5::3;;
    
    
    
    return true;
}


function CheckPolycom(obj)
{
    if (obj.checked)
    {
        document.getElementById("trPoly1").style.display="";
        document.getElementById("trPoly2").style.display="";
        document.getElementById("trPoly3").style.display="";
        //document.getElementById("trPoly4").style.display=""; // FB 2441 //ZD 100298
    }
    else
    {
        document.getElementById("trPoly1").style.display="none";
        document.getElementById("trPoly2").style.display="none";
        document.getElementById("trPoly3").style.display="none";
        //document.getElementById("trPoly4").style.display="none"; //FB 2441 //ZD 100298
    }
    DisplayLayoutChanges(); //ZD 101871
	document.getElementById("<%=imgVideoDisplay.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
    saveLayout(document.getElementById("<%=txtSelectedImage.ClientID %>").value);
	CheckLectureMode();
}

//ZD 101871 Start
function DisplayLayoutChanges()
{    
    if(document.getElementById("hdnLOSelection").value == "0")
    {
        var selectedLO = "01";
        if (document.getElementById("hdnPolycomMGC").value == "1") 
        {
            if("<%= defPolycomMGCLO %>" <= 9)
                selectedLO = "0" + "<%= defPolycomMGCLO %>";
            else
                selectedLO = "<%= defPolycomMGCLO %>";
        }
        else if(document.getElementById("hdnPolycomRMX").value == "1") 
        {
            if("<%= defPolycomRMXLO %>" <= 9)
                selectedLO = "0" + "<%= defPolycomRMXLO %>";
            else
                selectedLO = "<%= defPolycomRMXLO %>";
        }
        else if(document.getElementById("hdnCodian").value == "1")   
        {
            if("<%= DefCodianLO %>" <= 9)
                selectedLO = "0" + "<%= DefCodianLO %>";
            else
                selectedLO = "<%= DefCodianLO %>";
        }
        document.getElementById("<%=txtSelectedImage.ClientID %>").value = selectedLO;    
    }
}

 function saveLayout(id) {

    if (id == "00")
        id = "01";
    if (document.getElementById("hdnCodian").value == "1") 
    {
        document.getElementById("hdnFamilyLayout").value = 1;
        if (id == 101) {
            document.getElementById("imgVideoDisplay").style.display = '';
            document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "05" + ".gif";
            document.getElementById("imgLayoutMapping6").style.display = '';
            document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "06" + ".gif";
            document.getElementById("imgLayoutMapping7").style.display = '';
            document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "07" + ".gif";
            document.getElementById("imgLayoutMapping8").style.display = '';
            document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "44" + ".gif";
            document.getElementById("lblCodianLO").innerHTML = Family1; 
        }
        else if (id == 102) {
            document.getElementById("imgVideoDisplay").style.display = '';
            document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "01" + ".gif";
            document.getElementById("imgLayoutMapping6").style.display = 'none';
            document.getElementById("imgLayoutMapping7").style.display = 'none';
            document.getElementById("imgLayoutMapping8").style.display = 'none';
            document.getElementById("lblCodianLO").innerHTML = Family2; 
        }
        else if (id == 103) {
            document.getElementById("imgVideoDisplay").style.display = '';
            document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
            document.getElementById("imgLayoutMapping6").style.display = 'none';
            document.getElementById("imgLayoutMapping7").style.display = 'none';
            document.getElementById("imgLayoutMapping8").style.display = 'none';
            document.getElementById("lblCodianLO").innerHTML = Family3; 
        }
        else if (id == 104) {
            document.getElementById("imgVideoDisplay").style.display = '';
            document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
            document.getElementById("imgLayoutMapping6").style.display = '';
            document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "03" + ".gif";
            document.getElementById("imgLayoutMapping7").style.display = '';
            document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "04" + ".gif";
            document.getElementById("imgLayoutMapping8").style.display = '';
            document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "43" + ".gif";
            document.getElementById("lblCodianLO").innerHTML = Family4; 
        }
        else if (id == 105) {
            document.getElementById("imgVideoDisplay").style.display = '';
            document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "25" + ".gif";
            document.getElementById("imgLayoutMapping6").style.display = 'none';
            document.getElementById("imgLayoutMapping7").style.display = 'none';
            document.getElementById("imgLayoutMapping8").style.display = 'none';
            document.getElementById("lblCodianLO").innerHTML = Family5; 
        }
        else if (id == 100) {
            document.getElementById("imgVideoDisplay").style.display = 'none';
            document.getElementById("imgLayoutMapping6").style.display = 'none';
            document.getElementById("imgLayoutMapping7").style.display = 'none';
            document.getElementById("imgLayoutMapping8").style.display = 'none';
            document.getElementById("lblCodianLO").innerHTML = Defaultfamily; 

        }
        else {
            document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
            document.getElementById("imgVideoDisplay").style.display = '';
            document.getElementById("imgLayoutMapping6").style.display = 'none';
            document.getElementById("imgLayoutMapping7").style.display = 'none';
            document.getElementById("imgLayoutMapping8").style.display = 'none';
            document.getElementById("lblCodianLO").innerHTML = ""; 
            document.getElementById("hdnFamilyLayout").value = 0;
        }
    }
    else
    {
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgLayoutMapping6").style.display = 'none';
        document.getElementById("imgLayoutMapping7").style.display = 'none';
        document.getElementById("imgLayoutMapping8").style.display = 'none';
        document.getElementById("lblCodianLO").innerHTML = ""; 
        document.getElementById("hdnFamilyLayout").value = 0;
    }
 return true;
}

function CheckLectureMode()
{
    if (document.getElementById("chkLectureMode").checked)  
    {
        document.getElementById("trLecture1").style.display="";
        document.getElementById("trLecture2").style.display="";
        //Code Changed by offshore for FB Issue 1121 - Start
        //document.getElementById("lstVideoMode").options[1].selected = true; //FB case 764 Saima
        document.getElementById("lstVideoMode").options[3].selected = true; 
        //Code Changed by offshore for FB Issue 1121 - End
    }
    else
    {
        document.getElementById("trLecture1").style.display="none";
        document.getElementById("trLecture2").style.display="none";
        if(document.getElementById("lstVideoMode").options[3]!=null)  //FB 2516
            document.getElementById("lstVideoMode").options[3].selected = true; //FB case 764 Saima
    }
}

function managelayout (dl, epid, epty, par)
{
if (par == '0') {
    mousedownX = 700;
}
//	change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 4, "", ""); 
	change_display_layout_prompt('image/pen.gif', RSManageLayOut, epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
}
function displayLayout(s)
{	
	var obj = document.getElementById("default" + s);
	var temp = document.getElementById("display" + s);
	//alert(obj.checked);
	if (obj.checked)
		temp.style.display = "block";
	else
		temp.style.display = "none";
}

function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = document.getElementById('prompt').style; // FB 2050

	promptbox.position = 'absolute'
	promptbox.top = -155+ mousedownY + 'px'; //FB 1373 start FB 2050
	promptbox.left = mousedownX - 485 + 'px'; // FB 2050
	promptbox.width = rowsize * 125 + 'px'; // FB 2050
	promptbox.border = 'outset 1 #bbbbbb';
	promptbox.height = '400px' ; // FB 2050
	promptbox.overflow ='auto'; //FB 1373 End
    promptbox.backgroundColor='#FFFFE6';//ZD 100426
//     if (document.getElementById("hdnCodian").value == "1")  
//        images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:60:61:62:63:";
//     if(document.getElementById("hdnPolycomMGC").value == "1" || document.getElementById("hdnPolycomRMX").value == "1")
//       images = "01:02:03:04:05:06:12:13:14:15:16:17:18:19:20:24:25:33:60:61:62:63:";
       
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'>&nbsp;</td><td class='tableHeader'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
	imagesary = images.split(":");
	rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	 //Code Changed for Soft Edge Button
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "    <button id='btnLayoutSubmit' class='altMedium0BlueButtonFormat' style='width:80px' onClick='saveOrder(epid);' ><asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal></button>" //ZD 101028
	m += "    <button id='btnLayoutCancel' class='altMedium0BlueButtonFormat' style='width:80px' onClick='cancelthis();'><asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal></button>" //ZD 101028
	m += "  </td></tr>"
	m += "	<tr>";
	//Window Dressing
	m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageConference_DisplayLayout%>' runat='server'></asp:Literal></td>";//FB 2579
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"

     //ZD 101869 start
        if (document.getElementById("hdnCodian").value == "1") {

            m += "<tr><td colspan='" + (rowsize * 2) + "'>";
            m += "<table>";
            m += "  <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='100' onClick='epid=100'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Defaultfamily + "</td>";
            m += "  </tr>";

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='101' onClick='epid=101'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family1 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "05"+ ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "06" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "07" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "44" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='102' onClick='epid=102'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family2 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "01" + ".gif' width='57' height='43' alt='Layout'>"; 
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "  <tr>";
            m += "    <td  valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='103' onClick='epid=103'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family3 + "</td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"


            m += "  <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='104' onClick='epid=104'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family4 + "</td>";
            m += "    </td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "02" + ".gif' width='57' height='43' alt='Layout' >";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "03" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "04" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "    <td></td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "43" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>";

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "    <tr>";
            m += "    <td valign='middle'>";
            m += "      <input type='radio' name='layout' id='layout' value='105' onClick='epid=105'>";
            m += "    </td>";
            m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family5 + "</td>";
            m += "    </td>";
            m += "    <td valign='middle'>";
            m += "      <img src='" + imgpath + "25" + ".gif' width='57' height='43' alt='Layout'>";
            m += "    </td>";
            m += "  </tr>"

            m += "  <tr>"
            m += "    <td height='1'></td>";
            m += "  </tr>"

            m += "</table>";
            m += " </td></tr>"

            m += "  <tr>"
            m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
            m += "  </tr>"

        }
        //ZD 101869 End

	imgno = 0;
	for (i = 0; i < rowNum; i++) 
	{
		m += "  <tr>";
		for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
			
		
			m += "    <td valign='middle'>";
			m += "      <input type='radio' tabindex='3' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
			m += "    </td>";
			m += "    <td valign='middle'>";
			m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43' alt='layout'>"; //ZD 100419
			m += "    </td>";
			imgno ++;
		}
		m += "  </tr>";
	}
    
	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	//Code Changed for Soft Edge Button
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "    <button id='btnLayoutSubmit1' class='altMedium0BlueButtonFormat' style='width:80px'  onClick='saveOrder(epid);' tabindex='4'><asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal></button>" //ZD 101028
	m += "    <button id='btnLayoutCancel1' class='altMedium0BlueButtonFormat' style='width:80px' onClick='cancelthis();' tabindex='5'><asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal></button>" //ZD 101028
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
	document.getElementById('btnLayoutSubmit').focus();
//   if (document.getElementById('btnConfLayoutSubmit') != null)            
//       document.getElementById('btnConfLayoutSubmit').setAttribute("onblur", "document.getElementById('btnLayoutSubmit').focus(); document.getElementById('btnLayoutSubmit').setAttribute('onfocus', '');");

//	if (document.getElementById('btnLayoutSubmit') != null)
//	    document.getElementById('btnLayoutSubmit').setAttribute("onblur", "document.getElementById('btnLayoutCancel').focus(); document.getElementById('btnLayoutCancel').setAttribute('onfocus', '');");
//	if (document.getElementById('btnLayoutCancel') != null)
//	    document.getElementById('btnLayoutCancel').setAttribute("onblur", "document.getElementById('layout').focus(); document.getElementById('gprivate').setAttribute('layout', '');");
	//if (document.getElementById('layout') != null)
	  //  document.getElementById('layout').setAttribute("onblur", "document.getElementById('btnLayoutSubmit1').focus(); document.getElementById('btnLayoutSubmit1').setAttribute('onfocus', '');");
//	if (document.getElementById('btnLayoutSubmit1') != null)
//	    document.getElementById('btnLayoutSubmit1').setAttribute("onblur", "document.getElementById('btnLayoutCancel1').focus(); document.getElementById('btnLayoutCancel1').setAttribute('onfocus', '');");
//	if (document.getElementById('btnLayoutCancel1') != null)
//	    document.getElementById('btnLayoutCancel1').setAttribute("onblur", "document.getElementById('btnLayoutSubmit').focus(); document.getElementById('btnLayoutSubmit').setAttribute('onfocus', '');");
} 

function saveOrder(id) 
{
    if (id < 10)
        id = "0" + id;
	document.getElementById("<%=txtSelectedImage.ClientID %>").value = id;
	document.getElementById("<%=imgVideoDisplay.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
    document.getElementById("hdnLOSelection").value = "1";

    document.getElementById("hdnFamilyLayout").value = 0;
     if (id == "00") //FB 2384
         id = "01";
   
    //if (invokingObject == "hdnCodian") {
    if (document.getElementById("hdnCodian").value == "1" || document.getElementById("hdnLOSelection").value == "1" ) {
            //document.getElementById("hdnCodian").value = id;

            document.getElementById("hdnFamilyLayout").value = 1;
            if (id == 101) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "05" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = '';
                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "06" + ".gif";
                document.getElementById("imgLayoutMapping7").style.display = '';
                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "07" + ".gif";
                document.getElementById("imgLayoutMapping8").style.display = '';
                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "44" + ".gif";
                document.getElementById("lblCodianLO").innerHTML = Family1; 
            }
            else if (id == 102) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "01" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family2; 
            }
            else if (id == 103) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family3; 
            }
            else if (id == 104) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "02" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = '';
                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/" + "03" + ".gif";
                document.getElementById("imgLayoutMapping7").style.display = '';
                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/" + "04" + ".gif";
                document.getElementById("imgLayoutMapping8").style.display = '';
                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/" + "43" + ".gif";
                document.getElementById("lblCodianLO").innerHTML = Family4; 
            }
            else if (id == 105) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + "25" + ".gif";
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family5; 
            }
            else if (id == 100) {
                document.getElementById("imgVideoDisplay").style.display = 'none';
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Defaultfamily; 

            }
            else {
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgLayoutMapping6").style.display = 'none';
                document.getElementById("imgLayoutMapping7").style.display = 'none';
                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = ""; 
                document.getElementById("hdnFamilyLayout").value = 0;
            }
        }
	cancelthis();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
	//window.resizeTo(750,450); //FB Case 536 Saima
}

function CheckDefault(obj)
{
//    var textToChange = obj.parentNode.parentNode.innerHTML;
//    alert(textToChange);
//    if (obj.checked)
//        textToChange = textToChange.replace(/<TD style="/g, '<TD style="text-decoration:line-through ');

//    obj.parentNode.parentNode.innerHTML = textToChange;
}

function viewLayout()
{
    var obj = document.getElementById("<%=lstRoomLayout.ClientID%>");
    if (obj.disabled == "disabled")
        alert(ConfRoomdefined);
    else
        if (obj.selectedIndex > 0 && obj[obj.selectedIndex].text != '[None]') // Code added for FB 1176
        {
            // url = "image/room/" + obj[obj.selectedIndex].text + ".jpg";// Code added for FB 1176 // Image Project
            url = "../en/image/room/" + obj[obj.selectedIndex].text;// Image Project //FB 1830
            window.open(url, "RoomLayout", "width=750,height=400,resizable=yes,scrollbars=yes,status=no");
        }
        else
            alert(ConfRoomLayout);
        
}
function viewendpoint(val)
{
	
	url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else { // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
    	}
	}
}

function viewconflict(id, confTime, confDate, confDuration, confTZ, ConferenceName, confID)
{
    //alert(id + " : " + confTime + " : " + confDate + " : " + confDuration);
    var rms = document.getElementById("selectedloc").value;
	url = "preconferenceroomchart.asp?wintype=ifr&cid=" + confID + "&cno=" + id +
		"&date=" + confDate + "&time=" + confTime + "&timeZone=" + confTZ +
		"&duration=" + confDuration + "&r=" + rms + "&n=" + ConferenceName;
//alert(url);
	wincrm = window.open(url,'confroomchart','status=no,width=1,height=1,top=30,left=0,scrollbars=yes,resizable=yes')
	if (wincrm)
		wincrm.focus();
	else
		alert(EN_132);
}

/*
function formatTime(timeText, regText)
{
   if("<%=Session["timeFormat"]%>" == "1")
   {    
    //ZD 100284 
		var tText = document.getElementById(timeText);
        var isColon = true;
        var timeVal = tText.value;
        
	    if(timeVal.length < 8)
        {
            if(timeVal.indexOf(':') < 0)
                isColon = false;
            
            if(isColon == true)
            {
                var timSplit = timeVal.split(":");
                
                if(timSplit[0].length == 1)
                    timeVal = "0" + timSplit[0] + ":" + timSplit[1];
                
                timeVal = timeVal.replace('a','A').replace('p','P').replace('m','M');
                
                if(timeVal.indexOf(' AM') < 0)
                    timeVal = timeVal.replace('AM',' AM');
                    
                if(timeVal.indexOf(' PM') < 0)
                    timeVal = timeVal.replace('PM',' PM');               
                
            }
        }

        if (timeVal.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1 && isColon == true)
        {
            var a_p = "";
            //FB 2614
            var t = new Date();
            //var t = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>'));
            
            var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + timeVal); //document.getElementById(timeText).value);
            //var d = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + timeVal);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            //document.getElementById(timeText).focus();
            return false;
       }
   }
    return true;
}
*/
//FB 1716

function ChangeDuration()
{ 
    if ('<%=isEditMode%>' == "1" )    
    {        
        var duration = document.getElementById("hdnDuration").value;
        var chgVar =  document.getElementById("hdnChange").value;
              
        if(duration != '')
        {        
            var durArr = duration.split("&");
            var changeTime;
            
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);
            
            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);
            
            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);
           
            switch(chgVar)
            {
                case "ST":  
                    setupTime = setCDuration(setupTime,durArr[1]);                    
                    document.getElementById("SetupDate").value = getCDate(setupTime);                    
                    document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                    var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
                    + document.getElementById("SetupTime_Text").value);
                    
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "SU":
                
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "TD":
                   endTime = setCDuration(endTime,durArr[2]);
                   document.getElementById("confEndDate").value = getCDate(endTime);                   
                   document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;               
            }
        }
    }  
}

//FB 1716
function setCDuration(setupTime, dura)
{
    var chTime = new Date(setupTime);
    var d = 0;
    var min = 0;
    var hh = 0, dur = 0;
    if(dura > 60)
    {   
        hh = dura / 60;
        min = dura % 60;                        
        if(min > 0)
            hh = Math.floor(hh) + 1;                        
     
        for(d = 1; d <= hh ; d++)
        {
            if(min > 0 && d == hh)
                dur = dura % 60;
            else
                dur = 60;
                
             chTime.setMinutes(chTime.getMinutes() + dur);   
        }
    }
    else //if(dura > 0)        //FB 2634
        chTime.setTime(chTime.getTime() + (dura * 60 * 1000));
    //else
      //  chTime;
        
   return chTime
}
//FB 1716

function getCDate(changeTime)
{
    var strDate;
    var month,date;
    month = changeTime.getMonth() + 1;
    date = changeTime.getDate();
    
    if(eval(changeTime.getMonth() + 1) <10)
        month = "0" + (changeTime.getMonth() + 1);
    
    if(eval(date) <10)
        date = "0" + changeTime.getDate();
       
    if('<%=format%>' == 'MM/dd/yyyy')    
        strDate = month + "/" + date + "/" + changeTime.getFullYear();
    else
        strDate =  date + "/" + month + "/" + changeTime.getFullYear();
    
    return strDate;
}
//FB 1716

function getCTime(changeTime)
{
    //FB 2614
    var hh = changeTime.getHours(); //parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);   
    
    if("<%=Session["timeFormat"]%>" == "1") //FB 2108
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
             
    if (hh < 10)
        hh = "0" + hh;
    //FB 2614
    var mm = changeTime.getMinutes();//parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
    if (mm < 10)
        mm = "0" + mm;
    //var ap = changeTime.toLocaleTimeString().toString().split(" ")[1];
    var ap = changeTime.format("tt"); //FB 2108
    var evntime = parseInt(hh,10);
    
    if(evntime < 12 && ap == "PM")
       evntime = evntime + 12;
       
    var tiFormat =  "<%=Session["timeFormat"]%>" ;

    if(tiFormat == '0')
    {
        if(ap == "AM")                        
            strTime = (hh == "12") ? "00:" + mm : hh + ":" + mm ;
        else
        {
            if(evntime == "24")
                strTime = (hh == "12") ? "00:" + mm : evntime + ":" + mm;
            else
                strTime =  evntime + ":" + mm;
        }   
    }
    else
        strTime = hh + ":" + mm + " " + ap;
        
   return strTime;
}
//FB 2634
function fnEndDateValidation()
{

    ChangeTimeFormat("D"); //FB 2588
    var sDate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value); //FB 2588
    var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value); //FB 2588
    //ZD 103624
    if(!validateStartEndDate(sDate,eDate,0,1))
        return false;

    if ( (sDate >= eDate) && (document.getElementById("hdnStartTime").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
    && (document.getElementById("hdnEndTime").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
        if (sDate == eDate)
        {
             if (sDate > eDate)
                alert(EndTimeChangeAlert);
            else if (sDate == eDate)
                alert(EndTimeChangeAlert);            
        }
        else
            alert(EndDateChangeAlert);
            
         ChangeEndDate();
      }
}

//FB 2588
function ChangeTimeFormat() {

    var args = ChangeTimeFormat.arguments;
    var stime = document.getElementById("confStartTime_Text").value;
    var etime = document.getElementById("confEndTime_Text").value;
    var hdnsTime = document.getElementById("hdnStartTime");
    var hdneTime = document.getElementById("hdnEndTime");

    if ('<%=Session["timeFormat"]%>' == "2") {
        if (args[0] == "D") {
            stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
            
        }
        else {
            if (stime.indexOf("Z") < 0)
                stime = stime.replace(':', '') + "Z";
            if (etime.indexOf("Z") < 0)
                etime = etime.replace(':', '') + "Z";
        }
    }

    hdnsTime.value = stime
    hdneTime.value = etime
}
//ZD 103624
function validateStartEndDate(startDateTime,endDateTime, isStartValidation, isEndValidation)
{
    if(isStartValidation == 1)
    {
        if(startDateTime == "Invalid Date" && !formatTimeNew('confStartTime_Text','regConfStartTime',"<%=Session["timeFormat"]%>"))
        {
            document.getElementById("regConfStartTime").style.display = '';
            document.getElementById("confStartTime_Text").focus();
            return false;    
        }
        else if (startDateTime == "Invalid Date" || isNaN(startDateTime))//ZD 104163
        {
            document.getElementById("regConfStartDate").style.display = '';
            document.getElementById("confStartDate").focus();
            return false;            
        }
        else        
            document.getElementById("regConfStartDate").style.display = 'None';
            
    }

    if(isEndValidation == 1)
    {
        if(isNaN(endDateTime) && !formatTimeNew('confEndTime_Text','regEndTime',"<%=Session["timeFormat"]%>"))
        {
            document.getElementById("regEndTime").style.display = '';
            document.getElementById("confEndTime_Text").focus();
            return false;  
        }
        else if(isNaN(endDateTime))
        {
            document.getElementById("regEndDate").style.display = '';
            document.getElementById("confEndDate").focus();
            return false;  
        }
        else
            document.getElementById("regEndDate").style.display = 'None';
    }

    return true;
}
//FB 2634
function ChangeEndDate()
{

    setTimeout("fnUpdateStartList();", 100);// ZD 101444
    setTimeout("fnUpdateEndList();", 100);
    // FB 2692 Starts
//    if ('<%=isEditMode%>' == "1" && document.getElementById("chkRecurrence").checked == false) //ZD 102796 start
//    {
//        var sDate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("confStartTime_Text").value);
//        var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("confEndTime_Text").value);
//        
//         //ZD 103624
//        if(!validateStartEndDate(sDate,eDate,1,1))
//            return false;

//        if(sDate >= eDate)
//        {
//            document.getElementById("errLabel").innerHTML = "Start time should be less than end time";
//            window.scrollTo(0,0);
//            return false;
//        }
//        else
//        {
//            var nDate = eDate - sDate;
//            if(nDate < 900000)
//            {
//            document.getElementById("errLabel").innerHTML = "Minimum duration should be 15 minutes";
//            window.scrollTo(0,0);
//            return false;
//            }
//        }
//        document.getElementById("errLabel").innerHTML = "";
//        return false;
//    }
//    else if('<%=isEditMode%>' == "1")
//        return false; //ZD 102796 End
    // FB 2692 Ends
     ChangeTimeFormat("D"); //FB 2588
    var args = ChangeEndDate.arguments;
    var startDateTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value); //FB 2588
    var endDateTime = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value);
    var skipcheck = 0;
    //ZD 103624
    if(!validateStartEndDate(startDateTime,endDateTime,1,1))
        return false;

    if(args[0] == "ET")
    {
        if(startDateTime >= endDateTime)
            skipcheck = 0;
        else
            skipcheck = 1;
    }
    
    if('<%=Session["DefaultConfDuration"]%>' != null && skipcheck == 0)
    {
        var ConfDuration = '<%=Session["DefaultConfDuration"]%>';
        
//        if ('<%=isEditMode%>' == "1" ) //ZD 102796 start
//        {
//            var duration = document.getElementById("hdnDuration").value;
//            if(duration != "")
//                ConfDuration = duration            
//        } //ZD 102796 End
        
        var ConfHours = parseInt(ConfDuration, 10) / 60; // ZD 101722
        var ConfMinutes = parseInt(ConfDuration, 10) % 60; // ZD 101722
        ConfMinutes = ConfMinutes + startDateTime.getMinutes();
        if(ConfMinutes > 59)
        {
           ConfMinutes = ConfMinutes - 60;
           ConfHours = ConfHours + 1;
        }
        startDateTime.setHours(startDateTime.getHours() + ConfHours);
        startDateTime.setMinutes(ConfMinutes);
    }
    else 
        startDateTime = new Date(endDateTime);

    var month,date;
    month = startDateTime.getMonth() + 1;
    date = startDateTime.getDate();
    
    if(eval(month) < 10)
        month = "0" + month;
    
    if(eval(date) < 10)
        date = "0" + startDateTime.getDate();
    
    if('<%=format%>' == 'MM/dd/yyyy')
        document.getElementById("confEndDate").value = month + "/" + date + "/" + startDateTime.getFullYear();
    else
        document.getElementById("confEndDate").value =  date + "/" + month + "/" + startDateTime.getFullYear();
        
    var hh = startDateTime.getHours();
        
    if('<%=Session["timeFormat"]%>' == "1")
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
            
    if (hh < 10)
        hh = "0" + hh;
    var mm = startDateTime.getMinutes()
    if (mm < 10)
        mm = "0" + mm;
    var ap = startDateTime.format("tt");
    var evntime = parseInt(hh,10);
    
     if(evntime < 12 && ap == "PM")
        evntime = evntime + 12;
               
    if('<%=Session["timeFormat"]%>' == "0" || '<%=Session["timeFormat"]%>' == "2") //FB 2588
    {
        if(ap == "AM")
        {
            if(hh == "12")
                document.getElementById("confEndTime_Text").value = "00:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
        }
        else
        {
            if(evntime == "24")
                document.getElementById("confEndTime_Text").value = "12:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
        }
    }
    else
        document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        
    //FB 2588
    ChangeTimeFormat("O");
    document.getElementById("confEndTime_Text").value = document.getElementById("hdnEndTime").value;

    //ZD 103624
    var eDtTime = new Date((GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("confEndTime_Text").value))
    if(eDtTime != "Invalid Date")
    {
        ValidatorEnable(document.getElementById("regEndDate"), false);    
        ValidatorEnable(document.getElementById("regEndTime"), false);    
    }
    return true;
}


//Code added for FB 1308 start 

function DuraionCalculation()
{
    var confenddate = '';
    var confstdate = '';
    var durationMin = '';
    var totalMinutes;
    var totalDays;
    var totalHours;
    var lblconfduration = document.getElementById('<%=lblConfDuration.ClientID%>');
    
    lblconfduration.innerText = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value,'<%=format%>');
    confstdate = GetDefaultDate(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value,'<%=format%>');
    
    var durationMin = parseInt(Date.parse(confenddate) - Date.parse(confstdate), 10) /  1000  ; // ZD 101722
    
    if(durationMin != '')
    {
        totalDays = Math.floor(durationMin / (24 * 60 * 60));
        totalHours = Math.floor((durationMin - (totalDays * 24 * 60 * 60)) / (60 * 60));
        totalMinutes = Math.floor((durationMin - (totalDays * 24 * 60 * 60) -  (totalHours * 60 * 60)) / 60);
    }
    
    if (Math.floor(durationMin) < 0)
        lblconfduration.innerText = "Invalid duration";
    if (totalDays > 0)
        lblconfduration.innerText = totalDays + " day(s) ";
    if (totalHours > 0)
        lblconfduration.innerText += totalHours + " hrs "; //ZD 100528
     if (totalMinutes > 0)
        lblconfduration.innerText += totalMinutes + " mins"; //ZD 100528
}

function EndDateValidationold(frm,confstdate,confenddate,msgchk)
{
    var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value);
    var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
    
    //FB 1715
    if ( (sDate >= eDate) && (document.getElementById("confStartTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
    && (document.getElementById("confEndTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
         if(msgchk == 'S')
        {
            if (Date.parse(confstdate) == Date.parse(confenddate))        
            {
                if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert(EndTimeChangeAlert);  
               else if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) == Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert(EndTimeChangeAlert);              
            }    
            else
                alert(EndDateChangeAlert);
        }
            
         ChangeEndDateTime(confstdate,confenddate); //MOJ Fix
      }
     
      if(!document.getElementById("chkStartNow").checked) //buffer zone start
      {
        var chkbuffer = document.getElementById("chkEnableBuffer");
        if (("<%=enableBufferZone%>" == "1") && (chkbuffer.checked))
 /***  SJV BufferZone Fix ****/
        { 
              if ((setupdate < sDate) || (setupdate >= tDate) ||(setupdate > eDate) || (tDate > eDate) || (tDate < setupdate))
              {
                  if( (setupdate < sDate) || (setupdate >= tDate) || (setupdate > eDate) || (tDate < setupdate))
                    {
                        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                        document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;//Edited For FF...
                        document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                        document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;//Edited For FF...
                    }
              }
              if(tDate < setupdate)
               {
                    document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                    document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
               } 
              if(tDate > eDate)
              {
                   document.getElementById("confEndDate").value = document.getElementById("TearDownDate").value;
                   document.getElementById("confEndTime_Text").value = document.getElementById("TeardownTime_Text").value;
              }
             if(setupdate >= tDate)
              {
                   document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                   document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                   document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                   document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
              }
         }
         else
         {
            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value; //Edited for FF
                        
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;//Edited for FF
         }   
      }//buffer zone end
      DuraionCalculation();  
}

//Code added for FB 1308 end 

//Method added for MOJ
function ChangeEndDateTime(confstdate,confenddate)
{
      document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
         
         /*** Code changed for FB 1454 *****/
        //var endTime = new Date(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
        var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);
         /*** Code changed for FB 1454 *****/
        
        //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
        var apBfr = endTime.format("tt");      //FB 2108
                
        /*** Code added for FB 1454 *****/
        //FB 2501 - Start
        //endTime.setHours(endTime.getHours() + 1);
        if("<%=Session["DefaultConfDuration"]%>" != null)
        {
            var ConfDuration = "<%=Session["DefaultConfDuration"]%>";
            var ConfHours = parseInt(ConfDuration, 10) / 60; // ZD 101722
            var ConfMinutes = parseInt(ConfDuration, 10) % 60; // ZD 101722
            ConfMinutes = ConfMinutes + endTime.getMinutes();
            if(ConfMinutes > 59)
            {
               ConfMinutes = ConfMinutes - 60;
               ConfHours = ConfHours + 1;
            }
            endTime.setHours(endTime.getHours() + ConfHours);
            endTime.setMinutes(ConfMinutes);
        }      
               
        //Added for FB 1425 QA Bug START
            //if('<%=client.ToString().ToUpper()%>' =="MOJ")
            //endTime.setMinutes(endTime.getMinutes() - 45);
        //Added for FB 1425 QA Bug End
        //FB 2501 - End        
        
        var month,date;
        month = endTime.getMonth() + 1;
        date = endTime.getDate();
        
        if(eval(endTime.getMonth() + 1) <10)
            month = "0" + (endTime.getMonth() + 1);
        
        if(eval(date) <10)
            date = "0" + endTime.getDate();
        
                   
        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
           
        if('<%=format%>' == 'MM/dd/yyyy')
        {
            document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime.getFullYear();
        }
        else
        {
            document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime.getFullYear();
        }
        
        /*** Code added for FB 1454 *****/
        //FB 2614
        //var hh = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
        var hh = endTime.getHours();
        
        if("<%=Session["timeFormat"]%>" == "1") //FB 2108
        {
            if(hh >= 12)
                hh = hh - 12;
                
            if(hh == 0)
                hh = 12
        }
                
        if (hh < 10)
            hh = "0" + hh;
        //FB 2614
        //var mm = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
        var mm = endTime.getMinutes()
        if (mm < 10)
            mm = "0" + mm;
        //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
        var ap = endTime.format("tt");    //FB 2108  
        
        /*** Code changed for FB 1426 *****/
        //document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;


        var evntime = parseInt(hh,10);
        
         if(evntime < 12 && ap == "PM")
            evntime = evntime + 12;
            
                   
        if("<%=Session["timeFormat"]%>" == "0")
        {
            if(ap == "AM")
            {
                if(hh == "12")
                    document.getElementById("confEndTime_Text").value = "00:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                
            }
            else
            {
                if(evntime == "24")
                    document.getElementById("confEndTime_Text").value = "12:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
            }
                
        }
        else
        {
            document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        }
}

function ChangeStartDate(frm)
{//FB 2634
/*

//    //alert("in ChangeStartDate");
//    if (Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value) <= Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) )
//    {
//        alert("End Date/Time should be greater than Start Date/Time.");
//        document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
//        //document.getElementById("confStartTime").value = document.getElementById("confEndTime").value;
//        return false;
//    }
//    else

    if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
    {
        //Code added for FB 1073 -- Start
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
       
        //FB 1728
        //if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))-- FB 1073
//         if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value))
//        {
//            document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
//            if (frm == "0") 
//            {
//                alert("Invalid End Date or Time. It should be greater than time current time in user preferred timezone.");
//                DataLoading(0);
//                return false;
//            }
//        }
        
        //if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value) )
        //Code added & commented for FB 1308 start    
        EndDateValidation(frm,confstdate,confenddate,'S');

    }
     //FB 1716
     ChangeDuration(); 
    */
    return true;
    
}

function CheckDuration()
{
    //alert("here in check duration");
    if (document.getElementById("Recur").value == "")
    if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))
    {
        alert(InvalidDuration1);
        return false;
    }
}

function SavePassword()
{
    //alert(document.getElementById("<%=ConferencePassword.ClientID %>").value);
    document.getElementById("<%=confPassword.ClientID %>").value = document.getElementById("<%=ConferencePassword.ClientID %>").value;
}

function ChangePublic()
{
    var t;
	//FB 2274 Strats   
    var dynInvite_Session;
    if(document.getElementById("hdnCrossdynInvite").value != null)
        dynInvite_Session = document.getElementById("hdnCrossdynInvite").value;
    else 
        dynInvite_Session = "<%=Session["dynInvite"] %>";

    //FB 2274 Ends
    if(document.getElementById("chkPublic") != null)
    {
        if ((document.getElementById("chkPublic").checked) && (dynInvite_Session == "1") ) //FB 2274   
        {
            document.getElementById("openRegister").style.display = "";
        }
        else
        {
            document.getElementById("openRegister").style.display ="none";
        }
    }
}

//FB 2634

//FB 2870 Start
function ChangeNumeric()
{
    if(document.getElementById("ChkEnableNumericID") != null)
    {
        if ((document.getElementById("ChkEnableNumericID").checked)) 
        {
            document.getElementById("DivNumeridID").style.display = "";
            ValidatorEnable(document.getElementById("ReqNumeridID"), true);
        }
        else
        {
            document.getElementById("DivNumeridID").style.display ="none";
            ValidatorEnable(document.getElementById("ReqNumeridID"), false);
        }
    }
}
//FB 2870 End
//ZD 103550 - Start
function fnBJNMeetingOptSelection() 
{
		//ZD 104116 - Start
        var BJNSelectOption;
        var BJNIntegration;
        var BJNDisplay;
        BJNSelectOption = "<%=Session["BJNSelectOption"] %>";
        BJNIntegration = "<%=Session["EnableBJNIntegration"] %>";
        BJNDisplay = "<%=Session["BJNDisplay"] %>";

        if (document.getElementById("chkBJNMeetingID") != null) {
            if ((document.getElementById("chkBJNMeetingID").checked)) {
                document.getElementById("hdnChangeBJNVal").value = "1";//ZD 104116
                if(BJNSelectOption == 1)
                    document.getElementById("DivBJNMeetingID").style.display = "";                
                else
                    document.getElementById("DivBJNMeetingID").style.display = "none"; 

                
                    if(document.getElementById("trVMR") != null)
                        document.getElementById("trVMR").style.display ="none";

                    if(document.getElementById("trPCConf") != null)
                        document.getElementById("trPCConf").style.display = "none";
                
            }
            else {
                document.getElementById("hdnChangeBJNVal").value = "0";
                document.getElementById("DivBJNMeetingID").style.display = "none";                  
                if(BJNIntegration == 1 && BJNDisplay == 0)
                {
                    if(document.getElementById("trVMR") != null)
                        document.getElementById("trVMR").style.display ="none";

                    if(document.getElementById("trPCConf") != null)
                        document.getElementById("trPCConf").style.display = "none";
                }
                else
                {
                if(('<%=Session["ShowHideVMR"]%>' == 1)&&('<%=Session["EnablePersonaVMR"]%>' == 1 ||'<%=Session["EnableRoomVMR"]%>' == 1 ||'<%=Session["EnableExternalVMR"]%>' == 1)) //ZD 104307
                    if(document.getElementById("trVMR") != null)
                        document.getElementById("trVMR").style.display =""; 
                    
                    if (document.getElementById("trPCConf") != null && '<%=Session["EnablePCUser"]%>' == 1) 
                        document.getElementById("trPCConf").style.display = "";
                } 
                //ZD 104116 - End                
            }
        }    

 }
//ZD 103550 - End
// FB 2620 Starts
function fnShowHideAVforVMR()
{
 var isPCCOnf,lstVMR; //FB 3022
    if(document.getElementById("lstVMR") != null)
    {
        var e =document.getElementById("lstVMR");
        lstVMR =  e.options[e.selectedIndex].value;
        //lstVMR = document.getElementById("lstVMR").selectedIndex;// FB 2620
    }
    var TopMenun3 = document.getElementById("TopMenun3");
    
     if(document.getElementById("chkPCConf") != null) //FB 2819
        isPCCOnf = document.getElementById("chkPCConf").checked;

   //ZD 100522 Starts
    if(lstVMR >0)
    {   
        if(document.getElementById("trPCConf") != null)
        {
            if(document.getElementById("chkPCConf") != null)
                document.getElementById("chkPCConf").checked =false;
            document.getElementById("trPCConf").style.display ="none";
        }
        //ZD 103550
        if('<%=Session["EnableBJNIntegration"]%>' == 1)
        {
            if(document.getElementById('trBJNMeeting') != null)
                document.getElementById('trBJNMeeting').style.display = 'None';
        }

    }else
    {
         if(document.getElementById("trPCConf") != null) 
            if('<%=Session["EnablePCUser"]%>' == 1)  //ZD 100873 //ZD 103550
            {
                if(document.getElementById("chkBJNMeetingID") == null) 
                    document.getElementById("trPCConf").style.display ="";
                else if(document.getElementById("chkBJNMeetingID").checked == false) 
                    document.getElementById("trPCConf").style.display ="";
                else
                    document.getElementById("trPCConf").style.display = 'None';  
            }
        //ZD 103550
        if('<%=Session["EnableBJNIntegration"]%>' == 1)
        {
            if(document.getElementById('trBJNMeeting') != null)
                document.getElementById('trBJNMeeting').style.display = '';
        }
    }
   //ZD 100522 Ends
    
    if(lstVMR && (lstVMR ==1 || lstVMR ==3)) //ZD 100522
    {
        //if (lstVMR >0) // FB 2620
        //{
            if(TopMenun3)
            {
                document.getElementById("TopMenun3").innerHTML =  ""; // ZD 100369 508 Issues
            
                if(document.getElementById('dgRooms'))
                    document.getElementById('dgRooms').innerHTML="";
                if(document.getElementById('dgUsers'))
                    document.getElementById('dgUsers').innerHTML="";
            }
        //FB 3044 Starts
      
        if(document.getElementById("chkMCUConnect")!= null)
            document.getElementById("chkMCUConnect").checked = false;
        if(document.getElementById("MCUConnectDisplayRow")!= null)
            document.getElementById("MCUConnectDisplayRow").style.display = "none";
        if(document.getElementById("MCUConnectRow")!= null) 
            document.getElementById("MCUConnectRow").style.display = "none";
        //FB 3044 Ends
    }
    else if(isPCCOnf) //FB 2819 Starts
    {
      if(document.getElementById("trVMR") != null)
         document.getElementById("trVMR").style.display ="none";

         //ZD 103550
        if('<%=Session["EnableBJNIntegration"]%>' == 1)
        {
            if(document.getElementById('trBJNMeeting') != null)
                document.getElementById('trBJNMeeting').style.display = 'none';
        }
        
        if(TopMenun3)
        {
            document.getElementById("TopMenun3").innerHTML =  ""; // ZD 100369 508 Issues
            
            if(document.getElementById('dgRooms'))
                document.getElementById('dgRooms').innerHTML="";
            if(document.getElementById('dgUsers'))
                document.getElementById('dgUsers').innerHTML="";
        }
        if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = 'None';

        if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = 'None';
            
        if(document.getElementById('trVMRcall') != null)
            document.getElementById('trVMRcall').style.display = 'None';
          
          //FB 3044 Starts
        if(document.getElementById("chkMCUConnect")!= null)
            document.getElementById("chkMCUConnect").checked = false;
        if(document.getElementById("MCUConnectDisplayRow")!= null) 
            document.getElementById("MCUConnectDisplayRow").style.display = "none";
        if(document.getElementById("MCUConnectRow")!= null) 
            document.getElementById("MCUConnectRow").style.display = "none";
            //FB 3044 Ends
    } //FB 2819 Ends
    else
    {
          if(document.getElementById("trVMR") != null)
          //ZD 100707 Start
            if(('<%=Session["ShowHideVMR"]%>' == 1)&&('<%=Session["EnablePersonaVMR"]%>' == 1 ||'<%=Session["EnableRoomVMR"]%>' == 1 ||'<%=Session["EnableExternalVMR"]%>' == 1))
             {
                document.getElementById("trVMR").style.display ="";
             }
             //ZD 100707 End
             if('<%=Session["EnableStartMode"]%>' ==1)
             {
          if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = '';

        if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = '';
         }
        if(document.getElementById('trVMRcall') != null)
            //ZD 100707 Start
            if(('<%=Session["ShowHideVMR"]%>' == 1)&&('<%=Session["EnablePersonaVMR"]%>' == 1 ||'<%=Session["EnableRoomVMR"]%>' == 1 ||'<%=Session["EnableExternalVMR"]%>' == 1))
            {
                document.getElementById('trVMRcall').style.display = '';     
            } 
            //ZD 100707 End
         //FB 2819 Ends  
        if (TopMenun3)
        {
           if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
                document.getElementById("TopMenun3").innerHTML = "<TABLE class='tab TopMenu_4' border=0 cellSpacing=0 cellPadding=0 width='100%'><TBODY><TR><TD><a class='TopMenu_1 tab TopMenu_3' href='javascript:if (CheckFiles()) __doPostBack(\"TopMenu\",\"3\")' style='border-style:none;font-size:1em;'><div align='center' style='width:123'><asp:Literal Text='<%$ Resources:WebResources, MenuAudioSettings%>' runat='server'></asp:Literal></div></a></TD></TR></TBODY></TABLE>";
           else
           {
                document.getElementById("TopMenun3").innerHTML = "<TABLE class='tab TopMenu_4' border=0 cellSpacing=0 cellPadding=0 width='100%'><TBODY><TR><TD><a class='TopMenu_1 tab TopMenu_3' href='javascript:if (CheckFiles()) __doPostBack(\"TopMenu\",\"3\")'><div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><asp:Literal Text='<%$ Resources:WebResources, AudioVideoSettings%>' runat='server'></asp:Literal></div></a></TD></TR></TBODY></TABLE>";
                document.getElementById("TopMenun3").style.display ="block";  //FB 3022
           }
        }
    } 
   
}


//VMR


function changeVMR()
{ 
 var vmrParty = document.getElementById("isVMR")// FB 2620
 
 var iVMR = "1";
 var ShowVMRPIN="0"; //ZD 100522

if(document.getElementById("lstVMR") != null)
{
   //ZD 100753 Starts
    var e =document.getElementById("lstVMR");
    var VMRType=  e.options[e.selectedIndex].value;

    //ALLDEV-826 Starts
    var reqValHostPwd = document.getElementById("reqValHostPwd");
    if (document.getElementById('lstVMR') != null) {
        if (document.getElementById('lstVMR').value == "2") {                    
            ValidatorEnable(reqValHostPwd, false);
        }
    }
    //ALLDEV-826 Ends

    document.getElementById("hdnVMRId").value = VMRType;//ZD 101561
    
     if(document.getElementById("hdnCrossEnableConfPassword").value != null)
 	    ConfPass = document.getElementById("hdnCrossEnableConfPassword").value;
     else
 	    ConfPass = "<%=Session["EnableConfPassword"] %>";

    //ALLDEV-826 Starts
    if(document.getElementById("hdnCrossEnableHostGuestPwd").value != null)
 	    HostGuestPass = document.getElementById("hdnCrossEnableHostGuestPwd").value;
    else
 	    HostGuestPass = "<%=Session["EnableHostGuestPwd"] %>";
    //ALLDEV-826 Ends

     if(document.getElementById("hdnVMRPINChange").value != null) //ZD 100522
 	    ShowVMRPIN = document.getElementById("hdnVMRPINChange").value;
     else
 	    ShowVMRPIN = "<%=VMRPINChange %>";
 	    
    if(VMRType == "2")
    {
        if(document.getElementById("trConfPass1") != null)
        document.getElementById("trConfPass1").style.display = 'None';
        if(document.getElementById('trConfPass') != null)
        document.getElementById('trConfPass').style.display = 'None';
        //ALLDEV-826 Starts
        if(document.getElementById("trHostPwd") != null)
        document.getElementById("trHostPwd").style.display = 'None';
        if(document.getElementById("trCfmHostPwd") != null)
        document.getElementById("trCfmHostPwd").style.display = 'None';
        //ALLDEV-826 Ends
    }else
    {
        if( ConfPass =="1")
        {
            if(document.getElementById("trConfPass1") != null)
            document.getElementById("trConfPass1").style.display = '';
            if(document.getElementById('trConfPass') != null)
            document.getElementById('trConfPass').style.display = '';
        }
        //ALLDEV-826 Starts
        else if(HostGuestPass == "1")
        {
            if(document.getElementById("trConfPass1") != null)
                document.getElementById("trConfPass1").style.display = '';
            if(document.getElementById('trConfPass') != null)
                document.getElementById('trConfPass').style.display = '';
            if(document.getElementById("trHostPwd") != null)
                document.getElementById("trHostPwd").style.display = '';
            if(document.getElementById("trCfmHostPwd") != null)
                document.getElementById("trCfmHostPwd").style.display = '';
        }
        //ALLDEV-826 Ends
    }
    //ZD 100753 Ends
    if ((document.getElementById("lstVMR").selectedIndex) >0) // FB 2620
    {
        document.getElementById("divbridge").style.display = "None";
        if(document.getElementById("divChangeVMRPIN") != null)
            document.getElementById("divChangeVMRPIN").style.display = "None"; //ZD 100522
	    if (VMRType  == 1)//FB 2448 //ZD 100753   
            document.getElementById("divbridge").style.display = "block";
        if(VMRType == 2) //ZD 100522
        { 
            if(ShowVMRPIN == "1")
                if(document.getElementById("divChangeVMRPIN") != null)
                    document.getElementById("divChangeVMRPIN").style.display = "block";
        }
        vmrParty.value = "1"
        // FB 2501 Starts
        if(document.getElementById('ConferencePassword') != null )
        {
          if(document.getElementById('ConferencePassword').value == "")
            num_gen();
        }
        // FB 2501 Ends
        //FB 2634
        if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = 'None';

        if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = 'None';
        //ZD 103550
        if('<%=Session["EnableBJNIntegration"]%>' == 1)
        {
            if(document.getElementById('trBJNMeeting') != null)
                document.getElementById('trBJNMeeting').style.display = 'None';
        }
    }
    else
    {
        document.getElementById("divbridge").style.display ="none";
        if(document.getElementById("divChangeVMRPIN") != null)
            document.getElementById("divChangeVMRPIN").style.display = "none"; //ZD 100522
        vmrParty.value = "0"
        // FB 2501 Starts
        if(document.getElementById('ConferencePassword') != null )
        {
            //document.getElementById('ConferencePassword').value = ""; // FB 2717
            //document.getElementById('ConferencePassword2').value = "";// FB 2717
            SavePassword();
        }
        // FB 2501 Ends
        
        //FB 2634
        //trStartMode.style.display = '';
        //FB 2641 start
        if('<%=Session["EnableStartMode"]%>' ==1)
            document.getElementById("trStartMode").style.display='';
        else
           document.getElementById("trStartMode").style.display='None';
            //FB 2641 End
        if(document.getElementById("trStartMode1") != null)
            document.getElementById("trStartMode1").style.display = '';
        //ZD 103550
        if('<%=Session["EnableBJNIntegration"]%>' == 1)
        {
            if(document.getElementById('trBJNMeeting') != null)
                document.getElementById('trBJNMeeting').style.display = '';
        }
     }
     
     var confType =  document.getElementById("lstConferenceType").value;
     if(confType == "7" || confType == "4" || confType == "8")
      { 
        document.getElementById("lstVMR").selectedIndex = 0;
        if(document.getElementById("trVMR") != null)
            document.getElementById("trVMR").style.display = "none"; 

        if(document.getElementById("trBJNMeeting") != null)//ZD 104021
            document.getElementById("trBJNMeeting").style.display = "none"; 

     	//if(confType == "7" || confType == "4" || confType == "8")
		//{
			if(document.getElementById("trStartMode") != null)
	            document.getElementById("trStartMode").style.display = "none"; 
	            
	       if(document.getElementById("trPCConf") != null) //FB 2819
	        {
	            if(document.getElementById("chkPCConf") != null) //FB 3022
                    document.getElementById("chkPCConf").checked =false;
                document.getElementById("trPCConf").style.display ="none";
                
            }
		//}
      }
      
      //FB 2819 Starts
       if(document.getElementById("chkPCConf") != null) 
        var isPCCOnf = document.getElementById("chkPCConf").checked;
        if(isPCCOnf)
        {
            if(document.getElementById("trStartMode") != null)
                document.getElementById("trStartMode").style.display = 'None';

            if(document.getElementById('trStartMode1') != null)
                document.getElementById('trStartMode1').style.display = 'None';

            //ZD 104021
            if('<%=Session["EnableBJNIntegration"]%>' == 1)
            {
                if(document.getElementById('trBJNMeeting') != null)
                    document.getElementById('trBJNMeeting').style.display = 'None';
            }
        }
      //FB 2819 Ends
  }

return true;
}

function fnVMR()
{
 var iVMR; var ShowVMRPIN="0"; //ZD 100522
 
     if(document.getElementById("hdnVMRPINChange").value != null) //ZD 100522
 	    ShowVMRPIN = document.getElementById("hdnVMRPINChange").value;
     else
 	    ShowVMRPIN = "<%=VMRPINChange %>";

var vmrParty = document.getElementById("isVMR");
if(document.getElementById("lstVMR") != null)
{
     //ZD 100753 Starts
    var e =document.getElementById("lstVMR");
    var VMRType=  e.options[e.selectedIndex].value;
    var ConfPass="";
    var HostGuestPass = ""; //ALLDEV-826
    document.getElementById("hdnVMRId").value = VMRType;//ZD 101561
    
    if(document.getElementById("hdnCrossEnableConfPassword").value != null)
 	    ConfPass = document.getElementById("hdnCrossEnableConfPassword").value;
     else
 	    ConfPass = "<%=Session["EnableConfPassword"] %>";
 	    
    //ALLDEV-826 Starts
    if(document.getElementById("hdnCrossEnableHostGuestPwd").value != null)
 	    HostGuestPass = document.getElementById("hdnCrossEnableHostGuestPwd").value;
    else
 	    HostGuestPass = "<%=Session["EnableHostGuestPwd"] %>";
    //ALLDEV-826 Ends

    if(VMRType == "2")
    {
        if(document.getElementById("trConfPass1") != null)
        document.getElementById("trConfPass1").style.display = 'None';
        if(document.getElementById('trConfPass') != null)
        document.getElementById('trConfPass').style.display = 'None';
    }else
    {
        if( ConfPass =="1")
        {
            if(document.getElementById("trConfPass1") != null)
            document.getElementById("trConfPass1").style.display = '';
            if(document.getElementById('trConfPass') != null)
            document.getElementById('trConfPass').style.display = '';
        }
        //ALLDEV-826 Starts
        else if(HostGuestPass == "1")
        {
            if(document.getElementById("trConfPass1") != null)
                document.getElementById("trConfPass1").style.display = '';
            if(document.getElementById('trConfPass') != null)
                document.getElementById('trConfPass').style.display = '';
            if(document.getElementById("trHostPwd") != null)
                document.getElementById("trHostPwd").style.display = '';
            if(document.getElementById("trCfmHostPwd") != null)
                document.getElementById("trCfmHostPwd").style.display = '';
        }
        //ALLDEV-826 Ends
    }
    //ZD 100753 Ends
    if (document.getElementById("lstVMR").selectedIndex > 0)
    {
    iVMR = document.getElementById("lstVMR").selectedIndex; 
	if (VMRType  == "1")//FB 2448 //ZD 100753
	    {
            document.getElementById("divbridge").style.display = "block";
            if(document.getElementById("divChangeVMRPIN") != null)
                document.getElementById("divChangeVMRPIN").style.display = "none"; //ZD 100522

        }
        vmrParty.value = "1";
    }
    else if(VMRType == "2") //ZD 100522
    {
            document.getElementById("divbridge").style.display = "none";

            if(ShowVMRPIN == "1")
                if(document.getElementById("divChangeVMRPIN") != null)
                    document.getElementById("divChangeVMRPIN").style.display = "block";

            vmrParty.value = "0";

            
    }
    else
    {
        document.getElementById("divbridge").style.display ="none";
        if(document.getElementById("divChangeVMRPIN") != null)
            document.getElementById("divChangeVMRPIN").style.display = "none"; //ZD 100522
        vmrParty.value = "0"
     }
}

return true;
}

function ChangeImmediate()
{
	//FB 2634
    document.getElementById("lstDuration_Text").style.width = "65px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";

    var t = '';
    var t1 = 'None';
    
    if (document.getElementById("chkStartNow").checked)
    {
        t = 'None';
        t1 = '';
        //FB 3044 Starts
         if(document.getElementById("chkMCUConnect")!= null) 
            document.getElementById("chkMCUConnect").checked = false;
        if(document.getElementById("MCUConnectDisplayRow")!= null)
            document.getElementById("MCUConnectDisplayRow").style.display = "none";
       if(document.getElementById("MCUConnectRow")!= null) 
            document.getElementById("MCUConnectRow").style.display = "none";
        //FB 3044 Ends
    }
    
    //FB 2694
    if(document.getElementById("lstConferenceType") != null)
        var conftype = document.getElementById("lstConferenceType").value;
    
    if(t == '' && '<%=enableBufferZone%>' == "1" && conftype != 8)
    {
       //ZD 101755 start
       if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
         document.getElementById("SetupRow").style.display = '';
       else
         document.getElementById("SetupRow").style.display = 'None';
       if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
         document.getElementById("TearDownRow").style.display = '';
       else
         document.getElementById("TearDownRow").style.display = 'None';
         //ZD 101755 End

    }
    else
    {
        document.getElementById("SetupRow").style.display = 'None';
        document.getElementById("TearDownRow").style.display = 'None';
        document.getElementById("SetupDuration").value = "0"; //ZD 100085             
        document.getElementById("TearDownDuration").value = "0";
    }
        
    document.getElementById("ConfStartRow").style.display = t;
    document.getElementById("ConfEndRow").style.display = t;
    if ("<%=timeZone%>" == "0" ) //FB 2634
        document.getElementById("TimezoneRow").style.display = "None";
    else
        document.getElementById("TimezoneRow").style.display = t;
    
    if ('<%=Session["recurEnable"] %>' == "0")
        document.getElementById("RecurRow").style.display = "None";
    else if ("<%=isInstanceEdit%>" == "Y" )
        document.getElementById("RecurRow").style.display = "none";
    else
        document.getElementById("RecurRow").style.display = t;
    
    document.getElementById("divDuration").style.display = t1;
    
    document.getElementById("recurDIV").style.display = 'None'
    document.getElementById("RecurrenceRow").style.display = 'None';
    
    if(t == 'None')
    {
        document.frmSettings2.RecurSpec.value = '';
        document.frmSettings2.RecurringText.value = '';	 
        Page_ValidationActive=false;
    }
}

function ChangeImmediateOld()
{
    /*
    var t;
	var t1;
    //alert(document.getElementById("chkStartNow").checked);
    document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    document.getElementById("SetupTime_Text").style.width = "75px"; //buffer zone
    document.getElementById("TeardownTime_Text").style.width = "75px"; //buffer zone
    
    if (document.frmSettings2.Recur.value == "")
    {
		//Merging Recurrence - start
        if( document.getElementById("hdnRecurValue").value == 'R')
        {
             t = "none";
             t1 = "";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "none";
        }
        else if (document.getElementById("chkStartNow").checked)  //Merging Recurrence - end
        {
            t = "none";
            t1 = "none";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "";
            document.getElementById("chkEnableBuffer").checked = false;
        }
        else
        {
            t = "";
            t1 = "";
            Page_ValidationActive=true;
            document.getElementById("divDuration").style.display ="none";
        }
        //FB 2634
	    document.getElementById("SetupRow").style.display = t1;
	    document.getElementById("TearDownRow").style.display = t;
	    document.getElementById("TimezoneRow").style.display = t1;
	    //document.getElementById("NONRecurringConferenceDiv5").style.display = t; // Merging Recurrence
	    document.getElementById("ConfStartRow").style.display = t1; //buffer zone
	    document.getElementById("ConfEndRow").style.display = t1; //buffer zone
	    
	    //FB 1911
	    var args = ChangeImmediate.arguments;
	    if(args != null)
	    {
	        if(args[0] == "S")
	        {
	            document.getElementById("recurDIV").style.display = "None"; //FB 1981
	            document.frmSettings2.RecurSpec.value = '';
	            document.frmSettings2.RecurringText.value = '';	            
	        }
	    }
		//FB 2634
	    if ("<%=enableBufferZone%>" == "0")
	    {
	        document.getElementById("SetupRow").style.display = "None";
	        document.getElementById("TearDownRow").style.display = "None";
	    }
	    else
	    {
	        document.getElementById("SetupRow").style.display = t1;
	        document.getElementById("TearDownRow").style.display = t1
	    }
 		
	    //FB 1250
        if ("<%=Session["recurEnable"] %>" == "0")
        {
            //document.getElementById("recurDIV").style.display = "none";
            document.getElementById("RecurRow").style.display = "none";//FB 2634
        }
        else if ("<%=isInstanceEdit%>" == "Y" )
            document.getElementById("RecurRow").style.display = "none";//FB 2634
        else
        {
            //document.getElementById("recurDIV").style.display = t;
            document.getElementById("RecurRow").style.display = t1;//FB 2634
        }
        //FB 1250 
        
        if ("<%=timeZone%>" == "0" ) //FB 1425
            document.getElementById("TimezoneRow").style.display = "none";  //FB 2634  
          
        document.getElementById("EndDateArea").style.display = t;
	    document.getElementById("TeardownArea").style.display = t;
	    
	    fnEnableBuffer();
        
    }
    */
}


function isRecur()
{
//FB 2634
/*
    //Merging Recurrence - start
    var t = (document.frmSettings2.Recur.value == "") ? "" : "none";
    
    if(isRecur.arguments.length > 0 || document.getElementById("hdnRecurValue").value == 'R')
    {
        if(isRecur.arguments[0] == 'R' || document.getElementById("hdnRecurValue").value == 'R')
            t = "None";
    }
   //Code Commented for Merging Recurrence
    //FB 2274 Starts
    var EnableImmConf_Session;
    if(document.getElementById("hdnCrossEnableImmConf").value != null)
        EnableImmConf_Session = document.getElementById("hdnCrossEnableImmConf").value;
    else 
        EnableImmConf_Session = "<%=Session["EnableImmConf"] %>";
    //FB 2274 Ends
    
	document.getElementById("TearDownRow").style.display = t;
    //Code Modified For MOJ Phase 2 -Start
    if ("<%=client.ToString().ToUpper() %>" == "MOJ")  
    {  
        document.getElementById("StartNowRow").style.display = "none";
        document.getElementById("ConfStartRow").style.display = "none"; //buffer zone
	    document.getElementById("ConfEndRow").style.display = "none"; //buffer zone
	 }
    else if(EnableImmConf_Session == "0") //FB 2036 2274
       document.getElementById("StartNowRow").style.display = "none"; //t;
    else
       document.getElementById("StartNowRow").style.display = t;
	
    //Code Modified For MOJ Phase 2  -End
	document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    document.getElementById("SetupTime_Text").style.width = "75px"; //buffer zone 
    document.getElementById("TeardownTime_Text").style.width = "75px"; //buffer zone 
 	//Merging Recurrence - start
    if(t == "")
    {
        document.getElementById("RecurrenceRow").style.display = 'None';
        document.getElementById("DurationRow").style.display = 'None';
        
    }
    else
    {
        document.getElementById("RecurrenceRow").style.display = '';//Edited For FF...
        document.getElementById("DurationRow").style.display = '';//Edited For FF...
    }
    //Merging Recurrence - end
        
	if (t != "")
	{
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	}
	
	if ("<%=timeZone%>" == "0" )//FB 1425
      document.getElementById("TimezoneRow").style.display = "none"; 
      
	document.getElementById("EndDateArea").style.display = t;
    document.getElementById("StartDateArea").style.display = t;
    document.getElementById("TeardownArea").style.display = t;
*/      
      return false;
}


function getYourOwnEmailList (i,j) //ZD 100221  j : 0- Req , 1- Conf Host
{
	//Code Modified by Offshore for FB 412 - Start(changed frm=approverNET To party2NET)
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	
	if(i == 999)
//	    url = "dispatcher/conferencedispatcher.asp?frm=party2NET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management	
       url = "../en/emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=" + i; //Login Management
	else
	    //url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management
	    url = "../en/emaillist2main.aspx?t="+j+"&frm=approverNET&fn=Setup&n=" + i;//Login Management //ZD 100221
	       	
	//Code Modified by Offshore for FB 412 - End
	if (!window.winrtc) {	// has not yet been defined
	
		winrtc = window.open(url, "", "width=920,height=470px,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=920,height=470px ,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=920ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
	        winrtc.focus();
		}
}
 //FB 2670 Starts
function getVNOCEmailList()
{
    var Confvnoc = document.getElementById("hdnVNOCOperator");
    var ConfOrg = '<%=Session["OrganizationID"] %>'; //FB 2764
    if('<%=Session["multisiloOrganizationID"] %>' != '') 
        ConfOrg = '<%=Session["multisiloOrganizationID"] %>';
    //ZD 100420 - Start
    url = "VNOCparticipantlist.aspx?ConfOrg="+ConfOrg+"&cvnoc="+Confvnoc.value;  //FB 2764    
   if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
        else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
        else {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } 
    //ZD 100420 - End
}

function deleteVNOC()
{
   document.getElementById('txtVNOCOperator').value = "";
   document.getElementById('hdnVNOCOperator').value = "";
   document.getElementById('hdnSaveData').value = ""; //ZD 100875
}
 //FB 2670 Ends
function checkClicks()
{
    var o = window.event.srcElement;
    if (o.tagName == "INPUT" && o.type == "checkbox")
    {
        //alert(o.title);
    } 

}
function goToCal()
{
	roomcalendarview();
}
function openRecur()
{
    //ZD 101837
    var ConfRecurLimit = 50;
    if(document.getElementById('spnRecurLimit') != null)
        ConfRecurLimit = document.getElementById('spnRecurLimit').innerHTML;

    //ZD 101714
    if(document.getElementById("hdnSpecRec") != null)
        document.getElementById("hdnSpecRec").value = "0";

    if(document.getElementById('chkWebex') != null)
    {
        if(document.getElementById('chkWebex').checked == true)
        {
            if(ConfRecurLimit > 50)
                document.getElementById('spnRecurLimit').innerHTML = 50;
        }
    }
    else
    {
        if(document.getElementById('spnRecurLimit') != null)
            document.getElementById('spnRecurLimit').innerHTML = <%=Session["ConfRecurLimit"]%>;
    }
    //ZD 100875
    if (document.getElementById("hdnSaveData") != null)
        document.getElementById("hdnSaveData").value = "";
	//FB 2634
	var chkrecurrence = document.getElementById("chkRecurrence");
    var args = openRecur.arguments;
    var recType = "";
    if(args.length > 0)
        if(args[0] == 'S')
            recType = "S";
            
    if((recType == "" && chkrecurrence.checked))
        recType = "N";
        
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    
    //FB 2694
    if(document.getElementById("lstConferenceType") != null)
            var conftype = document.getElementById("lstConferenceType").value;
            
	if(recType == "N")
	{
	    document.getElementById("RecurrenceRow").style.display = '';
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	    
        document.getElementById("StartDateArea").style.display = 'None';
	    document.getElementById("EndDateArea").style.display = 'None';
        document.getElementById("StartNowRow").style.display = 'None';
        document.getElementById("divDuration").style.display = "None";
        document.getElementById("DurationRow").style.display = '';
        document.getElementById("ConfEndRow").style.display = 'None';
        document.getElementById("ConfStartRow").style.display = '';
        
        if('<%=enableBufferZone%>' == "1" && conftype != 8)//FB 2694
        {
            //ZD 101755 start
            if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
                document.getElementById("SetupRow").style.display = '';
            else
                document.getElementById("SetupRow").style.display = 'None';
            if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
                document.getElementById("TearDownRow").style.display = '';
            else
                document.getElementById("TearDownRow").style.display = 'None';
           //ZD 101755 End
        }
        else
        {
            document.getElementById("SetupRow").style.display = 'None';                
            document.getElementById("TearDownRow").style.display = 'None';
            document.getElementById("SetupDuration").value = "0"; //ZD 100085
            document.getElementById("TearDownDuration").value = "0";
                            
        }   
        
        //FB 2694
        if(conftype == 8)
        {
        if(document.getElementById("SPCell1"))
                document.getElementById("SPCell1").style.display = 'None'
            if(document.getElementById("SPCell2"))
                document.getElementById("SPCell2").style.display = 'None'
              
        //removerecur(); 
        
        }
        document.getElementById("RecurSpec").value = "";
        document.getElementById("hdnRecurValue").value = 'R';
        document.getElementById("recurDIV").style.display = 'None';
        
        initial();
        fnShow();       
        
        
	}
	else if (recType == "S")
	{
	    document.getElementById("hdnRecurDiv").value = "S";//FB 2770
	    var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
        var stime = document.getElementById("<%=confStartTime.ClientID%>");
        
        removerecur();
        
        chkrecurrence.checked = false;               
        document.getElementById("recurDIV").style.display = '';
        document.getElementById("SetupRow").style.display = 'None';
        document.getElementById("TearDownRow").style.display = 'None';
        document.getElementById("ConfStartRow").style.display = 'None';                
        document.getElementById("ConfEndRow").style.display = 'None';
        document.getElementById("RecurrenceRow").style.display = 'None';
        document.getElementById("DurationRow").style.display = 'None';
        document.getElementById("StartNowRow").style.display = 'None';
        //FB 2558
        ChangeTimeFormat("D")
        //FB 2634
        top.recurWin = window.open('recurNET.aspx?pn=C&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime 
        + "&su="+ document.getElementById("SetupDuration").value + "&tdn="+ document.getElementById("TearDownDuration").value 
        +"&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	}
	else
	{
        document.getElementById("DurationRow").style.display = 'None';
        document.getElementById("recurDIV").style.display = 'None';
        document.getElementById("hdnRecurValue").value = '';
	    document.getElementById("RecurrenceRow").style.display = 'None';
        if('<%=enableBufferZone%>' == "1" && conftype !=8)
        {
           //ZD 101755 start
           if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
             document.getElementById("SetupRow").style.display = '';
           else
             document.getElementById("SetupRow").style.display = 'None';
           if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
             document.getElementById("TearDownRow").style.display = '';
           else
             document.getElementById("TearDownRow").style.display = 'None';
             //ZD 101755 End
        }
        else
        {
            document.getElementById("SetupRow").style.display = 'None';
            document.getElementById("TearDownRow").style.display = 'None';
            document.getElementById("SetupDuration").value = "0"; //ZD 100085             
            document.getElementById("TearDownDuration").value = "0";
        }
            
        document.getElementById("ConfStartRow").style.display = '';                
        document.getElementById("ConfEndRow").style.display = '';
        document.getElementById("StartDateArea").style.display = '';
	    document.getElementById("EndDateArea").style.display = '';
		var EnableImmConf_Session;
        if(document.getElementById("hdnCrossEnableImmConf").value != null)
            EnableImmConf_Session = document.getElementById("hdnCrossEnableImmConf").value;
        else 
            EnableImmConf_Session = '<%=Session["EnableImmConf"] %>';

        //FB 2694 II Start  
        var chkStartNow;
        if(document.getElementById("chkStartNow") != null)
            chkStartNow = document.getElementById("chkStartNow");
            
        if(EnableImmConf_Session == "1" && conftype != 8 )
        {
            document.getElementById("StartNowRow").style.display = '';
        }
        else
        {
            chkStartNow.checked = false;
            document.getElementById("StartNowRow").style.display = 'None';
        }
        if(conftype == 8) 
        {      
        
            if(document.getElementById("SPCell1"))
                document.getElementById("SPCell1").style.display = 'None'
            if(document.getElementById("SPCell2"))
                document.getElementById("SPCell2").style.display = 'None'
            
            document.getElementById("RecurSpec").value = "";
            document.getElementById("hdnRecurValue").value = 'R';
            document.getElementById("recurDIV").style.display = 'None';  
            document.getElementById("hdnRecurDiv").value ="";  //FB 2770
            chkStartNow.checked = false;
            document.getElementById("StartNowRow").style.display = 'None';        
        }
         
          
        if(EnableImmConf_Session == "1" && conftype != 8 && document.getElementById("hdnRecurDiv").value != "S")
        {
           document.getElementById("StartNowRow").style.display = '';
        }
        else
        {
            chkStartNow.checked = false;
            document.getElementById("StartNowRow").style.display = 'None';
        }
        
        //FB 2959 Start
        if(EnableImmConf_Session == "1" && conftype != 8 && document.getElementById("hdnRecurDiv").value != "S"||EnableImmConf_Session == "1" && conftype != 8)
        {
            document.getElementById("StartNowRow").style.display = ''; 
        }
        else
        {
            chkStartNow.checked = false;
            document.getElementById("StartNowRow").style.display = 'None';
        }
        //FB 2959 End
            
        //FB 2694 II End
        
        removerecur();
	}
}

function openRecurOld()
{
/*
    //FB 1911
    var args = openRecur.arguments;
    var isarg = "0";
    if(args.length > 0)
        if(args[0] == 'S')
            isarg = "1";
    
	var chkrecurrence = document.getElementById("chkRecurrence");
    
    if(isarg == "1" && chkrecurrence.checked)
    {
        removerecur();        
        chkrecurrence.checked = false;
    }
    
    if(isarg =="0" && chkrecurrence.checked)
        document.getElementById("RecurSpec").value = "";
    
    if(chkrecurrence != null && chkrecurrence.checked == false && isarg == "0")
    {
        removerecur();
        ChangeEndDate();
    }
    else
    {
	    if (ChangeEndDate(1) && ChangeStartDate(1))
	    {
	        var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
	        //Code added by Offshore for FB Issue 1073 -- Start
	        //var sd = document.getElementById("<%=confStartDate.ClientID%>");
	        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
	        //Code added by Offshore for FB Issue 1073 -- End
	        var stime = document.getElementById("<%=confStartTime.ClientID%>");
	        
	        var setupTime = document.getElementById("SetupTime_Text"); //Edited for FF
	        var teardownTime = document.getElementById("TeardownTime_Text");//Edited for FF
	        var sTime = document.getElementById("hdnSetupTime");
	        var tTime = document.getElementById("hdnTeardownTime");
	       
	         sTime.value  = setupTime.value;
	         tTime.value = teardownTime.value;
	        
	        
		   //Merging Recurrence
	       document.getElementById("EndDateArea").style.display = 'None';
	       document.getElementById("StartDateArea").style.display = 'None';
	       document.getElementById("TeardownArea").style.display = 'None';
	       document.getElementById("TearDownRow").style.display = 'None';//FB 2634
           
           if(isarg == "1")
           {
                showSpecialRecur();	           
	            top.recurWin = window.open('recurNET.aspx?pn=C&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime + "&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	       }
           else
           {       //FB 2634 
	           document.getElementById("SetupRow").style.display = '';
	           document.getElementById("TearDownRow").style.display = '';
	           if(document.getElementById("chkEnableBuffer").checked == true)	
	           {
	               document.getElementById("ConfStartRow").style.display = '';                
	               document.getElementById("ConfEndRow").style.display = '';
	           }
	           document.getElementById("recurDIV").style.display = 'None';       
	           document.getElementById("RecurText").value = '';    
	           document.getElementById("hdnRecurValue").value = 'R';
	           isRecur();
	           initial();
	           fnShow();
	       }
	        //code commented for Merging Recurrence	        
		    //top.recurWin = window.open('recurNET.asp?frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime + "&wintype=pop", 'recur', 'titlebar=yes,width=600,height=450,resizable=yes,scrollbars=yes,status=yes');
		}
	}
	fnHideStartNow();//FB 1825	
	
	*/
}
//FB 1911
function visControls()
{
	//FB 2634
    if(document.getElementById("RecurSpec").value == "")
    {
	   document.getElementById("StartDateArea").style.display = '';
	   //document.getElementById("TearDownArea").style.display = '';
	   document.getElementById("EndDateArea").style.display = '';
		var conftype;
	   if(document.getElementById("lstConferenceType") != null)
          conftype = document.getElementById("lstConferenceType").value;
	   if('<%=enableBufferZone%>' == "1" && conftype != 8)
	   {
         //ZD 101755 start
	       if('<%=Session["EnableSetupTimeDisplay"]%>' == "1")
             document.getElementById("SetupRow").style.display = '';
           else
             document.getElementById("SetupRow").style.display = 'None';
           if('<%=Session["EnableTeardownTimeDisplay"]%>' == "1")
             document.getElementById("TearDownRow").style.display = '';
           else
             document.getElementById("TearDownRow").style.display = 'None';
         //ZD 101755 End
	   }
	   else
	   {
	    document.getElementById("SetupRow").style.display = 'None';	   
	    document.getElementById("TearDownRow").style.display = 'None';
	    document.getElementById("SetupDuration").value = "0"; //ZD 100085             
        document.getElementById("TearDownDuration").value = "0";
	   }
       document.getElementById("recurDIV").style.display = 'None';     
       //document.getElementById("RecurText").value = '';    
       document.getElementById("ConfStartRow").style.display = '';                
       document.getElementById("ConfEndRow").style.display = '';
       
       //FB 2694 II Start
        var conftype;
        if(document.getElementById("lstConferenceType") != null)
            conftype = document.getElementById("lstConferenceType").value;
         
        var chkStartNow; 
        if(document.getElementById("chkStartNow") != null)   
            chkStartNow = document.getElementById("chkStartNow");
       
        //FB 2686 Start
       if('<%=Session["EnableImmConf"]%>' ==1 && conftype != 8)
            document.getElementById("StartNowRow").style.display='';
        else
        {
           chkStartNow.checked = false;
           document.getElementById("StartNowRow").style.display='None';
        }
       //FB 2686 End 
       
       //FB 2694 II End

	}
}

function showSpecialRecur()
{
	//FB 2634
    document.getElementById("recurDIV").style.display = '';
    document.getElementById("SetupRow").style.display = 'None';
    document.getElementById("EndDateArea").style.display = 'None';
    document.getElementById("StartDateArea").style.display = 'None';
    //document.getElementById("TearDownArea").style.display = 'None';
    document.getElementById("TearDownRow").style.display = 'None';
    document.getElementById("ConfStartRow").style.display = 'None';                
    document.getElementById("ConfEndRow").style.display = 'None';
}

function roomcalendarview()
{   
    var frm = "<%=confStartDate.Text%>";//FB 1073
    rn = document.getElementById("selectedloc").value //FB 1138
    rn1 = document.getElementById("selectedTierList").value //ZD 102650
    //rn = getListViewChecked(rn);	//FB 1138
    //dispatcher/admindispatcher.asp?cmd=ManageConfRoom
    
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + frm + "&m1=" + rn1;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}
function roomconflict(confDate) //FB 1154
{   
    var frm = confDate;
    rn = document.getElementById("selectedloc").value
    
    //rn = getListViewChecked(rn);	
    
    //url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn  + "&d=" + frm;
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + confDate;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}
//FB 1048
function CallMeetingPla()
{   

        var confTimeZone = "<%=lstConferenceTZ.SelectedValue%>";
        var sd = "<%=confStartDate.Text %>";
       // alert(confTimeZone);
        var stime = "<%=confStartTime.Text %>";
        //alert(stime);
        var confDateTime = sd + " " + stime;
        var roomId = document.getElementById("selectedloc").value;

        //roomId = getListViewChecked(roomId); //FB 1138,1142
        rn = roomId.split(",").length ;
        rId = roomId.split(",").length ;



        if(roomId == ' ')
        {
            alert(ConfSeleRoom)
            return false;
            
        }
        
        
        if(rn==0 || (rn==1 && roomId == ', '))
        {
          alert(ConfSeleRoom)
          return false;
        }
                  
        // FB 2459 starts
        if (roomId.length < 1)
        {
          alert(ConfSeleRoom)
          return false;
        }
        // FB 2459 end
        if(rn >5)
        {
          alert(OnlyfiveRoom)
          return false;
        }
//         if(rn > 0)
//          alert("Make Sure the Selected Room has TimeZone Value")
       
        url = "MeetingPlanner.aspx?";
                url += "ConferenceDateTime=" + confDateTime + "&";
                url += "ConferenceTimeZone=" + confTimeZone + "&";
                url += "RoomID=" + roomId + ",";
    
	  window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
      return false;
          
      }
 //ZD 100815 Start
    function InitSetConference()
    {
    
    var confOrigin =0;
    if(document.getElementById('hdnconfOriginID'))
    confOrigin = document.getElementById('hdnconfOriginID').value;
        document.getElementById('hdnSaveData').value = '1';
        DataLoading(1);
    var conftype;
    //ZD 101228 Starts
    if (document.getElementById("hdnAVWOAlert")!= null && document.getElementById("hdnCAtWOAlert")!= null && document.getElementById("hdnFacWOAlert")!= null)
    {
        var AVWOAlert , CAtWOALert,FacWOAlert,WOalertmessage, AVWOtime,CatWOtime,FacWOtime;
        AVWOAlert = document.getElementById("hdnAVWOAlert").value;
        CAtWOALert = document.getElementById("hdnCAtWOAlert").value;
        FacWOAlert = document.getElementById("hdnFacWOAlert").value;
        AVWOtime = document.getElementById("hdnAVWOFullFill").value;
        CatWOtime = document.getElementById("hdnCATWOFullFill").value;
        FacWOtime = document.getElementById("hdnFacWOFullFill").value;
        WOalertmessage="";
        if(AVWOAlert == "1")
            WOalertmessage = ConfWOAlert1 + AVWOtime + ConfWOAlert2 +AVWO + "\n";
        if(CAtWOALert == "1")
            WOalertmessage += ConfWOAlert1 + CatWOtime + ConfWOAlert2 +CatWO+ "\n";
        if(FacWOAlert == "1")
            WOalertmessage += ConfWOAlert1 + FacWOtime + ConfWOAlert2 +FacWO;

        if((FacWOAlert =="1" || CAtWOALert == "1"  || AVWOAlert == "1") && WOalertmessage != "")
        {
            alert(WOalertmessage);
            DataLoading(0);
            document.getElementById("hdnSaveData").value = "";
            return false;
        }
    }
    //ZD 101228 Ends
    
        if(document.getElementById("hdnCrossEnableSmartP2P").value == "1" && document.getElementById("hdnRecurToggle").value == "0" && document.getElementById("hdnSmartP2PTotalEps").value == "2")
        {
            if (document.getElementById("hdnSmartP2PNotify").value == "1")
            {
                document.getElementById("p2pConfirmBtn").click();
                return false;
            }
            else if(document.getElementById("hdnSmartP2PNotify").value == "0")
            {
                document.getElementById("hdnconftype").value = "4";
                //ZD 104476
                if(!SetConference(confOrigin))
                    return false;
            }
            else if(document.getElementById("hdnSmartP2PNotify").value == "2")
            {
                if ("<%=isEditMode%>" == "0")
                {
                    document.getElementById("hdnconftype").value = "4";
                    SetConference(confOrigin);
                }
                else
                {
                     document.getElementById("p2pConfirmBtn").click();
                     return false;
                }
            }
        }
        else
        {
             //ZD 104476
            if(!SetConference(confOrigin))
                return false;
        }
    }

    function p2pSetConference(par)
    {
        if(par == '0')
            document.getElementById("hdnconftype").value = "4";
        else if(document.getElementById("hdnSmartP2PNotify").value == "2")
            document.getElementById("hdnconftype").value = "2";
        else
            document.getElementById("hdnconftype").value = "";
    
        document.getElementById("hdnRecurToggle").value = "1"
        document.getElementById("btnConfSubmit").click();
    }
    //ZD 100815 End
function SetConference(confOrigin)//FB 2457 Exchange round trip
{
  //FB 2430 start //ZD 100815 Start
  /*
  document.getElementById("hdnconftype").value = "";
  if (document.getElementById("hdnCrossEnableSmartP2P").value == "1" && document.getElementById("hdnSmartP2PTotalEps").value == 2)
  {
     if (confirm(ConfP2PMCUConnect))
        document.getElementById("hdnconftype").value = "4";
       
  }
   */
  //FB 2430 end  //ZD 100815 End   
      
 
  //FB 1830 Email Edit - end
  
  //ZD 101120 Starts
    if (document.getElementById("hdnGLWarningPopUp")!= null)
    {
        var alertmessage, GLCount ="0";
        var GLWarningPopUP =   document.getElementById("hdnGLWarningPopUp");
        var isPopUP = GLWarningPopUP.value.trim();

        var GLApprTime =   document.getElementById("hdnGLApprTime");
        var GLAppr = GLApprTime.value.trim(); var ActualGLApprTime;

        var GLConfTime =   document.getElementById("hdnGLConfTime");
        var GLConf = GLConfTime.value.trim(); 
        GLCount = document.getElementById("hdnGLCount").value;
        if(GLAppr == "0")
            ActualGLApprTime = 24;
        else if(GLAppr == "2")
            ActualGLApprTime = 72;
        else 
            ActualGLApprTime = 48;  

        if( isPopUP == "1") //Guest Location Pop up
        {
            if(GLCount != "" && GLCount!= "0")
            {
                if(GLConf == "1")
                {
                    alertmessage = Confalertmessage1 + " " + ActualGLApprTime + " " + Confalertmessage2;
                    alert(alertmessage);
                }else 
                {
                    alertmessage = Confalertmessag1 +" "+ ActualGLApprTime+" " + Confalertmessag2;
                    alert(alertmessage);
                }
            }
        }
    }  
  //ZD 101120 Ends
  
  //FB 2274 Starts
 var roomModule_Session ;
 var foodModule_Session ;
 var hkModule_Session ;
 		
 if(document.getElementById("hdnCrossroomModule").value != null)
 	roomModule_Session = document.getElementById("hdnCrossroomModule").value;
 else
 	roomModule_Session = "<%=Session["roomModule"] %>";
 	
 if(document.getElementById("hdnCrossfoodModule").value != null)
 	foodModule_Session = document.getElementById("hdnCrossfoodModule").value;
 else
 	foodModule_Session = "<%=Session["foodModule"] %>";
 	
 if(document.getElementById("hdnCrosshkModule").value != null)
 	hkModule_Session = document.getElementById("hdnCrosshkModule").value;
 else
 	hkModule_Session = "<%=Session["hkModule"] %>";
 //FB 2274 Ends
  
  document.getElementById("btnConfSubmit").style.display = "none";
  //btn.style.display = true;
  var flag = "1";
    if ( (document.getElementById("<%=plblAVWorkOrders.ClientID %>").value == "0") && (roomModule_Session == "1") && ("<%=Session["admin"] %>" != "0") && ("<%=client.ToString().ToUpper() %>" == "WASHU") ) //FB 2274
      if (confirm(AVWorkorder)) // FB 2570
          flag = "1";
      else
          flag = "2";
    if ( (document.getElementById("<%=plblCateringWorkOrders.ClientID %>").value == "0") && (foodModule_Session == "1") && ("<%=Session["admin"] %>" != "0") && ("<%=client.ToString().ToUpper() %>" == "WASHU") ) //FB 2274
        if (confirm(CatWorkorder))
            flag = "1";
        else
            flag = "2";
    if ( (document.getElementById("<%=plblHouseKeepingWorkOrders.ClientID %>").value == "0") && (hkModule_Session == "1") && ("<%=Session["admin"] %>" != "0") && ("<%=client.ToString().ToUpper() %>" == "WASHU") ) //FB 2274
        if (confirm(FacWorkorder)) // FB 2570
            flag = "1";
        else
            flag="2";

  if (flag == "1")
  {
    document.getElementById("__EVENTTARGET").value="btnConfSubmit";
  }
  else if (flag == "2")
  {
      document.getElementById("__EVENTTARGET").value="";
      document.getElementById("btnConfSubmit").style.display = "";
  }
  DataLoading(1);
  WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnConfSubmit", "", true, "", "", false, false));
/*
  if (flag=="1")
    document.getElementById("<%=btnConfSubmit.ClientID %>").disabled = true;
  else if (flag=="2")
    document.getElementById("<%=btnConfSubmit.ClientID %>").disabled = false;
*/
//FB 2457 Exchange round trip - starts
    if(confOrigin=="7")
    {
      alert(ConfPlease);
    }
    //FB 2457 Exchange round trip - ends
    return true; //ZD 101228
}
//Added by Sudhir Mangla For  Bug no 588

/*  Code Added For FB 1476 - Start  */
function getAGroupDetail(frm, cb, gid)
{
	if (cb == null) {
		if (gid != null) {
			id = gid;
		} else {
			alert(ConfSystemerror)
			return false;
		}
	} else {

		if (cb.selectedIndex != -1) {
			if (gid != null) id = gid; else id = cb.options[cb.selectedIndex].value;
		} else {
			alert(EN_53);
			return false;
		}
	}
	
	//code added for Managegroup2.asp to aspx convertion 
	if(frm == 'g')
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=no");
	else
	    //gm = window.open("memberallstatus.asp?f=" + frm + "&n=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	if (gm) gm.focus(); else alert(EN_132);
}
/*  Code Added For FB 1476 - End  */


//Audio Addon
function AudioAddon()
{
   
    var args = AudioAddon.arguments;
    
    var drptyp = document.getElementById("CreateBy");
    var controlName = 'dgUsers_ctl';
    var ctlid = '';
    
    if(drptyp)
    {
        if(drptyp.value == "2")
        {
            if(args)
            {
                var tbl1 = document.getElementById(args[0]);
                var tbl2 = document.getElementById(args[1]);
                var drp = document.getElementById(args[2]);
                var drp2 = document.getElementById(args[3]);
                 
                var usrArr=args[2].split("_");
                var len=usrArr.length;

                if(len > 2)
                {
                    ctlid = usrArr[1];
                    ctlid = ctlid.replace(/ctl/i,'');
                }
                
                var lstAddType = document.getElementById((controlName + ctlid +'_lstAddressType'));
                //var lstProtocol = document.getElementById((controlName + ctlid +'_lstProtocol'));
                var lstConType = document.getElementById((controlName + ctlid +'_lstConnectionType'));
                var txtAdd = document.getElementById((controlName + ctlid +'_txtAddress'));

                var txtaddphone = document.getElementById((controlName + ctlid +'_txtaddphone'));
                var txtconfcode = document.getElementById((controlName + ctlid +'_txtConfCode'));
                var txtleader = document.getElementById((controlName + ctlid +'_txtleaderPin'));
                var txtPartyCode = document.getElementById((controlName + ctlid +'_txtPartyCode')); //ZD 101446
                
//                var lstAddType = document.getElementById((controlName + ctlid +'_reqAddressType'));
//                var lstConType = document.getElementById((controlName + ctlid +'_RequiredFieldValidator3'));
//                var txtAdd = document.getElementById((controlName + ctlid +'_reqAddress'));

//                var txtaddphone = document.getElementById((controlName + ctlid +'_RequiredFieldValidator13'));
//                var txtconfcode = document.getElementById((controlName + ctlid +'_RequiredFieldValidator20'));
//                var txtleader = document.getElementById((controlName + ctlid +'_RequiredFieldValidator19'));
            
                if(drp.value == "2" && '<%=Application["EnableAudioAddOn"]%>' == "1")
                {
                    tbl1.style.display = 'none';
                    tbl2.style.display = 'block';
                    
                    if(lstAddType)
                    {
                        lstAddType.value = '1';
                    }
                    if(lstConType)
                    {
                        lstConType.value = '2';
                    }
                    if(txtAdd)
                    {
                        if(Trim(txtAdd.value) == '')
                            txtAdd.value = '127.0.0.1';
                    }
                }
                else
                {
                    if(txtaddphone)
                    {
                        if(Trim(txtaddphone.value) == '')
                        {
                            txtaddphone.value = '127.0.0.1';
                        }
                    }
                    if(txtconfcode)
                    {
                        if(Trim(txtconfcode.value) == '')
                            txtconfcode.value = '0';
                    }
                    if(txtleader)
                    {
                        if(Trim(txtleader.value) == '')
                            txtleader.value = '0';
                    }
                    if(txtPartyCode) //ZD 101446
                    {
                        if(Trim(txtPartyCode.value) == '')
                            txtleader.value = '0';
                    }
                    tbl1.style.display = 'block';
                    tbl2.style.display = 'none';
                }
                drp2.value = drp.value;
            }
                
        }
    }
}

</script>

<%--Merging Recurrence Script Start Here--%>

<script type="text/javascript">

var openerfrm = document.frmSettings2;


function CheckDate(obj)
{    
    var endDate;//FB 2246
    if ('<%=format%>'=='dd/MM/yyyy')
        endDate = GetDefaultDate(obj.value,'dd/MM/yyyy');
    else
        endDate = obj.value;
        
    if (Date.parse(endDate) < Date.parse(new Date()))
        alert(InvalidDate);
}

function fnHide()
{    
    document.getElementById("Daily").style.display = 'none';
    document.getElementById("Weekly").style.display = 'none';
    document.getElementById("Monthly").style.display = 'none';
    document.getElementById("Yearly").style.display = 'none';
    document.getElementById("Custom").style.display = 'none';
    document.getElementById("RangeRow").style.display = 'block';    
}

function fnShow()
{    

    var a = null;
    var f = document.forms[1];
    //ZD 103929
    var args = fnShow.arguments;    
    if (args.length == 0)
        a = document.getElementById("hdnRecurType").value;
    else
        a = args[0];
    
    /*
    var e = f.elements["RecurType"];

    for (var i=0; i < e.length; i++)
    {
        if (e[i].checked)
        {
        a = e[i].value;
        document.frmSettings2.RecPattern.value = a;
        break;
        }
    }
    */

    document.frmSettings2.RecPattern.value = a;

    if(a != null)
    {
        fnHide();
        calendar = null;//ZD 100420
        switch(a)
        {
            case "1":
	            initialdaily(rpstr);
                document.getElementById("Daily").style.display = 'block';                
                break;
            case "2":
	            initialweekly(rpstr);
                document.getElementById("Weekly").style.display = 'block';                            
                break;
            case "3":
	            initialmonthly(rpstr);
                document.getElementById("Monthly").style.display = 'block';                            
                break;
            case "4":
	            initialyearly(rpstr);
                document.getElementById("Yearly").style.display = 'block';                            
                break;
            case "5":
                document.getElementById('flatCalendarDisplay').innerHTML = "";  // clear the div values
                showFlatCalendar(1, dFormat,document.getElementById("StartDate").value);  //for custom calendar display      
	            initialcustomly(rpstr);
                document.getElementById("Custom").style.display = 'block';                            
                document.getElementById("RangeRow").style.display = 'none';
                break;            
        }
    }      
}

function initial()
{
	//FB 2634
    var setuphr;
    var setupmin;
    var setupap;
    var teardownhr;
    var teardownmin;
    var teardownap;
    
	if (document.getElementById("Recur").value=="") 
	{
	    //FB 2558
	    ChangeTimeFormat("D")
		timeMin = document.getElementById("hdnStartTime").value.split(":")[1].split(" ")[0] ;
		timeDur = getConfDurNET(document.frmSettings2, dFormat, document.getElementById("confStartTime_Text").value, document.getElementById("confEndTime_Text").value); //FB 2558
        //ZD 103624
		if (isNaN(timeDur)) {
		    document.getElementById("chkRecurrence").checked = false;
		    openRecur();
            return false;
        }

		if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2558
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("hdnStartTime").value.split(":")[0] + "&" + timeMin + "&" + "-1" + "&" + timeDur + "#"
		else
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + document.getElementById("confStartTime_Text").value.split(" ")[1] + "&" + timeDur + "#"
		
		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += document.getElementById("confStartDate").value + "&1&-1&-1";

 		document.frmSettings2.RecurValue.value = tmpstr;
		document.frmSettings2.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
	}
     
    if(document.frmSettings2.RecurValue.value == "")
	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value; 

	rpstr = AnalyseRecurStr(document.frmSettings2.RecurValue.value);

	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2558
	{
	    if(atint[1] < 12 && atint[3] == "PM")
             atint[1] = atint[1] + 12;
    }
	
	if(atint[1] < 10)
	    atint[1] = "0" + atint[1];
	    
	if(atint[2] < 10)
	    atint[2] = "0" + atint[2];
	
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2558
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	    	
	    //FB 2634
//	if(document.getElementById("confStartTime_Text"))
//	    document.getElementById("confStartTime_Text").value = startstr;
	
	var actualDur = atint[4];
    var setup = document.getElementById("SetupDuration").value;
    var teardown = document.getElementById("TearDownDuration").value;
    //ZD 100085
    //if(document.getElementById("Recur").value != "")
        //actualDur = parseInt(actualDur,10) - (parseInt(setup,10) + parseInt(teardown,10));
	
	if (document.frmSettings2.RecurDurationhr) {
		set_select_field (document.frmSettings2.RecurDurationhr, parseInt(actualDur/60, 10) , true);
		set_select_field (document.frmSettings2.RecurDurationmi, actualDur%60, true);
	}

    document.frmSettings2.RecurPattern.value = rpstr;
    //ZD 103929
	if (document.getElementById("RecurType_" + rpint[0])) {
	    document.getElementById("RecurType_" + rpint[0]).checked = true;
	    document.getElementById("hdnRecurType").value = rpint[0];
	}
//    if (document.frmSettings2.RecurType) 
//	    document.frmSettings2.RecurType[rpint[0] - 1].checked = true;
	
    document.frmSettings2.Occurrence.value ="";
    document.frmSettings2.EndDate.value ="";
   
	if (rpint[0] != 5) {
		document.frmSettings2.StartDate.value = rrint[0];
	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmSettings2.EndType.checked = true;
				break;
			case 2:
		        document.frmSettings2.REndAfter.checked = true;
				document.frmSettings2.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmSettings2.REndBy.checked = true;
				document.frmSettings2.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
	}
}

function recurTimeChg()
{
    //FB 2558
    ChangeTimeFormat("D")   

    var aryStart = document.getElementById("hdnStartTime").value.split(" ");
	
	rshr = aryStart[0].split(":")[0];
	rsmi = aryStart[0].split(":")[1];
	rsap = aryStart[1];

	if (document.frmSettings2.RecurDurationhr) {
		rdhr = parseInt(document.frmSettings2.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmSettings2.RecurDurationmi.value, 10);
		recurduration = rdhr * 60 + rdmi;
//        if (recurduration > (maxDuration *60))
		//     alert(EN_314); //ZD  103925
		document.frmSettings2.EndText.value = calEnd(calStart(rshr, rsmi, rsap), recurduration);
	}
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		document.frmSettings2.DurText.value = calDur(document.getElementById("confStartDate").value, document.getElementById("confEndDate").value, rshr, rsmi, rsap, rehr, remi, reap, 1);
	}
}

</script>

<%--daily Script--%>
<script type="text/javascript">
function summarydaily()
{
	dg  = Trim(document.frmSettings2.DayGap.value);
	rp  = "1&";
	rp += (document.frmSettings2.DEveryDay.checked) ? ("1&" + ((dg == "") ? "-1" : dg)) : "2&-1";
	rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialdaily(rp)   
{
	var rpary = (rp).split("&");
	if (rpary[0] == 1) {
		if (!isNaN(rpary[2]))
			dg = parseInt(rpary[2], 10);
		if ((dg == -1) || isNaN(dg))
			document.frmSettings2.DayGap.value = "";
		else
			document.frmSettings2.DayGap.value = dg;
		if (!isNaN(rpary[1]))
			dt =  parseInt(rpary[1], 10);
		if ((dt == -1) || isNaN(dt))
			rpary[1] = 1;
			
		if(rpary[1] == 1)
		    document.frmSettings2.DEveryDay.checked = true;
		else if(rpary[1] == 2)
		    document.frmSettings2.DWeekDay.checked = true;		
	} 
	else 
	{
		document.frmSettings2.DayGap.value = "";
	    document.frmSettings2.DEveryDay.checked = true;
	}
}

</script>

<%--Weekly Script--%>
<script type="text/javascript">

function summaryweekly ()
{
	wg  = Trim(document.frmSettings2.WeekGap.value);
	rp  = "2&-1&-1&";
	rp += ((wg == "") ? "" : wg) + "&";
	
	var elementRef = document.getElementById("WeekDay");
    var checkBoxArray = elementRef.getElementsByTagName('input');
    var checkedValues = '';
	for (var i = 0; i < checkBoxArray.length; i++)
        rp += checkBoxArray[i].checked ? ((i+1) + ",") : ""  
	
	if (rp.substr(rp.length-1,1)==",")
		rp = rp.substr(0, rp.length-1);

	rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialweekly(rp)
{
    var elementRef = document.getElementById("WeekDay");
    var checkBoxArray = elementRef.getElementsByTagName('input');
	rpary = (rp).split("&");
	if (rpary[0] == 2) {
		if (!(isNaN(rpary[3])))
			wg = parseInt(rpary[3], 10);
		if ((wg == -1) || isNaN(wg))
			document.frmSettings2.WeekGap.value = "1";
		else
			document.frmSettings2.WeekGap.value = wg;
		if (rpary[4] != -1) 
		{
			wdary = (rpary[4]).split(",");
	    
		    for (i = 0; i < wdary.length; i++) 
		        checkBoxArray[parseInt(wdary[i], 10) - 1].checked = true // ZD 101722
		}
	} 
	else 
	{
		document.frmSettings2.WeekGap.value = "1";
  
		for (var i = 0; i < checkBoxArray.length; i++)
		{
            if ( checkBoxArray[i].checked == true )    
			     checkBoxArray[i].checked = false;
		}
	}
}
</script>

<%--Monthly Script--%>
<script type="text/javascript">

function summarymonthly()
{
	mdn  = Trim(document.frmSettings2.MonthDayNo.value);
	mg1  = Trim(document.frmSettings2.MonthGap1.value);
	mg2  = Trim(document.frmSettings2.MonthGap2.value);
	rp  = "3&-1&-1&-1&-1&";
	rp += (document.frmSettings2.MEveryMthR1.checked) ? ("1&" + ((mdn == "") ? "-1" : mdn) + "&" + ((mg1 == "") ? "-1" : mg1) + "&-1&-1&-1") 
		: ("2&-1&-1&" + (document.frmSettings2.MonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.MonthWeekDay.selectedIndex + 1) + "&" + ((mg2 == "") ? "-1" : mg2));
	rp += "&-1&-1&-1&-1&-1&-1";

//	alert(rp);
	document.frmSettings2.RecurPattern.value = rp;
}

function initialmonthly(rp)
{
	rpary = (rp).split("&");
	if (rpary[0] == 3) {
		for (i=5; i<11; i++) {
			if (!isNaN(rpary[i]))
				rpary[i] = parseInt(rpary[i], 10);
			else
				rpary[i] = -1;
		}
		
		switch (rpary[5]) {
			case 1:
				document.frmSettings2.MEveryMthR1.checked = true;
				document.frmSettings2.MonthDayNo.value = (rpary[6] == -1) ? "" : rpary[6];
				document.frmSettings2.MonthGap1.value = (rpary[7] == -1) ? "" : rpary[7];
				document.frmSettings2.MonthWeekDayNo[0].checked = true;
				document.frmSettings2.MonthWeekDay[0].checked = true;
				document.frmSettings2.MonthGap2.value = "";
				break;
			case 2:
				document.frmSettings2.MEveryMthR2.checked = true;
				document.frmSettings2.MonthDayNo.value = "";
				document.frmSettings2.MonthGap1.value = "";
				if (rpary[8] == -1)
					document.frmSettings2.MonthWeekDayNo[0].selected = true;
				else
					document.frmSettings2.MonthWeekDayNo[rpary[8]-1].selected = true;
				if (rpary[9] == -1)
					document.frmSettings2.MonthWeekDay[0].selected = true;
				else
					document.frmSettings2.MonthWeekDay[rpary[9]-1].selected = true;
				document.frmSettings2.MonthGap2.value = (rpary[10] == -1) ? "" : rpary[10];
				break;
		}
	} 
	else 
	{
		document.frmSettings2.MEveryMthR1.checked = true;
		document.frmSettings2.MonthDayNo.value = "";
		document.frmSettings2.MonthGap1.value = "";
		document.frmSettings2.MonthWeekDayNo[0].checked = true;
		document.frmSettings2.MonthWeekDay[0].checked = true;
		document.frmSettings2.MonthGap2.value = "";
	}
}




</script> 

<%--Yearly Script--%>
<script type="text/javascript">

function summaryyearly()
{
	ymd  = Trim(document.frmSettings2.YearMonthDay.value);
	rp  = "4&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&";
	rp += (document.frmSettings2.YEveryYr1.checked) ? ("1&" + (document.frmSettings2.YearMonth1.selectedIndex + 1) + "&" + ((ymd == "") ? "-1" : ymd) + "&-1&-1&-1") 
		: ("2&-1&-1&" + (document.frmSettings2.YearMonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonthWeekDay.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonth2.selectedIndex + 1));
	
	document.frmSettings2.RecurPattern.value = rp;
}

function initialyearly(rp)
{   
	rpary = (rp).split("&");
	if (rpary[0] == 4) 
	{
		for (i=11; i<17; i++) 
		{
			if (!isNaN(rpary[i]))
				rpary[i] = parseInt(rpary[i], 10);
			else
				rpary[i] = -1;
		}
		switch (rpary[11]) 
		{
			case 1:
				document.frmSettings2.YEveryYr1.checked = true;
				if (rpary[12] == -1)
					document.frmSettings2.YearMonth1[0].selected = true;
				else
					document.frmSettings2.YearMonth1[rpary[12]-1].selected = true;
				document.frmSettings2.YearMonthDay.value = (rpary[13] == -1) ? "" : rpary[13];
				document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
				document.frmSettings2.YearMonthWeekDay[0].selected = true;
				document.frmSettings2.YearMonth2[0].selected = true;
				break;
			case 2:
				document.frmSettings2.YEveryYr2.checked = true;
				document.frmSettings2.YearMonth1[0].selected = true;
				document.frmSettings2.YearMonthDay.value = "";
				if (rpary[14] == -1)
					document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
				else
					document.frmSettings2.YearMonthWeekDayNo[rpary[14]-1].selected = true;
				if (rpary[15] == -1)
					document.frmSettings2.YearMonthWeekDay[0].selected = true;
				else
					document.frmSettings2.YearMonthWeekDay[rpary[15]-1].selected = true;
				if (rpary[16] == -1)
					document.frmSettings2.YearMonth2[0].selected = true;
				else
					document.frmSettings2.YearMonth2[rpary[16]-1].selected = true;
				break;
		}
	} 
	else 
	{
		document.frmSettings2.YEveryYr1.checked = true;
		document.frmSettings2.YearMonth1[0].selected = true;
		document.frmSettings2.YearMonthDay.value = "";
		document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
		document.frmSettings2.YearMonthWeekDay[0].selected = true;
		document.frmSettings2.YearMonth2[0].selected = true;
	}
}
</script>

<%--Custom Script--%>
<script  type="text/javascript" language="JavaScript">

function removedate(cb)
{
	if (cb.selectedIndex != -1) {
	    cb.options[cb.selectedIndex] = null;
	}
	cal.refresh();
}

function initialcustomly(seldates)
{
	if (seldates != "") 
	{	
		seldatesary = seldates.split("&");
	
		cb = document.getElementById("CustomDate");
		//cb = document.getElementById("StartDate");
		if(seldatesary[0].length > 2)
		{
		    for (var i = 0; i < seldatesary.length; i++)
			    chgOption(cb, seldatesary[i], seldatesary[i], false, false)	
    	
		    if (cal)	
			    cal.refresh();
	    }
	}
}

function summarycustomly()
{
	var selecteddate = "";
	dFormat = document.getElementById("HdnDateFormat").value;	
	SortDates();
	cb = document.getElementById("CustomDate");
	for (i=0; i<cb.length; i++)
		selecteddate += cb.options[i].value + "&";

	document.frmSettings2.CutomDates.value = (selecteddate == "") ? "" : selecteddate.substring(0, selecteddate.length-1);
}

function isOverInstanceLimit(cb)
{
	csl = parseInt("<%=CustomSelectedLimit%>", 10); // ZD 101722

	if (!isNaN(csl)) {
		if (cb.length >= csl) {
			alert(EN_211)
			return true;
		}
	}
	
	return false;
}

function SortDates()
{
	var temp;

	datecb = document.frmSettings2.CustomDate;
	
	var dateary = new Array();

	for (var i=0; i<datecb.length; i++) {
		dateary[i] = datecb.options[i].value;
		
		dateary[i] = ( (parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10) ) + "/" + ( (parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10) ) + "/" + ( parseInt(dateary[i].split("/")[2], 10) );
	}

	for (i=0;i<dateary.length-1;i++)
	 	for(j=i+1;j<dateary.length;j++)
			if (mydatesort(dateary[i], dateary[j]) > 0)
			{
				temp = dateary[i];
				dateary[i] = dateary[j];
				dateary[j] = temp;	
			}
            

	for (var i=0; i<dateary.length; i++) {
		datecb.options[i].text = dateary[i];
		datecb.options[i].value = dateary[i];		
	} 

return false;
}

//-->
</script>

<%--Submit Recurence --%>
<script type="text/javascript">

function SubmitRecurrence()
{
    //ZD 100875
    var args = "";
    var aa ="";
    if(SubmitRecurrence.arguments.length > 0)
    {
        args = SubmitRecurrence.arguments;
        aa = args[0];
    }
    
    if(document.getElementById("hdnSaveData") != null)
        document.getElementById("hdnSaveData").value = "1";
    //ZD 103624
    
    if(document.getElementById("confStartTime_Text"))
    {
        var sDate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value); //FB 2588
        var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value); //FB 2588
        if(!validateStartEndDate(sDate,eDate,1,1))
            return false;
    }
    if(aa == "1")
        return true;
        
    var chkrecurrence = document.getElementById("chkRecurrence");
    
    if(chkrecurrence != null && chkrecurrence.checked == true)
    {
        if (validateConfDuration())    //Buffer zone
        {
            if(summary())
                return true;
        }
        return false;        
    }
    
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") //FB Case 951
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "120";
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") // FB Case 951
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

/* Methods added for Buffer zone - start */
function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    
    if (obj.value == "") //FB Case 951
         obj.value = "0";
         
    if (obj1.value == "") //FB Case 951
        obj1.value = "0";
   
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value, 10) * 60; // ZD 101722
    totDur = totDur + parseInt(obj1.value, 10); // ZD 101722
    
    if (totDur < 0  ) // || totDur > eval(maxdur*60)) //ZD  103925
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}


/* *** Methods added for buffer zone *** -- End */

function summary()
{
     SetRecurBuffer();//SJV BufferZone Fix
    
    switch(document.frmSettings2.RecPattern.value)
    {
        case "1":
		    summarydaily();
		    break;
		case "2":	
		    summaryweekly();
		    break;
        case "3":
		    summarymonthly();
		    break;
	    case "4":
		    summaryyearly();
		    break;
        case "5":
		    summarycustomly();
		    if(document.frmSettings2.CutomDates.value != "")
		    {
		        var cuDates = document.frmSettings2.CutomDates.value.split("&");
		        if(cuDates.length <=1)
		        {
		            alert(InstanceCheck);
		            return false;   
		        }
		    }
		    break;
    }
    
     if(document.frmSettings2.RecPattern.value != "5" && document.frmSettings2.REndAfter.checked)
    {
       if(document.frmSettings2.Occurrence.value <= 1)
       {
           alert(InstanceCheck);
           document.frmSettings2.Occurrence.focus();
           return false;   
       }
    }

    //FB 2634
	//var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	//FB 2558
    ChangeTimeFormat("D")
	var cstartdate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("hdnStartTime").value);
                
    var dura = parseInt(document.getElementById("SetupDuration").value,10)    
	//var sttime = setCDuration(cstartdate, -dura); //ZD 100085
	var sttime = cstartdate;
	var sttime1 = getCTime(sttime);
	var aryStart = sttime1.split(" ");
	
	var sh = aryStart[0].split(":")[0];//ZD 101837
	var sm = aryStart[0].split(":")[1];
	var ss = aryStart[1];
	var tz = document.getElementById("<%=lstConferenceTZ.ClientID%>").value;
	//FB 2634
	if('<%=Session["timeFormat"]%>' == "0" || '<%=Session["timeFormat"]%>' == "2") //FB 2588
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}

//	//code added for buffer zone -Start
//	var aryStup = document.getElementById("confStartTime_Text").value.split(" ");
//	
//	setuphr = aryStup[0].split(":")[0];
//	setupmi = aryStup[0].split(":")[1];
//	setupap = aryStup[1];

//	var aryTear = document.getElementById("confEndTime_Text").value.split(" ");
//	tearhr = aryTear[0].split(":")[0];
//	tearmi = aryTear[0].split(":")[1];
//	tearap = aryTear[1];
//	
//	if("<%=Session["timeFormat"].ToString()%>" == '0')
//	{ 
//	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
//	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi);
//        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi);
//    }
//	else
//	{
//	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
//	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi) + " " + setupap;
//        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi) + " " + tearap;
//    }
//    
//    var insStDate = '';
//    if (document.frmSettings2.RecPattern.value == "5")
//    {
//        var cb = document.getElementById("CustomDate");

//	    if(cb.length > 0)
//	    {
//		    insStDate = cb.options[0].value;
//        }
//    }
//    else
//    {
//        insStDate = document.frmSettings2.StartDate.value;
//    }
//    startdate = insStDate + " " + startTime;
//   
//    var obj = document.getElementById("RecurDurationhr");
//    var obj1 = document.getElementById("RecurDurationmi");
//    
//    var totDur = parseInt(obj.value) * 60;
//    totDur = totDur + parseInt(obj1.value);
//    
//    if(totDur > 1440)
//    {
//        alert(EN_314);
//        return false;
//    }
//    
//    var confEndDt = '';
//    var sysdate = dateAddition(startdate,"m",totDur);
//    var dateP = sysdate.getDate();
//	
//	var monthN = sysdate.getMonth() + 1;
//	var yearN = sysdate.getFullYear();
//	var hourN = sysdate.getHours();
//	var minN = sysdate.getMinutes();
//	var secN = sysdate.getSeconds();
//	var timset = 'AM';
//	
//	if("<%=Session["timeFormat"].ToString()%>" == '0')
//	{
//	    timset = '';
//	}
//	else
//	{
//	    if(hourN == 12)
//	    {
//	        timset = 'PM';
//	    }
//	    else if( hourN > 12)
//	    {
//	         timset = 'PM';
//	         hourN = hourN - 12;
//	    }
//	    else if(hourN == 0)
//	    {
//	        timset = "AM";
//	        hourN = 12;
//	    }
//	 }
//	
//	var confDtAlone = monthN + "/" + dateP + "/" + yearN;
//	
//	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
//	
//	//if(document.getElementById("chkEnableBuffer").checked == true)	
//	{
//        setupDate = insStDate + " " + setupTime;
//        if(Date.parse(setupDate) < Date.parse(startdate))
//        {
//            setupDate = confDtAlone + " " + setupTime;
//        }
//        
//        teardownDate = confDtAlone + " " + teardownTime;
//        
//        if(Date.parse(confEndDt) < Date.parse(teardownDate))
//        {
//            teardownDate = insStDate + " " + teardownTime;
//        }
//      
//        if(Date.parse(setupDate) < Date.parse(startdate))
//        {
//            alert(EN_316);
//            return false;
//        }

//        if( (Date.parse(teardownDate) < Date.parse(setupDate)) || (Date.parse(teardownDate) < Date.parse(startdate)) || (Date.parse(teardownDate) > Date.parse(confEndDt)))
//        {
//             alert(EN_317);
//             return false;
//        }
//      
//        var setupDurMin = parseInt(Date.parse(setupDate) - Date.parse(startdate)) / (1000 * 60);
//        var tearDurMin = parseInt(Date.parse(confEndDt) - Date.parse(teardownDate)) / (1000 * 60);
//      }
//    else
//    {
//        setupDate = startdate;
//        teardownDate = confEndDt;
//    }  
//    
//    if(isNaN(setupDurMin))
//        setupDurMin = 0;
//    
//    if(isNaN(tearDurMin))
//        tearDurMin = 0;
//        
//    if(setupDurMin >0 || tearDurMin > 0)
//    {
//        if( (totDur -(setupDurMin + tearDurMin)) < 15)
//        {
//            alert(EN_314);
//            return false;
//        }
//    }
//    
//    document.frmSettings2.hdnBufferStr.value = setupTime + "&" + teardownTime;
//	document.frmSettings2.hdnSetupTime.value = setupDurMin;
//	document.frmSettings2.hdnTeardownTime.value = tearDurMin;
//		
	//code added for buffer zone -End
	
	if (document.frmSettings2.RecurDurationhr)
		dr = parseInt(document.frmSettings2.RecurDurationhr.value, 10) * 60 + parseInt(document.frmSettings2.RecurDurationmi.value, 10);

    //FB 2634
	if (dr < 15) 
    {
	    alert(EN_31);
	    return (false);		
	}
	//ZD 100085
    //dr = dr + parseInt(document.getElementById("SetupDuration").value,10) + parseInt(document.getElementById("TearDownDuration").value,10);
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	if ( dr < 15 ) {
		alert(EN_31);
		
		if (document.frmSettings2.RecurDurationhr)
			document.frmSettings2.RecurDurationhr.focus();
		if (document.frmSettings2.RecurEndhr)
			document.frmSettings2.RecurEndhr.focus();
			
		return (false);		
	}

	
	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	
	recurrange = document.frmSettings2.StartDate.value + "&" + (document.frmSettings2.EndType.checked ? "1&-1&-1" : ( document.frmSettings2.REndAfter.checked ? ("2&" + document.frmSettings2.Occurrence.value + "&-1") : (document.frmSettings2.REndBy.checked ? ("3&-1&" + document.frmSettings2.EndDate.value) : "-1&-1&-1") ));
	rv += (document.frmSettings2.RecPattern.value == 5) ? ("5#" + document.frmSettings2.CutomDates.value) : (document.frmSettings2.RecurPattern.value + "#" + recurrange);
	if (frmRecur_Validator(document.frmSettings2.RecurPattern.value, recurrange, (document.frmSettings2.RecPattern.value == 5)?true:false)) {
		document.getElementById("Recur").value = rv;
	
		if (document.frmSettings2.EndText)
			endvalue = document.frmSettings2.EndText.value;
		if (document.frmSettings2.RecurEndhr)
			endvalue = document.frmSettings2.RecurEndhr.value + ":" + document.frmSettings2.RecurEndmi.value + " " + document.frmSettings2.RecurEndap.value;

		//merging Conference
		//FB 2699
		document.getElementById("TimeZoneText").value = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].text;
		document.getElementById("RecurringText").value = recur_discription(rv, endvalue, document.getElementById("TimeZoneText").value, document.frmSettings2.StartDate.value,"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
		//FB 2558
		if('<%=Session["timeFormat"].ToString()%>' == '2')
		    document.getElementById("RecurringText").value = document.getElementById("RecurringText").value.replace(" AM","Z").replace(" PM","Z").replace(":","")
		isRecur();
		
		BtnCheckAvailDisplay ();
		
		return true;
	}
}


function frmRecur_Validator(rp, rr, isCustomly)
{
    //ZD 101837
    var ConfRecurLimit = <%=Session["ConfRecurLimit"]%>;
    if(document.getElementById('chkWebex') != null)
    {
        if(document.getElementById('chkWebex').checked == true)
        {
            if(ConfRecurLimit > 50)
                ConfRecurLimit = 50;
        }
    }
	if (parseInt(document.frmSettings2.Occurrence.value, 10) > parseInt(ConfRecurLimit, 10)) // ZD 101722 //ZD 101837
	{    
		alert(MaxLimit4 + ConfRecurLimit +MaxLimit5);
		return false;
	}
	
	if (isCustomly) {
		if (document.frmSettings2.CutomDates.value == "") {
			alert(EN_193)
			return false;
		} else
			return true;
	}

	if (chkPattern(rp))
		if (chkRange(rr))
			return true;
		else
			return false;
	else
		return false;
}


function chkPattern(rp)
{
	rpary = rp.split("&");
	
	switch (rpary[0]) {
		case "1":
			for (i=3; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}

			switch (rpary[1]) {
				case "1":
					rst = isPositiveInt (rpary[2], "interval");
					if (rst == 1)
						return true;
					else
						return false;
					break;
				case "2":
					if (rpary[2] == "-1")
						return true;						
					else {
						alert(EN_37);
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;

		case "2":
			for (i=1; i<3; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=5; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			rst = isPositiveInt (rpary[3], "weeks interval");
			if (rst == 1) {
				if (rpary[4]!="")
					return true;
				else {
					alert(EN_107);
					return false;
				}
			}
			break;
			
		case "3":
			for (i=1; i<5; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=11; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			
			switch (rpary[5]) {
				case "1":
					for (i=8; i<11; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if ( (isPositiveInt (rpary[6], "days interval") == 1) && (isPositiveInt (rpary[7], "months interval") == 1) )
						if ( isMonthDayNo(parseInt(rpary[6], 10)) )
							return true;
						else
							return false;
					else
						return false;
					break;
				case "2":
					for (i=6; i<8; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt(rpary[10], "months interval") == "1")
						return true;						
					else {
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
			
		case "4":
			for (i=1; i<11; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			switch (rpary[11]) {
				case "1":
					for (i=14; i<rpary.length; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt (rpary[13], "date of the month") == 1)
						if ( isYearMonthDay(parseInt(rpary[12], 10), parseInt(rpary[13], 10)) )
							return true;
						else
							return false;
					else
						return false;						
					break;
				case "2":
					for (i=12; i<14; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					return true;
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}
}

var british = false;//FB 1073

function chkRange(rr)
{
	rrary = rr.split("&");
    
    //if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')//FB 1073 104097
       // british = true;//FB 1073
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     
	if ( (!isValidDate(dDate)) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmSettings2.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmSettings2.Occurrence.focus();
					return false;
				}
			}
			break;
		case "3":
			if (rrary[2]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( (!isValidDate(GetDefaultDate(rrary[3],dFormat))) ||  (caldatecompare(GetDefaultDate(rrary[3],dFormat), servertoday)== -1) ) {//104097
					alert(EN_108);
					document.frmSettings2.EndDate.focus();
					return false;
				} else
			    {
                   //ZD 104854 - Start
			       var fstdate = GetDefaultDate(rrary[0],dFormat);  //FB 2366
			       var snddate = GetDefaultDate(rrary[3],dFormat);
				   //ZD 104854 - End
//			    	if(!british) 104097
//				     {
//				        fstdate = GetDefaultDate(rrary[0],"dd/MM/yyyy");
//				        snddate = GetDefaultDate(rrary[3],"dd/MM/yyyy");				         
//				     }
					if ( !dateIsBefore(fstdate,snddate) ) {
						alert(EN_109);
						document.frmSettings2.EndDate.focus();
						return false;
					} else
						return true;
				}
			}
			return false;
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{		
	if ( (e = document.getElementById("btnCheckAvailDIV")) != null ) {
		e.style.display = (document.getElementById("Recur").value=="") ? "" : "none";
	}
}
//FB 2634
function fnClear()
{
    var args = fnClear.arguments;
    
    var txtCtrl = document.getElementById(args[0]);
    
    if(txtCtrl)
    {
        if(txtCtrl.value == "")
            txtCtrl.value = "0";
    }
}

</script>

<script>
function removerecur()
{
	//FB 2634
    if(document.getElementById("RecurSpec").value == "")
    {
	    document.getElementById("Recur").value=""; 
	    //document.getElementById("RecurText").value = "no recurrence";
	    document.getElementById("RecurringText").value = "";
	    document.getElementById("hdnRecurValue").value = "";
	    document.getElementById('RecurValue').value = "";
	}
	//isRecur(); 
	//initial();
    
    return false;
}

//function goToCallist()
//{
//        if(document.getElementById("lstCalendar") != null)
//        {
//            if (document.getElementById("lstCalendar").value == "1"){
//			    window.location.href = "ConferenceSetup.aspx?t=n&op=1" ; 
//		    }
//		    
//		    if (document.getElementById("lstCalendar").value == "2"){
//			    window.location.href = "ConferenceSetup.aspx?t=n&op=2" ; 
//		    }
//		    
//		    if (document.getElementById("lstCalendar").value == "3"){
//			    window.location.href = "ManageTemplate.aspx" ; 
//		    }
//		}
//        
//		
//}

</script>

<script type="text/javascript">
// FB 2693
function fnPCconf()
{
    if (document.getElementById("chkPCConf").checked)
        document.getElementById("tblPcConf").style.display = "";
    else
        document.getElementById("tblPcConf").style.display = "none";
        
    DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>","<%=enableBufferZone%>"); //FB 2998
}

//ZD 100443 start
function fnCompareSetup(sender, args) {
    var setupDur = 0; var mcupreStart = 0;
    if (document.getElementById("SetupDuration") != null)
        setupDur = parseInt(document.getElementById("SetupDuration").value, 10);
    if (document.getElementById("txtMCUConnect") != null)
        mcupreStart = parseInt(document.getElementById("txtMCUConnect").value, 10);
	//ZD 102392 Starts
    var enableSetUpDisplay = '<%=Session["EnableSetupTimeDisplay"]%>';

    if (setupDur != 0) 
    {
        if (setupDur < mcupreStart && setupDur != mcupreStart) {
            if (enableSetUpDisplay == 0) {
                document.getElementById("customMCUPreStart").style.display = 'block';
            }
            else {
                document.getElementById("customMCUPreStart").style.display = 'none';
                document.getElementById("customSetup").style.display = 'block';
            }
            return args.IsValid = false;
        }
        else {
            document.getElementById("customMCUPreStart").style.display = 'none';
            document.getElementById("customSetup").style.display = 'none';
            return args.IsValid = true;
        }
    }
    else {
        document.getElementById("customMCUPreStart").style.display = 'none';
        document.getElementById("customSetup").style.display = 'none';
        return args.IsValid = true;
    }
	//ZD 102392 End
}
function fnCompareTear(sender, args) {
    var TearDown = 0; var mcupreEnd = 0;
    if (document.getElementById("TearDownDuration") != null)
        TearDown = parseInt(document.getElementById("TearDownDuration").value, 10);
    if (document.getElementById("txtMCUDisConnect") != null)
        mcupreEnd = parseInt(document.getElementById("txtMCUDisConnect").value, 10);
	//ZD 102392 Starts
    var enableTearDownDisplay = '<%=Session["EnableTeardownTimeDisplay"]%>';

    if (TearDown != 0) {
        if (TearDown < mcupreEnd && TearDown != mcupreEnd) {
            if (enableTearDownDisplay == 0) {
                document.getElementById("customMCUPreEnd").style.display = 'block';
            }
            else {
                document.getElementById("customMCUPreEnd").style.display = 'none';
                document.getElementById("customTearDown").style.display = 'block';
            }
            return args.IsValid = false;
        }
        else {
            document.getElementById("customMCUPreEnd").style.display = 'none';
            document.getElementById("customTearDown").style.display = 'none';
            return args.IsValid = true;
        }
    }
    else {
        document.getElementById("customMCUPreEnd").style.display = 'none';
        document.getElementById("customTearDown").style.display = 'none';
        return args.IsValid = true;
    }
	//ZD 102392 End

}
//ZD 100443 End
</script>
<%--Merging Recurrence script End here--%>


<%--FB 2693--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/pcconference.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server"></head>
<body>
    <form id="frmSettings2" name="frmSettings2" runat="server" autocomplete="off" method="post" enctype="multipart/form-data"> <%--FB 1931--%><%--ZD 100420--%><%--ZD 101190--%>
    <div>
  <%--<asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager>--%>
      <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>

                 
     <%--ZD 100568 21-12-2013 Inncrewin--%>
        <div id="modalDivPopup" style="display: none; position: fixed; z-index: 1000; left: 0px;
                                                                    top: 0px; width: 100%; height: 1000px; background-color: Gray; opacity: 0.5;">
                                                                </div>
        <div id="modalDivContent" style="display: none; left: 5%; position: fixed; top: 30%;
                                                                    padding-bottom: 0.5%; padding-top: 0.5%; z-index: 10000; background-color: White;
                                                                    width: 90%;">
                                                                    <table border="0" width="98%" cellpadding="5">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceTime%>" runat="server"></asp:Literal>
                                                                            </td>
                                                                            <td bgcolor="#65FF65">
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ConferenceSetup_TotallyFree%>" runat="server"></asp:Literal>
                                                                            </td>
                                                                            <td bgcolor="#F8F075">
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_PartiallyFree%>" runat="server"></asp:Literal>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <table id="tblSeatsAvailability" cellpadding="4" cellspacing="0" runat="server" height="50%"
                                                                                    width="100%" border="1">
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <input type="button" align="middle" id="btnClose" runat="server" onclick="javascript:fnPopupSeatsClose(); return false;"
                                                                        value="<%$ Resources:WebResources, Close%>" class="altShortBlueButtonFormat" />
                                                                    <br />
                                                                </div>       
        <div id="modalDivContentforConf" style="display: none; left: 13%; position: fixed;border:2px solid #a1a1a1;border-radius:25px;
            top: 30%; padding-bottom: 0.5%; padding-top: 0.5%; z-index: 10000; background-color: White;
            width: 70%;height:150px;">
            <div>
                <p style=" font-size: 12pt;left:10px; position:relative;"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ExpressConference_seatlimit%>" runat="server"></asp:Literal></p>
            </div>
            <div style="height: 30%;">
            </div>
            <div>
                <table style="width: 100%;">
                    <tr>
                       <td align="right"> <%--ZD 100875--%>
                            <asp:Button ID="Button1" runat="server" style="width: 230px;" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';fnPopupConfAlertClose();"
                            OnClick= "CheckSeatsAvailability" Text="<%$ Resources:WebResources, ExpressConference_CheckAvailableSeat%>" /> 
                             <input style=" width: 80px;" type="button" runat="server" onclick="javascript:fnPopupConfAlertClose(); return false;"
                                value="<%$ Resources:WebResources, Cancel%>" class="altShortBlueButtonFormat" />                       
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
        <%--ZD 100568 21-13-2013 Inncrewin--%>            
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <%--<tr>
            <td width="30%" align="left" nowrap  class="blackblodtext">
            <table>
            <tr>
            <td colspan="2">
            <b>Meeting Type:</b>
            <select id="lstCalendar" name="lstCalendar" class="altText" size="1" onChange="goToCallist()" runat="server">
                    <option value="1">Future</option>
                    <option value="2">Immediate</option>
                    <option value="3">From Template</option>
                </select>
            </td>
            
            </tr>
            
            
            </table> 
			    
            </td>
    
        </tr>--%>
        <tr>
            <td>
            <table width="100%">
              <tr>
                <td align="center" style="height: 21px">
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <asp:HiddenField ID="emailClient" runat="server" />
                    <asp:HiddenField ID="hostEmail" runat="server" />
                    <input type="hidden" runat="server" id="CreateBy" value="" />
                    <input type="hidden" name="ModifyType" value="0"/> <%--Edited for FF--%>
                    <input type="hidden" name="Recur" id="Recur" runat="server" />
                    <input id="confPassword" runat="server" type="hidden" />
                    <input runat="server" id="RecurFlag" type="hidden" />
                    <input runat="server" id="selectedloc" type="hidden" />
                    <input runat="server" id="selectedTierList" type="hidden" /><%-- ZD 102650--%>      
                    <input type="hidden" runat="server" id="hdnSaveData"  /><%-- ZD 100875--%>
                    <input type="hidden" runat="server" id="hdnConfName" /><%-- ZD 100875--%>
                    <input ID="RecurringText" runat="server" type="hidden" />
                    <input type="hidden" name="ConfID" value=""/>
                    <input type="hidden" name="NeedInitial" value="2"/>
                    <input type="hidden" name="outlookEmails" value=""/>
                    <input type="hidden" name="lotusEmails" value=""/>
                    <input type="hidden" name="hdnRecurDiv" id="hdnRecurDiv" runat="server" /><%--FB 2770--%>
                    <input type="hidden" name="hdnSetupTime" id="hdnSetupTime" runat="server" /> 
                    <input type="hidden" name="hdnTeardownTime" id="hdnTeardownTime" runat="server" />
                    <input type="hidden" name="hdnBufferStr" id="hdnBufferStr" runat="server" />
                    <input type="hidden" name="hdnextusrcnt" id="hdnextusrcnt" runat="server" /> <%-- API PortNo --%>
                    <%--<input type="hidden" name="hdnConceirgeSupp" id="hdnConceirgeSupp" runat="server" />--%> <%--FB 2023--%><%--FB 2377--%>
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                     </div><%--ZD 100678--%>
                    <asp:DropDownList ID="lstServices" Visible="false" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadCateringServices" ></asp:DropDownList>
                    
                    <%--Merging Recurrence - Start--%>
                    <input type="hidden" id="hdnOldTimezone" name="hdnOldTimezone" runat="server" value=""/> <%--FB 2699--%>
                    <input type="hidden" id="TimeZoneText" name="TimeZoneText" runat="server" value=""/> <%--FB 2699--%>
                    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value=""/>
                    <input type="hidden" id="EndDateText" name="EndDateText" value=""/>
                    <input type="hidden" id="RecurValue" name="RecurValue" value=""/>
                    <input type="hidden" id="RecurPattern" name="RecurPattern" value=""/>
                    <input type="hidden" id="CustomDates" name="CutomDates" value=""/>
                    <input type="hidden" id="RecPattern" name="RecPattern" value=""/>
                    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" /> 
                    <input type="hidden" id="hdnValue" runat="server"/>
                    <input type="hidden" id="hdnRecurValue" runat="server"/>
                    <%--Merging Recurrence - End--%>
                    <%--FB 1716--%>
                    <input runat="server" id="hdnChange" type="hidden" />
                    <input runat="server" id="hdnDuration" type="hidden" />
                    <input runat="server" name="hdnemailalert" id="hdnemailalert" type="hidden" /> <%--FB 1830 Email Edit--%>
					<input type="hidden" name="Recur" id="hdnParty" runat="server" /> <%--FB 1865--%>
					<%--FB 1911--%>
                    <input runat="server" id="hdnSpecRec" type="hidden" />
                    <input type="hidden" name="Recur" id="RecurSpec" runat="server" />
                    <%--FB 1985--%>
                    <input type="hidden" name="hdnMeetLinkSt" id="hdnMeetLinkSt" runat="server" />
                    <input type="hidden" name="hdnAudioBridges" id="hdnAudioBridges" runat="server" /><%--FB 2359--%>
                    <input type="hidden" name="hdnAVParamState" id="hdnAVParamState" runat="server" /><%--FB 2359--%>
					<%--FB 2274 Starts--%>
                    <input type="hidden" name="hdnCrossrecurEnable" id="hdnCrossrecurEnable" runat="server" />
                    <input type="hidden" name="hdnCrossdynInvite" id="hdnCrossdynInvite" runat="server" />
                    <input type="hidden" name="hdnCrossroomModule" id="hdnCrossroomModule" runat="server" />
                    <input type="hidden" name="hdnCrossfoodModule" id="hdnCrossfoodModule" runat="server" />
                    <input type="hidden" name="hdnCrosshkModule" id="hdnCrosshkModule" runat="server" />
                    <input type="hidden" name="hdnCrossisVIP" id="hdnCrossisVIP" runat="server" />
                    <input type="hidden" name="hdnCrossEnableRoomServiceType" id="hdnCrossEnableRoomServiceType" runat="server" />                   
                    <input type="hidden" name="hdnCrossisSpecialRecur" id="hdnCrossisSpecialRecur" runat="server" />
                    <input type="hidden" name="hdnCrossConferenceCode" id="hdnCrossConferenceCode" runat="server" />
                    <input type="hidden" name="hdnCrossLeaderPin" id="hdnCrossLeaderPin" runat="server" />
                    <input type="hidden" name="hdnCrossPartyCode" id="hdnCrossPartyCode" runat="server" /> <%--ZD 101446--%>
                    <input type="hidden" name="hdnCrossAdvAvParams" id="hdnCrossAdvAvParams" runat="server" />
                    <input type="hidden" name="hdnCrossEnableBufferZone" id="hdnCrossEnableBufferZone" runat="server" />
                    <input type="hidden" name="hdnCrossEnableEntity" id="hdnCrossEnableEntity" runat="server" />
                    <input type="hidden" name="hdnCrossAudioParams" id="hdnCrossAudioParams" runat="server" />
                    <input type="hidden" name="hdnCrossdefaultPublic" id="hdnCrossdefaultPublic" runat="server" />
                    <input type="hidden" name="hdnCrossP2PEnable" id="hdnCrossP2PEnable" runat="server" />
                    <input type="hidden" name="hdnCrossEnableRoomConfType" id="hdnCrossEnableRoomConfType" runat="server" />
                    <input type="hidden" name="hdnCrossEnableHotdeskingConfType" id="hdnCrossEnableHotdeskingConfType" runat="server" /><%--ZD 100719--%>
                    <input type="hidden" name="hdnCrossEnableAudioVideoConfType" id="hdnCrossEnableAudioVideoConfType" runat="server" />
                    <input type="hidden" name="hdnCrossDefaultConferenceType" id="hdnCrossDefaultConferenceType" runat="server" />
                    <input type="hidden" name="hdnCrossEnableAudioOnlyConfType" id="hdnCrossEnableAudioOnlyConfType" runat="server" />
                    <input type="hidden" name="hdnCrossenableAV" id="hdnCrossenableAV" runat="server" />
                    <input type="hidden" name="hdnCrossenableParticipants" id="hdnCrossenableParticipants" runat="server" />
                    <input type="hidden" name="hdnCrossisMultiLingual" id="hdnCrossisMultiLingual" runat="server" />
                    <input type="hidden" name="hdnCrossroomExpandLevel" id="hdnCrossroomExpandLevel" runat="server" />
                    <input type="hidden" name="hdnCrossEnableImmConf" id="hdnCrossEnableImmConf" runat="server" />
                    <input type="hidden" name="hdnCrossEnableAudioBridges" id="hdnCrossEnableAudioBridges" runat="server" />
                    <input type="hidden" name="hdnCrossEnablePublicConf" id="hdnCrossEnablePublicConf" runat="server" />
                    <input type="hidden" name="hdnCrossEnableConfPassword" id="hdnCrossEnableConfPassword" runat="server" />
                    <input type="hidden" name="hdnCrossEnableRoomParam" id="hdnCrossEnableRoomParam" runat="server" />
                    <input type="hidden" name="hdnCrossAddtoGroup" id="hdnCrossAddtoGroup" runat="server" />
					<input type="hidden" name="hdnCrossEnablePC" id="hdnCrossEnablePC" runat="server" /><%--FB 2348--%>
					<input type="hidden" name="hdnCrossEnableSurvey" id="hdnCrossEnableSurvey" runat="server" /><%--FB 2348--%>
                    <%--FB 2274 Ends--%>
					<input type="hidden" id="hdnSelectVMRRoom" runat="server" value=""  /> <%--FB 2448--%>
					<input type="hidden" id="isVMR" runat="server" value=""  /> <%--FB 2376--%>
					<input type="hidden" name="hdnCrossSetupTime" id="hdnCrossSetupTime" runat="server" /><%--FB 2398--%>
					<input type="hidden" name="hdnCrossTearDownTime" id="hdnCrossTearDownTime" runat="server" /><%--FB 2398--%>
					<input type="hidden" name="hdnCrossMeetGreetBufferTime" id="hdnCrossMeetGreetBufferTime" runat="server" /><%--FB 2632--%>
					<input type="hidden" runat="server" id="hdnGuestRoom" /><%--FB 2426--%>
					<input type="hidden" runat="server" id="hdnGuestRoomID" /><%--FB 2426--%>
					<input type="hidden" runat="server" id="hdnGuestloc" /><%--FB 2426--%>
					<input type="hidden" runat="server" id="hdnTxtMsg" /><%--FB 2486--%>
					<input type="hidden" runat="server" id="hdnSmartP2PTotalEps" /><%--FB 2430--%>
					<input type="hidden" runat="server" id="hdnCrossEnableSmartP2P" /><%--FB 2430--%>
					<input type="hidden" runat="server" id="hdnconftype" /><%--FB 2430--%>
					<input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--FB 2588--%>
					<input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--FB 2588--%>
					<input type="hidden" runat="server" id="hdnCrossEnableLinerate" name="hdnCrossEnableLinerate" /> <%--FB 2641--%>
					<input type="hidden" runat="server" id="hdnCrossEnableStartMode" name="hdnCrossEnableStartMode" /><%-- FB 2641--%>
					<input type="hidden" runat="server" id="hdnConferenceName" name="hdnConferenceName" /><%-- FB 2694--%>
					<input type="hidden" name="hdnOverBookConf" id="hdnOverBookConf" runat="server" /><%--FB 2659--%>
					<input type="hidden" name="hdnconfUniqueID" id="hdnconfUniqueID" runat="server" /><%--FB 2659--%>
					<input type="hidden" id="isPCCOnf" runat="server" value=""  /> <%--FB 2819--%>
					<input type="hidden" name="hdnEnableNumericID" id="hdnEnableNumericID" runat="server" /><%--FB 2870--%>
					<input type="hidden" name="hdnEnableProfileSelection" id="hdnEnableProfileSelection" runat="server" /><%--FB 2947--%>
                    <input type="hidden" name="hdnEnablePoolOrderSelection" id="hdnEnablePoolOrderSelection" runat="server" /><%--ZD 104256--%>
					<input type="hidden" id="hdnMCUConnectDisplay" runat="server" /><%--FB 2998--%>
					<input type="hidden" id="hdnMCUDisconnectDisplay" runat="server" /><%--FB 2998--%>
					<input type="hidden" id="hdnMCUSetupTime" runat="server" /><%--FB 2998--%>
					<input type="hidden" id="hdnMCUTeardownTime" runat="server" /><%--FB 2998--%>
					<input type="hidden" name="hdnNetworkSwitching" id="hdnNetworkSwitching" runat="server" /><%--FB 2993--%>
					<input type="hidden" runat="server" id="hdnicalID" name="hdnicalID" /><%--2457 exchange round trip--%>
                    <input type="hidden" runat="server" id="hdnconfOriginID" name="hdnconfOriginID" /><%--2457 exchange round trip--%>	
                    <input type="hidden" name="hdnWebExCOnf" id="hdnWebExCOnf" runat="server" /><%--ZD 100221--%>	
                    <input type="hidden" id="hdnPasschange" value="false" runat="server"/> <%--ZD 100221--%>
                    <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--ZD 100221--%>
                    <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--ZD 100221--%>
                    <input type="hidden" id="hdnWebExPwd" value="" runat="server"/> <%--ZD 100221--%>
                    <input type="hidden" id="hdnEnableWebExIngt" value="" runat="server" /> <%--ZD 100935--%>
					<input type="hidden" runat="server" id="hdnGoogleGUID" name="hdnGoogleGUID" /> <%--ZD 100152--%>
                    <input type="hidden" runat="server" id="hdnGoogleSequenceNumber" name="hdnGoogleSequenceNumber" /> <%--ZD 100152--%>
                    <input type="hidden" runat="server" id="hdnFileUpload" name="hdnFileUpload" /> <%--ZD 101052--%>
                    <input type="hidden" runat="server" id="hdnROWID" name="hdnROWID" /> <%--ZD 100619--%>
					<input type="hidden" runat="server" id="hdnGLWarningPopUp" name="hdnGLWarningPopUp" /> <%--ZD 101120--%>
                    <input type="hidden" runat="server" id="hdnGLApprTime" name="hdnGLApprTime" /> <%--ZD 101120--%> 
                    <input type="hidden" runat="server" id="hdnGLConfTime" name="hdnGLConfTime" /> <%--ZD 101120--%> 
                    <input type="hidden" runat="server" id="hdnGLCount" name="hdnGLCount" /> <%--ZD 101120--%> 
                    <input type="hidden" runat="server" id="hdnVMRPINChange" name="hdnVMRPINChange" /> <%--ZD 100522--%> 
                    <input type="hidden" runat="server" id="hdnPINlen" name="hdnPINlen" /> <%--ZD 100522--%> 
                    <input type="hidden" runat="server" id="hdnROOMPIN" value="0" /> <%--ZD 100522--%>
                    <input type="hidden" runat="server" id="hdnRecurToggle" value="0" /> <%--ZD 101815--%>
					<input type="hidden" runat="server" id="isIPAddress" name="isIPAddress" /> <%--ZD 101815--%> 
                    <input type="hidden" runat="server" id="isTestedModel" name="isTestedModel" /> <%--ZD 101815--%> 
                    <input type="hidden" runat="server" id="hdnSmartP2PNotify" name="SmartP2PNotify" /> <%--ZD 101815--%> 
                    <input type="hidden" runat="server" id="isP2PGL" name="isP2PGL" /> <%--ZD 101815--%> 
                    <input type="hidden" runat="server" id="hdnUpdateAV" value="0" name="hdnUpdateAV" /> <%--ZD 101815--%> 
                    <input type="hidden" runat="server" id="hdnAVWOAlert" value="0" name="hdnAVWOAlert" /> <%--ZD ZD 101228--%> 
                    <input type="hidden" runat="server" id="hdnCAtWOAlert" value="0" name="hdnCAtWOAlert" /> <%--ZD ZD 101228--%> 
                    <input type="hidden" runat="server" id="hdnFacWOAlert" value="0" name="hdnFacWOAlert" /> <%--ZD ZD 101228--%>  
                    <input type="hidden" runat="server" id="hdnFacWOFullFill" value="0" name="hdnFacWOFullFill" /> <%--ZD ZD 101228--%>  
                    <input type="hidden" runat="server" id="hdnCATWOFullFill" value="0" name="hdnCATWOFullFill" /> <%--ZD ZD 101228--%>  
                    <input type="hidden" runat="server" id="hdnAVWOFullFill" value="0" name="hdnAVWOFullFill" /> <%--ZD ZD 101228--%>  
					<input type="hidden" runat="server" id="hdnEnablEOBTP" name="hdnEnablEOBTP" value="0" /> <%--ZD 100513--%> 
                    <input type="hidden" runat="server" id="hdnWebExLaunch" name="hdnWebExLaunch" value="0" /> <%--ZD 100513--%> 
                    <input type="hidden" runat="server" id="hdnIsProfileChg" value="0" name="hdnIsProfileChg" /> <%--ZD ZD 101490--%>  
                    <input type="hidden" runat="server" id="hdnIsBridgeChg" value="0" name="hdnIsBridgeChg" /> <%--ZD ZD 101490--%>
                    <input type="hidden" runat="server" id="hdntemplatebooking" value="0" name="hdntemplatebooking" /> <%--ZD 101562--%>   
					<input type="hidden" id="hdnVMRId" runat="server" value=""  /> <%--ZD 101561--%>
                    <input type="hidden" runat="server" id="hdnSetuptimeDisplay" value="0" name="hdnSetuptimeDisplay" /> <%--ZD 101755 --%>
                    <input type="hidden" runat="server" id="hdnTeardowntimeDisplay" value="0" name="hdnTeardowntimeDisplay" /> <%--ZD 101755 --%>	
					<input type="hidden" runat="server" id="hdnActMsgDelivery" value="0" name="hdnActMsgDelivery" /> <%--ZD 101757--%>   
                    <%--ZD 101871 start--%>
                    <input id="hdnPolycomMGC" type="hidden" runat="server" value ="0" name="hdnPolycomMGC" />
                    <input id="hdnPolycomRMX" type="hidden" runat="server" value ="0" name="hdnPolycomRMX" />
                    <input id="hdnLOSelection" type ="hidden" runat="server" value="0" name ="hdnLOSelection" />
                    <%--ZD 101871 End--%>
					<input id="hdnCodian" type="hidden" runat="server" value ="0"  name="hdnCodian"/> <%--ZD 101869--%>
                    <input id="hdnShowVideoLayout"  type="hidden"  runat="server" value="0" name="hdnShowVideoLayout" /> <%--ZD 101931--%>
					<input type="hidden" runat="server" id="hdnIsRecurConference" value="0" name="hdnIsRecurConference" /> <%--ZD 102963--%>                    
                    <input type="hidden" runat="server" id="hdnChgCallerCallee" name="hdnChgCallerCallee" value="0" /> <%--ZD 103402--%>
                    <input type="hidden" runat="server" id="hdnSelEntityCodeVal" name="hdnSelEntityCodeVal" /> <%--ZD 102909--%>
					<input type="hidden" runat="server" id="hdnRemoveEntityVal" name="hdnRemoveEntityVal" /> <%--ZD 103265--%>
					<input type="hidden" name="hdnRecurType" id="hdnRecurType" runat="server" /> <%--ZD 103929--%>
                    <input type="hidden" runat="server" id="hdnisBJNConf" value="0" name="hdnisBJNConf" /><%--ZD 103263--%>                    
                    <input type="hidden" name="hdnCrossBJNSelectOption" id="hdnCrossBJNSelectOption" runat="server" /><%--ZD 103550--%>                    
                    <input type="hidden" name="hdnCrossBJNIntegration" id="hdnCrossBJNIntegration" runat="server" />    
                    <input type="hidden" name="hdnCrossEnableBlueJeans" id="hdnCrossEnableBlueJeans" runat="server" />                
                    <input type="hidden" name="hdnCrossBJNDisplay" id="hdnCrossBJNDisplay" runat="server" /><%--ZD 104116--%>
                    <input type="hidden" name="hdnChangeBJNVal" id="hdnChangeBJNVal" runat="server" /><%--ZD 104116--%>
                    <input type ="hidden" id="hdnDialString" runat="server" name ="hdnDialString" /> <%--ZD 104289--%>
                    <input type ="hidden" id="hdnIsHostChanged" runat="server" name ="hdnIsHostChanged" /> <%--ZD 104289--%>                    
                    <%--ALLDEV-826 Starts--%>
                    <input type="hidden" id="hdnHostPassword" runat="server"/>
                    <input type="hidden" name="hdnCrossEnableHostGuestPwd" id="hdnCrossEnableHostGuestPwd" runat="server" />
                    <%--ALLDEV-826 Ends--%>
					<input type="hidden" name="hdnIsHDBusy" id="hdnIsHDBusy" runat="server" /> <%--ALLDEV-807--%>
                    <input type="hidden" id="hdnIsDeletedRoom" runat="server"/> <%--ALLBUGS-123--%>
                </td>
              </tr>
            </table>
            </td>
        </tr>
        <tr valign="top"><td style="vertical-align:top"> 
        <div style="border:1px black">
                   <asp:Menu
                        id="TopMenu"
                        Orientation="Horizontal"
                        StaticMenuItemStyle-CssClass="tab"
                        StaticSelectedStyle-CssClass="selectedTab"
                        CssClass="tabs"
                        ItemWrap="true" 
                        OnMenuItemClick="Menu1_MenuItemClick"
                        Runat="server">
                        <Items><%--FB 2579 Start ZD 100420 ZD 100875--%>
                        <asp:MenuItem Selected="true" Text="<div align='center' id='BasicTab' onclick='javascript:return SubmitRecurrence(1);' style='width:123'>Basic<br>Details</div>" Value="0" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Select<br>Participants</div>" Value="1" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Select<br>Rooms</div>" Value="2" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Audio/Video<br>Settings</div>" Value="3" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Select<br>Audiovisual</div>" Value="4" /><%-- FB 2570 --%>
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Select<br>Catering</div>" Value="5" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Select<br>Facility</div>" Value="6"></asp:MenuItem><%-- FB 2570 --%>
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'>Additional<br>Options</div>" Value="7"></asp:MenuItem> <%--Custom attribute Fixes --%>
                        <asp:MenuItem Text="<div align='center' id='RevSubmit' style='width:123' onclick='javascript:return SubmitRecurrence();'>Review &amp;<br>Submit</div>" Value="8" />
                        </Items>
                    </asp:Menu>
                    <div class="tabContents"style="width:900;vertical-align:top" >  <%--FB 2948--%>
                            <asp:MultiView id="Wizard1" Runat="server">
                                <asp:View ID="BasicDetails" runat="server" OnDeactivate="SetSetupTearDownTime" > <%--FB 2692--%>
                                  <asp:Panel runat="server" Width="100%" Id="pnlNormal">
                                    <input type="hidden" id="helpPage" value="12">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td colspan="2" style="height: 20px; text-align: center">
                                            <h3>
                                                <asp:Label ID="lblConfHeader" runat="server"></asp:Label>
                                                <%-- Code Added for FB 1428--%>
                                                <span id="Field1" runat="server">
                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Field1%>" runat="server"></asp:Literal></span>
                                                </h3> <%--Added for FF--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:45%;" valign="top"> <%--ZD 100393--%>
                                                <table width="100%" cellpadding="3" border="0" cellspacing="0">
                                                    <tr>
                                                    <%--FB 1985 - Starts--%>
                                                     <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY"))){%>                                                        
                                                     <td align="left" colspan="2">
                                                         <span class="reqfldstarText">
                                                         <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_requiredfield%>" runat="server"></asp:Literal></span>
                                                         <span style="width:10%">&nbsp;</span>
                                                    </td> 
                                                     <%} else {%>                                                           
                                                                <td style="height:10px;"></td>
                                                     <%}%><%--FB 1985 - End--%>
                                                    </tr>
                                                    <tr id="trConfTemp" runat="server" > <%--FB 2694--%>
                                                         <td class="blackblodtext" align="left" nowrap="nowrap">
                                                             <asp:Label ID="Label2" runat="server" Visible="False" width="20%"></asp:Label>
                                                             <%-- Code Added for FB 1428--%>                                                
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CreateFromTem%>" runat="server"></asp:Literal>
                                                            </td><%--Added for FF--%>
                                                         <td style="height: 20px" valign="top">
                                                            <asp:DropDownList ID="lstTemplates" CssClass="altText" runat="server" DataTextField="name" DataValueField="ID"  AutoPostBack="true" OnSelectedIndexChanged="UpdateTemplates" onChange="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" Width="80%"> <%--ZD 100604--%> <%--ZD 100875--%>
                                                            </asp:DropDownList>
                                                         </td>                     
                                                     </tr>
                                                     <tr id="trConfTitle" runat="server"> <%--FB 2694--%>
                                                         <td class="blackblodtext" align="left" width="35%">
                                                             <asp:Label ID="lblConfID" runat="server" Visible="False" width="20%"></asp:Label>
                                                             <%-- Code Added for FB 1428--%>                                                
                                                            <span id="Field2" runat="server">
                                                             <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Field2%>" runat="server"></asp:Literal></span>
                                                             <span class="reqfldstarText">*</span></td>
                                                         <td style="height: 20px" valign="top">
                                                             <asp:TextBox ID="ConferenceName" runat="server" CssClass="altText" Width="85%"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ConferenceName" ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                                                             <!--[Vivek: 29th Apr 2008]Changed Regular expression as per issue number 306  -->
                                                             <%-- Code Added for FB 1640--%>                                                
                                                             <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2321--%>
                                                             <asp:RegularExpressionValidator runat="server" ID="valInput" ControlToValidate="ConferenceName"  ValidationExpression="^[\s\S]{0,256}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit2%>"
                                                                Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>
                                                         </td>                     
                                                     </tr>
                                                    <tr id="trConfType" runat="server"> <%-- Code Modified For MOJ Phase2 //FB 2262 //FB 2599 --%>                                        
                                                        <%-- Code Added for FB 1428--%><%--FB 2023--%>
                                                        <td class="blackblodtext" align="left" id="Field3" width="35%" runat="server">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Field3%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                            <asp:DropDownList ID="lstConferenceType" CssClass="altSelectFormat" runat="server" OnSelectedIndexChanged="ShowHidePasswords" onChange="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" AutoPostBack="True"> <%--ZD 100604--%> <%--ZD 100875--%>
                                                                <asp:ListItem Value="6" Text="<%$ Resources:WebResources, AudioOnly%>"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, AudioVideo%>"></asp:ListItem>
                                                                <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ManageConference_pttopt%>"></asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="7" Text="<%$ Resources:WebResources, RoomConference%>"></asp:ListItem>
                                                                <asp:ListItem Value="8" Text="<%$ Resources:WebResources, Hotdesking%>"></asp:ListItem> <%--FB 2694--%>
                                                                <asp:ListItem Value="9" Text="<%$ Resources:WebResources, OBTP%>"></asp:ListItem><%--ZD 100513--%>
                                                            </asp:DropDownList>
                                                            &nbsp;&nbsp;
                                                            <div style="display:none"> <%--ZD 102319--%>
                                                            <asp:ImageButton ID="imgAudioNote" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"/> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%--FB 1985 - Starts--%>
                                                    <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                                                      {%> 
                                                    <tr >
                                                        <td colspan="2" style="color:Red">
                                                        <asp:Label runat="server" ID="lblAudioNote" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%}%><%--FB 1985 - End--%>
                                                    <tr id="trConfDesc" runat="server"><%-- Custom Attribute Fixes --%>
                                                        <td align="left" style="font-weight:bold" class="blackblodtext">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Description%>" runat="server"></asp:Literal></td>
                                                        <td align="left">
                                                              <asp:TextBox ID="ConferenceDescription" runat="server" MaxLength="2000" CssClass="altText" Rows="2" TextMode="MultiLine"
                                                                  Width="325px" Columns="20" Wrap="true"></asp:TextBox> <%--ZD 100393--%>
                                                             <%-- Code Added for FB 1640--%>                                                
                                                              <asp:RegularExpressionValidator ID="regConfDisc" ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%-- ZD 100263 --%>
                                                              <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters34%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\^+|!`\[\]{}\=%~]*$"></asp:RegularExpressionValidator><%--//FB 2236--%><%--FB 1888 103385 --%>
                                                                <%--Removed ? from error message--%>         
                                                              <asp:RegularExpressionValidator runat="server" ID="ValConfDesc" ControlToValidate="ConferenceDescription"  ValidationExpression="^[\s\S]{0,2000}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit3%>"
                                                                Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>                                                         
                                                        </td>
                                                   </tr>
                                                   <tr><%--FB 2359--%>
                                                     <td class="blackblodtext" align="left">
                                                         <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Requestor%>" runat="server"></asp:Literal></td>
                                                          <%-- FB 2501 Starts--%>
                                                        <td>
                                                            <asp:TextBox ID="txtApprover7" runat="server" CssClass="altText"></asp:TextBox>
                                                             &nbsp;<a href="" onclick="this.childNodes[0].click();return false;"><img id="Img5" onclick="javascript:getYourOwnEmailList(6,2)" src="image/edit.gif" alt="Edit" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>"/></a><%--ZD 100420--%><%--FB 2798--%><asp:TextBox 
                                                                ID="hdnApprover7" runat="server" BackColor="Transparent" BorderColor="White" 
                                                                BorderStyle="None" ForeColor="Black" style="display:none" Width="0px"></asp:TextBox>
                                                            <asp:TextBox ID="hdnRequestorMail" runat="server" Width="0px" 
                                                                style="display:none"></asp:TextBox>
                                                        </td>
                                                        <%-- FB 2501 ends--%>
                                                        
                                                    </tr>
                                                   <tr>
                                                        <%-- Code Added for FB 1428--%>
                                                        <td class="blackblodtext" align="left" id="Field4" runat="server" nowrap="nowrap"> <%--ZD 101344--%>
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Field4%>" runat="server"></asp:Literal></td>
                                                        <td>
                                                            <asp:TextBox ID="txtApprover4" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                                            &nbsp;<a href="" onclick="this.childNodes[0].click();return false;"><img id="Img8"  onclick="javascript:getYourOwnEmailList(3,1)" src="image/edit.gif" alt="Edit" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a><%--ZD 100420--%><%--FB 2798--%><asp:TextBox 
                                                                ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White" 
                                                                BorderStyle="None" ForeColor="Black" style="display:none" Width="0px"></asp:TextBox><%--ZD 100419--%>
                                                          <%--Edited for FF--%>
                                                            <asp:TextBox ID="hdnApproverMail" runat="server" style="display:none" 
                                                                Width="0px"></asp:TextBox><%--Code added for FB : 1116--%>   
                                                        </td>
                                                    </tr>
                                                    
                                                  <tr id="trConfPass">
                                                        <asp:Panel ID="pnlPassword1" runat="server" >
                                                        <td class="blackblodtext" align="left" valign="top" style="padding-top: 7px"><%--ALLDEV-826--%>
                                                            <%--<asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_NumericPasswor%>" runat="server"></asp:Literal>--%> <%--ALLDEV-826--%>
                                                            <asp:Literal ID="litNumericOrGuestPwd" runat="server"></asp:Literal> <%--ALLDEV-826--%>
                                                          </td>
                                                        <td valign="top" nowrap="nowrap"><%--FB 2501--%>
                                                      
                                                            <asp:TextBox ID="ConferencePassword" maxlength="15" runat="server" TextMode="SingleLine" CssClass="altText" onblur="ConfPWDLen()" ></asp:TextBox><%--FB 2244--%> <%--ZD 100522--%><%--ALLDEV-826--%>
                                                            <%--Code changes for FB : 1232--%> <%--ZD 102692 Start--%>
                                                            <button runat="server" ID="btnGeneratePassword" type="button" style="width:180px" class="altMedium0BlueButtonFormat" onkeydown="if(event.keyCode == 13){ javascript:num_gen(); return false;}" onmousedown="javascript:num_gen(); return false;">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGeneratePassword%>" runat="server"></asp:Literal></button><%-- FB 676--%><%--ZD 100420--%><%--ZD 100875--%>
                                                            <%--<br />
                                                            <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="ConferencePassword2"
                                                                ControlToValidate="ConferencePassword" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_password%>"></asp:CompareValidator>--%>
                                                            <asp:RegularExpressionValidator ID="numPassword1" SetFocusOnError="true" runat="server"  ErrorMessage="<%$ Resources:WebResources, ConfSetup_PasswordLength%>" ControlToValidate="ConferencePassword" Display="Dynamic"></asp:RegularExpressionValidator> <%--Comments: Fogbugz case 107, 522 --%>
                                                        </td> <%--ZD 102692 End--%>
                                                        </asp:Panel>
                                                   </tr>
                                                    <tr id="trConfPass1">
                                                        <asp:Panel ID="pnlPassword2" runat="server">
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConfirmPasswor%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 20px" valign="top">
                                                            <asp:TextBox ID="ConferencePassword2" maxlength="15" runat="server" CssClass="altText" TextMode="SingleLine" onblur="ConfpwdValidation()" ></asp:TextBox><%--FB 2244--%><%--ALLDEV-826--%>
                                                            <asp:CompareValidator ID="cmpValPassword" runat="server" ControlToCompare="ConferencePassword"
                                                                ControlToValidate="ConferencePassword2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch1%>"></asp:CompareValidator>
                                                        </td>
                                                        </asp:Panel>
                                                    </tr>
                                                    <%--ALLDEV-826 Starts--%>
                                                    <tr id="trHostPwd">
                                                        <asp:Panel ID="pnlHostPwd1" runat="server">
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_HostPassword%>" runat="server"></asp:Literal>
                                                        </td>
                                                        <td valign="top" nowrap="nowrap">                                  
                                                            <asp:TextBox ID="txtHostPwd" maxlength="15" TextMode="SingleLine" CssClass="altText" onblur="HostPWDLen()" runat="server"></asp:TextBox>
                                                            <button ID="btnGenerateHostPwd" type="button" style="width:180px" class="altMedium0BlueButtonFormat" onkeydown="if(event.keyCode == 13){ javascript:host_gen(); return false;}" onmousedown="javascript:host_gen(); return false;" onmouseup="javascript:chkRequiredHostPwd(); return false;" runat="server">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGeneratePassword%>" runat="server"></asp:Literal>
                                                            </button>
                                                            <asp:RegularExpressionValidator ID="regValHostPwd" ControlToValidate="txtHostPwd" ErrorMessage="<%$ Resources:WebResources, ConfSetup_PasswordLength%>" 
                                                                ToolTip="<%$ Resources:WebResources, NumericValuesOnly1%>" Display="Dynamic" SetFocusOnError="True" runat="server">
                                                            </asp:RegularExpressionValidator>                                                            
                                                            <br /><asp:RequiredFieldValidator ID="reqValHostPwd" ControlToValidate="txtHostPwd" ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Bold="True" Enabled="false" Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
                                                        </td>
                                                        </asp:Panel>
                                                    </tr>
                                                    <tr id="trCfmHostPwd">
                                                        <asp:Panel ID="pnlHostPwd2" runat="server">
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConfirmPasswor%>" runat="server"></asp:Literal>
                                                        </td>
                                                        <td style="height:20px" valign="top">
                                                            <asp:TextBox ID="txtCfmHostPwd" maxlength="15" CssClass="altText" TextMode="SingleLine" onblur="HostPwdValidation()" runat="server"></asp:TextBox>
                                                            <asp:CompareValidator ID="cmpValHostPwd" ControlToValidate="txtCfmHostPwd" ControlToCompare="txtHostPwd"
                                                                ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch1%>" Display="Dynamic" runat="server">
                                                            </asp:CompareValidator>
                                                        </td>
                                                        </asp:Panel>
                                                    </tr>
                                                    <%--ALLDEV-826 Ends--%>
                                                    <tr id="trPublic" runat="server"><%--FB 2359--%>
                                                        <td class="blackblodtext" align="left" width="182px">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Public%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="chkPublic" runat="server" />
                                                            </td><td>
                                                            <div ID="openRegister" style="display:none">
                                                            <asp:CheckBox ID="chkOpenForRegistration" runat="server" TextAlign="Left" /><span 
                                                                    align="left" class="blackblodtext">
                                                                <asp:Literal runat="server" 
                                                                    Text="<%$ Resources:WebResources, ConferenceSetup_OpenforRegist%>"></asp:Literal></span>
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2595 - Start--%>  
                                                    <tr id="trSecure" runat="server"> <%--FB 2993 Starts--%>
                                                        <td class="blackblodtext" align="left" style="vertical-align:top">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_NetworkClassif%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                        <asp:DropDownList ID="drpNtwkClsfxtn" runat="server" CssClass="alt2SelectFormat" style="margin-left:3px; width:172px">
                                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ConferenceSetup_NATOSecret%>"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="<%$ Resources:WebResources, ConferenceSetup_NATOUnclassified%>"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr> <%--FB 2993 Ends--%>
                                                    <%--FB 2595 - End--%>
                                                    <tr id="trServType" runat="server" style="display:none;" ><%--FB 2219 ZD 102159--%>
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ServiceType%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                            <asp:DropDownList ID="DrpServiceType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" style="margin-left:3px; "> <%--FB 2993--%>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="trVIP" runat="server"> <%-- Code Modified For MOJ Phase2 --%>
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VIP%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="chkisVIP" runat="server" />
                                                            </td><td>
                                                            <div ID="Div1" style="display:none">
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trVMR" runat="server"> <%-- Code Modified For FB  2376 --%>
                                                        <td class="blackblodtext" align="left" style="vertical-align:top">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VMR%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                           <table border="0"><tr>
                                                               <td>
                                                           <%--FB 2620 Starts--%>
                                                <asp:DropDownList ID="lstVMR" runat="server" CssClass="alt2SelectFormat" 
                                                                       onchange="javascript:changeVMR();fnShowHideAVforVMR();" style="width:172px"> <%--FB 2993--%>
                                                    <%--FB 2448--%>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, None%>" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, ConferenceSetup_Personal%>" 
                                                        Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, ConferenceSetup_Room%>" 
                                                        Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, ConferenceSetup_External%>" 
                                                        Value="3"></asp:ListItem>
                                                    <%--FB 2481--%>
                                                </asp:DropDownList>
                                                           </td>
                                                           <td>
                                                           </td>
                                                           </tr></table>
                                                <%--FB 2620 Ends--%>
                                                            <%--VMR Start--%>
                                                        
								<table id="divbridge" style="display:none">
                                                                        <tr>
                                                                            <td class="blackblodtext" align="left">
                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_InternalBridge%>" runat="server"></asp:Literal></td>
                                                                            <td style="height: 24px">
                                                                               <asp:TextBox ID="txtintbridge" ReadOnly="true" runat="server" ></asp:TextBox>
										<input type="hidden" name="intbridge" id="hdnintbridge" runat="server" />
										</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="blackblodtext" align="left">
                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ExternalBridge%>" runat="server"></asp:Literal></td>
                                                                            <td style="height: 24px">
                                                                               <asp:TextBox ID="txtextbridge" ReadOnly="true" runat="server" ></asp:TextBox>
										<input type="hidden" name="extbridge" id="hdnextbridge" runat="server" />
										</td>
                                                                        </tr>
                                                                    </table>
                                                        <%--VMR End--%>
                                                        <%--ZD 100522 Starts--%>
                                                        <table id="divChangeVMRPIN" style="display:" runat="server">
                                                        <tr>
                                                            <td class="blackblodtext" align="left" nowrap="nowrap">
                                                            <button runat="server" ID="btnChngPIN" type="button" style="width:180px" class="altMedium0BlueButtonFormat" onclick="javascript:ChangeRoomVMRPIN();">
                                                            <asp:Literal  Text="<%$ Resources:WebResources, Confsetup_ChangePassword%>" runat="server"></asp:Literal>
                                                            </button> 
                                                            <asp:ImageButton ID="imgVMRPIN" src="image/info.png" style="cursor:default"  ToolTip="<%$ Resources:WebResources, VMRPasswordtip%>" AlternateText="Info" runat="server" OnClientClick="javascript:return false;"/><%-- ZD 102590 --%>
                                                            </td>
                                                            <td style="height: 24px">
										                    </td>
                                                        </tr>
                                                       <tr id="trVMRPIN1" style="display:none">
                                                        <td align="left" class="blackblodtext">
                                                            <asp:Literal ID="Literal80"  Text="<%$ Resources:WebResources, ConfSetup_NewPass%>" runat="server"></asp:Literal>
                                                          </td>
                                                        <td valign="top" nowrap="nowrap">
                                                            <asp:TextBox ID="txtVMRPIN1" runat="server" TextMode="SingleLine" CssClass="altText" onblur="VMRPWDLen()"></asp:TextBox>
                                                            <br />
                                                            <asp:CompareValidator ID="CmpVMRPIN1" runat="server" ControlToCompare="txtVMRPIN2"
                                                                ControlToValidate="txtVMRPIN1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_password%>"></asp:CompareValidator>
                                                            <asp:RegularExpressionValidator ID="RegVMRPIN" runat="server" SetFocusOnError="True"  ErrorMessage="<%$ Resources:WebResources, ConfSetup_PasswordLength%> " ToolTip="<%$ Resources:WebResources, NumericValuesOnly1%>" ControlToValidate="txtVMRPIN1" Display="Dynamic"></asp:RegularExpressionValidator>
                                                        </td>
                                                   </tr>
                                                    <tr id="trVMRPIN2" style="display:none">
                                                        <td align="left" class="blackblodtext">
                                                            <asp:Literal ID="Literal81"  Text="<%$ Resources:WebResources, Confsetup_ConfirmNewPass%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 20px" valign="top">
                                                            <asp:TextBox ID="txtVMRPIN2" runat="server" CssClass="altText" TextMode="SingleLine" ></asp:TextBox>
                                                            <asp:CompareValidator ID="CmpVMRPIN2" runat="server" ControlToCompare="txtVMRPIN1"
                                                                ControlToValidate="txtVMRPIN2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch1%>"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                                    </table>
                                                        <%--ZD 100522 Ends--%>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 100890 Starts--%>
                  	                                <tr id="trStatic" runat="server">
                                                        <td class="blackblodtext" align="left" width="182px">
                                                            <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Static%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="chkStatic" runat="server" onclick="javascript:fnchkStatic();" />
                                                            </td><td>
                                                            <div ID="divStaticID" runat="server" style="display:none">
                                                            <asp:TextBox ID="txtStatic" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 100890 End--%>
                                                    <%--FB 2717 Vidyo Integration Start--%>
                                                    <tr id="CloudConfRow" runat="server"> <%--FB 2717--%>
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Vidyo%>" runat="server"></asp:Literal></td><%--FB 2834--%>
                                                        <td style="height: 24px">
                                                           <asp:CheckBox ID="chkCloudConferencing" runat="server" onClick="fnChkCloudConference()" /> <%--AutoPostBack="true" OnCheckedChanged="CheckCloudConferecing" />--%>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2717 Vidyo Integration End--%>
                                                    <%--FB 2693 Start--%>
                                                    <tr id="trPCConf" runat="server"> 
                                                        <td class="blackblodtext" align="left" valign="top">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PCConferencing%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px; vertical-align:top"><%--FB 2694 Start--%>
                                                            <input type="checkbox" runat="server"  id="chkPCConf" onClick="javascript:fnPCconf();fnShowHideAVforVMR();" /> <%--FB 2819--%>
                                                            <table width="100%" border="0" id="tblPcConf" runat="server" style="display:none; margin-left:30px; margin-top:-20px" >
                                                                <tr id="trBJ"  >
                                                                <%--ZD 104021--%>
                                                                    <%--<td width="50%" align="left" id="tdBJ" runat="server" >
                                                                        <input type="radio" style="vertical-align:top" id="rdBJ" name="PCSelection" value="1" runat="server" />
                                                                        <img alt="Blue Jeans" width="20px" src="../image/BlueJeans.jpg" title="Blue Jeans" />
                                                                        <a href="#" runat="server" class="PCSelected" id="btnBJ" style="cursor:pointer; text-decoration:none; vertical-align:top" >
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnBJ%>" runat="server"></asp:Literal></a>
                                                                    </td>--%>
                                                                    <td align="left" id="tdJB" runat="server">
                                                                        <input type="radio" style="vertical-align:top" id="rdJB" name="PCSelection" value="2" runat="server" /> 
                                                                        <img alt="Jabber" width="20px" src="../image/Jabber.jpg"  title="Jabber" /><%--ZD 100419--%>
                                                                        <a href="#" runat="server" class="PCSelected" id="btnJB" style="cursor:pointer; text-decoration:none; vertical-align:top" >
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnBJ%>" runat="server"></asp:Literal></a><%--ZD 100420--%>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trLync"  >
                                                                    <td align="left" id="tdLy" runat="server">
                                                                        <input type="radio" style="vertical-align:top" id="rdLync" name="PCSelection" value="3" runat="server" /> 
                                                                        <img alt="Lync" width="20px" src="../image/Lync.jpg"  title="Lync" /></a><%--ZD 100420--%> <%--ZD 100419--%>
                                                                        <a href="#" runat="server" class="PCSelected" id="btnLync" style="cursor:pointer; text-decoration:none; vertical-align:top" >
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnBJ%>" runat="server"></asp:Literal></a><%--ZD 100420--%>
                                                                    </td>
                                                                    <td align="left" id="tdVid" runat="server" visible="false"> <%--ZD 102004--%>
                                                                        <input type="radio" style="vertical-align:top" id="rdVidtel" name="PCSelection" value="4" runat="server" />                                                                         
                                                                        <img alt="Vidtel" width="20px" src="../image/Vidtel.jpg"  title="Vidtel" /><%--ZD 100419--%>
                                                                        <a href="#" runat="server" class="PCSelected" id="btnVidtel" style="cursor:pointer; text-decoration:none; vertical-align:top" >
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnBJ%>" runat="server"></asp:Literal></a><%--ZD 100420--%>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trVidyo"  style="display:none" >
                                                                    <td align="left">
                                                                        <input type="radio" id="rdVidyo" name="PCSelection" value="5" runat="server" />                                                                        
                                                                        <a href="" onclick="this.childNodes[0].click();return false;">
                                                                        <img alt="Vidyo" src="../image/Vidyo.jpg" /><%--ZD 100419--%>
                                                                    </td>
                                                                    <td>
                                                                        <a href="" class="PCSelected" id="btnVidyo" >
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnBJ%>" runat="server"></asp:Literal></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 100221 Starts--%>
                                                     <tr id="trWebex" runat="server">
                                                        <td class="blackblodtext" style="vertical-align:top" align="left"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ManageConference_Webex%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px" nowrap="nowrap">
                                                           <asp:CheckBox runat="server" ID="chkWebex"  onclick="javascript:fnShowWebExConf()" />
                                                         <input type="hidden" name="hdnHostWebEx" id="hdnHostWebEx" runat="server" /> <%-- ZD 101015--%>
                                                           <input type="hidden" name="hdnSchdWebEx" id="hdnSchdWebEx" runat="server" /> <%-- ZD 101015--%>
                                                            </td>
                                                     </tr> 
                                                     <tr id="divWebPW" runat="server" style="display:none;vertical-align:top;" >
                                                     <td class="blackblodtext">
                                                        <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ConferenceSetup_Password%>" runat="server"></asp:Literal><span class="reqfldstarText">*&nbsp;</span>
                                                     </td>
                                                     <td>   
                                                            <asp:TextBox ID="txtPW1" TextMode="Password" CssClass="altText" runat="server" onblur="PasswordChange(1)" onfocus="fnTextFocus(this.id,1)"></asp:TextBox><%-- ZD 100221 --%>
                                                            <asp:ImageButton id="ImgWebExPW1" src="image/info.png" runat="server" alt="Info" style="cursor:default" 
                                                            tooltip="<%$ Resources:WebResources, WebexToolTip1%>" OnClientClick="javascript:return false;" /><%--ZD 100935--%> <%--ZD 100221_17Feb--%><%-- ZD 102590 --%>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPW1" Enabled="false"
                                                                    Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Bold="True"></asp:RequiredFieldValidator>
                                                             <asp:Label runat="server" ID="lblwebexerror" ForeColor="Red" style="display:none;margin-top:-16px;margin-left:15px;" Text="<br>Need one uppercase,one lowercase,one numeric value,password length 6 and & < > are invalid characters."></asp:Label>
                                                             <asp:RegularExpressionValidator style="margin-left:15px"   ID="regPassword1" ControlToValidate="txtPW1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required_WebexPwd%>" ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{6,}"></asp:RegularExpressionValidator> 
                                                     </td>   
                                                     </tr>
                                                     <tr id="divWebCPW" runat="server" style="display:none;vertical-align:top;">
                                                     <td class="blackblodtext">
                                                        <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ConferenceSetup_ConfirmPasswor%>" runat="server"></asp:Literal>
                                                         <span class="reqfldstarText">*&nbsp;</span>
                                                     </td>
                                                     <td>
                                                       <asp:TextBox CssClass="altText" ID="txtPW2" TextMode="Password"  runat="server" onblur="PasswordChange(2)" onfocus="fnTextFocus(this.id,2)"></asp:TextBox><%-- ZD 100221 --%>
                                                       <asp:ImageButton id="ImgWebExPW2" src="image/info.png" runat="server" alt="Info" style="cursor:default"
                                                        tooltip="<%$ Resources:WebResources, WebexToolTip1%>" OnClientClick="javascript:return false;" /><%--ZD 100935--%> <%--ZD 100221_17Feb--%><%-- ZD 102590 --%>
                                                        <asp:Label runat="server" ID="Label3" ForeColor="Red" style="display:none" ></asp:Label>   
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPW2"
                                                        ControlToValidate="txtPW1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>" style="margin-left:10px;margin-top:-16px;"></asp:CompareValidator>
                                                        <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPW2" style="margin-left:10px;margin-top:-16px;" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required_WebexPwd%>" ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{6,}"></asp:RegularExpressionValidator> 
                                                        <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPW1" style="margin-left:10px;margin-top:-16px;"
                                                        ControlToValidate="txtPW2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Passworddonotmatch1%>"></asp:CompareValidator>
                                                        </td>
                                                     </tr>
                                                    <%--ZD 100221 Ends--%>
                                                    <%--FB 2693 End--%>
                                                    <tr id="StartNowRow"> <%--FB 2634--%>
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_StartNow%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                            <asp:CheckBox runat="server" ID="chkStartNow" />
                                                        </td>
                                                    </tr>
                                                    <%--ZD 103550 Start--%>
                                                     <tr id="trBJNMeeting" runat="server"><%--FB 2359--%>
                                                        <td class="blackblodtext" align="left" width="182px">
                                                            <asp:Literal Text="<%$ Resources:WebResources, mainadministrator_BJNMeetingID%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="chkBJNMeetingID" runat="server" onclick="javascript:fnBJNMeetingOptSelection();" />
                                                            </td><td>
                                                            <div ID="DivBJNMeetingID" runat="server" style="display:none;">
                                                            <asp:DropDownList ID="DrpBJNMeetingID" runat="server" CssClass="alt2SelectFormat" style="width:172px">                                                                
                                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, mainadministrator_PesonalMeetingID%>"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, mainadministrator_OneTimeMeetingID%>"></asp:ListItem>
                                                            </asp:DropDownList>                                                            
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 103550 End--%>
                                                    <%--FB 2870 Start--%>
                                                     <tr id="TrCTNumericID" runat="server"><%--FB 2359--%>
                                                        <td class="blackblodtext" align="left" width="182px">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CTSNumericID%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="ChkEnableNumericID" runat="server" 
                                                                    onclick="javascript:ChangeNumeric();" />
                                                            </td><td>
                                                            <div ID="DivNumeridID" runat="server" style="display:none">
                                                            <asp:TextBox ID="txtNumeridID" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="ReqNumeridID" runat="server" 
                                                                    ControlToValidate="txtNumeridID" Display="dynamic" Enabled="false" 
                                                                    ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegNumeridID" runat="server" 
                                                                    ControlToValidate="txtNumeridID" Display="dynamic" 
                                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" 
                                                                    SetFocusOnError="true" ValidationExpression="^[^&lt;&gt;&amp;]*$"></asp:RegularExpressionValidator>
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2870 End--%>
                                                    <tr id="NONRecurringConferenceDiv9"  runat="server" style="display:none;" ><%--FB 2634--%>
                                                        <td class="blackblodtext">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_EnableBufferZ%>" runat="server"></asp:Literal></td>
                                                        <td>
                                                             <asp:CheckBox ID="chkEnableBuffer" runat="server" onclick="javascript:fnEnableBuffer();" />                                                            
                                                        </td>                                                        
                                                    </tr>
                                                    <%--FB 2998--%>
                                                    <tr id="MCUConnectRow" runat="server">
                                                        <td class="blackblodtext" align="left" id="Td2" nowrap="nowrap" >
                                                            <asp:Label ID="lblMCUConnect" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_lblMCUConnect%>"></asp:Label>
                                                        </td>
                                                        <td valign="middle" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                            width: 403px;" align="center" colspan="2">
                                                            <asp:CheckBox  runat="server" ID="chkMCUConnect" />
                                                        </td>
                                                    </tr>
                                                    <tr id="MCUConnectDisplayRow"  runat="server">
                                                        <td  align="left" id="Td3" nowrap="nowrap">
                                                        </td>
                                                        <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                            width: 403px;" align="center" colspan="2">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                             <tr>
                                                                 <td id="ConnectCell" runat="server" width="45%" valign="top" align="left">
                                                                    <span style="white-space: nowrap;">
                                                                    <span class="blackblodtext">
                                                                     <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConnectMinutes%>" runat="server"></asp:Literal></span>&nbsp;<asp:TextBox ID="txtMCUConnect"  runat="server" CssClass="altText" Width="40px"
                                                                    onblur="javascript:if(this.value.trim() =='') this.value='0';"></asp:TextBox> <%--ZD 100528--%>
                                                                    <asp:ImageButton ID="imgMCUConnect" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default" OnClientClick="javascript:return false;"/>  <%--ZD 100419--%><%-- ZD 102590 --%>
                                                                    </span>
                                                                    <br />
                                                                    <%--FB 2998 Validation Part Starts--%>
                                                                    <%--<asp:CompareValidator  id="cmpNumbers" Enabled="true"  ErrorMessage="MCU Connect time should be less than or equal to Setup Time." display="dynamic"
                                                                        ControlToCompare="SetupDuration" ControlToValidate="txtMCUConnect" Operator="LessThanEqual" runat="server" Type="Integer"
                                                                        ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                                                    <%--FB 2998 Validation Part Ends--%><%-- ZD 100405 --%>
                                                                  <%-- <asp:RangeValidator id="RangeValidator1" setfocusonerror="true" type="Integer" display="Dynamic" controltovalidate="txtMCUConnect" runat="server"
                                                                         errormessage="MCU prestart should not be in the past." ></asp:RangeValidator>--%> <%-- ZD 100433--%>
                                                                    <asp:RegularExpressionValidator id="RegularExpressionValidator17" validationgroup="Submit"
                                                                        controltovalidate="txtMCUConnect" display="dynamic" runat="server" setfocusonerror="true"
                                                                        errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                                        <%--ZD 102392--%>
                                                                        <br />
                                                                        <asp:CustomValidator id="customMCUPreStart" Enabled="true"  Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, MCUConnectTime%>"
                                                                         ControlToValidate="txtMCUConnect"  runat="server"  ClientValidationFunction="fnCompareSetup"></asp:CustomValidator><%-- ZD 100433--%>
                                                                 </td>
                                                                 <td id="DisconnectCell" runat="server" width="55%" valign="top">
                                                                    <span style="white-space: nowrap;">
                                                                   <span class="blackblodtext"> 
                                                                     <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DisconnectMinu%>" runat="server"></asp:Literal></span>&nbsp;<asp:TextBox ID="txtMCUDisConnect" runat="server" CssClass="altText" Width="40px"
                                                                   onblur="javascript:if(this.value.trim() =='') this.value='0';"></asp:TextBox> <%--ZD 100528--%>
                                                                   <asp:ImageButton ID="imgMCUDisconnect" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default" OnClientClick="javascript:return false;"/> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                                   </span>
                                                                    <br /><%-- ZD 100405 --%>
                                                                   <%-- <asp:rangevalidator id="RangeValidator2" setfocusonerror="true" type="Integer" minimumvalue="-15"
                                                                        maximumvalue="15" display="Dynamic" controltovalidate="txtMCUDisConnect" runat="server"
                                                                        errormessage="MCU disconnect time is not allowed more than 15 mins."></asp:rangevalidator>--%>
                                                                    <asp:regularexpressionvalidator id="RegularExpressionValidator20" validationgroup="Submit"
                                                                        controltovalidate="txtMCUDisConnect" display="dynamic" runat="server" setfocusonerror="true"
                                                                        errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="^-{0,1}\d+$"></asp:regularexpressionvalidator>
                                                                    <%--FB 2998 Validation Part Starts--%>
                                                                    <%--<asp:CompareValidator  id="cmpTeardown" Enabled="true"  ErrorMessage="MCU Disconnect Time should be less than or equal to Tear-Down Time." display="dynamic"
                                                                        ControlToCompare="TearDownDuration" ControlToValidate="txtMCUDisConnect" Operator="LessThanEqual" runat="server" Type="Integer"
                                                                         ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                                                         <%--ZD 102392--%>
                                                                         <br />
                                                                         <asp:CustomValidator id="customMCUPreEnd" Enabled="true"  Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ValidationMCUEndTime%>"
                                                                         ControlToValidate="txtMCUDisConnect"  runat="server"  ClientValidationFunction="fnCompareTear"></asp:CustomValidator><%-- ZD 100433--%>
                                                                    <%--FB 2998 Validation Part Ends--%>
                                        
                                                                 </td>
                                                             </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                      <%--FB 2634--%>
                                                     <tr id="SetupRow" runat="server"> <%--ZD 10755 --%>
                                                        <td class="blackblodtext" align="left" id="SDateText" nowrap="nowrap">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SDateText%>" runat="server"></asp:Literal></td>
                                                        <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                            width: 403px;" align="center" colspan="2">
                                                             <asp:TextBox ID="SetupDuration" runat="server" CssClass="altText" Width="22%" AutoPostBack="false" onblur="javascript:fnClear('SetupDuration')"></asp:TextBox>
                                                             <asp:ImageButton ID="imgSetup" src="image/info.png" runat="server" AlternateText="Info" style="cursor:default"  OnClientClick="javascript:return false;"/><%--FB 2998--%> <%--ZD 100419--%><%-- ZD 102590 --%>
                                                            <asp:RegularExpressionValidator ID="regSetupDuration" runat="server" ControlToValidate="SetupDuration"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDuration%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                               <asp:CustomValidator id="customSetup" Enabled="true"  ErrorMessage="<%$ Resources:WebResources, SetuptimeMsg%>" Display="Dynamic"
                                                                         ControlToValidate="SetupDuration"  runat="server"  ClientValidationFunction="fnCompareSetup"></asp:CustomValidator><%-- ZD 100433--%>
                                                        </td>
                                                    </tr>
                                                    <tr id="ConfStartRow">
                                                        <td class="blackblodtext" align="left" nowrap="nowrap">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceStar%>" runat="server"></asp:Literal>
                                                            <span class="reqfldstarText">*&nbsp;</span>
                                                        </td>
                                                        <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                            width: 403px;" align="center" colspan="2">
                                                            <span id="StartDateArea">
                                                                <asp:TextBox ID="confStartDate" runat="server" CssClass="altText" Width="22%" onblur="javascript:ChangeEndDate()"
                                                                 AutoPostBack="false"></asp:TextBox>
																<%--ZD 100420--%>
                                                                <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_triggerd').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}">
                                                            <img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align: middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" 
                                                                onclick="return showCalendar('<%=confStartDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /></a><%--ZD 100420--%> <%--ZD 100419--%>
                                                                <span class="blackblodtext">@</span> </span>
                                                            <mbcbb:ComboBox ID="confStartTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                                                                onblur="javascript:formatTime('confStartTime_Text');return ChangeEndDate();" Style="width: auto" AutoPostBack="false">
                                                            </mbcbb:ComboBox>
                                                            <asp:RequiredFieldValidator ID="reqConfStartTime" runat="server" ControlToValidate="confStartTime"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                                           <asp:RegularExpressionValidator ID="regConfStartTime" runat="server" ControlToValidate="confStartTime"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                            <asp:RequiredFieldValidator ID="reqConfStartDate" runat="server" ControlToValidate="confStartDate"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="regConfStartDate" runat="server" ControlToValidate="confStartDate"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                            <asp:TextBox runat="server" ID="SetupDateTime" Visible="false"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="ConfEndRow">
                                                        <td class="blackblodtext" align="left" id="EDateText">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_EDateText%>" runat="server"></asp:Literal>
                                                            <span class="reqfldstarText">*</span>
                                                        </td>
                                                        <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                                            width: 403px;" align="center" colspan="2">
                                                            <span id="EndDateArea">
                                                            <asp:TextBox ID="confEndDate" runat="server" CssClass="altText" Width="22%" onblur="javascript:fnEndDateValidation()"  AutoPostBack="false"></asp:TextBox>
															<%--ZD 100420--%>
                                                            <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_trigger1').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}">
                                                            <img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_trigger1"
                                                                style="cursor: pointer; vertical-align: middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onClick="return showCalendar('<%=confEndDate.ClientID %>', 'cal_trigger1', 1, '<%=format%>');" /></a><%--ZD 100420--%><%--ZD 100419--%>
                                                            <span class="blackblodtext">@ </span>
                                                            </span>
                                                            <mbcbb:ComboBox ID="confEndTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                                                onblur="javascript:formatTime('confEndTime_Text');" Style="width: auto" CausesValidation="True" AutoPostBack="false">
                                                            </mbcbb:ComboBox>
                                                            <asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="confEndTime"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="regEndTime" runat="server" ControlToValidate="confEndTime"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                            <asp:RequiredFieldValidator ID="reqEndDate" runat="server" ControlToValidate="confEndDate"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="regEndDate" runat="server" ControlToValidate="confEndDate"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr id="TearDownRow" runat="server"><%--ZD 10755 --%>
                                                        <td class="blackblodtext" align="left" id="Td1" nowrap="nowrap">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Td1%>" runat="server"></asp:Literal></td>
                                                        <td>
                                                            <span id="TearDownArea">
                                                                <asp:TextBox ID="TearDownDuration" runat="server" CssClass="altText" Width="22%" AutoPostBack="false" onblur="javascript:fnClear('TearDownDuration')"></asp:TextBox>
                                                                <asp:ImageButton ID="imgTear" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"/><%--FB 2998--%><%--ZD 100419--%><%-- ZD 102590 --%>
                                                                <asp:RegularExpressionValidator ID="regTearDown" runat="server" ControlToValidate="TearDownDuration"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDuration%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                <asp:CustomValidator id="customTearDown" Enabled="true"  ErrorMessage=" <%$ Resources:WebResources, TeardowntimetoMCUDisconnectTime%> " Display="Dynamic"
                                                                    ControlToValidate="TearDownDuration"  runat="server"  ClientValidationFunction="fnCompareTear"></asp:CustomValidator><%-- ZD 100433--%>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2659 Starts--%>
                                                    
                                                    <tr id="trSeatsAvailable" runat="server">
                                                        <td class="blackblodtext" align="left">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SeatsAvailable%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 24px;">   
                                                            <button id="btnchkSeatAvailable" class="altMedium0BlueButtonFormat" type="button" runat="server" style="width:150px;"
                                                            onclick="javascript:document.getElementById('hdnSaveData').value = '1';" onserverclick="CheckSeatsAvailability">
                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SeatsAvailable%>" runat="server"></asp:Literal></button><%--ZD 100875 ZD 101714--%>
                                                        </td>
                                                    </tr>
                                                    <tr >
                                                    <td></td>
                                                    <td align="center">
                                                    <%--ZD 100568 Inncrewin Commented this Code
                                                    <div id="modalDivPopup" style="display:none;  position:fixed; z-index:1000; left:0px; top:0px; width:100%; height:1000px; background-color:Gray; opacity:0.5;"></div>
                                                    <div id="modalDivContent" style="display:none; left:5%; position:fixed; top:30%; padding-bottom:0.5%; padding-top:0.5%; z-index:10000; background-color:White; width:90%;">
                                                    <table border="0" width="98%" cellpadding="5">
                                                    <tr>
                                                    <td align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceTime%>" runat="server"></asp:Literal></td>
                                                    <td bgcolor="#65FF65"></td>
                                                    <td align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_TotallyFree%>" runat="server"></asp:Literal></td>
                                                    <td bgcolor="#F8F075"></td>
                                                    <td align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PartiallyFree%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="5">
                                                    <table id="tblSeatsAvailability" cellpadding="4" cellspacing="0" runat="server" height="50%" width="100%" border="1" ></table>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    <input type="button" align="middle" id="btnClose" runat="server" onclick="javascript:fnPopupSeatsClose(); return false;" value="Close" class="altShortBlueButtonFormat" /> 
                                                    <br />
                                                    </div>
                                                    ZD 100568 Inncrewin Commented this Code--%>
                                                    </td>
                                                    </tr>
                                                    <%--FB 2659 End--%>
                                                        <tr id="RecurRow">
                                                           <td class="blackblodtext" align="left">
                                                               <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Recurrence%>" runat="server"></asp:Literal></td>
                                                           <td> <%--FB 1911--%>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr >
                                                                        <td width="5%" align="left">
                                                                           <asp:CheckBox onClick="openRecur()" runat="server" ID="chkRecurrence" />
                                                                        </td>
                                                                        <td class="blackblodtext" align="left" nowrap="" id="SPCell1"><span class="subtitleblueblodtext">
                                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_OR%>" runat="server"></asp:Literal>&nbsp;</span><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SPCell1%>" runat="server"></asp:Literal></td>
                                                                        <td id="SPCell2" width="45%">
                                                                            <a href="#" onClick="openRecur('S')" style="cursor: hand;">
                                                                              <img src="image/recurring.gif" alt="Special Recurring Pattern" width="25" height="25" border="0" style="cursor:pointer;" title="<%$ Resources:WebResources, recurNET_SpecialRecurri%>" runat="server"> <%--FB 2798--%> <%--ZD 100419--%>
                                                                            </a>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>   
                                                        </tr> <%--FB 1911--%>
                                                        <tr id="recurDIV" style="display:none;">
                                                            <td align="left" valign="top">
                                                                 <span class="blackblodtext">
                                                                 <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SpecialRecurre%>" runat="server"></asp:Literal></span>
                                                            </td>
                                                            <td>                                                               
                                                                <asp:TextBox ID="RecurText" Enabled="false" runat="server" CssClass="altText" Rows="4" TextMode="MultiLine" Width="70%" Text="<%$ Resources:WebResources, NoRecurrence%>"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv5" style="display:none;">
                                                            <td class="blackblodtext" align="left">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Duration%>" runat="server"></asp:Literal></td>
                                                            <td align="left">    
                                                                <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                                                <asp:Button ID="btnRefresh" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, ConferenceSetup_btnRefresh%>" OnClick="CalculateDuration" OnClientClick="javascript:DataLoading(1);" runat="server" ValidationGroup="Update" />
                                                            </td>
                                                        </tr>
                                                        <tr id="TimezoneRow"><%--FB 2634--%>
                                                            <td class="blackblodtext" align="left">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_TimeZone%>" runat="server"></asp:Literal>
                                                                <span class="reqfldstarText">*</span></td>
                                                            <td>
                                                                     <%-- <asp:DropDownList ID="lstConferenceTZ"  Width="300px" CssClass="altSelectFormat">--%> <%--Code Modified For FB 1453--%>
                                                                     <asp:DropDownList ID="lstConferenceTZ" runat="server"  DataTextField="timezoneName" DataValueField="timezoneID" 
                                                                     onchange="javascript:fnSetTimezone();" CssClass="altText" Width="330px"> <%--FB 2699--%> <%--ZD 100221--%>
                                                                     </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--FB 2501 starts--%>
                                                         <tr id="trStartMode" runat="server"><%--FB 2501--%>
                                                            <td class="blackblodtext" align="left">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_StartMode%>" runat="server"></asp:Literal></td>
                                                            <td >
                                                             <asp:DropDownList ID="lstStartMode" runat="server" Width="30%" CssClass="alt2SelectFormat">
                                                                      <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, Automatic%>"></asp:ListItem>
                                                                      <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Manual%>"></asp:ListItem>
                                                             </asp:DropDownList> 
                                                            </td>
                                                        </tr>
                                                        <%--FB2501 ends--%>
                                                        
                                                       
                                                        <tr id="divDuration" style="display:none">
                                                            <td class="blackblodtext" align="left">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Duration%>" runat="server"></asp:Literal>
                                                                <span class="reqfldstarText">*</span></td>
                                                            <td><mbcbb:combobox id="lstDuration" runat="server" CssClass="altSelectFormat" Rows="10" CausesValidation="True" style="width:auto"><%--Edited for FF--%>
                                                                    <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                                                    <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                                                    <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                                                    <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                                                    <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                                                    <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                                                    <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                                                    <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                                                    <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                                                    <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                                    <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                                    <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                            </mbcbb:combobox> 
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_hhmm%>" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                     <%--FB 1722 - Start--%>
                                                     <tr><td colspan="2">
                                                         <%--<table border="0" cellpadding="0" width="100%"> --%><%--Edited for FF--%> <%--ZD 100381--%>
                                                         <tr id="DurationRow" style="display:none;">                                                       
                                                            <td id="tabCelldur" align="left" class="blackblodtext" valign="top" width="35%">
                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_tabCelldur%>" runat="server"></asp:Literal></td><%--Edited For FF --%>
                                                            <td>
                                                            <table border="0" cellpadding="0px" cellspacing="0" width="100%"><tr><td>
                                                     <%--FB 1722 - End--%>
                                                                <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" 
                                                                    onblur="Javascript:  return validateDurationHr();" 
                                                                    onchange="javascript: recurTimeChg();" Width="15%"></asp:TextBox> hrs
                                                                <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" 
                                                                    onblur="Javascript: return validateDurationMi();" 
                                                                    onchange="javascript: recurTimeChg();" Width="15%"></asp:TextBox> mins                
                                                                <br />
                                                                <%if (enableBufferZone == "0")
                                                                  { %> 
                                                                    ( <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, MaxLimit1%>" runat="server"></asp:Literal>
                                                                    <%=Application["MaxConferenceDurationInHours"]%> 
                                                                    <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, hours%>" runat="server"></asp:Literal>) 
                                                                 <%}
                                                                  else
                                                                  { %>
                                                                    ( <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, MaxLimit1%>" runat="server"></asp:Literal>
                                                                    <%=Application["MaxConferenceDurationInHours"]%> 
                                                                    <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ExpressConference_MaxLimit12%>" runat="server"></asp:Literal>) 
                                                                <%} %>
                                                                <asp:TextBox ID="EndText" runat="server" style="display:none" ></asp:TextBox>
                                                                </td></tr></table>
                                                            </td>
                                                            <%--Window Dressing--%>
                                                        </tr>
                                                    <%--</table>--%></td></tr><%--Edited for FF--%>
                                                    
                                                </table>
                                            </td>                                             
                                            <td valign="top" style="width:55%;"> <%--ZD 100393--%>
                                                <table width="100%">
                                                    <%--Merging Recurrence start--%>
                                                    <tr id="RecurrenceRow">
                                                        <td colspan="2" align="left">
                                                            <table width="100%" border="0" >
                                                                <tr>
                                                                    <td align="left" style="width:31%;" valign="top" >
                                                                        <table style="width:100%;margin-left:-15px;" border="0" cellpadding="3" >
                                                                            <tr>
                                                                                <td colspan="2"  valign="top" >
                                                                                    <table style="width:100%;" border="0" cellpadding="3">
                                                                                        <tr>
                                                                                            <td valign="top" align="left" colspan="2">
                                                                                              <span class="blackblodtext">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RecurringPatte%>" runat="server"></asp:Literal></span>       
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" style="width:20%;" align="left">   <%--ZD 103929--%>
                                                                                                <table cellpadding="3">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="RecurType_1" Text="<%$ Resources:WebResources, CalendarWorkorder_btnDaily%>" OnClick="javascript:return fnShow('1');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="RecurType_2" Text="<%$ Resources:WebResources, CalendarWorkorder_btnWeekly%>"  OnClick="javascript:return fnShow('2');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="RecurType_3" Text="<%$ Resources:WebResources, CalendarWorkorder_btnMonthly%>"  OnClick="javascript:return fnShow('3');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="RecurType_4" Text="<%$ Resources:WebResources, Yearly%>"  OnClick="javascript:return fnShow('4');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="RecurType_5" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Custom%>"  OnClick="javascript:return fnShow('5');" GroupName="RecurType" runat="server" CssClass="blackxxxsboldtext" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                
                                                                                               <%-- <asp:RadioButtonList ID="RecurType" runat="server" CssClass="blackxxxsboldtext" RepeatDirection="Vertical" OnClick="javascript:return fnShow();" CellPadding="3" >
                                                                                                    <asp:ListItem Value="1">Daily</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">Weekly</asp:ListItem>
                                                                                                    <asp:ListItem Value="3">Monthly</asp:ListItem>
                                                                                                    <asp:ListItem Value="4">Yearly</asp:ListItem>
                                                                                                    <asp:ListItem Value="5">Custom</asp:ListItem>
                                                                                                </asp:RadioButtonList>   --%>                                                                                                              
                                                                                            </td>
                                                                                            <td align="left" nowrap="nowrap" valign="top" class="blackxxxsboldtext">
                                                                                                <%--Daily Recurring Pattern--%>
                                                                                                &nbsp;
                                                                                                <asp:Panel ID="Daily" runat="server" HorizontalAlign="left" >
                                                                                                    <asp:RadioButton ID="DEveryDay" runat="server" GroupName="RDaily" /> <asp:Literal ID="Literal53" Text="<%$ Resources:WebResources, ExpressConference_Every%>" runat="server"></asp:Literal> 
                                                                                                    <asp:TextBox ID="DayGap" CssClass="altText"  runat="server" Width="8%" onClick="javaScript: DEveryDay.checked = true;" onChange="javaScript: summarydaily();"></asp:TextBox> 
                                                                                                    <asp:Literal ID="Literal54" Text="<%$ Resources:WebResources, ExpressConference_days%>" runat="server"></asp:Literal>
                                                                                                    <br />
                                                                                                    <asp:RadioButton ID="DWeekDay" CssClass="blackxxxstext" runat="server" GroupName="RDaily" onClick="javaScript: DayGap.value=''; summarydaily();" /> <asp:Literal ID="Literal55" Text="<%$ Resources:WebResources, ExpressConference_EveryWeekday%>" runat="server"></asp:Literal>
                                                                                                </asp:Panel>                
                                                                                                <%--Weekly Recurring Pattern--%>
                                                                                                <asp:Panel ID="Weekly" runat="server">
                                                                                                    <asp:Literal ID="Literal56" Text="<%$ Resources:WebResources, ExpressConference_RecurEvery%>" runat="server"></asp:Literal>
                                                                                                    <asp:TextBox ID="WeekGap" runat="server" CssClass="altText" Width="8%"></asp:TextBox><asp:Literal 
                                                                                                        ID="Literal57" runat="server" 
                                                                                                        Text="<%$ Resources:WebResources, ExpressConference_weekson%>"></asp:Literal>
                                                                                                    <asp:CheckBoxList ID="WeekDay" runat="server" CssClass="blackxxxsboldtext" RepeatDirection="horizontal" RepeatColumns="4" CellPadding="2" CellSpacing="3" onClick="javaScript: summaryweekly();" ></asp:CheckBoxList>
                                                                                                </asp:Panel>
                                                                                                <%--Monthly Recurring Pattern--%>
                                                                                                <asp:Panel ID="Monthly" runat="server">
                                                                                                    <asp:RadioButton ID="MEveryMthR1" runat="server" GroupName="GMonthly" onClick="javaScript: MonthGap2.value = ''; summarymonthly();" />  
                                                                                                    <asp:Literal ID="Literal58" Text="<%$ Resources:WebResources, ExpressConference_Day%>" runat="server"></asp:Literal> <asp:TextBox ID="MonthDayNo" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;" onChange="javaScript: summarymonthly();" class="altText"></asp:TextBox> 
                                                                                                    <asp:Literal ID="Literal59" Text="<%$ Resources:WebResources, ExpressConference_ofevery%>" runat="server"></asp:Literal> <asp:TextBox ID="MonthGap1" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;" onChange="javaScript: summarymonthly();" ></asp:TextBox> 
                                                                                                    <asp:Literal ID="Literal60" Text="<%$ Resources:WebResources, ExpressConference_months%>" runat="server"></asp:Literal>
                                                                                                    <br /><br />
                                                                                                    <asp:RadioButton ID="MEveryMthR2" runat="server" GroupName="GMonthly" onClick="javaScript: summarymonthly(); MonthDayNo.value = ''; MonthGap1.value = '';"  /> 
                                                                                                    <asp:Literal ID="Literal61" Text="<%$ Resources:WebResources, ExpressConference_The%>" runat="server"></asp:Literal> <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDayNo" onClick="javaScript: summarymonthly();"></asp:DropDownList>
                                                                                                    <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDay" onClick="javaScript: summarymonthly();" Width="90px"></asp:DropDownList>  <%--ZD 100393--%>
                                                                                                    <asp:Literal ID="Literal62" Text="<%$ Resources:WebResources, ExpressConference_ofevery%>" runat="server"></asp:Literal> <asp:TextBox ID="MonthGap2" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR2.checked = true;" onChange="javaScript: summarymonthly();"></asp:TextBox> 
                                                                                                    <asp:Literal Text="<%$ Resources:WebResources, ExpressConference_months%>" runat="server"></asp:Literal>
                                                                                                </asp:Panel>
                                                                                                <%--Yearly Recurring Pattern--%>
                                                                                                <asp:Panel ID="Yearly" runat="server">
                                                                                                    <asp:RadioButton ID="YEveryYr1" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly();" />  
                                                                                                    <asp:Literal ID="Literal64" Text="<%$ Resources:WebResources, ExpressConference_Every%>" runat="server"></asp:Literal> <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth1" onClick="javaScript: summaryyearly();"></asp:DropDownList> 
                                                                                                    <asp:TextBox ID="YearMonthDay" runat="server" CssClass="altText" Width="8%" onChange="javaScript: summaryyearly();" onClick="YEveryYr1.checked = true;" ></asp:TextBox>
                                                                                                    <br /><br />
                                                                                                    <asp:RadioButton ID="YEveryYr2" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly(); document.frmSettings2.YearMonthDay.value = '';"/> 
                                                                                                    <asp:Literal ID="Literal65" Text="<%$ Resources:WebResources, ExpressConference_The%>" runat="server"></asp:Literal> <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDayNo" onClick="javaScript: summaryyearly();" Width="70px"></asp:DropDownList> <%-- ZD 100393--%>
                                                                                                    <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDay" onClick="javaScript: summaryyearly();"></asp:DropDownList> 
                                                                                                    <asp:Literal ID="Literal66" Text="<%$ Resources:WebResources, ExpressConference_of%>" runat="server"></asp:Literal> <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth2" onClick="javaScript: summaryyearly();"></asp:DropDownList>
                                                                                                </asp:Panel>
                                                                                                <%--Custom Recurring Pattern--%>
                                                                                                <asp:Panel ID="Custom" runat="server" >
                                                                                                    <table border="0" width="370px">
                                                                                                        <tr>
                                                                                                            <td style="width:210px"><%--FB 2274--%>                            
                                                                                                                <a href="#" class="name" onclick="return false;">
                                                                                                                <div id="flatCalendarDisplay" style="float: left; clear: both;background-color:#D4D0C8"></div></a><br />  <%--ZD 100426--%> <%--ZD 100639--%>                             
                                                                                                                <div id="Div2" style="font-size: 80%; text-align: center; padding: 2px"></div>
                                                                                                            </td>
                                                                                                            <td style="width:120px">
                                                                                                                <span > 
                                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SelectedDate%>" runat="server"></asp:Literal></span>
                                                                                                                </span><br />
                                                                                                                <asp:ListBox runat="server" id="CustomDate" Rows="8" CssClass="altSmall0SelectFormat" onChange="JavaScript: removedate(this);"></asp:ListBox>
                                                                                                                <br />
                                                                                                                <span>
                                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_clickadateto%>" runat="server"></asp:Literal></span>
                                                                                                            </td>
                                                                                                            <td style="width:40px">                            
                                                                                                                 <%--code added for Soft Edge button--%>
                                                                                                                <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/><%--edited for FF--%>
                                                                                                                <button ID="btnsortDates" runat="server" class="altMedium0BlueButtonFormat" style="width:65px" onclick="javascript:return SortDates();">
                                                                                                                    <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnsortDates%>" runat="server"></asp:Literal></button><%--ZD 100420--%> <%--ZD 100419--%>
                                                                                                            </td>
                                                                                                        </tr>                                                                                            
                                                                                                    </table>
                                                                                                    
                                                                                                </asp:Panel>
                                                                                            </td>
                                                                                        </tr> 
                                                                                       
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="RangeRow" runat="server">
                                                                                <td colspan="2"> 
                                                                                    <table border="0" width="100%" cellpadding="5">
                                                                                        <tr>
                                                                                            <td colspan="2"> 
                                                                                              <span class="blackblodtext">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RangeofRecurr%>" runat="server"></asp:Literal></span>       
                                                                                            </td>            
                                                                                        </tr>
                                                                                        <tr valign="top"  >
                                                                                            <td class="blackblodtext" nowrap style="width:20%;" align="left">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Start%>" runat="server"></asp:Literal> 
                                                                                                <asp:TextBox ID="StartDate" Width="100px" Font-Size="9" CssClass="altText" runat="server"  ></asp:TextBox>
																									<%--ZD 100420--%>
                                                                                                      <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_triggerd').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}">
                                                                                                <img alt="Date Selector" src="image/calendar.gif" border="0"  id="cal_triggerd1" style="cursor: pointer;vertical-align:top;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onClick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd1', 1, '<%=format %>');" /></a><%--ZD 100420--%><%--ZD 100419--%>
                                                                                            </td>    
                                                                                            <td >
                                                                                                <table width="100%" border="0"> 
                                                                                                    <tr>
                                                                                                        <td class="blackxxxsboldtext" colspan="2">
                                                                                                            <asp:RadioButton ID="EndType" runat="server" GroupName="RangeGroup"  onClick="javascript: document.frmSettings2.Occurrence.value=''; document.frmSettings2.EndDate.value='';"/> 
                                                                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Noenddate%>" runat="server"></asp:Literal>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td  class="blackxxxsboldtext" nowrap style="width:9%;">
                                                                                                            <asp:RadioButton ID="REndAfter" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.EndDate.value='';"/> 
                                                                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Endafter%>" runat="server"></asp:Literal>
                                                                                                        </td>
                                                                                                        <td class="blackxxxsboldtext" nowrap>
                                                                                                            <asp:TextBox ID="Occurrence" CssClass="altText"  Width="96px" runat="server" onClick="javascript: document.frmSettings2.REndAfter.checked=true; document.frmSettings2.EndDate.value='';"> <%--ZD 100393--%></asp:TextBox> 
                                                                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_occurrences%>" runat="server"></asp:Literal>                          
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td  class="blackxxxsboldtext">
                                                                                                            <asp:RadioButton ID="REndBy" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value='';"/> 
                                                                                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Endby%>" runat="server"></asp:Literal>
                                                                                                        </td>
                                                                                                        <td nowrap>
                                                                                                            <asp:TextBox ID="EndDate" Width="96px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)" CssClass="altText" runat="server"  onClick="javascript: document.frmSettings2.REndBy.checked=true; document.frmSettings2.Occurrence.value='';" ></asp:TextBox> <%--ZD 100393--%>
																											<%--ZD 100420--%>
                                                                                                            <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_trigger2').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}">
                                                                                                            <img alt="Date Selector" src="image/calendar.gif" border="0"  id="cal_trigger2" style="cursor: pointer;vertical-align:top;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onClick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" /></a><%--ZD 100420--%>   <%--ZD 100419--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>       
                                                                                        </tr>
                                                                                         <tr>
                                                                                            <td colspan="2">
                                                                                                <asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, recurNET_NoteMaximumli%>" runat="server"></asp:Literal> <%--ZD 101837--%>
                                                                                               <span id="spnRecurLimit"> <%=Session["ConfRecurLimit"]%> </span> <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, recurNET_NoteMaximumli1%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>  
                                                                        </table> 
                                                                    </td>
                                                                    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                               <%--Merging Recurrence end--%>
                                                </table>
                                            </td>
                                        </tr>
                                  </table>
                           <script language="javascript">
                           //FB 2634
                            document.getElementById("regConfStartTime").controltovalidate = "confStartTime_Text";
                            document.getElementById("reqConfStartTime").controltovalidate = "confStartTime_Text";
                            document.getElementById("regEndTime").controltovalidate = "confEndTime_Text";
                            document.getElementById("reqEndTime").controltovalidate = "confEndTime_Text";
//                            document.getElementById("regSetupStartTime").controltovalidate = "SetupTime_Text"; //buffer zone
//                            document.getElementById("reqSetupStartTime").controltovalidate = "SetupTime_Text";
//                            document.getElementById("regTearDownStartDate").controltovalidate = "TeardownTime_Text";//buffer zone // Edited for FF
                            
                            //alert("here");
                            //setTimeout("document.getElementById('confStartTime_Text').onchange = javascript:ChangeEndDate()", 1);
                            //setTimeout("document.getElementById('confEndTime_Text').onchange = javascript:ChangeStartDate()", 1);
                            //alert(document.getElementById("<%=Recur.ClientID %>").value);
//FB 2634
//				        if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
//				        {
//				            isRecur();
////				            alert(document.getElementById("<%=Recur.ClientID %>").value);
//					        AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
//					        st = calStart(atint[1], atint[2], atint[3]);
//					        et = calEnd(st, parseInt(atint[4], 10));
//					        //Merging Recurrence
//					        document.getElementById("RecurringText").value =  recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et, "Eastern Standard Time", Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
//					    }
//                        isRecur();
                         //ChangeImmediate();
                         ChangePublic();
                        //Recurrence Fixes - hiding recur icon on single instance edit - start
                        
                        if ("<%=isInstanceEdit%>" == "Y" )
                        {  
                            document.getElementById("RecurRow").style.display = "none"; //FB 2634
                        }
                        
                         //Recurrence Fixes - hiding recur icon on single instance edit - end
                        
                        if ("<%=timeZone%>" == "0" ) //FB 1425
                            document.getElementById("TimezoneRow").style.display = "none";//FB 2634
                                                   
                           //MOJ Phase 2 - Start
						//FB 2634
                        if ("<%=client.ToString().ToUpper() %>" == "MOJ")
                        {
                            document.getElementById("StartNowRow").style.display = "none";
                            //document.getElementById("trPublic").style.display = "none";  //FB 2359
                            document.getElementById("trConfType").style.display = "none"; 
                            document.getElementById("ConfStartRow").style.display =  "none"; //buffer zone
	                        document.getElementById("ConfEndRow").style.display =  "none"; //buffer zone
                            
                        }
                        
                        fnVMR();
                        //MOJ Phase 2 - End 
                         
                         //Code added for  Fb 1728
                         if(document.getElementById("txtConfRequestor"))
                             document.getElementById("txtConfRequestor").value = "<%=Session["userName"]%>";  
        
          
     if(document.getElementById("hdnValue").value == "1")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
    	
    	if (document.getElementById("ModifyType") != null)//FB 2694
	        if (document.getElementById("ModifyType").value=="3") {
		        document.getElementById("RemoveRecurDiv").style.display = "none";
	    }
        
         var chkrecurrence = document.getElementById("chkRecurrence");
         
        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
        {
            
            var chkrecurrence = document.getElementById("chkRecurrence");
            if(chkrecurrence)
                chkrecurrence.checked = true;
            document.getElementById("hdnRecurValue").value = 'R'
            initial();
           
            fnShow();
        }
        
         fnEnableBuffer();
        
     }
     //Code added for  Fb 1728
                           
                    </script>
                  </asp:Panel>
                </asp:View>
                <asp:View ID="SelectParticipants" runat="server" OnActivate ="GetConfTimeforAudbridge"> <%--ZD 104524--%>
                    <asp:Panel ID="pnlEndpoint" runat="server" Width="100%">
                            <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SelectParticip%>" runat="server"></asp:Literal></h3>
                            <input type="hidden" id="Hidden1" value="17">
                                 <table border="0" cellpadding="2" cellspacing="0" width="95%" height="95" align="center">
             <!--                     <tr>
                                    <td width="10%" height="15" align="left" valign="top"></td>
                                    <td width="90%" bordercolor="#0000ff" colspan="4" align="left">
                                      <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                          <td align="center" width="4%"><span class="tableblueblodtext">DELETE</span></td>
                                          <td align="center" width="28%"><span class="tableblueblodtext">NAME</span></td>
                                          <td align="center" width="29%"><span class="tableblueblodtext">EMAIL</span></td>
                                          <td align="center" width="8%"><span class="tableblueblodtext"><a>External Attendees</a></span></td>
                                          <td align="center" width="8%"><span class="tableblueblodtext"><a>Room Attendees</a></span></td>
                                          <td align="center" width="4%"><span class="tableblueblodtext">CC</span></td>
                                          <td align="center" width="4%"><span class="tableblueblodtext">NOTIFY</span></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>-->
                                  <tr>
                                    <td height="21" align="left" valign="top" width="150" class="blackblodtext">
                                      <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Participants%>" runat="server"></asp:Literal><br />
                                      <table>
                                      <tr>
                                      <td>
                                      <button name="AddGroup" type="button" id="btnAddgrp" style="width:80px; height:35px" class="altMedium0BlueButtonFormat" runat="server" onClick="saveInGroupNET();return false;" ><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, AddtoGroup%>" runat="server"></asp:Literal></button> <%--ZD 100875--%></td>
                                      <td>
                                      <asp:ImageButton ID="ImgGrpbtnNote" src="image/info.png" AlternateText="Info" ToolTip="<%$ Resources:WebResources, AddGroupMsg%>" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"/> <%--ZD 100419--%><%-- ZD 102590 --%>
                                      <%--FB 1985--%>
                                      <span style="color:Red;" ><asp:Label runat="server" ID="lblParNote" Visible="true"></asp:Label></span>
                                      <input type="button" name="sbtgroup" id="sbtgroup" style="display:none;" onClick="saveGroupSucc()" /> <%--Login Management--%>
                                      <input type="button" name="cbtgroup" id="cbtgroup" style="display:none;" onClick="saveGroupFail('<%=Session["GrpErrMsg"]%>')" /> <%--Login Management--%>
                                      
            <%--<iframe src="ifrmsaveingroup.asp?wintype=ifr" name="ifrmSaveingroup" width="0" height="0"> FB 412 --%>
            <iframe src="ifrmsaveingroup.aspx?wintype=ifr" name="ifrmSaveingroup" style="display:none" width="0" height="0"> <%--Edited for FF--%>
              <p><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SaveinGroupp%>" runat="server"></asp:Literal></p>
            </iframe>  
            </td>    
            </tr>           
            </table>         
                                    </td>
                                    <td bordercolor="#0000ff" colspan="5" align="center">
                                      <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                          <td width="100%" valign="top" align="left" colspan="6">

                                            <!--Changed a href Start-->
                                            <iframe align="left" height="350" name="ifrmPartylist" id="ifrmPartylist" src="../en/settings2partyNET.aspx?wintype=ifr" valign="top" width="100%"><%--Edited for FF--%>
                                              <p><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_goto%>" runat="server"></asp:Literal><a href="../en/settings2partyNET.aspx?wintype=ifr"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Participants%>" runat="server"></asp:Literal></a></p> <%--ZD 101714--%>
                                            </iframe> 
                                            <!--Changed a href End-->

                                          </td>
                                        </tr>
                                      </table>
                                      <%--the following 2 controls have been added to confirm the validation on next and previous click under CheckFiles function--%>
                                      <asp:TextBox ID="txtTempUser" runat="server" Visible="false" Text="<%$ Resources:WebResources, ConferenceSetup_txtTempUser%>" ></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="reqUser" ControlToValidate="txtTempUser" runat="server"></asp:RequiredFieldValidator>
                                    </td>
                                  </tr>
                                  <tr> <%--FB 1985 - Starts--%>                                    
                                    <td height="62" align="left" valign="top" class="blackblodtext"> 
                                    <%if (!(Application["Client"].ToString().ToUpper().Equals("DISNEY"))){%> 
                                      <asp:Literal Text="<%$ Resources:WebResources, Groups%>" runat="server"></asp:Literal>
                                      <% }%> 
                                    </td>
                                    <td id="trGroup" runat="server">
                                        <asp:ListBox ID="Group" runat="server" CssClass="altSelectFormat" SelectionMode="Multiple">
                                        </asp:ListBox>
                                        <asp:TextBox ID="txtUsersStr" runat="server" Width="0px" ForeColor="transparent" TabIndex="-1" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                                        <asp:TextBox ID="txtPartysInfo" style="display:none" runat="server" Width="0px" ForeColor="Black" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox><%--Edited for FF--%>
                                    </td>
                                    <td align="left" valign="middle"><font size="1" class="blackblodtext">
                                    <%if (!(Application["Client"].ToString().ToUpper().Equals("DISNEY"))){%> 
                                    <asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, AddGroup%>" runat="server"></asp:Literal></font>
                                    <% }%> 
                                    </td> <%--FB 1985 - End--%>
                                    <td width="21%" class="blackblodtext">
                                        <%--Window Dressing--%>
                                        <label class="blackblodtext"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, SendiCalInvitations%>" runat="server"></asp:Literal></label>
                                        <asp:CheckBox ID="chkICAL" runat="server" class="blackblodtext" Text="" TextAlign="left" />
                                        <br /><font size="1"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Notavailablef%>" runat="server"></asp:Literal></font>
                                    </td>
                                    <td width="31%" align="left" valign="top">
                                      <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                          <td width="100%" align="left">
                                            <button id="Button2" runat="server" name="Settings2Submit" class="altMedium0BlueButtonFormat" onClick="deleteAllPartyNET();return false;" language="JavaScript"  type="button"><asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, RemoveAll%>" runat="server"></asp:Literal></button> <%--ZD 100875--%>
                                            
                                            </td>
                                         
                                          <%--FB 1985 - Starts--%>
                                         <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY"))){%>
                                          <td width="100%" align="left">
                                            <button runat="server" id="VRMLookup1" class="altLongBlueButtonFormat" onClick="getYourOwnEmailListNET();return false;" language="JavaScript"  type="button"><asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, AddressBook%>" runat="server"></asp:Literal></button></td> <%--ZD 100875--%>
                                            <br />
                                            <% } else { %>
                                          <td width="100%" align="left"> <%--ZD 100666--%>
                                            <button runat="server" id="VRMLookup" type="button" class="altLongBlueButtonFormat" onClick="getYourOwnEmailListNET();return false;" language="JavaScript" style="width:220px;"><asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, AddressBook%>" runat="server"></asp:Literal></button></td>
                                            <br />
                                            <% } %><%--FB 1985 - End--%>
                                        </tr>
                                        <tr>
                                          <td width="100%" align="right" colspan="2">
                                          
                                            <button runat="server" id="btnAddNewParty" class="altLongBlueButtonFormat" onClick="javascript:addNewPartyNET(1);return false;" type="button" style="width:220px;">
                                            <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, AddNewParticipant%>" runat="server"></asp:Literal></button> <%--ZD 100875--%>
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                        <td>
                                        <button runat="server" id="btnAudioparticipant" class="altLongBlueButtonFormat" onClick="getAudioparticipantListNET();return false;" type="button"  style="width:220px;"><asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, AddAudioBridge%>" runat="server"></asp:Literal></button> <%--ZD 100875--%>
                                        </td>
                                        </tr>
                                        <tr><td  colspan="2"><table cellpadding="0px" cellspacing="0px"  border="0" width="50%" align="left"><%--Edited for FF--%>
                                        <tr id="trOutlook" align="left"> <%-- FB Case 526: Saima--%>
                                          <td>
                                            <input type="button" runat="server" id="OutlookLookup" value="<%$ Resources:WebResources, OutlookAddressBook%>" style="width:220px;" class="altLongBlueButtonFormat" onClick="javascript:getOutLookEmailList();"></td>
                                        </tr>
                                        </table></td></tr>
                                        <tr id="trLotus" style="display:none">
                                            <td width="100%" align="left" colspan="2">
                                                <asp:Button id="btnLotus" runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ConferenceSetup_btnLotus%>"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td width="100%" align="left" colspan="2" height="20">
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td style="width:3%"><%--FB 2023--%>
                                     <table width="100%" border="0">
                                     <tr>
                                          <td width="100%" align="left" colspan="2" height="60">
                                          </td>
                                      </tr>
                                      <tr style="vertical-align:bottom">
                                       <td style="display:none"><%--ZD 102323--%>
                                        <asp:ImageButton ID="imgParNote" src="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"/> <%--ZD 100419--%><%-- ZD 102590 --%>
                                       </td>
                                      </tr>
                                      <tr>
                                          <td width="100%" align="left" colspan="2" height="20">
                                          </td>
                                      </tr>
                                     </table>                                       
                                    </td>
                                   </tr>
                                  </table>
                                <script language="javascript">
                                //FB 1985 - Starts
                                  if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
                                   {
                                     document.getElementById("trGroup").style.display = "none";
                                   }
                                   //FB 1985 - End
                                    var recurText = document.getElementById("Recur").value;
                                    function dothis()
                                    {
                                        //alert("before dothis");
                                        ifrmPartylist.bfrRefresh();
                                        //alert("after dothis");
                                    }
                                    if (recurText != "")
                                    {
                                        //alert(recurText.split("#")[1].split("&")[0]);
                                        if (recurText.split("#")[1].split("&")[0] == "5")
                                        {
                                            document.getElementById("<%=chkICAL.ClientID %>").disabled = true;
                                            //document.getElementById("<%=chkICAL.ClientID %>").checked = false;
                                        }
                                        else
                                        {
                                            document.getElementById("<%=chkICAL.ClientID %>").disabled = false;
                                            //document.getElementById("<%=chkICAL.ClientID %>").checked = true;
                                        }
                                    }
            /* FB Case 727 Saima
                                    if(document.all)
                                        document.getElementById("ifrmPartylist").attachEvent("onblur",dothis);
                                    else
                                        document.getElementById("ifrmPartylist").contentDocument.addEventListener("blur",dothis,false);
            */                            
                                    if (document.getElementById("trOutlook") != null) // FB Case 526: Saima
                                    {   
                                        /*Code Modified For Enabling Outlook button on  21Mar09 -  FB 412 - Start */
                                        //if ("<%=Session["emailClient"] %>" != "1")
                                            //document.getElementById("trOutlook").style.display = "none";
                                            if ("<%=Session["emailClient"] %>" == "1")
                                            document.getElementById("trOutlook").style.display = "block";
                                        else
                                             document.getElementById("trOutlook").style.display = "none"; 
                                        /*Code Modified For Enabling Outlook button on  21Mar09 -  FB 412 - End */
                                    }
                                    if (document.getElementById("trLotus") != null) // FB Case 576: Saima
                                    {   
                                        if ("<%=Session["emailClient"] %>" != "2")
                                            document.getElementById("trLotus").style.display = "none";
                                    }
                                </script>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="SelectRooms" runat="server" OnActivate="ExpandTree"  OnDeactivate="SelectTree">
                                    <asp:Panel ID="pnlResource" runat="server" Width="100%">
                <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SelectRooms%>" runat="server"></asp:Literal></h3>
                <input type="hidden" id="Hidden2" value="18">

<!--                <img src="image/locationlist.jpg" />-->
               <table border="0" cellpadding="3" cellspacing="0" width="100%">
                   <tr>
                       <td align="left" valign="top" width="10%">
                       </td>
                       <td align="left" class="blackblodtext" valign="top" width="78%">
                        <table border="0" style="width: 100%">
                            <tr style="display:none;">
                                <td valign="top" align="left" width="80">
                                <%--Code changed for Search Room Error - start --%>                                
                                <input type="button" value="Compare" id="btnCompare" runat="server" onClick="javascript:compareselected();" class="altShortBlueButtonFormat" />
                                <%--Code changed for Search Room Error - end --%>                                
                                </td>
                                <td valign="top" align="left" class="blackblodtext">
                                   <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                      RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow"><%--FB 1481--%>
                                      <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, LevelView%>"></asp:ListItem>
                                      <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ListView%>"></asp:ListItem>
                                  </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>                     
                       </td>
                   </tr>

                  <tr>
                    
                    <td width="100%" colspan="2" align="left" style="font-weight: bold; font-size: small; color: blue; font-family: arial" valign="top"><%-- FB 2794--%>
                    <%-- Code added for Room Search --%>
    <iframe id="RoomFrame" runat="server" width="100%" valign="top" height="600px"></iframe>
    <div style="display:none;">
                    <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                      <%--  <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%" ShowCheckBoxes="All" onclick="javascript:getRooms(event)"
                            ShowLines="True" Width="95%" OnTreeNodeCheckChanged="treeRoomSelection_TreeNodeCheckChanged" OnSelectedNodeChanged="treeRoomSelection_SelectedNodeChanged" >
                            <NodeStyle CssClass="treeNode" />
                            <RootNodeStyle CssClass="treeRootNode" />
                            <ParentNodeStyle CssClass="treeParentNode" />
                            <LeafNodeStyle CssClass="treeLeafNode" />
                        </asp:TreeView>--%>  <%--ZD 101175--%>
                        
                        </asp:Panel>
                        <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial" Font-Size="Small" ForeColor="Green">
                            <%--code changed for FB 1319 -- start--%>
                            <input type="checkbox" id="selectAllCheckBox" runat="server" onClick="CheckBoxListSelect('lstRoomSelection',this);" />
                            <font size="2"> 
                            <asp:Literal runat="server" 
                                Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>"></asp:Literal></font>
                            <br />
                            <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" onclick="javascript:getValues(event)" Font-Names="arial" RepeatLayout="Flow">
                            </asp:CheckBoxList>
                            <%--code changed for FB 1319 -- end--%>
                        </asp:Panel>
                         <%--Code added for Search Room Error - start --%>                                
                        <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                            <table><tr align="center">
                                <td>
                            <asp:Literal runat="server" 
                                        Text="<%$ Resources:WebResources, ConferenceSetup_YouhavenoRoo%>"></asp:Literal></td></tr></table>                                                      
                        </asp:Panel>
                         <%--Code added for Search Room Error - start --%>  
                        <asp:TextBox runat="server" ID="txtTemp" Text="<%$ Resources:WebResources, ConferenceSetup_txtTemp%>" Visible="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp" runat="server" ></asp:RequiredFieldValidator>
                        
                        </div>
                    </td>
                  </tr>
                  
                  <tr>
                    <td></td>
                    <td align="right">
                      <table border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td align="left">
                            <label class="blackblodtext" style="display:none;">Check Availability</label>
                          </td>
                          <td align="left">
                            <asp:Button runat="server" ID="GetAvailableRoom" Text="<%$ Resources:WebResources, ConferenceSetup_GetAvailableRoom%>" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:DataLoading(1);" OnClick="RefreshRoom" style="display:none;"/>
                          </td>
                           <!--Meeting Planner FB 1048-->
                           <td id="tdMeetingPlanner" runat="server"> <%--FB 1985 ZD 100875--%>
                          <button ID="MeetingPlanner" class="altLongBlueButtonFormat" type="button" onclick="javascript:return CallMeetingPla(); return false;"  runat="server">
                              <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MeetingPlanner%>" runat="server"></asp:Literal></button>
                          </td>
                          <td align="left" style="font-weight:bold" id="btnCheckAvailDIV" runat="server"> <%--FB 2274, ZD 100875--%>
                              <button ID="openCalendar" runat="server" class="altMedium0BlueButtonFormat" type="button" onclick="javascript:goToCal(); return false;">
                                  <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_openCalendar%>" runat="server"></asp:Literal></button>
                          </td>
                        </tr>
                        <%--FB 1985 - Start--%>
                        <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                          {%> 
                        <tr>
                            <td align="left" colspan="4" style="padding:0px;">
                                <table width="60px" border="0" align="left" id="tblMeetPlan" runat="server">
                                    <tr> 
                                        <td align="center" onMouseOver="javascript:return fnShowHideMeetLink('1');" onMouseOut="javascript:return fnShowHideMeetLink('0');" >&nbsp;
                                            <asp:LinkButton ID="LnkMeetExpand" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_LnkMeetExpand%>" OnClientClick="javascript:return fnShowMeetPlanner()"></asp:LinkButton>
                                        </td>
                                     </tr>
                                </table>
                            </td>
                        </tr>
                         <%} %>
                         <%--FB 1985 - End--%>
                      </table>
                    </td>
                  </tr>
                 </table>
                 <input type="hidden" runat="server" id="txtHasCalendar" />
                 <script language="javascript">
                   if(document.getElementById("<%=txtHasCalendar.ClientID %>").value.toUpperCase() == "FALSE")
                        document.getElementById("btnCheckAvailDIV").style.display = "none";
                 </script>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="SelectAudioVisual" runat="server" >
                                    <asp:Panel ID="pnlAudioVisual" runat="server" Width="100%" HorizontalAlign="center">
                                      <%--FB 2359--%>
                                    <table id="tblAVExpand" runat="server" width="8%" border="0" align="left" style="display:none">
                                        <tr> 
                                            <td align="left" onMouseOver="javascript:return fnShowHideAVLink('1');" onMouseOut="javascript:return fnShowHideAVLink('0');" >&nbsp;
                                                <asp:LinkButton ID="LnkAVExpand" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_LnkAVExpand%>" OnClientClick="javascript:return fnShowAVParams()"></asp:LinkButton>
                                            </td>
                                         </tr>
                                    </table>
                    <%--FB 1985--%>
                <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                  {%>
                <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AudioSettings%>" runat="server"></asp:Literal></h3>
                <%} else{%>
                <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AdvancedAudioV%>" runat="server"></asp:Literal></h3>
                <%} %>
                <table width="100%" cellspacing="2" cellpadding="2">
                    <%-- Code Modified For FB 1422 - Added ID  to <tr> tag --%>             
                    <tr id="trAVCommonSettings" runat="server" align="left" width="100%"> <%--Edited for FF--%>
                        <td>
                            <table border="0" width="1200px" align="left"> <%--ZD 101869--%>
                                <tr>
                                    <td colspan="4" class="subtitlexxsblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CommonSettings%>" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left" class="blackblodtext">
                                       <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RestrictNetwor%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstRestrictNWAccess" runat="server" CssClass="altSelectFormat">
                                            <asp:ListItem Text="IP" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN,SIP" Value="3"></asp:ListItem>
                                            <%--<asp:ListItem Text="IP,ISDN,SIP,MPI" Value="4" Selected="True"></asp:ListItem> --%><%--FB 1721--%><%--FB 2390--%><%--ZD 103595--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RestrictUsage%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstRestrictUsage" runat="server" CssClass="altSelectFormat">
                                            <%--<asp:ListItem Text="None" Value="1"></asp:ListItem>--%><%--FB 1744--%>
                                            <asp:ListItem Text="Audio only" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Audio/Video" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqUsage" ControlToValidate="lstRestrictUsage" InitialValue="0" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                   </td>
                                </tr>
                                <tr>
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MaximumVideoP%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMaxVideoPorts" runat="server" CssClass="altText" ></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regMaxVideoPorts" ControlToValidate="txtMaxVideoPorts" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                    </td>
                                    <td align="left" class="blackblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MaximumAudioP%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMaxAudioPorts" CssClass="altText" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regMaxAudioPorts" ControlToValidate="txtMaxAudioPorts" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VideoCodecs%>" runat="server"></asp:Literal></span><span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstVideoCodecs" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqVideoCodecs" ControlToValidate="lstVideoCodecs" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AudioCodecs%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstAudioCodecs" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqAudioCodecs" ControlToValidate="lstAudioCodecs" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DualStreamMod%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkDualStreamMode" runat="server" />
                                    </td>
                                     <td align="left" class="blackblodtext" id="tdVideoDisplay" runat="server" > <%--ZD 101931 --%>
                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VideoDisplay%>" runat="server"></asp:Literal></td>
                                    <td nowrap="nowrap" id="tdimgVideoDisplay" runat="server" >
                                        <asp:Label runat="server" id="lblCodianLO" class="blackblodtext" ></asp:Label> <%--ZD 101931 --%>
                                        <asp:Image ID="imgVideoDisplay" runat="server" AlternateText="Video Display" /> <%--ZD 100419--%>
                                        <asp:image id="imgLayoutMapping6" runat="server" alt="Layout"  style="display:none" />
                                        <asp:image id="imgLayoutMapping7" runat="server" alt="Layout"  style="display:none;" />
                                        <asp:image id="imgLayoutMapping8" runat="server" alt="Layout"  style="display:none" />
                                        <button id="btnConfLayoutSubmit" runat="server" name="ConfLayoutSubmit" onkeydown="if(event.keyCode == 13){ javascript:managelayout('', '01', '','0');return false;}"  class="altMedium0BlueButtonFormat" onClick="managelayout('', '01', '','1');return false;" type="button"><asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, Change%>" runat="server"></asp:Literal></button> <%--ZD 100875--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Encryption%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td align="left">
                                       <asp:CheckBox ID="chkEncryption" runat="server" />
                                    </td>
                                    <td align="left" class="blackblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SingleDialinN%>" runat="server"></asp:Literal>
                                    </td>
                                    <td align="left">
                                       <asp:CheckBox ID="chkSingleDialin" runat="server" />
                                    </td>
                                </tr>
                                <tr style="display:none;"><%--FB 2341--%>
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_LiveAssistant%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkisLiveAssitant" runat="server" />
                                    </td>
                                    <td align="left" class="blackblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DedicatedEngin%>" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkisDedicatedEngineer" runat="server" />
                                    </td>
                                </tr>
                                <%--Code chaned for FB 1360 --%>
                                <tr>
                                    <td id="tdlinerate" runat="server" align="left" class="blackblodtext"> <%--FB 2641--%>
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MaximumLineRa%>" runat="server"></asp:Literal></span><span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqMaxLineRate" ControlToValidate="lstLineRate" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <%--FB 2501 Starts--%>
                                    <td id="tdFECC" runat="server" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_tdFECC%>" runat="server"></asp:Literal></td> <%--FB 2571--%>
                                    <td><asp:CheckBox ID="chkFECC" runat="server" /></td>
                                 </tr>
								<%--FB 2501 Ends--%>
                                
                                <%--Code chaned for FB 1360 --%>
                                 <tr>
                                    <td colspan="4" class="subtitlexxsblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PolycomSpecifi%>" runat="server"></asp:Literal>
                                        <asp:CheckBox ID="chkPolycomSpecific" onclick="javascript:CheckPolycom(this)" runat="server" />
                                    </td>
                                </tr>
                                <tr id="trPoly1">
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VideoMode%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoMode" runat="server" DataTextField="Name" DataValueField="ID">
                                        </asp:DropDownList>
                                    </td>
                                    <%--Code chaned for FB 1360 --%>
                                    <td colspan="2"></td>
                                    <%--Code chaned for FB 1360 --%>
                                </tr>
                                <tr id="trPoly2">
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_LectureMode%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkLectureMode" runat="server" onclick="javascript:CheckLectureMode()" />
                                    </td>
                                    <td align="left" id="trLecture1" class="blackblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_trLecture1%>" runat="server"></asp:Literal>
                                    </td>
                                    <td align="left" id="trLecture2">
                                        <asp:DropDownList ID="lstEndpoints" runat="server" CssClass="altSelectFormat"></asp:DropDownList>
<%--                                        <asp:RequiredFieldValidator ID="reqLecturer" InitialValue="-1" ControlToValidate="lstEndpoints" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic"></asp:RequiredFieldValidator>
--%>                                    </td>
                                </tr>
                                <tr id="trPoly3">
                                    <td align="left" class="blackblodtext">
                                        <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceonP%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkConfOnPort" runat="server" />
                                    </td>
                                    <%--FB 2441 Starts--%>
                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SendMail%>" runat="server"></asp:Literal></td>
                                    <td>
                                         <asp:CheckBox ID="chkSendMail" runat="server" />
                                    </td>
                                </tr>
<%--                                <tr id="trPoly4">
                                <td align="left" class="blackblodtext">
                                    <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceTemp%>" runat="server"></asp:Literal></span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_polycomTemplate" CssClass="altText" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="reg_polycomTemplate" ControlToValidate="txt_polycomTemplate" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>--%><%--ZD 100263--%>
                                    <%--<asp:DropDownList Width="37%" ID="txtProfileID" CssClass="alt2SelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                    <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                <%--</td>--%>
                                <%--FB 2441 Ends--%>
                                <%--</tr>--%><%--Commented for ZD 100298--%>
                                                               
                                <%-- FB 2426 Starts --%>
                                </table>
                        </td>
                    </tr>
                        <tr id="trGuestLocation" runat="server" align="left">
                            <td colspan="4" class="subtitlexxsblueblodtext">
                                <asp:Literal ID="Literal82" Text="<%$ Resources:WebResources, ConferenceSetup_GuestLocation%>" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr id="trVideoGuestLocation" runat="server" align="left">
                         <td>
                            <table width="450px">
                                <tr>
                            <td class="blackblodtext" nowrap="nowrap"> <%--ZD 101344--%>
                                <span style="margin-left:20px"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VideoGuestLoc%>" runat="server"></asp:Literal></span>
                            </td>
                            <td id="tdbtnGuestLocation" colspan="3">
                                <button ID="btnGuestLocation" runat="server" style="width:280px;"
                                    class="altLongBlueButtonFormat" onserverclick="fnGuestLocationCancel" onclick="javascript:return fnValidator();" type="button"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnGuestLocation%>" runat="server"></asp:Literal></button><br /> <%--ZD 100875--%>
                            </td>
                            </tr>
                            </table>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <ajax:ModalPopupExtender ID="guestLocationPopup" runat="server" TargetControlID="btnGuestLocation"
                                    PopupControlID="PopupLdapPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                                    CancelControlID="ClosePUp" BehaviorID="btnGuestLocation">
                                </ajax:ModalPopupExtender>
                                <asp:Panel ID="PopupLdapPanel" runat="server" Width="98%" Height="98%" HorizontalAlign="Center"
                                    CssClass="treeSelectedNode" ScrollBars="Vertical">
                                    <asp:UpdatePanel ID="updtGuestLocation" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" >
                                    <ContentTemplate>
                                    <input type="hidden" id="expColStat" runat="server" value="0" />
                                    <table align="center" cellpadding="3" cellspacing="0" width="98%" style="border-collapse: collapse;
                                        height: 100%;">
                                        <tr>
                                            <td align="center" bgcolor="#E0E0E0"><%--ZD 100426--%>
                                                <table width="100%" border="0" cellpadding="3" style="border-collapse: collapse;
                                                    height: 100%;">
                                                    <tr>
                                                        <td colspan="6" style="padding-bottom:20px">
                                                            <h3>
                                                                <asp:Literal ID="Literal72" Text="<%$ Resources:WebResources, ConferenceSetup_VideoGuestLoc%>" runat="server"></asp:Literal></h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">        
                                                                                                                 
                                                            <table border="0" style="margin-bottom:20px">
                                                            <tr>
                                                                    <td style="width: 1%;">
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" style="width:10%">
                                                                         <asp:Literal ID="Literal73" Text="<%$ Resources:WebResources, ConferenceSetup_ConnectionType%>" runat="server"></asp:Literal> 
                                                                    </td>
                                                                    <td align="left" style="width:11%">
                                                                         <asp:DropDownList ID="lstGuestConnectionType" Width="187px" CssClass="altSelectFormat"
                                                                            runat="server" DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="GuestDftConnctType" AutoPostBack="true" >
                                                                        </asp:DropDownList><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 1%;">
                                                                    </td>

                                                                    <td align="left" nowrap="nowrap" class="blackblodtext">
                                                                       <asp:Literal ID="Literal74" Text="<%$ Resources:WebResources, ExpressConference_GuestSiteName%>" runat="server"></asp:Literal>   <span class="reqfldstarText">*</span>
                                                                    </td>
                                                                    <td align="left">

                                                                        <asp:TextBox ID="txtsiteName" runat="server" CssClass="altText" MaxLength="20" Width="187px" /><br /><%--FB 2523--%><%--FB 2995--%>
                                                                
                                                                        <asp:RequiredFieldValidator ID="reqRoomName" Enabled="false" runat="server" ControlToValidate="txtsiteName"
                                                                        Display="dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                
                                                                        <asp:RegularExpressionValidator ID="regRoomName" Enabled="false" ControlToValidate="txtsiteName"
                                                                        Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters35%>"
                                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator><%-- ZD 103424 ZD 104000--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal75" Text="<%$ Resources:WebResources, ConferenceSetup_ContactName%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span>
                                                                    </td>
                                                                    <td align="left" nowrap="nowrap">
                                                                        <asp:TextBox ID="txtApprover5" runat="server" CssClass="altText" Width="187px" />
                                                                        <a href="" onclick="this.childNodes[0].click();return false;"><img id="Img10" onClick="javascript:getYourOwnEmailList(4,0)" alt="Edit" src="image/edit.gif" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a>
                                                                         <asp:TextBox ID="hdnApprover5" runat="server" BackColor="Transparent" BorderColor="White"
                                                                            BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                                                        <a href="javascript:deleteAssistant();"  onclick="javascript:document.getElementById('hdnSaveData').value = '1';" onmouseover="window.status='';return true;"><%--ZD 100875--%>
                                                                            <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>" /></a><br /> <%--FB 2798--%>
                                                                        <asp:RequiredFieldValidator ID="reqcontactName" Enabled="false" runat="server" ControlToValidate="txtApprover5" 
                                                                            Display="dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                    </td> 
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal76" Text="<%$ Resources:WebResources, ConferenceSetup_ContactEmail%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span><%--ZD 101790--%>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtEmailId" runat="server" CssClass="altText" Width="187px" /><br />
                                                                        <asp:RequiredFieldValidator ID="reqcontactEmail"  Enabled="false" runat="server" ControlToValidate="txtEmailId" SetFocusOnError="true"
                                                                            ValidationGroup="Submit1" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="regTestemail" runat="server" ControlToValidate="txtEmailId"
                                                                            ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>" Display="dynamic"  ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                                                        <asp:RegularExpressionValidator ID="regcontactEmail" ControlToValidate="txtEmailId"
                                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters%>"
                                                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="hdnGusetAdmin" runat="server" BackColor="Transparent" BorderColor="White"
                                                                            BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal77" Text="<%$ Resources:WebResources, ExpressConference_ContactPhone%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left">
<asp:TextBox ID="txtContactPhone" runat="server" CssClass="altText" Width="187px" />
                                                                        
                                                                    </td>
                                                                </tr>
                                                                 <tr>                                                                                
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="2" align="left" >
                                                                        <br /><br />
                                                                        <span class="blackblodtext"> <asp:Literal ID="Literal78" Text="<%$ Resources:WebResources, Profiles%>" runat="server"></asp:Literal></span> 
                                                                        <asp:ImageButton ID="imgRoomList" ImageUrl="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;" ToolTip="<%$ Resources:WebResources, MaxProfiles%>"  /><br /><%-- ZD 102590 --%>
                                                                        <span class="blackblodtext"><asp:Literal ID="Literal79" Text="<%$ Resources:WebResources, ConferenceSetup_DialingInforma%>" runat="server"></asp:Literal></span> 
                                                                    </td>                                                                                    
                                                                </tr>
                                                              </table>
                                                        </td>

                                                        <td width="50%" valign="top">   
                                                            <table border="0">
                                                                <tr>
                                                                    <td align="left" class="blackblodtext" valign="top" colspan="2" >
                                                                        <span style="vertical-align:top"><asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, ConferenceSetup_Address%>" runat="server"></asp:Literal></span>                                                                                   
                                                                        <asp:imagebutton alternatetext="Expand/Collapse" cssclass ="0"  id="ImgbtnAddress" runat="server" imageurl="image/loc/nolines_plus.gif" 
                                                                        height="25" width="25" vspace="0" hspace="0" OnClientClick="javascript:return ExpandAll();" /> 
                                                                        <asp:ImageButton ID="imgAddressList" ImageUrl="image/info.png" AlternateText="Info" runat="server" style="cursor:default"  OnClientClick="javascript:return false;"  ToolTip="<%$ Resources:WebResources, MsgAddressList%>"  /><%-- ZD 102590 --%>
                                                                    </td>
                                                                    <td style="width: 5%">
                                                                    </td
                                                                    
                                                                </tr>
                                                                </table>                                                                  
                                                            <table id="tblAddress" border="0" style="padding-bottom:15px; visibility:hidden;">                                                                               
                                                                <tr>
                                                                    <td style="width: 1%">
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" style="width: 10%">
                                                                        <asp:Literal ID="Literal26" Text="<%$ Resources:WebResources, ConferenceSetup_Address%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left" style="vertical-align: top; width: 31%">
                                                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="altText" TextMode="MultiLine"
                                                                            Width="183px" />
                                                                        <asp:RegularExpressionValidator ID="reqAddress2" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal27" Text="<%$ Resources:WebResources, ConferenceSetup_StateProvince%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtState" runat="server" CssClass="altText" Width="187px" />
                                                                        <asp:RegularExpressionValidator ID="reqState" ControlToValidate="txtState" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext">
                                                                        <asp:Literal ID="Literal28" Text="<%$ Resources:WebResources, ConferenceSetup_City%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtCity" runat="server" CssClass="altText" Width="187px" />
                                                                        <asp:RegularExpressionValidator ID="reqCity" ControlToValidate="txtCity" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal29" Text="<%$ Resources:WebResources, ConferenceSetup_PostalCode%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtZipcode" runat="server" CssClass="altText" Width="187px" />
                                                                        <asp:RegularExpressionValidator ID="reqZipcode" ControlToValidate="txtZipcode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal30" Text="<%$ Resources:WebResources, ConferenceSetup_SiteCountry%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                                            DataValueField="ID" Width="187px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                        <asp:Literal ID="Literal31" Text="<%$ Resources:WebResources, ConferenceSetup_RoomPhone%>" runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtPhone" runat="server" CssClass="altText" Width="187px" />
                                                                        <asp:RegularExpressionValidator ID="reqPhone" ControlToValidate="txtPhone" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <asp:DataGrid runat="server" ID="dgProfiles" AutoGenerateColumns="false" OnItemDataBound="InitializeGuest" OnItemCreated="InitializeGuest"
                                                              CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="100%" style="border-collapse:separate">
                                                            <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                            <EditItemStyle CssClass="tableBody" />
                                                            <AlternatingItemStyle CssClass="tableBody" />
                                                            <ItemStyle CssClass="tableBody" />
                                                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                                            <Columns>
                                                               <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ProfileNo%>" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Top" />
                                                                    <HeaderStyle CssClass="tableHeader" />
                                                                    <ItemTemplate>                          
                                                                        <asp:Label ID="lblProfileCount" Text="" runat="server" Font-Bold="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Profiles%>" HeaderStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="Left" Height="25" />
                                                                    <ItemTemplate>
                                                                    <table width="100%">
                                                                        <tr>                               
                                                                            <td align="left" class="blackblodtext">
                                                                                <asp:Literal ID="Literal32" Text="<%$ Resources:WebResources, ConferenceSetup_Address%>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span></td><%--ZD 101790--%>
                                                                            <td align="left" nowrap="nowrap" style="width:38%">
                                                                                <asp:TextBox CssClass="altText"  ID="txtGuestAddress" runat="server" Width="187px" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>' onchange="CheckIPAddress(this)" ></asp:TextBox>                                 
                                                                                <asp:RequiredFieldValidator ID="reqGuestAddress" Enabled="false" runat="server" ControlToValidate="txtGuestAddress"
                                                                                    Display="dynamic"  ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="regIPAddress" Enabled="false" ControlToValidate="txtGuestAddress"
                                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>"
                                                                            ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                           <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                                <asp:Literal ID="Literal33" Text="<%$ Resources:WebResources, ConferenceSetup_ConnectionType%>" runat="server"></asp:Literal> 
                                                                            </td>
                                                                            <td align="left" style="width:28%">
                                                                                    <asp:DropDownList ID="lstGuestConnectionType1" Width="187px" CssClass="altSelectFormat" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>'
                                                                                     OnSelectedIndexChanged="ChangeMCU"  AutoPostBack="true" runat="server" DataTextField="Name" DataValueField="ID" >
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqGuestConnectionType" runat="server" InitialValue="-1" ControlToValidate="lstGuestConnectionType1" ValidationGroup="Submit1" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                         </tr>
                                                                         <tr>
                                                                            <td align="left" class="blackblodtext">
                                                                                <asp:Literal ID="Literal34" Text="<%$ Resources:WebResources, ConferenceSetup_AddressType%>" runat="server"></asp:Literal> <span style="color:Red">*</span></td>
                                                                            <td align="left">
                                                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstGuestAddressType" runat="server" Width="187px" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>'></asp:DropDownList> 
                                                                                <asp:RequiredFieldValidator ID="reqGuestAddressType" runat="server" InitialValue="-1" ControlToValidate="lstGuestAddressType" ValidationGroup="Submit1" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator> 
                                                                            </td>
                                                                             <td align="left" class="blackblodtext">
                                                                             <asp:Literal ID="Literal35" Text="<%$ Resources:WebResources, ExpressConference_LineRate%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                                                             </td>
                                                                            <td align="left">
                                                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstGuestLineRate" runat="server" Width="187px" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.MaxLineRate") %>'></asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqGuestLineRate" runat="server"  ControlToValidate="lstGuestLineRate" ValidationGroup="Submit1" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator> 
                                                                            </td>
                                                                        </tr>
                                                                         <tr>                               
                                                                            <td align="left" class="blackblodtext">
                                                                                <asp:Literal ID="LblGstEP" Text="<%$ Resources:WebResources, ExpressConference_AssignedtoMCU%>" runat="server"></asp:Literal> </td>
                                                                            <td align="left">
                                                                               <asp:DropDownList CssClass="altSelectFormat" ID="lstGuestBridges" runat="server" Width="187px" DataTextField="BridgeName" DataValueField="BridgeID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'  onchange="javascript:document.getElementById('hdnSaveData').value = '1';__doPostBack(this.id,'');"  OnSelectedIndexChanged="ChangeMCU" ></asp:DropDownList>
                                                                               <asp:DropDownList CssClass="altSelectFormat" ID="lstTelnetUsers" Visible="false"  Width="187px" runat="server" onChange="javascript:document.getElementById('hdnChgCallerCallee').value = '1';">  <%--ZD 100815--%> <%--ZD 103402--%> 
                                                                            <asp:ListItem Value="0"  Text="<%$ Resources:WebResources, ExpressConference_Callee%>" ></asp:ListItem> 
                                                                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_Caller%>"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                            </td>
                                                                           <td style="display:none">
                                                                           <asp:DropDownList CssClass="altSelectFormat" ID="lstBridgeType" runat="server" DataTextField="BridgeType" DataValueField="BridgeID"  SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' ></asp:DropDownList> <%--ZD 100393--%>
                                                                           <asp:TextBox CssClass="altText"  ID="txtRowID" runat="server" Visible="false" value='<%# DataBinder.Eval(Container, "DataItem.RowUID") %>' ></asp:TextBox>                                 
                                                                           </td>
                                                                            <td></td>
                                                                         </tr>
                                                                    </table>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                    </FooterTemplate>
                                                                    <FooterStyle BackColor="beige" />
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Default%>" ItemStyle-VerticalAlign="Top">
                                                                <HeaderStyle CssClass="tableHeader" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rdDefault" runat="server" GroupName="DefaultProfile" OnCheckedChanged="SetDftProfile" AutoPostBack="true" onclick="javascript:DataLoading(1);" Checked='<%# DataBinder.Eval(Container, "DataItem.DefaultProfile").Equals("1") %>' /></td>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" ItemStyle-VerticalAlign="Top">
                                                                <HeaderStyle CssClass="tableHeader" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkDelete" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Deleted").Equals("1") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                            <SelectedItemStyle BackColor="Beige" />
                                                         </asp:DataGrid>  
                                                        </td>  
                                                    </tr>
                                                   
                                                    <tr align="center" style="height:50%">
                                                        <td align="center" colspan="6">
                                                            <br />
                                                            <div>
                                                                <asp:Button ID="btnUpdtGrid" runat="server" onclick="GusetLocClose" style="display:none"/>
                                                            </div>
                                                            <button align="middle" runat="server" id="ClosePUp" 
                                                                class="altMedium0BlueButtonFormat" onclick="javascript:return fnDisableValidator();"><asp:Literal ID="Literal37" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
                                                            <input type ="button" ID="btnGuestAddProfile" value="<%$ Resources:WebResources, ExpressConference_AddNewprofile%>"  runat="server" onserverclick="fnGuestAddProfile" style ="width:260px" 
                                                                ValidationGroup="Submit1" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';return ValidateflyEndpoints();" class="altLongBlueButtonFormat" />
                                                            <asp:Button CausesValidation="false" ID="btnGuestLocationSubmit" runat="server" Text="<%$ Resources:WebResources, Submit%>" ValidationGroup="Submit1"
                                                                OnClick="fnGuestLocationSubmit" class="altMedium0BlueButtonFormat" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';return ValidateflyEndpoints();"/>
                                                              <%--<asp:CustomValidator ID="cvSubmit" ValidationGroup="Submit1" OnServerValidate="ValidateInput" SetFocusOnError="true" runat="server" ErrorMessage="Invalid IP/ISDN Address" Display="dynamic"></asp:CustomValidator>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    </ContentTemplate> 
                                    <Triggers>
                                    <asp:PostBackTrigger ControlID="btnGuestLocationSubmit" />                                                   
                                    <asp:PostBackTrigger ControlID="ClosePUp" />                                                  
                                    
                                    </Triggers>
                                 
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="OnFlyRowGuestRoom" runat="server">
                            <td style="text-align: left" colspan="4">
                                <asp:DataGrid ID="dgOnflyGuestRoomlist" runat="server" AutoGenerateColumns="False"
                                    CellPadding="4" BorderColor="blue" BorderStyle="solid" BorderWidth="1" GridLines="None"
                                    OnEditCommand="dgOnflyGuestRoomlist_Edit" OnDeleteCommand="dgOnflyGuestRoomlist_DeleteCommand"
                                    OnItemCreated="BindRowsDeleteMessage" Width="100%" AllowSorting="True" Style="border-collapse: separate">
                                    <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                    <Columns>
                                        <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, GuestRoomName%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContactName" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ExpressConference_GuestRoomIncharge%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContactEmail" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContactPhoneNo" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContactAddress" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="State" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="City" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ZIP" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Country" Visible="false"></asp:BoundColumn>
                                        
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate><%--ZD 100875--%>
                                                <asp:LinkButton ID="btnGuestRoomEdit" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Edit" runat="server" Text="<%$ Resources:WebResources, Edit%>"></asp:LinkButton>
                                                <asp:LinkButton ID="btnGuestRoomDelete" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Delete" runat="server" Text="<%$ Resources:WebResources, Delete%>"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                         <asp:BoundColumn DataField="GuestContactPhoneNo" Visible="false"></asp:BoundColumn> <%--ZD 100619--%>
                                         <asp:BoundColumn DataField="DftConnecttype" Visible="false"></asp:BoundColumn> <%--ZD 100619--%>
                                         <asp:BoundColumn DataField="GuestAdmin" Visible="false"></asp:BoundColumn> <%--ZD 100619--%> 
                                         <asp:BoundColumn DataField="GuestAdminID" Visible="false"></asp:BoundColumn> <%--ZD 100619--%> 
                                    </Columns>
                                    
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableBody" Font-Bold="True" />
                                    <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                    <%--Window Dressing End--%>
                                </asp:DataGrid>
                            </td>
                        </tr>
                         
                    <tr  id="trp2pLinerate" runat="server" style="display:none;">
                    <td width="100%" align="left" class="blackblodtext">
                            <asp:Literal ID="Literal36" Text="<%$ Resources:WebResources, point2point_MaximumLineRa%>" runat="server"></asp:Literal> <%--ZD 101344--%>
                            <span class="reqfldstarText">*</span>
                            <asp:DropDownList CssClass="altSelectFormat" ID="DrpDwnLstRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ControlToValidate="DrpDwnLstRate" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                       
                        </td>
                    </tr>
                    <tr id="trRooms" runat="server"><%--FB 2359--%>
                        <td colspan="4" class="subtitlexxsblueblodtext"><asp:Literal ID="Literal24" Text="<%$ Resources:WebResources, ConferenceSetup_Rooms%>" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr id="trRoomsDetails" runat="server"><%--FB 2359--%>
                        <td align="center">
                            <asp:DataGrid runat="server" EnableViewState="true" OnItemDataBound="InitializeLists" ID="dgRooms" AutoGenerateColumns="false"
                              CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="98%" style="border-collapse:separate"> <%--Edited for FF--%>
                            <AlternatingItemStyle CssClass="tableBody" HorizontalAlign="left" />
                            <ItemStyle CssClass="tableBody" HorizontalAlign="left" />
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                                    <HeaderTemplate>
                                        <table width="100%">
                                            <tr><%--FB 2839 Start--%>
                                                <td width="15%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RoomName%>" runat="server"></asp:Literal></td>
                                                <td width="15%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Endpoint%>" runat="server"></asp:Literal></td>
                                                <td width="15%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_EndpointProfil%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                                <td width="15%" class="tableHeader"><asp:Label id="LblEP" runat="server" text="<%$ Resources:WebResources, ConferenceSetup_LblEP%>"></asp:Label><span class="reqfldstarText">*</span></td>
                                                <td width="15%" class="tableHeader"><asp:Label id="LblMCUProf" runat="server" text="<%$ Resources:WebResources, ConferenceSetup_LblMCUProf%>" visible="false"></asp:Label></td>
                                                <td width="15%" class="tableHeader"><asp:Label id="LblPoolOrder" runat="server" text="<%$ Resources:WebResources, PoolOrder%>" visible="false"></asp:Label></td> <%--ZD 104256--%>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="lblRoomName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                            </td>
                                            <td style="width:15%" class="tableBody">
                                                <asp:Label ID="lblEndpointName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndpointName") %>'></asp:Label>
                                                <asp:Label ID="lblEndpointID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndpointID") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblDefaultProfileID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileID") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblApiportno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APIPortNo") %>' Visible="false" ></asp:Label><%--API Port--%>
                                                <asp:Label ID="lblIsTelepresence" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isTelePresence") %>' Visible="false" ></asp:Label><%--FB 2400--%>
                                                <asp:Label ID="lblIsTestEquipment" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsTestEquipment") %>' Visible="false" ></asp:Label><%--ZD 101815--%>
                                                <asp:Label ID="lblAddressType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' Visible="false" ></asp:Label><%--ZD 101815--%>
                                            </td>
                                            <td style="width:15%">
                                                <asp:DropDownList CssClass="altSelectFormat" Enabled="false" ID="lstProfiles" runat="server" DataTextField="ProfileName" DataValueField="ProfileID" 
                                                onChange="javascript:document.getElementById('hdnSaveData').value = '1';" OnSelectedIndexChanged="ChangeDefaultBridge" AutoPostBack="true"></asp:DropDownList> <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>'--%> <%--ZD 100875--%>
                                                <asp:DropDownList Visible="false" ID="lstProfileType" runat="server" DataTextField="AddressType" DataValueField="ProfileID"></asp:DropDownList> <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>'--%>                                  
                                                <asp:DropDownList Visible="false" ID="lstProfileBridge" runat="server" DataTextField="Bridge" DataValueField="ProfileID"></asp:DropDownList> <%-- FB Case 198: Saima --%>                                  
                                                <asp:RequiredFieldValidator ID="reqProfiles" ControlToValidate="lstProfiles" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width:15%">
                                                <asp:DropDownList CssClass="altSelectFormat" Enabled="false" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" 
                                                onChange="javascript:document.getElementById('hdnSaveData').value = '1';" OnSelectedIndexChanged="ChangeBridgeProfile" AutoPostBack="true"></asp:DropDownList>  <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%> <%--ZD 100875--%>
                                                <asp:DropDownList CssClass="altSelectFormat" Visible="false" Enabled="false" ID="lstTelnet" runat="server"  onChange="javascript:document.getElementById('hdnChgCallerCallee').value = '1';" ><%--ZD 103402--%>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_Caller%>"></asp:ListItem> <%--101344--%>
                                                    <asp:ListItem Value="0"  Text="<%$ Resources:WebResources, ExpressConference_Callee%>" ></asp:ListItem> <%--101344--%>
                                                </asp:DropDownList> 
                                                <asp:Label ID="lblRoomTelnet" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Connect2") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td style="width:15%">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstMCUProfile" runat="server" DataTextField="Name" DataValueField="ID" onChange="javascript:document.getElementById('hdnSaveData').value = '1';" Enabled="false" OnSelectedIndexChanged="ChangeConfProfile" AutoPostBack="true" ></asp:DropDownList>        <%--ZD 100875--%>                                        
                                                <asp:Label ID="lstMCUProfSelected" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeProfileID") %>' Visible="false"></asp:Label> <%--FB 2839--%>
                                            </td>
                                            <td style="width:15%"> <%--ZD 104256--%>
                                                <asp:DropDownList CssClass="altSelectFormat" ID="drpPoolOrder" runat="server" DataTextField="Name" DataValueField="ID" onChange="javascript:document.getElementById('hdnSaveData').value = '1';" Enabled="false" OnSelectedIndexChanged="ChangeConfPoolOrder" AutoPostBack="true"></asp:DropDownList>
                                                <asp:Label ID="lstPoolOrderSelected" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgePoolOrder") %>' Visible="false"></asp:Label> <%--FB 2839--%>
                                            </td>
                                        </tr>
                                    </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ConferenceSetup_UseDefault%>" ItemStyle-VerticalAlign="Top">
                                <HeaderStyle CssClass="tableHeader" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkUseDefault" AutoPostBack="true" onclick="javascript:document.getElementById('hdnSaveData').value = '1';" OnCheckedChanged="EnableControls" runat="server" Checked='<%# !Request.QueryString["t"].ToString().Trim().Equals("") %>' /> <%-- Saima: fogbugz case 153 --%><%--ZD 100875--%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <SelectedItemStyle BackColor="Beige" />
                         </asp:DataGrid>
                         <%--Window Dressing--%>
                         <asp:Label id="lblNoRooms" runat="server" text="<%$ Resources:WebResources, ConferenceSetup_lblNoRooms%>" visible="False" cssclass="lblError"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                         <%--FB 1315--%>
                        <td colspan="4" class="subtitlexxsblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ExternalUsers%>" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:DataGrid runat="server" OnItemCreated="InitializeUsers" ID="dgUsers" AutoGenerateColumns="false" EnableViewState="true"
                            CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="98%" style="border-collapse:separate" OnItemDataBound="BindUsers"> <%--Edited for FF--%>
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                                    <ItemTemplate>
                                    <table width="100%"> <%-- FB 2359 --%> 
                                        <tr>
                                            <td align="left" colspan="6"> <%--ALLDEV-814--%>
                                                <asp:Label ID="lblUserName" CssClass="subtitlexxsblueblodtext" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                                <asp:TextBox ID="txtAudioaddon" runat="server" TabIndex="-1" Text='<%# DataBinder.Eval(Container, "DataItem.Audioaddon") %>' Width="0" Height="0" BackColor="Transparent" BorderColor="transparent" ForeColor="transparent" />
                                                <asp:TextBox ID="txtisUserAudio" runat="server" TabIndex="-1" Text='<%# DataBinder.Eval(Container, "DataItem.isUserAudio") %>' Width="0" Height="0" BackColor="Transparent" BorderColor="transparent" ForeColor="transparent" />
                                                <asp:TextBox ID="lblProfileID" runat="server" TabIndex="-1" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileID") %>' Width="0" Height="0" BackColor="Transparent" BorderColor="transparent" ForeColor="transparent" />
                                                <asp:TextBox ID="lblUserID" runat="server" TabIndex="-1" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Width="0" Height="0" BackColor="Transparent" BorderColor="transparent" ForeColor="transparent" />
                                                <asp:TextBox ID="txtEmail" runat="server" TabIndex="-1" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>' Width="0" Height="0" BackColor="Transparent" BorderColor="transparent" ForeColor="transparent" />
                                            </td>
                                            </tr>
                                            <tr>
                                            <td align="left" class="blackblodtext" style="width:15%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AddressPhone%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                            <td align="left"  style="width:21%">
                                                <asp:TextBox CssClass="altText" ID="txtAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="txtAddress" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td colspan="2">
                                             <table id="tbMCUandConn" runat="server" width="100%" border="0">
                                              <tr>
                                                  <td align="left" class="blackblodtext"  style="width:27%"><asp:Label ID="LblEPUsers" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_LblEPUsers%>" ></asp:Label><span class="reqfldstarText">*</span></td><%--ZD 104256--%>
                                                  <td align="left"   style="width:50%"><%--ZD 104256--%>
                                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" 
                                                    onChange="javascript:document.getElementById('hdnSaveData').value = '1';" OnSelectedIndexChanged="ChangeUserBridgeProfile" AutoPostBack="true" ></asp:DropDownList> <% //FB 1468 %>  <%--FB 2839--%><%--ZD 100875--%>
                                                     <asp:DropDownList CssClass="altSelectFormat" Visible="false" ID="lstTelnetUsers" runat="server" onChange="javascript:document.getElementById('hdnChgCallerCallee').value = '1';"> <%--ZD 103402--%>
                                                    <%-- ZD 101344 start--%> 
                                                         <asp:ListItem Value="0"  Text="<%$ Resources:WebResources, ExpressConference_Callee%>" ></asp:ListItem> 
                                                         <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ExpressConference_Caller%>"></asp:ListItem> <%-- ZD 101344--%> 
                                                    </asp:DropDownList> 
                                                    <asp:RequiredFieldValidator ID="reqBridge" Enabled="false" ControlToValidate="lstBridges" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' Visible="false"></asp:Label> <%--FB 2819--%>
                                                  </td>
                                              </tr>
                                             </table>
                                            </td>
                                             <%--ZD 104256 Start--%>
                                                <td align="left" class="blackblodtext">
                                                    <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_APIPort%>" runat="server"></asp:Literal></td>
                                                <td align="left" nowrap>
                                                    <asp:TextBox CssClass="altText"  ID="txtApiportno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APIPortNo") %>' MaxLength="5" TextMode="SingleLine"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtApiportno" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                </td>
                                             <%--ZD 104256 End--%>
                                        </tr>
                                        <tr id="AudioParams1" runat="server">
                                        <td align="left" class="blackblodtext" style="width:20%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Connection%>" runat="server"></asp:Literal></td>
                                                  <td align="left">
                                                    <asp:DropDownList ID="lstConnection" runat="server" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" Enabled="false" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Connection") %>'>
                                                    </asp:DropDownList>
                                                  </td>
                                            <td align="left" class="blackblodtext" style="width:10%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Protocol%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="lstProtocol" runat="server" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProtocol") %>'></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqProtocol" ControlToValidate="lstProtocol" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator><%--ALLBUGS-26--%>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AddressType%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqAddressType" ControlToValidate="lstAddressType" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="AudioParams2" runat="server">
                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_LineRate%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bandwidth") %>'></asp:DropDownList>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                <asp:RequiredFieldValidator ID="reqLineRate" ControlToValidate="lstLineRate" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator><%--FB 2444--%>
                                            </td>
                                           
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Equipment%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.VideoEquipment") %>'></asp:DropDownList>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                <asp:RequiredFieldValidator ID="reqEquipment" ControlToValidate="lstVideoEquipment" InitialValue="0" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConnectionType%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>'></asp:DropDownList>  <%--Fogbugz case 427--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" is="reqConnectionType" runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                            </td>
                                            
                                        </tr>
                                        <tr id="AudioParams3" runat="server">
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_URL%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                 <%--Window Dressing--%>                                                
                                                <asp:TextBox ID="txtEndpointURL" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.URL") %>'></asp:TextBox>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_EmailID%>" runat="server"></asp:Literal></td> <%-- ICAL Cisco Telepresence fix--%>
                                        <td align="left">
                                            <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExchangeID") %>' TextMode="SingleLine"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters3%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--ZD 104256 Start--%>
                                        <td align="left" class="blackblodtext"><asp:Literal ID="Literal86" Text="<%$ Resources:WebResources, ConferenceSetup_OutsideNW%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                <asp:CheckBox ID="chkIsOutside" EnableViewState="true" runat="server" Checked='<%# (DataBinder.Eval(Container, "DataItem.IsOutside").ToString().Equals("1")) %>'></asp:CheckBox>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                <asp:Label ID="Label1" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.IsOutside") %>'></asp:Label>
                                            </td>
                                        <%--ZD 104256 End--%>
                                        </tr>
                                        <tr>                                            
                                            <td align="left" class="blackblodtext">
                                            <asp:Label id="LblConfCode" cssclass="blackblodtext" runat="server" text="<%$ Resources:WebResources, ConferenceSetup_LblConfCode%>"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtConfCode" CssClass="altText" ReadOnly ='<%# (DataBinder.Eval(Container, "DataItem.Audioaddon").ToString().Equals("0") || ( DataBinder.Eval(Container, "DataItem.Audioaddon").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.isUserAudio").ToString().Equals("0"))) %>' runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConfCode") %>'></asp:TextBox>
                                            </td>
                                            
                                            <td align="left" class="blackblodtext"  style="width:11%">
                                                <asp:Label ID="LblLeaderpin" CssClass="blackblodtext" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_LblLeaderpin%>"></asp:Label>
                                            </td> 
                                            <td align="left" style="width:20%">
                                                <asp:TextBox ID="txtleaderPin" CssClass="altText" ReadOnly='<%# (DataBinder.Eval(Container, "DataItem.Audioaddon").ToString().Equals("0") || ( DataBinder.Eval(Container, "DataItem.Audioaddon").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.isUserAudio").ToString().Equals("0"))) %>' runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.LPin") %>'></asp:TextBox>
                                             </td>
                                             <%--ZD 104256 Start--%>
                                             <td align="left" class="blackblodtext">
                                                <asp:Label ID="LblPartyCode" CssClass="blackblodtext" runat="server" Text="<%$ Resources:WebResources, ParticipantCode%>"></asp:Label>
                                            </td> 
                                            <td align="left">
                                                <asp:TextBox ID="txtPartyCode" CssClass="altText" ReadOnly='<%# (DataBinder.Eval(Container, "DataItem.Audioaddon").ToString().Equals("0") || ( DataBinder.Eval(Container, "DataItem.Audioaddon").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.isUserAudio").ToString().Equals("0"))) %>' runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.participantCode") %>'></asp:TextBox>
                                             </td>
                                             <%--ZD 104256 End--%>
                                        </tr>                                        
                                         <%--ZD 104256 Starts--%>
                                        <tr>
                                        <td id="tdusrMCUprofile" runat="server" align="left" visible="false" class="blackblodtext" style="width:18%" > <%--FB 2839--%>
                                                    <asp:Literal ID="Literal84" Text="<%$ Resources:WebResources, ConferenceSetup_tdusrMCUprofile%>" runat="server"></asp:Literal>
                                                  </td>
                                                  <td align="left">
                                                     <asp:DropDownList CssClass="altSelectFormat" Visible="false" ID="lstMCUProfile" runat="server" onChange="javascript:document.getElementById('hdnSaveData').value = '1';"  DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="ChangeUserConfProfile" AutoPostBack="true" >
                                                     </asp:DropDownList><%--ZD 100875--%>
                                                     <asp:Label ID="lstMCUProfSelected" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeProfileID") %>' Visible="false"></asp:Label> <%--FB 2839--%>
                                                  </td>
                                               <td id="tdUserPoolOrder" runat="server" align="left"  visible="false" class="blackblodtext" style="width:11%" >
                                                    <asp:Literal ID="Literal83" Text="<%$ Resources:WebResources, PoolOrder%>" runat="server"></asp:Literal>
                                                  </td>
                                              <td  align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" Visible="false" ID="drpPoolOrder" runat="server" DataTextField="Name" DataValueField="ID" onChange="javascript:document.getElementById('hdnSaveData').value = '1';" OnSelectedIndexChanged="ChangeUserConfPool" AutoPostBack="true"></asp:DropDownList> 
                                                <asp:Label ID="lstPoolOrderSelected" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgePoolOrder") %>' Visible="false"></asp:Label> 
                                            </td>
                                            <td></td><td></td>
                                        </tr>
                                        <%--ZD 104256 Ends--%>
                                    </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ConferenceSetup_UseDefault%>" ItemStyle-VerticalAlign="Top">
                                <HeaderStyle CssClass="tableHeader" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkUseDefault" runat="server" Checked="false" onclick="javascript:CheckDefault(this)" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <SelectedItemStyle BackColor="Beige" />
                         </asp:DataGrid>
                        <%--Code added for FB 1315--%>
                         <asp:Label ID="lblNoUsers" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_lblNoUsers%>" Visible="False" CssClass="lblError"></asp:Label>                        
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="hasVisited" runat="server" Text="" Visible="false"></asp:TextBox>
                <asp:TextBox ID="ImageFiles" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="ImageFilesBT" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="ImagesPath" runat="server" Text=""></asp:TextBox>
                <input id="hdnFamilyLayout" type="hidden" runat="server" value="0" /><%--ZD 101869--%>
                <asp:TextBox ID="txtSelectedImage" runat="server" Text="01"></asp:TextBox> <%--FB 2524--%>
                <script language="javascript" type="text/javascript">
                    CheckPolycom(document.getElementById("<%=chkPolycomSpecific.ClientID%>"));
                </script>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="SelectAV" runat="server" OnLoad="SetInstructionsAV">
                    <asp:Panel ID="pnlAV" runat="server" Width="100%">
                <center>
                    <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AudiovisualWor%>" runat="server"></asp:Literal></h3><%-- FB 2570 --%>
                </center>
                <input type="hidden" id="Hidden3" value="14">
                <input type="hidden" id="lblTab" value="1" />
                <input type="hidden" id="AVTab" runat="server" value="1" /><%-- ZD 100716 --%>
                    <table width="100%">
                    <tr>
                        <td width="100%" align="left" style="height: 18px">
                            <asp:Label ID="lblAVWOInstructions" runat="server" Width="90%" CssClass="blackblodtext"></asp:Label>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<table width="100%">
                                <tr>
                                    <td colspan="3">
                                    
                            <asp:DataGrid ID="AVMainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4" BorderColor="blue" BorderStyle="solid" BorderWidth="1"
                            GridLines="None" OnCancelCommand="AVMainGrid_Cancel" OnUpdateCommand="AVMainGrid_Update" OnEditCommand="AVMainGrid_Edit" OnDeleteCommand="AVMainGrid_DeleteCommand"
                            OnItemCreated="BindRowsDeleteMessage" Width="100%" AllowSorting="True" style="border-collapse:separate"> <%--Edited for FF--%>
                            <%--Window Dressing--%>
                            <FooterStyle CssClass="tableBody" Font-Bold="True" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, Name%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToName" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, AssignedTo%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByDate" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ConferenceOrders_StartDate%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByTime" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, StartTime%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByDate" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, CompleteDate%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByTime" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, CompleteTime%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ConferenceOrders_Status%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, EditConferenceOrder_Room%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomLayout" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="TotalDeliveryCost" ItemStyle-CssClass="tableBody" HeaderText="Delivery<br>Cost (USD)" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn> <%--ZD 100237--%>
                                <asp:BoundColumn DataField="TotalServiceCharge" ItemStyle-CssClass="tableBody" HeaderText="Service<br>Charge (USD)" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn> <%--ZD 100237--%>
                                <asp:BoundColumn DataField="TotalCost" ItemStyle-CssClass="tableBody" HeaderText="Total<br>Cost (USD)" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                    <ItemTemplate><%--ZD 100875--%>
                                        <asp:LinkButton ID="btnEdit" OnClientClick="document.getElementById('hdnSaveData').value = '1';" CommandName="Edit" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_btnEdit%>"  Visible='<%# DataBinder.Eval(Container, "DataItem.Status") != "Completed" %>'></asp:LinkButton> <%-- FB 2050 --%>
                                        <asp:LinkButton ID="btnDelete" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Delete" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_btnDelete%>" ></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="SetID" ItemStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReqQuantity" ItemStyle-CssClass="tableHeader" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Notify" ItemStyle-CssClass="tableHeader" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DeliveryType" ItemStyle-CssClass="tableHeader" HeaderText="Delivery<br>Type" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Timezone" ItemStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DeliveryCost" ItemStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn> <%--ZD 100237--%>
                                <asp:BoundColumn DataField="ServiceCharge" ItemStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn><%--ZD 100237--%>
                            </Columns>
                            <%--Window Dressing Start--%>
                            <SelectedItemStyle CssClass="tableBody"  Font-Bold="True"/>
                            <EditItemStyle CssClass="tableBody"   />
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableBody"  Font-Bold="True" />
                            <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                            <%--Window Dressing End--%>
            </asp:DataGrid>
                                                </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Table runat="server" ID="AVItemsTable" Visible="False" Width="100%" CellPadding="0" CellSpacing="0">
                                                                           <asp:TableHeaderRow Height="20">
                                    <asp:TableHeaderCell CssClass="subtitleblueblodtext"><asp:Label ID="lblNewEditAV" runat="server"></asp:Label></asp:TableHeaderCell>
                                   </asp:TableHeaderRow>
                        <asp:TableRow ID="TableRow2" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:TableCell ID="TableCell1" runat="server">
                 <table border="0"  cellpadding="5" cellspacing="0" width="80%" ><%--ZD 100393--%>
                        <tr>
                            <td align="left" class="blackblodtext" width="15%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Room%>" runat="server"></asp:Literal></td>
                            <td style="font-weight: bold; " align="left"  width="25%">
                                <asp:DropDownList ID="lstRooms" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selRooms_SelectedIndexChanged" onclick="javascript:document.getElementById('hdnSaveData').value = '1';"> <%--ZD 100875--%>
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext" width="15%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SetName%>" runat="server"></asp:Literal></td>
                            <td align="left" width="25%">
                                <asp:DropDownList ID="lstAVSet" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selAVSet_SelectedIndexChanged" onclick="javascript:document.getElementById('hdnSaveData').value = '1';"> <%--ZD 100875--%>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="req2" ControlToValidate="lstAVSet" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" InitialValue="-1"></asp:RequiredFieldValidator>
                                </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Name%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:TextBox ID="txtWorkOrderName" runat="server" CssClass="altText"></asp:TextBox><asp:TextBox ID="txtWorkOrderID" runat="server" CssClass="altText" Visible="False"></asp:TextBox><asp:RequiredFieldValidator ID="ValidateWOName" runat="server" ControlToValidate="txtWorkOrderName" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regWOName" ControlToValidate="txtWorkOrderName" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Personincharge%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                &nbsp;<a href="" onclick="this.childNodes[0].click();return false;"><img id="ImageButton1" onClick="javascript:getYourOwnEmailList(0,0)"  alt ="Edit" src="image/edit.gif" style="cursor:pointer;" title="<%$ Resources:WebResources, AddressBook%>" runat="server" /></a><%--ZD 100420--%><%--FB 2798--%><asp:TextBox ID="hdnApprover1" TabIndex="-1" runat="server" BackColor="Transparent" BorderColor="White"
                                    BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqFieldApproverAV" runat="server" ControlToValidate="txtApprover1"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reqApprover1" ControlToValidate="txtApprover1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_StartbyDate%>" runat="server"></asp:Literal></td>
                            <td style="font-weight: bold; " align="left" >
                                <asp:TextBox ID="txtStartByDate" runat="server" CssClass="altText"></asp:TextBox>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="Img3" height="20px" onclick="javascript:showCalendar('<%=txtStartByDate.ClientID %>', 'Img3', 1, '<%=format%>');"-->
								<%--ZD 100420--%>
                                <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('Img3').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img id="Img3" height="20px" onClick="javascript:showCalendar('<%=txtStartByDate.ClientID %>', 'Img3', 1, '<%=format%>');return false;"
                                src="image/calendar.gif" width="20px" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" alt="Date Selector" /></a><%--ZD 100420--%> <%--FB 2798--%> <%--ZD 100419--%>
                                <!--//Code changed by Offshore for FB Issue 1073 -- end-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtStartByDate"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_StartbyTime%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                            <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="startByTime" CssClass="altSelectFormat" runat="server"  CausesValidation="True"
                                    Rows="10" style="width:auto"> <%--Edited for FF--%>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="startByTime"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ControlToValidate="startByTime"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CompletedbyDa%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:TextBox ID="txtCompletedBy" runat="server" CssClass="altText"></asp:TextBox>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="cal_trigger" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '%m/%d/%Y');"-->
								<%--ZD 100420--%>
                                <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_trigger').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img id="cal_trigger" height="20px" onClick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '<%=format%>');return false;"
                                 src="image/calendar.gif" width="20px" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" alt="Date Selector" /></a><%--ZD 100420--%> <%--FB 2798--%> <%--ZD 100419--%>
                                 <!--//Code changed by Offshore for FB Issue 1073 -- end-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCompletedBy"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CompletedbyTi%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                            <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="completedByTime" CssClass="altSelectFormat" runat="server" CausesValidation="True"
                                    Rows="10" style="width:auto"> <%--Edited for FF--%>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="reqTime" runat="server" ControlToValidate="completedByTime"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regTime" runat="server" ControlToValidate="completedByTime"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%> 
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_NotifyHost%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:CheckBox ID="chkNotify" runat="server" /></td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_TimeZone%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:DropDownList CssClass="altSelectFormat" ID="lstTimezones" OnInit="GetTimezones" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Status%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList ID="lstStatus" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Text="<%$ Resources:WebResources, Pending%>" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:WebResources, Completed%>" Value="1"></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Comments%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:TextBox ID="txtComments" runat="server" CssClass="altText" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="Submit" ID="RegularExpressionValidator15" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2011--%>
                            </td>
                        </tr>
                        <tr id="trDelivery" runat="server">
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DeliveryType%>" runat="server"></asp:Literal></td>
                            <td align="left" ><%-- FB 1830 --%>
                                <asp:DropDownList cssclass="altSelectFormat" ID="lstDeliveryType" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadDeliveryTypes" OnSelectedIndexChanged="ChangeDeliveryType" AutoPostBack="true" onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1)"></asp:DropDownList> <%--ZD 100875--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="lstDeliveryType" runat="server" ValidationGroup="Submit"  ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DeliveryCost%>" runat="server"></asp:Literal> (<%=cFormat %>)</td> <%--FB 1830--%>
                            <td align="left">
                                <asp:TextBox ID="txtDeliveryCost" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:CustomValidator ID="cusDCost" runat="server" ControlToValidate="txtDeliveryCost"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage=" <%$ Resources:WebResources, InvalidAmount%>" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                <%--<asp:RegularExpressionValidator ID="regDC" runat="server" ErrorMessage="Numerics only." SetFocusOnError="True" ToolTip="Numerics only." ControlToValidate="txtDeliveryCost" ValidationExpression="^\d+(?:\.\d{0,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <tr id="trService" runat="server">
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ServiceCharge%>" runat="server"></asp:Literal> (<%=cFormat %>)</td> <%--FB 1830--%>
                            <td align="left" >
                                <asp:TextBox ID="txtServiceCharges" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtServiceCharges"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage=" <%$ Resources:WebResources, InvalidAmount%>" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                <%--<asp:RegularExpressionValidator ID="regSC" runat="server" ErrorMessage="Numerics only." SetFocusOnError="True" ToolTip="Numerics only." ControlToValidate="txtServiceCharges" ValidationExpression="^\d+(?:\.\d{0,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="left" class="blackblodtext">&nbsp;</td>
                            <td align="left" >&nbsp;</td>
                        </tr>
                    </table>
                    </asp:TableCell>
                    </asp:TableRow>
                       <asp:TableRow ID="TableRow3" HorizontalAlign="Center" VerticalAlign="Middle" runat="server"><asp:TableCell ID="TableCell2" runat="server">
                       <table width="100%">
                      <tr>
                          <td colspan="2" style="font-weight: bold; " align="center">
                                <asp:DataGrid ID="itemsGrid" runat="server" AutoGenerateColumns="False" ShowFooter="true"
                                    CellPadding="4" Font-Bold="False" Font-Names="arial" Font-Size="Small" OnItemDataBound="SetDeliveryAttributes"
                                    GridLines="None" Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="1" style="border-collapse:separate"> <%--Edited for FF--%>
                                    <FooterStyle Font-Bold="True" ForeColor="Blue" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <Columns>
                                    <%--Window Dressing--%>
                                        <asp:BoundColumn DataField="ID" Visible="False"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, Name%>"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, SerialNo%>"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, EditInventory_tdHImage%>">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemStyle CssClass="tableBody" />
                                            <ItemTemplate>
                                               <asp:Image runat="server" ID="imgItem" AlternateText ="Work Order Item"   Height="30" Width="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" /> <%--ZD 100419--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, InventoryManagement_Comments%>"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Description" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ConferenceSetup_Description%>"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Quantity" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, EditInventory_tdHQuantity%>"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, DeliveryType1%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
<%--                                                <asp:Label ID="lblDeliveryType" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryType") %>' runat="server"></asp:Label>--%>
                                                  <asp:DropDownList AutoPostBack="true" CssClass="altText" OnSelectedIndexChanged="ChangeDeliveryTypeItem" runat="server" 
                                                  ID="lstDeliveryTypeItem" Enabled="false" DataTextField="Name" DataValueField="ID" OnInit="LoadDeliveryTypes" 
                                                   onChange="javascript:document.getElementById('hdnSaveData').value = '1';" SelectedText='<%# DataBinder.Eval(Container, "DataItem.DeliveryType") %>' ></asp:DropDownList><%-- ZD 100875--%>
                                             </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="Delivery<br>Cost (USD)" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDeliveryCost" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>' runat="server"/>
                                                <asp:DropDownList ID="lstDeliveryCost" runat="server" Visible="false" Enabled="true" DataValueField="DeliveryTypeID" DataTextField="DeliveryCost" /> <%--SelecteValue='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>'--%>
                                            </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="Service<br>Charge (USD)" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServiceCharge" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>' runat="server"/>
                                                <asp:DropDownList AutoPostBack="true" runat="server" Visible="false" ID="lstServiceCharge" Enabled="true" DataTextField="ServiceCharge" DataValueField="DeliveryTypeID"></asp:DropDownList><%--SelecteValue='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>'--%>
                                            </ItemTemplate>
                                         </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"  HeaderText="Price (USD)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPrice" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, RequestedQuantity%>"><HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReqQuantity" CssClass="altText" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="numPassword1" runat="server" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Numericsonly%>" SetFocusOnError="True" ToolTip="<%$ Resources:WebResources, Numericsonly%>" ControlToValidate="txtReqQuantity" ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReqQuantity" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="validateQuantityRange" runat="server" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, InvalidQuantity%>" ControlToValidate="txtReqQuantity" MinimumValue="0" Display="Dynamic" Type="Integer" MaximumValue='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:RangeValidator>
                                            </ItemTemplate>
                                            <FooterStyle Font-Bold="True" ForeColor="Blue" Horizontalalign="left" />
                                            <FooterTemplate>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Total%>" runat="server"></asp:Literal> (<%=cFormat %>): </span> <asp:Label runat="server" ID="lblTotalQuantity" Text="0" Width="50" OnLoad="UpdateTotalAV"></asp:Label>&nbsp;
                                                <br /><asp:Button ID="btnUpdateTotal" OnClick="UpdateTotalAV" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" runat="server" ValidationGroup="Submit" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, ConferenceSetup_btnUpdateTotal%>" /> <%--ZD 100875--%>
                                                <asp:Label runat="server" Visible="false" ID="lblTotalDlvyCost" Text="0"  OnLoad="UpdateTotalAV"></asp:Label> <%--ZD 100237--%>
                                                <asp:Label runat="server" Visible="false" ID="lblTotalSrvChrg" Text="0" OnLoad="UpdateTotalAV"></asp:Label> <%--ZD 100237--%>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="UID" Visible="false" HeaderText="UID"></asp:BoundColumn>
                                    </Columns>
                           <%--Window Dressing--%>
                                    <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                </asp:DataGrid>
                          </td>
                      </tr>
 
                      <tr>
                          <td align="right">
                             <button ID="btnCancel" type="button" runat="server" class="altMedium0BlueButtonFormat" ValidationGroup="Cancel" onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" onserverclick="btnCancel_Click"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnCancel%>" runat="server"></asp:Literal></button></td><%-- ZD 100875--%> 
                          <td align="left">
                             <button ID="btnSubmit" type="button" runat="server" class="altMedium0BlueButtonFormat" ValidationGroup="Submit" onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" onserverclick="btnSubmit_Click"><asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button></td><%-- ZD 100875--%> 
                      </tr>
                    </table>          
                            </asp:TableCell>
                            </asp:TableRow>

                            </asp:Table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="font-weight:bold"colspan="3">
                             <button ID="btnAddNewAV" style="width:350px" runat="server" type="button" onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" onserverclick="A_btnAddNew_Click"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnAddNewAV%>" runat="server"></asp:Literal></button><%-- FB 2570 --%>  <%-- FB 2664 ZD 100875--%> 
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                 </table>
                <script language="javascript">
                    if (document.getElementById("txtApprover1") != null)
                    {
                        document.getElementById("txtApprover1").Readonly = true;
//                        alert("in if");
                    }
                </script>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="SelectCatering" runat="server" OnLoad="SetInstructionsCAT">
                                    <asp:Panel ID="pnlCatering" runat="server" Width="100%">
                    <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CateringWorkO%>" runat="server"></asp:Literal></h3>
                    <input type="hidden" id="Hidden4" value="15">
                <input type="hidden" id="lblTab" value="2" />
                <input type="hidden" id="CatTab" runat="server" value="1" /><%-- ZD 100716 --%>
                    <table width="100%">
                    <tr>
                        <td width="100%" align="left">
                            <asp:Label ID="lblCATWOInstructions" CssClass="blackblodtext" runat="server" Width="90%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<table width="100%">
                                <tr>
                                    <td >
                                         <%--Window Dressing start--%>
                                        <asp:DataGrid ID="CATMainGrid" runat="server" AutoGenerateColumns="False" BorderColor="blue" BorderStyle="solid" BorderWidth="1" CellPadding="4" OnItemCreated="BindRowsDeleteMessage" GridLines="None" OnItemDataBound="SetCalendar" 
                                        OnCancelCommand="CancelChanges" OnUpdateCommand="UpdateWorkorder" OnEditCommand="EditWorkorder" OnDeleteCommand="DeleteWorkorder" Width="100%" style="border-collapse:separate"> <%--Edited for FF--%>
                                            <FooterStyle Font-Bold="True" CssClass="tableBody"/>
                                            <AlternatingItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                                            <ItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                                            <HeaderStyle CssClass="tableHeader" />
                                             <%--Window Dressing end--%>
                                             <EditItemStyle CssClass="tableBody" />
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Name" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SelectedService" Visible="false"></asp:BoundColumn>
                                             <%--Window Dressing --%>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageRoomProfile_lblTitle%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblRoomName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RoomName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label runat="server" ID="lblID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                        <asp:DropDownList ID="lstRooms" CssClass="altText" runat="server" selectedValue='<%# DataBinder.Eval(Container, "DataItem.RoomId") %>' OnInit="LoadRooms" AutoPostBack="true" onchange="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1)" OnSelectedIndexChanged="UpdateMenus" ></asp:DropDownList><%--ZD 100875--%>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                             <%--Window Dressing --%>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_ServiceType%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServiceName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="lstServices" CssClass="altText" runat="server" DataTextField="Name" DataValueField="ID" OnLoad="LoadCateringServices" OnInit="LoadCateringServices" AutoPostBack="true" onchange="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1)" OnSelectedIndexChanged="UpdateMenus" selectedValue='<%# DataBinder.Eval(Container, "DataItem.SelectedService") %>'></asp:DropDownList>  <%--OnLoad="GetServices"--%> <%--ZD 100875--%>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn ItemStyle-CssClass="tableBody">
                                                    <HeaderTemplate>
                                                        <table width="100%" cellpadding="4" cellspacing="0" border="0">
                                                            <tr>
                                                                <td width="60%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Name%>" runat="server"></asp:Literal></td>
                                                                <td class="tableHeader" width="40%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Quantity%>" runat="server"></asp:Literal></td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:DataGrid ID="dgCateringMenus" ShowHeader="false" Width="100%" CellPadding="4" CellSpacing="0" BorderWidth="1px" BorderStyle="Solid" runat="server" AutoGenerateColumns="false">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="top" />
                                                            <AlternatingItemStyle HorizontalAlign="left" VerticalAlign="Top" />
                                                            <Columns>
                                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                <asp:BoundColumn ItemStyle-CssClass="tableBody" DataField="Name" ItemStyle-Width="60%"></asp:BoundColumn>
                                                                <asp:BoundColumn ItemStyle-CssClass="tableBody" DataField="Quantity" ItemStyle-Width="40%"></asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:Label ID="lblCateringMenus" visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.strMenus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DataGrid ID="dgCateringMenus" ShowHeader="false" Width="100%" CellPadding="4" CellSpacing="0" BorderWidth="0px" BorderStyle="none" runat="server" AutoGenerateColumns="false" style="border-collapse:separate"> <%--Edited for FF--%>
                                                            <Columns>
                                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Price" Visible="false"></asp:BoundColumn>
                                                                <asp:TemplateColumn ItemStyle-Width="60%">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtPrice" runat="server" style="display:none" Text='<%# Double.Parse(DataBinder.Eval(Container, "DataItem.Price").ToString()).ToString("00.00") %>'></asp:TextBox>
                                                                        <asp:CheckBox ID="chkSelectedMenu" runat="server" />
                                                                        <a href="#"><asp:Label ID="txtMenuName" onmouseover="javascript:ShowItems(this)" onmouseout="javascript:HideItems()" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server"></asp:Label></a>
                                                                        <asp:DataGrid ID="dgMenuItems" AutoGenerateColumns="false" runat="server" style="border-collapse:separate"> <%--Edited for FF--%>
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, ItemsList%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn ItemStyle-Width="40%" >
                                                                    <ItemTemplate>
                                                                        <asp:TextBox runat="server" ID="txtQuantity" onkeyup="javascript:UpdateCheckbox(this);" Width="50px" CssClass="altText" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:Label ID="lblNoMenus" runat="server" Visible="false" Text="<%$ Resources:WebResources, ConferenceSetup_lblNoMenus%>" CssClass="lblError" ></asp:Label>
                                                        <asp:Label ID="lblCateringMenus" ItemStyle-CssClass="tableBody" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.strMenus") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-Wrap="true" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="2" MaxLength="2000" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regComments" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="Price(USD)" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"> 
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate> 
                                                        <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_DeliveryByDat%>" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeliverByDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliverByDate") + " " + DataBinder.Eval(Container, "DataItem.DeliverbyTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDeliverByDate" Width="100" runat="server" CssClass="altText" Text='<% # DataBinder.Eval(Container, "DataItem.DeliverByDate") %>'></asp:TextBox> <%-- FB 2050 --%>
														<%--ZD 100420--%>
                                                        <a href="#" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('imgCalendar').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><asp:Image ID="imgCalendar" runat="server" Height="20" Width="20" ImageUrl="image/calendar.gif" style="cursor:pointer;" title="<%$ Resources:WebResources, DateSelector%>" AlternateText="Date Selector"/></a> <%--FB 2798--%> <%--ZD 100419--%>
                                                        <mbcbb:ComboBox ID="lstDeliverByTime" runat="server" CausesValidation="True"
                                                        CssClass="altSelectFormat" Rows="10" style="width:auto" Text='<%# DataBinder.Eval(Container, "DataItem.DeliverByTime") %>'> <%--Edited for FF--%>
                                                    </mbcbb:ComboBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="lstDeliverByTime"
                                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="lstDeliverByTime"
                                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtDeliverByDate"
                                                            Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                                        <asp:RegularExpressionValidator ID="regDeliverByDate"  ControlToValidate="txtDeliverByDate" Display="Dynamic" ErrorMessage="Invalid Date (mm/dd/yyyy)" ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator> -->
                                                        <asp:RegularExpressionValidator ID="regDeliverByDate" runat="server" ControlToValidate="txtDeliverByDate" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate1%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                        <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate><%--ZD 100875--%>
                                                        <asp:LinkButton ID="btnEdit" Text="<%$ Resources:WebResources, Edit%>" runat="server" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Edit" CommandArgument="2"></asp:LinkButton>
                                                        <asp:LinkButton ID="btnDelete" Text="<%$ Resources:WebResources, Delete%>" runat="server" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Delete" CommandArgument="2"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="btnUpdate" Text="<%$ Resources:WebResources, Create%>" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" runat="server" CommandName="Update" CommandArgument="2"></asp:LinkButton><%--FB 1086 work order fixes --%>
                                                        <asp:LinkButton ID="btnCancel" Text="<%$ Resources:WebResources, Cancel%>" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" runat="server" CommandName="Cancel" CommandArgument="2"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="AssignedAdminID" Visible="false"></asp:BoundColumn><%--ZD 100757--%>
                                                <asp:BoundColumn DataField="DeliverByDate" Visible="false"></asp:BoundColumn><%--ZD 100995--%>
                                                <asp:BoundColumn DataField="DeliverByTime" Visible="false"></asp:BoundColumn><%--ZD 100995--%>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="font-weight:bold"colspan="3">
                             <button ID="btnAddNewCAT" class="altLongBlueButtonFormat" runat="server"  onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" onserverclick="C_btnAddNew_Click" style="width:405px" type="button" ><asp:Literal ID="Literal38" Text="<%$ Resources:WebResources, ConferenceSetup_btnAddNewCAT%>" runat="server"></asp:Literal></button><%-- FB 2570 --%> <%-- FB 2664 ZD 100875--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                 </table>
                                    </asp:Panel>
                    <div id="tblMenuItems" style="display:none">
    
                    </div>
                                </asp:View>
                                <asp:View ID="SelectHousekeeping" runat="server" OnLoad="SetInstructionsHK" >
                                    <asp:Panel ID="pnlSelectHousekeeping" runat="server" Width="100%">
                     <h3><asp:Literal ID="Literal39" Text="<%$ Resources:WebResources, ConferenceSetup_FacilityWorkO%>" runat="server"></asp:Literal></h3><!--FB 2570 -->
                     <input type="hidden" id="Hidden6" value="16">
                     <input type="hidden" id="lblTab" value="3" />
                     <input type="hidden" id="HKTab" runat="server" value="1" /><%-- ZD 100716 --%>
                     <table width="100%"><!--FB 1086 -->
                    <tr>
                        <td width="100%" align="left" style="height: 18px">
                            <asp:Label ID="lblHKWOInstructions" runat="server" Width="90%" class="blackblodtext"></asp:Label>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<table width="100%">
                                <tr>
                                    <td colspan="3">
                                    
                            <asp:DataGrid ID="HKMainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                                OnItemCreated="BindRowsDeleteMessage" OnCancelCommand="HKMainGrid_Cancel" OnUpdateCommand="HKMainGrid_Update" OnEditCommand="HKMainGrid_Edit" OnDeleteCommand="HKMainGrid_DeleteCommand" Width="100%" AllowSorting="True" style="border-collapse:separate"> <%--Edited for FF--%>
                            <FooterStyle CssClass="tableBody" />
                            <AlternatingItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                            <ItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToName" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, AssignedTo%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByDate" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, ConferenceOrders_StartDate%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByTime" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, StartTime%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByDate" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, CompleteDate%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByTime" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, CompleteTime%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, Status%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, EditConferenceOrder_Room%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="TotalCost" ItemStyle-CssClass="tableBody"  HeaderText="Total<br>Cost (USD)" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Edit" runat="server" Text="<%$ Resources:WebResources, Edit%>" Visible='<%# DataBinder.Eval(Container, "DataItem.Status") != "Completed" %>' ></asp:LinkButton> <%-- FB 2050 ZD 100875--%>
                                        <asp:LinkButton ID="btnDelete" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" CommandName="Delete" runat="server" Text="<%$ Resources:WebResources, Delete%>" ></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="SetID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReqQuantity" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Notify" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomLayout" Visible="False"></asp:BoundColumn> <%--FB 2079--%>
                            </Columns>
                                <SelectedItemStyle Font-Bold="True" CssClass="tableBody"/>
                                <EditItemStyle CssClass="tableBody"  />
                                <AlternatingItemStyle CssClass="tableBody" />
                                <ItemStyle CssClass="tableBody"/>
                                <HeaderStyle CssClass="tableHeader" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                   <asp:Table runat="server" ID="HKItemsTable" Visible="False" Width="100%" CellPadding="0" CellSpacing="0">
                                   <asp:TableHeaderRow Height="30">
                                    <asp:TableHeaderCell Height="30" CssClass="subtitleblueblodtext"><asp:Label ID="lblNewEditHK" runat="server"></asp:Label></asp:TableHeaderCell>
                                   </asp:TableHeaderRow>
               <asp:TableRow ID="TableRow7" runat="server" HorizontalAlign="center"><asp:TableCell ID="TableCell7" runat="server">
                        <table border="0" cellpadding="5" cellspacing="0" width="70%">
                        <tr>
                            <td align="left" width="15%" class="blackblodtext"><asp:Literal ID="Literal40" Text="<%$ Resources:WebResources, ConferenceSetup_Room%>" runat="server"></asp:Literal></td>
                            <td align="left" style="width:25%;">
                                <asp:DropDownList ID="lstHKRooms" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selHKRooms_SelectedIndexChanged" onclick="javascript:document.getElementById('hdnSaveData').value = '1';"><%--ZD 100875--%>
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext" width="15%"><asp:Literal ID="Literal41" Text="<%$ Resources:WebResources, ConferenceSetup_SetName%>" runat="server"></asp:Literal></td>
                            <td align="left"  width="25%">
                                <asp:DropDownList ID="lstHKSet" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selHKSet_SelectedIndexChanged" onclick="javascript:document.getElementById('hdnSaveData').value = '1';"><%--ZD 100875--%>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="req2HK" ControlToValidate="lstHKSet" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" InitialValue="Select one..." ValidationGroup="Submit" Display="dynamic"></asp:RequiredFieldValidator><%--FB 1951--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal ID="Literal42" Text="<%$ Resources:WebResources, Name%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:TextBox ID="txtHKWorkOrderName" runat="server" CssClass="altText"></asp:TextBox><asp:TextBox ID="txtHKWorkOrderID" runat="server" CssClass="altText" Visible="False"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtHKWorkOrderName" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regHKWOName" ControlToValidate="txtHKWorkOrderName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal ID="Literal43" Text="<%$ Resources:WebResources, ConferenceSetup_Personincharge%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:TextBox ID="txtApprover3" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                &nbsp;<a href="" onclick="this.childNodes[0].click();return false;"><img id="Img2" onclick="javascript:getYourOwnEmailList(2,0)"  alt ="Edit" src="image/edit.gif" style="cursor:pointer;" title="<%$ Resources:WebResources, myVRMAddressBook%>" runat="server" /></a><%--ZD 100420--%><%--FB 2798--%><asp:TextBox tabindex="-1" ID="hdnApprover3" runat="server" BackColor="Transparent" BorderColor="White"
                                    BorderStyle="None" Width="0px" ForeColor="Transparent"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtApprover3"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reqApprover3" ControlToValidate="txtApprover3" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--ZD 100263--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal ID="Literal44" Text="<%$ Resources:WebResources, ConferenceSetup_StartbyDate%>" runat="server"></asp:Literal></td>
                            <td align="left" nowrap="nowrap">
                                <asp:TextBox ID="txtHKStartByDate" runat="server" CssClass="altText"></asp:TextBox>
                                <%--code changed for FB 1090--%>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="Img6" height="20px" onclick="javascript:showCalendar('<%=txtHKStartByDate.ClientID %>', 'Img4', 1, '%m/%d/%Y');" change Img4 to Img6-->
								<%--ZD 100420--%>
                                <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('Img6').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img id="Img6" alt="Date Selector" height="20px" onclick="javascript:showCalendar('<%=txtHKStartByDate.ClientID %>', 'Img6', 1, '<%=format%>');return false;" src="image/calendar.gif" width="20px" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" /></a><%--ZD 100420--%> <%--FB 2798--%> <%--ZD 100419--%>
                                <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtHKStartByDate"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_StartbyTime%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                 <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="startByTimeHK" CssClass="altSelectFormat" runat="server" CausesValidation="True"
                                   Rows="10" style="width:auto"><%--edited for FF--%>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="startByTimeHK"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="startByTimeHK"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%> <%--FB 1715--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CompletedbyDa%>" runat="server"></asp:Literal></td>
                            <td style="font-weight: bold; " align="left" >
                                <asp:TextBox ID="txtHKCompletedBy" runat="server" CssClass="altText"></asp:TextBox>
                                <%--code changed for FB 1090--%>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="Img7" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '%m/%d/%Y');"
                                    src="image/calendar.gif" width="20px" />-->
                                 <%--Window Dressing--%>
								<%--ZD 100420--%>
                                <a href="" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('Img7').click();return false;}}" onclick="if(isIE){this.childNodes[0].click();return false;}"><img id="Img7" height="20px" alt="Date Selector" onclick="javascript:showCalendar('<%=txtHKCompletedBy.ClientID %>', 'Img7', 1, '<%=format%>');return false;" src="image/calendar.gif" width="20px"  style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" /></a><%--ZD 100420--%> <%--FB 2798--%><%--ZD 100419--%>
                                    <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtHKCompletedBy"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CompletedbyTi%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                 <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="completedByTimeHK" CssClass="altSelectFormat" runat="server" CausesValidation="True"
                                    Rows="10" style="width:auto"><%--edited for FF--%>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="completedByTimeHK"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" ControlToValidate="completedByTimeHK"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%><%--FB 1715--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_NotifyHost%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:CheckBox ID="chkHKNotify" runat="server" /></td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_TimeZone%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:DropDownList ID="lstHKTimezone" OnInit="GetTimezones" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Status%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:DropDownList ID="lstHKStatus" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Text="<%$ Resources:WebResources, Pending%>" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:WebResources, Completed%>" Value="1"></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Comments%>" runat="server"></asp:Literal></td>
                            <td align="left" >
                                <asp:TextBox ID="txtHKComments" runat="server" CssClass="altText" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="Submit" ID="RegularExpressionValidator19" ControlToValidate="txtHKComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2011--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RoomLayout%>" runat="server"></asp:Literal></td>
                                <%--Code added for FB 1176--%>
                                 <td align="left" colspan="2"> 
                                   <asp:DropDownList ID="lstRoomLayout" runat="server" CssClass="altSelectFormat"></asp:DropDownList>                                
                                   <button ID="btnViewRoomLayout" class="altMedium0BlueButtonFormat" OnClick="javascript:viewLayout();return false;"><asp:Literal ID="Literal45" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></button>
                                </td>
                            <td align="left">&nbsp;</td>
                           </tr>
                    </table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow8" runat="server">
                        <asp:TableCell ID="TableCell28" ColumnSpan="6" runat="server" HorizontalAlign="Center" ><br />
                             <asp:DataGrid runat="server" ID="itemsGridHK" AllowSorting="True" CellPadding="4" Font-Bold="False" Font-Names="arial" Font-Size="Small"
                              ForeColor="#333333" GridLines="None" Width="90%" AutoGenerateColumns="False" ShowFooter="true" OnItemDataBound="SetDeliveryAttributes" style="border-collapse:separate">  <%--WO Bug Fix--%>
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <FooterStyle CssClass="tableBody" Horizontalalign="left" />
                                 <Columns>
                                     <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, TaskItem%>" ItemStyle-CssClass="tableBody">
                                        <HeaderStyle CssClass="tableHeader"/>
                                     </asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" HeaderText="<%$ Resources:WebResources, SerialNo%>" ItemStyle-CssClass="tableBody">
                                        <HeaderStyle CssClass="tableHeader" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, EditInventory_tdHImage%>">
                                             <HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate >  <%--WO Bug Fix - Removed Image URL--%>
                                               <asp:Image Height="30" Width="30" runat="server" ID="imgItem"  AlternateText ="Work Order Item"  onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" /> <%--ZD 100419--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" HeaderText="<%$ Resources:WebResources, InventoryManagement_Comments%>">
                                        <%--Window Dressing--%>
                                        <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                     <asp:BoundColumn DataField="Price" HeaderText="Price (USD)" ItemStyle-CssClass="tableBody">
                                        <HeaderStyle CssClass="tableHeader"  />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, RequestedQuantity%>" ItemStyle-CssClass="tableBody">
                                            <HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReqQuantity" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regHKQuantity" runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PositiveNumericVal%>" SetFocusOnError="True" ControlToValidate="txtReqQuantity" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                            <FooterTemplate> <%--FB 1686--%> <%--FB 1830--%>
                                                <span class="blackblodtext" > <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Total%>" runat="server"></asp:Literal> (<%=cFormat %>):</span><asp:Label runat="server" ID="lblTotalQuantity" Text="0" OnLoad="UpdateTotalHK"></asp:Label>&nbsp;<%--Fb 1086--%><%--FB 1830--%><asp:Button ID="btnUpdateTotal" OnClick="UpdateTotalHK" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, ConferenceSetup_btnUpdateTotal%>" /><%--ZD 100875--%>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>                                       
                            <asp:BoundColumn DataField="UID" Visible="false" HeaderText="UID"></asp:BoundColumn>
                                  </Columns>
                                </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow9" runat="server"><%--ZD 100875--%>
                        <asp:TableCell ID="TableCell29" ColumnSpan="6" runat="server" HorizontalAlign="center"> 
                            <button runat="server" ID="btnHKCancel" onserverclick="btnHKCancel_Click" class="altMedium0BlueButtonFormat" onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" ValidationGroup="HKCancel"  type="button"><asp:Literal Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>&nbsp;
                            <button runat="server" ID="btnHKSubmit" onserverclick="btnHKSubmit_Click" class="altMedium0BlueButtonFormat" onclick="javascript:document.getElementById('hdnSaveData').value = '1';" ValidationGroup="Submit" type="button"><asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button><%--FB 1951--%>
                        </asp:TableCell>
                         </asp:TableRow>
                            </asp:Table>
                                    </td>
                                </tr>
                                <tr><%--ZD 100875--%>
                                    <td align="right" style="font-weight:bold"colspan="3">
                             <button ID="btnAddNewHK" runat="server" onclick="javascript:document.getElementById('hdnSaveData').value = '1';DataLoading(1);" onserverclick="H_btnAddNew_Click" style="width:325px" type="button"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnAddNewHK%>" runat="server"></asp:Literal></button><%-- FB 2570 --%> <%--FB 2664 ZD 100875--%>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                 </table>

                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="AdditionalComments" runat="server">
                                    <asp:Panel ID="pnlAdditionalComments" runat="server" Width="100%">
                        <input type="hidden" id="Hidden8" value="11" />
                        <input type="hidden" id="isAdditionalComments" value="1" />
                        <table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">
                                   <h3><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AdditionalOpti%>" runat="server"></asp:Literal></h3></td>
                            </tr>
                            <tr><td><br /></td></tr> <%--Custom Attribute Fixes--%>
                            <tr>
                                <td align="center">
                                    <asp:Table runat="server" ID="tblCustomAttribute" Width="80%" CellPadding="3" cellspacing="2" Visible="true">
                                    
                                    </asp:Table>
                                </td>
                             </tr>
                             <tr><%--FB 2341--%>
                                    <td align="center">
                                        <table width="100%" border ="0">
                                            <tr>
                                                <td align="left" id="trHdConcSupport" runat="server" width="28%">
                                                    <table width="100%" border="0">
                                                        <tr id="trConciergeSupport" runat="server" ><%--FB 2670--%>
                                                            <td align="left" class="subtitlexxsblueblodtext" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ConferenceSupp%>" runat="server"></asp:Literal></td> <%--FB 3023--%>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" id="tdTxtMsg" runat="server" width="42%"> <%--FB 2486--%>
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="subtitlexxsblueblodtext"  width="50%"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_ActiveMessage%>" runat="server"></asp:Literal></td><%--FB 2934--%>
                                                            <td align ="left" valign="bottom" class="blackblodtext"  width="20%"> <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_BeforeEnd%>" runat="server"></asp:Literal></td> <%--ZD 100425--%>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left"  style="width:30%">
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="subtitlexxsblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_FileUploads%>" runat="server"></asp:Literal></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--FB 2632 Starts--%>
                                                <td align="left" valign="top" id="tdConcSupport" runat="server" >
                                                  <table id="tblConciergeNew" cellspacing="2" cellpadding="3" border="0" style="width:80%;">
                                                        <tr id="trOnSiteAVSupport" runat="server">
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                                <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                               <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_OnSiteAVSuppo%>" runat="server"></asp:Literal></span>
                                                            </td>
                                                        </tr>
                                                        <tr id="trMeetandGreet" runat="server">
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                                <input id="chkMeetandGreet" type="checkbox" runat="server" />
                                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MeetandGreet%>" runat="server"></asp:Literal></span>
                                                            </td>
                                                        </tr>
                                                        <tr id="trConciergeMonitoring" runat="server" >
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                                <input id="chkConciergeMonitoring" type="checkbox" runat="server" />
                                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CallMonitoring%>" runat="server"></asp:Literal></span> <%--FB 3023--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="trDedicatedVNOCOperator" runat="server">
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                            <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server"  />
                                                            <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DedicatedVNOC%>" runat="server"></asp:Literal></span><br /><br />
                                                            <asp:TextBox ID="txtVNOCOperator" TextMode="MultiLine" runat="server" CssClass="altText"></asp:TextBox>
                                                             &nbsp;<a href="" onclick="javascript:this.childNodes[0].click();return false;"><img id="img9" onClick="javascript:getVNOCEmailList()" alt="Edit" src="image/VNOCedit.gif" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, SelectVNOCOperator%>" /></a><%--ZD 100420--%><%--FB 2670--%><%--FB 2783--%><%--ZD 100419--%><%--ZD 100875--%><a href="javascript: deleteVNOC();" onclick="javascript:document.getElementById('hdnSaveData').value = '1';" onMouseOver="window.status='';return true;"><img border="0" id="img11" src="image/btn_delete.gif" title="<%$ Resources:WebResources, Delete%>" alt="Delete" width="16" height="16" runat="server" style="cursor:pointer;"></a> <%--FB 2792 FB 2798--%>
                                                            <asp:TextBox ID="hdnVNOCOperator" runat="server" BackColor="Transparent" BorderColor="White"
                                                                BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                </table>
                                                </td>
                                                 <%--FB 2632 End--%>
                                                 <%--FB 2486 Starts--%>
                                                <td align="left" id="tdTxtMsgDetails" runat="server" valign="top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellpadding="3" cellspacing="0" width="100%" border = "0">
                                                                    <tr  id="tr1" runat="server">
                                                                        <td align="left"  width="5%" >
                                                                        <asp:CheckBox ID="chkmsg1" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration1');"  />
                                                                        </td>
                                                                        <td align="left" width="68%">
                                                                           <asp:DropDownList ID="drpdownconfmsg1" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%" >
                                                                           </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left" width="12%">
                                                                        <asp:DropDownList ID="drpdownmsgduration1" CssClass="altText" runat="server" Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                         <td align ="left" width="10%"></td>
                                                                     </tr>
                                                                    <tr  id="tr2" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg2" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration2');" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="drpdownconfmsg2" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration2" CssClass="altText" runat="server"   Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                    <tr  id="tr3" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg3" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration3');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg3" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration3" CssClass="altText" runat="server"   Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td><a id="displayText" href="javascript:toggle();" onclick="javascript:document.getElementById('hdnSaveData').value = '1';" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_displayText%>" runat="server"></asp:Literal></a></td> <%--FB 2506 ZD 100875--%>
                                                                     </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table id="toggleText" cellpadding="3" cellspacing="0" runat="server" width="100%" border="0"> <%--FB 2506--%>
                                                                     <tr  id="tr4" runat="server"> 
                                                                        <td align="left"  width="5%" >
                                                                        <asp:CheckBox ID="chkmsg4" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration4');"  />
                                                                        </td>
                                                                        <td align="left"  width="68%">
                                                                           <asp:DropDownList ID="drpdownconfmsg4" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                           </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left"  width="12%">
                                                                        <asp:DropDownList ID="drpdownmsgduration4" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>                                                            
                                                                        </td>
                                                                         <td align ="left" width="10%"></td>
                                                                     </tr>
                                                                     <tr  id="tr5" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg5" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration5');"  />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg5" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%">
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration5" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr6" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg6" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration6');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg6" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration6" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr7" runat="server"> 
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg7" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration7');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg7" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%">
                                                                           </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration7" CssClass="altText" runat="server" Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr8" runat="server"> 
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg8" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration8');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg8" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%">
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration8" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr9" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg9" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration9');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg9" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration9" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                </table>
                                                            </td>
                                                        </tr>   
                                                    </table>
                                              </td>
                                              <%--FB 2486 Ends--%>
                                                <td align="left" valign="top" >
                                                <table cellpadding="3" cellspacing="0" width="100%" border = "0" style="margin-left:5%">
                                                    <%-- Custom Attribute Fixes start --%>
                                                        <tr id="trFile1" runat="server" > <%-- Code Modified For MOJ Phase2 --%>                                
                                                            <td align="left" class="blackblodtext" valign="top" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_File1%>" runat="server"></asp:Literal></td> <%--FB 2909 --%> <%--ZD 102328--%>
                                                            <td align="left">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr><%--FB 2909 Start--%>
                                                                        <td width="100%"  valign="top"><%--ZD 102277--%> <%--ZD 102328--%>
                                                                            <div>
                                                                            <input type="text" class="file_input_textbox" id="txtFileUpload1"  readonly="readonly" runat="server" value='<%$ Resources:WebResources, Nofileselected%>' style="width: 125px; height:18px;"/> <%--ZD 102330--%>
                                                                              <div class="file_input_div"><button id="Browse1" class="file_input_button" onclick="document.getElementById('FileUpload1').click();return false;" ><asp:Literal ID="Literal46" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                                <input type="file"  class="file_input_hidden" tabindex="-1" id="FileUpload1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/></div></div>
                                                                                &nbsp;&nbsp;
                                                                                <div>
                                                                                <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="FileUpload1" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                                    SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                                </div>
                                                                            <%--<input type="file" id="FileUpload1" contenteditable="false" enableviewstate="true" size="40" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                            <br /><%--ZD 103762--%>
                                                                            <asp:Label ID="lblUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td width="30%" align="left" valign="top"> <%--ZD 100875--%> <%--ZD 102328--%>
                                                                            <asp:Button ID="btnRemove1" CssClass="altShortBlueButtonFormat" style="width:78px;" Text="<%$ Resources:WebResources, ConferenceSetup_btnRemove1%>" Visible="false" runat="server" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" OnCommand="RemoveFile" CommandArgument="1" />
                                                                            <asp:Label ID="hdnUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                         </tr>
                                                        <tr  id="trFile2" runat="server"> <%-- Code Modified For MOJ Phase2 --%>                             
                                                            <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_File2%>" runat="server"></asp:Literal></td> <%--FB 2909 --%> <%--ZD 102328--%>
                                                            <td align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="100%" valign="top"><%--FB 2909 Start--%><%--ZD 102277--%><%--ZD 102328--%>
                                                                            <div>
                                                                                <input  type="text" id="txtFileUpload2"  class="file_input_textbox" readonly="readonly" runat="server" value='<%$ Resources:WebResources, Nofileselected%>' style="width: 125px;height:18px;" /> <%--ZD 102330--%>
                                                                                <div class="file_input_div"><button id="Browse2" class="file_input_button" onclick="document.getElementById('FileUpload2').click();return false;" ><asp:Literal ID="Literal47" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                                   <input type="file"  class="file_input_hidden" tabindex="-1" id="FileUpload2" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,2);"/></div></div>
                                                                                   &nbsp;&nbsp;
                                                                                   <div>
                                                                                   <asp:RegularExpressionValidator ID="regFNVal2" ControlToValidate="FileUpload2" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                                    SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                                    </div>
                                                                                <%--<input type="file" id="FileUpload2" contenteditable="false" enableviewstate="true" size="40" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                                <br /><%--ZD 103762--%>
                                                                                <asp:Label ID="lblUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td width="30%" valign="top"> <%--ZD 102328--%>
                                                                                <asp:Button ID="btnRemove2" CssClass="altShortBlueButtonFormat" style="width:78px;" Text="<%$ Resources:WebResources, ConferenceSetup_btnRemove2%>" Visible="false" runat="server"  OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';" OnCommand="RemoveFile" CommandArgument="2" /> <%--ZD 100875--%>
                                                                                <asp:Label ID="hdnUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                            </td>
                                                         </tr>
                                                        <tr  id="trFile3" runat="server"> <%-- Code Modified For MOJ Phase2 --%>                             
                                                             
                                                            <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_File3%>" runat="server"></asp:Literal></td> <%--FB 2909 --%> <%--ZD 102328--%>
                                                            <td align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="100%" valign="top"><%--FB 2909 Start--%><%--ZD 102277--%> <%--ZD 102328--%>
                                                                                <div>
                                                                                <input type="text" class="file_input_textbox"  id="txtFileUpload3" readonly="readonly" runat="server" value='<%$ Resources:WebResources, Nofileselected%>' style="width: 125px;height:18px;" />
                                                                                    <div class="file_input_div"><button id="Browse3" class="file_input_button" onclick="document.getElementById('FileUpload3').click();return false;" ><asp:Literal ID="Literal48" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                                    <input type="file"  class="file_input_hidden" tabindex="-1" id="FileUpload3" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,3);"/></div></div><%--FB 2909 End--%>
                                                                                    &nbsp;&nbsp;
                                                                                    <div>
                                                                                    <asp:RegularExpressionValidator ID="regFNVal3" ControlToValidate="FileUpload3" Display="dynamic" runat="server" ValidationGroup="Submit1" 
                                                                                    SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                                    </div>
                                                                                <%--<input type="file" id="FileUpload3" contenteditable="false" enableviewstate="true" size="40" class="altText" runat="server" />--%>
                                                                                <br /><%--ZD 103762--%>
                                                                                <asp:Label ID="lblUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td width="30%"  valign="top"> <%--ZD 102328--%>
                                                                                <asp:Button ID="btnRemove3" CssClass="altShortBlueButtonFormat" style="width:78px;" Text="<%$ Resources:WebResources, ConferenceSetup_btnRemove3%>" Visible="false" OnClientClick="javascript:document.getElementById('hdnSaveData').value = '1';"
                                                                                runat="server" OnCommand="RemoveFile" CommandArgument="3" /> <%--ZD 100875--%>
                                                                                <asp:Label ID="hdnUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                            </td>
                                                          </tr>
                                                        <%--  ZD 102275 start--%>
                                                        <tr style="height:10px"></tr>
                                                        <tr style="height:10px">
                                                         <td></td>
                                                          <td colspan ="2" align="left">
                                                            <span ><asp:Literal Text="<%$ Resources:WebResources, ExpressConference_Filesizeexceed%>" runat="server"></asp:Literal></span>
                                                          </td>
                                                        </tr><%--  ZD 102275 End--%>
                                                        <tr style="height:10px"></tr>
                                                        <tr style="height:10px">
                                                            <td colspan="2" align="center"> <%--FB 3055-Filter in Upload Files--%>
                                                                <%--ZD 100420 START--%>
                                                                <%--<asp:Button ID="btnUploadFiles" OnClick="UploadFiles" runat="server" Text="Upload Files" CssClass="altLongBlueButtonFormat" />--%>
                                                                <button ID="btnUploadFiles" onserverclick="UploadFiles" validationgroup="Submit1" onclick="javascript:document.getElementById('hdnSaveData').value = '1';return fnUpload3(document.getElementById('FileUpload1'), document.getElementById('FileUpload2'), document.getElementById('FileUpload3'), this.id, 'Submit1');" runat="server" class="altLongBlueButtonFormat" type="button"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_btnUploadFiles%>" runat="server"></asp:Literal></button> <%--ZD 100875--%><%--ZD 102277--%> <%--ZD 102364--%>
                                                                 <%--ZD 100420 End--%>
                                                            </td>
                                                          </tr>
                                                    <%--Custom Attribute Fixes End--%>
                                                </table>
                                              </td>   
                                        </tr>
                                        </table>
                                    </td>
                                    
                                </tr>
                             
                             <tr>
                                <td align="left">
                                  <%--code added for Add FB 1470 start--%>
                                <%if (!(client.ToString().ToUpper() == "MOJ"))
                                  {%> <%--Edited For FB 1425--%>
                                    
                                
                                    <asp:Table runat="server" ID="tblEntityCode" Width="100%" CellPadding="0" cellspacing="2" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell Width="8%"></asp:TableCell>
                                        <asp:TableCell Width="7%" HorizontalAlign="left" CssClass="blackblodtext">
                                            <asp:Literal ID="Literal49" Text="<%$ Resources:WebResources, BilingCode%>" runat="server"></asp:Literal>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                           <%-- <asp:TextBox ID="txtCANGC1" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtCANGC1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                                            &nbsp;
                                            <asp:DropDownList ID="lstEntityCode" CssClass="altLong0SelectFormat" DataTextField="Name" DataValueField="ID" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="20%" Horizontalalign="left" ID="tblEntityDesc" runat="server" Visible="false" Font-Bold>
                                                <asp:Literal ID="Literal50" Text="<%$ Resources:WebResources, BilingDescription%>" runat="server"></asp:Literal>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                               <asp:Label ID="lblEntityDesc" runat="server" Visible="false" ForeColor="Blue"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <%}%><%--Fb 1425 --%>
                                  <%--code added for Add FB 1470 end--%>
                                
                                <asp:Table runat="server" ID="tblBillingOptions" width="100%" CellSpacing="2" CellPadding="0" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <table>
                                                <tr>
                                                    <td>
                                    <asp:Label ID="Label1" runat="server" CssClass="subtitleblueblodtext" Text="<%$ Resources:WebResources, ConferenceSetup_Label1%>"></asp:Label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left">
                                    <table border="0" cellspacing="5" cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" width="20%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PersonNameto%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA1" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtCA1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RequestorEmail%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA2" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator21" ControlToValidate="txtCA2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters36%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:#$%&()']*$"></asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtCA2" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_DepartmentName%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA3" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtCA3" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RequestorCampu%>" runat="server"></asp:Literal></td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA4" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtCA4" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" class="blackblodtext"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RequestorPhone%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:TextBox ID="txtCA5" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txtCA5" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_FISBillingNum%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:TextBox ID="txtCA6" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtCA6" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PaybyCheck%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:CheckBox ID="chkCA1" runat="server" />
                                            </td>
                                            <td align="left" class="blackblodtext" valign="top" width="20%" rowspan="2"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_BillanExterna%>" runat="server"></asp:Literal><br />
                                                Location/Sponsor</td>
                                            <td rowspan="2">
                                                <asp:TextBox ID="txtCA7" runat="server" CssClass="altText" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtCA7" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_NumberofParti%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:TextBox ID="txtCA10" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtCA10" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="reg10" runat="server" ControlToValidate="txtCA10" Display=Dynamic ValidationExpression="\d+" ErrorMessage="<%$ Resources:WebResources, Numericsonly%>"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_OnsiteRepresen%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:TextBox ID="txtCA8" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txtCA8" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_OnsiteRepresen%>" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:TextBox ID="txtCA9" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txtCA9" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr> 
                                    </table>
                                    </td></tr></table>
                                    </asp:TableCell>
                                        
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table runat="server" ID="tblLHRICCustomAttributes" Width="100%" Visible="false">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <table width="100%" cellspacing="5" cellpadding="2">
                                                    <tr>
                                                        <td colspan="4" align="left">
                                                            <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CustomAttribut%>" runat="server"></asp:Literal></SPAN>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" width="25%"><asp:Label ID="lblCA12" runat="server" class="blackblodtext"></asp:Label></td> <%--Student Class Conference--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA12" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="left"><asp:Label ID="lblCA13" runat="server" class="blackblodtext"></asp:Label></td> <%--Grade Level--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA13" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><asp:Label ID="lblCA17" runat="server" class="blackblodtext"></asp:Label></td><%--Professional Development--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA17" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="left"><asp:Label ID="lblCA14" runat="server" class="blackblodtext"></asp:Label></td><%--Content Area --%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA14" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><asp:Label ID="lblCA18" runat="server" class="blackblodtext"></asp:Label></td><%--Guidance/College--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA18" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="left"><asp:Label ID="lblCA15" runat="server" class="blackblodtext"></asp:Label></td><%--Teacher Last Name--%>
                                                        <td align="left"><asp:TextBox ID="ctrlCA15" runat="server" CssClass="altText"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><asp:Label ID="lblCA19" runat="server" class="blackblodtext"></asp:Label></td><%--Administrative--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA19" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="left"><asp:Label ID="lblCA16" runat="server" class="blackblodtext"></asp:Label></td><%--2nd Teacher Last Name--%>
                                                        <td align="left"><asp:TextBox ID="ctrlCA16" runat="server" CssClass="altText"></asp:TextBox></td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                     </asp:Table>
                                </td>
                            </tr>
                          </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="Preview" runat="server">
                                    <asp:Panel ID="pnlPreview" runat="server">
                <input type="hidden" id="Hidden9" value="13">

                <table width="90%" border="0" bordercolor="blue" cellspacing="2" cellpadding="2">
                    <tr>
                        <td colspan="3" style="height: 20px; text-align: center">
                            <h3> <%-- Code Added for FB 1428--%>
                            <span id="Field5" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Field5%>" runat="server"></asp:Literal></span>
                            </h3>
                        </td>
                    </tr>
                    <tr id="lblConfName" runat="server"> <%--FB 2694--%>
                        <td style="width:10%;"></td>
                        <td align="left" class="blackblodtext" style="width:22%;" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Name%>" runat="server"></asp:Literal></td><%--FB 2508--%>
                        <td valign="top">
                            <asp:Label ID="plblConfName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--Code added for FB : 1116 -- Start--%>                    
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_HostNameEmai%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblHostName" runat="server"></asp:Label>
                        </td>                        
                    </tr>
 					<%--FB 2501 starts--%>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RequestorName%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblRequestorName" runat="server"></asp:Label>
                        </td>   
                    </tr>
                    <%--FB 2501 ends--%> 
                    <%--Code added for FB : 1116 - End --%>
                    <%--FB 2501 Starts--%>
					<tr id ="trStartMode1" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_StartMode%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblStartMode" runat="server"></asp:Label>
                        </td>                        
                    </tr>
                    <%--FB 2501 Ends--%>
                    <tr id="trPwd" runat="server"> <%--Code Modified For MOJ Phase2 --%>                    
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal ID="plitNumericOrGuestPwd" runat="server"></asp:Literal></td><%--ALLDEV-826--%>
                        <td valign="top">
                            <asp:Label ID="plblPassword" runat="server"></asp:Label>
                        </td>
                        
                    </tr>

                    <%--ALLDEV-826 Starts--%>
                    <tr id="trHostPw" runat="server">                 
                        <td></td>
                        <td align="left" class="blackblodtext">
                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_HostPassword%>" runat="server"></asp:Literal>
                        </td>
                        <td valign="top">
                            <asp:Label ID="plblHostPassword" runat="server"></asp:Label>
                        </td>                        
                    </tr>
                    <%--ALLDEV-826 Ends--%>

                    <tr id="lblConfDesc" runat="server"> <%--FB 2694--%>
                        <td></td>
                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Description%>" runat="server"></asp:Literal></td><%--FB 2508--%>
                        <td valign="top">
                            <asp:Label ID="plblConfDescription" Width="500" runat="server"></asp:Label>
                        </td>
                    </tr>
                     <tr id="trType" runat="server"> <%-- Code Modified For MOJ Phase2 --%>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Type%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblConfType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--FB 2620--%>
                      <tr id="trVMRcall" runat="server"> <%--FB 2694--%>
                        <td></td>
                         <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_VMRCall%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblConfVMR" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--FB 2595 Starts--%>
                  	<tr id ="trPreviewNetworkState" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_NetworkState%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="lblNetworkState" runat="server"></asp:Label>
                        </td>                        
                    </tr><%--FB 2998--%>
                    <tr id ="trMCUConnect" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MCUConnectMin%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="lblPMCUConnect" runat="server"></asp:Label>
                        </td>                        
                    </tr>
                    <tr id ="trMCUDisconnect" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_MCUDisconnect%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="lblPMCUDisconnect" runat="server"></asp:Label>
                        </td>                        
                    </tr>
                    <%--Code added for buffer zone -- Start FB 2634 --%>
                    <%if (enableBufferZone == "1" && lstConferenceType.SelectedValue != "8")
                      { %>
                    <tr id="trSetupTime" runat="server" > <%--ZD 101755--%>
                        <td></td>
                        <td align="left" class="blackblodtext">
                          <asp:Label runat="server" ID="lblSetupDateTime"></asp:Label>
                        </td>
                        <td valign="top">
                            <asp:Label ID="plblSetupDTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td></td>
                        <td class="blackblodtext" align="left" id="PSndDate" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PSndDate%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblConfStartDateTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext" id="PEndDate" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_PEndDate%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblConfEndDateTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                   <%if (enableBufferZone == "1" && lstConferenceType.SelectedValue != "8")
                      { %>
                    <tr id="trTeardownTime" runat="server"> <%--ZD 101755--%>
                        <td></td>
                        <td align="left" class="blackblodtext">
                          <asp:Label runat="server" ID="lblTeardownDateTime"></asp:Label>
                        </td>
                        <td valign="top">
                            <asp:Label ID="plblTeardownDTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%} %>
                    <tr id="trRec" runat="server"> <%--Edited for FB 1425 MOJ--%>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_RecurrencePatt%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label id="plblConfRecurrance" runat="server" text="<%$ Resources:WebResources, ConferenceSetup_plblConfRecurrance%>"></asp:Label>
                        </td>
                    </tr>
                    <%if (!(client.ToString().ToUpper() == "MOJ"))
                      {%> <%--Edited For FB 1425--%>
                    <tr id="trparticipant" runat="server"> <%--FB 2694--%>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Participants%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                        <%--FB 2234 Start--%>
                        <div id="Div3" style="width:500px;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server">
                            <asp:Label ID="plblPartys" runat="server"></asp:Label>
                            </div>
                             <%--FB 2234 End--%>
                        </td>
                    </tr>
                    <%} %><%--Edited For FB 1425--%>
                    <tr id="traudiobrdige" runat="server"><%--FB 2359--%><%--FB 2443--%>
                        <td></td>
                        <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AudioBridges%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                        <div id="Div4" style="width:500px;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server">
                            <asp:Label ID="lblAudioBridge" runat="server"></asp:Label>
                            </div>
                        </td>
                    </tr>
                     <%--FB 2870 Start--%>
                    <tr id="trCTSNumericID" runat="server">
                    <td></td>
                    <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CTSNumericID%>" runat="server"></asp:Literal></td>  
                    <td valign="top">
                    <asp:Label ID="lblCTSNumericID" runat="server"></asp:Label>
                    </td>
                    </tr>
                    <%--FB 2870 End--%>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Location%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblLocation" runat="server"></asp:Label> <%--ZD 103216--%>
                            <asp:DataGrid Width="80%" ID="dgRoomHost" runat="server" AutoGenerateColumns="False" Style="border-collapse: separate;" GridLines="None"
                            OnItemDataBound="dgRoomHost_ItemDataBound">
                                <Columns>
                                    <asp:BoundColumn DataField="RoomName" HeaderText="" ItemStyle-Width="60%">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="blackblodtext" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:WebResources, SelectHost%>" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rdRoomHost" GroupName="RoomHost" onclick="javascript:ClearHostRadioSelect(this);"  runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="blackblodtext" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:WebResources, NoofAttendees%>" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID ="dlNoofAttendees" ></asp:DropDownList>                        
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <%--FB 2426 Start--%>
                     <tr id="trPreviewGuestLocation" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_GuestLocation%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblGuestLocation" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--FB 2426 End--%>
					<%--FB 2446 - Start--%>
                    <tr id="trPublicOpen" runat="server"> <%-- Code Modified For MOJ Phase2 --%>                    
                        <td></td>
                        <td id="tdpublic1" runat="server" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_tdpublic1%>" runat="server"></asp:Literal></td>
                        <td id="tdpubopen" runat="server" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_tdpubopen%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblPublic" runat="server"></asp:Label>
                            <asp:Label ID="plblSlash" Text="/" runat="server" />
                            <asp:Label ID="plblOpenForRegistration" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--FB 2446 - End--%>
                     <%if (!(client.ToString().ToUpper() == "MOJ"))
                       {%> <%--Edited For FB 1425--%>
                    <tr id="trSendIcalAttachment" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SendiCalAttac%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:Label ID="plblICAL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%} %><%--Edited For FB 1425--%>
                    <%--FB 2359--%>
                    <%--FB 2632 Starts--%>
                    <tr><%--FB 2670 Starts--%>
                       <td></td>
                       <td id="tdConcierge" class="blackblodtext" runat="server" align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_tdConcierge%>" runat="server"></asp:Literal></td><%--FB 3023--%>
                       <td>
                       <table style="margin-left:-3px; margin-top:-7px">
                       <tr>
                       <td id="tdOnsiteAV" valign="top" class="blackblodtext" runat="server" align="left">
                       <label id="LabelOnsiteAV" runat="server" style="font-weight: normal; "><asp:Literal ID="Literal51" Text="<%$ Resources:WebResources, ExpressConference_OnSiteAVSuppo%>" runat="server"></asp:Literal> ></label><%--FB 2670--%><label id="plblOnsiteAV" runat="server" style="font-weight: normal; " ></label></td>
                        </tr>
                        <tr>
                        <td id="tdMeetandGreet" valign="top" class="blackblodtext" runat="server" align="left">
                       <label id="LabelMeetandGreet" runat="server" style="font-weight: normal; " ><asp:Literal ID="Literal52" Text="<%$ Resources:WebResources, ExpressConference_MeetandGreet%>" runat="server"></asp:Literal> ></label><%--FB 2670--%><label id="plblMeetandGreet" runat="server" style="font-weight: normal; "></label></td>
                       </tr>
                       <tr>
                       <td id="tdConciergeMonitoring" valign="top" class="blackblodtext" runat="server" align="left">
                       <label id="LabelConciergeMonitoring" runat="server" style="font-weight: normal; "><asp:Literal ID="Literal63" Text="<%$ Resources:WebResources, ExpressConference_CallMonitoring%>" runat="server"></asp:Literal> ></label><%--FB 2670--%><label id="plblConciergeMonitoring" runat="server" style="font-weight: normal; " ></label></td>
                       </tr>
                       <tr>
                       <td id="tdDedicatedVNOC" valign="top" class="blackblodtext" runat="server" align="left">
                       <label id="lblDedicatedVNOC" runat="server" style="font-weight: normal; "><asp:Literal ID="Literal67" Text="<%$ Resources:WebResources, ConferenceSetup_DedicatedVNOC%>" runat="server"></asp:Literal> ></label><%--FB 2670--%><label id="plblDedicatedVNOC" runat="server"  style="font-weight: normal; "></label></td>
                       </tr>
                       </table>
                       </td>
                      
                     </tr>
                     <%--FB 2670 Ends--%>
                     <%--FB 2632 End--%>
                     <tr>
                        <td></td>
                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CustomOptions%>" runat="server"></asp:Literal></td> <%-- Changed for FB 1718 --%>
                        <td valign="top">
                            <asp:Label ID="plblCustomOption" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trPLBLAVWO">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_AudiovisualWor%>" runat="server"></asp:Literal></td><%-- FB 2570 --%>
                        <td valign="top">
                            <asp:TextBox TabIndex="-1" ID="plblAVWorkOrders" runat="server" ReadOnly="true" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="20px"></asp:TextBox><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_workorders%>" runat="server"></asp:Literal>
                            <asp:Label ID="plblAVInstructions" EnableViewState="false" runat="server" CssClass="lblError" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trPLBLCATWO">
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_CateringWorkO%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:TextBox TabIndex="-1" ID="plblCateringWorkOrders" runat="server" ReadOnly="true" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="20px"></asp:TextBox><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_workorders%>" runat="server"></asp:Literal>
                            <asp:Label ID="plblCATInstructions" runat="server" CssClass="lblError" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trPLBLHKWO" runat="server"><%--FB 2694--%>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal ID="Literal68" Text="<%$ Resources:WebResources, ConferenceSetup_FacilityWorkO%>" runat="server"></asp:Literal></td><%--FB 2570--%>
                        <td valign="top">
                            <asp:TextBox TabIndex="-1" ID="plblHouseKeepingWorkOrders" runat="server" ReadOnly="true" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="20px"></asp:TextBox><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_workorders%>" runat="server"></asp:Literal>
                            <asp:Label ID="plblHKInstructions" runat="server" CssClass="lblError" Text=""></asp:Label>
                        </td>
                    </tr>
                    <%-- Changed for FB 1926 --%>
                    <tr id="trAutParticipant" runat="server"> <%-- FB 2694 --%>
                        <td></td>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_SendAutomated%>" runat="server"></asp:Literal></td>
                        <td valign="top">
                            <asp:CheckBox ID="chkReminder" runat="server"></asp:CheckBox>
                        </td>
                    </tr>
                    
                    <tr><td colspan="3" align="center">
                    <asp:Table runat="server" Width="100%" CellSpacing="5" cellpadding="2" id="tblConflict" Visible="False">
                    <asp:TableRow ID="TableRow11" runat="server"><asp:TableCell ID="TableCell8" HorizontalAlign="Center" runat="server">
                    <h4><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_Resolvethefol%>" runat="server"></asp:Literal></h4>
                    </asp:TableCell></asp:TableRow>
                    <asp:TableRow ID="TableRow12" runat="server"><asp:TableCell ID="TableCell9" HorizontalAlign="Center" runat="server">
                    <!-- Code changed for FB 1073
                                    <asp:BoundColumn DataField="startDate" HeaderText="Date">
                                   -->  
                        <asp:DataGrid Width="80%" ID="dgConflict" runat="server" AutoGenerateColumns="False" OnItemDataBound="InitializeConflict" style="border-collapse:separate"> <%--Edited for FF--%>
                                <Columns>
                                    <asp:BoundColumn DataField="formatDate" HeaderText="<%$ Resources:WebResources, Date%>">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="conflict" HeaderText="<%$ Resources:WebResources, ExpressConference_Conflict%>" ItemStyle-Wrap="false">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <%--ZD 100085 --Start--%>
                                    <%--code added for buffer zone --Start--%>                                    
                                     <asp:TemplateColumn>
                                     <HeaderTemplate> <%--buffer zone --%>
                                      <asp:Literal ID="Literal69" Text="<%$ Resources:WebResources, ExpressConference_SetupTime%>" runat="server"></asp:Literal>
                                     </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictSetupTime" style="width:auto"><%--Edited for FF--%>
                                            </mbcbb:combobox>
                                    <asp:RequiredFieldValidator ID="ReqConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Start Time">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate><%--window dressing--%>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictStartTime" DataValueField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' DataTextField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' style="width:auto"><%--Edited for FF--%>
                                            </mbcbb:combobox>
                                        
                                    <%--Code added for FB 1426--%>
                                    <asp:RequiredFieldValidator ID="ReqConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%--FB 1715--%>
                                    <%--Code added for FB 1426--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                     
                                    <%--code added for buffer zone --End--%>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ExpressConference_Endtime%>">
                                        <ItemTemplate>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictEndTime" style="width:auto"><%--Edited for FF--%>
                                            </mbcbb:combobox>
                                    <asp:RequiredFieldValidator ID="ReqConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate> <%--buffer zone--%>
                                            <asp:Literal ID="Literal70" Text="<%$ Resources:WebResources, ExpressConference_Teardowntime%>" runat="server"></asp:Literal>  
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictTeardownTime" style="width:auto"><%--Edited for FF--%>
                                            </mbcbb:combobox>
                                    <asp:RequiredFieldValidator ID="ReqConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                        Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
									<%--ZD 100085 End--%>	
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Action%>">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <asp:Button runat="server" id="btnViewConflict" cssclass="altMedium0BlueButtonFormat" text="<%$ Resources:WebResources, ConferenceSetup_btnViewConflict%>"></asp:Button><%--FB 2889--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="startHour" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startSet" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="durationMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startDate" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkConflictDelete" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:TemplateColumn>
                                </Columns>
                            <AlternatingItemStyle  CssClass="tablebody"  />
                            <ItemStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableHeader" />
                            </asp:DataGrid>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                        </td>
                    </tr>
                    <tr><td colspan="3" align="center">
                        <%-- Code Added for FB 1428--%>
                        <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/><%--edited for FF--%>
                        <button runat="server" type="submit" class="altLongBlueButtonFormat" ID="btnConfSubmit" onclick="javascript:return InitSetConference();">Submit Conference</button><%--FB 2457 Exchange Round trip ZD 100875--%><%--ZD 101815--%>
                    </td></tr>
                      
                </table>
                <script language="javascript"> 
                if(document.getElementById("hdnCrossroomModule").value != null)
 	                roomModule_Session = document.getElementById("hdnCrossroomModule").value;
                 else
 	                roomModule_Session = "<%=Session["roomModule"] %>";
                 	
                 if(document.getElementById("hdnCrossfoodModule").value != null)
 	                foodModule_Session = document.getElementById("hdnCrossfoodModule").value;
                 else
 	                foodModule_Session = "<%=Session["foodModule"] %>";
                 	
                 if(document.getElementById("hdnCrosshkModule").value != null)
 	                hkModule_Session = document.getElementById("hdnCrosshkModule").value;
                 else
 	                hkModule_Session = "<%=Session["hkModule"] %>"; 
 	                
                    if (roomModule_Session != "1" || "<%=Session["admin"] %>" == "0") //FB 2274
                    {
                        document.getElementById("trPLBLAVWO").style.display="none";
                    }
                    if (hkModule_Session != "1" || "<%=Session["admin"] %>" == "0")  //FB 2274
                    {
                        document.getElementById("trPLBLHKWO").style.display="none";
                    }
                    
                    if (foodModule_Session != "1" || "<%=Session["admin"] %>" == "0")  //FB 2274
                        document.getElementById("trPLBLCATWO").style.display="none";
                </script>
        
<%--                <script language="javascript">
                    for (i=0;i<15;i++)
                    {
                        if (document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton") != null)
                        {
                            document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton").href = document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton").href.replace("javascript:", "javascript:CheckFiles();");
                            //alert(document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton").href);
                        }
                    }
                    if (document.getElementById("FinishNavigationTemplateContainerID_FinishPreviousButton"))
                        document.getElementById("FinishNavigationTemplateContainerID_FinishPreviousButton").style.display="none";
                </script>
--%>
                                    </asp:Panel>
                                </asp:View>
                            </asp:MultiView>
    </div>
    
    <%--FB 2693 Start--%>
    <div id="divPCdetails" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 400px; display: none; background-repeat:no-repeat;">
        <table width="100%">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'><asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_INFORMATION%>" runat="server"></asp:Literal></b>
                </td>
             </tr>                        
        </table>
        <div style="height:500px; overflow-y:scroll">
        <table  border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table2">
             <tr>
                <td colspan="3" height="20px;"></td>
             </tr>
            <tr>
                <td colspan="3" >
                    <div id="PCHtmlContent"></div>
                </td>
            </tr>           
        </table>
        </div>
        <table width="100%">
            <tr style="height: 40px;">
                <td align="center" colspan="3">
                    <button id="btnPCCancel" class="altShortBlueButtonFormat">
                        <asp:Literal ID="Literal71" Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal></button>
                </td>
            </tr>
        </table>                
    </div>
    <%--FB 2693 End--%>
    
<script language="javascript">

                    for (i=0;i<15;i++) //FB 1039
                    {
                        if (document.getElementById("TopMenun" + i) != null)
                        {
                       // alert(document.getElementById("TopMenun"));
                            document.getElementById("TopMenun" + i).innerHTML = document.getElementById("TopMenun" + i).innerHTML.replace("javascript:", "javascript:if (CheckFiles()) ");
                        }
                    }

</script>
    <asp:TextBox ID="HKDefaultRoomID" Text="-1" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="HKDefaultSetID" Text="0" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="HKDefaultQuantity" Text="1" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTimeCheck" Text="" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtModifyType" Text="" Visible="false" runat="server"></asp:TextBox>
    <input type="hidden" id="hdnSetStartNow" runat="server" /><%--FB 1825--%>
    <input type="hidden" id="txtdgUsers" runat="server" />
    <input type="hidden" id="txtLecturer" runat="server" />
                        <div id="divPic" style="display:none;">
                            <img src="" name="myPic" width="200" height="200" alt="MyPic" />  <%--ZD 100419--%>
                        </div>
                            
                    </div>
            </td>
        </tr>
        <%--Window Dressing--%>
        <tr class="tabContents">
            <td align="center">
                <table cellpadding="10" cellspacing="10" border="0" width="80%">
                    <tr>
                        <td align="right" width="50%">
                       <input id="txthdnFocus" type="text" onfocus="fnfocus();" style="background-color:Transparent;border:none;" />
                       <%--<button id="txthdnFocus" runat="server">Test</button>--%>
                            <asp:Button ID="btnPrev" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, ConferenceSetup_btnPrev%>" OnClientClick="javascript:return CheckFiles();" OnClick="MoveBack" /> 
                        &nbsp;</td>
                        <td align="left" width="50%">
                            <asp:Button ID="btnNext" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, ConferenceSetup_btnNext%>" OnClientClick="javascript:document.getElementById('hdnValue').value ='N';return CheckFiles('N');" OnClick="MoveNext" /> 
                        &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>    
    <%--ZD 101815--%>
    <input id="p2pConfirmBtn" type="button" runat="server" style="display:none" />
    <ajax:ModalPopupExtender ID="p2pConfirmPopup" runat="server" TargetControlID="p2pConfirmBtn" PopupControlID="p2pConfirmPanel" 
    DropShadow="false" Drag="true" BackgroundCssClass="modalBackground" CancelControlID="p2pConfirmOk">
    </ajax:ModalPopupExtender>
    <asp:Panel ID="p2pConfirmPanel" Width="50%" Height="25%" runat="server" CssClass="treeSelectedNode" >
    <table width="100%" style="text-align:center">
        <tr>
            <td colspan="2"><br /><br /><b><asp:Literal Text="<%$ Resources:WebResources, ExpressConference_P2PConfirmation%>" runat="server" /></b></td>
        </tr>
        <tr>
            <td><br /><br /></td>
        </tr>
        <tr>
            <td>
                <input id="p2pConfirmOk" type="button" class="altMedium0BlueButtonFormat" runat="server" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" onclick="p2pSetConference('0')" />
            </td>
            <td>
                <input id="p2pConfirmCan" type="button" class="altLongBlueButtonFormat" runat="server" style="width:180px" value="<%$ Resources:WebResources, ExpressConference_P2PConfMCU%>" onclick="p2pSetConference('1')" /><%--ZD 103063--%>
            </td>
        </tr>
    </table>
    </asp:Panel>    
	<%--ZD 101815--%>   
    </form>
    <%--Code added for FB 1308 start--%>
    <script type="text/javascript">

    //FB 2620 Starts
    if(document.getElementById("lstVMR") != null)
    {
        //var lstVMR = document.getElementById("lstVMR").selectedIndex;
        var e =document.getElementById("lstVMR"); //ZD 100753
        var lstVMR=  e.options[e.selectedIndex].value;
    }
    if(lstVMR == 1)  
    {  
        document.getElementById("divbridge").style.display = "";
        if(document.getElementById("divChangeVMRPIN") != null)
            document.getElementById("divChangeVMRPIN").style.display = "none"; //ZD 100522
    }
    //FB 2620 Ends
    
    //FB 2634 //ZD 100284
    if (document.getElementById("confStartTime_Text")) 
    {
        var confstarttime_text = document.getElementById("confStartTime_Text");
        confstarttime_text.onblur = function() {
            if(formatTimeNew('confStartTime_Text','regConfStartTime',"<%=Session["timeFormat"]%>"))
                return ChangeEndDate()
        };
    }
    if (document.getElementById("confEndTime_Text")) 
    {
        var confendtime_text = document.getElementById("confEndTime_Text");
        confendtime_text.onblur = function() {
            if(formatTimeNew('confEndTime_Text','regEndTime',"<%=Session["timeFormat"]%>"))
                return ChangeEndDate("ET")
        };
    }
    
    //ZD 100284
    if (document.getElementById("startByTime_Text")) 
    {
        var confstarttime_text = document.getElementById("startByTime_Text");
        confstarttime_text.onblur = function() {
            formatTimeNew('startByTime_Text','RegularExpressionValidator14',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("completedByTime_Text")) 
    {
        var confstarttime_text = document.getElementById("completedByTime_Text");
        confstarttime_text.onblur = function() {
            formatTimeNew('completedByTime_Text','regTime',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("startByTimeHK_Text")) 
    {
        var confstarttime_text = document.getElementById("startByTimeHK_Text");
        confstarttime_text.onblur = function() {
            formatTimeNew('startByTimeHK_Text','RegularExpressionValidator2',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("completedByTimeHK_Text")) 
    {
        var confstarttime_text = document.getElementById("completedByTimeHK_Text");
        confstarttime_text.onblur = function() {
            formatTimeNew('completedByTimeHK_Text','RegularExpressionValidator18',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("CATMainGrid_ctl02_lstDeliverByTime_Text")) 
    {
        var confstarttime_text = document.getElementById("CATMainGrid_ctl02_lstDeliverByTime_Text");
        confstarttime_text.onblur = function() {
            formatTimeNew('CATMainGrid_ctl02_lstDeliverByTime_Text','CATMainGrid_ctl02_RegularExpressionValidator1',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("startByTimeHK_Text")) 
    {
        var confstarttime_text = document.getElementById("startByTimeHK_Text");
        confstarttime_text.onblur = function() {
            formatTimeNew('startByTimeHK_Text','RegularExpressionValidator2',"<%=Session["timeFormat"]%>")
        };
    }
        
    var chkrecurrence = document.getElementById("chkRecurrence");
    
    if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
    {
        if(chkrecurrence)
                chkrecurrence.checked = true;
        document.getElementById("hdnRecurValue").value = 'R'
            
        AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
        st = calStart(atint[1], atint[2], atint[3]);
        et = calEnd(st, parseInt(atint[4], 10));
        //FB 2699
        document.getElementById("RecurringText").value =  recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et, document.getElementById("TimeZoneText").value, Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
    }
    //FB 2634
    if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
        openRecur()    
    
    if(chkrecurrence)
    {
        if(chkrecurrence.checked == true && (document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0"))
        {
            if(document.getElementById("Recur").value == "")
            {
                initial();    
                fnShow();
            }
        }
    }
    
    if(chkrecurrence)
    {
        if(!chkrecurrence.checked)
            ChangeImmediate();
    }
      //<%--FB 1481 Start--%>  
      if(document.getElementById("<%=rdSelView.ClientID%>"))
      {
        if(!document.getElementById("<%=rdSelView.ClientID%>").disabled)
            document.getElementById("<%=rdSelView.ClientID%>").onclick= function()
            { DataLoading(1);};
      }      
      <%--FB 1481 End--%>  
    </script>
    <%--Code added for FB 1308 end--%>
    
    <%--Merging Recurrence start--%>
    <script type="text/javascript"> 
	  
	  if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
    	
	    if (document.getElementById("ModifyType") != null)// Added for FF
	    if (document.getElementById("ModifyType").value=="3") {
		    document.getElementById("RemoveRecurDiv").style.display = "none";
	    }
        //FB 2634
//         var chkrecurrence = document.getElementById("chkRecurrence");
//         
//        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
//        {
//            var chkrecurrence = document.getElementById("chkRecurrence");
//            if(chkrecurrence)
//                chkrecurrence.checked = true;
//            document.getElementById("hdnRecurValue").value = 'R'
//            initial();
//           
//            fnShow();
//        }
        
         fnEnableBuffer();
        
     }
        //Added for FF Fix START
        if(document.getElementById("lstDuration_Container") != null)
        document.getElementById("lstDuration_Container").style.width = "auto";
        
        if(document.getElementById("confStartTime_Container") != null)
        document.getElementById("confStartTime_Container").style.width = "auto";
        
        if(document.getElementById("confEndTime_Container") != null)
        document.getElementById("confEndTime_Container").style.width = "auto";
        
        if(document.getElementById("SetupTime_Container") != null)
        document.getElementById("SetupTime_Container").style.width = "auto";
        
        if(document.getElementById("TeardownTime_Container") != null)
        document.getElementById("TeardownTime_Container").style.width = "auto";
        
        if(document.getElementById("completedByTime_Container") != null)
        document.getElementById("completedByTime_Container").style.width = "auto";
        
        if(document.getElementById("dgConflict_conflictStartTime_Container") != null)
        document.getElementById("dgConflict_conflictStartTime_Container").style.width = "auto";
        
        if(document.getElementById("startByTime_Container") != null)
        document.getElementById("startByTime_Container").style.width = "auto";
        
       //Commented for FB 1722 - Start
//        if(navigator.appName != "Microsoft Internet Explorer")
//	{ 
//	 if(document.getElementById("tabCelldur") != null)
//            document.getElementById("tabCelldur").style.width = "35%";
//	}
//       else
//	{             
//            if(document.getElementById("tabCelldur") != null)
//            document.getElementById("tabCelldur").style.width = "36%";
//	}
       //Commented for FB 1722 - End       
        //Added for FF Fix END
	//FB 1734 - Method getAudioparticipantListNET moved to settings2.js
	
	 //FB 1830
    function ClientValidate(source, arguments)
    {     
        var cFor = '<%=cFormat%>';
        var strValidation = "";

        if(cFor == "�")
            strValidation = /^\d+$|^\d+\,\d{1,2}$/ ;
        else
            strValidation = /^\d+$|^\d+\.\d{1,2}$/ ;

        if (!arguments.Value.match(strValidation ) ) 
            arguments.IsValid = false       
    }
    //FB 2274 Starts
    var isSpecialRecur_Session ;
 		
    if(document.getElementById("hdnCrossisSpecialRecur").value != null)
 	    isSpecialRecur_Session = document.getElementById("hdnCrossisSpecialRecur").value;
    else
 	    isSpecialRecur_Session = "<%=Session["isSpecialRecur"] %>";
    //FB 2274 Ends
    
    //FB 1911 - Start
    if ('<%=isEditMode%>' == "1" || isSpecialRecur_Session == "0") //FB 2052 FB 2274
    {
        if(document.getElementById("SPCell1"))
            document.getElementById("SPCell1").style.display = 'None'
        
        if(document.getElementById("SPCell2"))
            document.getElementById("SPCell2").style.display = 'None'
    }    
    
    
	if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	{
        if(document.getElementById("RecurSpec").value != "")
        {
           showSpecialRecur();
           document.getElementById("RecurText").value = document.getElementById("RecurringText").value;
        }
    }
    
    //FB 1911 - End
    
    //Firefox Remember Password Issue
    function clearValues()
    {
        if(document.getElementById('ConferencePassword2') != null)//FB 2164
        {
           if(document.getElementById('ConferencePassword2').value =="")
           {
            document.getElementById('ConferencePassword').value="";
           }
       }
    }

   //function triggerClear() // Covered on FB 2050
   //{
     //setTimeout("clearValues()",500);
   //}
   //window.onload=triggerClear;
    </script>
    <%--Merging Recurrence end--%>
    
     <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->
<script type="text/javascript">

//FB 2426 Start
    function fnchangetype() {
        if(document.getElementById('txtIPPassword')!= null)
            document.getElementById('txtIPPassword').type = "password";
        if (document.getElementById('txtIPconfirmPassword') != null)
            document.getElementById('txtIPconfirmPassword').type = "password";
        if (document.getElementById('txtISDNPassword') != null)
            document.getElementById('txtISDNPassword').type = "password";
        if (document.getElementById('txtISDNconfirmPassword') != null)
            document.getElementById('txtISDNconfirmPassword').type = "password";
        if (document.getElementById('txtSIPPassword') != null)
            document.getElementById('txtSIPPassword').type = "password";
        if (document.getElementById('txtSIPconfirmPassword') != null)
            document.getElementById('txtSIPconfirmPassword').type = "password";
    }
//FB 2426 End

function refreshImage()
{
  fnHideStartNow();//FB 1825
  fnchangetype();//FB 2426
  //triggerClear();
  setTimeout("clearValues()",500);
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle(); // Commented for Refresh Issue
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}

window.onload = refreshImage;

function refreshIframe()
{
    var iframeId = document.getElementById('ifrmPartylist');
    iframeId.src = iframeId.src;    
}

//FB 1825 Start
function fnHideStartNow() 
{
    var startNow = document.getElementById('hdnSetStartNow').value;
    if (startNow == "hide" && chkrecurrence.checked == false) 
    {
        document.getElementById("StartNowRow").style.display = "none";//FB 2634
    }
}
//FB 1825 End

//FB 2274
if(document.getElementById('hdnCrossAddtoGroup') != null) //FB 2998
{
    if(document.getElementById('hdnCrossAddtoGroup').value != "")
    {
        if(document.getElementById('btnAddgrp')) //FB 2998
            document.getElementById('btnAddgrp').style.display = 'none'
    }
}

//FB 2486
function fnCheck(arg)
{
   var srcID = document.getElementById(arg);
   var ckboxName = "chkmsg";
   var ctrlIDNo = arg.substring(arg.length, arg.length-1)
   var drpName = srcID.id.replace(ctrlIDNo,"");   
   var pVal = srcID.getAttribute("PreValue");   
   var ckboxSel = document.getElementById(ckboxName + "" + ctrlIDNo);

   if (ckboxSel && ckboxSel.checked == false)
       return true;
   
   if(pVal == null)
       pVal = srcID.options[0].text;
     
   for(var i = 1; i <= 9; i++)
   {    
        var ckbox = document.getElementById(ckboxName + "" + i);        
        if(ckbox)
        {
            if(i == ctrlIDNo)
                continue;
            
            if(ckbox.checked)
            {
                var destDrpName = document.getElementById(drpName + i);
                if(destDrpName)
                {
                    if(destDrpName.value == srcID.value)
                    {
                        srcID.value = pVal;
                        alert(ConfSelectTimemsg);                       
                        if(ckboxSel)
                            ckboxSel.checked = false;
                        return false;
                    }
                }
            }
        }
   }   
   
   srcID.setAttribute("PreValue",srcID.value);
   return true;

}
//FB 2717 Starts
function fnChkCloudConference() {

    var chkCloud = document.getElementById("chkCloudConferencing");
    if (chkCloud && chkCloud.checked) {
    
        var e = document.getElementById("lstVMR");
        
        var VMRType = e.options[e.selectedIndex].value;

        e.value = 3; //ZD 100753
        
        //document.getElementById("lstVMR").selectedIndex = 3;
        document.getElementById("lstVMR").disabled = true;
        document.getElementById("divbridge").style.display = "none";
        document.getElementById("trConfType").style.display = "none";
        document.getElementById("trStartMode").style.display = "none";
        document.getElementById("trVMR").style.display = "";
        document.getElementById("lstConferenceType").value = "2";
        if (document.getElementById("divChangeVMRPIN") != null)
            document.getElementById("divChangeVMRPIN").style.display = "none"; //ZD 100522
    }
    else {
        document.getElementById("lstVMR").selectedIndex = 0;
        document.getElementById("lstVMR").disabled = false;
        document.getElementById("trConfType").style.display = "";
    }
    changeVMR();
    fnShowHideAVforVMR();
}
//ZD 100890 Start
function fnchkStatic() 
{
    var chkStatic = document.getElementById("chkStatic");
    if (chkStatic && chkStatic.checked) 
    {
        document.getElementById("divStaticID").style.display = "block";
    }
    else
    {
        document.getElementById("divStaticID").style.display = "none";
    }
}
//ZD 100890 End
//FB 2699
function fnSetTimezone()
{
    var hdnOldTimezone = document.getElementById("hdnOldTimezone");
    var hdnSpecRec =  document.getElementById("hdnSpecRec");
    var RecurringText =  document.getElementById("RecurringText");
    var RecurText =  document.getElementById("RecurText");
    
    if(hdnSpecRec)
    {
        if(hdnSpecRec.value == "1" && hdnOldTimezone.value != document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].text)
        {
            RecurringText.value = RecurringText.value.replace(hdnOldTimezone.value,document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].text);
            RecurText.value = RecurringText.value;
            
            hdnOldTimezone.value = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].text            
        }
    }
    
}

// FB 2892 Starts
function fnRemoveTabSpace() {
    var id = "TopMenun";
    var dynId = "";
    var cont = "";
    for (var k = 0; k < 9; k++) {
        dynId = id + k.toString();
        cont = document.getElementById(dynId).innerHTML;
        if (cont.indexOf('div') > -1 || cont.indexOf('DIV') > -1)
            continue;
        document.getElementById(dynId).style.display = 'none';
        document.getElementById(dynId).nextSibling.style.display = 'none';
    }
}
fnRemoveTabSpace()
// FB 2892 Ends

//FB 2998
//if( "<%=mcuEnable %>" == "1")
//{
//    document.getElementById("chkMCUConnect").checked = true;
//    document.getElementById("MCUConnectDisplayRow").style.display = '';
//}

openMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>") //FB 2998

// ZD 100263 Starts
if (document.getElementById("lstDuration_Text") != null) {
    document.getElementById("lstDuration_Text").setAttribute("onblur", "fnCheckChar()");
}

function fnCheckChar() {
    var dur = document.getElementById("lstDuration_Text");
    if (dur.value.indexOf('<') > -1 || dur.value.indexOf('>') > -1 || dur.value.indexOf('&') > -1) {
        alert(ValidDuration);
        while (dur.value.indexOf('<') > -1 || dur.value.indexOf('>') > -1 || dur.value.indexOf('&') > -1) {
            dur.value = dur.value.replace("<", "");
            dur.value = dur.value.replace(">", "");
            dur.value = dur.value.replace("&", "");
        }
        dur.focus();
    }
}
// ZD 100263 Ends
//ZD 100420 Start
if (document.getElementById('Browse1') != null)
       document.getElementById('Browse1').setAttribute("onblur", "document.getElementById('btnRemove1').focus(); document.getElementById('btnRemove1').setAttribute('onfocus', '');");
       
if (document.getElementById('Browse2') != null)
       document.getElementById('Browse2').setAttribute("onblur", "document.getElementById('btnRemove2').focus(); document.getElementById('btnRemove2').setAttribute('onfocus', '');");

if (document.getElementById('Browse3') != null)
    document.getElementById('Browse3').setAttribute("onblur", "document.getElementById('btnRemove3').focus(); document.getElementById('btnRemove3').setAttribute('onfocus', '');");

function fnfocus() {

    if (document.getElementById("btnPrev") != null) {
        document.getElementById('btnPrev').setAttribute('onfocus', '');
        document.getElementById('btnPrev').focus();
        document.getElementById('btnPrev').setAttribute("onblur", "document.getElementById('btnNext').focus();");
        document.getElementById('btnNext').setAttribute('onfocus', '');
    }
    else if (document.getElementById("btnNext") != null) {
        document.getElementById('btnNext').setAttribute('onfocus', '');
        document.getElementById('btnNext').focus();
    }
    else
        document.getElementById('backtotop').focus();
}

//ZD 100420 End

</script>
<!-- FB 2050 End -->

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        //ZD 103872 - Start
        if (keyCode == 13) 
        {                    
                var str = document.activeElement.type;
                if ((str == "text" || str == "textarea" || str == "password"))
                    return false;
        }
        //ZD 103872 - End
        fnOnKeyDown(evt);
    };
    
    function EscClosePopup() {
            if (document.getElementById('prompt') != null)
                cancelthis(); //Close the Layout Popup in A/V settings
                //document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))
            if (document.getElementById('PopupLdapPanel') != null &&
                    document.getElementById('btnGuestLocation_backgroundElement') != null) {
                fnDisableValidator();
                document.getElementById("ClosePUp").click();
                //document.getElementById('PopupLdapPanel').style.display = "none";
                //document.getElementById('btnGuestLocation_backgroundElement').style.display = "none";
                
                
            }
    }
        var i = 1;
        if (navigator.userAgent.indexOf('Trident') > -1)
            i = 0;
        if (document.getElementById("TopMenu").parentNode.childNodes[i] != null)
            document.getElementById("TopMenu").parentNode.childNodes[i].tabIndex = '-1';

        if (document.getElementById('RevSubmit').parentNode.className.indexOf('selectedTab') == -1) {
        document.getElementById('RevSubmit').parentNode.setAttribute("onblur", "setTimeout('window.scrollTo(0,0);;', 1000);");
    }
    //ZD 100420
    if (document.getElementById("BasicTab"))
        document.getElementById("BasicTab").parentNode.setAttribute("onfocus", "setTimeout('window.scrollTo(0,0);;', 100);");
    
    if(document.getElementById("RecurType_0"))
        document.getElementById("RecurType_0").setAttribute("onfocus", "setTimeout('window.scrollTo(0,0);',100)");

    fnShowWebExConf(); // ZD 100221 Email

    function fnHandleWebExPwd() { // ZD PRABU
        if (document.getElementById("chkWebex") != null) {
            if (document.getElementById("chkWebex").checked) {
                if (document.getElementById("txtPW1") != null) {
                    var val = document.getElementById("hdnWebExPwd").value;
                    if (val != "") {
                        document.getElementById("txtPW1").value = val;
                        document.getElementById("txtPW2").value = val;
                        ValidatorEnable(document.getElementById('RequiredFieldValidator7'), false);
                    }
                    else
                        ValidatorEnable(document.getElementById('RequiredFieldValidator7'), true);
                }
            }
        }
    }
    fnHandleWebExPwd();
	//ZD 100875
    function closeMe(evt) {
    
        var needAlert = false;
        var confirmCheck = true;

        if (document.getElementById("hdnSaveData") != null) {
            if (document.getElementById("hdnSaveData").value == "1")
                confirmCheck = false;
        }

        if (confirmCheck && typeof(Page_ClientValidate) == 'function') {
            if (!Page_ClientValidate())
                confirmCheck = true;
        }
           
        if (confirmCheck == true) {

            if (document.getElementById("ConferenceName") != null) {
                if (document.getElementById("ConferenceName").value.trim() != "")
                {
                    needAlert = true;
                    document.getElementById("hdnConfName").value = document.getElementById("ConferenceName").value.trim();
                }
            }
            else if(document.getElementById("hdnConfName").value != "")
                needAlert = true;
            else if (document.getElementById("hdnConferenceName") != null) {
                if (document.getElementById("hdnConferenceName").value.trim() != "") {
                    needAlert = true;
                    document.getElementById("hdnConfName").value = document.getElementById("hdnConferenceName").value.trim();
                }
            }

            if (document.getElementById("selectedloc") != null) {
                if (document.getElementById("selectedloc").value.trim() != "")
                    needAlert = true;
            } 
            
            if (document.getElementById("txtPartysInfo") != null) {
                if (document.getElementById("txtPartysInfo").value.trim().replace('|', '') != "")
                    needAlert = true;
            }
        }

//        if (document.getElementById("hdnSaveData") != null) {
//            document.getElementById("hdnSaveData").value = ""
//        }

        if (needAlert == true)
            return ""
            //return "Are you sure you want to navigate away from this reservation? This meeting has not yet been scheduled and will not be saved.";
    }
    if (document.getElementById("hdnSaveData") != null) {
        document.getElementById("hdnSaveData").value = ""
    }
    window.onbeforeunload = closeMe;

    if (document.getElementById('btnGuestLocationSubmit') != null) {
        document.getElementById('ClosePUp').setAttribute("onblur", "document.getElementById('btnGuestAddProfile').focus()");
        document.getElementById('btnGuestAddProfile').setAttribute("onfocus", "");
        document.getElementById('btnGuestAddProfile').setAttribute("onblur", "document.getElementById('btnGuestLocationSubmit').focus()");
        document.getElementById('btnGuestLocationSubmit').setAttribute("onfocus", "");
    }

    // ZD 101444 Starts
    function fnUpdateStartList() {
        if (document.getElementById('confStartTime') != null && document.getElementById('confStartTime_Text') != null)
            if (document.getElementById('confStartTime').innerHTML.toString().indexOf(document.getElementById('confStartTime_Text').value) > -1)
                document.getElementById('confStartTime').value = document.getElementById('confStartTime_Text').value;
    }
    function fnUpdateEndList() {
        if (document.getElementById('confEndTime') != null && document.getElementById('confEndTime_Text') != null)
            if (document.getElementById('confEndTime').innerHTML.toString().indexOf(document.getElementById('confEndTime_Text').value) > -1)
                document.getElementById('confEndTime').value = document.getElementById('confEndTime_Text').value;
    }
    //document.getElementById("confStartTime_Text").setAttribute("onchange", "fnUpdateStartList();");
    //document.getElementById("confEndTime_Text").setAttribute("onchange", "fnUpdateEndList();");
    fnUpdateStartList();
    fnUpdateEndList();
    // ZD 101444 Ends

    //ZD 101562 START
    var EnableTemplatebooking_Session;
    if (document.getElementById("hdntemplatebooking").value != null)
        EnableTemplatebooking_Session = document.getElementById("hdntemplatebooking").value;
    else
        EnableTemplatebooking_Session = '<%=Session["EnableTemplateBooking"] %>';

    if (document.getElementById("trConfTemp")) {
        if (EnableTemplatebooking_Session == "1")
            document.getElementById("trConfTemp").style.display = '';
        else
            document.getElementById("trConfTemp").style.display = 'None';
    }
    //ZD 101562 END

    //ZD 103216
    function ClearHostRadioSelect() {    
        var rbbox
        var i = 2
        var check = 0;
        var args = ClearHostRadioSelect.arguments;
        
        var strCtrl = 'dgRoomHost_ctl' + i + '_rdRoomHost';

        if (i <= 9)
            strCtrl = 'dgRoomHost_ctl0' + i + '_rdRoomHost';

        rbbox = document.getElementById(strCtrl);
        
        while (rbbox != null) {

            rbbox.checked = false;
            var parentid = args[0].id;
           
            if (rbbox.id == parentid)
                rbbox.checked = true;

            i = i + 1;

            strCtrl = 'dgRoomHost_ctl' + i + '_rdRoomHost';
            if (i <= 9)
                strCtrl = 'dgRoomHost_ctl0' + i + '_rdRoomHost';

            rbbox = document.getElementById(strCtrl);
        }
    }

</script>
<%--ZD 100428 END--%>

</body>
</html>

