﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>	
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_EventLog" %>
<%@ Register TagPrefix="mbcbb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.ComboBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

<%--ZD 102493--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>

<%--fnConfirm--%>
<script type="text/javascript">
    var servertoday = new Date();
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //ZD 100429
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
  
    function fnConfirm()
    {
        var blnret = true;
        var pass = "";
        var args = fnConfirm.arguments;
        var purgeType = "";
        
        if(document.getElementById("hdnPurgeData").value == "" || document.getElementById("hdnPurgeData").value == "0")  // FB 1725
        {
            alert(EventLogPurge);
            blnret = false;
        }
        purgeType = document.getElementById("hdnPurgeType").value;
        
        if(purgeType == "")    // FB 1725
        {
            alert(EventLogPurg);
            blnret =  false;
        }
        else
        {
           purgeType =  document.getElementById("hdnPurgeType").value
           
           if(purgeType == "5")
           {
               var purgeNowDummy = document.getElementById('<%=PurgeNowDummy.ClientID%>');
               if(purgeNowDummy)
               {
                    var txtpwd = document.getElementById('<%=txtPwd.ClientID%>'); 
               
                    purgeNowDummy.click();
                    if(txtpwd)
                        txtpwd.focus();
                    
                    blnret = false;
               }
           }
        }
       
        return blnret;
    }
    
   </script>
 
 <%--fnShow--%>
 
   <script type="text/javascript">

    function fnShow() {
    
    
        var args = fnShow.arguments;        
        var lbltext = document.getElementById('<%=lblText.ClientID%>');
        if(lbltext.innerHTML == "")
            lbltext.innerHTML = document.getElementById("hdnLblMsg").value; // ZD 102727
        var confName = "";
        var lblVal = "";

        var timeofDay = document.getElementById('hdntformat').value; //FB 2588
        
        if( "<%=clientName %>" == "MOJ")
            confName = "Hearings";
        else
            confName = RSConferences;
        
         switch (args[0])
        {
            case "D":
                document.getElementById("hdnPurgeType").value = "1";
                lblVal = RSat + "" + timeofDay + " " + RSPurgeTextD;
                break;
            case "W":
                document.getElementById("hdnPurgeType").value = "2";
                lblVal = RSPurgeTextF + " " + timeofDay + " " + RSAutomatic;
                break;
            case "M":
                document.getElementById("hdnPurgeType").value = "3";
                lblVal = RSPurgeTextM + " " + timeofDay + " " + RSAutomatic;
                break;
            case "N":    
                document.getElementById("hdnPurgeType").value = "4";
                lblVal = "";
                break;
            case "R":
                document.getElementById("hdnPurgeType").value = "5";
                lblVal = "";
                break;
        }
        //Edited For FF..
        if(lblVal != "")

            lbltext.innerHTML = "<asp:Literal Text="<%$ Resources:WebResources, Eventlog_PurgeCompleted%>" runat="server"></asp:Literal>" + " "+ confName + " " + lblVal;
        else
            lbltext.innerHTML = "";
          
    }
</script>
 
 <%--fnDisplay--%>
   <script type="text/javascript">

    function fnDisplay()
    {
        var args = fnDisplay.arguments;
        var reVal
       
        switch(args[0])
        {
            case 'WS':
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "None";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "None";     
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "block";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "block";
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "none";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "none";  
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "none";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "none";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "none";  
                      
                   
                   
                   document.getElementById("WSysInfoCell").className = "cellbackcolor";
                   document.getElementById("WAppLogInfoCell").className = "";
                   document.getElementById("DmyVRMLogInfoCell").className = "";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "";
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                break;
            case 'WA':
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "block";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "block"; 
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "None";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "None";  
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "none";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "none";  
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "block";    
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "none";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "none";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "none"; 
                   
                   document.getElementById("WSysInfoCell").className = "";
                   document.getElementById("WAppLogInfoCell").className = "cellbackcolor";
                   document.getElementById("DmyVRMLogInfoCell").className = "";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "";
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                   
                break;
            case 'MA':
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "block";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "block"; 
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "None";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "None";     
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "None";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "None";    
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "block";  
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "none";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "none";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "none";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "none"; 
                   
                   document.getElementById("WSysInfoCell").className = "";
                   document.getElementById("WAppLogInfoCell").className = "";
                   document.getElementById("DmyVRMLogInfoCell").className = "cellbackcolor";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "";
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                break;
            case 'AI':
                if(document.getElementById("myVRMWebLogRow1"))
                   document.getElementById("myVRMWebLogRow1").style.display = "None";       
                if(document.getElementById("myVRMWebLogRow2"))
                   document.getElementById("myVRMWebLogRow2").style.display = "None"; 
                if(document.getElementById("WebLogRow1"))
                   document.getElementById("WebLogRow1").style.display = "None";       
                if(document.getElementById("WebLogRow2"))
                   document.getElementById("WebLogRow2").style.display = "None";     
                if(document.getElementById("WebSysInfoRow1"))
                   document.getElementById("WebSysInfoRow1").style.display = "None";
                if(document.getElementById("WebSysInfoRow2"))
                   document.getElementById("WebSysInfoRow2").style.display = "None";    
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "None";
                if(document.getElementById("ExportRow"))
                   document.getElementById("ExportRow").style.display = "None";    
                if(document.getElementById("myVRMassemblyRow1"))
                   document.getElementById("myVRMassemblyRow1").style.display = "Block";  
                if(document.getElementById("myVRMassemblyRow2"))
                   document.getElementById("myVRMassemblyRow2").style.display = "Block";  
                if(document.getElementById("ComponentsRow1"))
                   document.getElementById("ComponentsRow1").style.display = "Block";  
                if(document.getElementById("ComponentsRow2"))
                   document.getElementById("ComponentsRow2").style.display = "Block"; 
                   
                   document.getElementById("WSysInfoCell").className = "";
                   document.getElementById("WAppLogInfoCell").className = "";
                   document.getElementById("DmyVRMLogInfoCell").className = "";
                   document.getElementById("WmyVRMassemblyInfoCell").className = "cellbackcolor";
                   
                   document.getElementById("hdnDisplayValue").value = args[0];
                   reVal = false;
                break;
            case 'DS':
                if(document.getElementById("DBInfoRow"))
                   document.getElementById("DBInfoRow").style.display = "block";       
                if(document.getElementById("DataPurgeRow"))
                   document.getElementById("DataPurgeRow").style.display = "None"; 
                
                 document.getElementById("DSysInfoCell").className = "cellbackcolor";
                 document.getElementById("DPurgeCell").className = "";
                 document.getElementById("hdnDisplayValue").value = args[0];
                 
                  var errLabel = document.getElementById('errLabel');
                  if(errLabel)            
                       errLabel.style.display = 'None';

                reVal = false;
                break;
            case 'DP':
                if(document.getElementById("DBInfoRow"))
                   document.getElementById("DBInfoRow").style.display = "None";       
                if(document.getElementById("DataPurgeRow"))
                   document.getElementById("DataPurgeRow").style.display = "block"; 
                 
                 document.getElementById("DSysInfoCell").className = "";
                 document.getElementById("DPurgeCell").className = "cellbackcolor";
                
                 document.getElementById("hdnDisplayValue").value = args[0];
                 
                 reVal = false;
                break;
            case 'DA':
                break;
            case 'PC':
                var lbltext = document.getElementById('<%=lblText.ClientID%>');
                if (lbltext.innerHTML == "")
                    lbltext.innerHTML = document.getElementById("hdnLblMsg").value; // ZD 102727
                if (document.getElementById("hdnPurgeData"))
                    document.getElementById("hdnPurgeData").value = '1';
                
                if(document.getElementById('<%=Daily.ClientID%>'))
                    document.getElementById('<%=Daily.ClientID%>').disabled = false;
                    
                if(document.getElementById('<%=Weekly.ClientID%>'))
                    document.getElementById('<%=Weekly.ClientID%>').disabled = false;
                
                if(document.getElementById('<%=Monthly.ClientID%>'))
                    document.getElementById('<%=Monthly.ClientID%>').disabled = false;
                 
                if(document.getElementById('<%=RNow.ClientID%>'))                
                    document.getElementById('<%=RNow.ClientID%>').checked = false;                     
                    
                if(document.getElementById('<%=RemoveType.ClientID%>') && document.getElementById("hdnPurgeType").value == "5") 
                {
                    document.getElementById('<%=RemoveType.ClientID%>').checked = true;                    
                    document.getElementById('<%=RemoveType.ClientID%>').disabled = false;                   
                    document.getElementById("hdnPurgeType").value = '4';                    
                    lbltext.innerHTML = "";//Edited For FF...
                }                    
                   
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = true;
                break;
            case 'PA':
            case 'PD':
                var lbltext = document.getElementById('<%=lblText.ClientID%>');               
                
                if(document.getElementById('<%=RNow.ClientID%>'))
                {
                    document.getElementById('<%=RNow.ClientID%>').checked = true;   
                    document.getElementById("hdnPurgeType").value = '5';
                    lbltext.innerHTML = "";//Edited For FF...
                } 
                
                if(document.getElementById('<%=Daily.ClientID%>'))
                {
                    document.getElementById('<%=Daily.ClientID%>').disabled = true;
                    document.getElementById('<%=Daily.ClientID%>').checked = false;                    
                    lbltext.innerHTML = "";//Edited For FF...
                }   
                
                if(document.getElementById('<%=Weekly.ClientID%>'))
                {
                    document.getElementById('<%=Weekly.ClientID%>').disabled = true;
                    document.getElementById('<%=Weekly.ClientID%>').checked = false;
                    lbltext.innerHTML = "";//Edited For FF...
                }   
                
                if(document.getElementById('<%=Monthly.ClientID%>'))
                {
                    document.getElementById('<%=Monthly.ClientID%>').disabled = true;
                    document.getElementById('<%=Monthly.ClientID%>').checked = false;
                    lbltext.innerHTML = "";//Edited For FF...
                }
                
                
                if(document.getElementById('<%=RemoveType.ClientID%>'))
                {
                    document.getElementById('<%=RemoveType.ClientID%>').disabled = true;
                    document.getElementById('<%=RemoveType.ClientID%>').checked = false;                    
                    lbltext.innerHTML = "";//Edited For FF...
                } 
                
                if(document.getElementById("hdnPurgeData"))
                {
                    if(args[0] == "PA")
                        document.getElementById("hdnPurgeData").value = '2';
                    else if(args[0] == "PD")
                        document.getElementById("hdnPurgeData").value = '3';
                }
                
                document.getElementById("hdnDisplayValue").value = args[0];
                
                reVal = true;
                break;
           case 'EL':
                if(document.getElementById("LogSetRow"))
                   document.getElementById("LogSetRow").style.display = "Block"; 
                if(document.getElementById("SearchLogRow"))
                   document.getElementById("SearchLogRow").style.display = "None"; 
                if(document.getElementById("RetrieveLogRow"))
                   document.getElementById("RetrieveLogRow").style.display = "None"; 
                   
               document.getElementById("ELogCell").className = "cellbackcolor";
               document.getElementById("ESearchLogCell").className = "";
               document.getElementById("ERetrieveCell").className = "";
                   
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = false;
                break;
           case 'ES':
                if(document.getElementById("LogSetRow"))
                   document.getElementById("LogSetRow").style.display = "None"; 
                if(document.getElementById("SearchLogRow"))
                   document.getElementById("SearchLogRow").style.display = "Block"; 
                if(document.getElementById("RetrieveLogRow"))
                   document.getElementById("RetrieveLogRow").style.display = "None"; 
                   
                if(document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text'))
                    document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text').style.width = "75px";
                if(document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text'))
                    document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text').style.width = "75px";
                
                document.getElementById("ELogCell").className = "";
                document.getElementById("ESearchLogCell").className = "cellbackcolor";
                document.getElementById("ERetrieveCell").className = "";
           
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = false;                
                break;                
           case 'ER':
                if(document.getElementById("LogSetRow"))
                   document.getElementById("LogSetRow").style.display = "None"; 
                if(document.getElementById("SearchLogRow"))
                   document.getElementById("SearchLogRow").style.display = "None"; 
                if(document.getElementById("RetrieveLogRow"))
                   document.getElementById("RetrieveLogRow").style.display = "Block"; 
                
                document.getElementById("ELogCell").className = "";
                document.getElementById("ESearchLogCell").className = "";
                document.getElementById("ERetrieveCell").className = "cellbackcolor";   
                
                document.getElementById("hdnDisplayValue").value = args[0];
                reVal = false;
                break;
        }
        
        if(args.length > 1 )
         {
            if(args[1] == 'N')
                reVal = '';
         }  
         else
            return reVal;
    }
</script>
 
   <script type="text/javascript">
    function deleteLog(moduleName, deleteAll)
    {
        form1.action = "EventLog.aspx?m=" + moduleName + "&d=" + deleteAll;//FB 2027(DeleteModuleLog)
        form1.submit();
    }
    
    function fnTabValue()
    {
        document.getElementById('hdnValue').value= fnTabValue.arguments[0];
    }
    </script>
    
 <%--OnChanged--%>
 
   <script type="text/javascript">
    function OnChanged(sender, args)	
    {	    
        var errLabel = document.getElementById('errLabel');
        if(errLabel)
            errLabel.style.display = 'None';
        
        sender.get_clientStateField().value = sender.saveClientState();	
        var activeIndex = sender.get_activeTabIndex();
        
        document.getElementById('hdnActiveIndex').value = activeIndex;
        
        fnMaintainDesign();
    }	
    
    function fnMaintainDesign()
    {
        var activeIndex = document.getElementById('hdnActiveIndex').value;
        if(activeIndex == 0)
        {
            if(document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text'))
                document.getElementById('DiagnosticsTabs_EventLogView_startTimeCombo_Text').style.width = "75px";
            if(document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text'))
                document.getElementById('DiagnosticsTabs_EventLogView_endTimeCombo_Text').style.width = "75px";

           
                fnDisplay('ER') //ALLBUGS-27
        }
        else if(activeIndex == 1)
        {
            if(document.getElementById('hdnDisplayValue').value == 'WA')
                fnDisplay('WA');
            else if(document.getElementById('hdnDisplayValue').value == 'MA')
                fnDisplay('MA');
            else if(document.getElementById('hdnDisplayValue').value == 'AI')
                fnDisplay('AI');
            else
                fnDisplay('WS');
        }
        else if(activeIndex == 2)
        {
            var hdnval = document.getElementById('hdnDisplayValue').value;
            
            if(hdnval == 'DP' || hdnval == 'PA' || hdnval == 'PC' || hdnval == 'PD')
            {
                fnDisplay('DP','N');
                
                if(hdnval != 'DP' && hdnval != 'PC')
                    document.getElementById("hdnPurgeData").value = '2';
                
                if(document.getElementById("hdnPurgeData"))
                {
                    if(document.getElementById("hdnPurgeData").value == "1")
                    {
                        if(document.getElementById("DCompletedConf"))
                            document.getElementById("DCompletedConf").checked = true;
                            
                        fnDisplay('PC');
                    }
                    else
                        fnDisplay(hdnval);
                }  
            } 
            else
                fnDisplay('DS');
        }
    }
  </script>
  
 <%--fnCheck--%>
   <script type="text/javascript">  
    function fnCheck()
    {
        var txtpwd = document.getElementById('<%=txtPwd.ClientID%>');      
        var btnClose = document.getElementById('<%=Close.ClientID%>');  
        var errLabel = document.getElementById('errLabel');
        if(errLabel)            
           errLabel.style.display = 'None';    
          
        var pass;
        var blnret;
        
        var lbltext = document.getElementById('<%=lblText.ClientID%>');
        
        lbltext.innerText = "";
        
         if(txtpwd)
            pass = txtpwd.value;
        
        if(Trim(pass) == "")
        {
            alert(EventlogPass);
            blnret = false;
        }        
        else
        {
            document.getElementById('hdnPass').value = pass;
            blnret = true;
        }
        
        if(blnret)
        {
            if(document.getElementById("hdnPurgeData").value == '1')
            {
                if( "<%=clientName %>" == "MOJ")
                    blnret = confirm(EventLogPastHear);
                else
                    blnret = confirm(EventLogPastConf);
            }
            else if(document.getElementById("hdnPurgeData").value == '2')
            {
                if( "<%=clientName %>" == "MOJ")
                    blnret = confirm(EventLogDeleteHear);
                else
                    blnret = confirm(EventLogDeleteConf);
            }
            else if(document.getElementById("hdnPurgeData").value == '3')
            {
                blnret = confirm(EventLogDeleteall);
            }
        }
      
        return blnret;
    }
 </script>
 
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js" ></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<html> <%--ALLBUGS-27--%>
<head runat="server">
    <title>myVRM Event Logs</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%><%--FB 1947--%>
</head>
<body>
    <form id="form1" runat="server">
      <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" runat="server" id="hdnSeviceValue" />
    <input type="hidden" runat="server" id="hdnPurgeData" />
    <input type="hidden" runat="server" id="hdnPurgeType" />
    <input type="hidden" runat="server" id="hdnPass" />
    <input type="hidden" runat="server" id="hdnDisplayValue" />
    <input type="hidden" runat="server" id="hdnExportValue" />
    <input type="hidden" runat="server" id="hdnActiveIndex" />
    <input type="hidden" runat="server" id="hdnValue" />
    <input type="hidden" runat="server" id="hdnBlock" />
    <input type="hidden" runat="server" id="hdnEmailServiceStatus" />
    <input type="hidden" runat="server" id="hdnReminderServiceStatus" /><%--FB 1926--%>
    <input type="hidden" runat="server" id="hdntformat" /><%--FB 2588--%>
    <input type="hidden" runat="server" id="hdnLblMsg" value="" /> <%--ZD 102727--%>
    <div>
       <table width="80%" align="center" border="0">
         <tr>
            <td>
                <h3><asp:Literal Text="<%$ Resources:WebResources, EventLog_Diagnostics%>" runat="server"></asp:Literal></h3> 
            </td>
         </tr>
         <tr> <td > <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
            <img border='0' src='image/wait1.gif' alt='Loading..' />
         </div><%--ZD 100678--%></td></tr> <%--ZD 100429--%>
         <tr>
              <td align="center">
                  <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
              </td>
          </tr>
          <tr>
              <td>
                <asp:HiddenField ID="activeTab" runat="server" />
                <ajax:TabContainer ID="DiagnosticsTabs" runat="server" OnClientActiveTabChanged="OnChanged">  
                    <ajax:TabPanel ID="EventLogView"  runat="server">
                        <HeaderTemplate><%--ZD 100420--%>
                           <a href="" onclick="this.childNodes[0].click();return false;"><font class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, EventLog_EventLogs%>" runat="server"></asp:Literal></font></a>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" border="0"  width="100%">
                                <tr valign="top">
                                    <td class="blackblodtext" nowrap valign="top" width="10%" >
                                        <table cellspacing="0" cellpadding="5" width="100%">
                                             <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td id="ELogCell" class="cellbackcolor" nowrap ="nowrap" style="display:none"> <%--ZD 100420 ALLBUGS-27--%>
                                                    <asp:LinkButton runat="server" ID="ELog" GroupName="OptWebServer" CssClass="SelectedItem"  Text ="<%$ Resources:WebResources, EventLog_ELog%>"  OnClientClick="Javascript:return fnDisplay('EL')"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="ESearchLogCell" nowrap ="nowrap" style="display:none"> <%--ZD 100420 ALLBUGS-27--%>
                                                    <asp:LinkButton runat="server" ID="ESearchLog" GroupName="OptWebServer" CssClass="SelectedItem" Text="<%$ Resources:WebResources, EventLog_ESearchLog%>"  OnClientClick="Javascript:return fnDisplay('ES')"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="ERetrieveCell" nowrap ="nowrap"> <%--ZD 100420 ALLBUGS-27--%>   
                                                    <asp:LinkButton runat="server" ID="ERetrieve" GroupName="OptWebServer" CssClass="SelectedItem"  Text="<%$ Resources:WebResources, EventLog_ERetrieve%>" OnClientClick="Javascript:return fnDisplay('ER')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <div class="rbroundbox">
	                                        <div class="rbtop">
	                                            <div></div></div>
		                                            <div class="rbcontent" >
                                                        <table cellpadding="0" cellspacing="0" width="90%" align="center">
                                                            <tr id="LogSetRow" style="display:none"> <%--ALLBUGS-27--%>
                                                                <td width="100%" valign=top>
                                                                    <input type="hidden" name="SetLog" value="SetLogPreferences">
                                                                    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">         	
                                                                        <tr>
                                                                            <td width="98%" align=left>
                                                                                <SPAN class=subtitleblueblodtext><asp:Literal Text="<%$ Resources:WebResources, EventLog_LogSettings%>" runat="server"></asp:Literal></SPAN><br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height=200 valign=top align="center">
                                                                                <asp:Table ID="tblLogPref" runat="server" CssClass="tableBody" Width="100%" CellPadding="4" CellSpacing="0">
                                                                                </asp:Table> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align=center>
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                  <td align="center">
                                                                                    <input type="reset" name="SetlogReset" runat="server" value="<%$ Resources:WebResources, Reset%>" class="altMedium0BlueButtonFormat">
                                                                                  </td>
                                                                                  <td width="5%"></td>
                                                                                  <td align="center">
                                                                                      <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:WebResources, EventLog_btnSubmit%>" OnClientClick="javascript:DataLoading(1)" CssClass="altMedium0BlueButtonFormat" /> <%--ZD 100429--%>
                                                                                  </td>
                                                                                </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="SearchLogRow" style="display:none;">
                                                                <td valign="top" width="1000px"> <%--FB 2906 ALLBUGS-27 --%>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="90%">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_SearchLogs%>" runat="server"></asp:Literal></span><br />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                                <table border="0" width="90%" cellspacing="0" cellpadding="8" align="left">  <%--FB 2906--%>
                                                                                    <tr>
                                                                                        <td align="left" valign="center" width="10%"><%--FB 2579 Start--%><%--FB 2906 ALLBUGS-27--%>
                                                                                            <label class="blackblodtext"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, From%>" runat="server"></asp:Literal></label>
                                                                                        </td>
                                                                                        <td valign="center" nowrap="nowrap"><%--FB 2906 ALLBUGS-27--%>
                                                                                            <asp:TextBox ID="startDate" runat="server" CssClass="altText" ValidationGroup="Search"></asp:TextBox>
                                                                                            <%--ZD 100420--%>
                                                                                            <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" alt="Date Selector" style="cursor: pointer;vertical-align:middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"
                                                                                                 onclick="return showCalendar('<%=startDate.ClientID%>', 'cal_trigger1', -1, '<%=format%>');" /></a> <%--ZD 100369--%>
                                                                                            <asp:RequiredFieldValidator ID="startDateRequired" runat="server" ControlToValidate="startDate"
                                                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Size="Smaller" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>								
                                                                                            <br />
                                                                                            
                                                                                            <asp:RegularExpressionValidator ID="startDateRegEx" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidDate%>" Display="Dynamic" Font-Size="Smaller" ControlToValidate="startDate" SetFocusOnError="true" ValidationExpression="^(((((((0?[13578])|(1[02]))[\-/]?((0?[1-9])|([12]\d)|(3[01])))|(((0?[469])|(11))[\-/]?((0?[1-9])|([12]\d)|(30)))|((0?2)[\-/]?((0?[1-9])|(1\d)|(2[0-8]))))[\-/]?(((19)|(20))?([\d][\d]))))|((0?2)[\.\-/]?(29)[\-/]?(((19)|(20))?(([02468][048])|([13579][26])))))$|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" ValidationGroup="Search"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                        <td class="blackblodtext"  width="15%"><asp:Literal Text="<%$ Resources:WebResources, EventLog_Time%>" runat="server"></asp:Literal></td>
                                                                                        <td align="left" valign=center><%--FB 2906--%>
                                                                                            <table CELLSPACING="0" CELLPADDING="0" BORDER="0">
                                                                                                <tr>
	                                                                                                <td valign=center align="left"> <%--FB 2906--%>
		                                                                                                <mbcbb:ComboBox ID="startTimeCombo" runat=server style="width:auto"  CssClass="altText"  
		                                                                                                ValidationGroup="Search"></mbcbb:ComboBox> <%--ZD 100284--%>
                                                                                                        <asp:RequiredFieldValidator ID="startTimeReq" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" ControlToValidate="startTimeCombo:Text" 
                                                                                                        Font-Size="Smaller" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator ID="startTimeRegEx" runat="server" Font-Size="Smaller" ControlToValidate="startTimeCombo:Text" 
                                                                                                        ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" Display="Dynamic" 
                                                                                                        ValidationGroup="Search" SetFocusOnError="true" ></asp:RegularExpressionValidator>
                                                                                                       &nbsp;&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_To%>" runat="server"></asp:Literal></td>
                                                                                        <td valign=center align="left">   <%--FB 2906--%>                                                                         
                                                                                            <asp:TextBox ID="endDate" runat="server" CssClass="altText" ValidationGroup="Search"></asp:TextBox>
                                                                                            <%--ZD 100420--%>
                                                                                            <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger2" alt="Date Selector" style="cursor: pointer;vertical-align:middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"
                                                                                                onclick="return showCalendar('<%=endDate.ClientID%>', 'cal_trigger2', -1, '<%=format%>');" /></a> <%--ZD 100369--%>
                                                                                            <asp:RequiredFieldValidator ID="endDateRequired" runat="server" ControlToValidate="endDate"
                                                                                                Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Size="Smaller" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator></td>								
                                                                                        
                                                                                        <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_Time%>" runat="server"></asp:Literal></td>
                                                                                        <td valign=center align="left"><%--FB 2906--%>
                                                                                            <table CELLSPACING="0" CELLPADDING="0" BORDER="0">
                                                                                                <tr>
        	                                                                                        <td valign=center align="left" style="height: 24px"><%--FB 2906--%>
		                                                                                                <mbcbb:ComboBox ID="endTimeCombo" runat=server  CssClass="altText" style="width:auto" 
		                                                                                                ValidationGroup="Search"><%--Edited For FF--%>
		                                                                                                </mbcbb:ComboBox>
                                                                                                        <asp:RequiredFieldValidator ID="endTimeReq" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" ControlToValidate="endTimeCombo:Text" 
                                                                                                        Font-Size="Smaller" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator ID="endTimeRegEx" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" 
                                                                                                        ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" Font-Size="Smaller" ControlToValidate="endTimeCombo:Text" Display="Dynamic" 
                                                                                                        SetFocusOnError="true" ValidationGroup="Search"></asp:RegularExpressionValidator>
                                                                                                        &nbsp;&nbsp;
	                                                                                                </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, EventLog_Module%>" runat="server"></asp:Literal></td>
                                                                                        <td align="left" valign=center><%--FB 2906--%>
                                                                                            <asp:DropDownList CssClass="altText" ID="moduleType" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td class="blackblodtext" nowrap> <asp:Literal Text="<%$ Resources:WebResources, EventLog_ErrorCode%>" runat="server"></asp:Literal></td>
                                                                                        <td valign=center align=left><%--FB 2906--%>
                                                                                            <asp:TextBox ID="txtErrorCode" runat="server" CssClass="altText"></asp:TextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtErrorCode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>                                                                        
                                                                                        <td valign=center colspan=2 align=left><%--FB 2906--%>
                                                                                            <label class="blackblodtext">  <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, hardwareissuelog_ErrorMessage%>" runat="server"></asp:Literal></label><br>  
                                                                                            <asp:TextBox ID="ErrorMessage" runat="server" MaxLength="4000" Rows="3" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="ErrorMessage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                         <td align=left  valign=center colspan=2 class="blackblodtext"><%--FB 2906--%>
                                                                                            <label class="blackblodtext"> <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, Event%>" runat="server"></asp:Literal></label>
                                                                                            <asp:CustomValidator ID="validateSeverity" runat="server" ErrorMessage="<%$ Resources:WebResources, atleastoneoption%>" Font-Size="Smaller" OnServerValidate="validateSeverity_ServerValidate" Display="Dynamic" SetFocusOnError="true" AutoPostBack="true" ValidationGroup="Search"></asp:CustomValidator>
                                                                                            <table border="0" cellpadding="3" cellspacing="0" width="400px"><%--Edited For FF ZD 101714--%>
                                                                                                 <tr >
                                                                                                    <td class="blackblodtext">
                                                                                                      <asp:CheckBoxList ID="Severity" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" Height="50px" >
                                                                                                        </asp:CheckBoxList>
                                                                                                     </td>			    
                                                                                                 </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                  </table>
                                                                               </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align=center>
                                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                          <td align="center">
                                                                                            <input type="reset" name="HardwareissuelogSubmit1" value="<%$ Resources:WebResources, reset%>" runat="server" class="altMedium0BlueButtonFormat">
                                                                                          </td>
                                                                                          <td width="5%"></td>
                                                                                          <td align="center">
                                                                                              <asp:Button ID="searchSubmit" runat="server" Text="<%$ Resources:WebResources, EventLog_searchSubmit%>" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:DataLoading(1);" OnClick="searchSubmit_Click" ValidationGroup="Search" /> <%--ZD 100429--%>
                                                                                          </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                         </table>
                                                                    </td>
                                                                </tr>
                                                            <tr id="RetrieveLogRow" style="display:none;">
                                                                <td  style="height:150px;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="90%" >
                                                                        <tr>
                                                                            <td width="98%" align="left"><SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_RetrieveLogFi%>" runat="server"></asp:Literal></SPAN></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                             <table border="0" width="90%" cellspacing="0" cellpadding="8" align="right">
                                                                                <tr>
                                                                                    <td align="left" class="blackblodtext" width="700px"> <%--Edited For FF--%>
	                                                                                    <asp:Literal Text="<%$ Resources:WebResources, EventLog_Selectalogty%>" runat="server"></asp:Literal><%--FB 2579 End--%>
                                                                                        <asp:RadioButtonList ID="Servicesnames" runat="server">
                                                                                            <asp:ListItem   Text="<%$ Resources:WebResources, EmailLog%>" Value="0" Selected="True" ></asp:ListItem> <%--ALLBUGS-27--%>
                                                                                            <asp:ListItem  Text="<%$ Resources:WebResources, MCULaunchLog%>" Value="1"></asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--Edited For FF--%>
	                                                                                    <td>                                                                  
                                                                                            <asp:TextBox ID="logDate" runat="server" CssClass="altText"></asp:TextBox>
                                                                                            <%--ZD 100420--%>
                                                                                            <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerLog" alt="Date Selector" style="cursor: pointer;vertical-align:middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"
                                                                                                onclick="return showCalendar('<%=logDate.ClientID%>', 'cal_triggerLog', -1, '<%=format%>');" /></a> <%--ZD 100369--%>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
	                                                                                    <table align="center">
	                                                                                        <tr>
		                                                                                        <td>
			                                                                                        <input type="reset" name="HardwareissuelogSubmit2" runat="server" value="<%$ Resources:WebResources, Reset%>" class="altMedium0BlueButtonFormat">
		                                                                                        </td>
		                                                                                        <td align="center">
                                                                                                    <asp:Button ID="logRetSubmit" runat="server" Text="<%$ Resources:WebResources, EventLog_logRetSubmit%>" CssClass="altMedium0BlueButtonFormat"
                                                                                                    OnClientClick="javascript:DataLoading(1);setFormSubmitToFalse();" OnClick="logRetSubmit_Click"/> <%--ZD 100429 //ALLBUGS-27--%>
			                                                                                    </td>
	                                                                                        </tr>
	                                                                                    </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                </td>
                                                            </tr>	
                                                        </table>
                                                    </div>
	                                             <div class="rbbot">
	                                        <div></div></div>
                                       </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                     </ajax:TabPanel>
                    <ajax:TabPanel ID="WebServerView" runat="server">   
                        <HeaderTemplate><%--ZD 100420--%>
                            <a href="" onclick="this.childNodes[0].click();return false;"><font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_WebServer%>" runat="server"></asp:Literal></font></a>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td valign="top" style="width:10%;">
                                        <table cellspacing="0" cellpadding="5">
                                            <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="WSysInfoCell" class="cellbackcolor" > <%--ZD 100420--%>
                                                    <asp:LinkButton ID="WSysInfo" Text="<%$ Resources:WebResources, EventLog_WSysInfo%>" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('WS')" ></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="WAppLogInfoCell"> <%--ZD 100420--%>
                                                    <asp:LinkButton ID="WAppLogInfo" Text="<%$ Resources:WebResources, EventLog_WAppLogInfo%>" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('WA')"></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap ="nowrap" id="DmyVRMLogInfoCell" style="display:none" > <%--ZD 100420 ALLBUGS-27--%>
                                                    <asp:LinkButton ID="DmyVRMLogInfo" Text="<%$ Resources:WebResources, EventLog_DmyVRMLogInfo%>" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('MA')"></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="WmyVRMassemblyInfoCell" > <%--ZD 100420--%>
                                                    <asp:LinkButton ID="WmyVRMassemblyInfo" Text="<%$ Resources:WebResources, EventLog_WmyVRMassemblyInfo%>" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('AI')"></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                       </table>
                                    </td>
                                    <td valign="top">
                                       <div class="rbroundbox">
	                                        <div class="rbtop"><div></div></div>
		                                        <div class="rbcontent" >
                                                    <table width="100%" border="0">
                                                        <tr id="WebLogRow1"  style="display:none;">
                                                            <td>
                                                                <table width="600px"><%--Edited For FF--%>
                                                                    <tr >
                                                                        <td class="subtitleblueblodtext" align="center" nowrap><%--Edited For FF--%>                    
                                                                            <asp:Literal Text="<%$ Resources:WebResources, EventLog_ApplicationLog%>" runat="server"></asp:Literal>
                                                                           <asp:Button ID="ApExport" runat="server" Text="<%$ Resources:WebResources, EventLog_ApExport%>" OnClientClick="javascript:DataLoading(1);fnListenDownload();document.getElementById('hdnExportValue').value='2'" CssClass="altMedium0BlueButtonFormat" /> <%--ZD 100429  ZD 102491--%>
                                                                            <asp:Button ID="ApExportAll" runat="server" Text="<%$ Resources:WebResources, EventLog_ApExportAll%>" OnClientClick="javascript:DataLoading(1);fnListenDownload();document.getElementById('hdnExportValue').value='1'" CssClass="altMedium0BlueButtonFormat" Width="200px"/> <%--ZD 100429  ZD 102491--%>
                                                                        </td>       
                                                                        
                                                                    </tr>
                                                                </table>
                                                             </td>      
                                                        </tr>          
                                                        <tr id="WebLogRow2" style="display:none;">
                                                            <td>
                                                                <asp:DataGrid id="LogGrid" Width="100%" runat="server" AllowPaging="True" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
                                                                PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" HeaderStyle-HorizontalAlign="Center"
                                                                GridLines="Both" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                      <asp:BoundColumn HeaderText="<%$ Resources:WebResources, ManageConference_tdType%>" ItemStyle-Width="12%" />
                                                                      <asp:BoundColumn HeaderText="<%$ Resources:WebResources, Description%>" />
                                                                      <asp:BoundColumn HeaderText="<%$ Resources:WebResources, ResponseConference_DateTime%>" ItemStyle-Width="15%"/>
                                                                      <asp:BoundColumn HeaderText="<%$ Resources:WebResources, Source%>" ItemStyle-Width="10%"/>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                        <tr id="WebSysInfoRow1" style="display:block;" align="center"><%--Edited For FF--%>
                                                            <td style="width:100%;" class="subtitleblueblodtext" align="center"><asp:Literal Text="<%$ Resources:WebResources, EventLog_SystemInformat%>" runat="server"></asp:Literal></td>
                                                        </tr>
                                                        <tr id="WebSysInfoRow2" style="display:block;">
                                                            <td style="width:100%;">
                                                                <asp:Table ID="SystemInfoTable" runat="server" Width="700px" ></asp:Table><%--Edited For FF--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="myVRMWebLogRow1"  style="display:None;">
                                                            <td class="subtitleblueblodtext" >                    
                                                                <table width="700px" border="0"><%--Edited For FF--%>
                                                                    <tr >
                                                                        <td class="subtitleblueblodtext" >                    
                                                                            <asp:Literal Text="<%$ Resources:WebResources, EventLog_myVRMAlertsLo%>" runat="server"></asp:Literal>
                                                                        </td>       
                                                                        <td align="right"  nowrap>
                                                                            <asp:Button ID="AlExport" runat="server" Text="<%$ Resources:WebResources, EventLog_AlExport%>" OnClientClick="javascript:DataLoading(1);document.getElementById('hdnExportValue').value='3'" CssClass="altMedium0BlueButtonFormat" /><%--ZD 100429--%>
                                                                            <asp:Button ID="AlExportAll" runat="server" Text="<%$ Resources:WebResources, EventLog_AlExportAll%>" OnClientClick="javascript:DataLoading(1);document.getElementById('hdnExportValue').value='1'" CssClass="altMedium0BlueButtonFormat" Width="200px" /><%--ZD 100429--%>
                                                                        </td> 
                                                                    </tr>
                                                                </table>
                                                            </td>                
                                                        </tr>          
                                                        <tr id="myVRMWebLogRow2"  style="display:None;">
                                                            <td >
                                                                <asp:DataGrid id="myVRMLogGrid" runat="server" AllowPaging="True" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
                                                                PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="black" BorderWidth="1" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                GridLines="Both" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                      <asp:BoundColumn HeaderText="Type" ItemStyle-Width="12%" />
                                                                      <asp:BoundColumn HeaderText="Description" ItemStyle-Width="50%" />
                                                                      <asp:BoundColumn HeaderText="Date/Time" ItemStyle-Width="15%"/>
                                                                      <asp:BoundColumn HeaderText="Source" ItemStyle-Width="10%"/>                                                                      
                                                                    </Columns>                                                                                                                                                        
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                        <tr id="myVRMassemblyRow1" style="display:None;">
                                                            <td class="subtitleblueblodtext" align="center" width="700px"><asp:Literal Text="<%$ Resources:WebResources, EventLog_myVRMAssemblie%>" runat="server"></asp:Literal></td>
                                                        </tr>
                                                        <tr id="myVRMassemblyRow2" style="display:None;">
                                                            <td width="700px"><%--Edited For FF--%>
                                                                <asp:Table ID="MyVRMassemblyTable" runat="server" Width="80%" HorizontalAlign="Center" ></asp:Table>
                                                            </td>
                                                        </tr>
                                                        <tr id="ComponentsRow1" style="display:None;">
                                                            <td class="subtitleblueblodtext" align="center" width="700px"><asp:Literal Text="<%$ Resources:WebResources, EventLog_Components%>" runat="server"></asp:Literal></td>
                                                        </tr>
                                                        <tr id="ComponentsRow2" style="display:None;">
                                                            <td width="700px"><%--Edited For FF--%>
                                                                <asp:Table ID="ComponentTable" runat="server" Width="80%"  HorizontalAlign="Center"></asp:Table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
	                                        <div class="rbbot"><div></div></div>
                                       </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel ID="DataServerView" runat="server">
                        <HeaderTemplate> <%--ZD 100420--%>
                           <a href="" onclick="this.childNodes[0].click();return false;"><font class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, EventLog_DatabaseServer%>" runat="server"></asp:Literal> </font></a>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0">
                                <tr >
                                    <td class="blackblodtext" nowrap valign="top" width="10%" >
                                        <table cellspacing="0" cellpadding="5">
                                             <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td id="DSysInfoCell" class="cellbackcolor" nowrap> <%--ZD 100420--%>
                                                    <asp:LinkButton runat="server" ID="DSysInfo" GroupName="OptWebServer" CssClass="SelectedItem"  Text ="<%$ Resources:WebResources, EventLog_DSysInfo%>"  OnClientClick="Javascript:return fnDisplay('DS')"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="DPurgeCell" nowrap> <%--ZD 100420--%>
                                                    <asp:LinkButton runat="server" ID="DPurge" GroupName="OptWebServer" CssClass="SelectedItem" Text="<%$ Resources:WebResources, EventLog_DPurge%>"  OnClientClick="Javascript:return fnDisplay('DP')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="DAppLogInfoCell" nowrap> <%--ZD 100420--%>
                                                    <asp:LinkButton runat="server" ID="DAppLogInfo" GroupName="OptWebServer" CssClass="SelectedItem" Visible="false" Text="<%$ Resources:WebResources, EventLog_DAppLogInfo%>" OnClientClick="Javascript:return fnDisplay('DA')"></asp:LinkButton> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td  valign=top>
                                        <div class="rbroundbox">
	                                        <div class="rbtop"><div></div></div>
		                                        <div class="rbcontent" >
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr id="DataPurgeRow" style="display:none;">
                                                            <td width="50px"></td>
                                                            <td>
                                                                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="subtitleblueblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, EventLog_WhattoPurge%>" runat="server"></asp:Literal></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="blackblodtext">
                                                                            <asp:RadioButton ID="DCompletedConf" runat="server" GroupName="DeleteType" Text="<%$ Resources:WebResources, EventLog_DCompletedConf%>" onclick="javascript:return fnDisplay('PC')"  /> 
                                                                            <asp:RadioButton ID="DAllConf" runat="server" GroupName="DeleteType" Text="<%$ Resources:WebResources, EventLog_DAllConf%>" onclick="javascript:return fnDisplay('PA')" /> 
                                                                            <asp:RadioButton ID="DAllData" runat="server" GroupName="DeleteType" Text="<%$ Resources:WebResources, EventLog_DAllData%>" Visible="true" onclick="javascript:return fnDisplay('PD')" /> 
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td class="subtitleblueblodtext" valign="baseline" align="left">
                                                                                        <asp:Button ID="PurgeNow" Visible="false" runat="server" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return fnConfirm('N');" Text="<%$ Resources:WebResources, EventLog_PurgeNow%>" />
                                                                                        <asp:Button ID="PurgeNowDummy" runat="server" style="display:none;"  />
                                                                                        <ajax:ModalPopupExtender ID="PurgePopUp" runat="server" TargetControlID="PurgeNowDummy" PopupControlID="BlockDiv" CancelControlID="Close"  DropShadow="false" Drag="true" OnCancelScript = "javascript: document.getElementById('DiagnosticsTabs_DataServerView_txtPwd').value=''" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
                                                                                        <div id="BlockDiv" class="modalBackground"> 
                                                                                        <div class="rbroundbox">
                                                                                    <div ><div></div></div>
                                                                                        <div class="rbcontent" >
                                                                                            <table cellpadding="8" cellspacing="1">
                                                                                                <tr>
                                                                                                    <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_Pleaseenteryo%>" runat="server"></asp:Literal></td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="txtPwd" runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center" colspan="2">
                                                                                                        <asp:Button ID="PurgeNowInDiv" runat="server" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:return fnCheck();" Text="<%$ Resources:WebResources, EventLog_PurgeNowInDiv%>" />
                                                                                                        <asp:Button ID="Close" Text="<%$ Resources:WebResources, EventLog_Close%>" runat="server" OnClientClick="javascript: document.getElementById('DiagnosticsTabs_DataServerView_txtPwd').value=''" CssClass="altShortBlueButtonFormat" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>  
                                                                                              </div>
                                                                                        <div ><div></div></div>
                                                                                      </div> 
                                                                                        </div>
                                                                                    </td>
                                                                                 </tr>
                                                                            </table>
                                                                        </td>          
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="left">
                                                                            <hr style="width:85%;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="subtitleblueblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, EventLog_WhentoPurge%>" runat="server"></asp:Literal></td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow1" runat="server">
                                                                        <td class="blackblodtext" colspan="2">
                                                                            <asp:Literal Text="<%$ Resources:WebResources, EventLog_Scheduled%>" runat="server"></asp:Literal>&nbsp;&nbsp;
                                                                            <asp:RadioButton ID="RemoveType" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('N')" /> <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, None%>" runat="server"></asp:Literal>
                                                                            <asp:RadioButton ID="RNow" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('R')" /> <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Now%>" runat="server"></asp:Literal>
                                                                            <asp:RadioButton ID="Daily" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('D')" /> <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, Daily%>" runat="server"></asp:Literal>
                                                                            <asp:RadioButton ID="Weekly" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('W')" /> <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Weekly%>" runat="server"></asp:Literal>
                                                                            <asp:RadioButton ID="Monthly" runat="server" GroupName="ScheduleType" onclick="javascript:return fnShow('M')" /> <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, Monthly%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow2" runat="server">
                                                                        <td></td>
                                                                        <td>
                                                                            <br />
                                                                            <asp:Label ID="lblText" runat="server" CssClass="lblError"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow3" runat="server">
                                                                        <td></td>
                                                                        <td><br />
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:Button runat="server" ID="PurgeRegular"  OnClientClick="javascript:return fnConfirm();" 
                                                                            Width="100pt"  Text="<%$ Resources:WebResources, EventLog_PurgeRegular%>" /> <%--FB 2796--%> <%--ZD 100429--%><%--ALLBUGS-27 Removed DataLoading(1)--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>&nbsp;<br /></td>
                                                                    </tr>
                                                                    <tr id="PScheduleRow4" runat="server">
                                                                        <td></td>
                                                                        <td class="blackblodtext">
                                                                            <asp:Literal Text="<%$ Resources:WebResources, EventLog_LastUpdatedon%>" runat="server"></asp:Literal>&nbsp;&nbsp;
                                                                            <asp:Label ID="lblUpdateOn" runat="server" CssClass="blackblodtext"></asp:Label>
                                                                         </td>
                                                                     </tr>
                                                                </table>                                            
                                                            </td>
                                                        </tr>
                                                        <tr id="DBInfoRow" >
                                                            <td colspan="2" >
                                                                <table width="100%" border="0">
                                                                    <tr>
                                                                        <td class="subtitleblueblodtext" align="center"><asp:Literal Text="<%$ Resources:WebResources, EventLog_DatabaseServerInfo%>" runat="server"></asp:Literal></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td >
                                                                            <asp:Table ID="SQLServerInfoTable" runat="server" Width="700px" CellSpacing="1" CellPadding="3" ></asp:Table><%--Edited For FF--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                 </div>
	                                        <div class="rbbot"><div></div></div>
                                       </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel ID="ServiceView" runat="server">
                        <HeaderTemplate> <%--ZD 100420--%>
                            <a href="" onclick="this.childNodes[0].click();return false;"><font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EventLog_Services%>" runat="server"></asp:Literal></font> </a>
                        </HeaderTemplate>                        
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" style="width:10%;">
                                        <table cellspacing="0" cellpadding="5" width="100%">
                                            <tr>
                                                <td style="height:20px;"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap id="SStatusCell" class="cellbackcolor" > <%--ZD 100420--%>
                                                    <asp:LinkButton ID="SStatus" Text="<%$ Resources:WebResources, EventLog_SStatus%>" runat="server" CssClass="SelectedItem" OnClientClick="Javascript:return fnDisplay('WS')" ></asp:LinkButton> <br />
                                                </td>
                                            </tr>
                                       </table>
                                    </td>
                                    <td valign="top" >
                                       <div class="rbroundbox">
                                        <div class="rbtop"><div></div></div>
		                                    <div class="rbcontent">
                                                <table cellpadding="5" cellspacing="0" width="100%" align="center"  border="0" style="height:130px;">
                                                    <tr><td height="25px" colspan="2"></td></tr>
                                                    <tr>
                                                        <td class="blackblodtext" style="width:50%;" align="right" nowrap>
                                                           <asp:Literal Text="<%$ Resources:WebResources, EventLog_MCULaunchStatu%>" runat="server"></asp:Literal>
                                                        </td>
                                                        <td  >
                                                            <asp:Label runat="server" ID="ServiceStatus" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr><td height="25px" colspan="2"></td></tr>
                                                    <tr>
                                                        <td class="blackblodtext" style="width:50%;" align="right" nowrap>
                                                           <asp:Literal Text="<%$ Resources:WebResources, EventLog_EmailStatus%>" runat="server"></asp:Literal>
                                                        </td>
                                                        <td  >
                                                            <asp:Label runat="server" ID="EmailServiceStatus" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr><%--FB 1926--%>
                                                        <td class="blackblodtext" style="width:50%;" align="right" nowrap>
                                                           <asp:Literal Text="<%$ Resources:WebResources, EventLog_ReminderStatus%>" runat="server"></asp:Literal>
                                                        </td>
                                                        <td  >
                                                            <asp:Label runat="server" ID="ReminderServiceStatus" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <table cellspacing="5" cellpadding ="8">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="Start" Visible =false ToolTip="Start Service" Width="40px" Height="33px" OnClientClick="javascript: document.getElementById('hdnSeviceValue').value='1'" runat="server" ImageUrl="~/en/image/Start.jpg"  AlternateText="Start" /> <%--ZD 100419--%>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="Pause" Visible =false ToolTip="Pause Service" Width="40px"  Height="33px" runat="server" OnClientClick="javascript: document.getElementById('hdnSeviceValue').value='2'" ImageUrl="~/en/image/Pause.jpg"  AlternateText ="Pause"/> <%--ZD 100419--%>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="Stop" Visible =false ToolTip="Stop Service" Width="40px" Height="33px" runat="server" ImageUrl="~/en/image/Stop.jpg" OnClientClick="javascript: document.getElementById('hdnSeviceValue').value='3'" AlternateText="Stop"/> <%--ZD 100419--%>
                                                                    </td>
                                                               </tr>
                                                           </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
	                                        <div class="rbbot"><div></div></div>
                                       </div>
                                     </td>
                                   </tr>
                                </table>
                        </ContentTemplate>
                    </ajax:TabPanel>
                 </ajax:TabContainer>
             </td>
          </tr>
          <tr>
            <td>
                 <asp:Button ID="ServerInfo" runat="server" OnClick="BindSQLServerInfoTable"  style="display:none" />                
            </td>
          </tr>
      </table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript" src="inc/softedge.js"></script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript">
    fnMaintainDesign();
    
    //ZD 100284 - Start
    if (document.getElementById("DiagnosticsTabs_EventLogView_startTimeCombo_Text")) {
        var confstarttime_text = document.getElementById("DiagnosticsTabs_EventLogView_startTimeCombo_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('DiagnosticsTabs_EventLogView_startTimeCombo_Text', 'DiagnosticsTabs_EventLogView_startTimeRegEx',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("DiagnosticsTabs_EventLogView_endTimeCombo_Text")) {
        var confstarttime_text = document.getElementById("DiagnosticsTabs_EventLogView_endTimeCombo_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('DiagnosticsTabs_EventLogView_endTimeCombo_Text', 'DiagnosticsTabs_EventLogView_endTimeRegEx',"<%=Session["timeFormat"]%>")
        };
    }

    $("label").removeAttr("for"); //ZD 102493

    // ZD 102491 Start
    function fnListenDownload()
    {
        var cook = getCookie("downloadCookie");
        if(cook == "1")
        {
            document.cookie = "downloadCookie=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
            DataLoading(0);
        }
        else
            setTimeout("fnListenDownload()",100);
    }

    function Trim(x)
    {
        return x.replace(/^\s+|\s+$/gm,'');
    }

    function getCookie(cname)
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) 
            {
            var c = Trim(ca[i]);
            if (c.indexOf(name)==0) return c.substring(name.length,c.length);
            }
        return "";
    }
    // ZD 102491 End

    //ALLBUGS-27
   function setFormSubmitToFalse() {
    setTimeout(function () { DataLoading(0); }, 3000);
    return true;
    }

</script>