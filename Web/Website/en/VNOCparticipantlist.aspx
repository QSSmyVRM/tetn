﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_VNOCparticipantlist" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--ZD 101028 --%>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<style type="text/css">
    #grid_DXMainTable td:
    {
        color: Red;
    }
</style>

<script type="text/javascript">
    function grid_SelectionChanged(s, e) {
        s.GetSelectedFieldValues("ifrmDetails", GetSelectedFieldValuesCallback);
    }

    function GetSelectedFieldValuesCallback(values) {
        var names = "";
        var ids = "";
        var rnames = "";
        for (var i = 0; i < values.length; i++) {
            var arr = values[i].split('|');
            var arr2 = arr[1].split(' ');
            names += arr2[0] + " " + arr[2] + "\n";
            if (i == 0) {
                ids = arr[0];
                rnames = arr2[0] + " " + arr[2];
            }
            else {
                ids += "," + arr[0];
                rnames += ", " + arr2[0] + " " + arr[2];
            }

        }
        if (window.opener.document.getElementById("txtVNOCOperator") != null)
            window.opener.document.getElementById("txtVNOCOperator").value = names;
        if (window.opener.document.getElementById("hdnVNOCOperator") != null)
            window.opener.document.getElementById("hdnVNOCOperator").value = ids;
        if (window.opener.document.getElementById("txtRVNOCOperator") != null)
            window.opener.document.getElementById("txtRVNOCOperator").value = rnames;
    }


</script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428 END--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>VNOC Participants</title>
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                <b><font class="subtitleblueblodtext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, VNOCparticipantlist_VNOCOperatorA%>" runat="server"></asp:Literal></font></b><br /><br /><%--FB 2765--%>     
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="hidden" id="partysValue" name="partysValue" value="" />
                    <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="100%"
                        CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <dxwgv:ASPxGridView AllowSort="true" ID="grid" ClientInstanceName="grid" runat="server"
                                        KeyFieldName="userID" Width="100%" OnHtmlDataCellPrepared="HtmlDataCellPrepared"
                                        EnableRowsCache="True" OnDataBound="ASPxGridView1_DataBound">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Caption="<%$ Resources:WebResources, Select%>"> <%--ZD 100422--%>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="firstName" Caption="<%$ Resources:WebResources, FirstName%>" VisibleIndex="1"
                                                HeaderStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataTextColumn FieldName="lastName" Caption="<%$ Resources:WebResources, LastName%>" VisibleIndex="2"
                                                HeaderStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataTextColumn FieldName="login" Caption="<%$ Resources:WebResources, EmailSearch_LoginName%>" VisibleIndex="3"
                                                HeaderStyle-HorizontalAlign="Center" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="userEmail" Caption="<%$ Resources:WebResources, Email%>" VisibleIndex="5"
                                                HeaderStyle-HorizontalAlign="Center" Width="32%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="orgName" Caption="<%$ Resources:WebResources, ConferenceList_tdOrgName%>" VisibleIndex="6"
                                                HeaderStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataColumn FieldName="userID" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="ifrmDetails" Visible="False" />
                                        </Columns>
                                        <Styles>
                                            <CommandColumn Paddings-Padding="1" />
                                        </Styles>                                        
                                        <SettingsBehavior AllowMultiSelection="false" />
                                        <Settings ShowFilterRowMenu="true" />
                                        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true" Position="Bottom" >
                                        </SettingsPager>
                                        <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
                                    </dxwgv:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input align="middle" type="button" runat="server" style="width: 100px; cursor: pointer"
                                        id="ClosePUp" value=" <%$ Resources:WebResources, Close%> " onclick="javascript:window.close();" class="altMedium0BlueButtonFormat" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: Red;" align="left" colspan="2">
                                    <asp:Label ID="lblVNOC" runat="server">
                                    <b>(*)</b> <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SuperTenant%>" runat="server"></asp:Literal>
                                    </asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
