/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/

/* This notice must be untouched at all times.

dragdrop.js	v. 3.47
The latest version is available at
http://www.walterzorn.com
or http://www.devira.com
or http://www.walterzorn.de

Copyright (c) 2002 Walter Zorn. All rights reserved.
Created 26. 8. 2002 by Walter Zorn <walter@kreuzotter.de>
Last modified: 24. 2. 2003

This Drag & Drop Library adds Drag & drop functionality
to the following types of html-elements,
provided their names/IDs are passed to SET_DRAGGABLE():
- images, even if not positioned via layers,
  nor via stylesheets or any other kind of "hard-coding"
- absolutely positioned layers.

This program is free software;
you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation;
either version 2 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License
at http://www.gnu.org/copyleft/gpl.html for more details.
*/



function ERR(e)
{
	return true;
}
//window.onerror=ERR;


function DDClass()
{
	this.db = (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body? document.body : null;
	this.n = navigator.userAgent.toLowerCase();
	this.op = (window.opera && document.getElementById);
	this.op6 = (this.op && !(this.db && this.db.innerHTML));
	if (this.op && !this.op6) document.onmousedown = new Function('e',
		'if (((e = e || window.event).target || e.srcElement).tagName == "IMAGE") return false;'
	);
	this.ie = this.n.indexOf("msie")!=-1 && document.all && this.db && !this.op;
	this.iemac = this.ie && this.n.indexOf("mac")!=-1;
	this.ie4 = this.ie && !document.getElementById;
	this.n4 = document.layers? true : false;
	this.kq = (this.kqi = this.n.indexOf("konqueror"))>-1 && parseInt(this.n.substring(this.kqi+0x0a))>0x02;
		this.kq_new = this.kq && this.n.substring(this.kqi+0x0a, this.kqi+0x0d)!="3.0";
	this.n6 = !this.op && !this.kq && !this.ie && document.defaultView && typeof document.defaultView.getComputedStyle != "undefined";
	this.n7 = this.n6 && parseInt(this.n.substring((this.n6i = this.n.indexOf('gecko'))+6, this.n6i+0x0c))>200209;
	this.ce = document.captureEvents && document.releaseEvents;
	this.elements = new Array();
	if (this.n4)
	{
		this.dmmy = new Image();
		this.dmmy.src = self.location.href;
	}
	// Added by Xiaowan
	this.rx = this.ry = this.rw = this.rh = -1;
	this.ix = this.iy = -1;
	this.dragFrom = 0;	// 0 - out of region; 1 - in region
	this.dragTo = 0;
	this.dragAllowed = false;
	// End of addition
}
var dd = new DDClass();

dd.Int = function(x, y)
{
	return isNaN(y = parseInt(x))? 0 : y;
}


dd.setEvtHdl = function(i)
{
	if (dd.op6) WINSZ(0x02);
	if (dd.kq) setTimeout("dd.recalc(true)", 0x32); // || dd.n6 has been taken out to make it compatible with Firefox by - Saima
	if (dd.loadFunc) dd.loadFunc();
	dd.downFunc = document.onmousedown || null;
	dd.moveFunc = document.onmousemove || null;
	dd.upFunc = document.onmouseup || null;
	if (dd.ce) document.captureEvents(Event.MOUSEDOWN);
	document.onmousedown = PICK;
}


dd.evt = function(e)
{
	this.but = (e = e || window.event).which || e.button;
	this.src = e.target || e.srcElement;
	this.x = dd.Int(e.pageX || e.clientX || 0)+dd.Int((dd.ie || dd.kq_new)? dd.db.scrollLeft : 0);
	this.y = dd.Int(e.pageY || e.clientY || 0)+dd.Int((dd.ie || dd.kq_new)? dd.db.scrollTop : 0);
}


dd.recalc = function(x)
{
	var i, z = 0; while (z<dd.elements.length)
	{
		if ((i = dd.elements[z++]).img)
		{
			if (x || Math.abs(i.x-i.defx)<=0x0f && Math.abs(i.y-i.defy)<=0x0f)
			{
				i.getImg(i.img.name);
				if (dd.n6) i.resizeTo(i.defw = i.getImgW(), i.defh = i.getImgH());
				i.moveTo(i.defx, i.defy);
			}
			else i.getImg(i.img.name);
		}
		else i.getWH();
	}
}





// Optional commands on the html-page:
var NO_DRAG			= 'N0d4Ag';
var NODRAG			= NO_DRAG;
var RESET_Z			= 'r35E7z';
var NO_ALT			= 'no81T';
var NOALT			= NO_ALT;
var DETACH_CHILDREN = 'd37aCH';
var CLONE			= 'C10nE';
var MULTIPLY		= 'MUl71';
for (var dd_z = 0; dd_z<0x97; dd_z++)
{
	eval('var MULTIPLY' + dd_z + ' = "' + MULTIPLY + '' + dd_z + '";');
}

var CURSOR_DEFAULT  = '',
CURSOR_MOVE			= 'c:move',
CURSOR_CROSSHAIR	= 'c:crosshair',
CURSOR_POINTER		= (dd.ie?'hand':'c:pointer'),
CURSOR_E_RESIZE		= 'c:e-resize',
CURSOR_NE_RESIZE	= 'c:ne-resize',
CURSOR_NW_RESIZE	= 'c:nw-resize',
CURSOR_N_RESIZE		= 'c:n-resize',
CURSOR_SE_RESIZE	= 'c:se-resize',
CURSOR_SW_RESIZE	= 'c:sw-resize',
CURSOR_S_RESIZE		= 'c:s-resize',
CURSOR_W_RESIZE		= 'c:w-resize',
CURSOR_WAIT			= 'c:wait',
CURSOR_HELP			= 'c:help';





function DDObj(i, j)
{
	this.moveTo = function(x, y, i)
	{
		if (this.div)
		{
			(i = this.css || this.div).left = (x = dd.Int(x)) + this.px;
			i.top = (y = dd.Int(y)) + this.px;
			if (this.children)
				for (var z = 0; z<this.children.length; z++)
					(i = this.children[z]).moveTo(i.x + x - this.x, i.y + y - this.y);
			this.x = x;
			this.y = y;
		}
	}
	this.id = i;
	this.nodrag = this.getCmd(NODRAG);
	this.re_z = this.getCmd(RESET_Z);
	this.noalt = this.getCmd(NOALT);
	this.detach = this.getCmd(DETACH_CHILDREN);
	this.cln = this.getCmd(CLONE);
	this.multi = (i.indexOf(MULTIPLY)>-1)? dd.Int(i.substring(i.indexOf(MULTIPLY)+MULTIPLY.length)) : 0;
	this.getCmd(MULTIPLY);

	this.name = this.id + (j||'');
	this.px = (dd.op6 || dd.n4)? '' : 'px';
	this.img = this.getImg(this.id);
	if (this.img)
	{
		this.id += 'div' + (j||'');
		this.w = this.getImgW();
		this.h = this.getImgH();
		this.cach = new Image();
		this.cach.src = this.img.src;
		this.htm = '<div id="' + this.id +
			'" style="position:absolute;'+
			'left:' + this.defx + 'px;'+
			'top:' + this.defy + 'px;'+
			'width:' + this.w + 'px;'+
			'height:' + this.h + 'px;">'+
			'<img name="' + (this.id+'img') + '"'+
			' src="' + this.img.src + '" '+
			'alt="' + ((dd.noalt || this.noalt)? '' : (this.img.alt || '')) + '" '+
			'width="' + this.w + '" height="' + this.h + '"><\/div>\n';
	}
	else
	{
		this.div = this.getDiv(this.id);
		if (!this.div) return;
		if (this.div.style)	this.css = this.div.style;
		this.getDefXY(this.div);
		this.getWH();
	}
	this.defw = this.w;
	this.defh = this.h;
	this.resetXY();
	// Added by 
	this.rid = -1;
	// End
}


DDObj.prototype.hide = function()
{
	if (!this.div) return;
	(this.css || this.div).visibility = "hidden";
	this.visible = false;
	DROP();
	if (this.children)
		for (var z = 0; z<this.children.length; z++) this.children[z].hide();
}


DDObj.prototype.show = function()
{
	if (!this.div) return;
	(this.css ||  this.div).visibility = "visible";
	this.visible = true;
	if (this.children)
		for (var z = 0; z<this.children.length; z++) this.children[z].show();
}


DDObj.prototype.resizeTo = function(w, h, i)
{
	if (!this.div) return;
	w = dd.Int(w);
	h = dd.Int(h);
	if (dd.n4)
	{
		this.div.resizeTo(w, h);
		if (this.img)
		{
			(i = this.div.document).open();
			i.write('<img src="' + this.cach.src + '" width="' + w + '" height="' + h + '">');
			i.close();
			i.images[0].src = this.cach.src;
		}
	}
	else if (typeof this.css.pixelWidth != "undefined")
	{
		this.css.pixelWidth = w;
		this.css.pixelHeight = h;
		if (this.img)
		{
			(i = document.images[this.id+'img'].style).pixelWidth = w;
			i.pixelHeight = h;
		}
	}
	else
	{
		this.css.width = w + this.px;
		this.css.height = h + this.px;
		if (this.img)
		{
			(i = document.images[this.id+'img']).width = w;
			i.height = h;
			if (!i.complete) i.src = this.cach.src;
		}
	}
	if (this.css) this.css.overflow = "hidden";
	this.getWH();
}

// Added by Xiaowan
DDObj.prototype.inRegion = function()
{
	if (this.x<dd.rx || this.y<dd.ry || this.x+this.w > dd.rx+dd.rw || this.y+this.h > dd.ry+dd.rh) 
		return false;
	else 
		return true;
}
// End of addition


DDObj.prototype.swapImage = function(x, cp)
{
	if (this.inRegion() && this.name != "roomBgImg" ) return;	// Added by Xiaowan
	if (!(this.img && this.div)) return;
	if (this.cach.src != x) this.cach.src = x;
	if (dd.n4) this.div.document.images[0].src = this.cach.src;
	else document.images[this.id+'img'].src = this.cach.src;
	if (cp && this.copies)
		for (var z = 0; z<this.copies.length; z++) this.copies[z].swapImage(x);
}


DDObj.prototype.activate = function()
{
	var i, dz = dd.z-this.z;
	if (this.children)
		for (var z = 0; z<this.children.length; z++)
			(i = this.children[z]).setZ(i.z+dz+1);
	this.setZ(this.z+dz+1);
}


DDObj.prototype.deactivate = function(i)
{
	if (this.re_z)
	{
		this.setZ(this.defz);
		if (this.children)
			for (var z = 0; z<this.children.length; z++) (i = this.children[z]).setZ(i.defz);
	}
}


DDObj.prototype.getCmd = function(cmd)
{
	var i = this.id.indexOf(cmd),
	y = (i>-1);
	if (y) this.id = this.id.substring(0, i) + ((cmd==MULTIPLY)? "" : this.id.substring(i + cmd.length));
	return y;
}


DDObj.prototype.getDiv = function(i)
{
	if (dd.n4) return document[i] || null;
	if (dd.ie) return document.all[i] || null;
	else return document.getElementById(i) || null;
}


DDObj.prototype.getImg = function(i, d)
{
	this.defx = 0;
	this.defy = 0;
	d = d || window.document;
	if (d.layers)
	{
		if (d.images[i])
		{
			this.defx += d.images[i].x;
			this.defy += d.images[i].y;
			return d.images[i];
		}
		for (var z = 0; z<d.layers.length; z++)
		{
			var y = this.getImg(i, d.layers[z].document);
			if (y)
			{
				this.getDefXY(d.layers[z]);
				return y;
			}
		}
		return null;
	}
	else
	{
		var x, y = ((x = d.images[i]) && x.name==i)? x : null;
		if (y) this.getDefXY(y);
		return y;
	}
}


DDObj.prototype.getImgW = function()
{
	return dd.Int(
		this.img.width || (dd.n6? document.defaultView.getComputedStyle(this.img, "").getPropertyValue("width") : 0)
	);
}


DDObj.prototype.getImgH = function()
{
	return dd.Int(
		this.img.height || (dd.n6? document.defaultView.getComputedStyle(this.img, "").getPropertyValue("height") : 0)
	);
}


DDObj.prototype.getDefXY = function(i)
{
	if (dd.n4)
	{
		this.defx += i.left;
		this.defy += i.top;
	}
	else while (i)
	{
		this.defx += dd.Int(i.offsetLeft);
		this.defy += dd.Int(i.offsetTop);
		i = i.offsetParent;
	}
	if (dd.iemac) this.defy += dd.Int(dd.db.currentStyle.marginTop) + dd.Int(dd.db.currentStyle.paddingTop);
}


DDObj.prototype.getWH = function()
{
	this.w = dd.Int(
		dd.n4? this.div.clip.width
		: this.css.pixelWidth? this.css.pixelWidth
		: this.div.offsetWidth
	);
	this.h = dd.Int(
		dd.n4? this.div.clip.height
		: this.css.pixelHeight? this.css.pixelHeight
		: this.div.offsetHeight
	);
}


DDObj.prototype.resetXY = function()
{
	this.x = this.defx;
	this.y = this.defy;
}


DDObj.prototype.setZ = function(x)
{
	(this.css || this.div).zIndex = this.z = x;
	dd.z = Math.max(dd.z, x);
}


DDObj.prototype.addChild = function(kd)
{
	if (!this.detach) this.children[this.children.length] = kd;
	kd.setZ(kd.defz = kd.z-2+this.z);
}




function WINSZ(x)
{
	if (x==1 || x==0x02)
	{
		if (dd.n4 || dd.op6 && x==0x02)
		{
			dd.iW = innerWidth;
			dd.iH = innerHeight;
			if (dd.op6) setTimeout("WINSZ()", 0x1ff);
		}
		window.onresize = WINSZ;
	}
	else if ((dd.n4 || dd.op6) && (innerWidth!=dd.iW || innerHeight!=dd.iH))
		location.reload();
	else if (dd.op6) setTimeout("WINSZ()", 0x1ff);
	else setTimeout('dd.recalc()', 0x0a);
}
WINSZ(1);





function PICK(ev)
{
	var e = new dd.evt(ev),
	i, cmp = -1;
	for (var z = 0; z<dd.elements.length; z++)
	{
		i = dd.elements[z];
		if (dd.n4 && e.but>1 && e.src==i.img && !i.cln) return false;
		if (e.but<=1)
		{
			if (i.visible && !i.nodrag && e.x>i.x && e.x<i.x+i.w && e.y>i.y && e.y<i.y+i.h)
			{
				if (i.z>cmp)
				{
					cmp = i.z;
					dd.obj = i;
				}
			}
		}
	}
	if (dd.obj)
	{
		// Added by Xiaowan
		dd.dragFrom = dd.obj.inRegion()? 1:0;
		// End of addition
		
		if (dd.op && !dd.op6)
		{
			var oblur;
			(oblur = document.getElementById('OpBlUr')).style.pixelLeft = e.x;
			oblur.style.pixelTop = e.y;
			(oblur = oblur.children[0].children[0]).focus();
			oblur.blur();
		}
		dd.obj.activate();
		if (dd.ce) document.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP);
		document.onmousemove = DRAG;
		document.onmouseup = DROP;
		dd.prex = e.x;
		dd.prey = e.y;
		if (window.dd_myPickFunc) dd_myPickFunc(e);
		return !(dd.n4 && dd.obj.img || (dd.n6 || dd.kq) && (i = e.src.toString().toLowerCase()).indexOf('inputelement')==-1 && i.indexOf('textarea')==-1);
	}
	if (dd.downFunc) dd.downFunc(ev);
	return true;
}


function DRAG(ev)
{
	if (dd.ie4 || dd.kq || dd.n6 && !dd.n7)
	{
		if (dd.wait) return false;
		dd.wait = true;
		setTimeout('dd.wait = false;', 0x05);
	}

	// xb
	if (!Order_Completed)
		dd.obj.moveTo(dd.obj.x + (e = new dd.evt(ev)).x - dd.prex, dd.obj.y + e.y - dd.prey);
	
	if (window.dd_myDragFunc) dd_myDragFunc(e);
	dd.prex = e.x;
	dd.prey = e.y;
	return false;
}


function DROP(ev)
{
	if (dd.obj)
	{
		// Added by Xiaowan
		dd.dragTo = dd.obj.inRegion()? 1:0;
		// End of addition
		
		if (dd.ce && !dd.moveFunc && !dd.upFunc) document.releaseEvents(Event.MOUSEMOVE|Event.MOUSEUP);
		document.onmousemove = dd.moveFunc;
		document.onmouseup = dd.upFunc;
		if (!dd.obj.img) dd.obj.getWH();
		dd.obj.deactivate();
		if (window.dd_myDropFunc) dd_myDropFunc(new dd.evt(ev));
		dd.obj = null;
	}
}





function SET_DRAGGABLE()
{
	if (!(dd && (dd.n4 || dd.n6 || dd.ie || dd.op || dd.kq))) return;
	for (var a = SET_DRAGGABLE.arguments, htm = '', i, z = 0; z<a.length; z++)
	{
		// intra-loop delay for opera < 7
		if (dd.op6)
		{
			var t0 = (new Date()).getTime();
			while ((new Date()).getTime()-t0 < 0x78);
		}
		// global cursor cmd
		if (a[z].indexOf('c:')==0) dd.curs = a[z];
		// global no-alt cmd
		else if (a[z]==NO_ALT) dd.noalt = 1;
		else
		{
			i = new DDObj(a[z]);
			if (i.img || i.div)
			{
				dd.elements[dd.elements.length] = dd.elements[i.name] = i;
				i.z = 0x03;
				htm += i.htm || '';
				if (i.img && i.multi)
				{
					i.copies = new Array();
					for (var zz = 1; zz<=i.multi; zz++)
					{
						var j = new DDObj(i.name + (i.cln? CLONE : '') + (i.noalt? NOALT : ''), zz);
						dd.elements[dd.elements.length] = i.copies[i.copies.length] = dd.elements[j.name] = j;
						j.z = i.z+zz;
						j.copy = true;
						htm += j.htm;
					}
				}
			}
		}
	}
	if (dd.elements.length)
	{
		document.write(
			(dd.n4? '<div style="position:absolute;"><\/div>\n'
			: (dd.op && !dd.op6)? '<div id="OpBlUr" style="position:absolute;visibility:hidden;"><form><input type="text" size="1" style="width:1px;height:1px;"><\/form><\/div>'
			: '') + htm
		);
		dd.z = 0x32;
		for (z = 0; z<dd.elements.length; z++)
		{
			if ((i = dd.elements[z]).img)
			{
				i.div = i.getDiv(i.id);
				if (i.div.style) i.css = i.div.style;
				i.setZ(i.defz = i.z);
				if (!i.copy)
				{
					if (!i.cln)
					{
						if (dd.n4)
						{
							i.img.src = dd.dmmy.src || self.location.href;
							i.img.src = i.cach.src;
							i.img.src = dd.dmmy.src || self.location.href;
						}
						else i.img.style.visibility = 'hidden';
					}
				}
			}
			else
			{
				// get initial z-index
				if (document.defaultView && typeof document.defaultView.getComputedStyle != "undefined")
					i.defz = dd.Int(document.defaultView.getComputedStyle(i.div, '').getPropertyValue('z-index'));
				else if (i.div.currentStyle)
					i.defz = dd.Int(i.div.currentStyle.zIndex);
				else
					i.defz = dd.Int((i.css || i.div).zIndex);
				i.setZ(i.defz);
			}
			// set cursor
			// xb
			if (!Order_Completed)
				if (i.css && dd.curs && !i.nodrag) i.css.cursor = dd.curs.substring(2);
			// initial visibility of item
			i.visible = true;
		}
		for (z = 0; z<dd.elements.length; z++)
		{
			if (!(i = dd.elements[z]).img)
			{
				i.children = new Array();
				for (var zz = 0; zz<dd.elements.length; zz++)
				{
					var j = dd.elements[zz].img;
					if (j)
					{
						if (dd.n4)
						{
							for (var l = 0; l<i.div.document.images.length; l++)
							{
								if (i.div.document.images[l]==j)
								{
									i.addChild(dd.elements[zz]);
									break;
								}
							}
						}
						else while (j.parentNode || j.offsetParent)
						{
							if ((j.parentNode || j.offsetParent)==i.div)
							{
								i.addChild(dd.elements[zz]);
								break;
							}
							j = j.parentNode || j.offsetParent || null;
						}
					}
				}
			}
		}
		if (window.onload) dd.loadFunc = window.onload;
		window.onload = dd.setEvtHdl;
	}
}



//////////////////////////////////////////////////////////////////////////////
// If not required, the section below may be removed
//////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////
// FOR BACKWARD COMPATIBILITY ONLY
////////////////////////////////////////////

dd.d = document;			// < v. 2.72
var RESET_ZINDEX = RESET_Z; // < 3.44


////////////////////////////////////////////
// FUNCTIONS FOR EXTENDED SCRIPTING
// Use these for your own extensions,
// or to call functions defined elsewhere
////////////////////////////////////////////

/*
dd_myPickFunc IS AUTOMATICALLY CALLED WHEN AN ITEM STARTS TO BE DRAGGED.
The following objects/properties are accessible from here:
evt.x and evt.y: co-ordinates of mouse event (document-related).
evt.src: target of mouse event (not identical with the drag drop object itself).
dd.obj: reference to currently dragged item.
dd.obj.property: access to any property of that item.
dd.obj.method(): for example dd.obj.resizeTo() or dd.obj.swapImage() .
Most requested properties:
	dd.obj.name: image name or layer ID passed to SET_DRAGGABLE();
	dd.obj.x and dd.obj.y: co-ordinates;
	dd.obj.w and dd.obj.h: size.
	For more properties and details, go through the API documentation
	at http://www.walterzorn.com/dragdrop/api_e.htm (english) or
	http://www.walterzorn.de/dragdrop/api.htm (german)
*/
function dd_myPickFunc(evt)
{
	//window.status = 'dd.elements.' + dd.obj.name + '.x = ' + dd.obj.x + '      dd.elements.' + dd.obj.name + '.y = ' + dd.obj.y;
}





/*
dd_myDragFunc IS CALLED DURING AN ITEM IS DRAGGED.
See the description inside dd_myPickFunc above for what's accessible from here.
*/
function dd_myDragFunc(evt)
{
	//window.status = 'dd.elements.' + dd.obj.name + '.x  = ' + dd.obj.x + '     dd.elements.' + dd.obj.name + '.y = ' + dd.obj.y;
}





/*
THIS ONE IS CALLED ONCE AN ITEM IS DROPPED
See the description inside dd_myPickFunc for what's accessible from here.
Here may be investigated, for example, what's the name (dd.obj.name)
of the dropped item, and where (dd.obj.x, dd.obj.y) it has been dropped...
*/
function dd_myDropFunc(evt)
{
	// Added by Xiaowan
	if (dd.dragFrom == 0)
		if (dd.dragTo == 0)
		{
			// from outside to outside
			dd.obj.moveTo(dd.ix, dd.iy);
		}
		else
		{
			// from outside to inside
			if (dd.dragAllowed)
				allocateResource()
			else
				dd.obj.moveTo(dd.ix, dd.iy);
		}
	else
		if (dd.dragTo == 0)
		{
			// from inside to outside
			releaseResource(dd.obj.rid);
			dd.obj.moveTo(dd.ix, dd.iy);
		}
		else
		{
			// from inside to inside
		}

	/*
	if (!dd.obj.inRegion())
		dd.obj.moveTo(dd.ix, dd.iy);
	else
		//window.status = 'dd.elements.' + dd.obj.name + '    dropped at x = ' + dd.obj.x + '      y = ' + dd.obj.y;
		allocateResource()
	*/
	// End of addition
}


