SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[Gen_TimeZone_S]'
GO
CREATE TABLE [dbo].[Gen_TimeZone_S]
(
[TimeZoneID] [int] NOT NULL,
[StandardName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Offset] [float] NULL,
[TimeZone] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZoneDiff] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bias] [int] NULL,
[StandardBias] [int] NULL,
[DaylightBias] [int] NULL,
[swYear] [smallint] NULL,
[swMonth] [smallint] NULL,
[swDayOfWeek] [smallint] NULL,
[swDay] [smallint] NULL,
[swHour] [smallint] NULL,
[swMinute] [smallint] NULL,
[swSecond] [smallint] NULL,
[swMilliseconds] [smallint] NULL,
[dwYear] [smallint] NULL,
[dwMonth] [smallint] NULL,
[dwDayOfWeek] [smallint] NULL,
[dwDay] [smallint] NULL,
[dwHour] [smallint] NULL,
[dwMinute] [smallint] NULL,
[dwSecond] [smallint] NULL,
[dwMilliseconds] [smallint] NULL,
[systemID] [int] NULL,
[DST] [smallint] NULL,
[CurDSTstart] [datetime] NULL,
[CurDSTend] [datetime] NULL,
[CurDSTstart1] [datetime] NULL,
[CurDSTend1] [datetime] NULL,
[CurDSTstart2] [datetime] NULL,
[CurDSTend2] [datetime] NULL,
[CurDSTstart3] [datetime] NULL,
[CurDSTend3] [datetime] NULL,
[CurDSTstart4] [datetime] NULL,
[CurDSTend4] [datetime] NULL,
[CurDSTstart5] [datetime] NULL,
[CurDSTend5] [datetime] NULL,
[CurDSTstart6] [datetime] NULL,
[CurDSTend6] [datetime] NULL,
[CurDSTstart7] [datetime] NULL,
[CurDSTend7] [datetime] NULL,
[CurDSTstart8] [datetime] NULL,
[CurDSTend8] [datetime] NULL,
[CurDSTstart9] [datetime] NULL,
[CurDSTend9] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_TimeZone_S] on [dbo].[Gen_TimeZone_S]'
GO
ALTER TABLE [dbo].[Gen_TimeZone_S] ADD CONSTRAINT [PK_Gen_TimeZone_S] PRIMARY KEY CLUSTERED  ([TimeZoneID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Tmp_AdvAVParams_D]'
GO
CREATE TABLE [dbo].[Tmp_AdvAVParams_D]
(
[TmpID] [int] NOT NULL,
[lineRateID] [smallint] NULL,
[audioAlgorithmID] [smallint] NULL,
[videoProtocolID] [smallint] NULL,
[mediaID] [smallint] NULL,
[videoLayoutID] [int] NULL,
[dualStreamModeID] [smallint] NULL,
[conferenceOnPort] [smallint] NULL,
[encryption] [smallint] NULL,
[maxAudioParticipants] [int] NULL,
[maxVideoParticipants] [int] NULL,
[videoSession] [int] NULL,
[LectureMode] [smallint] NULL,
[videoMode] [smallint] NULL,
[singleDialin] [int] NULL CONSTRAINT [DF_Tmp_AdvAVParams_D_singleDialin] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sim_Contact_D]'
GO
CREATE TABLE [dbo].[Sim_Contact_D]
(
[UserId] [int] NOT NULL,
[SessionId] [int] NOT NULL,
[ContactId] [int] NOT NULL,
[Status] [smallint] NULL CONSTRAINT [DF_SIM_Contact_D_Status] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Cascade_D]'
GO
CREATE TABLE [dbo].[Conf_Cascade_D]
(
[uId] [int] NOT NULL IDENTITY(1, 1),
[confid] [int] NOT NULL,
[instanceid] [int] NOT NULL,
[cascadelinkid] [int] NOT NULL,
[cascadelinkname] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[masterorslave] [smallint] NULL,
[defvideoprotocol] [smallint] NULL,
[connectiontype] [smallint] NULL,
[ipisdnaddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bridgeid] [int] NULL,
[bridgeipisdnaddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[connectstatus] [smallint] NULL,
[outsidenetwork] [smallint] NULL,
[mcuservicename] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[audioorvideo] [smallint] NULL,
[deflinerate] [int] NULL,
[mute] [smallint] NULL,
[addresstype] [smallint] NULL,
[bridgeaddresstype] [smallint] NULL,
[layout] [int] NULL,
[prefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isLecturer] [int] NOT NULL CONSTRAINT [DF_Conf_Cascade_D_isLecturer] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_Cascade_D] on [dbo].[Conf_Cascade_D]'
GO
ALTER TABLE [dbo].[Conf_Cascade_D] ADD CONSTRAINT [PK_Conf_Cascade_D] PRIMARY KEY CLUSTERED  ([confid], [instanceid], [cascadelinkid])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Email_Queue_D]'
GO
CREATE TABLE [dbo].[Email_Queue_D]
(
[from] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[to] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[cc] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCC] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subject] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatetimeCreated] [datetime] NULL,
[Message] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UUID] [int] NOT NULL,
[RetryCount] [int] NULL CONSTRAINT [DF_Email_Queue_D_RetryCount] DEFAULT ((0)),
[LastRetryDateTime] [datetime] NULL,
[Attachment] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Email_Queue_D] on [dbo].[Email_Queue_D]'
GO
ALTER TABLE [dbo].[Email_Queue_D] ADD CONSTRAINT [PK_Email_Queue_D] PRIMARY KEY CLUSTERED  ([UUID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_SearchTemplates_D]'
GO
CREATE TABLE [dbo].[Usr_SearchTemplates_D]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[template] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[userid] [int] NULL CONSTRAINT [DF_Usr_SearchTemplates_D_userid] DEFAULT ((11))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_SearchTemplates_D] on [dbo].[Usr_SearchTemplates_D]'
GO
ALTER TABLE [dbo].[Usr_SearchTemplates_D] ADD CONSTRAINT [PK_Usr_SearchTemplates_D] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sim_UsrStatus_D]'
GO
CREATE TABLE [dbo].[Sim_UsrStatus_D]
(
[UserId] [int] NOT NULL,
[SessionId] [int] NOT NULL,
[status] [smallint] NOT NULL CONSTRAINT [DF_IM_UsrStatus_D_status] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_Level_S]'
GO
CREATE TABLE [dbo].[Usr_Level_S]
(
[LevelID] [int] NOT NULL,
[LevelName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeptID] [int] NOT NULL CONSTRAINT [DF_Usr_Level_S_DeptID] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_List_D]'
GO
CREATE TABLE [dbo].[Inv_List_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [int] NOT NULL,
[Image] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [int] NOT NULL CONSTRAINT [DF_Inv_List_D_Deleted_1] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Exchange_D]'
GO
CREATE TABLE [dbo].[Conf_Exchange_D]
(
[ConferenceID] [int] NOT NULL,
[InstanceID] [int] NOT NULL,
[ApointmentName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CalenderUID] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllDayEvent] [smallint] NOT NULL CONSTRAINT [DF_Conf_Exchange_D_AllDayEvent] DEFAULT ((0)),
[CreatedDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Duration] [int] NULL,
[MeetingStatus] [smallint] NULL,
[Description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subject] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZoneID] [int] NULL,
[Priority] [smallint] NULL,
[ReadStatus] [smallint] NULL,
[MailHeader] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusyStatus] [smallint] NULL,
[LastModified] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_Exchange_D] on [dbo].[Conf_Exchange_D]'
GO
ALTER TABLE [dbo].[Conf_Exchange_D] ADD CONSTRAINT [PK_Conf_Exchange_D] PRIMARY KEY CLUSTERED  ([ConferenceID], [InstanceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_TechSupport_D]'
GO
CREATE TABLE [dbo].[Sys_TechSupport_D]
(
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Sys_TechSupport_D_name] DEFAULT (N'myVRM'),
[email] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[info] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[feedback] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[id] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Ldap_ServerConfig_D]'
GO
CREATE TABLE [dbo].[Ldap_ServerConfig_D]
(
[serveraddress] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[login] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[password] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[port] [int] NULL,
[timeout] [int] NULL,
[schedule] [int] NULL,
[SyncTime] [datetime] NULL,
[SearchFilter] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Ldap_ServerConfig_D_LoginKey] DEFAULT ('on'),
[domainPrefix] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[scheduleTime] [datetime] NULL,
[scheduleDays] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_Templates_D]'
GO
CREATE TABLE [dbo].[Usr_Templates_D]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[connectiontype] [int] NULL,
[ipisdnaddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[initialtime] [int] NULL,
[timezone] [int] NULL,
[videoProtocol] [int] NULL,
[location] [int] NULL,
[languageId] [int] NULL,
[lineRateId] [int] NULL,
[bridgeId] [int] NULL,
[deptId] [int] NULL,
[groupId] [int] NULL,
[ExpirationDate] [datetime] NULL CONSTRAINT [DF_Usr_Templates_D_ExpirationDate] DEFAULT (((1)/(1))/(1980)),
[emailNotification] [int] NULL,
[outsideNetwork] [int] NULL,
[role] [int] NULL,
[addressBook] [int] NULL,
[equipmentId] [int] NULL,
[deleted] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_Templates_D] on [dbo].[Usr_Templates_D]'
GO
ALTER TABLE [dbo].[Usr_Templates_D] ADD CONSTRAINT [PK_Usr_Templates_D] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Dept_CustomAttr_Ex_D]'
GO
CREATE TABLE [dbo].[Dept_CustomAttr_Ex_D]
(
[CustomAttributeID] [int] NOT NULL,
[OptionID] [int] NOT NULL,
[DeptID] [int] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_Ex_D_DeptID] DEFAULT ((0)),
[SubOptionID] [int] NOT NULL,
[SubOptionType] [int] NOT NULL,
[AttributeID] [int] NOT NULL,
[DisplayValue] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayCaption] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_MailServer_D]'
GO
CREATE TABLE [dbo].[Sys_MailServer_D]
(
[IsRemoteServer] [smallint] NOT NULL,
[ServerAddress] [nchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Login] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[password] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[portNo] [int] NULL,
[ConTimeOut] [int] NULL,
[CompanyMail] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[displayname] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[messagetemplate] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[websiteURL] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[id] [int] NOT NULL CONSTRAINT [DF_Sys_MailServer_D_id] DEFAULT ((1))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_RecurInfo_D]'
GO
CREATE TABLE [dbo].[Conf_RecurInfo_D]
(
[confid] [int] NOT NULL,
[uId] [int] NOT NULL IDENTITY(1, 1),
[confuId] [int] NOT NULL CONSTRAINT [DF_Conf_RecurInfo_D_confuId] DEFAULT ((0)),
[timezoneid] [int] NULL,
[duration] [int] NULL,
[recurType] [int] NULL,
[subType] [int] NULL,
[yearMonth] [int] NULL,
[days] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dayNo] [int] NULL,
[gap] [int] NULL,
[startTime] [datetime] NULL,
[endTime] [datetime] NULL,
[endType] [int] NULL,
[occurrence] [int] NULL,
[dirty] [smallint] NULL,
[RecurringPattern] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_Inactive_D]'
GO
CREATE TABLE [dbo].[Usr_Inactive_D]
(
[id] [int] NULL,
[UserID] [int] NOT NULL,
[FirstName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Financial] [int] NULL,
[Admin] [smallint] NULL,
[Login] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZone] [smallint] NULL,
[Language] [smallint] NULL,
[PreferedRoom] [smallint] NULL,
[AlternativeEmail] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoubleEmail] [smallint] NULL,
[PreferedGroup] [smallint] NULL,
[CCGroup] [smallint] NULL,
[Telephone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomID] [int] NULL,
[PreferedOperator] [int] NULL,
[lockCntTrns] [int] NULL,
[DefLineRate] [int] NULL,
[DefVideoProtocol] [int] NULL,
[Deleted] [int] NULL,
[DefAudio] [int] NULL,
[DefVideoSession] [int] NULL,
[MenuMask] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[initialTime] [int] NULL,
[EmailClient] [smallint] NULL,
[newUser] [smallint] NULL,
[PrefTZSID] [int] NULL,
[IPISDNAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultEquipmentId] [smallint] NOT NULL CONSTRAINT [DF_Usr_Inactive_D_DefaultEquipmentId] DEFAULT ((1)),
[connectionType] [smallint] NULL,
[companyId] [int] NULL,
[securityKey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LockStatus] [smallint] NOT NULL CONSTRAINT [DF_Usr_Inactive_D_LockStatus] DEFAULT ((0)),
[LastLogin] [datetime] NULL,
[outsidenetwork] [smallint] NULL,
[emailmask] [bigint] NULL,
[roleID] [int] NULL,
[addresstype] [smallint] NULL,
[accountexpiry] [datetime] NULL,
[approverCOunt] [int] NULL,
[BridgeID] [int] NOT NULL CONSTRAINT [DF_Usr_Inactive_D_BrdigeID] DEFAULT ((1)),
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LevelID] [int] NULL CONSTRAINT [DF_Usr_Inactive_D_LevelID] DEFAULT ((0)),
[preferredISDN] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[portletId] [int] NULL,
[searchId] [int] NULL,
[endpointId] [int] NOT NULL CONSTRAINT [DF_Usr_Inactive_D_endpointId] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_Inactive_D] on [dbo].[Usr_Inactive_D]'
GO
ALTER TABLE [dbo].[Usr_Inactive_D] ADD CONSTRAINT [PK_Usr_Inactive_D] PRIMARY KEY CLUSTERED  ([UserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Ept_List_D]'
GO
CREATE TABLE [dbo].[Ept_List_D]
(
[uId] [int] NOT NULL IDENTITY(1, 1),
[endpointId] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[password] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[protocol] [smallint] NULL,
[connectiontype] [smallint] NULL,
[addresstype] [smallint] NULL,
[address] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deleted] [smallint] NOT NULL CONSTRAINT [DF_Ept_List_D_deleted] DEFAULT ((0)),
[outsidenetwork] [smallint] NULL,
[videoequipmentid] [int] NULL,
[linerateid] [int] NULL,
[bridgeid] [int] NULL,
[endptURL] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[profileId] [int] NOT NULL CONSTRAINT [DF_Ept_List_D_profileId] DEFAULT ((1)),
[profileName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isDefault] [int] NOT NULL CONSTRAINT [DF_Ept_List_D_isDefault] DEFAULT ((0)),
[encrypted] [int] NOT NULL CONSTRAINT [DF_Ept_List_D_encrypted] DEFAULT ((0)),
[MCUAddress] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MCUAddressType] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Ept_List_D] on [dbo].[Ept_List_D]'
GO
ALTER TABLE [dbo].[Ept_List_D] ADD CONSTRAINT [PK_Ept_List_D] PRIMARY KEY CLUSTERED  ([endpointId], [profileId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_ItemList_AV_D]'
GO
CREATE TABLE [dbo].[Inv_ItemList_AV_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[chargeId] [int] NOT NULL CONSTRAINT [DF_Inv_ItemList_AV_D_chargeId] DEFAULT ((0)),
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Quantity] [int] NOT NULL,
[Price] [float] NOT NULL,
[serialNumber] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[portable] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[comment] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Inv_ItemList_AV_D_comment] DEFAULT (''),
[description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deliveryType] [int] NULL,
[deliveryCost] [float] NULL,
[serviceCharge] [float] NULL,
[deleted] [int] NOT NULL CONSTRAINT [DF_Inv_ItemList_D_deleted] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_List_D]'
GO
CREATE TABLE [dbo].[Usr_List_D]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NOT NULL,
[FirstName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Financial] [int] NULL CONSTRAINT [DF_Usr_List_D_Financial] DEFAULT ((0)),
[Admin] [smallint] NULL CONSTRAINT [DF_Usr_List_D_Admin] DEFAULT ((0)),
[Login] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZone] [smallint] NULL CONSTRAINT [DF_Usr_List_D_TimeZone] DEFAULT ((1)),
[Language] [smallint] NULL CONSTRAINT [DF_Usr_List_D_Language] DEFAULT ((1)),
[PreferedRoom] [smallint] NULL CONSTRAINT [DF_Usr_List_D_PreferedRoom] DEFAULT ((1)),
[AlternativeEmail] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoubleEmail] [smallint] NULL CONSTRAINT [DF_Usr_List_D_DoubleEmail] DEFAULT ((0)),
[PreferedGroup] [smallint] NULL CONSTRAINT [DF_Usr_List_D_PreferedGroup] DEFAULT ((1)),
[CCGroup] [smallint] NULL CONSTRAINT [DF_Usr_List_D_CCGroup] DEFAULT ((1)),
[Telephone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomID] [int] NULL CONSTRAINT [DF_Usr_List_D_RoomID] DEFAULT ((1)),
[PreferedOperator] [int] NULL CONSTRAINT [DF_Usr_List_D_PreferedOperator] DEFAULT ((1)),
[lockCntTrns] [int] NULL CONSTRAINT [DF_Usr_List_D_lockCntTrns] DEFAULT ((0)),
[DefLineRate] [int] NULL CONSTRAINT [DF_Usr_List_D_DefLineRate] DEFAULT ((2)),
[DefVideoProtocol] [int] NULL CONSTRAINT [DF_Usr_List_D_DefVideoProtocol] DEFAULT ((1)),
[Deleted] [int] NOT NULL CONSTRAINT [DF_Usr_List_D_Deleted] DEFAULT ((0)),
[DefAudio] [int] NULL,
[DefVideoSession] [int] NULL,
[MenuMask] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[initialTime] [int] NULL,
[EmailClient] [smallint] NULL CONSTRAINT [DF_Usr_List_D_EmailClient] DEFAULT ((0)),
[newUser] [smallint] NULL CONSTRAINT [DF_Usr_List_D_newUser] DEFAULT ((1)),
[PrefTZSID] [int] NULL CONSTRAINT [DF_Usr_List_D_PrefTZSID] DEFAULT ((0)),
[IPISDNAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultEquipmentId] [smallint] NOT NULL CONSTRAINT [DF_Usr_List_D_DefaultEquipmentId] DEFAULT ((1)),
[connectionType] [smallint] NULL,
[companyId] [int] NULL,
[securityKey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LockStatus] [smallint] NOT NULL CONSTRAINT [DF_Usr_List_D_LockStatus] DEFAULT ((0)),
[LastLogin] [datetime] NULL CONSTRAINT [DF_Usr_List_D_LastLogin] DEFAULT ('01/01/1970 12:00 AM'),
[outsidenetwork] [smallint] NULL,
[emailmask] [bigint] NULL,
[roleID] [int] NULL,
[addresstype] [smallint] NULL,
[accountexpiry] [datetime] NULL CONSTRAINT [DF_Usr_List_D_accountexpiry] DEFAULT ('12/31/2010'),
[approverCount] [int] NULL CONSTRAINT [DF_Usr_List_D_approverCount] DEFAULT ((0)),
[BridgeID] [int] NOT NULL CONSTRAINT [DF_Usr_List_D_BridgeID] DEFAULT ((1)),
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LevelID] [int] NULL CONSTRAINT [DF_Usr_List_D_LevelID] DEFAULT ((0)),
[preferredISDN] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[portletId] [int] NULL,
[searchId] [int] NULL,
[endpointId] [int] NOT NULL CONSTRAINT [DF_Usr_List_D_endpointId] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_List_D] on [dbo].[Usr_List_D]'
GO
ALTER TABLE [dbo].[Usr_List_D] ADD CONSTRAINT [PK_Usr_List_D] PRIMARY KEY CLUSTERED  ([UserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_ItemCharge_D]'
GO
CREATE TABLE [dbo].[Inv_ItemCharge_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ItemId] [int] NOT NULL,
[TypeId] [int] NOT NULL,
[deliveryCharge] [float] NOT NULL CONSTRAINT [DF_Inv_ItemCharge_D_deliveryCost] DEFAULT ((0)),
[serviceCharge] [float] NOT NULL CONSTRAINT [DF_Inv_ItemCharge_D_serviceCharge] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_CustomAttrEx_D]'
GO
CREATE TABLE [dbo].[Conf_CustomAttrEx_D]
(
[confid] [int] NOT NULL CONSTRAINT [DF_Conf_CustomAttrEx_D_confid] DEFAULT ((0)),
[instanceid] [int] NOT NULL CONSTRAINT [DF_Conf_CustomAttrEx_D_instanceid] DEFAULT ((0)),
[CustomAttributeID] [int] NOT NULL,
[OptionID] [int] NOT NULL,
[SubOptionID] [int] NOT NULL,
[SubOptionType] [int] NOT NULL,
[AttributeID] [int] NOT NULL,
[DisplayValue] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayCaption] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_Settings_D]'
GO
CREATE TABLE [dbo].[Sys_Settings_D]
(
[LastModified] [datetime] NULL,
[Admin] [int] NULL,
[TimeZone] [int] NOT NULL,
[Logo] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyTel] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyEmail] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyURL] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Connect2] [smallint] NOT NULL,
[DialOut] [smallint] NULL,
[AccountingLogic] [smallint] NULL,
[BillPoint2Point] [smallint] NULL,
[AllLocation] [smallint] NULL,
[SecurityKey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[superAdmin] [smallint] NULL,
[tzsystemid] [int] NULL,
[RealtimeStatus] [int] NOT NULL,
[BillRealTime] [smallint] NOT NULL,
[MultipleDepartments] [int] NULL,
[overAllocation] [smallint] NULL,
[Threshold] [int] NULL,
[autoApproveImmediate] [smallint] NOT NULL,
[adminEmail1] [int] NULL,
[adminEmail2] [int] NULL,
[adminEmail3] [int] NULL,
[AutoAcceptModConf] [smallint] NULL,
[recurEnabled] [smallint] NULL,
[responsetime] [int] NULL,
[responsemessage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SystemStartTime] [datetime] NULL,
[SystemEndTime] [datetime] NULL,
[Open24hrs] [smallint] NULL,
[Offdays] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISDNLineCost] [decimal] (2, 0) NOT NULL,
[ISDNPortCost] [decimal] (2, 0) NOT NULL,
[IPLineCost] [decimal] (2, 0) NOT NULL,
[IPPortCost] [decimal] (2, 0) NOT NULL,
[ISDNTimeFrame] [smallint] NULL,
[foodsecuritykey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[resourcesecuritykey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dynamicinviteenabled] [smallint] NULL,
[doublebookingenabled] [smallint] NULL,
[emaildisclaimer] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[license] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMEnabled] [smallint] NULL CONSTRAINT [DF_Sys_Settings_D_IMEnabled] DEFAULT ((0)),
[IMRefreshConn] [float] NULL,
[IMMaxUnitConn] [int] NULL,
[IMMaxSysConn] [int] NULL,
[DefaultToPublic] [smallint] NOT NULL CONSTRAINT [DF_Sys_Settings_D_DefaultToPublic] DEFAULT ((0)),
[DefaultConferenceType] [smallint] NULL CONSTRAINT [DF_Sys_Settings_D_DefaultConferenceType] DEFAULT ((2)),
[EnableRoomConference] [smallint] NULL CONSTRAINT [DF_Sys_Settings_D_EnableRoomConference] DEFAULT ((1)),
[EnableAudioVideoConference] [smallint] NULL CONSTRAINT [DF_Sys_Settings_D_EnableAudioVideoConference] DEFAULT ((1)),
[EnableAudioOnlyConference] [smallint] NULL CONSTRAINT [DF_Sys_Settings_D_EnableAudioOnlyConference] DEFAULT ((1)),
[DefaultCalendarToOfficeHours] [smallint] NULL CONSTRAINT [DF_Sys_Settings_D_DefaultCalendarToOfficeHours] DEFAULT ((1)),
[RoomTreeExpandLevel] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Sys_Settings_D_RoomTreeExpandLevel] DEFAULT ((3))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_WorkOrder_D]'
GO
CREATE TABLE [dbo].[Inv_WorkOrder_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Type] [int] NOT NULL,
[CatID] [int] NOT NULL,
[AdminId] [int] NOT NULL,
[ConfID] [int] NOT NULL,
[InstanceID] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomID] [int] NOT NULL,
[CompletedBy] [datetime] NULL,
[startBy] [datetime] NULL,
[woTimeZone] [int] NULL CONSTRAINT [DF_Inv_WorkOrder_D_timezone] DEFAULT ((26)),
[comment] [nvarchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [int] NOT NULL CONSTRAINT [DF_Inv_WorkOrder_D_Status] DEFAULT ((0)),
[Reminder] [int] NOT NULL CONSTRAINT [DF_Inv_WorkOrder_D_Reminder] DEFAULT ((0)),
[Notify] [int] NOT NULL CONSTRAINT [DF_Inv_WorkOrder_D_Notify] DEFAULT ((0)),
[WkOType] [int] NOT NULL CONSTRAINT [DF_Inv_WorkOrder_D_WkOType] DEFAULT ((0)),
[description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deliveryType] [int] NULL,
[deliveryCost] [float] NULL,
[serviceCharge] [float] NULL,
[woTax] [float] NULL,
[woTtlCost] [float] NULL,
[deleted] [int] NOT NULL CONSTRAINT [DF_Inv_WorkOrder_D_deleted] DEFAULT ((0)),
[strData] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_ISDNServices_D]'
GO
CREATE TABLE [dbo].[Mcu_ISDNServices_D]
(
[BridgeID] [int] NOT NULL CONSTRAINT [DF_Mcu_ISDNServices_D_BridgeID] DEFAULT ((0)),
[SortID] [int] NOT NULL CONSTRAINT [DF_Mcu_ISDNServices_D_SortID] DEFAULT ((1)),
[ServiceName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartNumber] [bigint] NOT NULL,
[EndNumber] [bigint] NOT NULL,
[NetworkAccess] [smallint] NOT NULL CONSTRAINT [DF_Mcu_ISDNServices_D_networkaccess] DEFAULT ((0)),
[Usage] [smallint] NOT NULL CONSTRAINT [DF_Mcu_ISDNServices_D_usage] DEFAULT ((0)),
[RangeSortOrder] [smallint] NOT NULL CONSTRAINT [DF_Mcu_ISDNServices_D_RangeSortOrder] DEFAULT ((0)),
[uId] [int] NOT NULL IDENTITY(1, 1)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_ISDNServices_D_1] on [dbo].[Mcu_ISDNServices_D]'
GO
ALTER TABLE [dbo].[Mcu_ISDNServices_D] ADD CONSTRAINT [PK_Mcu_ISDNServices_D_1] PRIMARY KEY CLUSTERED  ([uId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Room_D]'
GO
CREATE TABLE [dbo].[Conf_Room_D]
(
[ConfID] [int] NOT NULL,
[RoomID] [int] NOT NULL,
[DefLineRate] [int] NULL,
[DefVideoProtocol] [int] NULL,
[StartDate] [datetime] NULL,
[Duration] [int] NULL,
[instanceID] [int] NOT NULL,
[connect2] [smallint] NULL,
[bridgeIPISDNAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bridgeid] [int] NULL,
[connectiontype] [smallint] NULL,
[ipisdnaddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[connectstatus] [smallint] NULL,
[outsidenetwork] [smallint] NULL,
[mcuservicename] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[audioorvideo] [smallint] NULL,
[mute] [smallint] NULL,
[addresstype] [smallint] NULL,
[bridgeaddresstype] [smallint] NULL,
[layout] [int] NULL,
[endpointId] [int] NULL,
[uId] [int] NOT NULL IDENTITY(1, 1),
[confuId] [int] NOT NULL CONSTRAINT [DF_Conf_Room_D_confuId] DEFAULT ((0)),
[profileId] [int] NULL,
[prefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isLecturer] [int] NOT NULL CONSTRAINT [DF_Conf_Room_D_isLecturer] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_User_D]'
GO
CREATE TABLE [dbo].[Conf_User_D]
(
[ConfID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[CC] [int] NULL,
[Status] [int] NULL,
[Reason] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseDate] [datetime] NULL CONSTRAINT [DF_Conf_User_D_ResponseDate] DEFAULT (((1)/(1))/(1980)),
[DefLineRate] [int] NULL,
[Invitee] [int] NULL,
[RoomID] [int] NULL,
[DefVideoProtocol] [int] NULL,
[InstanceId] [int] NOT NULL,
[IpAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[connectionType] [int] NULL,
[connect2] [smallint] NULL,
[AudioOrVideo] [smallint] NULL,
[IPISDN] [smallint] NULL,
[IPISDNAddress] [nchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bridgeIPISDNAddress] [nchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bridgeid] [int] NULL,
[connectstatus] [smallint] NULL,
[outsidenetwork] [smallint] NULL,
[mcuservicename] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailreminder] [smallint] NULL,
[mute] [smallint] NULL,
[addresstype] [smallint] NULL,
[bridgeaddresstype] [smallint] NULL,
[layout] [int] NULL,
[PartyNotify] [smallint] NULL CONSTRAINT [DF_Conf_User_D_PartyNotify] DEFAULT ((0)),
[interfacetype] [smallint] NULL,
[uId] [int] NOT NULL IDENTITY(1, 1),
[confuId] [int] NOT NULL CONSTRAINT [DF_Conf_User_D_confuId] DEFAULT ((0)),
[videoEquipment] [int] NULL,
[prefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isLecturer] [int] NOT NULL CONSTRAINT [DF_Conf_User_D_isLecturer] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_IPServices_D]'
GO
CREATE TABLE [dbo].[Mcu_IPServices_D]
(
[BridgeID] [int] NOT NULL CONSTRAINT [DF_Mcu_IPServices_D_BridgeID] DEFAULT ((0)),
[SortID] [int] NOT NULL CONSTRAINT [DF_Mcu_IPServices_D_SortID] DEFAULT ((1)),
[ServiceName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Mcu_IPServices_D_ServiceName] DEFAULT ((0)),
[AddressType] [smallint] NOT NULL CONSTRAINT [DF_Mcu_IPServices_D_addresstype] DEFAULT ((0)),
[IPAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NetworkAccess] [smallint] NOT NULL CONSTRAINT [DF_Mcu_IPServices_D_NetworkAccess] DEFAULT ((0)),
[Usage] [smallint] NOT NULL CONSTRAINT [DF_Mcu_IPServices_D_Usage] DEFAULT ((0)),
[uId] [int] NOT NULL IDENTITY(1, 1)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_IPServices_D_1] on [dbo].[Mcu_IPServices_D]'
GO
ALTER TABLE [dbo].[Mcu_IPServices_D] ADD CONSTRAINT [PK_Mcu_IPServices_D_1] PRIMARY KEY CLUSTERED  ([uId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetDSTDate]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION 
dbo.GetDSTDate
(@tzid INTEGER, @time DATETIME, @flag INTEGER)  
RETURNS DATETIME 
AS  
BEGIN 
	DECLARE @year AS INTEGER
	DECLARE @return AS DATETIME
	SELECT TOP 1 @year = year(@time) - 2004 FROM SYS_SETTINGS_D
	IF @year = 0
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart FROM GEN_TIMEZONE_S  WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 1
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart1 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend1 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 2
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart2 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend2 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 3
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart3 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend3 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 4
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart4 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend4 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 5
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart5 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend5 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 6
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart6 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend6 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 7
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart7 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend7 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 8
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart8 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend8 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 9
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart9 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend9 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	RETURN @return
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_GuestList_D]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[Usr_GuestList_D]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NOT NULL,
[FirstName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Financial] [int] NULL,
[Admin] [smallint] NULL,
[Login] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZone] [smallint] NULL,
[Language] [smallint] NULL,
[PreferedRoom] [smallint] NULL,
[AlternativeEmail] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoubleEmail] [smallint] NULL,
[PreferedGroup] [smallint] NULL CONSTRAINT [DF_Usr_GuestList_D_PreferedGroup] DEFAULT ((0)),
[CCGroup] [smallint] NULL,
[Telephone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomID] [int] NULL,
[PreferedOperator] [int] NULL,
[lockCntTrns] [int] NULL,
[DefLineRate] [int] NULL,
[DefVideoProtocol] [int] NULL,
[Deleted] [int] NOT NULL CONSTRAINT [DF_Usr_GuestList_D_Deleted] DEFAULT ((0)),
[DefAudio] [int] NULL,
[DefVideoSession] [int] NULL,
[MenuMask] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailClient] [smallint] NULL,
[initialTime] [int] NULL,
[newUser] [smallint] NULL,
[PrefTZSID] [int] NULL,
[IPISDNAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultEquipmentId] [smallint] NOT NULL CONSTRAINT [DF_Usr_GuestList_D_DefaultEquipmentId] DEFAULT ((1)),
[connectionType] [smallint] NULL,
[companyId] [int] NULL,
[securitKey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LockStatus] [smallint] NOT NULL CONSTRAINT [DF_Usr_GuestList_D_LockStatus] DEFAULT ((0)),
[LastLogin] [datetime] NULL,
[outsidenetwork] [smallint] NULL,
[emailmask] [bigint] NULL,
[roleID] [int] NULL,
[addresstype] [smallint] NULL,
[accountexpiry] [datetime] NULL,
[approverCount] [int] NULL,
[BridgeID] [int] NOT NULL CONSTRAINT [DF_Usr_GuestList_D_BridgeID] DEFAULT ((1)),
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LevelID] [int] NULL CONSTRAINT [DF_Usr_GuestList_D_LevelID] DEFAULT ((0)),
[affiliateId] [int] NULL,
[searchId] [int] NULL,
[endpointId] [int] NOT NULL CONSTRAINT [DF_Usr_GuestList_D_endpointId] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_GuestList_D] on [dbo].[Usr_GuestList_D]'
GO
ALTER TABLE [dbo].[Usr_GuestList_D] ADD CONSTRAINT [PK_Usr_GuestList_D] PRIMARY KEY CLUSTERED  ([UserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_List_D]'
GO
CREATE TABLE [dbo].[Mcu_List_D]
(
[BridgeID] [int] NOT NULL,
[BridgeLogin] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BridgePassword] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BridgeAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [int] NULL,
[Admin] [int] NULL,
[LastModified] [datetime] NULL,
[BridgeName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChainPosition] [int] NULL,
[updateInBridge] [smallint] NULL,
[NewBridge] [smallint] NULL,
[ISDNPhonePrefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BridgeTypeId] [int] NULL,
[SoftwareVer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateFromBridge] [smallint] NULL,
[VirtualBridge] [smallint] NULL,
[isdnportrate] [decimal] (18, 0) NULL,
[isdnlinerate] [decimal] (18, 0) NULL,
[isdnmaxcost] [decimal] (18, 0) NULL,
[isdnthreshold] [decimal] (18, 0) NULL,
[timezone] [int] NULL,
[syncflag] [smallint] NULL,
[isdnthresholdalert] [smallint] NULL,
[malfunctionalert] [smallint] NULL,
[reservedportperc] [int] NULL,
[responsetime] [int] NULL,
[responsemessage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deleted] [smallint] NOT NULL CONSTRAINT [DF_Mcu_List_D_deleted] DEFAULT ((0)),
[maxConcurrentAudioCalls] [int] NULL CONSTRAINT [DF_Mcu_List_D_maxConcurrentAudioCalls] DEFAULT ((10)),
[maxConcurrentVideoCalls] [int] NULL CONSTRAINT [DF_Mcu_List_D_maxConcurrentVideoCalls] DEFAULT ((10))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_List_D] on [dbo].[Mcu_List_D]'
GO
ALTER TABLE [dbo].[Mcu_List_D] ADD CONSTRAINT [PK_Mcu_List_D] PRIMARY KEY CLUSTERED  ([BridgeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Conference_D]'
GO
CREATE TABLE [dbo].[Conf_Conference_D]
(
[userid] [int] NULL,
[confid] [int] NOT NULL,
[externalname] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[internalname] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[password] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[owner] [int] NULL,
[confdate] [datetime] NULL,
[conftime] [datetime] NULL,
[timezone] [int] NULL,
[immediate] [int] NULL,
[audio] [int] NULL,
[videoprotocol] [int] NULL,
[videosession] [int] NULL,
[linerate] [int] NULL,
[recuring] [int] NULL,
[duration] [int] NULL,
[description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[public] [int] NULL,
[deleted] [smallint] NULL,
[continous] [int] NULL,
[transcoding] [int] NULL,
[deletereason] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[instanceid] [int] NOT NULL,
[advanced] [int] NULL,
[totalpoints] [int] NULL,
[connect2] [int] NULL,
[settingtime] [datetime] NULL,
[videolayout] [int] NULL,
[manualvideolayout] [int] NULL,
[conftype] [int] NULL,
[confnumname] [int] NOT NULL IDENTITY(1, 1),
[status] [int] NULL CONSTRAINT [DF_Conf_Conference_D_status] DEFAULT ((0)),
[lecturemode] [int] NULL CONSTRAINT [DF_Conf_Conference_D_lecturemode] DEFAULT ((0)),
[lecturer] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dynamicinvite] [int] NULL,
[CreateType] [int] NULL CONSTRAINT [DF_Conf_Conference_D_CreateType] DEFAULT ((0)),
[ConfDeptID] [int] NULL CONSTRAINT [DF_Conf_Conference_D_ConfDeptID] DEFAULT ((0)),
[ConfOrigin] [int] NOT NULL CONSTRAINT [DF_Conf_Conference_D_ConfOrigin] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_Conference_D] on [dbo].[Conf_Conference_D]'
GO
ALTER TABLE [dbo].[Conf_Conference_D] ADD CONSTRAINT [PK_Conf_Conference_D] PRIMARY KEY CLUSTERED  ([confid], [instanceid])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_AdvAVParams_D]'
GO
CREATE TABLE [dbo].[Conf_AdvAVParams_D]
(
[ConfID] [int] NOT NULL,
[InstanceID] [int] NOT NULL,
[uId] [int] NOT NULL IDENTITY(1, 1),
[confuId] [int] NOT NULL CONSTRAINT [DF_Conf_AdvAVParams_D_confuId] DEFAULT ((0)),
[lineRateID] [smallint] NULL,
[audioAlgorithmID] [smallint] NULL,
[videoProtocolID] [smallint] NULL,
[mediaID] [smallint] NULL,
[videoLayoutID] [int] NULL,
[dualStreamModeID] [smallint] NULL,
[conferenceOnPort] [smallint] NULL,
[encryption] [smallint] NULL,
[maxAudioParticipants] [int] NULL,
[maxVideoParticipants] [int] NULL,
[videoSession] [int] NULL,
[LectureMode] [smallint] NULL,
[videoMode] [int] NULL CONSTRAINT [DF_Conf_AdvAVParams_D_videoMode] DEFAULT ((0)),
[singleDialin] [int] NULL CONSTRAINT [DF_Conf_AdvAVParams_D_singleDialin] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_AdvAVParams_D] on [dbo].[Conf_AdvAVParams_D]'
GO
ALTER TABLE [dbo].[Conf_AdvAVParams_D] ADD CONSTRAINT [PK_Conf_AdvAVParams_D] PRIMARY KEY CLUSTERED  ([ConfID], [InstanceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Loc_Room_D]'
GO
CREATE TABLE [dbo].[Loc_Room_D]
(
[RoomID] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomBuilding] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomFloor] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Capacity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Assistant] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssistantPhone] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectorAvailable] [int] NULL,
[MaxPhoneCall] [int] NULL,
[AdminID] [int] NULL,
[videoAvailable] [smallint] NOT NULL,
[DefaultEquipmentid] [smallint] NOT NULL,
[DynamicRoomLayout] [smallint] NULL,
[Caterer] [smallint] NULL,
[L2LocationId] [int] NULL,
[L3LocationId] [int] NULL,
[Disabled] [smallint] NOT NULL CONSTRAINT [DF_Loc_Room_D_Disabled] DEFAULT ((0)),
[responsetime] [int] NULL CONSTRAINT [DF_Loc_Room_D_responsetime] DEFAULT ((60)),
[responsemessage] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roomimage] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[setuptime] [int] NULL,
[teardowntime] [int] NULL,
[auxattachments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[endpointid] [int] NULL,
[costcenterid] [int] NULL,
[notifyemails] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Dept_CustomAttr_D]'
GO
CREATE TABLE [dbo].[Dept_CustomAttr_D]
(
[DeptID] [int] NOT NULL,
[CustomAttributeId] [int] NOT NULL,
[DisplayTitle] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [int] NOT NULL,
[Mandatory] [smallint] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_D_Mandatory] DEFAULT ((0)),
[Deleted] [smallint] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_D_Deleted] DEFAULT ((0)),
[status] [int] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_D_status] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Dept_CustomAttr_D] on [dbo].[Dept_CustomAttr_D]'
GO
ALTER TABLE [dbo].[Dept_CustomAttr_D] ADD CONSTRAINT [PK_Dept_CustomAttr_D] PRIMARY KEY CLUSTERED  ([DeptID], [CustomAttributeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_WorkItem_D]'
GO
CREATE TABLE [dbo].[Inv_WorkItem_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ItemID] [int] NOT NULL,
[WorkOrderID] [int] NOT NULL CONSTRAINT [DF_Inv_WorkItem_D_WorkOrderID] DEFAULT ((0)),
[quantity] [int] NOT NULL,
[deliveryType] [int] NULL,
[deliveryCost] [float] NULL,
[description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[serviceCharge] [float] NULL,
[deleted] [int] NOT NULL CONSTRAINT [DF_Inv_ListItem_D_deleted] DEFAULT ((0)),
[serviceId] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Dept_CustomAttr_Option_D]'
GO
CREATE TABLE [dbo].[Dept_CustomAttr_Option_D]
(
[CustomAttributeID] [int] NOT NULL,
[DeptID] [int] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_Option_D_DeptID] DEFAULT ((0)),
[OptionID] [int] NOT NULL,
[OptionType] [int] NOT NULL CONSTRAINT [DF_Dept_CustomAttr_Option_D_OptionType] DEFAULT ((4)),
[OptionValue] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Caption] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_ItemList_CA_D]'
GO
CREATE TABLE [dbo].[Inv_ItemList_CA_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MenuCode] [int] NULL,
[Price] [float] NOT NULL,
[Quantity] [int] NOT NULL,
[SerialNumber] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[portable] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[comment] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Inv_ItemList_CA_D_comment] DEFAULT (''),
[description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deliveryType] [int] NULL,
[deliveryCost] [float] NULL,
[serviceCharge] [float] NULL,
[deleted] [int] NOT NULL CONSTRAINT [DF_Inv_ItemList_Cat_D_deleted] DEFAULT ((0)),
[itemId] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_Category_D]'
GO
CREATE TABLE [dbo].[Inv_Category_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Type] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Inv_Category_D_Comment] DEFAULT (''),
[AdminID] [int] NOT NULL,
[Notify] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssignedCostCtr] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowOverBooking] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deleted] [int] NOT NULL CONSTRAINT [DF_Inv_List_D_deleted] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VIEW1]'
GO
CREATE VIEW dbo.VIEW1
AS
SELECT     TOP 100 PERCENT c.DeptID, c.CustomAttributeId, c.DisplayTitle, c.Description, c.Type, c.Mandatory, c.Deleted, o.OptionID, o.OptionValue, 
                      o.Caption
FROM         dbo.Dept_CustomAttr_D c INNER JOIN
                      dbo.Dept_CustomAttr_Option_D o ON c.CustomAttributeId = o.CustomAttributeID
ORDER BY c.CustomAttributeId, o.OptionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VIEW2]'
GO
CREATE VIEW dbo.VIEW2
AS
SELECT     TOP 100 PERCENT *
FROM         dbo.Dept_CustomAttr_Option_D
ORDER BY CustomAttributeID, OptionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_ItemList_HK_D]'
GO
CREATE TABLE [dbo].[Inv_ItemList_HK_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[comment] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deleted] [int] NOT NULL CONSTRAINT [DF_Inv_ItemList_HK_D_deleted] DEFAULT ((0)),
[Quantity] [int] NULL,
[Price] [float] NULL,
[SerialNumber] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[portable] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Inv_ItemList_HK_D_description] DEFAULT (''),
[deliveryType] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[serviceCharge] [float] NULL,
[deliveryCost] [float] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IsDST]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION 
dbo.IsDST 
(@tzid INTEGER, @time DATETIME)  
RETURNS INTEGER 
AS  
BEGIN 

	DECLARE @dstflag AS INTEGER
	DECLARE @return AS INTEGER
	DECLARE @startDST AS DATETIME
	DECLARE @endDST AS DATETIME
	SET @return = 0
	/* First check if the given timezone observes DST anytime of the year */
	SELECT @dstflag = dst FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
	IF @dstflag = 1
	BEGIN
		/* Get the start date for DST for this timezone; Second parameter = 1 */
		SET @startDST = dbo.GetDSTDate(@tzid, @time, 1)
		/* Get the end date for DST for this timezone: Second parameter = 2 */
		SET @endDST = dbo.GetDSTDate(@tzid, @time, 2)
		/* If the given time is in the DST window ... */
		IF @time BETWEEN @startDST AND @endDST
			SET @return = 1
		GOTO QUIT
	END
	ELSE
		GOTO QUIT	
QUIT:
	
	RETURN @return
END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[changeToGMTTime]'
GO
CREATE FUNCTION dbo.changeToGMTTime
   (@tzid integer, @time datetime )
RETURNS datetime 
AS
BEGIN
	/* The time we get in @time is a time in the timezone 
	 * specified by @tzid. We should first apply the DST
	 * correction to this time and next convert it to GMT.
	 */
	DECLARE @offset INTEGER
	DECLARE @return DATETIME
	IF dbo.IsDST(@tzid, @time) = 1
	BEGIN
		/* The time is in DST and a DST correction
		 * needs to be applied
		 */
		SELECT @offset = daylightbias FROM GEN_TIMEZONE_S
			WHERE timezoneid = @tzid
		SET @time = dateadd(minute, @offset, @time)
	END
	SELECT @offset = bias FROM GEN_TIMEZONE_S 
		WHERE timezoneid = @tzid
	SET @return = dateadd(minute, @offset, @time)
	RETURN @return
END
/* 
 * Old code for userpreferedtime is commented because it does
 * not account for Daylight Saving Time (DST). New code above
 * does just that
 */
/*
CREATE FUNCTION dbo.changeToGMTTime
   (@timezoneid integer, @time datetime )
RETURNS datetime 
AS
BEGIN
	declare @offset integer
	set @offset=0
	select @offset=offset  from timezone where timezoneid=@timezoneid 
	RETURN dateadd(hour,@offset,@time)
END
*/



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[userPreferedTime]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[userPreferedTime]
   (@uid integer, @srvtime datetime )
RETURNS datetime 
AS
BEGIN
	/* The time we get in @srvtime is GMT. We should first
	 * convert it into the timezone time of the user. We'll
	 * account for DST after that
	 */
	DECLARE @offset INTEGER
	DECLARE @tzid INTEGER
	DECLARE @return DATETIME
	/* New scheme to determine timezone of user
	 * The old method is commented below. The user
	 * can also be a guest, who might or might not
	 * have a valid timezone field. We default to 
	 * the default system timezone for all guests
	 */
	SELECT TOP 1 @offset = T.bias, @tzid = T.tzid FROM
	(SELECT T.bias AS bias, U.timezone AS tzid
		FROM USR_LIST_D U, GEN_TIMEZONE_S T WHERE
		U.userid = @uid AND U.timezone = T.timezoneid) AS T
	/*SELECT @offset = T.bias, @tzid = U.timezone 
		FROM [USER] U, TIMEZONE T WHERE
		U.userid = @uid AND U.timezone = T.timezoneid
	*/
	SET @return = dateadd(minute, -@offset, @srvtime)
	IF dbo.IsDST(@tzid, @return) = 1
	BEGIN
		/* The time is in DST and a DST correction
		 * needs to be applied
		 */
		SELECT @offset = daylightbias FROM GEN_TIMEZONE_S
			WHERE timezoneid = @tzid
		SET @return = dateadd(minute, -@offset, @return)
	END
	RETURN @return
END
/* 
 * Old code for userpreferedtime is commented because it does
 * not account for Daylight Saving Time (DST). New code above
 * does just that
 */
/*
BEGIN
	declare @offset integer
	select @offset=t.offset  from [user] u,timezone t where u.userid=@uid and u.timezone=t.timezoneid 
	RETURN dateadd(hour,-@offset,@srvtime)
END
*/




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[changeTime]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION dbo.changeTime
   (@tzid integer, @srvtime datetime )
RETURNS datetime 
AS
BEGIN
	/* The time we get in @srvtime is GMT. We should first
	 * convert it into the timezone time specified in @tzid. 
	 * We'll account for DST after that
	 */
	DECLARE @offset INTEGER
	DECLARE @return DATETIME
	SELECT @offset = bias FROM GEN_TIMEZONE_S 
		WHERE timezoneid = @tzid
	SET @return = dateadd(minute, -@offset, @srvtime)
	IF dbo.IsDST(@tzid, @return) = 1
	BEGIN
		/* The time is in DST and a DST correction
		 * needs to be applied
		 */
		SELECT @offset = daylightbias FROM GEN_TIMEZONE_S
			WHERE timezoneid = @tzid
		SET @return = dateadd(minute, -@offset, @return)
	END
	RETURN @return
END
/* 
 * Old code for userpreferedtime is commented because it does
 * not account for Daylight Saving Time (DST). New code above
 * does just that
 */
/*
CREATE FUNCTION dbo.changeTime
   (@timezoneid integer, @srvtime datetime )
RETURNS datetime 
AS
BEGIN
	declare @offset integer
	set @offset=0
	select @offset=offset  from timezone where timezoneid=@timezoneid 
	RETURN dateadd(hour,-@offset,@srvtime)
END
*/


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GMTToUserPreferedTime]'
GO
CREATE FUNCTION dbo.GMTToUserPreferedTime
   (@uid integer, @srvtime datetime )
RETURNS datetime 
AS
BEGIN
	/* The time we get in @srvtime is GMT. We should first
	 * convert it into the timezone time of the user. We'll
	 * account for DST after that
	 */
	DECLARE @offset INTEGER
	DECLARE @tzid INTEGER
	DECLARE @return DATETIME
	SELECT @offset = T.bias, @tzid = U.timezone 
		FROM USR_LIST_D U, GEN_TIMEZONE_S T WHERE
		U.userid = @uid AND U.timezone = T.timezoneid
	SET @return = dateadd(minute, -@offset, @srvtime)
	IF dbo.IsDST(@tzid, @return) = 1
	BEGIN
		/* The time is in DST and a DST correction
		 * needs to be applied
		 */
		SELECT @offset = daylightbias FROM GEN_TIMEZONE_S
			WHERE timezoneid = @tzid
		SET @return = dateadd(minute, -@offset, @return)
	END
	RETURN @return
END
/* 
 * Old code for userpreferedtime is commented because it does
 * not account for Daylight Saving Time (DST). New code above
 * does just that
 */
/*
BEGIN
	declare @offset integer
	select @offset=t.offset  from [user] u,timezone t where u.userid=@uid and u.timezone=t.timezoneid 
	RETURN dateadd(hour,-@offset,@srvtime)
END
*/



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SysDate]'
GO
CREATE FUNCTION dbo.SysDate(@today DATETIME)
 
RETURNS datetime 
AS
BEGIN
	/* The time zone is read from systemtable for system time zone 
	 * it will then convert this date (useually called by getdatre).. 
	 * into system time zone.
	 */
	DECLARE @timezone  INT
	DECLARE @return DATETIME
	
	SELECT @timezone  = TimeZone  FROM Sys_Settings_D 

	SET @return = dbo.changeToGMTTime(@timezone, @today)
	
	return @return	
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[userToGMTTime]'
GO
CREATE FUNCTION dbo.userToGMTTime
   (@uid integer, @time datetime )
RETURNS datetime 
AS
BEGIN
	/* The time we get in @time is a time in the timezone 
	 * specified by the user @uid's timezone. We should 
	 * first apply the DST correction to this time and 
	 * next convert it to GMT.
	 */
	DECLARE @tzid INTEGER
	DECLARE @offset INTEGER
	DECLARE @return DATETIME
	SELECT @tzid = U.timezone FROM USR_LIST_D U WHERE
		U.userid = @uid
	IF dbo.IsDST(@tzid, @time) = 1
	BEGIN
		/* The time is in DST and a DST correction
		 * needs to be applied
		 */
		SELECT @offset = daylightbias FROM GEN_TIMEZONE_S
			WHERE timezoneid = @tzid
		SET @time = dateadd(minute, @offset, @time)
	END
	SELECT @offset = bias FROM GEN_TIMEZONE_S 
		WHERE timezoneid = @tzid
	SET @return = dateadd(minute, @offset, @time)
	RETURN @return
END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Acc_Balance_D]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[Acc_Balance_D]
(
[UserID] [int] NOT NULL,
[TotalTime] [int] NOT NULL,
[TimeRemaining] [int] NOT NULL,
[InitTime] [datetime] NULL,
[ExpirationTime] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Acc_CostCenter_D]'
GO
CREATE TABLE [dbo].[Acc_CostCenter_D]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[minutes] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Acc_Group_D]'
GO
CREATE TABLE [dbo].[Acc_Group_D]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[depreciation] [int] NULL,
[department] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Acc_GroupList_D]'
GO
CREATE TABLE [dbo].[Acc_GroupList_D]
(
[userid] [int] NULL,
[acgroupid] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Acc_Scheme_S]'
GO
CREATE TABLE [dbo].[Acc_Scheme_S]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Cat_Category_S]'
GO
CREATE TABLE [dbo].[Cat_Category_S]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[deleted] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Cat_FoodItem_D]'
GO
CREATE TABLE [dbo].[Cat_FoodItem_D]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[categoryid] [int] NOT NULL,
[price] [money] NULL,
[image] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[veg] [smallint] NULL,
[kosher] [smallint] NULL,
[deleted] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Cat_Order_D]'
GO
CREATE TABLE [dbo].[Cat_Order_D]
(
[orderid] [int] NOT NULL,
[itemid] [int] NOT NULL,
[quantity] [int] NULL,
[totalprice] [money] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Alerts_D]'
GO
CREATE TABLE [dbo].[Conf_Alerts_D]
(
[ConfID] [int] NOT NULL,
[InstanceID] [int] NOT NULL,
[AlertTypeID] [int] NOT NULL,
[Timestamp] [datetime] NULL,
[Message] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_Alerts_D] on [dbo].[Conf_Alerts_D]'
GO
ALTER TABLE [dbo].[Conf_Alerts_D] ADD CONSTRAINT [PK_Conf_Alerts_D] PRIMARY KEY CLUSTERED  ([ConfID], [InstanceID], [AlertTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Approval_D]'
GO
CREATE TABLE [dbo].[Conf_Approval_D]
(
[uId] [int] NOT NULL IDENTITY(1, 1),
[confid] [int] NOT NULL,
[instanceid] [int] NOT NULL,
[entitytype] [smallint] NOT NULL,
[entityid] [int] NOT NULL,
[approverid] [int] NULL,
[decision] [smallint] NOT NULL,
[responsetimestamp] [datetime] NULL,
[responsemessage] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Attachments_D]'
GO
CREATE TABLE [dbo].[Conf_Attachments_D]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[confid] [int] NOT NULL,
[instanceid] [int] NOT NULL,
[attachment] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Bridge_D]'
GO
CREATE TABLE [dbo].[Conf_Bridge_D]
(
[ConfID] [bigint] NOT NULL,
[InstanceID] [int] NOT NULL,
[BridgeID] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_Bridge_D] on [dbo].[Conf_Bridge_D]'
GO
ALTER TABLE [dbo].[Conf_Bridge_D] ADD CONSTRAINT [PK_Conf_Bridge_D] PRIMARY KEY CLUSTERED  ([ConfID], [InstanceID], [BridgeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_CreateType_S]'
GO
CREATE TABLE [dbo].[Conf_CreateType_S]
(
[CreateID] [smallint] NOT NULL,
[VendorName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Version] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Conf_CreateType_S] on [dbo].[Conf_CreateType_S]'
GO
ALTER TABLE [dbo].[Conf_CreateType_S] ADD CONSTRAINT [PK_Conf_CreateType_S] PRIMARY KEY CLUSTERED  ([CreateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_CustomAttr_D]'
GO
CREATE TABLE [dbo].[Conf_CustomAttr_D]
(
[ConfId] [int] NOT NULL,
[InstanceId] [int] NOT NULL,
[CustomAttributeId] [int] NOT NULL,
[SelectedOptionId] [int] NOT NULL,
[SelectedValue] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_FoodOrder_D]'
GO
CREATE TABLE [dbo].[Conf_FoodOrder_D]
(
[orderid] [int] NOT NULL,
[confid] [int] NOT NULL,
[instanceid] [int] NOT NULL,
[roomid] [int] NULL,
[comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[totalprice] [money] NULL,
[completed] [smallint] NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timestamp] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Group_D]'
GO
CREATE TABLE [dbo].[Conf_Group_D]
(
[ConfID] [int] NULL,
[GroupID] [int] NULL,
[CC] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Location_D]'
GO
CREATE TABLE [dbo].[Conf_Location_D]
(
[LocationID] [int] NOT NULL,
[name] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Telephone] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_McuIsdnNumber_D]'
GO
CREATE TABLE [dbo].[Conf_McuIsdnNumber_D]
(
[id] [int] NULL,
[phoneNumber] [nchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[endtime] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_Monitor_D]'
GO
CREATE TABLE [dbo].[Conf_Monitor_D]
(
[confid] [int] NULL,
[instanceid] [int] NULL,
[endpointid] [int] NULL,
[endpointtype] [smallint] NULL,
[timestamp] [datetime] NULL,
[endpointaddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bandwidth] [int] NULL,
[status] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_ResourceOrder_D]'
GO
CREATE TABLE [dbo].[Conf_ResourceOrder_D]
(
[orderid] [int] NOT NULL,
[confid] [int] NOT NULL,
[instanceid] [int] NOT NULL,
[roomid] [int] NULL,
[totalprice] [money] NULL,
[completed] [smallint] NULL,
[locationid] [int] NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timestamp] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Conf_RoomSplit_D]'
GO
CREATE TABLE [dbo].[Conf_RoomSplit_D]
(
[ConfId] [int] NOT NULL,
[InstanceId] [int] NOT NULL,
[RoomId] [int] NOT NULL,
[StartTime] [datetime] NOT NULL,
[Duration] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Custom_HKaw_ReportTier2_D]'
GO
CREATE TABLE [dbo].[Custom_HKaw_ReportTier2_D]
(
[Tier2ID] [int] NULL,
[TimeZoneID] [int] NULL,
[FilePath] [nchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Custom_HKLaw_RoomReport_D]'
GO
CREATE TABLE [dbo].[Custom_HKLaw_RoomReport_D]
(
[RoomID] [int] NULL,
[TimeZoneID] [int] NULL,
[FilePath] [nchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Dept_Approver_D]'
GO
CREATE TABLE [dbo].[Dept_Approver_D]
(
[departmentid] [int] NOT NULL,
[approverid] [int] NOT NULL,
[id] [int] NOT NULL IDENTITY(1, 1)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Dept_List_D]'
GO
CREATE TABLE [dbo].[Dept_List_D]
(
[departmentId] [int] NOT NULL,
[departmentName] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[securityKey] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [int] NOT NULL,
[responsetime] [int] NULL,
[responsemessage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Email_Type_S]'
GO
CREATE TABLE [dbo].[Email_Type_S]
(
[emailtype] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailsubject] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailid] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Err_List_S]'
GO
CREATE TABLE [dbo].[Err_List_S]
(
[ID] [int] NOT NULL,
[Message] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Err_Log_D]'
GO
CREATE TABLE [dbo].[Err_Log_D]
(
[logid] [bigint] NOT NULL IDENTITY(1, 1),
[modulename] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[moduleerrcode] [int] NOT NULL,
[severity] [smallint] NOT NULL,
[file] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[line] [int] NULL,
[timesubmitted] [datetime] NULL,
[timelogged] [datetime] NOT NULL,
[message] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Err_LogPrefs_S]'
GO
CREATE TABLE [dbo].[Err_LogPrefs_S]
(
[modulename] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[loglevel] [smallint] NULL,
[loglife] [int] NULL,
[LogModule] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_AddressType_S]'
GO
CREATE TABLE [dbo].[Gen_AddressType_S]
(
[id] [smallint] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_AlertType_S]'
GO
CREATE TABLE [dbo].[Gen_AlertType_S]
(
[AlertTypeID] [int] NOT NULL,
[Description] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_AlertType_S] on [dbo].[Gen_AlertType_S]'
GO
ALTER TABLE [dbo].[Gen_AlertType_S] ADD CONSTRAINT [PK_Gen_AlertType_S] PRIMARY KEY CLUSTERED  ([AlertTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_AudioAlg_S]'
GO
CREATE TABLE [dbo].[Gen_AudioAlg_S]
(
[AudioID] [int] NULL,
[AudioType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_ConfVideoProtocol_S]'
GO
CREATE TABLE [dbo].[Gen_ConfVideoProtocol_S]
(
[VideoProtocolID] [int] NULL,
[VideoProtocolType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_DialingOption_S]'
GO
CREATE TABLE [dbo].[Gen_DialingOption_S]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[DialingOption] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_DualStreamMode_S]'
GO
CREATE TABLE [dbo].[Gen_DualStreamMode_S]
(
[ID] [int] NOT NULL,
[DualStreamModeType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_HardwareIssue_D]'
GO
CREATE TABLE [dbo].[Gen_HardwareIssue_D]
(
[DateIssue] [datetime] NOT NULL,
[Message] [char] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_InterfaceType_S]'
GO
CREATE TABLE [dbo].[Gen_InterfaceType_S]
(
[ID] [int] NOT NULL,
[Interface] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_InterfaceType_S] on [dbo].[Gen_InterfaceType_S]'
GO
ALTER TABLE [dbo].[Gen_InterfaceType_S] ADD CONSTRAINT [PK_Gen_InterfaceType_S] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_Language_S]'
GO
CREATE TABLE [dbo].[Gen_Language_S]
(
[LanguageID] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_Language_S] on [dbo].[Gen_Language_S]'
GO
ALTER TABLE [dbo].[Gen_Language_S] ADD CONSTRAINT [PK_Gen_Language_S] PRIMARY KEY CLUSTERED  ([LanguageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_LineRate_S]'
GO
CREATE TABLE [dbo].[Gen_LineRate_S]
(
[LineRateID] [int] NOT NULL,
[LineRateType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_LineRate_S] on [dbo].[Gen_LineRate_S]'
GO
ALTER TABLE [dbo].[Gen_LineRate_S] ADD CONSTRAINT [PK_Gen_LineRate_S] PRIMARY KEY CLUSTERED  ([LineRateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_MediaType_S]'
GO
CREATE TABLE [dbo].[Gen_MediaType_S]
(
[ID] [int] NOT NULL,
[MediaType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_MediaType_S] on [dbo].[Gen_MediaType_S]'
GO
ALTER TABLE [dbo].[Gen_MediaType_S] ADD CONSTRAINT [PK_Gen_MediaType_S] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_VideoEquipment_S]'
GO
CREATE TABLE [dbo].[Gen_VideoEquipment_S]
(
[VEid] [int] NOT NULL,
[VEname] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_VideoEquipment_S] on [dbo].[Gen_VideoEquipment_S]'
GO
ALTER TABLE [dbo].[Gen_VideoEquipment_S] ADD CONSTRAINT [PK_Gen_VideoEquipment_S] PRIMARY KEY CLUSTERED  ([VEid])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_VideoLayout_S]'
GO
CREATE TABLE [dbo].[Gen_VideoLayout_S]
(
[ID] [int] NOT NULL,
[VideoLayout] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_VideoLayout_S] on [dbo].[Gen_VideoLayout_S]'
GO
ALTER TABLE [dbo].[Gen_VideoLayout_S] ADD CONSTRAINT [PK_Gen_VideoLayout_S] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_VideoMode_S]'
GO
CREATE TABLE [dbo].[Gen_VideoMode_S]
(
[id] [int] NOT NULL,
[modeName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_VideoProtocol_S]'
GO
CREATE TABLE [dbo].[Gen_VideoProtocol_S]
(
[VideoProtocolID] [int] NOT NULL,
[VideoProtocolType] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_VideoProtocol_S] on [dbo].[Gen_VideoProtocol_S]'
GO
ALTER TABLE [dbo].[Gen_VideoProtocol_S] ADD CONSTRAINT [PK_Gen_VideoProtocol_S] PRIMARY KEY CLUSTERED  ([VideoProtocolID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_VideoSession_S]'
GO
CREATE TABLE [dbo].[Gen_VideoSession_S]
(
[VideoSessionID] [int] NOT NULL,
[VideoSessionType] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_VideoSession_S] on [dbo].[Gen_VideoSession_S]'
GO
ALTER TABLE [dbo].[Gen_VideoSession_S] ADD CONSTRAINT [PK_Gen_VideoSession_S] PRIMARY KEY CLUSTERED  ([VideoSessionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Grp_Detail_D]'
GO
CREATE TABLE [dbo].[Grp_Detail_D]
(
[GroupId] [int] NOT NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Owner] [int] NULL,
[CC] [int] NULL,
[Private] [int] NULL,
[Description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Grp_Detail_D] on [dbo].[Grp_Detail_D]'
GO
ALTER TABLE [dbo].[Grp_Detail_D] ADD CONSTRAINT [PK_Grp_Detail_D] PRIMARY KEY CLUSTERED  ([GroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Grp_Participant_D]'
GO
CREATE TABLE [dbo].[Grp_Participant_D]
(
[GroupID] [int] NULL,
[UserID] [int] NULL,
[CC] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_DeliveryType_S]'
GO
CREATE TABLE [dbo].[Inv_DeliveryType_S]
(
[id] [int] NOT NULL,
[deliveryType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inv_DeliveryType_S] on [dbo].[Inv_DeliveryType_S]'
GO
ALTER TABLE [dbo].[Inv_DeliveryType_S] ADD CONSTRAINT [PK_Inv_DeliveryType_S] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_ItemService_D]'
GO
CREATE TABLE [dbo].[Inv_ItemService_D]
(
[menuId] [int] NOT NULL,
[serviceId] [int] NOT NULL,
[id] [int] NOT NULL IDENTITY(1, 1)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_ItemService_S]'
GO
CREATE TABLE [dbo].[Inv_ItemService_S]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[type] [int] NOT NULL,
[name] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inv_ItemType_S] on [dbo].[Inv_ItemService_S]'
GO
ALTER TABLE [dbo].[Inv_ItemService_S] ADD CONSTRAINT [PK_Inv_ItemType_S] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_Menu_D]'
GO
CREATE TABLE [dbo].[Inv_Menu_D]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[categoryID] [int] NOT NULL,
[name] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[quantity] [int] NULL,
[price] [float] NULL,
[comments] [nvarchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inv_Menu_D] on [dbo].[Inv_Menu_D]'
GO
ALTER TABLE [dbo].[Inv_Menu_D] ADD CONSTRAINT [PK_Inv_Menu_D] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inv_Room_D]'
GO
CREATE TABLE [dbo].[Inv_Room_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[LocationID] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Ldap_BlankUser_D]'
GO
CREATE TABLE [dbo].[Ldap_BlankUser_D]
(
[userid] [int] NULL,
[firstname] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[lastname] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[email] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[telephone] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[whencreated] [datetime] NULL,
[newuser] [smallint] NULL,
[login] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Loc_Approver_D]'
GO
CREATE TABLE [dbo].[Loc_Approver_D]
(
[roomid] [int] NOT NULL,
[approverid] [int] NOT NULL,
[id] [int] NOT NULL IDENTITY(1, 1)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Loc_Department_D]'
GO
CREATE TABLE [dbo].[Loc_Department_D]
(
[RoomId] [bigint] NOT NULL,
[DepartmentId] [bigint] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Loc_Tier2_D]'
GO
CREATE TABLE [dbo].[Loc_Tier2_D]
(
[Id] [int] NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[L3LocationId] [int] NULL,
[Comment] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[disabled] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Loc_Tier3_D]'
GO
CREATE TABLE [dbo].[Loc_Tier3_D]
(
[Id] [int] NULL,
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[disabled] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_Approver_D]'
GO
CREATE TABLE [dbo].[Mcu_Approver_D]
(
[mcuid] [int] NOT NULL,
[approverid] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_CardDetail_S]'
GO
CREATE TABLE [dbo].[Mcu_CardDetail_S]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[CardName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_Card_S] on [dbo].[Mcu_CardDetail_S]'
GO
ALTER TABLE [dbo].[Mcu_CardDetail_S] ADD CONSTRAINT [PK_Mcu_Card_S] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_CardList_D]'
GO
CREATE TABLE [dbo].[Mcu_CardList_D]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[BridgeID] [int] NOT NULL,
[cardTypeId] [int] NOT NULL,
[maxCalls] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_CardList_D] on [dbo].[Mcu_CardList_D]'
GO
ALTER TABLE [dbo].[Mcu_CardList_D] ADD CONSTRAINT [PK_Mcu_CardList_D] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_CardStatus_S]'
GO
CREATE TABLE [dbo].[Mcu_CardStatus_S]
(
[id] [int] NULL,
[status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_CardType_S]'
GO
CREATE TABLE [dbo].[Mcu_CardType_S]
(
[CardTypeID] [int] NOT NULL,
[CardTypeName] [nchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_Details_D]'
GO
CREATE TABLE [dbo].[Mcu_Details_D]
(
[BridgeID] [int] NOT NULL,
[SlotID] [int] NOT NULL,
[CardType] [int] NULL,
[CardStatus] [int] NULL,
[updateInBridge] [smallint] NULL,
[SerialNo] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoftwareVer] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HardwareVer] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_MPIServices_D]'
GO
CREATE TABLE [dbo].[Mcu_MPIServices_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[BridgeID] [int] NOT NULL,
[SortID] [int] NOT NULL,
[ServiceName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AddressType] [int] NULL,
[IPAddress] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetworkAccess] [int] NULL,
[Usage] [int] NULL,
[RangeSortOrder] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_Status_S]'
GO
CREATE TABLE [dbo].[Mcu_Status_S]
(
[id] [smallint] NULL,
[status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_Tokens_D]'
GO
CREATE TABLE [dbo].[Mcu_Tokens_D]
(
[McuID] [int] NOT NULL,
[McuToken] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[McuUserToken] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdate] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_Tokens_D] on [dbo].[Mcu_Tokens_D]'
GO
ALTER TABLE [dbo].[Mcu_Tokens_D] ADD CONSTRAINT [PK_Mcu_Tokens_D] PRIMARY KEY CLUSTERED  ([McuID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Mcu_Vendor_S]'
GO
CREATE TABLE [dbo].[Mcu_Vendor_S]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BridgeInterfaceId] [smallint] NULL,
[videoparticipants] [int] NULL,
[audioparticipants] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Mcu_Vendor_S] on [dbo].[Mcu_Vendor_S]'
GO
ALTER TABLE [dbo].[Mcu_Vendor_S] ADD CONSTRAINT [PK_Mcu_Vendor_S] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Rep_PendingQueue_D]'
GO
CREATE TABLE [dbo].[Rep_PendingQueue_D]
(
[submittedby] [int] NOT NULL,
[type] [smallint] NOT NULL,
[id] [int] NOT NULL,
[input] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[output] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[desc] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[command] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[priority] [smallint] NULL,
[datein] [datetime] NULL,
[dateout] [datetime] NULL,
[pending] [smallint] NOT NULL,
[timelimit] [int] NULL,
[error] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Res_Allocation_D]'
GO
CREATE TABLE [dbo].[Res_Allocation_D]
(
[orderid] [int] NOT NULL,
[resourceid] [int] NOT NULL,
[x] [int] NULL,
[y] [int] NULL,
[fromtime] [datetime] NULL,
[totime] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Res_AllocationPosition_D]'
GO
CREATE TABLE [dbo].[Res_AllocationPosition_D]
(
[AllocationId] [int] NOT NULL,
[X] [int] NOT NULL,
[Y] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Res_List_D]'
GO
CREATE TABLE [dbo].[Res_List_D]
(
[id] [int] NOT NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[model] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[costprice] [money] NULL,
[rentprice] [money] NULL,
[comment] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[image] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[deleted] [smallint] NULL,
[locationid] [int] NULL,
[quantity] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Rpt_Input_D]'
GO
CREATE TABLE [dbo].[Rpt_Input_D]
(
[InputID] [int] NOT NULL,
[InputClause] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Rpt_Schedule_D]'
GO
CREATE TABLE [dbo].[Rpt_Schedule_D]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NOT NULL,
[tID] [int] NOT NULL,
[SubmitDate] [datetime] NOT NULL,
[ReportTitle] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportDescription] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompleteDate] [datetime] NULL,
[TimeZone] [int] NOT NULL,
[Status] [int] NOT NULL,
[inXml] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Deleted] [int] NOT NULL,
[NotifyEmail] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[report] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Rpt_Template_D]'
GO
CREATE TABLE [dbo].[Rpt_Template_D]
(
[ID] [int] NOT NULL,
[userID] [int] NOT NULL,
[inputID] [int] NOT NULL,
[lastModified] [datetime] NOT NULL,
[templateTitle] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[templateDescription] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[query] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[tables] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[filter] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[orderby] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[outputFormat] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[totalQuery] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[columnHeadings] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[outputTotal] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Public] [int] NOT NULL,
[deleted] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sim_Message_D]'
GO
CREATE TABLE [dbo].[Sim_Message_D]
(
[UserId] [int] NOT NULL,
[SessionId] [int] NOT NULL,
[ContactId] [int] NOT NULL,
[Msg] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [smallint] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_Approver_D]'
GO
CREATE TABLE [dbo].[Sys_Approver_D]
(
[approverid] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_DatabaseVersion_S]'
GO
CREATE TABLE [dbo].[Sys_DatabaseVersion_S]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[version] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[changeLog] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[changeDate] [datetime] NOT NULL,
[changeAuthor] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Sys_DatabaseVersion_S] on [dbo].[Sys_DatabaseVersion_S]'
GO
ALTER TABLE [dbo].[Sys_DatabaseVersion_S] ADD CONSTRAINT [PK_Sys_DatabaseVersion_S] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_Exchange_Settings_D]'
GO
CREATE TABLE [dbo].[Sys_Exchange_Settings_D]
(
[CalendarURL] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServerURL] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUPdate] [datetime] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_LoginAudit_D]'
GO
CREATE TABLE [dbo].[Sys_LoginAudit_D]
(
[UserId] [int] NOT NULL,
[LoginDate] [datetime] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_TDF_D]'
GO
CREATE TABLE [dbo].[Sys_TDF_D]
(
[confid] [int] NOT NULL,
[instanceid] [int] NOT NULL,
[transDate] [datetime] NOT NULL,
[transType] [int] NOT NULL,
[table] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[stmt] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[filename] [nchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[line] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sys_TimeZonePref_S]'
GO
CREATE TABLE [dbo].[Sys_TimeZonePref_S]
(
[systemID] [int] NULL,
[name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Tmp_Group_D]'
GO
CREATE TABLE [dbo].[Tmp_Group_D]
(
[TmpID] [int] NOT NULL,
[GroupID] [int] NOT NULL,
[CC] [smallint] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Tmp_List_D]'
GO
CREATE TABLE [dbo].[Tmp_List_D]
(
[TmpID] [int] NOT NULL,
[TmpName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TmpOwner] [int] NULL,
[TmpDescription] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TmpPublic] [smallint] NULL,
[confName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Audio] [int] NULL,
[VideoProtocol] [int] NULL,
[VideoSession] [int] NULL,
[LineRate] [int] NULL,
[Duration] [int] NULL,
[partyNotify] [smallint] NULL,
[Location] [int] NULL,
[Operator] [int] NULL,
[confDescription] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[public] [smallint] NULL,
[deleted] [smallint] NULL,
[Continuous] [smallint] NULL,
[Transcoding] [smallint] NULL,
[DeleteReason] [nchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderID] [int] NULL,
[Advanced] [smallint] NULL,
[Priority] [smallint] NULL,
[PriorityType] [smallint] NULL,
[videoLayout] [smallint] NULL,
[manualVideoLayout] [smallint] NULL,
[lectureMode] [smallint] NOT NULL,
[lecturer] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dynamicinvite] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Tmp_Participant_D]'
GO
CREATE TABLE [dbo].[Tmp_Participant_D]
(
[TmpID] [int] NOT NULL,
[UserID] [int] NULL,
[DefLineRate] [int] NULL,
[Invitee] [smallint] NULL,
[InviteeInRoom] [smallint] NULL,
[DefVideoProtocol] [smallint] NULL,
[partyNotify] [smallint] NULL,
[IPAddress] [nchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConnectionType] [smallint] NULL,
[partyphoneNumber] [nchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [smallint] NULL,
[roomAssistant] [smallint] NULL,
[InterfaceType] [smallint] NULL,
[IPISDNADDRESS] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[audioorVideo] [smallint] NULL,
[VideoEquipmentID] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Tmp_Room_D]'
GO
CREATE TABLE [dbo].[Tmp_Room_D]
(
[tmpid] [int] NOT NULL,
[RoomID] [int] NOT NULL,
[DefLineRate] [int] NULL,
[DefVideoProtocol] [int] NULL,
[Duration] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_Accord_D]'
GO
CREATE TABLE [dbo].[Usr_Accord_D]
(
[NumT1Channel] [int] NOT NULL,
[NumIPPort] [int] NOT NULL,
[NumAudioPort] [int] NOT NULL,
[NumReservedT1] [int] NOT NULL,
[NumReservedIP] [int] NOT NULL,
[NumReservedAudio] [int] NOT NULL,
[LastUpdate] [datetime] NOT NULL,
[Admin] [int] NOT NULL,
[DefLineRate] [int] NULL,
[DefVideoProtocol] [int] NULL,
[DefAudio] [int] NULL,
[DefVideoSession] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_Dept_D]'
GO
CREATE TABLE [dbo].[Usr_Dept_D]
(
[departmentId] [int] NOT NULL,
[userid] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_Dept_D] on [dbo].[Usr_Dept_D]'
GO
ALTER TABLE [dbo].[Usr_Dept_D] ADD CONSTRAINT [PK_Usr_Dept_D] PRIMARY KEY CLUSTERED  ([departmentId], [userid])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_LotusNotesPrefs_D]'
GO
CREATE TABLE [dbo].[Usr_LotusNotesPrefs_D]
(
[userid] [int] NOT NULL,
[noteslogin] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[notespwd] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[notespath] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_LotusNotesPrefs_D] on [dbo].[Usr_LotusNotesPrefs_D]'
GO
ALTER TABLE [dbo].[Usr_LotusNotesPrefs_D] ADD CONSTRAINT [PK_Usr_LotusNotesPrefs_D] PRIMARY KEY CLUSTERED  ([userid])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_Roles_D]'
GO
CREATE TABLE [dbo].[Usr_Roles_D]
(
[roleID] [int] NOT NULL,
[roleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roleMenuMask] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[locked] [smallint] NULL,
[level] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_Roles_D] on [dbo].[Usr_Roles_D]'
GO
ALTER TABLE [dbo].[Usr_Roles_D] ADD CONSTRAINT [PK_Usr_Roles_D] PRIMARY KEY CLUSTERED  ([roleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Usr_TemplatePrefs_D]'
GO
CREATE TABLE [dbo].[Usr_TemplatePrefs_D]
(
[userid] [int] NOT NULL,
[tmpid] [int] NOT NULL,
[priority] [smallint] NULL,
[prioritytype] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Usr_TemplatePrefs_D] on [dbo].[Usr_TemplatePrefs_D]'
GO
ALTER TABLE [dbo].[Usr_TemplatePrefs_D] ADD CONSTRAINT [PK_Usr_TemplatePrefs_D] PRIMARY KEY CLUSTERED  ([userid], [tmpid])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[financial_sperse_delete_conference]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE  PROCEDURE financial_sperse_delete_conference 
   @confid int, 
   @instanceid int,
   @recurrentmode int
AS 

BEGIN
--
-- the procedure for delete a conference, refund the conference creator or users
-- if the billing mode is Straight, just refund the creator
-- if the billing mode is Disperse, refund the invitees, inviteds, and host.
--
  declare @userid int
  declare @duration int

if(@recurrentmode=1)-- if it is one instance of a recurrent conference, so refund it once.
begin
  if (@instanceid=0)  begin set @instanceid=1 end
   
	select @duration = duration 
	from conference where confid=@confid and instanceid=@instanceid

	declare confuser_cursor cursor
	for
	select cu.userid
	 from confuser cu, conference c 
	where cu.confid=c.confid and cu.instanceid=c.instanceid 
		and cu.confid=@confid and cu.instanceid=@instanceid 

	open confuser_cursor
	
	FETCH NEXT FROM confuser_cursor into @userid
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		print @userid
		update financial set 
		timeRemaining = f.timeRemaining+@duration,
		amountAvailable = f.amountAvailable+@duration
		from financial f where userid=@userid	
		FETCH NEXT FROM confuser_cursor into @userid
	END

	CLOSE confuser_cursor
	DEALLOCATE confuser_cursor
	return 0
end
else  -- recurrentmode=0, single conference or a recurrent conference as a single conference
begin
--declare @instanceid int

declare instance_cursor cursor
for
select instanceid from conference where confid=3

open instance_cursor
fetch next from instance_cursor into @instanceid

while @@fetch_status = 0
begin

  --print @instanceid
    
	select @duration = duration 
	from conference where confid=3 and instanceid=@instanceid

	declare confuser_cursor cursor
	for
	select cu.userid
	 from confuser cu, conference c 
	where cu.confid=c.confid and cu.instanceid=c.instanceid 
		and cu.confid=3 and cu.instanceid=@instanceid 


	open confuser_cursor
	
	FETCH NEXT FROM confuser_cursor into @userid
	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		print @userid
		update financial set 
		timeRemaining = f.timeRemaining+@duration,
		amountAvailable = f.amountAvailable+@duration
		from financial f where userid=@userid	
	
		FETCH NEXT FROM confuser_cursor into @userid
	END

	CLOSE confuser_cursor
	DEALLOCATE confuser_cursor
    fetch next from instance_cursor into @instanceid
end

deallocate instance_cursor
end

end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SP_financial_sperse_delete_conference]'
GO
CREATE PROCEDURE SP_financial_sperse_delete_conference 
   @confid int, 
   @instanceid int,
   @recurrentmode int
AS 

BEGIN
--
-- the procedure for delete a conference, refund the conference creator or users
-- if the billing mode is Straight, just refund the creator
-- if the billing mode is Disperse, refund the invitees, inviteds, and host.
--
  declare @userid int
  declare @duration int

if(@recurrentmode=1)-- if it is one instance of a recurrent conference, so refund it once.
begin
  if (@instanceid=0)  begin set @instanceid=1 end
   
	select @duration = duration 
	from conference where confid=@confid and instanceid=@instanceid

	declare confuser_cursor cursor
	for
	select cu.userid
	 from confuser cu, conference c 
	where cu.confid=c.confid and cu.instanceid=c.instanceid 
		and cu.confid=@confid and cu.instanceid=@instanceid 

	open confuser_cursor
	
	FETCH NEXT FROM confuser_cursor into @userid
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		print @userid
		update financial set 
		timeRemaining = f.timeRemaining+@duration,
		amountAvailable = f.amountAvailable+@duration
		from financial f where userid=@userid	
		FETCH NEXT FROM confuser_cursor into @userid
	END

	CLOSE confuser_cursor
	DEALLOCATE confuser_cursor
	return 0
end
else  -- recurrentmode=0, single conference or a recurrent conference as a single conference
begin
--declare @instanceid int

declare instance_cursor cursor
for
select instanceid from conference where confid=3

open instance_cursor
fetch next from instance_cursor into @instanceid

while @@fetch_status = 0
begin

  --print @instanceid
    
	select @duration = duration 
	from conference where confid=3 and instanceid=@instanceid

	declare confuser_cursor cursor
	for
	select cu.userid
	 from confuser cu, conference c 
	where cu.confid=c.confid and cu.instanceid=c.instanceid 
		and cu.confid=3 and cu.instanceid=@instanceid 


	open confuser_cursor
	
	FETCH NEXT FROM confuser_cursor into @userid
	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		print @userid
		update financial set 
		timeRemaining = f.timeRemaining+@duration,
		amountAvailable = f.amountAvailable+@duration
		from financial f where userid=@userid	
	
		FETCH NEXT FROM confuser_cursor into @userid
	END

	CLOSE confuser_cursor
	DEALLOCATE confuser_cursor
    fetch next from instance_cursor into @instanceid
end

deallocate instance_cursor
end

end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT N'Creating extended properties'
GO
sp_addextendedproperty N'MS_Description', N'lookup table is Gen_VideoLayout_D', 'SCHEMA', N'dbo', 'TABLE', N'Conf_AdvAVParams_D', 'COLUMN', N'videoLayoutID'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'lookup table is Gen_DualStreamMode_D', 'SCHEMA', N'dbo', 'TABLE', N'Conf_AdvAVParams_D', 'COLUMN', N'dualStreamModeID'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'boolean field (0=disabled, 1= enabled)', 'SCHEMA', N'dbo', 'TABLE', N'Conf_AdvAVParams_D', 'COLUMN', N'conferenceOnPort'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'boolean field (0= disabled , 1= enabled)', 'SCHEMA', N'dbo', 'TABLE', N'Conf_AdvAVParams_D', 'COLUMN', N'encryption'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'zero is normal, 1 is hidden', 'SCHEMA', N'dbo', 'TABLE', N'Dept_CustomAttr_D', 'COLUMN', N'status'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'Attachment field - to be used for ICal files.', 'SCHEMA', N'dbo', 'TABLE', N'Email_Queue_D', 'COLUMN', N'Attachment'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Inv_ItemList_HK_D', 'COLUMN', N'deleted'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'0=conference,1=template', 'SCHEMA', N'dbo', 'TABLE', N'Inv_WorkOrder_D', 'COLUMN', N'WkOType'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'lookup table is Gen_VideoLayout_D', 'SCHEMA', N'dbo', 'TABLE', N'Tmp_AdvAVParams_D', 'COLUMN', N'videoLayoutID'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'lookup table is Gen_DualStreamMode_D', 'SCHEMA', N'dbo', 'TABLE', N'Tmp_AdvAVParams_D', 'COLUMN', N'dualStreamModeID'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'boolean field (0=disabled, 1= enabled)', 'SCHEMA', N'dbo', 'TABLE', N'Tmp_AdvAVParams_D', 'COLUMN', N'conferenceOnPort'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'boolean field (0= disabled , 1= enabled)', 'SCHEMA', N'dbo', 'TABLE', N'Tmp_AdvAVParams_D', 'COLUMN', N'encryption'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Usr_GuestList_D', 'COLUMN', N'BridgeID'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Usr_List_D', 'COLUMN', N'TimeZone'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Usr_List_D', 'COLUMN', N'Language'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Usr_List_D', 'COLUMN', N'PreferedRoom'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Usr_List_D', 'COLUMN', N'DoubleEmail'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Loc_Tier2_D
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name nvarchar(256) NULL,
	Address nvarchar(512) NULL,
	L3LocationId int NULL,
	Comment nvarchar(4000) NULL,
	disabled smallint NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_Loc_Tier2_D ON
GO
IF EXISTS(SELECT * FROM dbo.Loc_Tier2_D)
	 EXEC('INSERT INTO dbo.Tmp_Loc_Tier2_D (Id, Name, Address, L3LocationId, Comment, disabled)
		SELECT Id, Name, Address, L3LocationId, Comment, disabled FROM dbo.Loc_Tier2_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Loc_Tier2_D OFF
GO
DROP TABLE dbo.Loc_Tier2_D
GO
EXECUTE sp_rename N'dbo.Tmp_Loc_Tier2_D', N'Loc_Tier2_D', 'OBJECT' 
GO
COMMIT
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Loc_Tier3_D
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name nvarchar(256) NULL,
	Address nvarchar(512) NULL,
	disabled smallint NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_Loc_Tier3_D ON
GO
IF EXISTS(SELECT * FROM dbo.Loc_Tier3_D)
	 EXEC('INSERT INTO dbo.Tmp_Loc_Tier3_D (Id, Name, Address, disabled)
		SELECT Id, Name, Address, disabled FROM dbo.Loc_Tier3_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Loc_Tier3_D OFF
GO
DROP TABLE dbo.Loc_Tier3_D
GO
EXECUTE sp_rename N'dbo.Tmp_Loc_Tier3_D', N'Loc_Tier3_D', 'OBJECT' 
GO
COMMIT

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[Loc_Room_D]'
GO
ALTER TABLE [dbo].[Loc_Room_D] DROP CONSTRAINT [DF_Loc_Room_D_Disabled]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Loc_Room_D]'
GO
ALTER TABLE [dbo].[Loc_Room_D] DROP CONSTRAINT [DF_Loc_Room_D_responsetime]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Sys_TechSupport_D]'
GO
ALTER TABLE [dbo].[Sys_TechSupport_D] DROP CONSTRAINT [DF_Sys_TechSupport_D_name]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_State_S]'
GO
CREATE TABLE [dbo].[Gen_State_S]
(
[StateID] [int] NOT NULL IDENTITY(1, 1),
[CountryID] [int] NOT NULL,
[StateCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Selected] [int] NOT NULL CONSTRAINT [DF_Gen_State_S_Default] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Gen_State_S] on [dbo].[Gen_State_S]'
GO
ALTER TABLE [dbo].[Gen_State_S] ADD CONSTRAINT [PK_Gen_State_S] PRIMARY KEY CLUSTERED  ([StateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Sys_TechSupport_D]'
GO
ALTER TABLE [dbo].[Sys_TechSupport_D] ALTER COLUMN [name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [dbo].[Sys_TechSupport_D] ALTER COLUMN [id] [int] NOT NULL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Gen_Country_S]'
GO
CREATE TABLE [dbo].[Gen_Country_S]
(
[CountryID] [int] NOT NULL,
[CountryName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Selected] [int] NOT NULL CONSTRAINT [DF_Gen_Country_S_Default] DEFAULT ((0))
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[Loc_Room_D]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_Loc_Room_D]
(
[RoomID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomBuilding] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomFloor] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Capacity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Assistant] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssistantPhone] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectorAvailable] [int] NULL,
[MaxPhoneCall] [int] NULL,
[AdminID] [int] NULL,
[videoAvailable] [smallint] NOT NULL,
[DefaultEquipmentid] [smallint] NOT NULL CONSTRAINT [DF_Loc_Room_D_DefaultEquipmentid] DEFAULT ((-1)),
[DynamicRoomLayout] [smallint] NULL,
[Caterer] [smallint] NULL,
[L2LocationId] [int] NULL,
[L3LocationId] [int] NULL,
[Disabled] [smallint] NOT NULL CONSTRAINT [DF_Loc_Room_D_Disabled] DEFAULT ((0)),
[responsetime] [int] NULL CONSTRAINT [DF_Loc_Room_D_responsetime] DEFAULT ((60)),
[responsemessage] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roomimage] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[setuptime] [int] NULL,
[teardowntime] [int] NULL,
[auxattachments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[endpointid] [int] NULL,
[costcenterid] [int] NULL,
[notifyemails] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [int] NULL,
[Zipcode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [int] NULL,
[Maplink] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParkingDirections] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalComments] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimezoneID] [int] NULL,
[Longitude] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[auxattachments2] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MapImage1] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MapImage2] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecurityImage1] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecurityImage2] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscImage1] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiscImage2] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom5] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom6] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom7] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom8] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom9] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom10] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Loc_Room_D] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_Loc_Room_D]([RoomID], [Name], [RoomBuilding], [RoomFloor], [RoomNumber], [RoomPhone], [Capacity], [Assistant], [AssistantPhone], [ProjectorAvailable], [MaxPhoneCall], [AdminID], [videoAvailable], [DefaultEquipmentid], [DynamicRoomLayout], [Caterer], [L2LocationId], [L3LocationId], [Disabled], [responsetime], [responsemessage], [roomimage], [setuptime], [teardowntime], [auxattachments], [endpointid], [costcenterid], [notifyemails]) SELECT [RoomID], [Name], [RoomBuilding], [RoomFloor], [RoomNumber], [RoomPhone], [Capacity], [Assistant], [AssistantPhone], [ProjectorAvailable], [MaxPhoneCall], [AdminID], [videoAvailable], [DefaultEquipmentid], [DynamicRoomLayout], [Caterer], [L2LocationId], [L3LocationId], [Disabled], [responsetime], [responsemessage], [roomimage], [setuptime], [teardowntime], [auxattachments], [endpointid], [costcenterid], [notifyemails] FROM [dbo].[Loc_Room_D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Loc_Room_D] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[Loc_Room_D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_Loc_Room_D]', N'Loc_Room_D'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Loc_Department_D]'
GO
ALTER TABLE [dbo].[Loc_Department_D] ADD
[id] [int] NOT NULL IDENTITY(1, 1)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[Loc_Tier2_D]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_Loc_Tier2_D]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[L3LocationId] [int] NULL,
[Comment] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[disabled] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Loc_Tier2_D] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_Loc_Tier2_D]([Id], [Name], [Address], [L3LocationId], [Comment], [disabled]) SELECT [Id], [Name], [Address], [L3LocationId], [Comment], [disabled] FROM [dbo].[Loc_Tier2_D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Loc_Tier2_D] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[Loc_Tier2_D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_Loc_Tier2_D]', N'Loc_Tier2_D'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[Loc_Tier3_D]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_Loc_Tier3_D]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[disabled] [smallint] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Loc_Tier3_D] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_Loc_Tier3_D]([Id], [Name], [Address], [disabled]) SELECT [Id], [Name], [Address], [disabled] FROM [dbo].[Loc_Tier3_D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Loc_Tier3_D] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[Loc_Tier3_D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_Loc_Tier3_D]', N'Loc_Tier3_D'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Sys_TechSupport_D]'
GO
ALTER TABLE [dbo].[Sys_TechSupport_D] ADD CONSTRAINT [DF_Sys_TechSupport_D_id] DEFAULT ((1)) FOR [id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Dept_List_D
	(
	departmentId int NOT NULL IDENTITY (1, 1),
	departmentName varchar(256) NULL,
	securityKey nvarchar(256) NULL,
	Deleted int NOT NULL,
	responsetime int NULL,
	responsemessage nvarchar(4000) NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_Dept_List_D ON
GO
IF EXISTS(SELECT * FROM dbo.Dept_List_D)
	 EXEC('INSERT INTO dbo.Tmp_Dept_List_D (departmentId, departmentName, securityKey, Deleted, responsetime, responsemessage)
		SELECT departmentId, departmentName, securityKey, Deleted, responsetime, responsemessage FROM dbo.Dept_List_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Dept_List_D OFF
GO
DROP TABLE dbo.Dept_List_D
GO
EXECUTE sp_rename N'dbo.Tmp_Dept_List_D', N'Dept_List_D', 'OBJECT' 
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Mcu_Approver_D
	(
	mcuid int NOT NULL,
	approverid int NOT NULL,
	id int NOT NULL IDENTITY (1, 1)
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_Mcu_Approver_D OFF
GO
IF EXISTS(SELECT * FROM dbo.Mcu_Approver_D)
	 EXEC('INSERT INTO dbo.Tmp_Mcu_Approver_D (mcuid, approverid)
		SELECT mcuid, approverid FROM dbo.Mcu_Approver_D WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Mcu_Approver_D
GO
EXECUTE sp_rename N'dbo.Tmp_Mcu_Approver_D', N'Mcu_Approver_D', 'OBJECT' 
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D
	DROP CONSTRAINT DF_Mcu_List_D_deleted
GO
ALTER TABLE dbo.Mcu_List_D
	DROP CONSTRAINT DF_Mcu_List_D_maxConcurrentAudioCalls
GO
ALTER TABLE dbo.Mcu_List_D
	DROP CONSTRAINT DF_Mcu_List_D_maxConcurrentVideoCalls
GO
CREATE TABLE dbo.Tmp_Mcu_List_D
	(
	BridgeID int NOT NULL IDENTITY (1, 1),
	BridgeLogin nvarchar(256) NULL,
	BridgePassword nvarchar(1024) NULL,
	BridgeAddress nvarchar(256) NULL,
	Status int NULL,
	Admin int NULL,
	LastModified datetime NULL,
	BridgeName nvarchar(256) NULL,
	ChainPosition int NULL,
	updateInBridge smallint NULL,
	NewBridge smallint NULL,
	ISDNPhonePrefix nvarchar(50) NULL,
	BridgeTypeId int NULL,
	SoftwareVer nvarchar(100) NULL,
	UpdateFromBridge smallint NULL,
	VirtualBridge smallint NULL,
	isdnportrate decimal(18, 0) NULL,
	isdnlinerate decimal(18, 0) NULL,
	isdnmaxcost decimal(18, 0) NULL,
	isdnthreshold decimal(18, 0) NULL,
	timezone int NULL,
	syncflag smallint NULL,
	isdnthresholdalert smallint NULL,
	malfunctionalert smallint NULL,
	reservedportperc int NULL,
	responsetime int NULL,
	responsemessage nvarchar(4000) NULL,
	deleted smallint NOT NULL,
	maxConcurrentAudioCalls int NULL,
	maxConcurrentVideoCalls int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_deleted DEFAULT ((0)) FOR deleted
GO
ALTER TABLE dbo.Tmp_Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_maxConcurrentAudioCalls DEFAULT ((10)) FOR maxConcurrentAudioCalls
GO
ALTER TABLE dbo.Tmp_Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_maxConcurrentVideoCalls DEFAULT ((10)) FOR maxConcurrentVideoCalls
GO
SET IDENTITY_INSERT dbo.Tmp_Mcu_List_D ON
GO
IF EXISTS(SELECT * FROM dbo.Mcu_List_D)
	 EXEC('INSERT INTO dbo.Tmp_Mcu_List_D (BridgeID, BridgeLogin, BridgePassword, BridgeAddress, Status, Admin, LastModified, BridgeName, ChainPosition, updateInBridge, NewBridge, ISDNPhonePrefix, BridgeTypeId, SoftwareVer, UpdateFromBridge, VirtualBridge, isdnportrate, isdnlinerate, isdnmaxcost, isdnthreshold, timezone, syncflag, isdnthresholdalert, malfunctionalert, reservedportperc, responsetime, responsemessage, deleted, maxConcurrentAudioCalls, maxConcurrentVideoCalls)
		SELECT BridgeID, BridgeLogin, BridgePassword, BridgeAddress, Status, Admin, LastModified, BridgeName, ChainPosition, updateInBridge, NewBridge, ISDNPhonePrefix, BridgeTypeId, SoftwareVer, UpdateFromBridge, VirtualBridge, isdnportrate, isdnlinerate, isdnmaxcost, isdnthreshold, timezone, syncflag, isdnthresholdalert, malfunctionalert, reservedportperc, responsetime, responsemessage, deleted, maxConcurrentAudioCalls, maxConcurrentVideoCalls FROM dbo.Mcu_List_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Mcu_List_D OFF
GO
DROP TABLE dbo.Mcu_List_D
GO
EXECUTE sp_rename N'dbo.Tmp_Mcu_List_D', N'Mcu_List_D', 'OBJECT' 
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	PK_Mcu_List_D PRIMARY KEY CLUSTERED 
	(
	BridgeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_List_D ADD
	confType int NULL
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D
 DROP CONSTRAINT DF_Usr_List_D_LastLogin
GO
ALTER TABLE dbo.Usr_List_D
 DROP CONSTRAINT DF_Usr_List_D_accountexpiry
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
 DF_Usr_List_D_LastLogin DEFAULT getutcdate() FOR LastLogin
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
 DF_Usr_List_D_accountexpiry DEFAULT getutcdate() FOR accountexpiry
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
 DF_Usr_GuestList_D_LastLogin DEFAULT (getUTCdate()) FOR LastLogin
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
 DF_Usr_GuestList_D_accountexpiry DEFAULT (getUTCdate()) FOR accountexpiry
GO
COMMIT
--Table Changes :
--•	Usr_List_D 
--•	Usr_inactive_D
--o	Added the field DateFormat nvarchar(50) default value : nvarchar(50)
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	DateFormat nvarchar(50) NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DateFormat DEFAULT N'MM/dd/yyyy' FOR DateFormat
GO
COMMIT

		
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	DateFormat nvarchar(50) NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_DateFormat DEFAULT N'MM/dd/yyyy' FOR DateFormat
GO
COMMIT

/* Added for AV SWitch*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	enableAV int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableAV DEFAULT 0 FOR enableAV
GO
COMMIT

/* Added for AV SWitch*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableAV int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableAV DEFAULT 0 FOR enableAV
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	timezonedisplay int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_timezonedisplay DEFAULT 0 FOR timezonedisplay
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	timeformat int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_timeformat DEFAULT 0 FOR timeformat
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	enableParticipants int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableParticipants DEFAULT 0 FOR enableParticipants
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableParticipants int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableParticipants DEFAULT 0 FOR enableParticipants
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	timeformat int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_timeformat DEFAULT 0 FOR timeformat
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	timezonedisplay int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_timezonedisplay DEFAULT 0 FOR timezonedisplay
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	TelnetAPI int NULL
GO
ALTER TABLE dbo.Ept_List_D ADD CONSTRAINT
	DF_Ept_List_D_TelnetAPI DEFAULT 0 FOR TelnetAPI
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	SetupTime datetime NULL,
	TearDownTime datetime NULL
GO
COMMIT


