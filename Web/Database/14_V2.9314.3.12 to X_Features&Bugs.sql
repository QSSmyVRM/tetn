/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.12  Starts(19 Sep 2014)           */
/*                                                                                              */
/* ******************************************************************************************** */

/* **********************ZD 101201 -17th Sep 2014 Starts************ */

/****** Object:  StoredProcedure [dbo].[SelectCalendarDetails]    Script Date: 09/16/2014 17:27:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SelectCalendarDetails] 
(
	-- Add the parameters for the stored procedure here
	@timezone			int=null,
	@startTime			datetime=null,
	@endTime			datetime=null,
	@specificEntityCode int=null,
	@codeType			int=null,
	@tablename			varchar(200)=null,
	@dtFormat			varchar(15) = null,
	@tformat			varchar(10)=null
)

As
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@timezone is null)
	begin
		return
	end
	
	if (@codeType is null)
	begin
		return
	end

	/* Temp table for Calendar details */

	CREATE TABLE #TempCalendarDT(
	[Date] datetime,
	Start varchar(50),
	[End] varchar(50), 
	Duration int, 
	Room varchar(500),
	[Meeting Title] varchar(1000),
	[Last Name] varchar(100),
	First varchar(100),
	email varchar(200),
	Phone varchar(50),
	State varchar(100),
	City varchar(100),
	[Conf #] varchar(50), 
	[Meeting Type] varchar(100),
	Category varchar(50),
	Status varchar(50),
	Remarks varchar(2500),
	conftype int,
	connectionType int,
	defVideoProtocol int,
	vidProtocol varchar(20),
	TypeFlag char(1),
	CustomAttributeId int, 
	SelectedOptionId int,
	addresstype int,
	langauge int,
	[Entity Code] varchar(50),
	[Entity Name] varchar(200)
	
)

	declare @ecode as varchar(50)
	declare @optionid as int
	declare @calqry as varchar(4000)
	declare @where as varchar(2000)
	declare @cond as varchar(1000)
	declare @filter as varchar(1000)

	declare @seloptionid as int
	--FB 2045 start	
	declare @syscustattrid as varchar(50)
	SELECT @syscustattrid = CustomAttributeId FROM Dept_CustomAttr_D 
    where CreateType = 1 and DisplayTitle = 'Entity Code'
    --FB 2045 end

	if(@codeType = 1) -- None (Need to identify code)
		begin
			set @seloptionid = ''
			set @ecode = 'NA'			
		end
	else if(@codeType = 2) -- Specific
		begin
			set @seloptionid = @specificEntityCode	
			SELECT top 1 @ecode = Caption FROM Dept_CustomAttr_Option_D where OptionId=@specificEntityCode and Caption is not null
		end
	
	set @filter = ''

	if (@startTime is not null And @endTime is not null)
		begin
			if(@tformat = 'HHmmZ')
				set @filter = @filter + ' AND c.confdate BETWEEN '''+ cast(@startTime as varchar(40)) +''' AND '''+ cast(@endTime as varchar(40)) +''''
			else
				set @filter = @filter + ' AND c.confdate BETWEEN dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@startTime as varchar(40)) +''') AND dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@endTime as varchar(40)) +''')'
		end

	if (@tablename <> '' AND @tablename is not null)
		begin
			if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
			 begin
				set @filter = @filter +' AND cu.roomid IN (Select roomid from '+ @tablename +')'
			 end
		end

/* Details Part */
	set @calqry = 'Select a.*, isnull(d.[Entity Code],''NA'') as [Entity Code], isnull(d.[Entity Name],''NA'') as [Entity Name] From '
	if(@tformat = 'HHmmZ')
	Begin
		set @calqry = @calqry + '(SELECT c.confdate AS Date,'
	End
	else
	Begin
		set @calqry = @calqry + '(SELECT dbo.changeTime('+ cast(@timezone as varchar(2)) +',c.confdate) AS Date,'
	End
	--FB 2588
	if(@tformat = 'HHmmZ')
	Begin
		set @calqry = @calqry + 'replace(CONVERT(varchar(5),c.confdate,114),'':'','''') + ''Z'' as Start,'
		set @calqry = @calqry + 'replace(CONVERT(varchar(5),dateadd(minute,c.duration, c.confdate),114),'':'','''') + ''Z'' as [End],'
	End
	else if(@tformat = 'HH:mm')
	Begin
		set @calqry = @calqry + 'CONVERT(varchar(5),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate),108) as Start,'
		set @calqry = @calqry + 'CONVERT(varchar(5),dbo.changeTime('+ cast(@timezone as varchar(2)) +', dateadd(minute,c.duration, c.confdate)),108) as [End],'	
	End
	else
	Begin
		set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate)))),13,8) as Start,'
		set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate)) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate))))),13,8) as [End],'
	End
	set @calqry = @calqry + ' c.Duration, l.name AS Room, c.externalname AS [Meeting Title], u.lastname as [Last Name], '
	set @calqry = @calqry + ' u.firstname as First, u.email,'
	set @calqry = @calqry + ' Case when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))>0)) Then (l.AssistantPhone + '', '' + u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))=0)) Then (l.AssistantPhone) '
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))>0)) Then (u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))=0)) Then ('''')'
	set @calqry = @calqry + ' End   AS Phone ' 
	set @calqry = @calqry + ' ,l3.name AS State, l2.name AS City, '
	set @calqry = @calqry + ' c.confnumname AS [Conf #],'
	set @calqry = @calqry + ' case c.conftype '
	set @calqry = @calqry + ' when 1 then ''Video'' '
	set @calqry = @calqry + ' when 2 then ''AudioVideo'' '
	set @calqry = @calqry + ' when 3 then ''Immediate'' '
	set @calqry = @calqry + ' when 4 then ''Point to Point'' '
	set @calqry = @calqry + ' when 5 then ''Template'' '
	set @calqry = @calqry + ' when 6 then ''AudioOnly'' '
	set @calqry = @calqry + ' when 7 then ''RoomOnly'' '
	set @calqry = @calqry + ' when 8 then ''Hotdesking'' '
	set @calqry = @calqry + ' when 9 then ''OBTP'' '
	set @calqry = @calqry + ' when 11 then ''Phantom'' '
	set @calqry = @calqry + ' else '''''	
	set @calqry = @calqry + ' end AS [Meeting Type],'

/*	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Outbound'' '
	set @calqry = @calqry + ' else ''Inbound'' '
	set @calqry = @calqry + ' end AS Category,'
*/
	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Dial-out from MCU'' '
	set @calqry = @calqry + ' when 1 then ''Dial-in to MCU'' '
	set @calqry = @calqry + ' when 3 then ''Direct to MCU'' '
	set @calqry = @calqry + ' end AS Category,'

	set @calqry = @calqry + ' case c.Status '
	set @calqry = @calqry + ' when 0 then ''Confirmed'' '
	set @calqry = @calqry + ' else ''Pending'''
	set @calqry = @calqry + ' end AS ''Status'', c.description  as Remarks'
	set @calqry = @calqry + ',c.conftype,cu.connectionType,cu.defVideoProtocol,'
	
	--set @calqry = @calqry + ' case cu.defVideoProtocol '
	
	set @calqry = @calqry + ' case cu.addresstype '
	set @calqry = @calqry + ' when 4 then ''ISDN'''
	set @calqry = @calqry + ' else ''IP'''
	set @calqry = @calqry + ' end AS vidProtocol, ''D'' as TypeFlag'
	set @calqry = @calqry + ' ,CustomAttributeId, SelectedOptionId, cu.addresstype, u.language'
/* Where Part */

	set @where = ' FROM '
	set @where = @where + ' conf_conference_d c,'
	set @where = @where + ' usr_list_d u, '
	set @where = @where + ' conf_room_d cu, '
	set @where = @where + ' loc_room_d l, '
	set @where = @where + ' loc_tier2_d l2,'
	set @where = @where + ' loc_tier3_d l3,'
	set @where = @where + ' conf_customattr_d ca'
	set @where = @where + ' WHERE (c.confid = cu.confid AND c.instanceid = cu.instanceid)'
	set @where = @where + ' AND (c.deleted = 0)'
	set @where = @where + ' AND c.owner = u.userid'
	set @where = @where + ' AND l.roomid = cu.roomid'
	set @where = @where + ' AND l2.id = l.l2locationid'
	set @where = @where + ' AND l3.id = l.l3locationid'
	set @where = @where + ' AND (c.confid = ca.confid AND c.instanceid = ca.instanceid)'

	if(len(@filter) > 1)
		set @where = @where + @filter

	set @where = @where + ' )a'
	set @where = @where + ' left outer join '
	set @where = @where + ' (Select Caption AS [Entity Code], helpText as [Entity Name], '
	set @where = @where + ' OptionID, CustomAttributeId, languageid from Dept_CustomAttr_Option_D '
	--set @where = @where + ' Where CustomAttributeId=1'
    --set @where = @where + ' Where CustomAttributeId='''+ @syscustattrid +'''' --FB 2045
	--Set @where = @where + ' And OptionType=6'
	--Set @where = @where + ' And DeptId=0'
	--Set @where = @where + ' And OptionValue=0)D '
	set @where = @where + ' )D on a.CustomAttributeId = D.CustomAttributeId'
	set @where = @where + ' AND a.SelectedOptionId=D.OptionID'
	set @where = @where + ' AND a.language = D.Languageid'

	declare @cmdsql as varchar(8000)

	if(@codeType=0) -- All
		Begin
			set @seloptionid = ''
			Declare cursor1 Cursor for
				
				SELECT Distinct Caption AS EntityCode, OptionID as OptionID
				FROM Dept_CustomAttr_Option_D where Caption is not null

			Open cursor1

			Fetch Next from cursor1 into @ecode,@optionid

			While @@Fetch_Status = 0
			Begin
								
				set @cond = ''
				Set @cond = ' Where a.SelectedOptionId ='+ Cast(@optionid as varchar(10))
												
				Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
				--print @cmdsql
				exec (@cmdsql)
				set @cond = ''
				set @cmdsql = ''

				Fetch Next from cursor1 into @ecode,@optionid
			End

			Close cursor1
			Deallocate cursor1

			/* Need to identify the entity codes */
			
			set @cond = ''
			Set @cond = ' Where a.SelectedOptionId ='''' OR a.SelectedOptionId = -1'
						
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			--print @cmdsql
			exec (@cmdsql)
			set @cond = ''
			set @cmdsql = ''

		End
	else 
		Begin
			
			set @cond = ''
			if @codetype = 1
				set @cond = ' Where [Entity code] is null or [Entity code] = ''-1'''
			else
				set @cond = ' Where a.SelectedOptionId ='+ cast(@seloptionid as varchar(10))
			
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			print @cmdsql
			exec (@cmdsql)
			set @cmdsql = ''			
			set @cond = ''
		End

/* ** Updates the duration as per requirement **

conftype = 11 phantom
conftype = 4  point-point
conftype = 7  room only

7. If a conference type is room only then the value posted in the duration field will always be equal to 0
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where conftype in(7,11) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

/*
8. Due to the method of connectivity by NGC, in a point-to-point defined conference there will be three rooms with the same meeting title, and the same date/time displayed, however,  one of the rooms is a phantom room and should be assigned a duration of 0.
   All point-to-point calls are to be billed as ISDN calls.

 PROGRAMMING NOTE: Any ISDN Room which doesn't belong to State = "ISDN Network" and City = "PacBell PRI's" should have duration as zero (0).
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where (rtrim(State) <> ''ISDN Network'' AND rtrim(City) <> ''PacBell PRIs'' AND addresstype=4) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

set @calqry = ''

if (@tablename <> '' AND @tablename is not null)
begin
	if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
	 begin
		set @cmdsql = ''
		set @cmdsql = 'drop table '+ @tablename
		exec(@cmdsql)
	 end
end
set @cmdsql = ''

/* Detail */
set @cmdsql = null
--FB 2588
-- ZD 100958
declare @dformat as varchar(10);
if(@dtFormat = 'dd/MM/yyyy')
Begin
	set @dformat = '103';
End
else if(@dtFormat = 'dd MMM yyyy')
Begin
	set @dformat = '106';
End
else
Begin
	set @dformat = '101';
End

set @cmdsql = ' Select ltrim(rtrim(convert(char,[Date],'+ @dformat +'))) as [Date],'

set @cmdsql = @cmdsql +'ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where TypeFlag=''D'';'

/* Consolidated Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as Entity_Code, sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Connects]) as [Connects],'

set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP(Hours)], '
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([VOIP Totals])/sum([Hours]))*100) '
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [IP %],'

set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in (Hours)],'

set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out (Hours)],'
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([ISDN Outbound Totals])/sum([Hours]))*100)'
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [ISDN Dial-out %]'
--ISDN Outbound
set @cmdsql = @cmdsql + ' from '
set @cmdsql = @cmdsql + ' (Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'''
set @cmdsql = @cmdsql + ' group by [Entity Code] '

set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], isnull((sum(isnull(Duration,0))/60),0) as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where addresstype <> 4 and  TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], count(*) as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT  Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]) d group by [Entity Code];'

/* Entity codewise Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql = @cmdsql + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql = @cmdsql + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'
set @cmdsql = @cmdsql + ' from ('
--Pt-Pt Mins
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'' group by [Entity Code]'

--ISDN Outbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'' '
set @cmdsql = @cmdsql + ' group by [Entity Code] '

--ISDN Inbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and  TypeFlag=''D'' group by [Entity Code]'

--IP Dial-Out Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], sum(isnull(Duration,0)) as [VOIP Totals],0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype<>4 AND connectionType=0) and  TypeFlag=''D'' group by [Entity Code]'

--Hours
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

--Minutes
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]'
--Audio-Video Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' group by [Entity Code]'

--IP Dial-in Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], sum(isnull(Duration,0)) as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype <> 4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ') d group by [Entity Code];'

--ISDN Network - seperate tab
--All point-to-point calls are to be billed as ISDN calls.
-- Conftype = 4 (Point-Point)
--FB 2588
-- ZD 100958
set @cmdsql = @cmdsql + ' Select ltrim(rtrim(convert(char,[Date],'+ @dformat +'))) as [Date],'

set @cmdsql = @cmdsql + ' ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where ((addresstype=4 and TypeFlag=''D'''
set @cmdsql = @cmdsql + ' and State=''ISDN Network'' and City=''PacBell PRIs'') OR (Conftype=4));'


/* ISDN Summary */
declare @condisdn as varchar (1000)
declare @condisdn1 as varchar (1000)
declare @cmdsql2 as varchar(5000)

set @condisdn1 =  'and ((addresstype=4 and State=''ISDN Network'' and City=''PacBell PRIs''))'

set @cmdsql2 = ' Select ''ISDN'' as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql2 = @cmdsql2 + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'

set @cmdsql2 = @cmdsql2 + ' from ('
--Pt-Pt Mins
set @cmdsql2 = @cmdsql2 + ' Select sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Outbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=0 and  TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Inbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=1 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--Hours
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT Where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--Minutes
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1
--Audio-Video Mins
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--IP Dial-in Mins
set @cmdsql2 = @cmdsql2 + ')d;'

exec(@cmdsql + @cmdsql2)


END

GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/* **********************ZD 101201 -17th Sep 2014 ENDs************ */
--ZD 101930
Update Icons_Ref_S set Label='Account Settings' Where Label ='My Preferences' and IconID =36

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.12  Ends(25 Sep 2014)             */
/*                              Features & Bugs for V2.9314.3.13  Starts(26 Sep 2014)           */
/*                                                                                              */
/* ******************************************************************************************** */


/* **************************** ZD 102057 - 1st Oct 2014  Start ************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acc_Balance_D ADD
	UsedMinutes int NULL,
	ScheduledMinutes int NULL
GO
ALTER TABLE dbo.Acc_Balance_D ADD CONSTRAINT
	DF_Acc_Balance_D_UsedMinutes DEFAULT 0 FOR UsedMinutes
GO
ALTER TABLE dbo.Acc_Balance_D ADD CONSTRAINT
	DF_Acc_Balance_D_ScheduledMinutes DEFAULT 0 FOR ScheduledMinutes
GO
COMMIT


/* Query for existing data */
Update Acc_Balance_D  set InitTime = (select MIN(LoginDate) from Sys_LoginAudit_D where UserId = 11), 
ExpirationTime = (select MIN(LoginDate) from Sys_LoginAudit_D where UserId = 11) where UserID = 11

Update Acc_Balance_D set TotalTime = TimeRemaining, UsedMinutes = 0;


Update Acc_Balance_D set scheduledMinutes = 
isnull((select mins from (select  a.owner, SUM(isnull(McuSetupTime,0) + isnull(duration,0) - isnull(MCUTeardonwnTime,0)) mins
from Conf_Conference_D a where a.status not in (9) 
group by a.owner) as x where owner = a.userid), 0) from Acc_Balance_D a 



/* Audit table */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Acc_Balance_D ADD
	UsedMinutes int NULL,
	ScheduledMinutes int NULL
GO
COMMIT

Update Audit_Acc_Balance_D set UsedMinutes = 0, ScheduledMinutes = 0

 
/* **************************** ZD 102057 - 1st Oct 2014  End ************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.13  Ends(1st Oct 2014)             */
/*                              Features & Bugs for V2.9414.3.0  Starts(1st Oct 2014)           */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.0  Ends(1st Oct 2014)             */
/*                              Features & Bugs for V2.9414.3.1  Starts(10th Oct 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */

--ZD 101714
insert into Gen_Language_S (LanguageID, Name, Country, languagefolder) values(5, 'French', 'France', 'fr')

--ZD 102054

update User_Lobby_D set label = 'Advanced Form' where Iconid = 13
update User_Lobby_D set label = 'Express Form' where Iconid = 19
update icons_ref_s set label = 'Advanced Form' where Iconid = 13
update icons_ref_s set label = 'Express Form' where Iconid = 19

/* **************************** ZD 102098 - 14th Oct 2014  Starts ************************** */

update Gen_TimeZone_S set TimeZone ='Nuku''alofa' where TimeZoneID = 65
Delete from Gen_TimeZone_S where TimeZoneID in (75, 97, 40)

-- 75 - 71
-- 97 - 33
-- 40 - 81

update Usr_List_D set TimeZone = 71 where TimeZone = 75
update Usr_List_D set TimeZone = 33 where TimeZone = 97
update Usr_List_D set TimeZone = 81 where TimeZone = 40

update Loc_Room_D set TimezoneID = 71 where TimezoneID = 75
update Loc_Room_D set TimezoneID = 33 where TimezoneID = 97
update Loc_Room_D set TimezoneID = 81 where TimezoneID = 40

update Conf_Conference_D set timezone = 71 where timezone = 75
update Conf_Conference_D set timezone = 33 where timezone = 97
update Conf_Conference_D set timezone = 81 where timezone = 40

update Mcu_List_D set timezone = 71 where timezone = 75
update Mcu_List_D set timezone = 33 where timezone = 97
update Mcu_List_D set timezone = 81 where timezone = 40

update Sys_Settings_D set timezone = 71 where timezone = 75
update Sys_Settings_D set timezone = 33 where timezone = 97
update Sys_Settings_D set TimeZone = 81 where timezone = 40

update Usr_Templates_D set timezone = 71 where timezone = 75
update Usr_Templates_D set timezone = 33 where timezone = 97
update Usr_Templates_D set TimeZone = 81 where timezone = 40

update Inv_WorkOrder_D set woTimeZone = 71 where woTimeZone = 75
update Inv_WorkOrder_D set woTimeZone = 33 where woTimeZone = 97
update Inv_WorkOrder_D set woTimeZone = 81 where woTimeZone = 40

/* **************************** ZD 102098 - 14 Oct 2014  End ************************** */

/* **************************** ZD 102159 - 16 Oct 2014  Starts ************************** */
update Org_Settings_D set EnableRoomServiceType = 0

update Loc_Room_D set ServiceType = -1

update Conf_Conference_D set ServiceType = -1
/* **************************** ZD 102159 - 14 Oct 2014  End ************************** */
/* **************************** ZD 102085 - 17 Oct 2014  Starts ************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EWSConfAdmins nvarchar(150) NULL
GO
COMMIT

Update Org_Settings_D set EWSConfAdmins =''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Audit_EWS_FailureConf_D
	(
	UDID int NOT NULL IDENTITY (1, 1),
	ConfHost int NOT NULL,
	ConfOrgID int NOT NULL,
	ConfInXML nvarchar(MAX) NULL,
	ConfOutXML nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Audit_EWS_FailureConf_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* **************************** ZD 102085 - 17 Oct 2014  End ************************** */
/* **************************** ZD 102234 - 23 Oct 2014 Starts************************** */
update gen_timezone_s set DST = 1 where timezoneid in (57, 78, 80, 81, 87, 88, 89, 90, 91, 93, 94, 95, 102)
/* **************************** ZD 102234 - 23 Oct 2014 Ends************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.1  Ends(24th Oct 2014)            */
/*                              Features & Bugs for V2.9414.3.2  Starts(27th Oct 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */



--ZD 102052-LDAP with Templates - AD Integration Phase 2 - Starts

update Usr_Templates_D set groupId = -1 where groupId = 0
update Usr_Templates_D set deptId = '' where deptId is null
update Usr_Templates_D set ipisdnaddress = '' where ipisdnaddress is null

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.Ldap_Groups_D.RoleId', N'Tmp_RoleOrTemplateId_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Ldap_Groups_D.Tmp_RoleOrTemplateId_1', N'RoleOrTemplateId', 'COLUMN' 
GO
ALTER TABLE dbo.Ldap_Groups_D ADD
	Origin int NULL
GO
COMMIT

Update Ldap_Groups_D set Origin = 0 


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Templates_D ADD
	enableParticipants int NULL,
	enableAV int NULL,
	enableAdditionalOption int NULL,
	enableAVWO int NULL,
	enableCateringWO int NULL,
	enableFacilityWO int NULL,
	enableExchange int NULL,
	enableDomino int NULL,
	enableMobile int NULL
GO
COMMIT

update Usr_Templates_D set enableParticipants =1,enableAV=1,enableAdditionalOption=1,enableAVWO=1,enableCateringWO=1,enableFacilityWO=1
update Usr_Templates_D set enableExchange =1,enableDomino=1,enableMobile=1



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Templates_D
	DROP CONSTRAINT DF_Usr_Templates_D_ExpirationDate
GO
CREATE TABLE dbo.Tmp_Usr_Templates_D
	(
	id int NOT NULL IDENTITY (1, 1),
	name nvarchar(256) NULL,
	connectiontype int NULL,
	ipisdnaddress nvarchar(256) NULL,
	initialtime int NULL,
	timezone int NULL,
	videoProtocol int NULL,
	location nvarchar(150) NULL,
	languageId int NULL,
	lineRateId int NULL,
	bridgeId int NULL,
	deptId nvarchar(250) NULL,
	groupId int NULL,
	ExpirationDate datetime NULL,
	emailNotification int NULL,
	outsideNetwork int NULL,
	role int NULL,
	addressBook int NULL,
	equipmentId int NULL,
	deleted int NULL,
	companyId int NULL,
	enableParticipants int NULL,
	enableAV int NULL,
	enableAdditionalOption int NULL,
	enableAVWO int NULL,
	enableCateringWO int NULL,
	enableFacilityWO int NULL,
	enableExchange int NULL,
	enableDomino int NULL,
	enableMobile int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Usr_Templates_D SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Usr_Templates_D ADD CONSTRAINT
	DF_Usr_Templates_D_ExpirationDate DEFAULT (((1)/(1))/(1980)) FOR ExpirationDate
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_Templates_D ON
GO
IF EXISTS(SELECT * FROM dbo.Usr_Templates_D)
	 EXEC('INSERT INTO dbo.Tmp_Usr_Templates_D (id, name, connectiontype, ipisdnaddress, initialtime, timezone, videoProtocol, location, languageId, lineRateId, bridgeId, deptId, groupId, ExpirationDate, emailNotification, outsideNetwork, role, addressBook, equipmentId, deleted, companyId, enableParticipants, enableAV, enableAdditionalOption, enableAVWO, enableCateringWO, enableFacilityWO, enableExchange, enableDomino, enableMobile)
		SELECT id, name, connectiontype, ipisdnaddress, initialtime, timezone, videoProtocol, location, languageId, lineRateId, bridgeId, CONVERT(nvarchar(250), deptId), groupId, ExpirationDate, emailNotification, outsideNetwork, role, addressBook, equipmentId, deleted, companyId, enableParticipants, enableAV, enableAdditionalOption, enableAVWO, enableCateringWO, enableFacilityWO, enableExchange, enableDomino, enableMobile FROM dbo.Usr_Templates_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_Templates_D OFF
GO
DROP TABLE dbo.Usr_Templates_D
GO
EXECUTE sp_rename N'dbo.Tmp_Usr_Templates_D', N'Usr_Templates_D', 'OBJECT' 
GO
ALTER TABLE dbo.Usr_Templates_D ADD CONSTRAINT
	PK_Usr_Templates_D PRIMARY KEY CLUSTERED 
	(
	id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


--ZD 102052-LDAP with Templates - AD Integration Phase 2 - End

--ZD 102312

update  Dept_CustomAttr_D set FieldController = 'Host' where DisplayTitle = 'Work'
update  Dept_CustomAttr_D set FieldController = 'Host' where DisplayTitle = 'Cell'

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.2  Ends(12th Nov 2014)            */
/*                              Features & Bugs for V2.9414.3.3  Starts(13th Nov 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.3  Ends(17th Nov 2014)            */
/*                              Features & Bugs for V2.9414.3.4  Starts(17th Nov 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.4  Ends(19th Nov 2014)            */
/*                              Features & Bugs for V2.9414.3.5  Starts(19th Nov 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */


--ZD 102366 Starts
update Usr_GuestList_D set FirstName = replace(FirstName, '<', '(') where FirstName like '%<%'
update Usr_GuestList_D set FirstName = replace(FirstName, '>', ')') where FirstName like '%>%'

update Usr_GuestList_D set LastName = replace(LastName, '<', '(') where LastName like '%<%'
update Usr_GuestList_D set LastName = replace(LastName, '>', ')') where LastName like '%>%'

update Usr_GuestList_D set Email = replace(Email, '''', '') where Email like '%''%'

--ZD 102366 End
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.5  Ends(26th Nov 2014)            */
/*                              Features & Bugs for V2.9414.3.6  Starts(26th Nov 2014)          */
/*                              Features & Bugs for V2.9414.3.6  Ends  (2nd Dec 2014)          */
/* ******************************************************************************************** */

